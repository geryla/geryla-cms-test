<?php
    $navigationMenu = new navigationMenu($database, $appController);
    //$adminEngine = new adminEngine($database, $localeEngine);
    //$moduleAdmins = new moduleAdmins($adminEngine, $localizationClass);

    $navigationMenu->setPermissions([]);
    $navigationMenu->checkPermanentAccess(1);
    $defaultActive = ( $presenter->activeContent->pageType != "module") ? 'active': '';

    echo '<ul class="navigation">';
        echo '<li id="dashboard" class="link '.$defaultActive.'">';
            echo '<a href="/'.ADMIN.'/dashboard">';
                echo '<span class="icon"><i class="fa fa-university"></i></span>';
                echo '<span class="text full">'.translate("dashboard").'</span>';
            echo '</a>';
        echo '</li>';
        $navigationMenu->getModulesToAdminMenu($presenter->activeContent->module, $presenter->activeContent->page, 2);
    echo '</ul>';