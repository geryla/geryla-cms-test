<?php
    echo '<!--[if IE 8]>';
    echo '<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>';
    echo '<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>';
    echo '<![endif]-->';

    echo '<script type="text/javascript" src="'.relativePath(ADMIN_DIR).'assets/scripts/main.js?v=2"></script>';

    echo '<script src="'.relativePath(ADMIN_DIR).'assets/scripts/jquery.colorbox-min.js"></script>';
    echo '<script type="text/javascript" src="'.relativePath(ADMIN_DIR).'assets/scripts/js.cookie.js"></script>';
    echo '<script type="text/javascript" src="'.relativePath(ADMIN_DIR).'assets/scripts/jquery.nestable.js"></script>';
    echo '<script type="text/javascript" src="'.relativePath(ADMIN_DIR).'assets/scripts/jquery-ui-1.10.4.custom.min.js"></script>';
    echo '<script type="text/javascript" src="'.relativePath(ADMIN_DIR).'assets/scripts/jqueryTimepicker.js"></script>';
    echo '<script type="text/javascript" src="'.relativePath(ADMIN_DIR).'assets/scripts/colorpicker.js"></script>';

    foreach( $modulesController->getConfig('paths') as $modulePaths ){
        if( isset($modulePaths["js"]["backend"]) ){
            foreach($modulePaths["js"]["backend"] as $jsFile){
                echo '<script type="text/javascript" src="'.$jsFile.'?v=2"></script>';
            }
        }
    }

    if( isset($enableGraphs) && $enableGraphs === true){
        echo '<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">'; // GRAPGS
        echo '<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>';
        echo '<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>';

        require_once(relativePath(ADMIN_DIR).'layout/dashGraphsScripts.php');
    }
?>