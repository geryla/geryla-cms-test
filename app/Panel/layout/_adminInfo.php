<?php
echo '<li class="dropdown adminInfo">';
    echo '<span class="icon dropdown-label">'.$presenter->loggedAdmin->name.' '.$presenter->loggedAdmin->surname.'</span>';
    echo '<ul>';
        echo '<i class="role">'.$presenter->loggedAdmin->adminTitle.'</i>';
        if($appController->isActiveModule("attendances")){
            include(MODULE_DIR.'attendances/includes/admin/navigationLink.php');
        }
        echo '<li><a class="icon" href="/'.ADMIN.'/logout"><i class="fa fa-sign-out"></i><b>'.translate("logout").'</b></a></li>';
    echo '</ul>';
echo '</li>';