<?php
    $moduleErrors = false;

    $enabledLanguages = $localizationClass->allActiveLanguages;
    $notTranslatedTemplates = $otherLanguages = $missingTranslates = [];
    $mainLanguage = $localeEngine->returnMainLanguage();

    foreach($enabledLanguages as $activeLang){
        if($activeLang["langShort"] !== $mainLanguage["langShort"]){
            $otherLanguages[] = $activeLang["title"];
            $notTranslatedTemplates = array_merge($notTranslatedTemplates, $templateClass->getAllTemplateAnchors($activeLang["langShort"],true, true));
        }
    }

    echo '<article class="module">';
        echo '<header><h2>'.translate("mod-localization-dashboard-languageSettings").'</h2></header>';
        echo '<section>';

            if(!empty($notTranslatedTemplates)){
                foreach($notTranslatedTemplates as $missingTemplate => $missingTemplateUrl){
                    $missingTranslates[] = $missingTemplate;
                }

                echo '<div class="moduleWarning error">';
                    echo "<b>".translate("mod-localization-dashboard-warning")."</b>: <i class='red'>".translate("mod-localization-dashboard-missingTranslations")."</i><br />";
                    echo '<i>'.implode(", ", $missingTranslates).'</i>';
                echo '</div>';
            }

            echo '<div>';
                echo '<div><b>'.translate("mod-localization-dashboard-defaultLanguage").':</b> '.$mainLanguage["title"].'</div>';
                echo '<div>';
                    if(!empty($otherLanguages)){
                        echo '<b>'.translate("mod-localization-dashboard-otherLanguages").':</b> '.implode(", ", $otherLanguages).'<br />';
                    }
                echo '</div>';
            echo '</div>';

            if($appController->isActiveModule("localeSatellite")){
                $localeSatellite = new localeSatellite($database, $appController);
                $satelliteDomains = $localeSatellite->getSatelliteDomains();
                if(!empty($satelliteDomains)){
                    echo '<br><div>';
                        echo '<div><b>'.translate("localeSatellite").':</b> '.translate("localeSatellite-dashboard-active").'</div>';
                        echo '<div>';
                            foreach($satelliteDomains as $domain => $domainLang){
                                echo '<span><b class="blue">'.$domain.'|</b>'.$domainLang.'</span><br />';
                            }
                        echo '</div>';
                    echo '</div>';
                }
            }
        echo '</section>';
    echo '</article>';
