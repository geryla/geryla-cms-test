<?php
    if(isset($_SESSION["security"]["adminLogin"]["remainingTime"])){
        if($_SESSION["security"]["adminLogin"]["remainingTime"] <= 1){
            $remainTime = translate("error-logOut-remainingTime-less");
        }elseif($_SESSION["security"]["adminLogin"]["remainingTime"] <= 10 && $_SESSION["security"]["adminLogin"]["remainingTime"] >= 5){
            $remainTime = "1 ".translate("error-logOut-remainingTime-minute");
        }else{
            $remainTime = (int) $_SESSION["security"]["adminLogin"]["remainingTime"]." ".translate("error-logOut-remainingTime-minutes");
        }

        echo '<p id="notification" class="error logOut">';
            echo '<i class="fa fa-clock-o"></i>';
            echo '<span>'.translate("error-logOut-remainingTime")." ".$remainTime.'</span>';
            echo '<span class="extend"><a href="'.$filtrationEngine->easyFilterLink($_SERVER["REQUEST_URI"],"extendLogin",$presenter->loggedAdmin->id).'">'.translate("error-logOut-extendTime").'</a></span>';
        echo '</p>';
    }

    if( isset($_GET["extendLogin"]) ){
        $adminEngine = new adminEngine($database, $localeEngine);
        $extendTime = $adminEngine->extendAdminLogin($_GET["extendLogin"], $_SESSION["security"]["adminLogin"]["logIn"]);

        if($extendTime === true){
            $parts = parse_url($_SERVER["REQUEST_URI"]);
            parse_str($parts['query'], $queryParams);
            unset($queryParams["extendLogin"]);
            $queryString = http_build_query($queryParams);
            $extendRedirect = (!empty($queryString)) ? $parts['path']."?".$queryString : $parts['path'];

            header('Location:  '.$extendRedirect);
        }
    }