<?php
$moduleErrors = false;

echo '<article class="module">';
    echo '<header><h2>'.translate("logAttempts-lastLogin-title").'</h2></header>';
    echo '<section>';
        $lastLogin = $systemLogger->getAllLogsByAction("login", 10);
        if($lastLogin){
            $admins = new adminEngine($database, $localeEngine);
            foreach ($lastLogin as $log){
                $user = $admins->getAdminById($log["userId"]);
                echo '<div>'.$log["logTime"].' - '.$user["name"].' '.$user["surname"].'</div>';
            }
        }
    echo '</section>';
echo '</article>';