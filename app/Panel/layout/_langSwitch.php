<?php  
    $languageSwitcher = $localizationClass->getAllActiveLanguages();
    $switcherValue = $activeSwitch = null;

    $urlString = (strpos($_SERVER["REQUEST_URI"], '?') === false) ? "?" : "&";
    $urlString = $_SERVER["REQUEST_URI"].$urlString."setAdminLang=";

    foreach($languageSwitcher as $langSwitch){
        $defaultNoTax = ($langSwitch["defaultNoTax"] == 1) ? "true" : "false";
        if($_SESSION["languages"]["adminLang"] == $langSwitch["langShort"]) {
            $isDefault = ($langSwitch["main"] == 1) ? "true" : "false";
            $activeSwitch =  '<span class="icon dropdown-label" data-default="'.$isDefault.'">'.$langSwitch["title"].'</span>';
            continue;
        }
        $switcherValue .= '<li class="item"><a class="text" href="'.$urlString.$langSwitch["langShort"].'">'.$langSwitch["title"].'</a></li>';
    }

    echo '<li class="dropdown langSwitcher" data-defaultNoTax="'.$defaultNoTax.'">';
        echo $activeSwitch;
        if($switcherValue !== null){
            echo '<ul>';
                echo $switcherValue;
            echo '</ul>';
        }
    echo '</li>';

    unset($switcherValue);
    unset($urlString);
    unset($activeSwitch);
    unset($isDefault);
    unset($languageSwitcher);
?>