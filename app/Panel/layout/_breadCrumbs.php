<?php  
    echo '<div id="breadcrumbs">';
        echo '<span class="icon">'.(( isset($presenter->activeModuleConfig->moduleIcon) ) ? '<i class="fa '.$presenter->activeModuleConfig->moduleIcon.'"></i>' : '<i class="fa fa-university"></i>').'</span>';
        echo '<h2 class="pageTitle">'.$presenter->templateData->title.'</h2>';

        if( isset($presenter->breadCrumbs) ){
            echo '<ul class="crumbs">';
                foreach($presenter->breadCrumbs as $breadOrderNum => $breadCrumb){
                    $breadCrumb = (object) $breadCrumb;
                    $parent = $navigationMenu->activeParent;

                    if($breadOrderNum == 0 && $parent !== null && ($parent->url != $breadCrumb->url && !empty($parent->title)) ){
                        echo '<li><a href="/'.ADMIN.'/'.$parent->url.'">'.$parent->title.'</a></li>';
                    }
                    echo '<li><a href="/'.ADMIN.'/'.$breadCrumb->url.'">'.$breadCrumb->title.'</a></li>';
                }
            echo '</ul>';
        }
    echo '</div>';
?>