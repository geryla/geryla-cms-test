<?php
  //print_r($graphMoneyData);
  $monthsForMoney = array();
  foreach($graphMoneyData as $orders){
    $orderMonth = date('n', strtotime($orders["vytvoreno"]));
    $orderYear = date('Y', strtotime($orders["vytvoreno"]));

    if( !$monthsForMoney[$orderMonth] ){
      $monthsForMoney[$orderMonth] = array("nazev"=>translate("ad-date-month-".$orderMonth)." ".$orderYear,"castka"=>$orders["konecna_cena_s_DPH"]);
    }else{     
      $monthsForMoney[$orderMonth]["castka"] = $monthsForMoney[$orderMonth]["castka"] + $orders["konecna_cena_s_DPH"];
    }
  }   
?>

<script type="text/javascript">
  var day_data = [
    <?php 
      $pomCount = 0;
      $allMonths = count($monthsForMoney);
      foreach($monthsForMoney as $key => $moneyMonth){
        $pomCount++;    
        echo '{"elapsed": "'.$moneyMonth["nazev"].'", "value": "'.$moneyMonth["castka"].' '.translate("currency").'"}';
        echo ( $allMonths != $pomCount ) ? ',' : '';            
      }
    ?>
  ];
  Morris.Line({
    element: 'dateBarGraph',
    data: day_data,
    xkey: 'elapsed',
    ykeys: ['value'],
    labels: ['<?php echo translate("dashboard-subtitle-celkem");?>'],
    parseTime: false
  });
</script>