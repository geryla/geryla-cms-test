<?php
// ======= FUNCTIONS 
/* From security reason cant be password changed before user is logged (other admin can do a change) */
    $adminEngine = new adminEngine($database, $localeEngine);
    $adminEngine->adminRedirect();
    
// ======= CONTENT 
  echo '<div class="loginBox col-md-4 col-md-offset-4">';
        
    echo '<div class="topBorder"></div>';
    echo '<div class="content">';
      echo '<h1>'.translate("authorization-pass-title").'</h1>';
      echo '<p>'.translate("authorization-pass-subtitle").'</p>';
      
      echo '<form id="forgottForm" action="" method="POST" >';
        
        echo '<div class="formRow">';
          echo '<label>';
            echo '<div class="placeholder">'.translate("authorization-login-email").':</div>';
            echo '<input type="email" name="email" />';
          echo '</label>';
        echo '</div>';
        
        echo '<div class="customCheckbox">';
          //echo '<input type="checkbox" name="remember" class="checkboxStyled" id="remember" />';
  			  //echo '<label for="remember"><span>'.translate("authorization-login-remember").'</span></label>';
        echo '</div>';
        
        echo '<input type="submit" name="submitPass" value="'.translate("authorization-pass-send").'" class="loginButton" />';
        
      echo '</form>';
      
    echo '</div>';
    
    if( isset($returnMessage) && !empty($returnMessage) ){ echo "<div class='loginMessage'>".$returnMessage."</div>"; }
    echo '<div class="bottomBorder">© '.date('Y').' '.DEVELOP_NAME.'. '.translate("system-build").' '.SYSTEM_VERSION.'</div>';
    
  echo '</div>';
?>