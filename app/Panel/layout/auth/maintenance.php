<?php
    $maintenanceMeta =  '<head>';
        $maintenanceMeta .= '<meta charset="utf-8">';
        $maintenanceMeta .= '<meta name="description" content="'.$presenter->metaData->description.'">';
        $maintenanceMeta .= '<meta name="keywords" content="'.$presenter->metaData->keywords.'">';
        $maintenanceMeta .= '<meta name="author" content="www.ress-design.cz">';
        $maintenanceMeta .= '<meta property="og:url" content="'.$presenter->metaData->pageUrl.'">';
        $maintenanceMeta .= '<meta property="og:title" content="'.$presenter->metaData->title.'">';
        $maintenanceMeta .= '<meta property="og:image" content="">';
        $maintenanceMeta .= '<meta property="og:description" content="'.$presenter->metaData->description.'">';
        $maintenanceMeta .= '<meta property="og:site_name" content="'.translate("page_title").'">';
        $maintenanceMeta .= '<meta name="robots" content="'.$presenter->metaData->allowIndex.'" />';
        $maintenanceMeta .= '<meta name="viewport" content="width=device-width, initial-scale=1">';
        $maintenanceMeta .= '<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">';
        $maintenanceMeta .= '<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">';
        $maintenanceMeta .= '<title>'.translate("page_title").' - '.$presenter->metaData->title.'</title>';
        $maintenanceMeta .= ($presenter->templateData->css !== null ) ? '<style type="text/css">'.$presenter->templateData->css.'</style>' : '';
    echo str_replace("<head>", $maintenanceMeta, $presenter->templateData->html);