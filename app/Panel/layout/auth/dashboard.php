<?php
    if(useIfExists(env('ALLOW_PROJECT_PANELS'), true) === true){
        if(isNotEmpty($modulesController->getConfig('panels'))){
            $panelsParameters = isExisting('panels', $_POST) ? $_POST["panels"] : null;

            $panelDateTime = ($panelsParameters !== null && isExisting('panelDateTime', $panelsParameters)) ? $panelsParameters["panelDateTime"] : "today";
            $dateTimeStart = ($panelsParameters !== null && isExisting('dateTimeStart', $panelsParameters)) ? $panelsParameters["dateTimeStart"] :  date('d.m.Y').' 00:00';
            $dateTimeEnd = ($panelsParameters !== null && isExisting('dateTimeEnd', $panelsParameters)) ? $panelsParameters["dateTimeEnd"] :  date('d.m.Y').' 23:59';
            $panelSortableDates = Panels::getSortableDates();

            //TODO: Switch price type, convert into different prices (now support only CZK)
            Panels::setParameters(["panelDateTime" => $panelDateTime, "dateTimeStart" => $dateTimeStart, "dateTimeEnd" => $dateTimeEnd,  "priceType" => "noTax", "currency" => "CZK"]);

            echo '<section class="panel-wrap">';
                echo '<form class="panel-filter" id="js-panel_filter" method="POST">';
                foreach($panelSortableDates as $dateType){
                    echo '<label class="'.(($panelDateTime == $dateType) ? "active" : null).'">';
                        echo '<input type="radio" group="panelDateTime" name="panels[panelDateTime]" value="'.$dateType.'" '.(($panelDateTime == $dateType) ? "checked" : null).' />';
                        echo '<span>'.translate("ad-panels-sortable-dates-".$dateType).'</span>';
                    echo '</label>';

                    if($dateType == "range"){
                        echo '<div class="panel-range '.(($panelDateTime == $dateType) ? "active" : null).'">';
                            echo '<input type="text" name="panels[dateTimeStart]" value="'.$dateTimeStart.'" class="datepicker" />';
                            echo '<input type="text" name="panels[dateTimeEnd]" value="'.$dateTimeEnd.'" class="datepicker" />';
                            echo '<label><i class="fa fa-filter"></i></label>';
                        echo '</div>';
                    }
                }
                echo '</form>';

                $enabledPanels = (file_exists(CONFIG_DIR.'/_CmsPanels.php')) ? include(CONFIG_DIR.'/_CmsPanels.php') : [];
                foreach($modulesController->getConfig('panels') as $panelsModule => $modulePanels){
                    if(empty($enabledPanels) || array_key_exists($panelsModule, $enabledPanels)){
                        foreach($modulePanels as $panelName => $panelPath){
                            if(empty($enabledPanels) || empty($enabledPanels[$panelsModule]) || in_array($panelName, $enabledPanels[$panelsModule])){
                                include_once($panelPath);
                            }
                        }
                    }
                }
            echo '</section>';
        }
    }

    //TODO: include_once(LAYOUT_DIR."_loginAttempts.php");
    include_once(LAYOUT_DIR."_systemCondition.php");
    include_once(LAYOUT_DIR."_localizationStatus.php");