<?php
// ADMIN LOGIN
  if( isset($_POST['submitLogin']) ){
    $adminEngine = new adminEngine($database, $localeEngine);
    $isOptIn = (isset($_POST['isOptIn']));
    $loginCallback = $adminEngine->loginAdmin($_POST['email'], $_POST['password'], $isOptIn);

    if($loginCallback === true){
      $adminEngine->adminRedirect();
    }
  } 
  
echo '<!DOCTYPE html>';
 echo '<head>';
  echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
  echo '<meta name="description" content="">';
  echo '<meta name="author" content="ReSs Design">';
  echo '<meta name="robots" content="noindex, nofollow" />'; 
  echo '<meta name="viewport" content="width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0" />';

  echo '<link href="https://fonts.googleapis.com/css?family=Hind:300,400,500,600,700&amp;subset=latin-ext" rel="stylesheet">';
  echo '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">';
  echo '<link href="https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,900,700italic,900italic&subset=latin,latin-ext" rel="stylesheet" type="text/css">';
  
  echo '<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">';  
  echo '<link rel="stylesheet" type="text/css" href="'.relativePath(ADMIN_DIR).'assets/styles/adminLayout.css">';
  
  echo '<title>'.translate("page_title_admin").'</title>';
 echo '</head>';

 echo '<body class="image">';