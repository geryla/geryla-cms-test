<?php
echo '<article class="module">';
  
  $render->header(translate("ad_404_title"), "/".ADMIN, null);

  echo '<section>';                                      
    echo '<div class="moduleWrap">';              			
      echo '<div>'.translate("ad_404_text_up").'</div>';
      echo '<br />';
      echo '<div>'.translate("ad_404_text_down").'</div>';
    echo '</div>'; 
  echo '</section>'; 
  
echo '</article>';