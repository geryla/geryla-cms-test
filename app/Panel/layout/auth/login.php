<?php
// ======= FUNCTIONS 
  // In system HEAD
  $adminEngine = new adminEngine($database, $localeEngine);
  $isOptInValidation = (isset($loginCallback["isOptIn"])) ? true : false;

// ======= CONTENT 
  echo '<div class="loginBox col-md-4 col-md-offset-4">';
    
    /*echo '<div class="absoluteBox">';
      echo '<div class="box">';
        echo '<a href="forgotten-password"><img src="/adminFiles/imgs/iconKey.png" alt="'.translate("authorization-login-forgot").'"></a>';
      echo '</div>';
    echo '</div>';*/

    echo '<div class="content">';
      echo '<h1>'.translate("authorization-login-title").'</h1>';
      echo '<p>'.translate("authorization-login-subtitle").'</p>';
      
      echo '<form id="loginform" action="" method="POST" >';

        if($isOptInValidation){
            if(!empty($loginCallback["user"])){
                echo '<input type="hidden" name="email" value="'.$loginCallback["user"].'" readonly />';
            }else{
                echo '<div class="formRow">';
                    echo '<label>';
                        echo '<div class="placeholder">'.translate("authorization-login-email").':</div>';
                        echo '<input type="email" name="email" value=""  />';
                    echo '</label>';
                echo '</div>';
            }

            echo '<div class="formRow">';
                echo '<label>';
                    echo '<div class="placeholder">'.translate("authorization-login-optInCode").':</div>';
                    echo '<input type="text" name="password" value=""  />';
                echo '</label>';
            echo '</div>';

            echo '<input type="hidden" name="isOptIn" value="true" />';
        }else{
            echo '<div class="formRow">';
              echo '<label>';
                echo '<div class="placeholder">'.translate("authorization-login-email").':</div>';
                echo '<input type="email" name="email" />';
              echo '</label>';
            echo '</div>';

            echo '<div class="formRow">';
              echo '<label>';
                echo '<div class="placeholder">'.translate("authorization-login-password").':</div>';
                echo '<input type="password" name="password" />';
              echo '</label>';
            echo '</div>';
        }

        
        echo '<div class="customCheckbox">';
          //echo '<input type="checkbox" name="remember" class="checkboxStyled" id="remember" />';
          //echo '<label for="remember"><span>'.translate("authorization-login-remember").'</span></label>';
        echo '</div>';
        
        echo '<button name="submitLogin" class="btn">'.translate("authorization-login-login").'</button>';
        
      echo '</form>';
      
    echo '</div>';
    
    if( isset($loginCallback) && !empty($loginCallback["message"]) ){ echo "<div class='loginMessage'>".$loginCallback["message"]."</div>"; }
    echo '<div class="bottomBorder">© '.date('Y').' '.DEVELOP_NAME.'. '.translate("system-build").' '.SYSTEM_VERSION.'</div>';
    
  echo '</div>';