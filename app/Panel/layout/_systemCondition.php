<?php
$moduleErrors = false;

echo '<article class="module">';
    echo '<header><h2>'.translate("systemCondition").'</h2></header>';
    echo '<section>';
        $requiredModules = $modulesController->getData('required');
        if( !empty($requiredModules) ){
            $moduleErrors = true;

            echo '<div class="moduleWarning error">';
                echo translate("error_requiredModules").'<hr width="30%">';
                    foreach($requiredModules as $required){
                        echo '<span><i>'.$required.'</i></span>';
                    }
                echo '</div>';
            echo '<br>';
        }

        if($moduleErrors === false){
            echo '<div class="moduleWarning success">';
                echo translate("success_systemInCondition");
            echo '</div>';
        }

        echo '<div class="toggleConfig"><span><i class="fa fa-angle-double-down"></i>'.translate("systemCondition-title").'</span></div>';
        echo '<div class="configurationInfo">';
            echo '<div>'.translate("systemCondition-serverTime", ["value" => date('d/m/Y H:i')]).'</div><br />';
            echo '<div>'.translate("systemCondition-appMode", ["value" => ((env('APP_IN_PROD'))  ? translate("systemCondition-true") : translate("systemCondition-false"))]).'</div>';

            echo '<div>'.translate("systemCondition-phpVersion", ["value" => PHP_VERSION]).'</div>';
            echo '<div>'.translate("systemCondition-apache", ["value" => $_SERVER['SERVER_SOFTWARE']]).'</div>';
            echo '<div>'.translate("systemCondition-sslEnabled", ["value" => ((SERVER_PROTOCOL === "https") ? translate("systemCondition-true") : translate("systemCondition-false"))]).'</div><br />';

            echo '<div>'.translate("systemCondition-executionTime", ["value" => ini_get('max_execution_time')]).'</div>';
            echo '<div>'.translate("systemCondition-sessionExpireTime", ["value" => getSessionLifeTime()]).'</div><br />';
            echo '<div>'.translate("systemCondition-memoryLimit", ["value" => ini_get('memory_limit')]).'</div>';
            echo '<div>'.translate("systemCondition-maxFileSize", ["value" => MEDIA_MAXFILESIZE]).'</div>';

            echo '<div>'.translate("systemCondition-browserCore", ["value" => $_SERVER["HTTP_USER_AGENT"]]).'</div>';
        echo '</div>';


    echo '</section>';
echo '</article>';