<?php
    echo '<head>';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<meta name="description" content="">';
        echo '<meta name="author" content="ReSs Design">';
        echo '<meta name="robots" content="noindex, nofollow" />';
        echo '<meta name="viewport" content="width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0" />';

        echo '<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;subset=latin-ext" rel="stylesheet">';
        echo '<link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800&amp;subset=latin-ext" rel="stylesheet">';
        echo '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">';
        echo '<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">';
        echo '<link rel="stylesheet" type="text/css" href="'.relativePath(ADMIN_DIR).'assets/styles/colorpicker.css">';
        echo '<link rel="stylesheet" type="text/css" href="'.relativePath(ADMIN_DIR).'assets/styles/adminLayout.css?v=2">';

        foreach( $modulesController->getConfig('paths') as $modulePaths ){
            if( isset($modulePaths["css"]["backend"]) ){
              foreach($modulePaths["css"]["backend"] as $cssFile){
                echo '<link rel="stylesheet" type="text/css" href="'.$cssFile.'?v=2">';
              }
            }
        }

        echo '<script src="https://code.jquery.com/jquery-1.10.2.js"></script>';
        echo '<script type="text/javascript" src="'.relativePath(ADMIN_DIR).'assets/scripts/tinymce/tinymce.min.js"></script>'; // 27.2.2018 - Version 4.7.8
        echo '<script type="text/javascript">
        var requestsUrl = "/'.ADMIN.'/dashboard";
       
        function tinyMceInit(){
        tinymce.init({
              language : "cs",  
              theme : "modern", 
              selector: ".textarea",            
              
              /* Enabled plugins */
              plugins: [
                "visualblocks jbimages advlist autolink autosave link image lists charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "table contextmenu directionality imagetools emoticons template textcolor paste fullpage textcolor"
              ],  
              
              /* Icons in toolbar - HIDDEN: styleselect */       
              toolbar1: " bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist table | forecolor backcolor | cut copy paste",
              toolbar2: " /*formatselect*/ styleselect fontsizeselect | addShortcode jbimages media | link unlink anchor | subscript charmap superscript | removeformat | fullscreen preview code mypluginname",          
                        
              /* Replace pt sizes as px */
              fontsize_formats: "8px 10px 12px 14px 18px 24px 36px",
              allow_conditional_comments: true,
              
              /* Advantage options for images (style, borders, ...) */
              image_advtab: true,
             
              /* Disable relative URL from editor */
                relative_urls : false,
                remove_script_host : false,
                convert_urls : false,
              
              /* Replace formats as HTML5 blocks*/
              style_formats: [
                { title: "Headers", items: [
                  { title: "Header 1", block: "h1" },
                  { title: "Header 2", block: "h2" },
                  { title: "Header 3", block: "h3" },
                  { title: "Header 4", block: "h4" },
                  { title: "Header 5", block: "h5" },
                  { title: "Header 6", block: "h6" }
                ] },
              
                { title: "Blocks", items: [
                  { title: "Blockquote", format: "blockquote" },               
                  { title: "Paragraph", block: "p" },
                  { title: "Div", block: "div" },       
                ] },
                
                { title: "Inline", items: [
                  { title: "Span", inline: "span"}, 
                  { title: "Code", format: "code"}          
                ] },
                
                { title: "Containers", items: [
                  { title: "section", block: "section", wrapper: true, merge_siblings: false },
                  { title: "article", block: "article", wrapper: true, merge_siblings: false },
                  { title: "aside", block: "aside", wrapper: true },
                  { title: "figure", block: "figure", wrapper: true }
                ] }
              ], 
              
              
              /* Create map of colors in FORE and BACK color tool  
                textcolor_map: [
                  "000000", "Black",
                ],
                textcolor_cols: "8",
                textcolor_rows: "5",
              */
              
              /* Show HTML blocks in editor */
              visualblocks_default_state: true,
              end_container_on_empty_block: true,
              
              /* Disable menu bar (File, Edit, Show, Paste, ...) */
              menubar: false,
              
              /* Size of icons */
              toolbar_items_size: "medium",
              
              /* Size of textarea */
              height : 400,
              
              /* Custom shortcode inserter */
              ';$shortCodes->loadShortCodesList($modulesController->getConfig("shortcodes"));echo'             
        });
        }
        tinyMceInit();
        </script>';

        echo '<title>'.$presenter->templateData->title.' | '.translate("page_title_admin").'</title>';
    echo '</head>';
?>