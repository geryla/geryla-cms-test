<?php
echo '<!DOCTYPE html>';
    include_once(LAYOUT_DIR."_header.php");
    echo '<body>';

    echo '<div id="adminWrap">';
        echo '<div id="sidebar">';
            echo '<div class="appInfo">';
                echo '<h1>'.translate("admin-title").'</h1>';
                echo '<span class="build">'.translate("system-build").' '.SYSTEM_VERSION.'</span>';
            echo '</div>';
            include_once(LAYOUT_DIR."_navigation.php");
        echo '</div>';

        echo '<div id="content" style="width: 100%;">';
            echo '<div id="toolbar">';
                echo '<div class="menuToggle"><span class="icon"><i class="fa fa-bars"></i></span></div>';
                echo '<ul class="toolsList">';
                    echo '<li><a class="icon" href="'.(env('PROJECT_REMOTE_URL') ? env('PROJECT_REMOTE_URL') : "/").'" target="_blank"><i class="fa fa-desktop"></i></a></li>';
                    include_once(LAYOUT_DIR."_adminInfo.php");
                    include_once(LAYOUT_DIR."_langSwitch.php");
                echo '</ul>';
            echo '</div>';

            include_once(LAYOUT_DIR."_breadCrumbs.php");

            echo '<div id="container">';
                $filtrationEngine = new filtrationClass($database, $appController);
                $filterToModule = new filterToModules($database, $appController);

                $filtration = [];
                $filtration["activePage"] = ( isset($_GET["page"]) ) ? $_GET["page"] : 0;
                $filtration["limit"] = ( isset($presenter->activeModuleConfig->modulePaginationLimit) && $presenter->activeModuleConfig->modulePaginationLimit > 0 ) ? $presenter->activeModuleConfig->modulePaginationLimit : $filtrationEngine->pageLimit;
                $filtration["moduleLimit"] = ( isset($presenter->activeModuleConfig->modulePaginationLimit) && $presenter->activeModuleConfig->modulePaginationLimit > 0 ) ? $presenter->activeModuleConfig->modulePaginationLimit : 0;
                $filterToModule->init($filtration);
                unset($filtration);

                $render = new adminRender($database, $appController);
                $prefixEngine = new prefixEngine($database);

                require_once(SERVICES_DIR.'DataCompiler.php');
                $dataCompiler = new \Compiler\DataCompiler($database, $appController);

                $dataCompiler->setActiveModuleConfig($presenter->activeModuleConfig);
                $presenter->activeContent->enabledMultilanguage = count($localizationClass->allActiveLanguages);

                include_once($presenter->activeContent->pagePath);

                if($filterToModule->enabledPagination === true ){
                    echo '<div id="pagination">';
                        echo '<div class="wrap">';
                            $filtrationEngine->getAdminPagination($filterToModule->limit, $filterToModule->dataCount, $filterToModule->activePage, false, $_SERVER['REQUEST_URI']);
                        echo '</div>';
                    echo '</div>';
                }

            echo '</div>';

            echo '<div id="contentLoader"><span class="icon"></span></div>';
            include_once(LAYOUT_DIR."_logOutAlert.php");

        echo '</div>';
    echo '</div>';
    include_once(LAYOUT_DIR."_footer.php");

    echo '</body>';
echo '</html>';