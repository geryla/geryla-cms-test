<?php
/* DEFAULT language file - NOT ALLOWED TO EDIT - file can be REPLACED by default file with SYSTEM UPDATE !!!
 * THIS file is FIRST in load order -> If you need to edit texts, you can create own variable in custom lang file
*/

$lang = [];

// BASIC - Page default
$lang["page_title"] = '';
$lang["page_title_admin"] = 'Administrační rozhraní';
$lang["developer_copyright-meta"] = 'ReSs-Design.cz - Tvorba webových stránek a aplikací';
$lang["developer_copyright-anchor"] = 'Developed by <a href="http://www.ress-design.cz" target="_blank">ReSs Design</a>';

// ADMIN - System configuration
$lang["systemCondition"] = "Aktivní kofigurace systémového jádra";
$lang["systemCondition-true"] = "Ano";
$lang["systemCondition-false"] = "Ne";
$lang["systemCondition-title"] = "Podrobné informace";
$lang["systemCondition-memoryLimit"] = "<b>Limit paměti:</b> {value}";
$lang["systemCondition-executionTime"] = "<b>Max. doba provádění scriptu:</b> {value} sekund";
$lang["systemCondition-maxFileSize"] = "<b>Max. velikost souboru:</b> {value}";
$lang["systemCondition-phpVersion"] = "<b>Verze PHP:</b> {value}";
$lang["systemCondition-sslEnabled"] = "<b>Zabezpečení SSL aktivní:</b> {value}";
$lang["systemCondition-browserCore"] = "<b>Typ prohlížeče:</b> {value}";
$lang["systemCondition-databaseConnection"] = "<b>Spojení s MySQL aktivní:</b> {value}";
$lang["systemCondition-appMode"] = "<b>Produkční verze:</b> {value}";
$lang["systemCondition-apache"] = "<b>Verze softwaru serveru:</b> {value}";
$lang["systemCondition-serverTime"] = "<b>Systémový čas:</b> {value}";
$lang["systemCondition-sessionExpireTime"] = "<b>Maximální doba platnosti session:</b> {value} sekund";

// ADMIN - System errors
$lang["error_requiredModules"] = "<b>CHYBA:</b> Některé části systému vykazují problém";
$lang["success_systemInCondition"] = "Modulární části systému v pořádku - <b>Systém běží</b>";
$lang["error-logOut-remainingTime"] = "Do odhlášení zbývá";
$lang["error-logOut-remainingTime-minutes"] = "minut";
$lang["error-logOut-remainingTime-minute"] = "minuty";
$lang["error-logOut-remainingTime-less"] = "méně než minuta";
$lang["error-logOut-extendTime"] = "Prodloužit";

// ADMIN - Logs list
$lang["logAttempts-lastLogin-title"] = "Poslední přihlášení";

// ADMIN - Button names
$lang["save"] = 'Uložit';
$lang["save_and_next"] = 'Uložit a vložit další';
$lang["edit"] = 'Upravit';
$lang["delete"] = 'Smazat';
$lang["translate"] = 'Přeložit';
$lang["submit"] = 'Odeslat';
$lang["change_pass"] = 'Změnit heslo';
$lang["order"] = 'Objednat';
$lang["back"] = 'Zpět';
$lang["next"] = 'Další';
$lang["add"] = 'Přidat';
$lang["remove"] = 'Odebrat';
$lang["preview"] = 'Náhled';
$lang["load"] = 'Načíst';
$lang["show"] = 'Zobrazit';
$lang["menuOpen"] = 'Menu';
$lang["menuClose"] = 'Zavřít';
$lang["download"] = 'Stáhnout';

// ADMIN - Return messages
$lang["admin-create-true"] = "Úspěšně vytvořeno!";
$lang["admin-create-false"] = "Nastala chyba při ukládání!";
$lang["admin-create-prefix"] = "Nelze vytvořit prefix URL!";
$lang["admin-edit-true"] = "Úspěšně aktualizováno!";

// ADMIN - Delete method alert texts
$lang["confirm-delete-head"] = "Mazání položky";
$lang["confirm-delete-text"] = "Opravdu chcete smazat položku?";
$lang["confirm-delete-yes"] = "Ano";
$lang["confirm-delete-no"] = "Ne";

// ADMIN - System basic texts
$lang["system-build"] = 'Verze';
$lang["admin-title"] = 'Administrace';
$lang["show-web"] = 'Zobrazit web';
$lang["logout"] = 'Odhlásit se';

// ADMIN - SYSTEM PAGES - texts
$lang["login"] = 'Přihlášení';
$lang["register"] = 'Registrace';
$lang["forgotten-password"] = 'Zapomenuté heslo';
$lang["dashboard"] = 'Základní přehled';
$lang["notFound"] = 'Nenalezeno';
$lang["object-visibility-no"] = 'Skryto';
$lang["object-visibility-yes"] = 'Zobrazeno';

// ADMIN - AUTHORIZATION - pages - LOGIN
$lang["authorization-login-title"] = "Přihlášení do administrace";
$lang["authorization-login-subtitle"] = "Zadejte svůj e-mail a heslo:";
$lang["authorization-login-email"] = "E-mail";
$lang["authorization-login-password"] = "Heslo";
$lang["authorization-login-optInCode"] = "Kód";
$lang["authorization-login-remember"] = "Zapamatovat";
$lang["authorization-login-forgot"] = "Zapomněl jsem heslo";
$lang["authorization-login-login"] = "Přihlásit";
$lang["authorization-login-send"] = "Odeslat";

// ADMIN - AUTHORIZATION - pages - FORGOTT PASS
$lang["authorization-pass-title"] = "Obnovení hesla";
$lang["authorization-pass-subtitle"] = "Zadejte e-mail pro obnovení:";
$lang["authorization-pass-email"] = "E-mail";
$lang["authorization-pass-send"] = "Obnovit";

// ADMIN - 404 page texts
$lang["ad_404_title"] = 'Copak hledáte? ';
$lang["ad_404_text_up"] = '<b>Požadovaná stránka nebyla nalezena, nebo nemáte dostatečná práva k jejímu zobrazení.</b>';
$lang["ad_404_text_down"] = 'Pokud jste si jistí správností adresy, kontaktujte podporu a zažádejte o přístupová práva.';

// DASHBOARD - ADMIN - Calendar format and translations
$lang["ad-date-sentence"] = "<span class='date'>%s %s. %s %s</span>";
$lang["ad-date-day-1"] = "Pondělí";
$lang["ad-date-day-2"] = "Úterý";
$lang["ad-date-day-3"] = "Středa";
$lang["ad-date-day-4"] = "Čtvrtek";
$lang["ad-date-day-5"] = "Pátek";
$lang["ad-date-day-6"] = "Sobota";
$lang["ad-date-day-7"] = "Neděle";
$lang["ad-date-month-1"] = "leden";
$lang["ad-date-month-2"] = "únor";
$lang["ad-date-month-3"] = "březen";
$lang["ad-date-month-4"] = "duben";
$lang["ad-date-month-5"] = "květen";
$lang["ad-date-month-6"] = "červen";
$lang["ad-date-month-7"] = "červenec";
$lang["ad-date-month-8"] = "srpen";
$lang["ad-date-month-9"] = "září";
$lang["ad-date-month-10"] = "říjen";
$lang["ad-date-month-11"] = "listopad";
$lang["ad-date-month-12"] = "prosinec";

$lang["ad-panels-sortable-dates-today"] = "Dnes";
$lang["ad-panels-sortable-dates-yesterday"] = "Včera";
$lang["ad-panels-sortable-dates-week"] = "Tento týden";
$lang["ad-panels-sortable-dates-month"] = "Tento měsíc";
$lang["ad-panels-sortable-dates-range"] = "Období";

// Tracy debugger - custom panels
$lang["tracyPanel-cache"] = "Optimalizace";
$lang["tracyPanel-cache-table-SQL"] = "SQL dotazy včetně vykreslení";
$lang["tracyPanel-cache-table-group"] = "Skupina";
$lang["tracyPanel-cache-table-simple"] = "Původní";
$lang["tracyPanel-cache-table-cache"] = "Optimalizováno";
$lang["tracyPanel-cache-table-time"] = "Přibližné vykreslení";
$lang["tracyPanel-cache-table-total"] = "Celkem dotazů";
$lang["tracyPanel-cache-regenerate"] = "Regenerovat knihovny >>";
$lang["tracyPanel-logs"] = "SQL Dotazy";
$lang["tracyPanel-logs-table-list"] = "Seznam dotazů";
$lang["tracyPanel-logs-table-error"] = "Chyba";

// EXTENDED multimodule texts - Price taxes and pieces of products
$lang["without_DPH"] = 'bez DPH';
$lang["with_DPH"] = 's DPH';
$lang["pcs"] = 'ks';