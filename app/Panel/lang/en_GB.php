<?php
/* DEFAULT language file - NOT ALLOWED TO EDIT - file can be REPLACED by default file with SYSTEM UPDATE !!!
  * THIS file is FIRST in load order -> If you need to edit texts, you can create your own variable in custom lang file
*/

$lang = [];

// BASIC - Page default
$lang["page_title"] = '';
$lang["page_title_admin"] = 'Admin Interface';
$lang["developer_copyright-meta"] = 'ReSs-Design.cz - Development of websites and applications';
$lang["developer_copyright-anchor"] = 'Developed by <a href="http://www.ress-design.cz" target="_blank">ReSs Design</a>';

// ADMIN - System configuration
$lang["systemCondition"] = "Active System Kernel Configuration";
$lang["systemCondition-true"] = "Yes";
$lang["systemCondition-false"] = "No";
$lang["systemCondition-title"] = "Detailed information";
$lang["systemCondition-memoryLimit"] = "<b>Memory Limit:</b> {value}";
$lang["systemCondition-executionTime"] = "<b>Maximum script execution time:</b> {value} seconds";
$lang["systemCondition-maxFileSize"] = "<b>Max file size:</b> {value}";
$lang["systemCondition-phpVersion"] = "<b>PHP Version:</b> {value}";
$lang["systemCondition-sslEnabled"] = "<b>SSL security enabled:</b> {value}";
$lang["systemCondition-browserCore"] = "<b>Browser Type:</b> {value}";
$lang["systemCondition-databaseConnection"] = "<b>MySQL connection active:</b> {value}";
$lang["systemCondition-appMode"] = "<b>Production Version:</b> {value}";
$lang["systemCondition-apache"] = "<b>Server software version:</b> {value}";
$lang["systemCondition-serverTime"] = "<b>System Time:</b> {value}";
$lang["systemCondition-sessionExpireTime"] = "<b>Maximum session expiration time:</b> {value} seconds";

// ADMIN - System errors
$lang["error_requiredModules"] = "<b>ERROR:</b> Some parts of the system are showing a problem";
$lang["success_systemInCondition"] = "Modular system parts OK - <b>System running</b>";
$lang["error-logOut-remainingTime"] = "Logout remaining";
$lang["error-logOut-remainingTime-minutes"] = "minutes";
$lang["error-logOut-remainingTime-minute"] = "minutes";
$lang["error-logOut-remainingTime-less"] = "less than a minute";
$lang["error-logOut-extendTime"] = "Extend";

// ADMIN - Logs list
$lang["logAttempts-lastLogin-title"] = "Last Login";

// ADMIN - Button names
$lang["save"] = 'Save';
$lang["save_and_next"] = 'Save and Paste Next';
$lang["edit"] = 'Edit';
$lang["delete"] = 'Delete';
$lang["translate"] = 'Translate';
$lang["submit"] = 'Submit';
$lang["change_pass"] = 'Change password';
$lang["order"] = 'Order';
$lang["back"] = 'Back';
$lang["next"] = 'Next';
$lang["add"] = 'Add';
$lang["remove"] = 'Remove';
$lang["preview"] = 'Preview';
$lang["load"] = 'Load';
$lang["show"] = 'Show';
$lang["menuOpen"] = 'Menu';
$lang["menuClose"] = 'Close';
$lang["download"] = 'Download';

// ADMIN - Return messages
$lang["admin-create-true"] = "Created successfully!";
$lang["admin-create-false"] = "There was an error saving!";
$lang["admin-create-prefix"] = "Unable to create URL prefix!";
$lang["admin-edit-true"] = "Updated successfully!";

// ADMIN - Delete method alert texts
$lang["confirm-delete-head"] = "Deleting Head";
$lang["confirm-delete-text"] = "Are you sure you want to delete the item?";
$lang["confirm-delete-yes"] = "Yes";
$lang["confirm-delete-no"] = "No";

// ADMIN - System basic texts
$lang["system-build"] = 'Version';
$lang["admin-title"] = 'Administration';
$lang["show-web"] = 'Show Web';
$lang["logout"] = 'Logout';

// ADMIN - SYSTEM PAGES - texts
$lang["login"] = 'Login';
$lang["register"] = 'Register';
$lang["forgotten-password"] = 'Forgotten password';
$lang["dashboard"] = 'Basic Overview';
$lang["notFound"] = 'Not Found';
$lang["object-visibility-no"] = 'Hidden';
$lang["object-visibility-yes"] = 'Shown';

// ADMIN - AUTHORIZATION - pages - LOGIN
$lang["authorization-login-title"] = "Login to administration";
$lang["authorization-login-subtitle"] = "Enter your email and password:";
$lang["authorization-login-email"] = "Email";
$lang["authorization-login-password"] = "Password";
$lang["authorization-login-remember"] = "Remember";
$lang["authorization-login-forgot"] = "I forgot my password";
$lang["authorization-login-login"] = "Login";
$lang["authorization-login-send"] = "Send";

// ADMIN - AUTHORIZATION - pages - FORGOTT PASS
$lang["authorization-pass-title"] = "Password Recovery";
$lang["authorization-pass-subtitle"] = "Enter your renewal email:";
$lang["authorization-pass-email"] = "Email";
$lang["authorization-pass-send"] = "Renew";

// ADMIN - 404 page texts
$lang["ad_404_title"] = 'What are you looking for? ';
$lang["ad_404_text_up"] = '<b>The requested page was not found or you do not have sufficient rights to view it.</b>';
$lang["ad_404_text_down"] = 'If you are sure that the address is correct, contact support and request access rights.';

// DASHBOARD - ADMIN - Calendar format and translations
$lang["ad-date-sentence"] = "<span class='date'>%s %s. %s %s</span>";
$lang["ad-date-day-1"] = "Mondayand";
$lang["ad-date-day-2"] = "Tuesday";
$lang["ad-date-day-3"] = "Wednesday";
$lang["ad-date-day-4"] = "Thursday";
$lang["ad-date-day-5"] = "Friday";
$lang["ad-date-day-6"] = "Saturday";
$lang["ad-date-day-7"] = "Sunday";
$lang["ad-date-month-1"] = "January";
$lang["ad-date-month-2"] = "February";
$lang["ad-date-month-3"] = "March";
$lang["ad-date-month-4"] = "April";
$lang["ad-date-month-5"] = "May";
$lang["ad-date-month-6"] = "June";
$lang["ad-date-month-7"] = "July";
$lang["ad-date-month-8"] = "August";
$lang["ad-date-month-9"] = "September";
$lang["ad-date-month-10"] = "October";
$lang["ad-date-month-11"] = "November";
$lang["ad-date-month-12"] = "December";

$lang["ad-panels-sortable-dates-today"] = "Today";
$lang["ad-panels-sortable-dates-yesterday"] = "Yesterday";
$lang["ad-panels-sortable-dates-week"] = "This week";
$lang["ad-panels-sortable-dates-month"] = "This month";
$lang["ad-panels-sortable-dates-range"] = "Period";

// Tracy debugger - custom panels
$lang["tracyPanel-cache"] = "Optimization";
$lang["tracyPanel-cache-table-SQL"] = "SQL queries including rendering";
$lang["tracyPanel-cache-table-group"] = "Group";
$lang["tracyPanel-cache-table-simple"] = "Original";
$lang["tracyPanel-cache-table-cache"] = "Optimized";
$lang["tracyPanel-cache-table-time"] = "Approximate rendering";
$lang["tracyPanel-cache-table-total"] = "Total Queries";
$lang["tracyPanel-cache-regenerate"] = "Regenerate Libraries >>";
$lang["tracyPanel-logs"] = "SQL Queries";
$lang["tracyPanel-logs-table-list"] = "Query List";
$lang["tracyPanel-logs-table-error"] = "Error";

// EXTENDED multimodule texts - Price taxes and pieces of products
$lang["without_VAT"] = 'without VAT';
$lang["with_VAT"] = 'with VAT';
$lang["pcs"] = 'pcs';