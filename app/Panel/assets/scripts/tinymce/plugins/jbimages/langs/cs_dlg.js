/**
 * Justboil.me - a TinyMCE image upload plugin
 * jbimages/langs/cs_dlg.js
 *
 * Released under Creative Commons Attribution 3.0 Unported License
 *
 * License: http://creativecommons.org/licenses/by/3.0/
 * Plugin info: http://justboil.me/
 * Author: Viktor Kuzhelnyi
 *
 * Version: 2.3 released 23/06/2013
 */

 tinyMCE.addI18n('cs.jbimages_dlg',{
	title : 'Nahrát obrázek z počítače',
	select_an_image : 'Vybrat obrázek',
	upload_in_progress : 'Nahrávání',
	upload_complete : 'Nahrávání kompletní',
	upload : 'Nahrát',
	longer_than_usual : 'Trvá to déle než je obvyklé.',
	maybe_an_error : 'Vyskytla se chyba.',
	view_output : 'Zobrazit výstup scriptu',
	
	lang_id : 'czech' /* php-side language files are in: ci/application/language/{lang_id}; and in ci/system/language/{lang_id} */
});
