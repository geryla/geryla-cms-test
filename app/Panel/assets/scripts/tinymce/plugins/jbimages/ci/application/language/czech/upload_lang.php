<?php

$lang['upload_userfile_not_set'] = "Nepodařilo se najít příspěvek s proměnnou s názvem userfile.";
$lang['upload_file_exceeds_limit'] = "Nahraný soubor přesahuje maximální povolenou velikost.";
$lang['upload_file_exceeds_form_limit'] = "Nahraný soubor přesahuje maximální povolenou velikost.";
$lang['upload_file_partial'] = "Soubor byl nahrán jen částečně.";
$lang['upload_no_temp_directory'] = "Dočasná složka chybí.";
$lang['upload_unable_to_write_file'] = "Soubor nelze zapsat na disk.";
$lang['upload_stopped_by_extension'] = "Upload souboru byl zastaven kvůli příponě.";
$lang['upload_no_file_selected'] = "Nevybrali jste soubor, který chcete nahrát.";
$lang['upload_invalid_filetype'] = "Daný typ, jež se snažíte nahrát není povolen.";
$lang['upload_invalid_filesize'] = "Soubor, který se pokoušíte nahrát, je větší, než je povolená velikost.";
$lang['upload_invalid_dimensions'] = "Obrázek, který se pokoušíte nahrát přesahuje maximální výšku nebo šířku.";
$lang['upload_destination_error'] = "Byl zjištěn problém při pokusu přesunout nahraný soubor do cílového místa určení.";
$lang['upload_no_filepath'] = "Cesta k souboru se nezdá platná.";
$lang['upload_no_file_types'] = "Nezvolili jste žádné povolené typy souborů.";
$lang['upload_bad_filename'] = "Název souboru, který nahráváte již na serveru existuje.";
$lang['upload_not_writable'] = "Složka pro nahrávání se nezdá být zapisovatelná.";


/* End of file upload_lang.php */