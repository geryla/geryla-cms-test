<?php

$lang['imglib_source_image_required'] = "Musíte zadat zdrojový obraz v předvolbách.";
$lang['imglib_gd_required'] = "Je zapotřebí image knihovna GD pro tuto funkci.";
$lang['imglib_gd_required_for_props'] = "Váš server musí podporovat image knihovny GD s cílem určit vlastnosti obrazu.";
$lang['imglib_unsupported_imagecreate'] = "Váš server nepodporuje funkci GD potřebné pro zpracování tohoto typu obrazu.";
$lang['imglib_gif_not_supported'] = "GIF není povolený typ souborus.  Použíjte JPG nebo PNG typ.";
$lang['imglib_jpg_not_supported'] = "JPG není povolený typ souboru.";
$lang['imglib_png_not_supported'] = "PNG není povolený typ souboru.";
$lang['imglib_jpg_or_png_required'] = "Velikost protokolu obrazu je určena v předvolbách a pracuje pouze s typy obrázků JPEG nebo PNG.";
$lang['imglib_copy_error'] = "Došlo k chybě při pokusu o nahrazení souboru. Ujistěte se, že do adresáře zapisovat.";
$lang['imglib_rotate_unsupported'] = "Rotace obrazu nezdá být podporována serverem.";
$lang['imglib_libpath_invalid'] = "Cesta k obrazu knihovny není správná. Prosím, nastavte správnou cestu v preferencích.";
$lang['imglib_image_process_failed'] = "Zpracování obrazu se nezdařilo. Ověřte, zda váš server podporuje zvolený protokol, a že cesta k obrazu knihovny je správná.";
$lang['imglib_rotation_angle_required'] = "Úhel natočení je povinné pro otočení obrazu.";
$lang['imglib_writing_failed_gif'] = "Typ souboru GIF.";
$lang['imglib_invalid_path'] = "Cesta k obrázku není správná.";
$lang['imglib_copy_failed'] = "Zobrazení kopie se nezdařilo.";
$lang['imglib_missing_font'] = "Nelze najít písmo pro použítí.";
$lang['imglib_save_failed'] = "Nelze uložit obrázek. Ujistěte se, že do adresáře souborů lze zapisovat.";


/* End of file imglib_lang.php */