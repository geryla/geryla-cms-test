function adminLoader(time = 3000){
    time = (time === undefined) ? 3000 : time;

    if(time === true){
        $("#contentLoader").css('display', 'flex');
    }else if(time === false){
        $("#contentLoader").hide();
    }else{
        $("#contentLoader").css('display', 'flex');
        setTimeout(function(){ $("#contentLoader").hide(); }, time);
    }
}
function addParameter(url, parameterName, parameterValue, atStart){
    replaceDuplicates = true;
    if(url.indexOf('#') > 0){
        var cl = url.indexOf('#');
        urlhash = url.substring(url.indexOf('#'),url.length);
    } else {
        urlhash = '';
        cl = url.length;
    }
    sourceUrl = url.substring(0,cl);

    var urlParts = sourceUrl.split("?");
    var newQueryString = "";

    if (urlParts.length > 1)
    {
        var parameters = urlParts[1].split("&");
        for (var i=0; (i < parameters.length); i++)
        {
            var parameterParts = parameters[i].split("=");
            if (!(replaceDuplicates && parameterParts[0] == parameterName))
            {
                if (newQueryString == "")
                    newQueryString = "?";
                else
                    newQueryString += "&";
                newQueryString += parameterParts[0] + "=" + (parameterParts[1]?parameterParts[1]:'');
            }
        }
    }
    if (newQueryString == "")
        newQueryString = "?";

    if(atStart){
        newQueryString = '?'+ parameterName + "=" + parameterValue + (newQueryString.length>1?'&'+newQueryString.substring(1):'');
    } else {
        if (newQueryString !== "" && newQueryString != '?')
            newQueryString += "&";
        newQueryString += parameterName + "=" + (parameterValue?parameterValue:'');
    }
    return urlParts[0] + newQueryString + urlhash;
};

$(document).on('ready ajaxComplete', function () {
    /* DATE and TIME picker */
// Date
    $.datepicker.regional['cs'] = {
        closeText: 'Zavřít',
        prevText: 'Předchozí',
        nextText: 'Další',
        currentText: 'Nyní',
        monthNames: ['Leden','Únor','Březen','Duben','Květen','Červen', 'Červenec','Srpen','Září','Říjen','Listopad','Prosinec'],
        monthNamesShort: ['Le','Ún','Bř','Du','Kv','Čn', 'Čc','Sr','Zá','Ří','Li','Pr'],
        dayNames: ['Neděle','Pondělí','Úterý','Středa','Čtvrtek','Pátek','Sobota'],
        dayNamesShort: ['Ne','Po','Út','St','Čt','Pá','So',],
        dayNamesMin: ['Ne','Po','Út','St','Čt','Pá','So'],
        weekHeader: 'Sm',
        dateFormat: 'dd.mm.yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['cs']);
// Time
    $.timepicker.regional['cs'] = {
        timeOnlyTitle: 'Pouze čas',
        timeText: 'Čas',
        hourText: 'Hodiny',
        minuteText: 'Minuty',
        secondText: 'Sekundy',
        millisecText: 'Milisekundy',
        timezoneText: 'Časová zóna',
        currentText: 'Nyní',
        closeText: 'Zavřít',
        timeFormat: 'HH:mm',
        amNames: ['AM', 'A'],
        pmNames: ['PM', 'P'],
        isRTL: false
    };
    $.timepicker.setDefaults($.timepicker.regional['cs']);

    $('#datepicker, .datepicker').datetimepicker();
    $('#datepickersimple, .datepickersimple').datepicker({
        beforeShow: function (selectedDate) {
            let minDate = $(this).attr('min');
            if (minDate && minDate !== undefined) {
                // Convert 'yyyy-mm-dd' to 'dd.mm.yy'
                let parts = minDate.split('-');
                minDate = parts[2] + '.' + parts[1] + '.' + parts[0];
                $(this).datepicker('option', 'minDate', minDate);
            }
        }
    });

/* ===== SORTING objects === */
    $('.sortableWrap').each(function(){
        let axis = ($(this).attr("data-axis")) ? $(this).attr("data-axis") : 'y';

        $(this).sortable({
            axis: axis,
            opacity: 0.7,
            helper: function(e, ui) {
                ui.children().each(function() { $(this).width($(this).width()); }); return ui;
            },
            handle: '.handle',
            update: function(event, ui) {
                let name = $(this).attr("data-name");
                let list = $(this).sortable('toArray').toString();
                let template = $(this).find('.imageBox').first().attr('data-template');

                let sortOption = $('form.filtration select[name="order"]').find(':selected');
                let isDescending = sortOption.length > 0 ? sortOption.val().includes('desc') : false;
                if (isDescending) {
                    list = list.split(',').reverse().join(',');
                }

                $.ajax({
                    url: requestsUrl,
                    type: 'POST',
                    data: {requestName: 'sorting', admin: true, response: 'json', data: {list:list, type:name, template: template}}
                });
            }
        });
    });

/* ===== GROUPS/CATEGORIES list === */
    $(".categoryCheck li.active").each(function() {
        let activeItem = $(this);
        $(activeItem).parents("ul").each(function() {
            $(this).show();
            $(this).closest("li").find("i").first().removeClass("fa-plus-square-o");
            $(activeItem).find("i").first().removeClass("fa-plus-square-o");
            $(activeItem).find(".subcategory").first().show();
        });
    });
    $(".categoryCheck li i").on('click', function(){
        if(!$(this).hasClass("fa-square-o")){
            var result = $(this).parent('li').attr('id');
            $(this).toggleClass("fa-plus-square-o");
            $(".categoryCheck li#"+result+" > ul").toggle();
        }
    });

    $(document).on('click', '.js-passwordSwitch', function(){
        let passwordInput = $(this).closest('td').find('input.password');
        if (passwordInput.attr("type") === "password") {
            passwordInput.attr("type", "text");
        } else {
            passwordInput.attr("type", "password");
        }
    });
});
$(window).load(function() {
/* Navigation menu */ 
    $(".navigation .arrow").click(function(){
      var isActive = ($(this).parent('li').hasClass("active")) ? true : false;
      $(".navigation .link").removeClass("active");
      (isActive === true) ? $(this).parent('li').removeClass("active") : $(this).parent('li').addClass("active");
    });

/* Navigation menu */
    if($("body").width() <= 1024){
        $("#sidebar").addClass("hidden");
    }
    $(".menuToggle .icon").click(function(){
        var isHidden = ($("#sidebar").hasClass("hidden")) ? true : false;
        adminLoader(true);

        (Cookies.get('adminNav')) ? Cookies.remove('adminNav') : Cookies.set('adminNav', 'true');
        (isHidden === true) ?
        $("#sidebar").show("slide", { direction: "left" }, 1000).removeClass("hidden") && $("#content").css({'width':'100%'}):
        $("#sidebar").hide("slide", { direction: "left" }, 1000).addClass("hidden") && $("#content").css({'width':'100%'});

        adminLoader(1000);
    });
    if(Cookies.get('adminNav')){
        $("#sidebar").hide().addClass("hidden");
        $("#content").css({'width':'100%'});
    }

/* Contentbox tabs */
    var toggleBoxVisible = $(".moduleWrap .optionBox .boxOption").length;
    var toggleBoxCookie = Cookies.get('tabToogle');
    if(toggleBoxVisible == 1){
        var toggleBoxClass = (toggleBoxCookie !== undefined) ? toggleBoxCookie : "basic";
        $(".contentBox .boxContent#"+toggleBoxClass).toggle();
        $(".optionBox .boxOption .tab").removeClass("active");
        $(".optionBox .boxOption .tab#"+toggleBoxClass).addClass("active");
    }else{
        Cookies.remove('tabToogle');
    }
    $(".optionBox .boxOption .tab").click(function(){
        var result = $(this).attr('id');
        $( ".contentBox .boxContent").hide();
        $( ".contentBox .boxContent#"+result ).toggle();
        Cookies.set('tabToogle', result);

        if( !$(this).hasClass("active") ){
            $(".optionBox .boxOption .tab").removeClass("active");
            $(this).addClass("active");
        }
    });

/* Lang switch */
    $("#toolbar .dropdown .dropdown-label").click(function(){
        $(this).parent('.dropdown').toggleClass("collapsed");
    });
    function removeLangParameter(parameter) {
        var url = window.location.href;
        var urlparts= url.split('?');

        if (urlparts.length>=2) {
            var prefix= encodeURIComponent(parameter)+'=';
            var pars= urlparts[1].split(/[&;]/g);

            //reverse iteration as may be destructive
            for (var i= pars.length; i-- > 0;) {
                //idiom for string.startsWith
                if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                    pars.splice(i, 1);
                }
            }

            url = urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : "");
            window.history.pushState({ path: url }, '', url);
        }
    }
    removeLangParameter("setAdminLang");

/* Generate URL*/
    $("#generateUrl").click(function(){
        let url = $("#prettyTitle").val();
        $.ajax({
            url: requestsUrl,
            type: 'POST',
            data: {requestName: 'prettyUrl', admin: true, response: 'json', data: {url:url}},
            success: function(data) {
                $('#prettyUrl').val(JSON.parse(data));
            }
        });
    });

/* Hide notification */
    $("#notification #closeAlert").click(function(){
        $('#notification').slideUp();
        setTimeout(function() { $('#notification').remove(); }, 2500);
    });

/* ===== DELETE  === */
    $(document).on('click', '.deleteContent', function(e) {
        var type = $(this).attr("name");
        var objectId = $(this).attr("data");

        var confirmHead = $(this).attr("confirm-head");
        var confirmText = $(this).attr("confirm-text");
        var confirmYes = $(this).attr("confirm-yes");
        var confirmNo = $(this).attr("confirm-no");

        $('<div></div>').appendTo('body').html('<div><h6>'+confirmText+'</h6></div>')
        .dialog({
            modal: true,
            title: confirmHead,
            zIndex: 10000,
            autoOpen: true,
            width: 'auto',
            resizable: false,
            buttons:[
            {
              text: confirmYes,
              click: function() {
                adminLoader(true);
                  $.ajax({
                      url: requestsUrl,
                      type: 'POST',
                      data: {requestName: 'deleting', admin: true, response: 'json', data: {id:objectId, type:type}},
                      success: function(data) {
                          location.reload(true);
                      }
                  });
              },
              'class': 'agreeButton'
            },
            {
              text: confirmNo,
              click: function() {
                $(this).dialog("close");
              },
              'class': 'closeButton'
            }
            ],
            close: function (event, ui) {
              $(this).remove();
            }
        });
    });
  
/* ===== VISIBILITY  === */
    $(".visibility").click(function(){
        var type = $(this).attr("name");
        var objectId = $(this).attr("data");

        $.ajax({
            url: requestsUrl,
            type: 'POST',
            data: {requestName: 'visibility', admin: true, response: 'dom', data: {id:objectId, type:type}},
            success: function(data) {
                $('#v_'+objectId).html(data);
            }
        });
    });

/* ===== PRICES COUNTER - count price with tax  === */
    function countPriceWithTax(element){
        var parentWrap = $(element).closest(".rowBox");
        var taxValue = parseInt($('.taxValue').val());
        var priceValue = parseFloat($(element).val().replace(",", "."));

        if(taxValue && taxValue != 0 && priceValue > 0){
            var defaultNoTax = $("#toolbar .langSwitcher").data('defaultnotax');
            var taxDecimal = (100 + taxValue) / 100;
            var priceWithTax = (defaultNoTax === true) ?  priceValue * taxDecimal :  priceValue / taxDecimal;

            if($(parentWrap).find(".priceHelper").length > 0){
                $(parentWrap).find(".priceHelper").text(priceWithTax);
            }else{
                $(parentWrap).append("<span class='priceHelper'>"+priceWithTax+"</span>");
            }
        }else{
            $(parentWrap).find(".priceHelper").remove();
        }
    }
    $(document).on('keyup', '.priceValue', function() {
        countPriceWithTax($(this));
    });
    $(".priceValue").each(function() {
        countPriceWithTax($(this));
    });

/* ==== COLORPICKER === */
    $(".colorPick").each(function() {
        $(this).find('input').after('<span class="colorW" style="background-color:'+$(this).find("input").val()+'"></span>');
    });
    $('.colorPick > input').ColorPicker({
        onBeforeShow: function () {
            $(this).ColorPickerSetColor(this.value);
        },
        onSubmit: function(hsb, hex, rgb, el) {
            $(el).val('#' + hex);
            $(el).closest('.colorPick').find('.colorW').css('background-color', '#' + hex);
            $(el).ColorPickerHide();
        }
    }).bind('keyup', function(){
        $(this).ColorPickerSetColor(this.value);
    });

/* ====== BUTTON SELECT - with value redirect as GET parameter ===*/
    $(document).on('click', '.toggleConfig', function() {
        $(this).closest("section").find(".configurationInfo").toggle();
        $(this).toggleClass("active");
    });

/* === PANEL FILTER ==== */
    $("#js-panel_filter").on('click', 'label', function() {
        $(this).closest("form").submit();
    });

});