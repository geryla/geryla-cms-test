<?php
if(isExisting('list', $request["data"]) && isExisting('type', $request["data"])){
    $requestSuccess = false;

    switch ($request["data"]["type"]) {
        case "postsCategory":
            $table = databaseTables::getTable('contentEditor', 'categoryTable');
            $moduleAction = "contentEditorCateg";
            break;
        case "postsGroup":
            $table = databaseTables::getTable('contentEditor', 'groupTable');
            $moduleAction = "contentEditorGroup";
            break;
        case "postsPost":
            $table = databaseTables::getTable('contentEditor', 'postTable');
            $moduleAction = "contentEditorPost";
            break;
        case "slideshow":
            $table = databaseTables::getTable('slideshow', 'slideshowTable');
            $moduleAction = "slideshow";
            break;
        case "contactBox":
            $table = databaseTables::getTable('contactBoxes', 'contacts');
            $moduleAction = "contactBox";
            break;
        case "mediaManager":
            $table = databaseTables::getTable('mediaManager', 'files');
            $moduleAction = "mediaFiles";
            break;
        case "gallery":
            $table = databaseTables::getTable('gallery', 'galleries');
            $moduleAction = "gallery";
            break;
        case "courses":
            $table = databaseTables::getTable('courses', 'coursesTable');
            $moduleAction = "course";
            break;
        case "courseLesson":
            $table = databaseTables::getTable('courses', 'lessonTable');
            $moduleAction = "lesson";
            break;
        case "courseSentence":
            $table = databaseTables::getTable('courses', 'sentenceTable');
            $moduleAction = "sentence";
            break;
        case "deliveryMethods":
            $table = databaseTables::getTable('deliveries', 'deliveryTable');
            $moduleAction = "deliveryMethods";
            break;
        case "paymentMethods":
            $table = databaseTables::getTable('payments', 'paymentTable');
            $moduleAction = "paymentMethods";
            break;
        case "productCategory":
            $table = databaseTables::getTable('products', 'categoryTable');
            $moduleAction = "productCategories";
            break;
        case "product":
            $table = databaseTables::getTable('products', 'productTable');
            $moduleAction = "product";
            break;
        case "productVariant":
            $table = databaseTables::getTable('productVariants', 'variants');
            $moduleAction = "productVariants";
            break;
        case "productParams":
            $table = databaseTables::getTable('products', 'crossParamTable');
            $moduleAction = "productParams";

            $customId = "valueId";
            break;
        case "schedule":
            $table = databaseTables::getTable('settings', 'scheduler');
            $moduleAction = "schedule";
            break;
        default:
            return false;
    }

    $list = explode(',' , $request["data"]["list"]);
    $orderNum = 1 ;
    foreach($list as $objectId) {
        if($objectId != 0){
            $order = $requestsController->sortItems($objectId, $table, $orderNum, ((isset($customId)) ? $customId : "id") );
            $orderNum++ ;
            $requestSuccess = true;
        }

        if($request["data"]["type"] === 'mediaManager' && !empty($request["data"]["template"])){
            $mediaCache = new MediaCache($database, $appController);
            $mediaCache->deleteCache($request["data"]["template"], true);
        }
    }

    if($requestSuccess === true){
        $systemLogger->createLog($_SESSION["languages"]["adminLang"], $request["data"]["type"], "sorting", 0, $_SESSION["security"]["adminLogin"]["logIn"]);
    }
}