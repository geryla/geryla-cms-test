<?php
    if( isExisting('url', $request["data"]) ){
        $urlCreator = new prefixEngine($database);
        $prettyUrl = $urlCreator->title2pagename($request["data"]["url"]);

        return $prettyUrl;
    }

    return false;