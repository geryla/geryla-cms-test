<?php
if(isExisting('id', $request["data"]) && isExisting('type', $request["data"])){
    $setVisibility = false;
    
    switch ($request["data"]["type"]) {
        case "templates":
            $moduleName = "templates";
            $templateClass = new templateClass($database, $appController);
            $setVisibility = $requestsController->switchVisibility($request["data"]["id"], $templateClass->templatesTable);
            break;
        case "language":
            $moduleName = "localization";
            $setVisibility = $requestsController->switchVisibility($request["data"]["id"], $localizationClass->languagesTable);
            break;
        case "posts":
            $moduleName = "contentEditor";
            $moduleCMS = new cmsPost($database, $appController);
            $setVisibility = $requestsController->switchVisibility($request["data"]["id"], $moduleCMS->postsTable);
            break;
        case "postCategory":
            $moduleName = "contentEditor";
            $moduleCMS = new cmsCategories($database, $appController);
            $setVisibility = $requestsController->switchVisibility($request["data"]["id"], $moduleCMS->categoriesTable);
            break;
        case "postGroup":
            $moduleName = "contentEditor";
            $moduleCMS = new cmsGroups($database, $appController);
            $setVisibility = $requestsController->switchVisibility($request["data"]["id"], $moduleCMS->groupsTable);
            break;
        case "slideshow":
            $moduleName = "slideshow";
            $slideShow = new moduleSlider($database, $appController);
            $setVisibility = $requestsController->switchVisibility($request["data"]["id"], $slideShow->slidesTable);
            break;
        case "slideshowGroup":
            $moduleSlider = new moduleSlider($database, $appController);
            $setVisibility = $requestsController->switchVisibility($request["data"]["id"], $moduleSlider->slidesGroupsTable);

            $true = ($setVisibility === false) ? false : true;
            $moduleAction = "slideshow";
            break;
        case "contactBox":
            $moduleName = "contactBox";
            $contactBoxes = new contactBoxes($database, $appController);
            $setVisibility = $requestsController->switchVisibility($request["data"]["id"], $contactBoxes->headTable);
            break;
        case "gallery":
            $moduleName = "gallery";
            $gallery = new moduleGallery($database, $appController);
            $setVisibility = $requestsController->switchVisibility($request["data"]["id"], $gallery->galleryTable);
            break;
        case "contactForm":
            $moduleName = "contactForm";
            $contactForm = new contactForm($database, $appController);
            $setVisibility = $requestsController->switchVisibility($request["data"]["id"], $contactForm->headTableName);
            break;
        case "courses":
            $moduleName = "courses";
            $courses = new moduleCourses($database, $appController);
            $setVisibility = $requestsController->switchVisibility($request["data"]["id"], $courses->coursesTable);
            break;
        case "courseLesson":
            $moduleName = "courses";
            $courses = new moduleCourses($database, $appController);
            $lessons = new moduleCoursesLesson($courses);
            $setVisibility = $requestsController->switchVisibility($request["data"]["id"], $lessons->lessonTable);
            break;
        case "courseSentence":
            $moduleName = "courses";
            $courses = new moduleCourses($database, $appController);
            $lessons = new moduleCoursesLesson($courses);
            $sentence = new moduleCoursesSentence($lessons);
            $setVisibility = $requestsController->switchVisibility($request["data"]["id"], $sentence->sentenceTable);
            break;
        case "deliveryMethods":
            $moduleName = "deliveryMethods";
            $delivery = new deliveryMethods($database, $appController);
            $setVisibility = $requestsController->switchVisibility($request["data"]["id"], $delivery->dataTable);
            break;
        case "paymentMethods":
            $moduleName = "paymentMethods";
            $payment = new paymentMethods($database, $appController);
            $setVisibility = $requestsController->switchVisibility($request["data"]["id"], $payment->dataTable);
            break;
        case "orderStatus":
            $moduleName = "orders";
            $orders = new moduleOrders($database, $appController);
            $setVisibility = $requestsController->switchVisibility($request["data"]["id"], $orders->orderStatus);
            break;
        case "discountCode":
            $moduleName = "discounts";
            $discount = new DiscountsModule($database, $appController);
            $setVisibility = $requestsController->switchVisibility($request["data"]["id"], $discount->discountsTable);
            break;
        case "productCategory":
            $moduleName = "products";
            $category = new categoryClass($database, $appController);
            $setVisibility = $requestsController->switchVisibility($request["data"]["id"], $category->dataTable);
            break;
        case "productManufacturer":
            $moduleName = "products";
            $manufacturerClass = new manufacturerClass($database, $appController);
            $setVisibility = $requestsController->switchVisibility($request["data"]["id"], $manufacturerClass->manufacturerTable);
            break;
        case "productSupplier":
            $moduleName = "products";
            $supplierClass = new supplierClass($database, $appController);
            $setVisibility = $requestsController->switchVisibility($request["data"]["id"], $supplierClass->supplierTable);
            break;
        case "product":
            $moduleName = "products";
            $productsClass = new productsClass($database, $appController);
            $setVisibility = $requestsController->switchVisibility($request["data"]["id"], $productsClass->dataTable);
            break;
        case "productVariant":
            $moduleName = "products";
            $productVariants = new productVariants($database, $appController);
            $setVisibility = $requestsController->switchVisibility($request["data"]["id"], $productVariants->variantsTable);
            break;
        case "redirects":
            $moduleName = "templates";
            $redirect = new redirectedUrls($database);
            $setVisibility = $requestsController->switchVisibility($request["data"]["id"], $redirect->redirectsTable);
            break;
        case "reservationGroup":
            $moduleName = "reservations";
            $reservations = new reservationsClass($database);
            $setVisibility = $requestsController->switchVisibility($request["data"]["id"], $reservations->groupsTable);
            break;
        case "loyaltyLevel":
            $moduleName = "loyaltyProgram";
            $loyaltyProgram = new loyaltyProgram($database, $appController);
            $setVisibility = $requestsController->switchVisibility($request["data"]["id"], $loyaltyProgram->tableLevels);
            break;
        case "xmlFeed":
            $moduleName = "xmlFeeds";
            $xmlFeeds = new xmlFeedsModule($database, $appController);
            $setVisibility = $requestsController->switchVisibility($request["data"]["id"], $xmlFeeds->headTable);
            break;
        case "schedule":
            $moduleName = "settings";
            $scheduler = new ScheduleController($database, $appController);
            $setVisibility = $requestsController->switchVisibility($request["data"]["id"], $scheduler->schedulesTable);
            break;
        case "orderReview":
            $reviews = new ordersReviews($database);
            $setVisibility = $requestsController->switchVisibility($request["data"]["id"], $reviews->reviewsTable);

            $true = ($setVisibility === false) ? false : true;
            $moduleAction = "ordersReviews";
            break;
        case "bookingsObjects":
            $moduleName = "bookingSystem";
            $bookingsController = new BookingsController($database, $appController);
            $setVisibility = $requestsController->switchVisibility($request["data"]["id"], $bookingsController->tableObjects);
            break;
        case "bookingsUnits":
            $moduleName = "bookingSystem";
            $bookingsController = new BookingsController($database, $appController);
            $setVisibility = $requestsController->switchVisibility($request["data"]["id"], $bookingsController->tableUnits);
            break;
    }

    $requestSuccess = ($setVisibility === false) ? false : true;

    if($requestSuccess === true){
        if( (int) $setVisibility == 0){
            echo '<i class="fa fa-times-circle red" title="'.translate("object-visibility-no").'"></i>';
        }else{
            echo '<i class="fa fa-check-circle green" title="'.translate("object-visibility-yes").'"></i>';
        }

        $systemLogger->createLog($_SESSION["languages"]["adminLang"], $request["data"]["type"], "visibility", $request["data"]["id"], $_SESSION["security"]["adminLogin"]["logIn"]);
    }
}