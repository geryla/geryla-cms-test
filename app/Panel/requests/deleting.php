<?php
if(isExisting('id', $request["data"]) && isExisting('type', $request["data"])){
    $prefixEngine = new prefixEngine($database);
    $requestSuccess = false;

    switch ($request["data"]["type"]) {
        case "bonds":
            $moduleName = "contentEditor";
            $bondsTable = databaseTables::getTable('contentEditor', 'bondTable');
            $requestsController->deleteData($request["data"]["id"], $bondsTable);

            $cmsHook = new cmsHook($database, $appController);
            $cmsHook->updateChild($request["data"]["id"]);

            $requestSuccess = true;
            break;
        case "bondsCustom":
            $moduleName = "contentEditor";
            $bondsTable = databaseTables::getTable('contentEditor', 'bondTable');

            $cmsHook = new cmsHook($database, $appController);
            $bondInfo = $cmsHook->getAnchorFromHook($request["data"]["id"]);
            if($bondInfo){
                $requestsController->deleteData($request["data"]["id"], $bondsTable);
                $cmsHook->deleteHookAnchor($bondInfo["objectId"]);
                $cmsHook->updateChild($request["data"]["id"]);

                $requestSuccess = true;
            }
            break;
        case "posts":
            $moduleCMS = new CMS($database, $appController);
            $cmsCategories = new cmsCategories($database, $appController);
            $cmsGroups = new cmsGroups($database, $appController);
            $cmsPost = new cmsPost($database, $appController);

            $postTableLang = databaseTables::getTable('contentEditor', 'postTableLang');
            $postsTable = databaseTables::getTable('contentEditor', 'postTable');

            $cmsCategories->deleteSelectedCategories($request["data"]["id"]);
            $cmsGroups->deleteSelectedGroups($request["data"]["id"]);

            $requestsController->deleteLocale($request["data"]["id"], $postTableLang);
            $requestsController->deleteData($request["data"]["id"], $postsTable);

            $requestSuccess = true;
            $moduleName = "contentEditor";
            $mediaName = $modulesController->getModuleMediaName($cmsPost->mediaNamePost);
            break;
        case "postCategory":
            $moduleCMS = new CMS($database, $appController);
            $cmsCategories = new cmsCategories($database, $appController);

            $categoriesTable = databaseTables::getTable('contentEditor', 'categoryTable');

            $requestsController->deleteData($request["data"]["id"], $categoriesTable);
            $requestsController->deleteGlobalLocale($request["data"]["id"], $cmsCategories->translateType);
            $cmsCategories->deletePostCategoriesCross($request["data"]["id"]);
            $cmsCategories->replaceChildCategories($request["data"]["id"]);

            $requestSuccess = true;
            $moduleName = "contentEditor";
            $mediaName = $modulesController->getModuleMediaName($cmsCategories->mediaNameCategory);
            break;
        case "postGroup":
            $moduleCMS = new CMS($database, $appController);
            $cmsGroups = new cmsGroups($database, $appController);

            $groupsTable = databaseTables::getTable('contentEditor', 'groupTable');

            $requestsController->deleteData($request["data"]["id"], $groupsTable);
            $requestsController->deleteGlobalLocale($request["data"]["id"], $cmsGroups->translateType);
            $cmsGroups->deletePostGroupsCross($request["data"]["id"]);
            $cmsGroups->replaceChildGroups($request["data"]["id"]);

            $requestSuccess = true;
            $moduleName = "contentEditor";
            $mediaName = $modulesController->getModuleMediaName($cmsGroups->mediaNameGroup);
            break;
        case "postLabel":
            $cmsPostBlog = new cmsPostBlog($database, $appController);
            $translateType = $appController->getModuleConfig('contentEditor', 'translateType')["postLabel"];

            $cmsPostBlog->deleteLabel($request["data"]["id"], true);
            $requestsController->deleteGlobalLocale($request["data"]["id"], $translateType);

            $requestSuccess = true;
            $moduleName = "contentEditor";
            break;
        case "postLabelGroup":
            $cmsPostBlog = new cmsPostBlog($database, $appController);
            $translateType = $appController->getModuleConfig('contentEditor', 'translateType')["postLabelGroup"];

            $cmsPostBlog->deleteLabelGroup($request["data"]["id"], true);
            $requestsController->deleteGlobalLocale($request["data"]["id"], $translateType);

            $requestSuccess = true;
            $moduleName = "contentEditor";
            break;
        case "admins":
            $tableName = databaseTables::getTable('admins', 'users');
            $requestsController->deleteData($request["data"]["id"], $tableName);
            $requestSuccess = true;
            $moduleName = "admins";
            break;
        case "adminsLevels":
            $adminEngine = new adminEngine($database, $localeEngine);
            $moduleAdmins = new moduleAdmins($adminEngine, $localizationClass);
            $tableName = databaseTables::getTable('admins', 'levels');

            $moduleAdmins->deletePermissions($request["data"]["id"]);
            $requestsController->deleteData($request["data"]["id"], $tableName);
            $requestSuccess = true;
            $moduleName = "admins";
            break;
        case "slideshow":
            $moduleSlider = new moduleSlider($database, $appController);

            $requestsController->deleteData($request["data"]["id"], $moduleSlider->slidesTable);
            $requestsController->deleteGlobalLocale($request["data"]["id"], $moduleSlider->translateType);
            $requestSuccess = true;
            $finalAction = "deleteSlide";
            $moduleName = "slideshow";
            $mediaName = $modulesController->getModuleMediaName($moduleSlider->mediaInfoName);
            break;
        case "slideshowGroup":
            $moduleSlider = new moduleSlider($database, $appController);
            $moduleSlider->deleteGroup($request["data"]["id"], true);

            $requestSuccess = true;
            $finalAction = "deleteSlideGroup";
            $moduleName = "slideshow";
            break;
        case "contactBox":
            $contactBoxes = new contactBoxes($database, $appController);
            $requestsController->deleteData($request["data"]["id"], $contactBoxes->headTable);
            $requestsController->deleteGlobalLocale($request["data"]["id"], $contactBoxes->translateType);
            $requestSuccess = true;
            $moduleName = "contactBox";
            $mediaName = $modulesController->getModuleMediaName($contactBoxes->mediaInfoName);
            break;
        case "gallery":
            $gallery = new moduleGallery($database, $appController);
            $requestsController->deleteData($request["data"]["id"], $gallery->galleryTable);
            $requestsController->deleteGlobalLocale($request["data"]["id"], $gallery->translateType);
            $requestSuccess = true;
            $moduleName = "gallery";
            $mediaName = $modulesController->getModuleMediaName($gallery->mediaInfoName);
            break;
        case "emailTemplates":
            $emailTemplate = new emailTemplates($database, $appController);
            $requestsController->deleteData($request["data"]["id"], $emailTemplate->headTableName);
            $requestsController->deleteGlobalLocale($request["data"]["id"], $emailTemplate->translateType);
            $requestSuccess = true;
            $moduleName = "emailTemplates";
            break;
        case "contactForm":
            $contactForm = new contactForm($database, $appController);
            $requestsController->deleteData($request["data"]["id"], $contactForm->headTableName);
            $requestsController->deleteGlobalLocale($request["data"]["id"], $contactForm->translateType);
            $requestSuccess = true;
            $moduleName = "contactForm";
            break;
        case "courses":
            $courses = new moduleCourses($database, $appController);
            $lessonTable = databaseTables::getTable('courses', 'lessonTable');
            $isEmpty = $courses->checkAllChildResults($lessonTable, $request["data"]["id"], "courseId");
            if($isEmpty !== true){break;}
            $requestsController->deleteLocale($request["data"]["id"], $courses->coursesLangTable);
            $requestsController->deleteData($request["data"]["id"], $courses->coursesTable);
            $requestSuccess = true;
            $moduleName = "courses";
            $mediaName = $modulesController->getModuleMediaName($courses->mediaNameCourse);
            break;
        case "courseLesson":
            $courses = new moduleCourses($database, $appController);
            $lessons = new moduleCoursesLesson($courses);
            $sentenceTable = databaseTables::getTable('courses', 'sentenceTable');
            $isEmpty = $courses->checkAllChildResults($sentenceTable, $request["data"]["id"], "lessonId");
            if($isEmpty !== true){break;}
            $requestsController->deleteData($request["data"]["id"], $lessons->lessonTable);
            $requestsController->deleteGlobalLocale($request["data"]["id"], $lessons->translateType);
            $requestSuccess = true;
            $moduleName = "courses";
            $mediaName = $modulesController->getModuleMediaName($lessons->mediaNameLesson);
            break;
        case "courseSentence":
            $courses = new moduleCourses($database, $appController);
            $lessons = new moduleCoursesLesson($courses);
            $sentence = new moduleCoursesSentence($lessons);
            $requestsController->deleteData($request["data"]["id"], $sentence->sentenceTable);
            $requestsController->deleteGlobalLocale($request["data"]["id"], $sentence->translateType);
            $requestSuccess = true;
            $moduleName = "courses";
            $mediaName = $modulesController->getModuleMediaName($sentence->mediaNameSentence);
            break;
        case "deliveryMethods":
            $delivery = new deliveryMethods($database, $appController);
            $delivery->removeDeliveryMethod($request["data"]["id"]);
            $delivery->removeMethodHooks($request["data"]["id"], null);
            $requestsController->deleteLocale($request["data"]["id"], $delivery->translateTable);
            $requestsController->deleteData($request["data"]["id"], $delivery->dataTable);

            $requestSuccess = true;
            $moduleName = "deliveryMethods";
            $mediaName = $modulesController->getModuleMediaName($delivery->mediaNameDelivery);
            break;
        case "paymentMethods":
            $payment = new paymentMethods($database, $appController);
            $delivery = new deliveryMethods($database, $appController);
            $delivery->removeMethodHooks(null, $request["data"]["id"]);
            $requestsController->deleteLocale($request["data"]["id"], $payment->translateTable);
            $requestsController->deleteData($request["data"]["id"], $payment->dataTable);

            $requestSuccess = true;
            $moduleName = "paymentMethods";
            $mediaName = $modulesController->getModuleMediaName($payment->mediaNamePayment);
            break;
        case "orders":
            $orders = new moduleOrders($database, $appController);
            $orderStocks = new orderStocks($database, $appController);

            $orders->deleteOrder($request["data"]["id"], true);
            $orderStocks->deleteOrderStocks($objectId);

            $requestSuccess = true;
            $moduleName = "orders";
            break;
        case "orderStatus":
            $orders = new moduleOrders($database, $appController);
            $requestsController->deleteData($request["data"]["id"], $orders->orderStatus);
            $requestsController->deleteGlobalLocale($request["data"]["id"], $orders->translateType);
            $requestSuccess = true;
            $moduleName = "orders";
            break;
        case "orderReview":
            $reviews = new ordersReviews($database);

            $requestsController->deleteData($request["data"]["id"], $reviews->reviewsTable);

            $true = true;
            $finalAction = "deleteOrderReview";
            $moduleAction = "ordersReviews";

            break;
        case "discountCode":
            $discount = new DiscountsModule($database, $appController);
            $requestsController->deleteData($request["data"]["id"], $discount->discountsTable);
            $requestsController->deleteGlobalLocale($request["data"]["id"], $discount->translateType);
            $requestSuccess = true;
            $moduleName = "discounts";
            break;
        case "productCategory":
            $categoryClass = new categoryClass($database, $appController);
            $categoryClass->deleteSelectedHooks(null, $request["data"]["id"]);
            $requestsController->deleteLocale($request["data"]["id"], $categoryClass->translateTable);
            $requestsController->deleteData($request["data"]["id"], $categoryClass->dataTable);
            $requestSuccess = true;
            $moduleName = "products";
            $mediaName = $modulesController->getModuleMediaName($categoryClass->mediaName);
            break;
        case "productAvailability":
            $availabilityClass = new availabilityClass($database, $appController);
            $requestsController->deleteData($request["data"]["id"], $availabilityClass->availabilityTable);
            $requestsController->deleteGlobalLocale($request["data"]["id"], $availabilityClass->translateType);
            $requestSuccess = true;
            $moduleName = "products";
            break;
        case "productManufacturer":
            $manufacturerClass = new manufacturerClass($database, $appController);
            $requestsController->deleteData($request["data"]["id"], $manufacturerClass->manufacturerTable);
            $requestsController->deleteGlobalLocale($request["data"]["id"], $manufacturerClass->translateType);
            $requestSuccess = true;
            $moduleName = "products";
            $mediaName = $modulesController->getModuleMediaName($manufacturerClass->mediaName);
            break;
        case "productSupplier":
            $supplierClass = new supplierClass($database, $appController);
            $requestsController->deleteData($request["data"]["id"], $supplierClass->supplierTable);
            $requestsController->deleteGlobalLocale($request["data"]["id"], $supplierClass->translateType);
            $requestSuccess = true;
            $moduleName = "products";
            break;
        case "productParameter":
            $productParams = new productParams($database, $appController);
            $productParams->deleteParamToProductHook($request["data"]["id"]);
            $requestsController->deleteGlobalLocale($request["data"]["id"], $productParams->translateHookValue);

            $requestsController->deleteData($request["data"]["id"], $productParams->paramsTable);
            $requestsController->deleteGlobalLocale($request["data"]["id"], $productParams->translateType);
            $requestSuccess = true;
            $moduleName = "products";
            break;
        case "productUnit":
            $productUnits = new ProductUnits($database, $appController);
            $requestsController->deleteData($request["data"]["id"], $productUnits->unitsTable);
            $requestsController->deleteGlobalLocale($request["data"]["id"], $productUnits->translateType);
            $requestSuccess = true;
            $moduleName = "products";
            break;
        case "productLabel":
            $productLabels = new productLabels($database, $appController);
            $productLabels->deleteLabelToProductHook($request["data"]["id"]);

            $requestsController->deleteData($request["data"]["id"], $productLabels->labelTable);
            $requestsController->deleteGlobalLocale($request["data"]["id"], $productLabels->translateType);
            $requestSuccess = true;
            $moduleName = "products";
            break;
        case "product":
            $productsClass = new productsClass($database, $appController);
            $productsClass->deleteAllProductHooks($request["data"]["id"]);
            if($appController->isActiveModule("productVariants")){
                $productVariants = new productVariants($database, $appController);
                $productVariants->deleteAllProductCombinations($request["data"]["id"]);
                $productVariants->deleteAllProductVariantsParams($request["data"]["id"]);
            }

            $requestsController->deleteLocale($request["data"]["id"], $productsClass->translateTable);
            $requestsController->deleteData($request["data"]["id"], $productsClass->dataTable);
            $requestSuccess = true;
            $moduleName = "products";
            $mediaName = $modulesController->getModuleMediaName($productsClass->mediaName);
            break;
        case "users":
            $userEngine = new userEngine($database, $appController);
            $user = new moduleUsers($database, $appController);
            $userControl = new userControl($database, $appController);

            $companyTable = databaseTables::getTable('users', 'companyTable');
            $addressesTable = databaseTables::getTable('users', 'addressesTable');

            $userInfo = $user->getUserById($request["data"]["id"]);
            if($appController->isActiveModule("orders")){
                $modOrders = new moduleOrders($database, $appController);
                $modOrders->eraseOrderByEmail($userInfo["email"]);
            }

            $userEngine->removeUserData($request["data"]["id"], $companyTable);
            $userEngine->removeUserData($request["data"]["id"], $addressesTable);
            $userControl->deleteAllLogsForUser($request["data"]["id"]);
            $userEngine->removeUserData($request["data"]["id"], $userEngine->headTable, true);

            if($appController->isActiveModule("optysPartner")){
                $optysController = new OptysController($database, $appController);
                $optysController->removeUser($request["data"]["id"]);
            }

            $requestSuccess = true;
            $moduleName = "users";
            break;
        case "users-companies":
            $userEngine = new userEngine($database, $appController);

            $companyTable = databaseTables::getTable('users', 'companyTable');
            $userEngine->removeUserData($request["data"]["id"], $companyTable, true);

            $requestSuccess = true;
            $moduleName = "users";
            break;
        case "users-address":
            $userEngine = new userEngine($database, $appController);

            $addressesTable = databaseTables::getTable('users', 'addressesTable');
            $userEngine->removeUserData($request["data"]["id"], $addressesTable, true);

            $requestSuccess = true;
            $moduleName = "users";
            break;
        case "redirects":
            $tableName = databaseTables::getTable('templates', 'redirectsTable');
            $requestsController->deleteData($request["data"]["id"],$tableName);

            $requestSuccess = true;
            $moduleName = "templates";
            break;
        case "priceLevel":
            $tableName = databaseTables::getTable('priceLevels', 'levels');
            $requestsController->deleteData($request["data"]["id"], $tableName);

            $requestSuccess = true;
            $moduleName = "priceLevels";
            break;
        case "reservationGroup":
            $reservation = new reservationsClass($database);
            $requestsController->deleteData($request["data"]["id"], $reservation->groupsTable);
            $mediaName = $modulesController->getModuleMediaName($reservation->mediaInfoName);

            $requestSuccess = true;
            $moduleName = "reservations";
            break;
        case "reservation":
            $tableName = databaseTables::getTable('reservations', 'reservations');
            $requestsController->deleteData($request["data"]["id"], $tableName);

            $requestSuccess = true;
            $moduleName = "reservations";
            break;
        case "loyaltyLevel":
            $tableName = databaseTables::getTable('loyaltyProgram', 'levels');
            $requestsController->deleteData($request["data"]["id"], $tableName);

            $requestSuccess = true;
            $moduleName = "loyaltyProgram";
            break;
        case "xmlFeed":
            $xmlFeeds = new xmlFeedsModule($database, $appController);

            $feedInfo = $xmlFeeds->getFeedInfo($request["data"]["id"]);
            $fileUrl = $xmlFeeds->buildFilePath($feedInfo);
            $removeFile = $xmlFeeds->removeXmlFile($fileUrl);

            if($removeFile){
                $requestsController->deleteData($request["data"]["id"], $xmlFeeds->headTable);

                $requestSuccess = true;
                $moduleName = "xmlFeeds";
            }

            break;
        case "schedule":
            $tableName = databaseTables::getTable('settings', 'scheduler');
            $requestsController->deleteData($request["data"]["id"], $tableName);

            $requestSuccess = true;
            $moduleName = "settings";

            break;
        case "optysPartner":
            $optysController = new OptysController($database, $appController);
            $optysController->removePartner($request["data"]["id"]);

            $requestSuccess = true;
            $moduleName = "optysPartner";
            break;
        case "optysCenter":
            $optysController = new OptysController($database, $appController);
            $optysController->removeCenter($request["data"]["id"]);

            $requestSuccess = true;
            $moduleName = "optysPartner";
            break;
        case "emailMessage":
            $tableName = databaseTables::getTable('emailTemplates', 'messages');
            $requestsController->deleteData($request["data"]["id"], $tableName);

            $emailTemplates = new emailTemplates($database, $appController);
            $emailTemplates->deleteMessageAttachments($request["data"]["id"]);

            $requestSuccess = true;
            $moduleName = "emailTemplates";

            break;
        case "optysPartnerFile":
            $maskPatten = explode('_', $request["data"]["id"]);
            $partnerId = $maskPatten[0];
            $mask = $maskPatten[1].(isset($maskPatten[2]) ? '_'.$maskPatten[2] : null);

            $optysController = new OptysController($database, $appController);
            $optysController->deleteFile($partnerId, true, 'lineup', $mask);

            $requestSuccess = true;
            $moduleName = "optysPartner";

            break;
        case "bookingsObjects":
            $bookingsController = new BookingsController($database, $appController);
            $requestsController->deleteData($request["data"]["id"], $bookingsController->tableObjects);

            $requestSuccess = true;
            $moduleName = "bookingSystem";
            $mediaName = $modulesController->getModuleMediaName($bookingsController->mediaObjects);

            break;
        case "bookingsUnits":
            $bookingsController = new BookingsController($database, $appController);
            $requestsController->deleteData($request["data"]["id"], $bookingsController->tableUnits);

            $requestSuccess = true;
            $moduleName = "bookingSystem";
            $mediaName = $modulesController->getModuleMediaName($bookingsController->mediaUnits);

            break;
        case "qrOrdersTable":
            $qrOrders = new QrOrders($database, $appController);
            $requestsController->deleteData($request["data"]["id"], $qrOrders->tableTables);

            $requestSuccess = true;
            $moduleName = "qrOrders";
            break;
        default:
            $requestSuccess = false;
    }
    
    if($requestSuccess === true){
        if( isset($mediaName) && !empty($mediaName) ){
            $mediaController->clearObjectMediaTraces($request["data"]["id"], $mediaName);
        }

        if($appController->isActiveModule('contentBlocks')){
            $contentBlocks = new ContentBlocks($database, $appController);
            $contentBlocks->deleteModelBlocks($request["data"]["type"], $request["data"]["id"]);
        }
        
        $systemLogger->createLog($_SESSION["languages"]["adminLang"], $request["data"]["type"], "deleting", $request["data"]["id"], $_SESSION["security"]["adminLogin"]["logIn"]);
    }
}