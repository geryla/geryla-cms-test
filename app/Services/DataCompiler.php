<?php
    namespace Compiler;

    class DataCompiler{
        private $database;
        private $appController;
        private $localizationClass;

        private $lang;
        private $langShort;

        private $enableTranslate = false;
        private $activeModuleConfig = null;

        public $data = null;
        public $defaultData = null;

        public $tableForData = null;
        public $tableForTexts = null;
        public $rowsForData = null;
        public $rowsForText = null;

        public $nextStep = false;
        public $returnMessage = null;

        public $moduleCompiler = null;

        public function __construct(\Database $database, \AppController $appController){
            $this->database = $database;
            $this->appController = $appController;
            $this->localizationClass = $appController->returnLocalizationClass();

            $this->lang = $appController->getLangFile();
            $this->langShort = $appController->getLang();
        }

// COMPILE FUNCTIONS
        public function setTables($tableForData, $tableForTexts = null){
            if($tableForData === null){
                return false;
            }

            $this->tableForData = $tableForData;
            $this->tableForTexts = $tableForTexts;
            return true;
        }
        public function setData($postData, $translateRows, $dataRows){
            if($postData === null){
                return false;
            }

            $this->rowsForData = $dataRows;
            $this->rowsForText = $translateRows;
            $this->data = $this->localizationClass->createMultiTableQuery($postData, useIfExists($translateRows, []), $dataRows);
            return true;
        }
        public function setDefaultData($defaultData){
            if($defaultData === null){
                return false;
            }

            $this->defaultData = $defaultData;
            return true;
        }
        public function moveTranslatableData($objectId, $translateType, $langShort, $translatableSkipRows){
            if($this->localizationClass->getOneLangByLangShort($langShort)["useRate"] == "0"){
                $this->data["data"] = $this->localizationClass->saveValueAndSkipEditing($langShort, $translateType, $this->data["data"], $translatableSkipRows, $objectId, $this->tableForTexts);
            }

            return true;
        }

        private function checkIt(){
            return ($this->tableForData === null OR $this->data === null OR $this->rowsForData === null) ? false : true;
        }

        public function setTranslate($translate){
            $this->enableTranslate = $translate;
        }
        public function setContinue($return){
            $this->nextStep = ($return === false) ? false : true;
        }
        public function setMessage($message){
            $this->returnMessage = $message;
        }

        public function showMessage(){
            if( $this->returnMessage !== null ){
                $_SESSION["notifications"]["admin"] = ["message" => $this->returnMessage, "displayed" => "false", "type" => ($this->nextStep === false) ? "error" : "success"];
            }
            if(isset($_SESSION["notifications"]["admin"]) && $_SESSION["notifications"]["admin"] !== null && $_SESSION["notifications"]["admin"]["displayed"] === "false"){
                echo '<p id="notification" class="'.$_SESSION["notifications"]["admin"]["type"].'"><span class="text">'.$_SESSION["notifications"]["admin"]["message"].'</span><i id="closeAlert" class="fa fa-close"></i></p>';
                if( $this->returnMessage === null ){$_SESSION["notifications"]["admin"]["displayed"] = "true";}
            }
        }
        public function refreshPage($enable, $page){
            if($enable === true){
                die('<script>setTimeout(function(){window.location.href="'.$page.'"}, 0)</script>');
            }
        }
        public function redirectPage($second, $page){
            $sec = ($second > 0) ? $second."000" : 0;
            die('<script>setTimeout(function(){window.location.href="'.$page.'"}, '.$sec.')</script>');
        }
        public function isSucceed(){
            return $this->nextStep;
        }

// ACTIVE MODULE FUNCTIONS
        public function setActiveModuleConfig($activeModuleConfig){
            $this->activeModuleConfig = $activeModuleConfig;
        }
        public function modulePageUrl($pageId){
            return $this->activeModuleConfig->modulePages[$pageId-1]["url"];
        }
        public function buildPageUrl(String $pageUrl, String $moduleName = null){
            return ($moduleName !== null) ? '/'.ADMIN.'/'.$moduleName.'/'.$pageUrl : $pageUrl;
        }

        public function decodeBackLink(Array $getParams = [], String $fallback = null){
            return (!empty($getParams) && $getParams["backLink"]) ? base64_decode($getParams["backLink"]) : $fallback;
        }
        public function encodeBackLink(String $url, String $path = null){
            $encodedPath = ($path !== null) ? base64_encode($path) : null;
            if($encodedPath !== null){
                $url = $url.((strpos($url, '?') !== false) ? '&' : '?').'backLink='.$encodedPath;
            }

            return $url;
        }

// QUERY FUNCTIONS
        public function addQuery($objectForSkip = null){
            if($this->checkIt() !== false){
                $return = $this->localizationClass->addQuery($this->data, $this->tableForData, $this->tableForTexts, $objectForSkip);
            }

            $this->setContinue( ($return === false) ? false : true );
            $this->setMessage( ($return === false) ? translate("admin-create-false") : translate("admin-edit-true") );
            return $return;
        }
        public function addQueryAndTranslate(){
            $return = false;
            $returnId = 0;

            if($this->data !== null && $this->tableForData !== null && $this->checkIt() !== false){
                $returnId = $this->localizationClass->addQueryAndTranslate($this->data, $this->tableForData);
                $return = ($returnId === 0) ? false : true;
            }

            $this->setContinue($return);
            $this->setMessage( ($return === false) ? translate("admin-create-false") : translate("admin-create-true") );
            return $returnId;
        }
        public function updateQuery($objectId){
            $return = $skipProcess = false;

            if($objectId !== null && $this->checkIt() !== false){ // Check if data are completed
                if($this->enableTranslate){ // If translate is enabled, check for calling right method (not created translate cant be updated)
                    $translateExist = $this->localizationClass->checkForTranslate($objectId, null, $this->tableForTexts, $this->langShort);
                    if($translateExist === false){ // If translate not exist ADD query instead of UPDATE
                        $return = $this->addQuery($objectId); // ADD query but ONLY to lang module (data is existing, translate must be created)
                        $skipProcess = true;
                    }
                }

                $return = ($skipProcess === true) ? $return : $this->localizationClass->updateQuery($this->data, $this->tableForData, $this->tableForTexts, $objectId, $this->langShort);
            }

            $this->setContinue($return);
            $this->setMessage( ($return === false) ? translate("admin-create-false") : translate("admin-edit-true") );
            return $return;
        }
        public function updateQueryAndTranslate($objectId, $translateType){
            $return = false;
            if($objectId !== null && $this->checkIt() !== false){
                $return = $this->localizationClass->updateQueryAndTranslate($this->data, $this->tableForData, $objectId, $translateType, $this->langShort, $this->defaultData, $this->enableTranslate);
            }

            $this->setContinue($return);
            $this->setMessage( ($return === false) ? translate("admin-create-false") : translate("admin-edit-true") );
            return $return;
        }

// QUERY MODULES
        public function updateImages($mediaTranslateTable){
            if( $this->nextStep === true && isset($this->data["other"]["images"]) ){
                foreach($this->data["other"]["images"] as $imageId => $imageParams){
                    $imageData["translate"] = ["title" => $imageParams["title"], "alt" => $imageParams["alt"]];

                    $translateSql = "SELECT id FROM `{$mediaTranslateTable}` WHERE associatedId = :associatedId AND langShort = :langShort;";
                    $translateCheck = $this->database->getQuery($translateSql, ["associatedId" => $imageId, "langShort" => $this->langShort], false);

                    if(!empty($translateCheck)){
                        $done = $this->localizationClass->updateQuery($imageData, null, $mediaTranslateTable, $imageId, $this->langShort);
                    }else{
                        $imageData["translate"]["langShort"] = $this->langShort;
                        $done = $this->localizationClass->addQuery($imageData, null, $mediaTranslateTable, $imageId);
                    }

                    if($done === false){
                        $this->setMessage(translate("mod-mediaManager-return-add-false-adding"));
                        $this->setContinue(false);
                    }
                }
            }
        }
        public function processContentBlocks(Array $objectData){
            if($this->appController->isActiveModule('contentBlocks')){
                $contentBlocks = new \ContentBlocks($this->database, $this->appController);
                $pageBuilder = [];

                if(isset($objectData["pagebuilder"])){
                    $pageBuilder["modelName"] = $objectData["pagebuilder"]["modelName"];
                    $pageBuilder["objectId"] = $objectData["pagebuilder"]["objectId"];

                    if(isExisting('child', $objectData["pagebuilder"]["blocks"][0])){
                        $pageBuilder["blocks"] = $objectData["pagebuilder"]["blocks"][0]["child"];
                    }
                    if(isExisting('deleted', $objectData["pagebuilder"])){
                        $pageBuilder["deleted"] = $objectData["pagebuilder"]["deleted"];
                    }

                    unset($objectData["pagebuilder"]);
                }

                if(isExisting('blocks', $pageBuilder)){
                    $contentBlocks->proceedBlocks($pageBuilder["blocks"], $pageBuilder["objectId"], $pageBuilder["modelName"]);
                }
                if(isExisting('deleted', $pageBuilder)){
                    $contentBlocks->deleteBlocks($pageBuilder["deleted"]);
                }
                if(!empty($pageBuilder)){
                    $cache = new \Cache();
                    $cache->removeCache('Query', 'contentBlocks/'.$pageBuilder["objectId"]);
                }

            }
        }

// HELPERS
        public function setLog($objectId, $name, $type, $langShort){
            if($this->nextStep === true){
                $systemLogger = $this->appController->returnLogger();
                $systemLogger->createLog($langShort, $name, $type, $objectId, session('security.adminLogin.logIn'));
            }
        }
        public function checkEnabledTranslation(?Bool $enabledTranslation = null){
            if($enabledTranslation === null){
                $mainLanguage = $this->localizationClass->getMainLanguage();
                $mainLanguage = (!empty($mainLanguage)) ? $mainLanguage["langShort"] : DEFAULT_LANG;
                $enabledTranslation = ($this->appController->getLang() == $mainLanguage) ? false : true;
            }

            return $enabledTranslation;
        }
    }