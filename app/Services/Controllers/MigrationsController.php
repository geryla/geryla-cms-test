<?php
class MigrationsController {
    private $database;
    private $modulesController;

    private $migrationsTable;
    private $modulesTable;

    public function __construct(Database $database, ModulesController $modulesController) {
        $this->database = $database;
        $this->modulesController = $modulesController;

        $this->migrationsTable = databaseTables::getTable("settings", "migrations");
        $this->modulesTable = databaseTables::getTable("settings", "moduleTables");
    }

// Call
    public function run(string $moduleName = ''){
        $migrationPath = null;

        if (empty($moduleName)){
            $migrationPath = MIGRATIONS_DIR;
        }else{
            if (is_dir(MODULE_DIR.$moduleName)) {
                $migrationPath = MODULE_DIR.$moduleName.'/'.MIGRATIONS.'/';
            }
        }

        if($migrationPath !== null){
            $migration = $this->check($moduleName, $migrationPath);
            return $migration;
        }

        return ["status" => false];
    }
    public function clear($moduleName){
        $migration = ["status" => false];

        if($moduleName !== null){
            if ( is_dir(MODULE_DIR.$moduleName) ) {
                $migration = $this->read("uninstall.sql", MODULE_DIR.$moduleName.'/'.MIGRATIONS.'/', $moduleName);
                if($migration["status"] === true){
                    $this->delete($moduleName);
                }
            }

        }

        return $migration;
    }
// Work
    private function check(string $moduleName, string $folderName){
        $moduleVersion = $this->modulesController->getModuleConfig($moduleName, "moduleVersion");

        if(file_exists($folderName)){
            $files = scandir($folderName, 1);

            $orderByDate = [];
            foreach($files as $key => $sql) {
                if(!is_dir($folderName.$sql) && strpos($sql, 'migration') !== false){
                    $migrationDate = str_replace(["migration_", ".sql"], "", $sql);
                    $migrationDate = explode(".", $migrationDate);
                    $orderByDate[$migrationDate[2].'/'.$migrationDate[1].'/'.$migrationDate[0]] = $sql;
                }
            }
            ksort($orderByDate);

            if(!empty($orderByDate)){
                foreach($orderByDate as $fileName){
                    $migrationExist = $this->get($moduleName, $fileName);
                    if(!$migrationExist){
                        $migration = $this->read($fileName, $folderName, $moduleName);
                        if($migration["status"] === true){
                            $this->insert($migration["moduleName"], $migration["migration"], $migration["date"]);
                        }else{
                            return $migration;
                        }
                    }
                }
            }

            $this->update($moduleName, $moduleVersion);
        }else{
            $this->update($moduleName, $moduleVersion);
        }

        return ["status" => true];
    }
    private function read($migrationFile, $folderName, $moduleName){
        if(file_exists($folderName.$migrationFile)){
            $fileExtension = substr(strrchr($migrationFile, "."), 1);
            if($fileExtension == "sql"){
                $scriptPath = file_get_contents($folderName.$migrationFile);
                $migrationProcess = ["migration" => $migrationFile, "moduleName" => $moduleName, "date" => date("Y-m-d H:i:s")];

                $output = $this->database->migrationQuery($scriptPath);

                if($output === true){
                    $migrationProcess["status"] = true;
                }else{
                    $migrationProcess["status"] = false;
                    $migrationProcess["error"] = $output[0];
                    $migrationProcess["code"] = $output[1];
                    $migrationProcess["message"] = $output[2];
                }

                return $migrationProcess;
            }
        }

        return ["status" => false, "error" => 'Cannot read file'];
    }
// Query
    private function get($moduleName, $fileName){
        $migrationExist = $this->database->getQuery("SELECT * FROM `{$this->migrationsTable}` WHERE `migration` = :migration AND `moduleName` = :moduleName", ["migration" => $fileName, "moduleName" => $moduleName], false);
        return $migrationExist;
    }
    private function insert($moduleName, $migration, $createdDate){
        $this->database->insertQuery("INSERT INTO `{$this->migrationsTable}`", ["migration" => $migration, "moduleName" => $moduleName, "createdDate" => $createdDate]);
    }
    private function update($moduleName, $moduleVersion){
        $this->database->updateQuery("UPDATE `{$this->modulesTable}`", ["moduleVersion" => $moduleVersion], ["moduleName" => $moduleName], "WHERE moduleName = :moduleName");
    }
    private function delete($moduleName){
        $this->database->deleteQuery("DELETE FROM `{$this->migrationsTable}`", ["moduleName" => $moduleName], "WHERE moduleName = :moduleName");
    }
}