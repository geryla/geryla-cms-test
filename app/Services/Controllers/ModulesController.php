<?php

class ModulesController {
    private $database;
    private $migrationsController;

    private $modulesDirectory = null;
    private $modulesTable;

    private $modulesData = [
        "loaded" => [],
        "missing" => [],
        "tree" => [],
        "required" => []
    ];
    private $modulesConfig = [
        "tables" => [
            "content" => [],
            "replacement" => []
        ],
        "paths" => [],
        "requireFiles" => [],
        "media" => [],
        "pages" => [],
        "hooks" => [],
        "panels" => [],
    ];
    public $moduleFolders = [
        "class" => "class",
        "models" => "models",
        "lang" => "languages",
        "css" => "css",
        "js" => "js",
        "frontend" => "frontend",
        "backend" => "backend"
    ];

    public function __construct(Database $database) {
        $this->database = $database;
        $this->migrationsController = new MigrationsController($database, $this);

        $this->modulesTable = databaseTables::getTable("settings", "moduleTables");
        $this->modulesDirectory = dir(MODULE_DIR);
    }

    public function loadAllModules(bool $reload = false) {
        if (!isExisting('all', $this->modulesData["loaded"]) || $reload === true) {
            $this->modulesData["loaded"]["all"] = $this->getAllModulesByGroup('moduleGroup', $reload);
        }
        return $this->modulesData["loaded"]["all"];
    }

    public function returnModules(string $type) {
        switch ($type) {
            case "active":
                return $this->modulesData["loaded"]["active"];
            case "default":
                return $this->modulesData["loaded"]["default"];
            default:
                return $this->modulesData["loaded"]["all"];
        }
    }
    public function returnContentTables() {
        return $this->modulesConfig["tables"]["content"];
    }
    public function returnReplacementTables() {
        return $this->modulesConfig["tables"]["replacement"];
    }

    public function load(){
        $systemCache = new SystemCache();

        $cachedData = $systemCache->get('modules', 'modulesData');
        if(!empty($cachedData)){
            $this->modulesData = $cachedData;
        }else{
            $this->initModules();
            $systemCache->set('modules', 'modulesData', $this->modulesData);
        }

        $cachedConfig = $systemCache->get('modules', 'modulesConfig');
        if(!empty($cachedData)){
            $this->modulesConfig = $cachedConfig;
        }else{
            $this->loadModuleAssets();
            $systemCache->set('modules', 'modulesConfig', $this->modulesConfig);
        }

        if (!empty($this->modulesConfig['requireFiles'])) {
            foreach ($this->modulesConfig['requireFiles'] as $requiredFile){
                require($requiredFile);
            }
        }
    }

    public function initModules() {
        $firstSeed = false;
        $allModules = $this->loadAllModules();

        if (empty($allModules)) {
            $this->migrationsController->run();
            $allModules = $this->loadAllModules(true);
            $firstSeed = true;
        }

        $this->loadModulesConfig($allModules);

        if ($firstSeed === true) {
            $defaultModules = $this->returnModules("default");
            if (isNotEmpty($defaultModules)) {
                foreach ($defaultModules as $moduleName) {
                    $this->migrationsController->run($moduleName);
                }
            }
        }
    }
    private function loadModulesConfig(Array $modulesList) {
        if (isNotEmpty($modulesList)) {
            // Load configuration
            foreach ($modulesList as $module) {
                $isActive = false;
                $folderPath = MODULE_DIR . $module["moduleName"];

                if ($module["activeModule"] == 1 && (file_exists($folderPath) && is_dir($folderPath))) {
                    $this->modulesData["loaded"]["active"][] = $module["moduleName"];
                    $isActive = true;

                    if (!empty($module["contentTables"])) {
                        $this->modulesConfig["tables"]["content"][$module["moduleName"]] = array_map('trim', explode(',', $module["contentTables"]));
                    }
                    if (!empty($module["replaceContentTables"])) {
                        $this->modulesConfig["tables"]["replacement"][$module["moduleName"]] = array_map('trim', explode(',', $module["replaceContentTables"]));
                    }

                    if ($module["moduleGroup"] == 0) {
                        $this->modulesData["tree"][$module["id"]] = ["id" => $module["id"], "name" => $module["moduleName"], "order" => $module["orderNum"], "group" => $module["moduleGroup"], "child" => []];
                    } else {
                        $this->modulesData["tree"][$module["moduleGroup"]]["child"][$module["id"]] = ["id" => $module["id"], "name" => $module["moduleName"], "order" => $module["orderNum"], "group" => $module["moduleGroup"], "child" => []];
                    }
                }

                if ($module["defaultModule"] == 1) {
                    $this->modulesData["loaded"]["default"][$module["defaultPriority"]] = $module["moduleName"];
                    if ($isActive === false) {
                        $this->modulesData["missing"][] = $module["moduleName"];
                    }
                }
            }

            // Sort modules (GROUP 0) by database orderNum
            usort($this->modulesData["tree"], function ($a, $b) {
                return $a['order'] - $b['order'];
            });
            ksort($this->modulesData["loaded"]["default"]);

            if (!empty($this->modulesData["missing"])) {
                foreach ($this->modulesData["missing"] as $module) {
                    throw new RuntimeException("Missing default module: {$module}");
                }
            }
        }
    }
    private function loadModuleAssets() {
        while (false !== ($moduleName = $this->modulesDirectory->read())) {
            if ($moduleName != '.' && $moduleName != '..' && in_array($moduleName, $this->modulesData["loaded"]["active"])) {
                if (is_dir(MODULE_DIR . $moduleName)) {
                    $modulePath = MODULE_DIR . $moduleName;

                    $this->scanFolderForFiles($moduleName, $modulePath . '/' . $this->moduleFolders["class"] . '/', "php", true, false, $this->moduleFolders["class"]);
                    $this->scanFolderForFiles($moduleName, $modulePath . '/' . $this->moduleFolders["models"] . '/', "php", true, false, $this->moduleFolders["models"]);
                    $this->scanFolderForFiles($moduleName, $modulePath . '/' . $this->moduleFolders["lang"] . '/', "php", false, false, $this->moduleFolders["lang"]);
                    $this->scanFolderForFiles($moduleName, $modulePath . '/' . $this->moduleFolders["css"] . '/' . $this->moduleFolders["frontend"] . '/', "css", false, true, $this->moduleFolders["css"]);
                    $this->scanFolderForFiles($moduleName, $modulePath . '/' . $this->moduleFolders["css"] . '/' . $this->moduleFolders["backend"] . '/', "css", false, true, $this->moduleFolders["css"]);
                    $this->scanFolderForFiles($moduleName, $modulePath . '/' . $this->moduleFolders["js"] . '/' . $this->moduleFolders["frontend"] . '/', "js", false, true, $this->moduleFolders["js"]);
                    $this->scanFolderForFiles($moduleName, $modulePath . '/' . $this->moduleFolders["js"] . '/' . $this->moduleFolders["backend"] . '/', "js", false, true, $this->moduleFolders["js"]);

                    $this->checkForDependencies($this->getModuleConfig($moduleName), $this->modulesData["loaded"]["active"]);
                }
            }
        }
    }
    private function checkForDependencies($moduleConfig, $activeModules) {
        // REQUIRED MODULES - smooth run of application
        if (isset($moduleConfig["requiredModules"])) {
            foreach ($moduleConfig["requiredModules"] as $required) {
                if (!in_array($required["moduleName"], $activeModules)) {
                    $this->modulesData["required"][$required["moduleName"]] = $required["moduleName"];
                }
            }
        }
        // MEDIA SETTINGS - loading media files
        if (isset($moduleConfig["mediaSettings"])) {
            foreach ($moduleConfig["mediaSettings"] as $mediaType => $mediaName) {
                $this->modulesConfig["media"][$mediaType] = $mediaName;
            }
        }
        // SHORT CODES - list for wysiwyg editor at backend
        if (file_exists(MODULE_DIR . $moduleConfig["moduleUrlName"] . '/shortcodes/')) {
            $this->modulesConfig["shortcodes"][$moduleConfig["moduleUrlName"]] = $moduleConfig["moduleUrlName"];
        }
        // PAGES - all module pages from replaceContentTables
        if (file_exists(MODULE_DIR . $moduleConfig["moduleUrlName"] . '/pages/')) {
            $this->modulesConfig["pages"][] = $moduleConfig["moduleUrlName"];
        }
        // HOOKS - all module hooks for templates
        if (file_exists(MODULE_DIR . $moduleConfig["moduleUrlName"] . '/hooks/')) {
            $folderName = MODULE_DIR . $moduleConfig["moduleUrlName"] . '/hooks/';
            $views = scandir($folderName);
            foreach ($views as $file) {
                list($fileName, $fileExtension) = explode(".", $file);
                if (!is_dir($folderName . $file)) {
                    $this->modulesConfig["hooks"][$moduleConfig["moduleUrlName"]][$fileName] = $folderName . $file;
                }
            }
        }
        // ADMIN DASHBOARD PANELS - all module panels for dashboard
        if (file_exists(MODULE_DIR . $moduleConfig["moduleUrlName"] . '/includes/panels/')) {
            $folderName = MODULE_DIR . $moduleConfig["moduleUrlName"] . '/includes/panels/';
            $views = scandir($folderName);
            foreach ($views as $file) {
                list($fileName, $fileExtension) = explode(".", $file);
                if (!is_dir($folderName . $file)) {
                    $this->modulesConfig["panels"][$moduleConfig["moduleUrlName"]][$fileName] = $folderName . $file;
                }
            }
        }
    }
    private function scanFolderForFiles($moduleName, $folderName, $fileType, $require, $reGroup, $groupName) {

        if (file_exists($folderName)) {
            $files = scandir($folderName);
            if ($reGroup === true) {
                $reGroupName = explode("/", $folderName);
                $groupNames = array_filter($reGroupName);
                $reGroupName = array_slice($groupNames, -1, 1);
            }
            foreach ($files as $file) {
                $fileInfo = explode(".", $file);
                $fileName = $fileInfo[0];
                $fileExtension = $fileInfo[count($fileInfo)-1];
                $fullPath = $folderName.$file;
                //$fileExtension = substr(strrchr($folderName . $file, "."), 1);
                if ($fileExtension == $fileType) {
                    if (!is_dir($fullPath)) {
                        $folderName = ($fileType == "css" OR $fileType == "js") ? relativePath(MODULE_DIR) . $moduleName . "/" . $fileType . "/" . $reGroupName[0] . "/" : $folderName;
                        if ($reGroup === false) {
                            $this->modulesConfig["paths"][$moduleName][$groupName][$fileName] = $folderName.$file;
                        } else {
                            $this->modulesConfig["paths"][$moduleName][$groupName][$reGroupName[0]][$fileName] = $folderName.$file;
                        }
                        if ($require === true) {
                            $this->modulesConfig["requireFiles"][] = $folderName.$file;
                        }
                    }
                }
            }
        }

    }

    public function isActive($moduleName) {
        return (in_array($moduleName, $this->modulesData["loaded"]["active"])) ? true : false;
    }

    public function getModuleMediaName($mediaType = null) {
        if ($mediaType !== null) {
            return (isset($this->modulesConfig["media"][$mediaType])) ? $this->modulesConfig["media"][$mediaType] : null;
        } else {
            return $this->modulesConfig["media"];
        }
    }
    public function getModuleConfig(String $moduleName, String $configPart = null) {
        if (file_exists(MODULE_DIR . $moduleName . "/config.php")) {
            include(MODULE_DIR . $moduleName . "/config.php");

            if ($configPart !== null) {
                return (isset($config[$configPart])) ? $config[$configPart] : null;
            } else {
                return $config;
            }
        }
        return false;
    }
    public function getData(String $name) {
        return $this->modulesData[$name];
    }
    public function getConfig(String $name) {
        return $this->modulesConfig[$name];
    }

// Queries
    public function getAllModulesByGroup(String $orderBy = 'id', bool $skipCache = false) {
        $query = "SELECT * FROM `{$this->modulesTable}` ORDER BY {$orderBy};";
        return $this->database->getQuery($query, null, true, $skipCache);
    }
    public function triggerModuleAction(String $actionName, String $moduleName) {
        $moduleName = trim($moduleName);
        $isFinished = false;

        switch ($actionName) {
            case "activate":
                $response = $this->activateModule($moduleName);
                if ($response["status"] === true) {
                    $response = $this->runComposer($moduleName, $actionName, $response);
                    $isFinished = true;
                }
                break;
            case "deactivate":
                $response = $this->deactivateModule($moduleName);
                $isFinished = true;
                break;
            case "remove":
                $response = $this->removeModule($moduleName);
                if ($response["status"] === true) {
                    $response = $this->runComposer($moduleName, $actionName, $response);
                    $isFinished = true;
                }
                break;
            case "update":
                $response = $this->updateModule($moduleName);
                if ($response["status"] === true) {
                    $response = $this->runComposer($moduleName, $actionName, $response);
                    $isFinished = true;
                }
                break;
        }

        if($isFinished === true){
            $systemCache = new SystemCache();
            $systemCache->remove('modules');
        }

        unlink(DATABASE_TABLES);

        return (isset($response)) ? $response : null;
    }

    private function activateModule(String $moduleName) {
        if (empty($moduleName)) {
            return false;
        }

        $response = $this->migrationsController->run($moduleName);
        $this->database->updateQuery("UPDATE `{$this->modulesTable}`", ["activeModule" => 1], ["moduleName" => $moduleName], "WHERE moduleName = :moduleName");

        return $response;
    }
    private function deactivateModule(String $moduleName) {
        if (empty($moduleName)) {
            return false;
        }

        $response = $this->migrationsController->run($moduleName);
        $this->database->updateQuery("UPDATE `{$this->modulesTable}`", ["activeModule" => 0], ["moduleName" => $moduleName], "WHERE moduleName = :moduleName");

        return $response;
    }
    private function removeModule(String $moduleName) {
        if (empty($moduleName)) {
            return false;
        }

        $response = $this->migrationsController->clear($moduleName);
        $this->database->updateQuery("UPDATE `{$this->modulesTable}`", ["activeModule" => 0], ["moduleName" => $moduleName], "WHERE moduleName = :moduleName");

        return $response;
    }
    private function updateModule(String $moduleName) {
        $response = $this->migrationsController->run($moduleName);

        return $response;
    }
    private function runComposer(String $moduleName, String $actionName, Array $callback) {
        $moduleConfig = $this->getModuleConfig($moduleName);

        if (isset($moduleConfig["composer"])) {
            $callback["status"] = true;
            chdir(ROOT);
            foreach ($moduleConfig["composer"] as $composer) {
                $composerAction = ($actionName === "remove") ? 'composer remove' : 'composer require';
                $composerCmd = $composerAction . ' ' . $composer;
                exec($composerCmd, $output, $exitCode);
                if ($exitCode !== 0) {
                    $callback["status"] = false;
                    $callback["message"] .= " Failed " . $composerCmd;
                    $callback["code"] = $exitCode;
                }
            }
        }

        return $callback;
    }

}