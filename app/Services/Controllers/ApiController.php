<?php
use Api\Auth\AuthController;

class ApiController {
        protected $database;
        protected $appController;
        private $authController;

        private $responseCode = 200;
        private $responseError = null;
        private $responseForceCode = null;
        private $responseData = [];

        protected $requestData;
        protected $isCorrectMethod;

        protected $authRequired = false;
        protected $cacheAllowed = false;
        private $routerSessionId = null;

        private $lastValidationValue;
        protected $requestParametersValidated;
        protected $requestBodyValidated;

        private $controllerErrorCodes = [
            200 => 'Success',
            300 => 'The requested API method is not exist',
            301 => 'The requested method type is ',
            302 => 'The Accepted language header is missing',
            303 => 'The Accepted language header is wrong',
            304 => 'The required value is missing',
            305 => 'The value must be ',
            306 => 'The method value is not expected',
            307 => 'The required parameter is missing',
            308 => 'The parameter is not valid',
            310 => 'Unknown action',
            311 => "The Authorization header is not enabled on server",
            312 => "The access key is missing",
            313 => "The access key is invalid",
            314 => "The access key does not exist",
            315 => "The access key has expired",
            316 => "Failed to load session",
        ];

        public function __construct(\Database $database, \AppController $appController, Array $requestData) {
            $this->database = $database;
            $this->appController = $appController;
            $this->authController = new AuthController($database, $appController);

            $this->requestData = $requestData;
            $this->routerSessionId = session_id();
        }

        protected function returnErrorCodes(Array $factoryErrorCoded){
            return $this->controllerErrorCodes + $factoryErrorCoded;
        }

        public function setResponseData($responseData){
            $this->responseData = $responseData;
        }
        public function getResponseData(){
            return $this->responseData;
        }

        public function setState($httpCode, $httpError = null, $httpFoceCode = null){
            $this->responseCode = $httpCode;
            $this->responseError = $httpError;
            $this->responseForceCode = $httpFoceCode;
        }
        public function getResponseCode(){
            return $this->responseCode;
        }
        public function getResponseError(){
            return $this->responseError;
        }
        public function getResponseForceCode(){
            return $this->responseForceCode;
        }

        public function isAuthRequired(){
            return $this->authRequired;
        }
        public function getRequestSessionInfo(){
            return $this->authController->getRequestSessionInfo();
        }
        public function isCacheAllowed(){
            return $this->cacheAllowed;
        }

        protected function validateRequest(Array $request, Array $requestsList){
        // Request exists
            if(!array_key_exists($request["action"], $requestsList)){
                $this->setState(300);
                return false;
            }

        // AUTH check
            $requestBlueprint = $requestsList[$request["action"]];
            $authorized = $this->openAuthRequest($requestBlueprint);
            if($authorized === false){
                return false;
            }

        // Method is correct
            if($request["method"] !== $requestBlueprint["method"]){
                $this->setState(301, $this->controllerErrorCodes[301].$requestBlueprint["method"]);
                return false;
            }

        // Locale validation
            if($this->preventLocale($request["locale"]) === false){
                return false;
            }

        // Value is valid
            $isValidValue = $this->validateValue($this->requestData["value"], useIfExists(arraySearch($requestBlueprint, 'value.required'), false), useIfExists(arraySearch($requestBlueprint, 'value.type'), ''));
            if($isValidValue === false){
                return false;
            }

        // Parameters are valid [GET]
            $this->requestParametersValidated = [];
            if(isset($requestBlueprint["params"])){
                foreach(useIfExists($requestBlueprint["params"], []) as $paramName => $paramInfo){
                    $isValidValue = isExisting($paramName, $request["params"]) ? $this->validateParameter($paramName, $request["params"][$paramName], $paramInfo["required"], $paramInfo["type"]) : true;
                    if($isValidValue === false){
                        return false;
                    }

                    $this->requestParametersValidated[$paramName] = (isset($request["params"][$paramName]) && !empty($request["params"][$paramName])) ? $this->lastValidationValue : $requestBlueprint["params"][$paramName]["default"];
                }
            }

        // Body parameters are valid [POST]
            if($request["method"] === 'POST'){
                $requiredBodyValidation = (!isset($requestBlueprint["requiredBody"]) || $requestBlueprint["requiredBody"] === true) ? true : false;
                if($requiredBodyValidation === true){

                    if(!empty($request["body"])){
                        $this->requestBodyValidated = $this->validateBody($request["body"], $requestBlueprint["body"], []);
                        if($this->requestBodyValidated  === false){
                            return false;
                        }
                    }
                }else{
                    if(!empty($request["body"])){
                        $this->requestBodyValidated = $request["body"];
                    }
                }
            }

            $this->validateWith();

            $this->cacheAllowed = (isExisting('cache', $requestBlueprint) && $requestBlueprint["cache"] === true);

            $this->appController->setLang($this->requestData["locale"]);
            $forcedLocale = $this->appController->returnCurrencyBuilder()->init();
            $this->appController->setLang($forcedLocale);

            return true;
        }
        protected function validateBody(Array $requestBody, Array $requestBlueprint, Array $requestBodyValidated){
            foreach(useIfExists($requestBlueprint, []) as $bodyParamName => $bodyParamInfo){

                $isValidValue = $this->validateParameter($bodyParamName, $requestBody[$bodyParamName], $bodyParamInfo["required"], $bodyParamInfo["type"]);
                if($isValidValue === false){
                    return false;
                }

                $validatedValue = isset($requestBody[$bodyParamName]) ? $requestBody[$bodyParamName] : $requestBlueprint[$bodyParamName]["default"];

                if(isset($bodyParamInfo["values"]) && is_array($bodyParamInfo["values"]) && isExisting($bodyParamName, $requestBody)){
                    if(isset($requestBody[$bodyParamName][0]) && $validatedValue[0]){
                        foreach($requestBody[$bodyParamName] as $key => $bodyVal){
                            $validatedSubValue = $this->validateBody($bodyVal, $bodyParamInfo["values"], $validatedValue[$key]);
                            if($validatedSubValue === false){
                                return false;
                            }
                        }
                    }else{
                        $validatedValue = $this->validateBody($requestBody[$bodyParamName], $bodyParamInfo["values"], $validatedValue);
                    }

                    if($validatedValue === false){
                        return false;
                    }
                }

                $requestBodyValidated[$bodyParamName] = $validatedValue;
            }

            return $requestBodyValidated;
        }
        protected function validateValue($value, Bool $isRequired, String $dataType = ''){
            if($isRequired === true && ($value !== '0' && empty($value))){
                $this->setState(304);
                return false;
            }

            if($isRequired === false && !empty($value)){
                $this->setState(306);
                return false;
            }

            if($isRequired === true){
                $isValid = $this->validateType($value, $dataType);
                if($isValid === false){
                    $this->setState(305, $this->controllerErrorCodes[305].$dataType);
                    return false;
                }
            }


            return true;
        }
        protected function validateParameter(String $name, $value, Bool $isRequired, String $dataType = ''){
            if($isRequired === true && empty($value)){
                if( ($dataType === "Integer" && empty($value) && $value !== 0) || ($dataType === "Boolean" && !is_bool($value)) || ($dataType !== "Integer" && $dataType !== "Boolean" && empty($value)) ){
                    $this->setState(307, "{$this->controllerErrorCodes[307]} [{$name}]");
                    return false;
                }
            }

            if(!empty($value) || $dataType === 'Boolean'){
                $isValid = $this->validateType($value, $dataType);
                if($isValid === false){
                    $this->setState(308, "{$this->controllerErrorCodes[308]} {$dataType} [{$name}]");
                    return false;
                }
            }

            return true;
        }
        protected function validateType($value, $dataType){
            $paramValue = $value;

            switch($dataType){
                case "Integer":
                    $paramValue = (is_numeric($value)) ? intval($value) : $value;
                    $isValid = is_int($paramValue);
                    break;
                case "Numeric":
                    $isValid = is_numeric($paramValue);
                    break;
                case "String":
                    $isValid = is_string($paramValue);
                    break;
                case "Boolean":
                    $isBoolean = filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
                    $isValid = ($isBoolean !== null) ? true : false;
                    $paramValue = $isBoolean;
                    break;
                case "Array":
                    $isValid = is_array($value);
                    break;
                case "Float":
                    $isValid = is_float($value);
                    break;
                default:
                    $isValid = false;
                    break;
            }

            $this->lastValidationValue = $paramValue;

            return $isValid;
        }
        protected function validateWith(){
            $isValid = true;

            if(isset($this->requestData["params"]["with"])){
                $isValid = isNotEmpty($this->requestData["params"]["with"], true);
            }

            if($isValid === false){
                unset($this->requestData["params"]["with"]);
            }
        }
        protected function preventLocale($langShort){
        // Locale header exists
            if(empty($langShort)){
                $this->setState(302);
                return false;
            }

            $localizationClass = $this->appController->returnLocalizationClass();
            $activeLocales = array_column($localizationClass->allActiveLanguages, "langShort");
            $langShort = str_replace('-', '_', $langShort); // Prevent Browser format

            if(in_array($langShort, $activeLocales)){
               return true;
            }

            $langShortSimple = $langShort;
            if(strpos($langShort, '_') !== false){
                $langShortSimple = explode("_", $langShort)[0];
            }

            foreach($activeLocales as $locale){
                $localeSimple = explode("_", $locale)[0];
                if($localeSimple === $langShortSimple){
                    $this->requestData["locale"] = $locale;
                    return true;
                    break;
                }
            }

        // Prevent non existing locale
            $this->requestData["locale"] = $localizationClass->mainLanguage;
            return true;
        }

        private function openAuthRequest(Array $requestBlueprint){
            if(isExisting('authorized', $requestBlueprint) && $requestBlueprint["authorized"] === true){
                $isValidAuth = $this->authController->validateAuth();
                $authError = $this->authController->getAuthError();
                $isNoTokenError = ($authError !== null) ? (in_array($authError, [311, 312, 313])) : false;

                if ($isNoTokenError === false) {
                    if($isValidAuth === false){
                        $sessionInfo = $this->authController->getSessionInfo();
                        $authErrorMessage = $this->controllerErrorCodes[$authError].(($sessionInfo !== null && !empty($sessionInfo)) ? " [{$sessionInfo['accessKey']}] - {$sessionInfo['expire']}": null);
                        $this->setState($authError, $authErrorMessage);
                        return false;
                    }

                    $this->authController->initSession($this->authController->getRequestSessionInfo()["sessionId"]);
                    $this->authRequired = true;
                }
            }

            return true;
        }
        protected function closeAuthRequest(){
            if($this->authRequired === true){
                $this->authController->initSession($this->routerSessionId);
            }
        }
    }