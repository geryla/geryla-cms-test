<?php

class AppController {
    private $database;
    protected $modulesController;
    protected $databaseMigrations;
    protected $settingsController;
    protected $systemLogger;

    protected $localeEngine;
    protected $localization;
    protected $currencyBuilder;

    protected $mediaController;
    protected $mediaProcessor;
    protected $templateClass;

    protected $langShort;
    protected $langFile;
    protected $systemSettings = null;

    public function __construct(Database $database) {
        $this->database = $database;
        $this->loadCoreControllers();
    }

    private function loadCoreControllers() {
        $this->modulesController = new ModulesController($this->database);
        $this->modulesController->load();

        $this->localeEngine = new LocaleEngine($this->database, $this->modulesController, null, (isset($_GET['setAdminLang']) ? $_GET['setAdminLang'] : null));
        $this->localization = new LocalizationClass($this->database, $this);
        $this->setLang($this->localeEngine->returnAppLang());
        $this->setLangFile($this->localeEngine->returnLanguageFile('adminLang'));

        $this->settingsController = new loaderSettings($this->database, $this);
        $this->systemLogger = new systemLogger($this->database);

        $this->currencyBuilder = new currencyBuilder($this->database, $this);

        $this->mediaController = new MediaController($this->database, $this);
        $this->mediaProcessor = new mediaProcessor($this->database, $this, $this->mediaController);
    }

    public function returnModulesController() {
        return $this->modulesController;
    }
    public function returnSettingsController() {
        return $this->settingsController;
    }
    public function returnLogger() {
        return $this->systemLogger;
    }
    public function returnLocaleEngine() {
        return $this->localeEngine;
    }
    public function returnLocalizationClass() {
        return $this->localization;
    }
    public function returnMediaController() {
        return $this->mediaController;
    }
    public function returnMediaProcessor() {
        return $this->mediaProcessor;
    }
    public function returnCurrencyBuilder() {
        //$this->setLang($this->currencyBuilder->setUserInfo());//TODO: Is this best way how to init? (Because of API)
        return $this->currencyBuilder;
    }
    public function returnTemplatesClass() {
        if ($this->templateClass === null) {
            return $this->initTemplateClass();
        }

        return $this->templateClass;
    }

    public function initTemplateClass() {
        $this->templateClass = new templateClass($this->database, $this);
        return $this->templateClass;
    }

    public function setLangFile(Array $langFile) {
        $this->langFile = $langFile;
    }
    public function getLangFile() {
        return $this->langFile;
    }
    public function setLang(String $langShort) {
        $this->langShort = $langShort;
    }
    public function getLang() {
        return $this->langShort;
    }
    public function getSettings() {
        if($this->systemSettings === null){
            $this->systemSettings = $this->settingsController->loadSettings(true);
        }

        return $this->systemSettings;
    }

/* GLOBAL HELP FUNCTIONS */
    public function getModuleConfig(String $moduleName, String $configPart = null) {
        include(MODULE_DIR.$moduleName."/config.php");
        return ($configPart !== null) ? (isset($config[$configPart])) ? $config[$configPart] : null : $config;
    }
    public function isActiveModule(String $moduleName) {
        return $this->modulesController->isActive($moduleName);
    }
    public function getProjectPath(?String $langShort = null, Bool $stripLastSlash = false){
        $langShort = (!empty($langShort)) ? $langShort : $this->getLang();
        $projectPath = ((env('API_ENABLED') === true && env('PROJECT_DISABLED') === true ) ? env('PROJECT_REMOTE_URL') : SERVER_PROTOCOL."://".SERVER_NAME).'/';

        if($this->getModuleConfig("localeSatellite")){
            $settingsClass = $this->returnSettingsController();
            $allSettings = $settingsClass->getSettings(true);
            $satelliteDomains = [];

            if(isset($allSettings["localeDomains"])){
                $parsedDomains = explode(";", $allSettings["localeDomains"]);
                if(!empty($parsedDomains)){
                    foreach($parsedDomains as $domain){
                        $domainDetails = explode(",", $domain);
                        if(count($domainDetails) == 2){
                            $satelliteDomains[$domainDetails[0]] = $domainDetails[1];
                        }
                    }
                }
            }
            if(!empty($satelliteDomains)){
                foreach($satelliteDomains as $domain => $domainLang){
                    if($langShort === $domainLang){
                        $projectPath = SERVER_PROTOCOL."://".$domain.'/';
                        break;
                    }
                }
            }
        }

        if($stripLastSlash === true){
            $projectPath = substr($projectPath, 0, -1);
        }

        return $projectPath;
    }
}