<?php
class ScheduleController {

    private $database;
    private $appController;

    private $status;
    private $statusCodes = [
        200 => 'Completed successfully',
        300 => 'No action required',
        301 => 'Partially completed',
        302 => 'Failed',
        400 => 'Unauthorized request'
    ];

    public $schedulesTable;
    public $jobsTable;

    public $dataRows = ["cronJob", "dailyFrequency", "start", "end", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday", "createdDate", "updatedDate"];

    public function __construct(Database $database, AppController $appController) {
        $this->database = $database;
        $this->appController = $appController;

        $this->schedulesTable = databaseTables::getTable("settings", "scheduler");
        $this->jobsTable = databaseTables::getTable("settings", "jobs");

        $this->setStatus(300);
    }

    public function setStatus(Int $statusCode){
        $this->status = $statusCode;
    }
    public function getStatus(){
        return ['code' => $this->status, 'message' => $this->statusCodes[$this->status]];
    }

    public function getOneTask($id) {
        $returnData = $this->database->getQuery("SELECT * FROM `{$this->schedulesTable}` WHERE id = :id;", ["id" => $id], false);
        return $returnData;
    }
    public function getAllTasks($visibility = null) {
        $visibility = ($visibility === null) ? null : (($visibility === false) ? "WHERE visibility = 0" : "WHERE visibility = 1");

        $returnData = $this->database->getQuery("SELECT * FROM `{$this->schedulesTable}` {$visibility} ORDER BY orderNum, id;", null, true, true);
        return $returnData;
    }
    public function confirmTaskRun($jobId, $jobRuns) {
        $this->database->updateQuery("UPDATE `{$this->schedulesTable}`", ["jobRuns" => ($jobRuns+1), "lastRun" => date("Y-m-d H:i:s")], ["id" => $jobId], "WHERE id = :id");
    }
    public function resetTaskRun($jobId) {
        $this->database->updateQuery("UPDATE `{$this->schedulesTable}`", ["jobRuns" => 0], ["id" => $jobId], "WHERE id = :id");

        return 0;
    }

    public function createJob(String $name, String $moduleName, Array $parameters = [], ?Int $dependency = 0, ?String $startDate = null) {
        $info = ["name" => $name, "moduleName" => $moduleName, "parameters" => json_encode($parameters), "dependency" => $dependency, "status" => 'pending', "startDate" => $startDate, "updatedDate" => date('Y-m-d H:i:s')];
        $jobId = $this->database->insertQuery("INSERT INTO `{$this->jobsTable}`", $info);
        return (int) $jobId;
    }
    public function updateJob(Int $id, String $status, ?String $message = null, ?Float $execution = null) {
        $updateValues = ["status" => $status, "message" => $message, "execution" => $execution, "updatedDate" => date("Y-m-d H:i:s")];
        $this->database->updateQuery("UPDATE `{$this->jobsTable}`", $updateValues, ["id" => $id], "WHERE id = :id");
        return true;
    }
    public function getOneJob(Int $id) {
        return $this->database->getQuery("SELECT * FROM `{$this->jobsTable}` WHERE id = :id", ["id" => $id], false, true);
    }
    public function getJobs(?String $status = null, ?Int $limit = null, ?String $startDate = null, Array $dependentJobs = []) {
        $whereBind = null;
        $whereCondition = null;

        if($status !== null){
            $whereCondition .= ($whereCondition === null) ? "WHERE " : " AND ";
            $whereCondition .= "status = :status";
            $whereBind["status"] = $status;
        }
        if($startDate !== null){
            $whereCondition .= ($whereCondition === null) ? "WHERE " : " AND ";
            $whereCondition .= "(startDate IS NULL OR startDate <= :startDate)";
            $whereBind["startDate"] = $startDate;
        }
        if(!empty($dependentJobs)){
            $whereCondition .= ($whereCondition === null) ? "WHERE " : " AND ";
            $whereCondition .= "id NOT IN (".implode(",", $dependentJobs).")";
        }
        if($limit !== null){
            $limit = "LIMIT {$limit}";
        }

        return $this->database->getQuery("SELECT * FROM `{$this->jobsTable}` {$whereCondition} {$limit}", $whereBind, true, true);
    }

    public function runJob(Int $id, Array $jobInfo = []) {
        $isReady = false;

        if(empty($jobInfo)){
            $jobInfo = $this->getOneJob($id);
        }

        if(!empty($jobInfo)){
            $isReady = ($jobInfo["status"] !== 'finished') ? true : false;
            if($jobInfo["dependency"] > 0){
                $dependency = $this->getOneJob($jobInfo["dependency"]);
                if(!empty($dependency) && $dependency["status"] !== 'finished'){
                    return null;
                }
            }
        }

        if($isReady === true){
            $jobUpdatedInfo = $this->getOneJob($jobInfo["id"]);
            if($jobUpdatedInfo['status'] === 'pending'){
                $this->updateJob($jobInfo["id"], "in-progress");

                $isFinished = false;
                $jobExecutionTime = 0;
                $jobPath = MODULE_DIR.$jobInfo["moduleName"]."/cron/".$jobInfo["name"].".php";

                if(file_exists($jobPath)){
                    $jobStartTime = microtime(true);
                    $database = $this->database;
                    $appController = $this->appController;
                    $parameters = json_decode(stripslashes($jobInfo["parameters"]), true);

                    $isFinished = include($jobPath);

                    $jobExecutionTime = round(microtime(true) - $jobStartTime, 2);
                }

                $this->updateJob($jobInfo["id"], ($isFinished === true ? 'finished' : 'failed'), null, $jobExecutionTime);

                return $isFinished;
            }else{
                return null;
            }
        }

        return false;
    }
    public function checkAndClearJobs() {
        $this->database->deleteQuery("DELETE FROM `{$this->jobsTable}`", ["status" => "finished"], "WHERE status = :status AND updatedDate < NOW() - INTERVAL 24 HOUR");

        $updateValues = ["status" => "pending", "message" => "Restarted ".date("Y-m-d H:i:s"), "updatedDate" => date("Y-m-d H:i:s")];
        $this->database->updateQuery("UPDATE `{$this->jobsTable}`", $updateValues, ["statusOld" => "in-progress"], "WHERE status = :statusOld AND updatedDate < NOW() - INTERVAL 2 HOUR");
    }
}