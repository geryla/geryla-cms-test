<?php
    class RequestsController {
        private $database;
        private $appController;
        private $localEngine;
        private $localizationClass;

        private $error = null;
        private $errorCodes = [
            400 => 'Authorization failed - permissions error',
            401 => 'Invalid request name',
            402 => 'Invalid or missing data array',
            403 => 'Invalid response type (DOM or JSON allowed)',
        ];

        private $requestName;
        private $requestModuleName;
        private $requestPath;
        private $requestData;
        private $requestFiles;
        private $requestResponseType;
        private $isModuleRequest;
        private $isAdminRequest;
        private $isValidRequest;

        public function __construct(Database $database, AppController $appController) {
            $this->database = $database;
            $this->appController = $appController;
            $this->localEngine = $appController->returnLocaleEngine();
            $this->localizationClass = $appController->returnLocalizationClass();
        }

        private function setError(Int $errorCode){
            $this->error = ["code" => $errorCode, "message" => $this->errorCodes[$errorCode]];
        }
        public function getError(){
            return $this->error;
        }

        public function validate(Array $requestData, Array $requestFiles = null, Bool $verifyAdminPerms = true){
            $this->isValidRequest = false;

            if($verifyAdminPerms === true){
                if($this->allowAccess($this->database, $this->localEngine) === false){
                    return false;
                }
            }

            $this->isValidRequest = $this->validateRequestData($requestData, $requestFiles);
            return $this->isValidRequest;
        }
        public function getRequestPath(){
            if($this->isValidRequest === false){
                return false;
            }

            return $this->requestPath.'/'.$this->requestName.'.php';
        }
        public function getResponseType(){
            return $this->requestResponseType;
        }
        public function getRequestInfo(){
            return ["name" => $this->requestName, "moduleName" => $this->requestModuleName, "data" => $this->requestData, "files" => $this->requestFiles];
        }

        private function allowAccess(Database $database, LocaleEngine $localeEngine){
            //$adminEngine = new adminEngine($database, $localeEngine);

            $isValidAdmin = true;
            if($isValidAdmin === false){
                $this->setError(400);
            }

            return $isValidAdmin;
        }
        private function validateRequestData($postData, $postFiles = null){
            if(!isExisting('requestName', $postData)){
                $this->setError(401);
                return false;
            }
            if(!isExisting('data', $postData)){
                $this->setError(402);
                return false;
            }

            $this->isModuleRequest = (isExisting('module', $postData) && $this->appController->isActiveModule($postData["module"]));
            $this->isAdminRequest = ($this->isModuleRequest === false && (isExisting('admin', $postData) && boolval($postData["admin"]) === true));

            $this->requestName = $postData["requestName"];
            $this->requestModuleName = useIfExists($postData, null, 'module');
            $this->requestData = $this->protectRequestInputs($postData["data"]);
            $this->requestFiles = $postFiles;
            $this->requestPath = ($this->isModuleRequest === true) ? MODULE_DIR.$postData["module"].'/'.REQUESTS : (($this->isAdminRequest === true) ? ADMIN_DIR.REQUESTS : RESOURCES_DIR.REQUESTS);
            $this->requestResponseType = strtolower(((isExisting('response', $postData)) ? $postData["response"] : 'DOM'));

            return true;
        }
        private function protectRequestInputs($dataValues){
            $protectedValues = [];

            if(is_string($dataValues)){
                $jsonDecoded = json_decode($dataValues);
                $dataValues = ($jsonDecoded != null) ? $jsonDecoded : $dataValues;
            }

            if(!empty($dataValues)){
                foreach($dataValues as $key => $value){
                    if(is_string($value)){
                        if (strpos($value, '&') && strpos($value, '=')) {
                            parse_str($value, $parsedValues);
                            if(!empty($parsedValues)){
                                $protectedValues[$key] = $this->protectRequestInputs($parsedValues);
                                continue;
                            }
                        }
                    }

                    if(is_array($value)){
                        $protectedValues[$key] = $this->protectRequestInputs($value);
                        continue;
                    }

                    $protectedValues[$key] = $this->database->injectionProtect($value);
                }
            }

            return $protectedValues;
        }

// Global actions for system requests
        public function deleteData($objectId, $table) {
            return $this->database->deleteQuery("DELETE FROM `{$table}`", ["id" => $objectId], "WHERE id = :id");
        }
        public function deleteLocale($objectId, $table) {
            return $this->database->deleteQuery("DELETE FROM `{$table}`", ["id" => $objectId], "WHERE associatedId = :id");
        }
        public function deleteGlobalLocale($objectId, $objectType) {
            return $this->database->deleteQuery("DELETE FROM `{$this->localizationClass->translationsTable}`", ["id" => $objectId, "type" => $objectType], "WHERE objectId = :id AND objectType = :type");
        }
        public function switchVisibility($objectId, $table) {
            $contentData = $this->database->getQuery("SELECT * FROM `{$table}` WHERE id = :id;", ["id" => $objectId], false);

            if (isset($contentData["visibility"])) {
                $setVisibility = ($contentData["visibility"] == 0) ? 1 : 0;
            } else {
                return false;
            }

            $response = $this->database->updateQuery("UPDATE `{$table}`", array("visibility" => $setVisibility), ["id" => $objectId], "WHERE id = :id");
            return ($response !== true) ? false : $setVisibility;
        }
        public function sortItems($objectId, $table, $orderNum, $customColumn = 'id') {
            return $this->database->updateQuery("UPDATE `{$table}`", ["orderNum" => $orderNum], ["id" => $objectId], "WHERE {$customColumn} = :id");
        }

    }