<?php

namespace Api\Auth;

class ApiFactory extends \ApiController {
    private $authController;

    private $requestMethods;

    private $errorCodes = [
        400 => 'The access key could not be generated',
        401 => 'The access key could not be refreshed',
    ];

    public function __construct(\Database $database, \AppController $appController, Array $requestData) {
        parent::__construct($database, $appController, $requestData);
        $this->authController = new AuthController($database, $appController);

        $this->requestMethods = include(__DIR__ . '/requests.php');
    }

    public function getErrorCodes() {
        return $this->returnErrorCodes($this->errorCodes);
    }

    public function validate(){
        return $this->validateRequest($this->requestData, $this->requestMethods);
    }
    public function proceedRequest() {

        switch ($this->requestData["action"]) {
            case "getAccessKey":
                $this->getAccessKey();
                break;
            case "refreshAccessKey":
                $this->refreshAccessKey();
                break;
            default:
                $this->setState(310);
                return false;
        }

        $this->closeAuthRequest();
        return true;
    }

    private function getAccessKey() {
        $accessKey = $this->authController->generateAccessKey($this->requestData["origin"]);
        if (empty($accessKey)) {
            $this->setState(400);
            return false;
        }

        $this->setResponseData($accessKey);
        return true;
    }

    private function refreshAccessKey() {
        $refreshedKey = $this->authController->refreshAccessKey($this->requestData["origin"]);
        if ($refreshedKey === false) {
            $this->setState(401);
            return false;
        }

        $this->setResponseData($refreshedKey);
        return true;
    }
}