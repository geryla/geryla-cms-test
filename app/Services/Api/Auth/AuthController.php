<?php
namespace Api\Auth;

class AuthController{
    private $appController;
    private $database;

    private $sessionTableName;
    private $requestSessionInfo;
    private $authError = null;
    private $sessionInfo = null;

    public function __construct(\Database $database, \AppController $appController) {
        $this->database = $database;
        $this->appController = $appController;

        $this->sessionTableName = \DatabaseTables::getTable('settings', 'apiSessions');
    }

    public function getAuthError(){
        return $this->authError;
    }
    public function getSessionInfo(){
        return $this->sessionInfo;
    }

    public function getRequestSessionInfo(){
        return $this->requestSessionInfo;
    }
    public function getSessionExpiration($returnFormat) {
        $sessionExpire = (getSessionLifeTime() / 60) - 5;
        $date = new \DateTime();
        $date->modify("+" . $sessionExpire . " minutes");

        return $date->format($returnFormat);
    }
    public function initSession($sessionId, $start = true, $destroy = false) {
        $sessionData = $_SESSION;
        session_write_close();
        session_id($sessionId);

        if($start === true){
            session_start();
            if(empty($_SESSION)){
                $_SESSION = $sessionData;
            }
        }
        if($destroy === true){
            session_destroy();
        }
    }

    public function validateAuth() {
        $authToken = null;
        if (isset($_SERVER['HTTP_AUTHORIZATION'])) {
            $authToken = $_SERVER['HTTP_AUTHORIZATION'];
        } elseif (isset($_SERVER['REDIRECT_HTTP_AUTHORIZATION'])) {
            $authToken = $_SERVER['REDIRECT_HTTP_AUTHORIZATION'];
        }

        if (empty($authToken)) {
            $this->authError = 311;
            return false;
        }

        if (!preg_match('/Token\s(\S+)/', $authToken, $matches)) {
            $this->authError = 312;
            return false;
        }

        if(!$matches[1]){
            $this->authError = 313;
            return false;
        }

        $isValid = $this->verifyAccessKey($matches[1]);
        if($isValid === false){
            return false;
        }

        if(empty($this->requestSessionInfo) || $this->requestSessionInfo === null){
            $this->authError = 316;
            return false;
        }

        return true;
    }
    private function verifyAccessKey($accessKey) {
        if ($accessKey) {
            $sessionInfo = $this->database->getQuery("SELECT * FROM `{$this->sessionTableName}` WHERE accessKey = :accessKey", ["accessKey" => $accessKey], false);
            $this->sessionInfo = $sessionInfo;
            if (!empty($sessionInfo)) {
                $date = new \DateTime();
                $currentTime = $date->format('Y-m-d H:i:s');
                if ($sessionInfo["expire"] > $currentTime) {
                    $this->requestSessionInfo = $sessionInfo;
                    return true;
                }
                $this->authError = 315;
                $this->initSession($sessionInfo["sessionId"], false, true);
                return false;
            }
            $this->authError = 314;
            return false;
        }
        $this->authError = 312;
        return false;
    }

    private function encodeAccessKey($hashData){
        $hashData["rand"] = rand(0000, 9999).time().rand(0000, 9999);
        $json = json_encode($hashData, JSON_UNESCAPED_SLASHES);
        return str_replace('=', '', strtr(base64_encode($json), '+/', '-_'));
    }
    private function decodeAccessKey($accessKey){
        return json_decode(base64_decode($accessKey), JSON_UNESCAPED_SLASHES);
    }

    public function generateAccessKey($requestOrigin) {
        $currentSessionId = session_id();
        $newSessionId = session_create_id();

        $this->initSession($newSessionId, true);
        $this->initSession($currentSessionId, false);

        $hashData["origin"] = $requestOrigin;
        $hashData["expire"] = $this->getSessionExpiration('Y-m-d H:i:s');
        $accessKey = $this->encodeAccessKey($hashData);

        $this->database->insertQuery("INSERT INTO `{$this->sessionTableName}`", ["accessKey" => $accessKey, "sessionId" => $newSessionId, "expire" => $hashData["expire"]]);

        $response = ["accessKey" => $accessKey, "expire" => $hashData["expire"]];
        return $response;
    }
    public function refreshAccessKey($requestOrigin) {
        $isValid = $this->validateAuth();
        if($isValid !== true){
            return false;
        }

        $accessKey = $this->requestSessionInfo["accessKey"];
        $hashData = $this->decodeAccessKey($accessKey);
        if($hashData["origin"] !== $requestOrigin){
            return false;
        }

        $hashData["expire"] = $this->getSessionExpiration('Y-m-d H:i:s');
        $newAccessKey = $this->encodeAccessKey($hashData);

        $this->database->updateQuery("UPDATE `{$this->sessionTableName}`", ["accessKey" => $newAccessKey, "expire" => $hashData["expire"]], ["sessionId" => $this->requestSessionInfo["sessionId"]], "WHERE sessionId = :sessionId");
        $response = ["accessKey" => $newAccessKey, "expire" => $hashData["expire"]];
        return $response;
    }

    public function regenerateSessionId($oldSessionId, $newSessionId) {
        $sessionData = $this->database->getQuery("SELECT * FROM `{$this->sessionTableName}` WHERE `sessionId` = :sessionId", ["sessionId" => $oldSessionId], false);
        if(!empty($sessionData)){
            $sessionData["sessionId"] = $newSessionId;

            $this->database->deleteQuery("DELETE FROM `{$this->sessionTableName}`", ["sessionId" => $oldSessionId], "WHERE sessionId = :sessionId");
            $this->database->insertQuery("INSERT INTO `{$this->sessionTableName}`", $sessionData);
        }
        return true;
    }

    public function clearExpiredAccesses() {
        $date = new \DateTime();
        $currentTime = $date->format('Y-m-d H:i:s');
        $this->database->deleteQuery("DELETE FROM `{$this->sessionTableName}`", ["expireTime" => $currentTime], "WHERE expire < :expireTime");
    }
}