<?php

return [
    "getAccessKey" => [
        "path" => "/auth/getAccessKey",
        "authorized" => false,
        "cache" => false,
        "method" => "GET",
    ],
    "refreshAccessKey" => [
        "path" => "/auth/refreshAccessKey",
        "authorized" => true,
        "cache" => false,
        "method" => "GET",
    ],
];