<?php
namespace Debugger;
use Tracy;

class SqlLogs implements Tracy\IBarPanel{

    private $database;
    private $langFile;


    public function __construct(\Database $database, \AppController $appController) {
        $this->database = $database;
        $this->langFile = $appController->getLangFile();


    }

    public function getTab() {
        $icon = (!empty($this->database->getErrors())) ? '<b style="color: red;"> ! </b>' : null;
        return '| '.$icon.translate("tracyPanel-logs");
	}
    public function getPanel() {
        $allQueries = $this->database->getQueries();
        $allErrors = $this->database->getErrors();

        $html = '<div class="tracy-inner">';
            $html .= '<div class="tracy-inner-container">';

                if(!empty($allErrors)){
                    $html .= '<table>';
                        $html .= '<tr>';
                            $html .= '<th colspan="4" style="background-color: red;color: white;">'.translate("tracyPanel-logs-table-error").': '.count($allErrors).'x</th>';
                        $html .= '</tr>';
                        foreach($allErrors as $error){
                            $html .= '<tr>';
                                $html .= '<td>'.$error[0].'</td>';
                                $html .= '<td>'.$error[1].'</td>';
                                $html .= '<td>'.$error[2].'</td>';
                                $html .= '<td>'.$error[3].'</td>';
                            $html .= '</tr>';
                        }
                    $html .= '</table>';
                    $html .= '<br />';
                }

                $html .= '<table>';
                    $html .= '<tr>';
                        $html .= '<th>'.translate("tracyPanel-logs-table-list").': '.count($allQueries).'x</th>';
                    $html .= '</tr>';
                    foreach($allQueries as $query){
                        $html .= '<tr>';
                            $html .= '<td>'.$query.'</td>';
                        $html .= '</tr>';
                    }
                $html .= '</table>';

            $html .= '</div>';
        $html .= '</div>';

        return $html;
	}
}