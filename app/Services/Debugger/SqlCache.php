<?php
namespace Debugger;
use Tracy;

class SqlCache implements Tracy\IBarPanel{

    private $database;
    private $langFile;
    private $icon = 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gU3ZnIFZlY3RvciBJY29ucyA6IGh0dHA6Ly93d3cub25saW5ld2ViZm9udHMuY29tL2ljb24gLS0+DQo8IURPQ1RZUEUgc3ZnIFBVQkxJQyAiLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4iICJodHRwOi8vd3d3LnczLm9yZy9HcmFwaGljcy9TVkcvMS4xL0RURC9zdmcxMS5kdGQiPg0KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB2aWV3Qm94PSIwIDAgMTAwMCAxMDAwIiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAxMDAwIDEwMDAiIHhtbDpzcGFjZT0icHJlc2VydmUiPg0KPG1ldGFkYXRhPiBTdmcgVmVjdG9yIEljb25zIDogaHR0cDovL3d3dy5vbmxpbmV3ZWJmb250cy5jb20vaWNvbiA8L21ldGFkYXRhPg0KPGc+PHBhdGggZD0iTTY2My40LDEwTDIyNy44LDUyMi40bDI1Ni4yLDY0TDMzNi43LDk5MGw0MzUuNi00NjEuMmwtMjExLjQtOTYuMUw2NjMuNCwxMHoiLz48L2c+DQo8L3N2Zz4=';

    public function __construct(\Database $database, \AppController $appController) {
        $this->database = $database;
        $this->langFile = $appController->getLangFile();
    }

    public function getTab() {
        $html = '<img src="'.$this->icon.'" style="width: 16px; height: 16px;" alt="'.translate("tracyPanel-cache").'" />'.translate("tracyPanel-cache");
        return $html;
	}
    public function getPanel() {
        $totalTime = 0;
        $cacheQueries = $this->database->getQueryCounter();
        $simpleTotal = $cachedTotal = 0;

        $html = '<div class="tracy-inner">';
            $html .= '<div class="tracy-inner-container">';
                $html .= '<span><b>'.translate("tracyPanel-cache-table-SQL").':</b></span><br /><br />';
                $html .= '<table>';
                $html .= '<tr>';
                    $html .= '<th>'.translate("tracyPanel-cache-table-group").'</th>';
                    $html .= '<th>'.translate("tracyPanel-cache-table-simple").'</th>';
                    $html .= '<th>'.translate("tracyPanel-cache-table-cache").'</th>';
                    $html .= '<th style="text-align: right;">'.translate("tracyPanel-cache-table-time").'</th>';
                $html .= '</tr>';
                foreach($cacheQueries["simpleQueries"] as $groupName => $simpleQuery){
                    $diff = ($simpleQuery - $cacheQueries["cachedQueries"][$groupName]);
                    $style = ($diff > 0) ? "red" : "green";
                    $time = ($cacheQueries["time"][$groupName] * 1000);

                    $html .= '<tr>';
                        $html .= '<td>'.$groupName.'</td>';
                        $html .= '<td>'.$simpleQuery.'</td>';
                        $html .= '<td>'.$cacheQueries["cachedQueries"][$groupName].' <b style="color: '.$style.';">('.$diff.')</b></td>';
                        $html .= '<td style="text-align: right;">'.round($time, 2).'ms</td>';
                    $html .= '</tr>';

                    $simpleTotal = $simpleTotal + $simpleQuery;
                    $cachedTotal = $cachedTotal + $cacheQueries["cachedQueries"][$groupName];
                    $totalTime = $totalTime + $time;
                }
                $html .= '<tr>';
                    $html .= '<td><b>'.translate("tracyPanel-cache-table-total").'</b></td>';
                    $html .= '<td><b>'.$simpleTotal.'</b></td>';
                    $html .= '<td><b>'.$cachedTotal.'</b></td>';
                    $html .= '<td style="text-align: right;"><b>'.round($totalTime, 2).'ms</b></td>';
                $html .= '</tr>';
                $html .= '</table>';
                $html .= '<br /><a href="?afterDeploy=true" target="_blank"><i>'.translate("tracyPanel-cache-regenerate").'</i></a>';
            $html .= '</div>';
        $html .= '</div>';

        return $html;
	}
}