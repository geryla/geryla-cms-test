<?php

class SystemCache extends Cache{

    private $dataType = 'System';

    public function __construct() {
        parent::__construct();
    }

    private function isExist(String $groupName, String $cacheName, Bool $purge = true){
        $response = null;
        $duration = 1440 * 7; // 24h * 7 days
        $path = $this->getPath($this->dataType.'/'.$groupName, $cacheName);

        if(file_exists($path)){
            $data = json_decode(file_get_contents($path), true);
            if($data){
                $elapsedTime = time() - $data["time"];
                if ($elapsedTime < ($duration * 60)) {
                    $response = $data["response"];
                }else{
                    if($purge === true){
                        unlink($path);
                    }
                }
            }else{
                unlink($path);
            }
        }

        return $response;
    }

    public function get(String $groupName, String $cacheName){
        $response = $this->isExist($groupName, $cacheName, true);
        return $response;
    }
    public function set(String $groupName, String $cacheName, Array $data = []){
        $path = $this->getPath("{$this->dataType}/{$groupName}", $cacheName);
        $data = $this->createCache($data);

        $isStored = file_put_contents($path, json_encode($data));
        return ($isStored !== false);
    }
    public function remove(String $groupName, String $cacheName = null){
        $this->removeCache($this->dataType, $groupName, $cacheName);
    }

}