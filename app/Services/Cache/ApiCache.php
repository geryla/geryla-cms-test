<?php

class ApiCache extends Cache {

    private $dataType = 'Api';

    public function __construct() {
        parent::__construct();
    }

    private function hash(Array $request) {
        return hash('sha256', json_encode($request));
    }
    public function get(String $moduleName, Array $request) {
        $hash = $this->hash($request);
        $response = $this->isExist($moduleName, $hash);
        return $response;
    }
    public function set(String $moduleName, Array $request, Array $response = []) {
        $hash = $this->hash($request);
        $path = $this->getPath("{$this->dataType}/{$moduleName}", $hash);
        $data = $this->createCache($response);

        $isStored = file_put_contents($path, json_encode($data));
        return ($isStored !== false);
    }

    private function isExist(String $moduleName, String $hash, Bool $purge = true) {
        $response = null;
        $duration = 1440 * 7; // 24h * 7 days
        $path = $this->getPath("{$this->dataType}/{$moduleName}", $hash);

        if (file_exists($path)) {
            $data = json_decode(file_get_contents($path), true);
            if ($data) {
                $elapsedTime = time() - $data["time"];
                if ($elapsedTime < ($duration * 60)) {
                    $response = $data["response"];
                } else {
                    if ($purge === true) {
                        unlink($path);
                    }
                }
            } else {
                unlink($path);
            }
        }


        return $response;
    }

    public function verifyApi(String $queryTable){
        $tablesData = databaseTables::getTables();
        $moduleApiName = null;

        foreach ($tablesData as $moduleName => $tables) {
            foreach ($tables as $tableName => $tableInfo) {
                if($queryTable === $tableInfo["name"]){
                    $moduleApiName = $moduleName;
                }
            }
        }

        if($moduleApiName !== null){
            $this->removeCache($this->dataType, $moduleApiName);
            return $moduleApiName;
        }
        return null;
    }
}