<?php

class EntityCache extends Cache{

    private $dataType = 'Entity';
    private $folderType = [0 => 'public', 1 => 'private'];

    public function __construct() {
        parent::__construct();
    }

    private function create(Array $entity){
        return ["response" => $entity, "time" => time()];
    }
    private function getFolder(?Int $userId = null, Bool $setFolder = true){
        $folderType = null;
        if($setFolder === true){
            $folderType = $this->folderType[0];
            if($userId !== null){
                $folderType = "{$this->folderType[1]}/{$userId}";
            }
        }

        return $folderType;
    }

    public function get(String $type, ?String $id, String $lang, ?Int $userId = null, Bool $setFolder = true){
        $type = str_replace('/', '', $type);

        if(in_array($type, ['product', 'productCategory', 'post'])){
            $response = $this->isExist($type, $id, $lang, $userId, $setFolder);
        }else{
            $response = null;
        }

        return $response;
    }
    public function set(String $type, ?String $id, String $lang, Array $entity = [], ?Int $userId = null, Bool $setFolder = true){
        $path = $this->getPath("{$this->dataType}/{$type}/{$id}/{$this->getFolder($userId, $setFolder)}", $lang);
        $data = $this->create($entity);

        $isStored = file_put_contents($path, json_encode($data));
        return ($isStored !== false);
    }
    private function isExist(String $type, ?String $id, String $lang, ?Int $userId = null, Bool $setFolder = true){
        $response = null;
        $duration = 1440 * 7; // 24h * 7 days
        $path = $this->getPath("{$this->dataType}/{$type}/{$id}/{$this->getFolder($userId, $setFolder)}", $lang);

        if(file_exists($path)){
            $data = json_decode(file_get_contents($path), true);
            if($data){
                $elapsedTime = time() - $data["time"];
                if ($elapsedTime < ($duration * 60)) {
                    $response = $data["response"];
                }else{
                    unlink($path);
                }
            }else{
                unlink($path);
            }
        }


        return $response;
    }

    public function verifyEntity(String $queryTable, String $query){
        $tablesData = databaseTables::getTables();
        $tablesByEntity = $entitiesWithTables = [];
        foreach ($tablesData as $moduleName => $tables) {
            foreach ($tables as $tableName => $tableInfo) {
                if(isset($tableInfo["entityBonds"]) && !empty($tableInfo["entityBonds"])){
                    $tablesByEntity[$tableInfo["name"]] = $tableInfo["entityBonds"];
                }
            }
        }

        if(isset($tablesByEntity[$queryTable])){
            foreach($tablesByEntity[$queryTable] as $entityName => $entityInfo){
                $customIdentification = (isset($entityInfo["identification"])) ? $entityInfo["identification"] : null;
                $entityId = $this->getId($query, $customIdentification);

                if($entityInfo["major"] === true){//TODO: Why it delete cache on create? Do a dump
                    $this->removeCache($this->dataType, "{$entityName}/{$entityId}/public");
                    $this->removeCache($this->dataType, "{$entityName}/{$entityId}/private");
                    $this->removeCache($this->dataType, "images/{$entityName}/{$entityId}");

                    $database = new Database();
                    $categoryInfo = $database->getQuery("SELECT * FROM `products-categories` WHERE `id` = :id", ["id" => $entityId], false, true);
                    if(!empty($categoryInfo["parentId"])){
                        $parentId = $categoryInfo["parentId"];
                        $this->removeCache($this->dataType, "{$entityName}/{$parentId}/public");
                        $this->removeCache($this->dataType, "{$entityName}/{$parentId}/private");
                        $this->removeCache($this->dataType, "images/{$entityName}/{$parentId}");
                    }

                    //$data = ['table' => $queryTable, 'query' => $query, 'entity' => $entityId, 'identify' => $customIdentification];
                    //file_put_contents(ROOT.'/debug/'.date('d-m-H-i-s').'_'.$this->dataType.'_'.$entityName.'_'.$entityId.'.txt', json_encode($data));
                }else{
                    //TODO: Get Bonds and delete bonded Entities
                    //bdump($query);
                    //bdump($entityId);
                }
            }
        }
    }
    private function getId(String $query, ?String $customIdentification = null) {
        if (preg_match('/WHERE(.*?)(?:;|$)/s', $query, $matches)) {
            $whereClause = trim($matches[1]);
            $identification = ($customIdentification !== null) ? $customIdentification : 'id|associatedId';

            $idFound = preg_match('/\b('.$identification.')\s*=\s*\'?(\d+)\'?\b/i', $whereClause, $paramMatches);
            return ($idFound) ? $paramMatches[2] : null;
        }

        return null;
    }
}