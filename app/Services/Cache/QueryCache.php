<?php

class QueryCache extends Cache{

    private $dataType = 'Query';

    private $longTimeCache = ['modules', 'templates', 'language', 'language-translations', 'admins', 'admins-levels', 'emailTemplates', 'maintenance', 'schedules', 'redirects'];
    private $noCache = ['access', 'apiSessions', 'logs', 'migrations'];

    public function __construct() {
        parent::__construct();
    }

    private function hash(String $query, Array $bind = null){
        $request = $this->bindQuery($query, $bind);
        return hash('sha256', json_encode($request));
    }
    private function bindQuery(String $query, Array $bind = null){
        if(!empty($bind)){
            foreach($bind as $name => $value){
                $protectedValue = useIfExists($this->protect($value), '');
                $query = str_replace(":{$name}", $protectedValue, $query);
            }
        }

        return $query;
    }
    private function protect(String $value = null){
        if($value !== null){
            $value = addslashes($value);
            $value = htmlspecialchars(htmlspecialchars($value));
            $value = str_replace('`','\`',$value);
        }

        return $value;
    }
    private function getTable($query, $part, $index = 1) {
        $tableName = null;
        $fromIndex = stripos($query, $part);

        for ($i = 1; $i < $index; $i++) {
            $fromIndex = stripos($query, $part, $fromIndex + 1);
        }

        if ($fromIndex !== false) {
            $fromIndex += strlen($part) + 1; // Skip 'FROM ' and the following whitespace
            $closingApostropheIndex = strpos($query, '`', $fromIndex + 1);

            if ($closingApostropheIndex !== false) {
                $queryTable = substr($query, $fromIndex, $closingApostropheIndex - $fromIndex + 1);
                $tableName = str_replace('`', '', $queryTable);

                // Check if the table name contains a space
                if (strpos($tableName, ' ') !== false) {
                    // If it contains a space, recursively try to find the next occurrence
                    $tableName = $this->getTable($query, $part, $index + 1);
                }
            }
        }

        return ($tableName === null) ? '' : $tableName;
    }
    private function getDuration(String $tableName){
        if(in_array($tableName, $this->longTimeCache)){
            $duration = 1440 * 2; // 48h
        }elseif(in_array($tableName, $this->noCache)){
            $duration = 0;
        }elseif(strpos($tableName, '-lang') !== false) {
            $duration = 1440 * 3; // 72h
        }else{
            $duration = $this->ttl;
        }

        return $duration;
    }

    public function get(String $query, Array $bind = null, Bool $purge = true){
        $hash = $this->hash($query, $bind);
        $response = $this->isExist($this->bindQuery($query, $bind), $hash, $purge);
        return $response;
    }
    public function set(String $query, Array $bind = null, Array $response = []){
        $hash = $this->hash($query, $bind);
        $table = $this->getTable($query, 'FROM');
        $duration = $this->getDuration($table);
        if($duration === 0){
            return true;
        }

        $isStored = $this->setSingle($response, $this->bindQuery($query, $bind), $hash);
        return ($isStored !== false);
    }
    private function isExist(String $query, String $hash, Bool $purge = true){
        $response = null;
        $table = $this->getTable($query, 'FROM');
        $folderPath = $this->dataType.'/'.$table;
        $objectId = $this->getQueryId($query);
        if($objectId !== null){
            $folderPath .= "/{$objectId}";
        }

        $duration = $this->getDuration($table);
        if($duration > 0){
            $path = $this->getPath($folderPath, $hash);
            if(file_exists($path)){
                $data = json_decode(file_get_contents($path), true);
                if($data){
                    $elapsedTime = time() - $data["time"];
                    if ($elapsedTime < ($duration * 60)) {
                        $response = $data["response"];
                        if(isset($data["next"])){
                            $nextResponse = $this->isExist($query, $data["next"], $purge);
                            if($nextResponse !== null){
                                $response = array_merge($response, $nextResponse);
                            }
                        }
                    }else{
                        if($purge === true){
                            unlink($path);
                        }
                    }
                }else{
                    unlink($path);
                }
            }
        }

        return $response;
    }
    private function setSingle(Array $data, String $query,  String $hash){
        $folderPath = $this->dataType.'/'.$this->getTable($query, 'FROM');
        $objectId = $this->getQueryId($query);
        if($objectId !== null){
            $folderPath .= "/{$objectId}";
        }

        $cachePath = $this->getPath($folderPath, $hash);
        $data = $this->createCache($data);
        $isStored = file_put_contents($cachePath, json_encode($data));

        return $isStored;
    }

    public function remove(String $query, String $queryNeedle){
        $tableName = $this->getTable($query, $queryNeedle);
        if($tableName){
            /*$queryIdentifications = $this->getQueryMultipleIdentifications($query);
            if(!empty($queryIdentifications)){
                foreach($queryIdentifications as $identification => $key){
                    $queryCachePath = $tableName."/{$identification}_{$key}";
                    $this->removeCache($this->dataType, $queryCachePath, null);
                }
                $this->removeUnsortedCache($this->dataType, $tableName);
            }else{
                $this->removeCache($this->dataType, $tableName, null);
            }*/
            $this->removeUnsortedCache($this->dataType, $tableName);
            $this->removeCache($this->dataType, $tableName, null);

            $entityCache = new EntityCache();
            $entityCache->verifyEntity($tableName, $query);

            $apiCache = new ApiCache();
            $apiCache->verifyApi($tableName);

            if($tableName === 'products-categories-lang' || $tableName === 'products-categories'){
                $this->removeCache('Api', 'contentEditor');
            }
            if($tableName === 'users' || $tableName === 'users-control'){
                $this->removeCache('Api', 'optysPartner');
            }
            if($tableName === 'contenteditor-labels' || $tableName === 'contenteditor-labelshook'){
                $this->removeCache('Api', 'contentEditor');
            }
        }
    }
    private function getQueryId(String $query, ?String $customIdentification = null) {
        if (preg_match('/WHERE(.*?)(?:;|$)/s', $query, $matches)) {
            $whereClause = trim($matches[1]);
            $identification = ($customIdentification !== null) ? $customIdentification : 'id|associatedId|objectId';

            $idFound = preg_match('/\b('.$identification.')\s*=\s*\'?(\d+)\'?\b/i', $whereClause, $paramMatches);
            return ($idFound) ? $paramMatches[2] : null;
        }

        return null;
    }
}