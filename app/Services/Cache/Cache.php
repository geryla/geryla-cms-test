<?php

    class Cache {
        protected $folder;
        protected $ttl;

        public function __construct() {
            $this->folder = FILE_CACHE;
            $this->ttl = useIfExists(env('CACHE_TTL'), 10);
        }

        protected function checkDirectory(String $dataType){
            $folderPath = "{$this->folder}{$dataType}";
            if (!is_dir($folderPath)) {
                mkdir($folderPath, 0777, true);
            }

            return $folderPath;
        }
        protected function getPath(String $dataType, String $hash, String $ext = 'txt'){
            return "{$this->checkDirectory($dataType)}/{$hash}.{$ext}";
        }
        protected function createCache(Array $response, Array $parameters = []){
            return array_merge($parameters, ["response" => $response, "time" => time()]);
        }

        public function removeCache(String $dataType, String $groupName = null, String $cacheName = null, String $ext = 'txt'){
            $folderPath = "{$this->folder}{$dataType}";
            if($groupName !== null){
                $folderPath .= "/{$groupName}";
            }

            if($cacheName === null){
                $this->removeDirectory($folderPath);
            }else{
                if(file_exists("{$folderPath}/{$cacheName}.{$ext}")){
                    unlink("{$folderPath}/{$cacheName}.{$ext}");
                }
            }
        }
        public function removeUnsortedCache(String $dataType, String $groupName) {
            $folderPath = "{$this->folder}{$dataType}/{$groupName}";
            if (is_dir($folderPath)) {
                $objects = scandir($folderPath);
                foreach ($objects as $object) {
                    if ($object != "." && $object != "..") {
                        if (!is_dir($folderPath."/".$object)) {
                            unlink($folderPath."/".$object);
                        }
                    }
                }
            }
        }
        private function removeDirectory($folderPath) {
            if (is_dir($folderPath)) {
                $objects = scandir($folderPath);
                foreach ($objects as $object) {
                    if ($object != "." && $object != "..") {
                        if (is_dir($folderPath."/".$object)) {
                            $this->removeDirectory($folderPath."/".$object);
                        } else {
                            unlink($folderPath."/".$object);
                        }
                    }
                }
                rmdir($folderPath);
            }
        }
    }