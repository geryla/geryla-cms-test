<?php
class moduleLoader{
    private $database;
    private $appController;

    public function __construct(Database $database, AppController $appController) {
        $this->database = $database;
        $this->appController = $appController;
    }

// GET OBJECT DATA FOR TEMPLATE RENDER
    public function createModuleEntity($activeTemplate, $contentTable, $templateInfo, $customThumbnail = null, $optimizeParams = []){
    // Return if no-data given
        if( empty($templateInfo) ){
            return false;
        }

        $objectToken = "notFound";

    // A) - POST displayed as PAGE (contentEditor)
        if( $contentTable == databaseTables::getTable("contentEditor", "postTable") && $activeTemplate == "page" ){
            $objectToken = "contenteditor-post";
        }
    // B) - BLOG CATEGORY (contentEditor) with replace template
        if( $contentTable == databaseTables::getTable("contentEditor", "categoryTable") && ($activeTemplate == "blog" || $activeTemplate == "blogCategory") ){
            $objectToken = "contenteditor-category";
            $activeTemplate = "blogCategory";
        }
    // C) - POST displayed as BLOG POST (contentEditor) with replace template
        if( $contentTable == databaseTables::getTable("contentEditor", "postTable") && ($activeTemplate == "blog" || $activeTemplate == "blogDetail") ){
            $objectToken = "contenteditor-blogPost";
            $activeTemplate = "blogDetail";
        }
    // D) - PRODUCT CATEGORY - eshop - (products)
        if( $contentTable == databaseTables::getTable("products", "categoryTable") && $activeTemplate == "productCategory" ){
            $objectToken = "products-category";
        }
    // E) - PRODUCT DEATIL - eshop - (products)
        if( $contentTable == databaseTables::getTable("products", "productTable") && $activeTemplate == "productDetail" ){
            $objectToken = "products-detail";
        }
    // F) - CONTENT GROUP - (contentEditor) as multiple views
        if( $contentTable == databaseTables::getTable("contentEditor", "groupTable") ){
            if( !empty($templateInfo["templateName"]) && $activeTemplate == $templateInfo["templateName"] ){
                $objectToken = "contenteditor-group";
            }else{
                $objectToken = "notFound";
            }
        }
    // G) - COURSES - detailed - (courses) - CUSTOM PLUGIN
        if( $contentTable == databaseTables::getTable("courses", "coursesTable") ){
            $objectToken = "courses";
        }

        return $object = (object) $this->getEntity($objectToken, $activeTemplate, $templateInfo, $customThumbnail, $optimizeParams);
    }
    private function getEntity($objectType, $activeTemplate, $templateInfo, $customThumbnail, $optimizeParams){
        $templateClass = $this->appController->returnTemplatesClass();
        $langShort = $this->appController->getLang();

        $rewriteTemplateName = null;
        $parentBreadCrumbs = null;

        switch ($objectType) {
            case "notFound":
                $entityData = new EntityTemplate($templateClass->optimize($templateClass->getTemplateInfoByName("notFound", $langShort, false), $langShort, ["itemThumbnail" => $customThumbnail, "with" => $optimizeParams]));
                $objectData = $entityData->get();
                $activeTemplate = "notFound";
                break;
            case "contenteditor-post":
                $cmsPost = new cmsPost($this->database, $this->appController);
                $objectData = $cmsPost->optimize($templateInfo, null, ["itemThumbnail" => $customThumbnail, "with" => $optimizeParams]);
                $parentBreadCrumbs = ["data" => databaseTables::getTable("contentEditor", "groupTable"), "cross" => databaseTables::getTable("contentEditor", "crossGroupTable"), "identifierObject" => "postId", "identifierParent" => "groupId"];
                break;
            case "contenteditor-blogPost":
                $cmsBlog = new cmsPostBlog($this->database, $this->appController);
                $objectData = $cmsBlog->optimize($templateInfo, null, ["itemThumbnail" => $customThumbnail, "with" => $optimizeParams]);
                $rewriteTemplateName = "blog"; // Replace TEMPLATE for blog detail as BLOG
                break;
            case "contenteditor-category":
                $cmsCategories = new cmsCategories($this->database, $this->appController);
                $objectData = $cmsCategories->optimize($templateInfo, $customThumbnail, $optimizeParams);
                $rewriteTemplateName = "blog"; // Replace TEMPLATE for blog category as BLOG
                break;
            case "contenteditor-group":
                $cmsGroups = new cmsGroups($this->database, $this->appController);
                $objectData = $cmsGroups->optimize($templateInfo, $customThumbnail, $optimizeParams);
                break;
            case "products-category":
                $categoryClass = new categoryClass($this->database, $this->appController);
                $objectData = $categoryClass->optimize($templateInfo, null, true, ["itemThumbnail" => $customThumbnail, "with" => $optimizeParams]);
                break;
            case "products-detail":
                $productClass = new productsClass($this->database, $this->appController);
                $objectData = $productClass->optimize($templateInfo, null, true, ["itemThumbnail" => $customThumbnail, "with" => $optimizeParams]);
                $parentBreadCrumbs = ["data" => databaseTables::getTable("products", "categoryTableLang"), "cross" => databaseTables::getTable("products", "crossCategoryTable"), "identifierObject" => "productId", "identifierParent" => "categoryId"]; // Add tables and table cols for check breadcrumbs
                break;
            /*case "courses": //TODO: Complete module refactor (last in 2017)
                $moduleCourses = new moduleCourses($this->database, $this->appController);
                $optInfo = $moduleCourses->optimize($templateInfo);
                if(isset($_SESSION["security"]["appLogin"]["logIn"])){ // If logged user, check if course is purchased
                    $moduleCoursesUsers = new moduleCoursesUsers($this->database, $this->appController, $moduleCourses);
                    $userInfo = $moduleCoursesUsers->getUser($_SESSION["security"]["appLogin"]["logIn"]);
                    $userCourses = $moduleCoursesUsers->getUserCoursesIds($userInfo["id"], true);
                    $optInfo["userPurchased"] = (in_array($optInfo["id"], $userCourses) OR $optInfo["prices"]["freePrice"] === true) ? true : false;
                }
                $newEntity = new courseFactory($optInfo);
                $objectData = $newEntity->getTemplate();
            break;*/
            default:
                $entityData = new EntityTemplate($templateClass->optimize($templateInfo));
                $objectData = $entityData->get();
        }


        if($objectData["templateName"] === null){
            $objectData["templateName"] = $templateInfo["templateName"];
        }
        if($rewriteTemplateName !== null){
            $objectData["templateName"] = $rewriteTemplateName;
        }

        return ["objectData" => $objectData, "activeTemplate" => $activeTemplate, "parentBreadCrumbs" => $parentBreadCrumbs];
    }
}