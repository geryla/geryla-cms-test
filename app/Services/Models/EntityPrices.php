<?php
class EntityPrices extends Entity {

    protected $extendedParams = [
        "currency" => [
            "type" => "String",
        ],
        "symbol" => [
            "type" => "String",
        ],
        "tax" => [
            "type" => "Integer",
        ],
        "price" => [
            "type" => "String",
        ],
        "price_tax" => [
            "type" => "String",
        ],
        "price_value" => [
            "type" => "Float",
        ],
        "price_value_tax" => [
            "type" => "Float",
        ],
    ];

    public function __construct(Array $objectData) {
        $objectData = $this->extendEntity($objectData);
        $this->build($objectData, $this->extendedParams);
    }
    private function extendEntity(Array $objectData) {
        return $objectData;
    }
}