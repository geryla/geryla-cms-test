<?php
class Entity {

    protected $entityParams = [
        "id" => [
            "type" => "Integer",
        ],
        "langShort" => [
            "type" => "String",
        ],

        "metaKeywords" => [
            "type" => "String",
        ],
        "metaTitle" => [
            "type" => "String",
        ],
        "metaDescription" => [
            "type" => "String",
        ],
        "allowIndex" => [
            "type" => "Integer",
        ],

        "url" => [
            "type" => "String",
        ],
        "urlPrefix" => [
            "type" => "Integer",
        ],

        "templateName" => [
            "type" => "String",
        ],
        "moduleName" => [
            "type" => "String",
        ],

        "createdDate" => [
            "type" => "String",
        ],
        "updatedDate" => [
            "type" => "String",
        ],

        "visibility" => [
            "type" => "Boolean",
        ],
        "orderNum" => [
            "type" => "Integer",
        ],
    ];
    protected $extendedParams = [];
    protected $entityObject = null;

    public function build(Array $objectData, Array $extendedParams = [], Bool $rewrite = false) {
        if (!empty($extendedParams)) {
            $this->extendedParams = $extendedParams;
            if($rewrite === true){
                $this->entityParams = $extendedParams;
            }else{
                $this->entityParams += $extendedParams;
            }
        }
        if (!empty($objectData)) {
            foreach ($objectData as $key => $value) {
                $this->verifyParameter($this->entityParams, $key, $value);
            }
        }
        //var_dump(get_class($this));
    }
    public function get(Bool $returnAsObject = false) {
        if ($returnAsObject === true) {
            return json_decode(json_encode($this->entityObject));
        }

        return $this->entityObject;
    }
    public function getParams($extendedOnly = false) {

        return ($extendedOnly === true) ? $this->extendedParams : $this->entityParams;
    }

    public function restructureObject(Array $objectData, Array $parameters, Array $newObject = [], String $parentKey = null){
        if(!empty($parameters)){
            foreach($parameters as $key => $value){
                $isArrayType = $this->validateArrayType($value["type"]);
                if(!$isArrayType){
                    if($parentKey !== null && isset($objectData[$parentKey])){
                        if(is_array($objectData[$parentKey]) && array_key_exists($key, $objectData[$parentKey])){
                            $newObject[$key] = $objectData[$parentKey][$key];
                            continue;
                        }
                    }

                    if(isset($objectData[$key])){
                        $newObject[$key] = $objectData[$key];
                    }
                }else{
                    $valueBody = ($value["body"] !== null) ? $value["body"] : [];
                    $newObject[$key] = $this->restructureObject($objectData, $valueBody, [], $key);
                }
            }
        }

        return $newObject;
    }
    private function verifyParameter($schema, $key, $value, $return = false) {
        if (isset($schema[$key])) {
            $isArrayType = $this->validateArrayType($schema[$key]["type"]);
            if ($isArrayType && is_array($value)) {
                if (isset($schema[$key]["body"])) {
                    $nestedValue = [];
                    foreach($value as $subKey => $subValue) {
                        $blueprint = (is_numeric($subKey)) ? $schema : $schema[$key]["body"];
                        $nestedKey = (is_numeric($subKey)) ? $key : $subKey;

                        $nested = $this->verifyParameter($blueprint, $nestedKey, $subValue, true);
                        if($nested !== "not-valid-return"){
                            $nestedValue[$subKey] = $nested;
                        }
                    }
                    $value = $nestedValue;
                }
            }

            $value = $this->convertDateType($value, $schema[$key]);
            if(!is_array($value)){
                $value = $this->preventLinks($value);
            }

            if ($return === true) {
                return $value;
            }

            $this->entityObject[$key] = $value;
            return true;
        }

        return "not-valid-return";
    }
    private function validateDateType($value, $dataType) {
        $isValid = null;

        switch(trim($dataType)){
            case "Integer":
                $value = (is_numeric($value)) ? intval($value) : $value;
                $isValid = is_int($value);
                break;
            case "Numeric":
                $isValid = is_numeric($value);
                break;
            case "String":
                $isValid = is_string($value);
                break;
            case "Boolean":
                $isBoolean = filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
                $isValid = ($isBoolean !== null) ? true : false;
                break;
            case "Array":
                $isValid = is_array($value);
                break;
            case "Float":
                $isValid = is_float($value);
                break;
            default:
                $isValid = false;
                break;
        }

        return $isValid;
    }
    private function validateArrayType(String $type){
        return ($type === "Array" || ((strpos($type, "|") !== false) && (strpos($type, "Array") !== false)));
    }
    private function convertDateType($value, $requiredDataType) {
        $response = null;

        $dataTypes = explode("|", trim($requiredDataType["type"]));
        $useType = $dataTypes[0];
        if(count($dataTypes) > 1){
            foreach($dataTypes as $type){
                $isValid = $this->validateDateType($value, $type);
                if($isValid === true){
                    $useType = $type;
                    break;
                }
            }
        }

        switch (trim($useType)) {
            case "Integer":
                $value = (is_numeric($value)) ? intval($value) : $value;
                $response = (is_int($value)) ? $value : (int)$value;
                break;
            case "String":
                $response = (is_string($value)) ? $value : (string)$value;
                break;
            case "Boolean":
                $response = (bool)$value;
                break;
            case "Array":
                $response = (is_array($value)) ? $value : ((empty($value)) ? [] : (array)$value);
                break;
            case "Float":
                $response = (is_float($value)) ? $value : (float)$value;
                break;
        }

        return $response;
    }

    private function getStringsBetween($string, $start, $end) {
        $matches = [];
        if (preg_match_all('/' . preg_quote($start, '/') . '(.*?)' . preg_quote($end, '/') . '/', $string, $matches)) {
            return $matches[1];
        }
        return [];
    }
    private function preventLinks($string) {
        $linksArray = [];
        $linkAttributes = ["src", "href", "action"];

        foreach($linkAttributes as $attribute){
            $links = $this->getStringsBetween($string, $attribute.'="', '"');
            if (!empty($links)) {
                foreach($links as $link){
                    if (!preg_match('/^(https?:\/\/|http?:\/\/|\/\/|www\.)\S+/i', $link)) {
                        $linksArray[] = $link;
                    }
                }
            }
        }

        if (!empty($linksArray)) {
            global $appController;
            $defaultPath = SERVER_PROTOCOL."://".SERVER_NAME;
            $path = (env('API_ENABLED') === true && env('PROJECT_DISABLED') === true ) ? $appController->getProjectPath(null, true) : $defaultPath;
            $linksArray = array_unique($linksArray);

            foreach ($linksArray as $linkForReplace){
                $firstChar = substr($linkForReplace, 0, 1);
                if (strpos($linkForReplace, 'mailto:') !== false || strpos($linkForReplace, 'data:image/') !== false || $linkForReplace === '/' || $firstChar === "#") {
                    continue;
                }

                $linkPath = (strpos($linkForReplace, "/".STORAGE."/") !== false) ? $defaultPath : $path;
                $string = str_replace($linkForReplace, $linkPath.$linkForReplace, $string);
            }
        }

        return $string;
    }
}