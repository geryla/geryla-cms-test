<?php
class databaseTables {
    private static $tableNames = null;

    private static function loadTableNames() {
        if (self::$tableNames === null) {
            self::$tableNames = json_decode(file_get_contents(DATABASE_TABLES), true);
        }
    }

    public static function getTable(String $moduleName, String $tableName){
        self::loadTableNames();
        $returnTable = self::$tableNames[$moduleName][$tableName]["name"];

        return $returnTable;
    }
    public static function getTables(?String $moduleName = null){
        self::loadTableNames();

        return ($moduleName === null) ? self::$tableNames : self::$tableNames[$moduleName];
    }

    public static function getTablesList(){
        self::loadTableNames();

        $tablesList = [];
        foreach (self::$tableNames as $moduleName => $tables) {
            foreach ($tables as $tableName => $tableInfo) {
                $tablesList[$tableInfo["name"]] = $moduleName;
            }
        }

        return $tablesList;
    }
}