<?php
class navigationMenu{
    private $database;
    private $modulesController;

    private $lang;
    private $lastLink = null;

    public $activeParent = null;
    private $adminPagePermissions = [];
    private $permanentAccess = false;

    public function __construct(Database $database, AppController $appController) {
        $this->database = $database;
        $this->modulesController = $appController->returnModulesController();

        $this->lang = $appController->getLangFile();
    }


    public function setPermissions($allPermissions){
        $this->adminPagePermissions = $allPermissions;
    }
    public function checkPermanentAccess($permanentAccess){
        $this->permanentAccess = ($permanentAccess == 1) ? true : false;
    }

    public function getModulesToAdminMenu($activeModule, $activePage, $adminLevel, $renderType = "list", $showAllPages = false){
        $structuredData = $reOrderedPages = [];
        $modulesTree = $this->modulesController->getData('tree');

        foreach($modulesTree as $module){
          $otherPages = [];
          $headPage = null;
          $subActive = false;
          if(!isset($module["name"])){
              continue;
          }

          $modConfig = $this->modulesController->getModuleConfig($module["name"]);

          if( isset($modConfig["modulePages"]) ){
            foreach($modConfig["modulePages"] as $page){
              if( isset($page["onlyAdmin"]) && $page["onlyAdmin"] === true && $adminLevel != 1){
                  continue; // SKIP ONLY ADMIN PAGE if user is NOT SUPER-ADMIN
              }

              if( $page["headPage"] == 1 ){
                  $headPage = $page;
              }
              if( ($page["inMenu"] == 1 AND $page["headPage"] != 1) OR $showAllPages === true){
                  $otherPages[] = $page;
              }
            }
          }

          if( !isset($modConfig["modulePages"]) OR (empty($otherPages) && $headPage == null ) ){
            continue;
          }

          $href = '/'.ADMIN.'/'.$modConfig["moduleUrlName"].'/'.$headPage["urlName"];
          $submenu = ( !empty($module["child"]) OR !empty($otherPages) ) ? 'hasChild' : '';

          if( !empty($module["child"]) ){
            $subModules = array_values($module["child"]);
            $subModule = array_search($activeModule, array_column($subModules, 'name'));
            $subActive = ( $subModules[$subModule]["name"] == $activeModule ) ? true : false;
            usort($module["child"], function($a, $b) { return $a['order'] <=> $b['order']; });
          }

          $activeMenu = ($activeModule == $module["name"] OR (isset($subActive) && $subActive === true) ) ? 'active' : '';

          // FOR BREADCRUMBS included in wrapper.php
          if($activeMenu == "active"){
            $this->activeParent = (object) ["title" => translate($modConfig["moduleLangName"]), "url" => $modConfig["moduleUrlName"].'/'.$headPage["urlName"]];
          }

            $moduleSubpages = []; $pageOrder = 1;
            if( !empty($module["child"]) OR !empty($otherPages) ){
                // Module pages
                if( !empty($otherPages) ){
                    foreach($otherPages as $page){
                        $activeSub = ($activePage == $page["urlName"]) ? 'active' : '';
                        $subPageData = ["url" => '/'.ADMIN.'/'.$modConfig["moduleUrlName"].'/'.$page["url"], "class" => $activeSub, "name" => translate($page["url"]), "urlName" => $page["url"], "realModuleName" => translate($modConfig["moduleLangName"]), "parentId" => $page["parent"]];
                        if(isset($page["groupId"])){
                            $subPageData["groupId"] = $page["groupId"];
                        }

                        if(!isset($page["groupId"])){
                            $moduleSubpages[$pageOrder] = $subPageData;
                            $pageOrder++;
                        }else{
                            // If module config.php pages have groupId and groupOrder, push value to another group
                            ($activePage == $subPageData["urlName"]) ? $activeMenu = null : null;
                            $reOrderedPages[$page["groupId"]][] = ["data" => $subPageData, "order" => $page["groupOrder"]];
                        }
                    }
                }

                // Other modules in group pages
                if($module["child"]){
                    foreach($module["child"] as $mod){
                        $subModConfig = $this->modulesController->getModuleConfig($mod["name"]);
                        if(isset($subModConfig["modulePages"])){
                            foreach($subModConfig["modulePages"] as $modPage){
                                if( $modPage["inMenu"] == 1 OR $showAllPages === true){
                                    $activeMod = ($activeModule == $subModConfig["moduleUrlName"] ) ? 'active' : '';
                                    $subPageData = ["url" => '/'.ADMIN.'/'.$subModConfig["moduleUrlName"].'/'.$modPage["url"], "class" => $activeMod, "name" => translate($modPage["url"]), "urlName" => $modPage["url"], "realModuleName" => translate($subModConfig["moduleLangName"]), "parentId" => $modPage["parent"]];
                                    if(isset($modPage["groupId"])){
                                        $subPageData["groupId"] = $modPage["groupId"];
                                    }

                                    if(!isset($modPage["groupId"])){
                                        $moduleSubpages[$pageOrder] = $subPageData;
                                        $pageOrder++;
                                    }else{
                                        // If module config.php pages have groupId and groupOrder, push value to another group
                                        ($activePage == $subPageData["urlName"]) ? $activeMenu = null : null;
                                        $reOrderedPages[$modPage["groupId"]][] = ["data" => $subPageData, "order" => $modPage["groupOrder"]];
                                    }
                                }
                            }
                        }
                    }
                }
            }

          $structuredData[$module["name"]] = [
              "moduleId" => $module["id"],
              "moduleIndex" => $modConfig["moduleUrlName"],
              "moduleGroupId" => $module["group"],
              "moduleName" => translate($modConfig["moduleLangName"]),
              "moduleClass" => $submenu.' '.$activeMenu,
              "moduleUrl" => $href,
              "moduleIcon" => $modConfig["moduleIcon"],
              "moduleSubmenu" => $moduleSubpages
          ];

        }

        // Push page to another group by config.php settings
          if(!empty($reOrderedPages)){
              uasort($reOrderedPages, function($a, $b) {
                  return ($a['order'] ?? 0) <=> ($b['order'] ?? 0);
              });

              $helpArray = $structuredData;
              foreach($structuredData as $moduleName => $menuTabs){
                  foreach($menuTabs["moduleSubmenu"] as $nKey => $nPage){
                      $helpArray[$moduleName]["moduleSubmenu"][$nKey] = $nPage;
                      $helpArray[$moduleName]["moduleSubmenu"][$nKey]["class"] = (($activePage == $nPage["urlName"]) ? 'active' : '');
                  }
                  if(!empty($reOrderedPages[$menuTabs["moduleId"]])){
                      foreach($reOrderedPages[$menuTabs["moduleId"]] as $pageKey => $newPage){
                          $pushPage = $newPage["data"];
                          $pushPage["class"] = "";

                          if($activePage == $newPage["data"]["urlName"]){
                              $helpArray[$moduleName]["moduleClass"] .= ' active';
                              $pushPage["class"] = "active";
                              $this->activeParent = (object) ["title" => $menuTabs["moduleName"], "url" => str_replace('/'.ADMIN.'/', "", $menuTabs["moduleUrl"])];
                          }

                          array_splice($helpArray[$moduleName]["moduleSubmenu"], $newPage["order"], 0, [$pushPage]);
                      }
                  }
              }
              $structuredData = $helpArray;
          }

          if($renderType === "list"){
              $this->renderMenu($structuredData);
          }elseif($renderType === "checkboxes"){
              return $structuredData;
          }
      }
    public function modulePagesAsCheckbox($adminPermissions, $adminInfo){
    $pom = 1;
    $allPages = $this->getModulesToAdminMenu(null, null, $adminInfo->adminLevel, "checkboxes", true);

      foreach($allPages as $moduleName => $moduleInfo){
          echo '<li id="'.$moduleInfo["moduleId"].'">';
              echo '<label>';
                  echo '<i class="fa fa-plus-square-o fa-minus-square-o"></i>';
                  echo '<span>'.$moduleInfo["moduleName"].'</span>';
              echo '</label>';

                  echo '<ul class="subcategory">';
                  $this->createCheckboxItem($pom, $moduleInfo, $adminPermissions);
                  $pom++;
                  if(!empty($moduleInfo["moduleSubmenu"])){
                      foreach($moduleInfo["moduleSubmenu"] as $pageInfo){
                          $this->createCheckboxItem($pom, $pageInfo, $adminPermissions);
                          $pom++;
                      }
                  }
                  echo '</ul>';

          echo '</li>';
      }
    }

    private function renderMenu($structuredData){
        $renderList = null;

        foreach($structuredData as $modulePage){
            $renderItem = null;
            $renderSubList = null;
            $subPagesCount = 0;
            $modulePageAllowed = (in_array(str_replace('/'.ADMIN.'/', '', $modulePage["moduleUrl"]), $this->adminPagePermissions)) ? true : false;

            $renderItem .= '<li id="'.$modulePage["moduleIndex"].'" class="link '.$modulePage["moduleClass"].'">';
            $renderItem .= '<a href="'.(($modulePageAllowed === true OR $this->permanentAccess === true) ? $modulePage["moduleUrl"] : "#").'">';
            $renderItem .= '<span class="icon"><i class="fa '.$modulePage["moduleIcon"].'"></i></span>';
            $renderItem .= '<span class="text">'.$modulePage["moduleName"].'</span>';
            $renderItem .= '</a>';

            if(!empty($modulePage["moduleSubmenu"])){
                $renderSubList .= '<span class="arrow"></span>';
                $renderSubList .= '<ul class="submenu">';
                foreach($modulePage["moduleSubmenu"] as $page){
                    $subPagesCount = (in_array(str_replace('/'.ADMIN.'/', '', $page["url"]), $this->adminPagePermissions)) ? $subPagesCount+1 : $subPagesCount;
                    $isAllowed = (in_array(str_replace('/'.ADMIN.'/', '', $page["url"]), $this->adminPagePermissions)) ? true : false;
                    $renderSubList .= ($isAllowed === true OR $this->permanentAccess === true) ? '<li><a href="'.$page["url"].'" class="'.$page["class"].'"><span>'.$page["name"].'</span></a></li>' : null;
                }
                $renderSubList .= '</ul>';
            }
            $renderItem .= ($subPagesCount > 0 OR $this->permanentAccess === true) ? $renderSubList : null;

            $renderItem .= '</li>';

            $renderList .= ($modulePageAllowed === true OR ($modulePageAllowed === false && $subPagesCount > 0) OR $this->permanentAccess === true) ? $renderItem : null;
        }

        echo $renderList;
    }
    private function createCheckboxItem($index, $data, $adminPermissions){
        $itemLink = str_replace('/'.ADMIN.'/', "", (isset($data["moduleUrl"])) ? $data["moduleUrl"] : $data["url"]);
        $realModuleName = (isset($data["realModuleName"])) ? $data["realModuleName"] : $data["moduleName"];
        $moduleName = (isset($data["parentId"]) && $data["parentId"] != 0 && !isset($data["groupId"])) ? '- ' : null;
        $moduleName .= (isset($data["moduleName"])) ? $data["moduleName"] : $data["name"];
        $selectedItem = ( !empty($adminPermissions) && in_array($itemLink, $adminPermissions) ) ? "checked" : "";

        if($this->lastLink != $itemLink){
            echo '<li>';
            echo '<div class="customInput inline checkbox">';
            echo '<input type="checkbox" id="cat'.$index.'" name="permissions[]" value="'.$itemLink.'" '.$selectedItem.' class="checkboxStyled">';
            echo '<label for="cat'.$index.'"><span>'.$moduleName.'<b class="small green">('.$realModuleName.')</b></span></label>';
            echo '</div>';
            echo '</li>';
        }

        $this->lastLink = $itemLink;
    }
} 