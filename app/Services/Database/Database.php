<?php
class Database extends SQLCache {
    protected $pdo;
    private $errorLogs;
    private $queryLogs;
    private $bindHtmlRows = ["title", "description", "content", "emailContent", "shortDesc", "translate", "emailTemplateText", "translation", "sentence", "alias", "deliveryInfo", "paymentInfo",  "companyName", "message", "name", "selectedAttributes", "extraData", "jsonPersonalization", "jsonTheme", "parameters"];

    private $queryCache;

    public function __construct() {
        $connection = new Connection();
        $this->queryCache = new QueryCache();

        $this->pdo = $connection->getDatabase();
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
        $this->PDO_start = microtime(true);
    }
    public function pdo() {
        return (env('APP_DEBUG') === true) ? $this->pdo : null;
    }

// DATABASE QUERIES
    public function migrationQuery($query) {
        $stmt = $this->pdo->prepare($query);
        $output = $stmt->execute();

        return ($output !== true) ? $stmt->errorInfo() : true;
    }
    public function insertQuery($query, $bindValues) {
        $keys = array_keys($bindValues);
        $columns = implode(",", $keys);
        $values = implode(",:", $keys);

        $stmt = $this->pdo->prepare($query." ($columns) VALUES (:$values) ;");
        $stmt = $this->sqlInjectionQuery($stmt, $bindValues);

        $executeBool = $stmt->execute();
        $insertedId = $this->pdo->lastInsertId();

        $this->catchResults($stmt, $query);
        $this->queryCache->remove($this->getLastQuery(), 'INTO');

        return ($insertedId != 0) ? $insertedId : (($executeBool === true) ? true : false);
    }
    public function insertBulkQuery($tableName, $columns, $values, $triggerCacheVerify = true) {
        $query = "INSERT INTO `{$tableName}`";
        $columnsString = implode(", ", $columns);

        $valuesData = [];
        $valuesString = [];
        foreach($values as $index => $value){
            $valuesBinds = [];
            foreach($columns as $columnName){
                if(isset($value[$columnName])){
                    $valueAlias = $columnName.'_'.$index;
                    $valuesBinds[] = ":{$valueAlias}";
                    $valuesData[$valueAlias] = $value[$columnName];
                }
            }
            $valuesString[] = '('.implode(",", $valuesBinds).')';
        }
        $valuesArray = implode(",", $valuesString);

        $stmt = $this->pdo->prepare("{$query} ({$columnsString}) VALUES {$valuesArray};");
        $stmt = $this->sqlInjectionQuery($stmt, $valuesData);

        $executeBool = $stmt->execute();
        $insertedId = $this->pdo->lastInsertId();

        $this->catchResults($stmt, $query);
        if($triggerCacheVerify === true){
            $this->queryCache->remove($this->getLastQuery(), 'INTO');
        }

        return ($insertedId != 0) ? $insertedId : (($executeBool === true) ? true : false);
    }
    public function updateQuery($query, $bindValues, $whereValues, $whereSql = "" ) {
        $columns = null;
        foreach($bindValues as $key => $value){
            $columns .= "`$key` = :$key, ";
        }
        $columns = substr($columns, 0, -2);

        $stmt = $this->pdo->prepare($query." SET {$columns} {$whereSql} ;");

        $stmt = $this->sqlInjectionQuery($stmt, $bindValues);
        if( !empty($whereSql) ){
            $stmt = $this->sqlInjectionQuery($stmt, $whereValues);
        }
        $result = $stmt->execute();
        /*if($result === true){
            if($stmt->rowCount() === 0){
                $result = false;
            }
        }*/

        $this->catchResults($stmt, $query);
        $this->queryCache->remove($this->getLastQuery(), 'UPDATE');

        return $result;
    }
    public function deleteQuery($query, $whereValues, $whereSql = "" ) {
        $stmt	= $this->pdo->prepare($query." {$whereSql} ;");

        if( !empty($whereSql) ){
            $stmt = $this->sqlInjectionQuery($stmt, $whereValues);
        }
        $stmt->execute();

        $this->catchResults($stmt, $query);
        $this->queryCache->remove($this->getLastQuery(), 'FROM');

        return true;
    }
    public function getQuery($sql, $bindValues, $wrapOneResult = true, $skipCache = false) {
        $this->PDO_index++;
        $response = null;

        if($skipCache === false){
            $response = $this->queryCache->get($sql, $bindValues);
            $this->queryLogs[] = $this->bindQuery($sql, $bindValues);
        }

        if($response === null) {
            $response = $this->selectQuery($sql, $bindValues, $wrapOneResult);
            if($skipCache === false){
                $this->queryCache->set($sql, $bindValues, $response);
            }
        }

        return $response;
    }
    private function selectQuery($query, $bindValues, $wrapOneResult = true) {
        $stmt = $this->pdo->prepare($query);
        $stmt = $this->sqlInjectionQuery($stmt, $bindValues);
        $stmt->execute();
        $this->catchResults($stmt, $query, $bindValues);

        $queryResults = $this->queryResults($stmt, $wrapOneResult);
        return $queryResults;
    }
    private function queryResults($stmt, $arrayWrap) {
        $this->PDO_indexCache++;
        $queryData = $stmt->fetchAll(PDO::FETCH_ASSOC);


        $queryCount = count($queryData);

        if( $queryCount > 1 ){
            return $queryData;
        }else{
            if( $queryCount == 1 ){
                return ( $arrayWrap === true ) ? $queryData : $queryData[0];
            }else{
                return [];
            }
        }
    }
    private function sqlInjectionQuery(Object $stmt, ?Array $bindValues) {
        if( !empty($bindValues) ){
            foreach($bindValues as $bindName => $bindValue){
                if( in_array($bindName, $this->bindHtmlRows) ){
                    $stmt->bindValue(":".$bindName, $this->injectionProtectHTML($bindValue));
                }else{
                    $stmt->bindValue(":".$bindName, $this->injectionProtect($bindValue));
                }
            }
        }

        return $stmt;
    }

    public function injectionProtect(?String $string) {
        if($string === null) return $string;

        $string = addslashes($string);
        $string = htmlspecialchars(htmlspecialchars($string));
        $string = str_replace('`','\`', $string);

        return $string;
    }
    public function injectionProtectHTML(?String $string) {
        if($string === null) return $string;

        $string = addslashes($string);
        $string = str_replace(['`', '\"'], ['\`', '"'], $string);
        return $string;
    }

    private function catchResults(Object $stmt, String $query, ?Array $bindValues = []){
        $querySQL = $this->getFullQuery($stmt, $query, $bindValues);
        $this->queryLogs[] = $querySQL;

        if(!empty($stmt->errorInfo())){
            if($stmt->errorInfo()[0] !== "00000"){
                $errorInfo = $stmt->errorInfo();
                $errorInfo[] = $querySQL;
                $this->errorLogs[] = $errorInfo;
            }
        }
    }
    public function getErrors(){
        return $this->errorLogs;
    }
    private function getFullQuery(Object $stmt, String $query, ?Array $bindValues = []){
        $querySQL = null;

        ob_start();
        $stmt->debugDumpParams();
        $debugOutput = ob_get_clean();
        if (preg_match('/Sent\s+SQL:\s+\[\d+\]\s+(.*?)\s+Params:\s+\d+/s', $debugOutput, $matches)) {
            if(isset($matches[1])){
                $querySQL = $matches[1];
            }
        }

        if($querySQL === null){
            $querySQL = $query;
            $this->bindQuery($querySQL, $bindValues);
        }

        return $querySQL;
    }
    private function bindQuery(String $query, ?Array $bindValues = [] ){
        if(!empty($bindValues)){
            foreach($bindValues as $bindName => $bindValue){
                if($bindValue !== null){
                    $query = str_replace(":{$bindName}", $bindValue, $query);
                }
            }
        }

        return $query;
    }


    public function getQueries(){
        return $this->queryLogs;
    }
    public function getLastQuery(){
        return ($this->queryLogs !== null) ? end($this->queryLogs) : null;
    }
}