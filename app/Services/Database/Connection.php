<?php

class Connection {
    private $variables;
    private $pdo;

    public function __construct() {
        $this->getEnv();
        $this->connect();
        $this->clearEnv();
    }

    private function getEnv(){
        $this->clearEnv();
        $this->variables = [
            "host" => env('DB_HOST'),
            "port" => env('DB_PORT'),
            "user" => env('DB_USERNAME'),
            "pass" => env('DB_PASSWORD'),
            "name" => env('DB_DATABASE')
        ];
    }
    private function clearEnv(){
        $this->variables = null;
    }
    private function connect(){
        try {
            $this->pdo = new PDO("mysql:dbname={$this->variables["name"]};host={$this->variables["host"]};port={$this->variables["port"]}", $this->variables["user"], $this->variables["pass"], [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"]);
            $this->pdo->exec("SET NAMES 'utf8';");
        }catch(PDOException $e) {
            throw new \RuntimeException($e->getMessage());
        }
    }

    public function getDatabase(){
        return $this->pdo;
    }
}