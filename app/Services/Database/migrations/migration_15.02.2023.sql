CREATE TABLE IF NOT EXISTS `apiSessions` (
  `accessKey` varchar(500) NOT NULL,
  `sessionId` varchar(120) NOT NULL,
  `expire` datetime NOT NULL

) ENGINE=InnoDB DEFAULT CHARSET=utf8;