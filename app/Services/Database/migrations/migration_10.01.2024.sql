ALTER TABLE `apiSessions` ADD UNIQUE INDEX accessKeyUnique (accessKey);

CREATE INDEX bySubject ON `esoItemsPrices` (`systemId`, `idSubject`, `updatedDate`);
CREATE INDEX byPriceGroup ON `esoItemsPrices` (`systemId`, `idPriceGroup`, `updatedDate`);

CREATE INDEX itemLang ON `products-lang`(`associatedId`, `langShort`);
CREATE INDEX itemUrl ON `products-lang`(`url`);

CREATE INDEX templateUrl ON `templates-lang`(`url`);

CREATE INDEX email ON `users`(`email`);
CREATE INDEX userSalt ON `users`(`userSalt`);

CREATE INDEX status ON `scheduledJobs`(`status`);

ALTER TABLE `redirects` MODIFY inputUrl VARCHAR(1000);
CREATE INDEX byVisibleUrl ON `redirects` (`inputUrl`, `visibility`);