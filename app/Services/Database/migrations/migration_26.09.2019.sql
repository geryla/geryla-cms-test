-- CREATE TABLE for MODULES
CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(4) NOT NULL AUTO_INCREMENT,

  `title` varchar(35) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `moduleName` varchar(25) NOT NULL,
  `moduleInfo` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `moduleVersion` varchar(10) NULL,
  `contentTables` varchar(255) NOT NULL,
  `replaceContentTables` varchar(255) NOT NULL,
  `moduleGroup` int(15) NOT NULL DEFAULT '0',
  `defaultModule` int(1) NOT NULL DEFAULT '0',
  `defaultPriority` int(1) DEFAULT '0',
  `activeModule` int(1) NOT NULL DEFAULT '0',

  `orderNum` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- CREATE TABLE for MIGRATIONS
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) NOT NULL AUTO_INCREMENT,

  `migration` varchar(75) NOT NULL,
  `moduleName` varchar(50) NOT NULL,
  `createdDate` datetime NOT NULL,

  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- INSERT MODULE into TABLE 'modules' (default + all created until this date)
INSERT INTO `modules` (`id`, `title`, `moduleName`, `moduleInfo`, `contentTables`, `replaceContentTables`, `moduleGroup`, `defaultModule`,  `defaultPriority`, `activeModule`, `orderNum`) VALUES
  (1, 'Templates', 'templates', 'All templates which are active for website and prefixes for pages', '', '', 6, 1, 1, 1, 1),
  (2, 'Languages', 'localization', 'All languages functions (content, translate, multicurrency eshop, etc.)', '', '', 3, 1, 3, 1, 1),
  (3, 'Settings', 'settings', 'Basic settings for website and log file of actions', '', '', 0, 1, 2, 1, 13),
  (4, 'Filtration', 'filtration', 'Filtration (frontend & backend - products, posts, etc.)', '', '', 0, 1, 5, 1, 7),
  (5, 'Media manager', 'mediaManager', 'Upload and editing files like images, pdf, etc.', '', '', 0, 1, 6, 1, 6),
  (6, 'Content editor', 'contentEditor', 'Create posts, blog posts, etc.', 'contenteditor-categories, contenteditor-groups, contenteditor-posts-lang', '', 0, 1, 7, 1, 3),
  (7, 'Email templates', 'emailTemplates', 'Dynamic email templates sended by other modules actions', '', '', 0, 1, 8, 1, 11),
  (8, 'Administrators', 'admins', 'Access to administration by user profiles', '', '', 3, 1, 4, 1, 12),
  (9, 'Marketing', 'marketing', 'Tracking, analytics, conversion codes, etc.', '', 'marketing-pages', 0, 1, 9, 1, 12),
  (10, 'Cookies agreement', 'cookies', 'Add cookie agreement bar', '', '', 9, 1, 10, 1, 5),
  (11, 'Maintenance', 'maintenance', 'Maintenance page', '', '', 3, 1, 11, 1, 13),
  (12, 'Contact boxes', 'contactBoxes', 'Contact boxes', '', '', 3, 1, 12, 1, 11),

  (13, 'Contact form', 'contactForm', 'Create contact forms', '', '', 7, 0, 0, 0, 1),
  (14, 'Slideshow', 'slideshow', 'Slides (images) with groups for placing through website', '', '', 5, 0, 0, 0, 8),
  (15, 'Galleries', 'gallery', 'Galleries for images', '', '', 5, 0, 0, 0, 9),
  (16, 'Registered users', 'users', 'All registered users', '', '', 0, 0, 0, 0, 5),
  (17, 'Courses', 'courses', 'Language courses (FREMDULO.com)', 'courses-lang', '', 0, 0, 0, 0, 4),
  (18, 'Shopping cart', 'shoppingCart', 'Enable shopping on website', '', '', 3, 0, 0, 0, 2),
  (19, 'Orders', 'orders', 'Control all orders', '', '', 0, 0, 0, 0, 1),
  (20, 'Payment methods', 'payments', 'Add methods to shopping cart', '', '', 19, 0, 0, 0, 2),
  (21, 'Delivery methods', 'deliveries', 'Add methods to shopping cart', '', '', 19, 0, 0, 0, 3),
  (22, 'Discounts', 'discounts', 'Discount codes for orders', '', '', 9, 0, 0, 0, 2),
  (23, 'GoPay payment gateway', 'goPay', 'Payment gateway', '', '', 3, 0, 0, 0, 3),
  (24, 'Invoices', 'invoices', 'Invoices for orders', '', '', 19, 0, 0, 0, 3),
  (25, 'Products', 'products', 'Module for e-shop containing products, categories, manufacturers, etc.', 'products-categories-lang, products-lang', '', 0, 0, 0, 0, 2),
  (26, 'Product variants', 'productVariants', 'Extends the product module to create variations', '', '', 25, 0, 0, 0, 7),
  (27, 'reCaptcha anti-spam', 'reCaptcha', 'Anti-spam for forms', '', '', 7, 0, 0, 0, 3);