-- INSERT MODULE into TABLE 'modules'
INSERT INTO `modules` (`id`, `title`, `moduleName`, `moduleInfo`, `contentTables`, `replaceContentTables`, `moduleGroup`, `defaultModule`, `activeModule`, `orderNum`) VALUES
  (30, 'Product relations', 'productRelations', 'Ability to add related products via admin panel', '', '', 25, 0, 0, 8);