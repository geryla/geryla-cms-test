SET FOREIGN_KEY_CHECKS=0;

-- DELETE TABLES
DROP TABLE `modules`;
DROP TABLE `migrations`;
DROP TABLE `schedules`;

DELETE FROM `settings` WHERE settGroup = 'scheduler';

SET FOREIGN_KEY_CHECKS=1;