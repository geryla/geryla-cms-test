-- INSERT MODULE SEARCH into TABLE 'modules'
INSERT INTO `modules` (`id`, `title`, `moduleName`, `moduleInfo`, `contentTables`, `replaceContentTables`, `moduleGroup`, `defaultModule`, `activeModule`, `orderNum`) VALUES
  (28, 'Search', 'search', 'Search field for multiple modules at frontend', '', '', 3, 0, 0, 4);