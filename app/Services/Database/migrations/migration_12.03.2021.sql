-- CREATE TABLE for JOBS (CRON Schedules)
CREATE TABLE IF NOT EXISTS `schedules` (
  `id` int(4) NOT NULL AUTO_INCREMENT,

  `cronJob` TEXT NOT NULL,
  `moduleName` VARCHAR(50) NULL,

  `dailyFrequency` INT(10) NOT NULL DEFAULT '1',
  `jobRuns` INT(10) NOT NULL DEFAULT '0',
  `start` INT(2) NOT NULL DEFAULT '0',
  `end` INT(2) NOT NULL DEFAULT '23',

  `monday` INT(1) NULL DEFAULT '0',
  `tuesday` INT(1) NULL DEFAULT '0',
  `wednesday` INT(1) NULL DEFAULT '0',
  `thursday` INT(1) NULL DEFAULT '0',
  `friday` INT(1) NULL DEFAULT '0',
  `saturday` INT(1) NULL DEFAULT '0',
  `sunday` INT(1) NULL DEFAULT '0',

  `orderNum` INT(10) NOT NULL DEFAULT '0',
  `visibility` INT(1) NOT NULL DEFAULT '1',

  `lastRun` datetime NULL,
  `createdDate` datetime NULL,
  `updatedDate` datetime NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;