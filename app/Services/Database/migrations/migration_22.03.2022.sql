-- INSERT MODULE into TABLE 'modules'
INSERT INTO `modules` (`id`, `title`, `moduleName`, `moduleInfo`, `contentTables`, `replaceContentTables`, `moduleGroup`, `defaultModule`, `activeModule`, `orderNum`) VALUES
  (46, 'Product combinations', 'productCombinations', 'Dynamic extension of product parameters to create combinations', '', '',3, 0, 0, 16);