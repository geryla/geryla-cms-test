<?php
class SQLCache{
    protected $PDO_counter = [];
    protected $PDO_counterCache = [];
    protected $PDO_time = [];
    protected $PDO_index = 1;
    protected $PDO_indexCache = 1;
    protected $PDO_start = null;

    public function queryCounter($group, $add = false){
        $this->PDO_counter[$group] = ($add === true) ? $this->PDO_counter[$group] + $this->PDO_index : $this->PDO_index;
        $this->PDO_counterCache[$group] = ($add === true) ? $this->PDO_counterCache[$group] + $this->PDO_indexCache : $this->PDO_indexCache;
        $this->PDO_index = 0;
        $this->PDO_indexCache = 0;

        $this->PDO_time[$group] = microtime(true) - $this->PDO_start;
        $this->PDO_start = microtime(true);
    }
    public function getQueryCounter(){
        return ["simpleQueries" => $this->PDO_counter, "cachedQueries" => $this->PDO_counterCache, "time" => $this->PDO_time];
    }
}