<?php
class adminRender{
    private $database;
    private $localeEngine;

    private $langFile;
    public $deleteText = null;

    private $translatableFields = [];
    public $moduleBoxes = [];
    public $activeModules = [];
    public $templatesAnchors = [];

    public function __construct(Database $database, AppController $appController){
        $this->database = $database;
        $this->localeEngine = $appController->returnLocaleEngine();
        $templateClass = $appController->returnTemplatesClass();

        $this->langFile = $appController->getLangFile();
        $this->activeModules = $appController->returnModulesController()->returnModules('active');

        $this->templatesAnchors = $templateClass->getAllTemplateAnchors($this->localeEngine->returnAppLang());
        $this->deleteText = "confirm-head='".translate("confirm-delete-head")."' confirm-text='".translate("confirm-delete-text")."' confirm-yes='".translate("confirm-delete-yes")."' confirm-no='".translate("confirm-delete-no")."'";
    }


/* ===== TRANSLATABLE ROWS FOR DISABLE DURING TRANSLATION PROCESS */

// Set translatable fields
    public function setTranslatableFields($allFields, $activeLang){
        if($activeLang != DEFAULT_LANG){
            $this->translatableFields = ( empty($this->translatableFields) ) ? $allFields : array_merge($this->translatableFields, $allFields);
            return true;
        }
        return false;
    }
// Compare field if is translatable
    private function isTranslatable($fieldName){
        return ( empty($this->translatableFields) OR (in_array($fieldName, $this->translatableFields)) ) ? true : false;
    }
// Return edited filed class name for script and css edit
    private function getTranslatableClass($fieldName, $fieldClass){
        $classSpace = (!empty($fieldClass)) ? " " : "";
        return ($this->isTranslatable($fieldName) === true) ? $fieldClass : "notTranslatable".$classSpace.$fieldClass;
    }

// ===== RENDER INPUTS
    public function input($type, $name, $value, $placeholder = null, $required = false, $id = null, $class = null, $autocomplete = true, $disabled = false, $customAttr = null){

        $class = $this->getTranslatableClass($name, $class);
        $placeholder = ($placeholder !== null) ? "placeholder='{$placeholder}'" : "";
        $required = ($required === true) ? "required" : "";
        $autocomplete = ($autocomplete !== true) ? "autocomplete='off'" : "";
        $disabled = ($disabled === true) ? "disabled" : "";

        $boxId = ($id !== null) ? " id='{$id}'" : "";
        $boxClass = ($class !== null) ? " class='{$class}'" : "";

        return "<input type='{$type}' name='{$name}' value='{$value}'{$boxId}{$boxClass} {$placeholder} {$required} {$autocomplete} {$disabled} {$customAttr} />";
    }
    public function checkbox($name, $value, $checked = false, $required = false, $id = null, $class = null){
        $class = $this->getTranslatableClass($name, $class);
        $required = ($required === true) ? "required" : "";

        $checkedValue = ($checked === true) ? "checked" : "";
        $boxId = ($id !== null) ? " id='{$id}'" : "";
        $boxClass = ($class !== null) ? " class='{$class}'" : "";

        return "<input type='checkbox' name='{$name}' value='{$value}'{$boxId}{$boxClass} {$checkedValue} {$required} />";
    }
    public function radio($name, $value, $checked = false, $required = false, $id = null, $class = null){
        $class = $this->getTranslatableClass($name, $class);
        $required = ($required === true) ? "required" : "";

        $checkedValue = ($checked === true) ? "checked" : "";
        $boxId = ($id !== null) ? " id='{$id}'" : "";
        $boxClass = ($class !== null) ? " class='{$class}'" : "";

        return "<input type='radio' name='{$name}' value='{$value}'{$boxId}{$boxClass} {$checkedValue} {$required} />";
    }
    public function select($name, $values, $valName = null, $valTitle = null, $selected = "", $required = false, $id = null, $class = null, $disabled = null){
        $class = $this->getTranslatableClass($name, $class);
        $required = ($required === true) ? "required" : "";
        $disabled = ($disabled === true) ? "disabled" : "";

        $boxId = ($id !== null) ? " id='{$id}'" : "";
        $boxClass = ($class !== null) ? " class='{$class}'" : "";

        $select = "<select name='{$name}' {$boxClass} {$required} {$disabled}>";
          foreach($values as $key => $value){
              $loopValue = ($valName === null) ? $key : $value[$valName];
              $loopTitle = ($valName === null) ? $value : $value[$valTitle];

            $selectedValue = ($selected != "" && $selected == $loopValue) ? "selected" : "";
            $select .= "<option{$boxId} value='{$loopValue}' {$selectedValue}>{$loopTitle}</option>";
          }
        $select .= "</select>";

        return $select;
    }
    public function textarea($name, $value, $rows = 1, $placeholder = null, $required = false, $id = null, $class = null, $disabled = null){
        $class = $this->getTranslatableClass($name, $class);
        $placeholder = ($placeholder !== null) ? "placeholder='{$placeholder}'" : "";
        $required = ($required === true) ? "required" : "";
        $disabled = ($disabled === true) ? "disabled" : "";

        $boxId = ($id !== null) ? " id='{$id}'" : "";
        $boxClass = ($class !== null) ? " class='{$class}'" : "";

        return "<textarea{$boxId}{$boxClass} rows='{$rows}' name='{$name}' {$placeholder} {$required} {$disabled}>{$value}</textarea>";
    }
    public function button($name, $value, $text, $required = false, $id = null, $class = null){
        $required = ($required === true) ? "required" : "";

        $boxId = ($id !== null) ? " id='{$id}'" : "";
        $boxClass = ($class !== null) ? " class='{$class}'" : "";

        return "<button name='{$name}' value='{$value}'{$boxId}{$boxClass}{$required} />".$text."</button>";
    }
    public function disabled($type, $value){
        return "<input type='{$type}' name='disabledValue' value='{$value}' disabled />";
    }

// ===== RENDER GRAPHIC PARTS

  public function header($title, $backUrl = null, $nextUrl = null, $previewUrl = null){
    echo '<header>';
  		echo '<h2>'.$title.'</h2>';
      if( $backUrl !== null OR $nextUrl !== null ){
        echo '<div class="options">';
         echo ($previewUrl !== null) ? '<a href="'.env('PROJECT_REMOTE_URL').$previewUrl.'?preview=true" target="_blank" class="btn icon search inverted"" title="'.translate("preview").'"><span>'.translate("preview").'</span></a>' : '';
         echo ($backUrl !== null) ? '<a href="'.$backUrl.'" class="btn icon back warning" title="'.translate("back").'"><span>'.translate("back").'</span></a>' : '';
         echo ($nextUrl !== null) ? '<a href="'.$nextUrl.'" class="btn icon add inverted"" title="'.translate("add").'"><span>'.translate("add").'</span></a>' : '';
        echo '</div>';
      }
  	echo '</header>';
  }
  public function headerShow($title, $backUrl = null, $nextUrl = null, $showUrl = null){
    echo '<header>';
  		echo '<h2>'.$title.'</h2>';
      if( $backUrl !== null OR $nextUrl !== null OR $showUrl !== null ){
        echo '<div class="options">';
         echo ($backUrl !== null) ? '<a href="'.$backUrl.'" class="btn" title="'.translate("back").'"><span class="save back">'.translate("back").'</span></a>' : '';
         echo ($nextUrl !== null) ? '<a href="'.$nextUrl.'" class="btn" title="'.translate("add").'"><span class="save">'.translate("add").'</span></a>' : '';
         echo ($showUrl !== null) ? '<a href="'.$showUrl.'" class="btn" title="'.translate("show").'"><span class="save back">'.translate("show").'</span></a>' : '';
        echo '</div>';
      }
  	echo '</header>';
  }

  public function tableHead($values, Int $pushEmpty = 0){
		echo '<tr>';
          foreach($values as $value){
            echo '<th><b>'.$value.'</b></th>';
          }
          if($pushEmpty !== 0){
              for($i = 0; $i < $pushEmpty; $i++){
                  echo '<th></th>';
              }
          }
		echo '</tr>';
    $this->tableSpace(1);
  }
  public function tableHeadSortable($values, $emptyRow = 1){
    echo '<tr>';
        echo '<th class="sortable"></th>';
        foreach($values as $value){
            echo '<th><b>'.$value.'</b></th>';
        }
    echo '</tr>';

    if($emptyRow > 0){
        $this->tableSpace($emptyRow);
    }
  }
  public function tableRow($title, $value = "", $heading = false){
  	$color = ($heading === true) ? " class='blue'" : "";
    echo "<tr{$color}>";
      echo '<td>'.$title.'</td>';
      echo '<td>'.$value.'</td>';
  	echo '</tr>';
  }
  public function tableColumn($value, $width = null, $center = null, $className = null){
    $tdWidth = ($width !== null) ? ' width="'.$width.'"' : "";
    $tdCenter = ($center !== null) ? ' style="text-align: center;"' : '';
    $class = ($className !== null) ? ' class="'.$className.'"' : '';
    echo "<td{$tdWidth}{$tdCenter}{$class}>".$value."</td>";
  }
  public function tableColumnSortable($orderNum = null){
    echo'<td class="sortable">';
        echo '<span class="handle">';
            echo '<i class="fa fa-arrows"></i>';
            echo (($orderNum !== null) ? "<b>{$orderNum}</b>" : null);
        echo '</span>';
    echo '</td>';
  }
  public function tableSpace($num = 1){
  	for($i=0;$i<$num;$i++){
      echo '<tr class="spacer"><td>&nbsp;</td></tr>';
    }
  }

  public function visibilitySwitch($objectId, $tableName, $data, $visibility){
    $visibilityClass = ($visibility == 0) ? "times-circle red" : "check-circle green";
    $visibilityText = ($visibility == 0) ? translate("object-visibility-no") : translate("object-visibility-yes");

    $visibilitySwicth = '<a class="visibility" id="v_'.$objectId.'" data="'.$data.'" name="'.$tableName.'">';
      $visibilitySwicth .= '<i class="fa fa-'.$visibilityClass.'" title="'.$visibilityText.'"></i>';
    $visibilitySwicth .= '</a>';

    return $visibilitySwicth;
  }
  public function tableActionColumn($editAction, $deleteAction, $previewAction, $width = null, $translateText = false, $previewInSelf = false){
    $tdWidth = ($width !== null) ? ' width="'.$width.'"' : "";

    $translateText = ($translateText === true) ? translate("translate") : translate("edit");
    $previewUrl = ($previewInSelf === false) ? env('PROJECT_REMOTE_URL').$previewAction.'?preview=true' : $previewAction;

    $column = "";
    $column .= ($editAction !== null) ? '<a href="'.$editAction.'" class="anchor green"><i class="fa fa-pencil" title="'.$translateText.'"></i> '.$translateText.'</a>' : '';
    $column .= ($previewAction !== null) ? '<a href="'.$previewUrl.'" class="anchor blue" '.(($previewInSelf === false) ? 'target="_blank"' : null).'><i class="fa fa-search" title="'.translate("preview").'"></i> '.translate("preview").'</a>' : '';
    $column .= ($deleteAction !== null) ? $this->deleteAnchor($deleteAction, null) : '';

    echo "<td class='actions' {$tdWidth}>".$column."</td>";
  }
    public function deleteAnchor($deleteAction, $ownText = null, $customClass = null){
        $text = ($ownText === null) ? '<i class="fa fa-trash-o" title="'.translate("delete").'"></i> '.translate("delete") : $ownText;
        return '<a class="anchor red deleteContent '.$customClass.'" '.$deleteAction.' action="delete" '.$this->deleteText.'>'.$text.'</a>';
    }
  public function deleteAnchorCustomClass($deleteAction, $className, $ownText = null){
        $text = ($ownText === null) ? '<i class="fa fa-trash-o" title="'.translate("delete").'"></i> '.translate("delete") : $ownText;
        return '<a '.$className.' '.$deleteAction.' action="delete" '.$this->deleteText.'>'.$text.'</a>';
    }

  public function contentDiv($open, $id = null, $class = null){
    $boxId = ($id !== null) ? " id='{$id}'" : "";
    $boxClass = ($class !== null) ? " class='{$class}'" : "";
    echo ($open === true) ? "<div{$boxId}{$boxClass}>" : "</div>";
  }
  public function contentRow($id = null, $class = null, $title = null, $help = null, $value = null){
    if($title === null & $value === null){return false;}

    $boxId = ($id !== null) ? " id='{$id}'" : "";
    $boxClass = ($class !== null) ? " class='{$class}'" : "";

    echo "<div{$boxId}{$boxClass}>";
      echo ($title !== null) ? $title : '';
      echo ($help !== null) ? $help : '';
      echo ($value !== null) ? $value : '';
    echo "</div>";
  }

    public function boxTab($moduleName, $boxName, $langTitle, $cutomId = null, $customClass = null, $hideBox = false, $requiredModules = []){
        $continue = true;
        if(!empty($requiredModules)){
            foreach($requiredModules as $module){
                if(!in_array($module, $this->activeModules)){
                    $continue = false;
                }
            }
        }


        if($continue === true && $hideBox === false && in_array($moduleName, $this->activeModules)){
            $this->moduleBoxes[$boxName] = $this->getBoxPath($moduleName, $boxName);

            $boxId = ($cutomId !== null) ? " id='{$cutomId}'" : "";
            $boxClass = ($customClass !== null) ? " class='{$customClass}'" : "";
            $boxTitle = useIfExists($this->langFile, null, $langTitle);

            echo ($boxTitle !== null) ? "<span{$boxId}{$boxClass}><span>{$boxTitle}</span></span>" : null;
        }
    }
    public function getBoxPath($moduleName, $boxName){
        if(in_array($moduleName, $this->activeModules)){
           return MODULE_DIR.$moduleName.'/includes/moduleBoxes/'.$boxName.'.php';
        }
    }

    public function getPreviewPath($templateName, $url, $urlPrefix = null, $onlyBase = false){
        $previewPath = '/'.$this->templatesAnchors[$templateName];
        if($onlyBase === false){
            $previewPath .= ($urlPrefix !== null) ? '/'.$urlPrefix.'-'.$url : '/'.$url;
        }

        return $previewPath;
    }
    public function helpTextBox($content, $classes = null){
      echo '<div class="helpText '.$classes.'">';
          echo '<i class="infoIcon fa fa-info-circle"></i>';
          echo '<span class="text">';
            echo $content;
          echo '</span>';
      echo '</div>';
    }

    public function renderExportSelect(Array $exportTypes, String $selectName,String $selectId, ?String $jsName = null){
        echo '<select id="'.$selectId.'" name="'.$selectName.'">';
            foreach($exportTypes as $fileKey => $fileName){
                echo '<option value="'.$fileKey.'">'.translate($fileName).'</option>';
            }
        echo '</select>';
        echo '<span class="'.$jsName.' btn icon save" data-button="'.$selectId.'">'.translate("download").'</span>';
    }
}