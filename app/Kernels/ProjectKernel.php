<?php
if($presenter->routerType !== 'project'){
    return;
}
if(useIfExists(env('PROJECT_DISABLED'), false) === true){
    return;
}

ob_start("sanitize_output");

$presenter->templateData = (object) $presenter->templateData;
bdump($presenter);

// Load controllers
require_once(EVENTS_DIR.'Controller.php');
$database->queryCounter("AppController");

if(file_exists(CONTROLLERS_DIR."controller.php")){
    include_once(CONTROLLERS_DIR."controller.php");
}
$database->queryCounter("CommonController");

if($presenter->activeContent->templateController !== null){
    include_once($presenter->activeContent->templateController);
}
$database->queryCounter("TemplateController");

// Set meta image
$firstLoadedImage = $mediaProcessor->getLoadedImage(0);
$presenter->metaData->image = null;
if ($firstLoadedImage !== null) {
    $presenter->metaData->image = $mediaProcessor->getImagePath($firstLoadedImage, "ogTag");
}
if (empty($presenter->metaData->image)) {
    $presenter->metaData->image = '/assets/img/ogImage.jpg';
}

// Render content
if( $presenter->maintenance === true ){
    require_once($presenter->templateData->projectPath);
}else{
    $shortCodes = new shortCodes($database, $appController);

    if($presenter->activeContent->replaceContent === true){
        $pageContent = renderReturn("replaceContent", [
            "breadcrumbs" => $presenter->breadCrumbs,
            "data" => ["model" => $presenter->templateData, "controller" => $controller, "templatePath" => $presenter->activeContent->templatePath]]);
    }else{
        $pageContent = renderReturn('pages.'.$presenter->activeContent->templateName, [
            "breadcrumbs" => $presenter->breadCrumbs,
            "data" => ["model" => $presenter->templateData, "controller" => $controller]]
        );
    }

    $shortCodes->buildContent($shortCodes->getShortCodes($pageContent));
}
$database->queryCounter("Render");