<?php
if($presenter->routerType !== 'app'){
    return;
}

$presenter->templateData = (object) $presenter->templateData;

// Render content
if( $presenter->activeContent->authorizationPage === true ){
    require_once(AUTH_DIR.'_header.php');
    require_once($presenter->activeContent->pagePath);
    require_once(AUTH_DIR.'_footer.php');
}else{
    $shortCodes = new shortCodes($database, $appController);
    require_once(LAYOUT_DIR.'layout.php');
}
$database->queryCounter("Render");;