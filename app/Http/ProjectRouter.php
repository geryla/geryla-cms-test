<?php
class ProjectRouter{
    private $router;
    private $database;
    private $appController;
    private $modulesController;
    private $templateClass;
    private $localeEngine;
    private $localizationClass;
    private $moduleLoader;

    protected $contentTables;
    protected $replacementTables;
    protected $templatesContentTable = ["templates" => null];
    protected $translationsEnabled = false;

    private $parsedUrl;

    public $activeTemplate;
    public $activeModule;
    public $langFile;
    protected $modelName;

// Template arrays
    private $templateInfo = [];
    private $templateData = [];
    private $breadcrumbs = [];
// Template data rules
    private $skipBreadcrumbs = false;
    private $defaultTemplate = null;
    private $blogCategoryBread = null;
    private $routerOptions = [];
// Flags for controller/presenter
    private $replaceContent = false;
    private $replaceContentModule = null;
// Language rules
    private $autoUrlFixEnabled = true; // Enable automatic fix of URL part by PREFIX
    private $switchLangByUrlEnabled = true; // Allow switch app language by URL

    private $defaultAppLang = null; // Default application language for render (not editable)
    private $forcedAppLang = null; // Forced language for render (editable)
    private $requestedAppLang = null; // Required language for manual switch by parameter

    private $switchLangRequested = false; // Required change of active language
    private $urlPartsHistory = []; // Contains url parts info

    private $redirectUrlPath = null;
    private $allowSubPagesControl = true;

    private $routerData = [
        "code" => 200,
        "redirect" => null,
        "maintenance" => false,
        "activeTemplate" => null
    ];

    public function __construct(Router $router, AppController $appController) {
        $this->router = $router;
        $this->appController = $appController;
        $this->modulesController = $appController->returnModulesController();
        $this->localeEngine = $appController->returnLocaleEngine();
        $this->localizationClass = $appController->returnLocalizationClass();
        $this->templateClass = $appController->returnTemplatesClass();

        $this->database = $router->getDatabase();
        $this->moduleLoader = new moduleLoader($this->database, $appController);

        $this->parsedUrl = $this->router->getParsedUrl();
        $this->langFile = $appController->getLangFile();
    }

    public function get(){
        return $this->routerData;
    }
    public function init(Array $options = []){
        $this->routerOptions = $options;
        $this->contentTables = $this->modulesController->returnContentTables();
        $this->replacementTables = $this->modulesController->returnReplacementTables();
        $this->translationsEnabled = (count($this->localizationClass->allActiveLanguages) > 1) ? true : false;
        $this->templatesContentTable["templates"][] = databaseTables::getTable("templates", "templateTableLang");

        $redirectOnly = isExisting("redirectOnly", $this->routerOptions);

        $this->loadLocale();
        $templateFound = $this->loadTemplate(!$redirectOnly);
        if($templateFound === false){
            return false;
        }

    // CONTENT | TEMPLATE DATA - Get data for page render (template)
        $this->setTemplate();

        // CONFIG | Specific logic for project
        if (file_exists(CONFIG_DIR.'/_ProjectRoutes.php')) {
            $routesConfig = include_once(CONFIG_DIR.'/_ProjectRoutes.php');
            $routeConfigResponse = null;
            $routesParams = [
                "activeTemplate" => $this->activeTemplate
            ];

            foreach ($routesConfig as $functionName => $functionData) {
                if ($functionName === $this->activeTemplate && isset($functionData['action']) && is_callable($functionData['action'])) {
                    if($redirectOnly === true){
                        if($this->parsedUrl["urlPartsCount"] > 1){
                            for($part=2; $part <= $this->parsedUrl["urlPartsCount"]; $part++){
                                $this->activeTemplate .='/'.$this->parsedUrl["urlParts"][$part];
                            }
                        }
                    }else{
                        $params = array_map(function ($param) use ($functionData, $routesParams) {
                            if (strpos($param, 'this->') === 0) {
                                return $this->{substr($param, 6)};
                            } elseif (array_key_exists($param, $routesParams)) {
                                return $routesParams[$param];
                            } else {
                                return null;
                            }
                        }, $functionData['params']);
                        $routeConfigResponse = $functionData['action'](...$params);
                    }
                }
            }

            if($routeConfigResponse !== null){
                $this->allowSubPagesControl = $routeConfigResponse["allowSubPagesControl"];

                if($routeConfigResponse["isExisting"] === false){
                    $this->templateInfo = $this->tempByName("notFound", $this->forcedAppLang);
                    $this->setTemplate();

                    $this->router->setHttp(404, null);
                    $this->router->initHttp();
                }else{
                    if($routeConfigResponse["activeTemplate"] !== null){
                        $this->activeTemplate = $routeConfigResponse["activeTemplate"];
                    }
                    if(!empty($routeConfigResponse["templateData"])){
                        $this->templateData = $routeConfigResponse["templateData"];
                    }
                    if(!empty($routeConfigResponse["templateInfo"])){
                        $this->templateInfo = $routeConfigResponse["templateInfo"];
                    }
                }
            }
        }

        if($redirectOnly === true){
            $this->activeTemplate = $this->checkTemplateDependency($this->activeTemplate);
            $this->response(true);
            return true;
        }

    // CONTENT | Check and set content data and assets
        $this->build();

    // BUILD DATA | Build object with data and rules for the view render
        $this->response(false);
        return true;
    }
    private function build(){
    // BREADCRUMBS | Init
        $breadCrumbs = new breadCrumbs();

    // BREADCRUMBS | Add homepage breadcrumbs
        if($this->activeTemplate !== "notFound"){
            $homeTemplate = $this->templateClass->getTemplateInfoByName("home", $this->forcedAppLang, false);
            $breadCrumbs->add($homeTemplate, "home");
        }
    // BREADCRUMBS | Add template
        if($this->skipBreadcrumbs !== true){
            $breadCrumbs->add($this->templateData, $this->activeTemplate);
        }

    // ORDERS - Order detail info
        if($this->activeTemplate === "orderDetail" && $this->appController->isActiveModule("shoppingCart")){
            $this->allowSubPagesControl = false;
            $orderExist = false;
            if(isset($this->parsedUrl["urlParts"][2])){
                $moduleOrder = new moduleOrders($this->database, $this->appController);
                $existingOrder = $moduleOrder->checkExistingOrder($this->parsedUrl["urlParts"][2]);

                $isValidToken = (isset($existingOrder["previewToken"]) && ($existingOrder["previewToken"] !== null && $existingOrder["previewToken"] !== '')) ? false : true;
                $previewToken = (!empty($this->parsedUrl["urlParams"]) && isset($this->parsedUrl["urlParams"]["previewToken"])) ? $this->parsedUrl["urlParams"]["previewToken"] : null;
                if($isValidToken === false && $previewToken !== null){
                    if($existingOrder["previewToken"] === $previewToken){
                        $isValidToken = true;
                    }
                }

                if(!empty($existingOrder) && $isValidToken === true){
                    $orderExist = true;
                    $this->templateData["path"] .= "/{$this->parsedUrl['urlParts'][2]}?previewToken={$existingOrder['previewToken']}}";
                    $this->templateData["orderId"] = (int) $this->parsedUrl["urlParts"][2];
                }
            }

            if($orderExist === false){
                $this->templateInfo = $this->tempByName("notFound", $this->forcedAppLang);
                $this->setTemplate();

                $breadCrumbs->removeAll();
                $breadCrumbs->add($this->templateData, $this->activeTemplate);

                $this->router->setHttp(404, null);
                $this->router->initHttp();
            }
        }

    // CONTENT | TEMPLATE INFO - Get updated info based on url parts (sub pages)
        if($this->allowSubPagesControl === true && $this->parsedUrl["urlPartsCount"] > 1 && $this->activeTemplate != "notFound"){
            for($urlCount = 2; $urlCount <= $this->parsedUrl["urlPartsCount"]; $urlCount++){
            // PARSE URL part and search for the content
                $partUrl = $this->router->breakUrlParts($this->parsedUrl["urlParts"][$urlCount]);

            // CHANGE FORCED language if switch lang by url is enabled
                $this->forcedAppLang = ($this->switchLangByUrlEnabled === true) ? $this->templateInfo["langShort"] : $this->forcedAppLang;

            // CONTENT TYPE C) | Sub-page object
                $subContentResults = $this->searchTables($this->contentTables, $urlCount, $partUrl["url"], $partUrl["urlPrefix"]);

            // VALIDATION | Sub content validation
                $subContentValid = (!empty($subContentResults["data"])) ? true : false;
                $this->templateInfo = ($subContentValid === true) ? $subContentResults["data"] : [];
                
            // VISIBILITY | Check if object got valid rules for the visibility
                if(!empty($this->templateInfo)){
                    $nullInfo = false;

                    // BLOG (Category and Post) - module ContentEditor
                    if( $this->templateData["templateName"] == "blog" ){
                        if( !empty($partUrl["urlPrefix"]) && $this->templateInfo["postType"] != 2 ){
                            $nullInfo = true; // Try to access page which may be CLASSIC PAGE
                        }
                    }

                    // PAGES (Post) - module ContentEditor
                    if( $this->templateData["templateName"] == "page" ){
                        if( $urlCount == 2 ){
                            if( isset($this->templateInfo["urlPrefix"]) && empty($partUrl["urlPrefix"]) ){
                                $nullInfo = true; // Try to access page post without URL PREFIX
                            }elseif( $this->templateInfo["postType"] == 2 ){
                                $nullInfo = true; // Try to access page which may be BLOG PAGE
                            }
                        }
                    }

                    // VISIBILITY - Settings from administration
                    if( $this->templateInfo["visibility"] == 0){
                        $nullInfo = true;
                        if(env('PROJECT_DISABLED') === true ){
                            if(is_array($this->parsedUrl["urlParams"])){
                                if(array_key_exists("preview", $this->parsedUrl["urlParams"])){
                                    $nullInfo = false;
                                }
                            }
                        }else{
                            if(!empty($_SESSION["security"]["adminLogin"]["adminSalt"])){
                                $nullInfo = false;
                            }
                        }
                    }

                    // RESULT - Null content - 404 not found
                    if( $nullInfo === true ){
                        $this->templateInfo = [];
                    }
                }

            // 404 FORCED | Mark URL as not found and stop sub pages loop
                if(empty($this->templateInfo)){
                    $this->templateInfo = $this->tempByName("notFound", $this->forcedAppLang);
                    $this->router->setHttp(404, null);
                    $this->setTemplate();

                    $breadCrumbs->removeAll();
                    $breadCrumbs->add($this->templateData, $this->activeTemplate);
                    break;
                }

                $entityExtend = ['all'];

            // Create object of MODULE CONTENT (all new modules with content showing must be added in this functions!)
                $newTemplate = $this->moduleLoader->createModuleEntity($this->activeTemplate, $subContentResults["tables"]["data"], $this->templateInfo, $this->routerOptions["thumbnail"], $entityExtend);
                $this->activeTemplate = $newTemplate->activeTemplate;
                $this->templateData = $newTemplate->objectData;

                if($this->activeTemplate === 'productDetail'){
                    if($this->templateInfo["type"] === 'combination' && !empty($this->templateData["variants"])){
                        if(!empty($this->parsedUrl["urlParams"]) && array_key_exists("combination", $this->parsedUrl["urlParams"])){
                            $productVariants = new productVariants($this->database, $this->appController);
                            $combinationIdentify = $this->parsedUrl["urlParams"]["combination"];
                            foreach($this->templateData["variants"] as &$variantInfo){
                                $variantInfo["isActive"] = false;
                                if($variantInfo["combination"] == $combinationIdentify){
                                    $this->templateData = $productVariants->setVariantInfoIntoProduct($this->templateData, $variantInfo);
                                    $variantInfo["isActive"] = true;
                                    break;
                                }
                            }
                        }
                    }
                }

            // BREADCRUMBS - Check for parent content #1 (parent object - CMS groups, CMS categories)
                if(isset($this->templateData["parentId"]) && $this->templateData["parentId"] != 0){
                    $helpId = $this->templateData["parentId"];
                    $helpTable = $subContentResults["tables"]["lang"];
                    $helpBread = [];

                    do{
                        $helpInfo = $this->getContentFromOtherTableById($helpId, $helpTable);
                        $helpData = $this->moduleLoader->createModuleEntity($this->activeTemplate, $helpInfo["contentTable"], $helpInfo["data"], $this->routerOptions["thumbnail"], $entityExtend);
                        $helpId = $helpData->objectData["parentId"];
                        if($helpData->activeTemplate != "notFound"){
                            $helpBread[] = (array) $helpData->objectData;
                        }

                    }while($helpId != 0);

                    $helpBread = array_reverse($helpBread);
                }

            // BREADCRUMBS - Check for parent content #2 (associated object - products, blog posts)
                if( !isset($helpBread) && $newTemplate->parentBreadCrumbs !== null ){
                    $parentBread = $this->getBreadcrumbsContentFromOtherTable($newTemplate->parentBreadCrumbs, $this->templateData, $this->blogCategoryBread);
                    $helpBread = $parentBread["data"];
                }

            // BREADCRUMBS - Add parent breadcrumbs with check for blogCategory
                if(isset($helpBread) && !empty($helpBread)){
                    $lastBread = $breadCrumbs->getLast();
                    if(isset($lastBread->template) && $lastBread->template == "blogCategory"){
                        $breadCrumbs->removeLastBread();
                    }
                    foreach($helpBread as $addBread){
                        if(isset($addBread["visibility"]) && (int) $addBread["visibility"] === 0){
                            continue;
                        }
                        $breadCrumbs->add($addBread, ($addBread["template"] ? $addBread["template"] : $this->activeTemplate));
                    }
                }

            // BREADCRUMBS - Add new template content
                $breadCrumbs->add($this->templateData, $this->activeTemplate);

            // Stop loading other URL parts if CONTENT NOT FOUND
                if( $this->activeTemplate == "notFound" ){break;}

            // If PREFIX is OK, but URL NOT - set up redirect to right URL
                if($this->autoUrlFixEnabled === true && $this->parsedUrl["urlPath"] != $this->redirectUrlPath){
                    $correctUrl = substr($this->redirectUrlPath, 1);
                    $redirectPath = (strpos($correctUrl, '/') === 0) ? $correctUrl : '/'.$correctUrl;

                    $this->routerData["code"] = 303;
                    $this->routerData["redirect"] = $redirectPath;
                    $this->router->setHttp(303, $redirectPath);
                    $this->router->initHttp();

                    if(env('PROJECT_DISABLED') === true ){
                        return false;
                    }else{
                        die;
                    }
                }
            }
        }

    // LANGUAGE | Switch active language if it is required by app (by valid parameter or satellite)
        $this->switchLangRequested = ($this->defaultAppLang !== $this->forcedAppLang) ? true : $this->switchLangRequested;
        if($this->switchLangRequested === true){
            $reloadPage = false;
            if($this->requestedAppLang === null){
                // Satellite OR Switch by URL
                $this->localeEngine->setLanguage("appLang", $this->forcedAppLang);
                if($this->switchLangByUrlEnabled){
                    $reloadPage = true;
                }
            }else{
                // Parameter
                $this->localeEngine->setLanguage("appLang", $this->requestedAppLang);
                $reloadPage = true;
            }

            if($reloadPage === true){
                $this->router->setHttp(303, substr($this->parsedUrl["urlPath"], 1));
                $this->router->initHttp();
            }
        }

    // SEO | Disable indexing content for product categories with a filtration enabled
        if($this->activeTemplate == "productCategory" && !empty($this->parsedUrl["urlParams"])){
            if(useIfExists(env('ALLOW_INDEX_FILTERS'), false) === false){
                $this->templateData["allowIndex"] = 0;
            }
        }

    // BREADCRUMBS | MODULE ext: Gallery - Add for objects with pageType = 0 (templatePartsCount === 0)
        if( $this->activeTemplate == "gallery" && $this->parsedUrl["urlPartsCount"] === 0 && count($this->parsedUrl["urlParts"]) > 1 ){
            $moduleGallery = new moduleGallery($this->database, $this->appController);
            $galleryGroup =  null;
            for($gBread = 1; $gBread <= count($this->parsedUrl["urlParts"]); $gBread++){
                $gBreadData = $moduleGallery->getOneVisibleGalleryBySlug($this->templateData["langShort"], $this->parsedUrl["urlParts"][$gBread]);
                if(!empty($gBreadData) && $galleryGroup === null){
                    $prefixEngine = new prefixEngine($this->database);
                    $galleryGroup = $moduleGallery->getOneGroup($gBreadData["groupId"], $this->templateData["langShort"]);
                    $breadCrumbs->add((object) ["id" => $galleryGroup["id"], "title" => $galleryGroup["title"], "url" => $this->templateData["url"].'/'.$prefixEngine->title2pagename($galleryGroup["title"]), "template" => $this->activeTemplate], $this->activeTemplate);
                }
                $breadCrumbs->add((object) ["id" => $gBreadData["id"], "title" => $gBreadData["title"], "url" => $this->templateData["url"].'/'.$gBreadData["slug"], "template" => $this->activeTemplate], $this->activeTemplate);
            }
        }
        $this->breadcrumbs = $breadCrumbs->get();

    // HTTP HEADERS | Set correct header and force redirect if needs
        $this->router->initHttp();

        return true;
    }
    private function response(Bool $shortedResponse = false){
        $this->routerData["activeTemplate"] = $this->activeTemplate;

        if($shortedResponse === false){
            $this->routerData["templateData"] = $this->templateData;
            $this->routerData["activeLanguage"] = $this->localizationClass->getOneLangByLangShort($this->localeEngine->returnProjectLang());
            $this->routerData["templatesAnchors"] = $this->loadTemplatesAnchors();
            $this->routerData["activeContent"] = (object) $this->prepareController();
            $this->routerData["breadCrumbs"] = $this->breadcrumbs;
        }
    }

    private function loadLocale(){
    // PARSE URL - Get URL info
        $this->router->setHttp(200, null);

    // SET EXPECTED language for the default content load
        //$this->forcedAppLang = $this->localeEngine->returnProjectLang();
        $this->forcedAppLang = $this->appController->getLang();

    // LANGUAGE | Switcher - Trigger manual language change
        (is_string($this->parsedUrl["urlParams"])) ? parse_str($this->parsedUrl["urlParams"],$this->parsedUrl["urlParams"]) : [];
        if(isset($this->parsedUrl["urlParams"]["setLang"])){
            $this->switchLangRequested = true;
            $this->requestedAppLang = $this->parsedUrl["urlParams"]["setLang"];
            unset($this->parsedUrl["urlParams"]["setLang"]);
        }

    // LANGUAGE | MODULE ext: Local satellite - Enforce automatic language change
        if($this->appController->isActiveModule("localeSatellite")){
            $localeSatellite = new localeSatellite($this->database, $this->appController);

            // SET active language by satellite domain OR REMOVE possible manual trigger IF satellite is enabled
            if($localeSatellite->checkSatelliteAllow()){
                $this->switchLangByUrlEnabled = false;
                $this->requestedAppLang = null;

                if($localeSatellite->switchLanguageBySatellite($this->localeEngine->returnProjectLang()) === true){
                    $this->switchLangRequested = true;
                    $this->forcedAppLang = $localeSatellite->setSatelliteLang();
                }else{
                    $this->switchLangRequested = false;
                }
            }
        }

    // LANGUAGE | Copy expected language into variable as backup (forcedAppLang can be changed)
        $this->defaultAppLang = $this->forcedAppLang;
    }
    private function loadTemplate(Bool $verifySessionRules = true){
        // REDIRECTS CHECK - Compare URL with possible redirects
        $redirected = new redirectedUrls($this->database);
        $redirectInfo = $redirected->getByUrl($this->parsedUrl["urlPath"], true, $this->forcedAppLang);
        if(!empty($redirectInfo) && $redirectInfo !== null){
            $this->routerData["code"] = $redirectInfo["status"];
            $this->routerData["redirect"] = $redirectInfo["outputUrl"];
            $this->router->setHttp($redirectInfo["status"], $redirectInfo["outputUrl"]);
            $this->router->initHttp();
            return false;
        }

        // MAINTENANCE CHECK
        $maintenance = new maintenanceCatcher($this->database);
        $maintenanceData = $maintenance->checkMaintenance($this->forcedAppLang);
        if($maintenanceData["enabled"] === true){
            $this->routerData["code"] = 503;
            $this->router->setHttp($this->routerData["code"], null);
            $this->router->initHttp();

            $this->routerData["maintenance"] = true;
            $this->routerData["metaData"] = $maintenanceData["metaData"];
            $this->routerData["templateData"] = $maintenanceData["data"];
            return false;
        }

        // CONTENT | TEMPLATE INFO - Get correct template (from modules) or set Home/404
        $this->activeTemplate = null;
        if(empty($this->parsedUrl["urlParts"]) || $this->parsedUrl["urlParts"][1] == "/" || $this->parsedUrl["urlParts"][1] == ""){
            $this->templateInfo = $this->tempByName("home", $this->forcedAppLang);
            $this->activeTemplate = "home";
        }

        // CONTENT | Template content tables
        if($this->activeTemplate === null){
            $searchedResult = $this->searchTables($this->templatesContentTable, 1, $this->parsedUrl["urlParts"][1], null);
            $this->templateInfo = ($searchedResult["data"]) ?? null;
            $this->activeTemplate = ($searchedResult["data"]["templateName"]) ?? null;
        }

        // SESSION rules
        if($verifySessionRules === true){
            if($this->activeTemplate !== null && $this->appController->isActiveModule("shoppingCart")){
                $cartRouter = new CartRouter($this->database, $this->appController);
                $cartResponse = $cartRouter->preventCartRoute($this->activeTemplate, useIfExists($this->parsedUrl["urlParams"], []));
                if($cartResponse["redirect"] !== null){
                    $this->routerData["code"] = $cartResponse["redirect"]["status"];
                    $this->routerData["redirect"] = $cartResponse["redirect"]["outputUrl"];
                    $this->router->setHttp($cartResponse["redirect"]["status"], $cartResponse["redirect"]["outputUrl"]);
                    $this->router->initHttp();
                    return false;
                }
            }
            if($this->activeTemplate !== null && isset($this->templateInfo["loggedOnly"]) && $this->appController->isActiveModule("users")){
                $usersController = new UsersController($this->database, $this->appController);
                $userPermissions = $usersController->checkPermissions($this->activeTemplate, $this->templateInfo["loggedOnly"]);
                if($userPermissions["redirect"] !== null){
                    $this->routerData["code"] = $userPermissions["redirect"]["code"];
                    $this->routerData["redirect"] = $userPermissions["redirect"]["url"];
                    $this->router->setHttp($userPermissions["redirect"]["code"], $userPermissions["redirect"]["url"]);
                    $this->router->initHttp();
                    return false;
                }
            }
        }

        if($this->activeTemplate === "home"){
            return true;
        }

        // CONTENT | Modules secondary content tables (replace-content pages)
        if(empty($this->templateInfo)){
            $replacedResults = $this->searchTables($this->replacementTables, 1, $this->parsedUrl["urlParts"][1], null);
            $this->templateInfo = $replacedResults["data"];

            if(!empty($this->templateInfo) && $this->templateInfo["visibility"] == 1){
                $this->activeTemplate = $this->templateInfo["templateName"];
                $this->replaceContentModule = $this->templateInfo["moduleName"];
                // SET variable for render view
                $this->replaceContent = true;
            }else{
                $this->templateInfo = null;
            }
        }

        // REPLACE templateName as default template for content - PAGE (exclude pages with default style)
        if(!empty($this->templateInfo)){
            if(isset($this->templateInfo["templateName"])){
                $this->modelName = $this->templateInfo["templateName"];

                if($this->templateInfo["moduleName"] === "marketing"){
                    $this->modelName = $this->templateInfo["moduleName"];
                    $this->replaceContent = false;
                    $this->activeTemplate = "page";
                }
            }
        }

        // CONTENT check | Update template info and variable for next steps
        if(!empty($this->templateInfo) && isset($this->templateInfo["pageType"])){
            if($this->parsedUrl["urlPartsCount"] > 1 && $this->templateInfo["pageType"] == 0){
                $this->parsedUrl["urlPartsCount"] = 0; // Skip checking sub-page (step 6) - only keep URL with more params to controller check
            }elseif( $this->parsedUrl["urlPartsCount"] > 1 && $this->templateClass->pageTypes[$this->templateInfo["pageType"]] === false){
                $this->templateInfo = null; // URL must not have more than 1 param
            }elseif( $this->parsedUrl["urlPartsCount"] <= 1 && $this->templateClass->pageTypes[$this->templateInfo["pageType"]] === true){
                $this->templateInfo = null; // URL must have less than 2 params
            }

            if($this->templateClass->pageTypes[$this->templateInfo["pageType"]] === true){
                $this->skipBreadcrumbs = true;
            }
        }

        // CONTENT | 404 template - if all previous conditions failed
        if(empty($this->templateInfo)){
            $this->templateInfo = $this->tempByName("notFound", $this->forcedAppLang);
            $this->modelName = $this->templateInfo["templateName"];
            $this->routerData["code"] = 404;
            $this->router->setHttp(404, null);
            $this->router->initHttp();
        }

        return true;
    }
    private function prepareController(){
        $templateFolder = ($this->replaceContentModule === null) ? PAGES_DIR : MODULE_DIR.$this->replaceContentModule.'/pages/';
        $templatePath = ($this->replaceContentModule === null) ? $templateFolder.$this->activeTemplate.'.blade.php' : $templateFolder.$this->activeTemplate.'.php';
        $templateController = CONTROLLERS_DIR.$this->activeTemplate.'.php';
        $templateController = (file_exists($templateController)) ? $templateController : null;

        return [
            "templateName" => $this->activeTemplate,
            "templatePath" => $templatePath,
            "templateController" => $templateController,
            "replaceContent" => $this->replaceContent,
            "replaceModule" => $this->replaceContentModule
        ];
    }
    private function loadTemplatesAnchors(){
        $CMSGroups = new cmsGroups($this->database, $this->appController);

    // ANCHORS | Templates - Get all templates links for active language
        $templatesAnchors = $this->templateClass->allTemplates;

    // ANCHORS | Replaced content - Get all links for active language
        if(!empty($this->replacementTables)){
            $allAnchors = [];

            foreach($this->replacementTables as $moduleName => $module){
                foreach($module as $table){
                    $allLinks = $this->database->getQuery("SELECT id, templateName, url FROM `{$table}`;", null, true);
                    if (!empty($allLinks)) {
                        foreach ($allLinks as $link) {
                            $allAnchors[$link["templateName"]] = $link["url"];
                        }
                    }
                }
            }

            if(!empty($allAnchors)){
                $templatesAnchors = array_merge($templatesAnchors, $allAnchors);
            }
        }

    // ANCHORS | CMS Groups - Get all links for active language
        $groupAnchors = $CMSGroups->getAllGroupsAnchors($this->forcedAppLang, false);

        return array_merge($templatesAnchors, $groupAnchors);
    }
    private function checkTemplateDependency(String $templateName){
        if($this->parsedUrl["urlPartsCount"] > 1){
            $urlParts = $this->router->breakUrlParts($this->parsedUrl["urlParts"][2]);

            if($templateName === 'blog'){
                if($urlParts["urlPrefix"] !== null){
                    $templateName = 'blogDetail';
                }else{
                    $templateName = 'blogCategory';
                }
            }
        }

        return $templateName;
    }

// Help functions
    private function setTemplate(){
        $templateInfo = $this->templateInfo;
        $templateInfo["modelName"] = $this->modelName;
        if($templateInfo["moduleName"] === "marketing"){
            $marketingClass = new marketingClass($this->database, $this->appController);
            $optimizedTemplate = $marketingClass->optimize($templateInfo);
        }else{
            $optimizedTemplate = $this->templateClass->optimize($templateInfo);
        }// TODO: FIX THIS

        $templateEntity = new EntityTemplate($optimizedTemplate);
        $this->templateData = $templateEntity->get();

        $this->activeModule = $this->templateData["moduleName"];
        $this->defaultTemplate = $this->activeTemplate;
    }
    private function tempByName($tmpName, $langShort = null) {
        $tmpLang = ($langShort !== null) ? $langShort : $_SESSION["languages"]["appLang"];
        $templateInfo = $this->templateClass->getTemplateInfoByName($tmpName, $tmpLang, false);
        $this->activeTemplate = $tmpName;

        return $templateInfo;
    }
    private function buildUrlPart($url, $urlPrefix){
        $dataPrefix = (isset($urlPrefix) && $urlPrefix > 0) ? $urlPrefix : null;
        $dataUrl = (isset($url)) ? $url : null;
        return (($dataPrefix !== null) ? $dataPrefix."-" : null).$dataUrl;
    }

    private function searchTables($dataTables, $urlPartLevel, $url, $prefix = null){
        // SEARCH FOR DATA | Original data BY URL link
        $objectData = $this->searchForData($dataTables, $url, $prefix);
        $dataLanguage = null;

        // RULES | Get rules by returned data array
        $dataExists = (!empty($objectData["data"])) ? true : false;
        $multipleDataObjects = (isset($objectData["data"][0])) ? true : false;

        // DATA Correction | In case of multiple data pick correct language
        $returnArray = ($dataExists) ? $objectData : [];
        $returnArray["data"] = ($dataExists === true && $multipleDataObjects === true) ? $this->localizationClass->selectRightTranslate($objectData["data"], $this->forcedAppLang) : $objectData["data"];
        $returnArray["translated"] = false;

        // RULES | Get data language
        if(!empty($returnArray["data"])){
            if(!isset($returnArray["data"]["langShort"])){
                $returnArray["data"]["langShort"] = $this->forcedAppLang;
            }
            $dataLanguage = $returnArray["data"]["langShort"];
        }

        // CHECK match
        $dataMatched_url = ($url === (($returnArray["data"]["url"]) ?? null)) ? true : false;
        $dataMatched_lang = ($dataLanguage === $this->forcedAppLang) ? true : false;

        // DATA Re-search | Only if not matched and translations are enabled
        if($dataMatched_url === false || $dataMatched_lang === false){
            if($this->translationsEnabled === true){
                // Check for translations to search forced language mutation
                $translateData = $this->searchForTranslation($returnArray["data"], $this->forcedAppLang, $url);
                $translationFound = (!empty($translateData)) ? true : false;

                // Check for translation based on URL, if dataURL is not matched with require url (from multiple)
                if($translationFound === false && $dataMatched_lang === true && $dataMatched_url === false){
                    if($multipleDataObjects){
                        $translatedObject = array_search($url, array_column($objectData["data"], 'url'));
                        if($translatedObject !== false){
                            $translateData = $objectData["data"][$translatedObject];
                            $translationFound = true;
                        }
                    }
                }

                // If translated data language is not same as required
                $translateLanguage = ($translationFound) ? $translateData["langShort"] : $dataLanguage;
                if($translateLanguage !== $this->forcedAppLang){
                    $resetData = false;
                    // Satellite enabled | skip control (mixed language URL not allowed)
                    if($this->switchLangByUrlEnabled !== true){
                        $resetData = true;
                    }
                    // Satellite disabled | check if all URL parts are in same language (switch app to different language) or not
                    if($this->switchLangByUrlEnabled === true){
                        if($urlPartLevel > 1){
                            $lastUrlPart = end($this->urlPartsHistory);
                            // If the previous URL part is not same as actual (mixed URL) reset data and forced language
                            if($lastUrlPart["langShort"] !== $translateLanguage){
                                $resetData = true;
                                $this->forcedAppLang = $this->defaultAppLang;
                            }
                        }
                    }

                    if($resetData== true){
                        $returnArray = $translateData = [];
                    }
                }

                if(!empty($translateData)){
                    $returnArray["data"] = $translateData;
                    if(isset($translateData["contentTable"])){
                        $returnArray["tables"]["data"] = $translateData["contentTable"];
                    }

                    $returnArray["translated"] = true;
                }
            }else{
                $returnArray = [];
            }
        }

        if(!empty($returnArray["data"])){
            // Create redirect PATH if manual language switch by PARAM is active
            $redirectUrlPath = null;
            if($this->switchLangRequested === true && $this->requestedAppLang !== null){
                if($returnArray["data"]["langShort"] !== $this->requestedAppLang){
                    if($objectData["tables"]["useGlobalTable"] === true){
                        $redirectedContent = $this->localizationClass->checkExistingTranslationByDefaultUrl($returnArray["data"]["url"], $this->requestedAppLang);
                    }else{
                        $redirectedContent = $this->getContentFromOtherTableById($returnArray["data"]["id"], $objectData["tables"]["lang"], $this->requestedAppLang);
                        $redirectedContent = (!empty($redirectedContent["data"])) ? $redirectedContent["data"] : [];
                    }

                    $redirectUrlPath = $this->buildUrlPart($redirectedContent["url"], $redirectedContent["urlPrefix"]);
                }
            }

            // Save URL parts history and redirect path
            $dataUrlPath = $this->buildUrlPart($returnArray["data"]["url"], $returnArray["data"]["urlPrefix"]);
            $this->urlPartsHistory[] = ["langShort" => $returnArray["data"]["langShort"], "url" => $dataUrlPath];
            $this->redirectUrlPath .= "/".(($redirectUrlPath === null) ? $dataUrlPath : $redirectUrlPath);
            if($returnArray["tables"]["useGlobalTable"] === false){
                $targetLang = $returnArray["data"]["langShort"];
                $updateFound = $this->localizationClass->updateExistingTranslationByTableName($targetLang, $this->localeEngine->returnMainLanguage()["langShort"], $returnArray["tables"]["lang"], $returnArray["data"]);
                if($updateFound !== false && !empty($updateFound)){
                    $returnArray["data"] = $updateFound;
                }
            }

            // Update DATA array
            $returnArray["data"]["templateName"] = (isset($returnArray["data"]["templateName"])) ? $returnArray["data"]["templateName"] : $this->activeTemplate;
        }

        return $returnArray;
    }
    private function searchForData($dataTables, $url, $prefix = null){
        $returnArray = ["data" => []];

        foreach($dataTables as $groupName => $tableArray){
            foreach($tableArray as $langTableName){
                // Create two tables if -lang prefix exist (tableName and tableName-lang)
                $dataTableName = ( strpos($langTableName, "-lang") !== false ) ? str_replace("-lang", "", $langTableName) : null;

                // SEARCH for data by URL (PREFIX optional)
                $sqlQuery = $this->createQuery($langTableName, $dataTableName, $url, $prefix);
                $searchedResults = $this->database->getQuery($sqlQuery["sql"], $sqlQuery["bind"], false);

                if( !empty($searchedResults) ){
                    $returnArray["data"] = $searchedResults;
                    $returnArray["tables"]["data"] = ($dataTableName !== null) ? $dataTableName : $langTableName;
                    $returnArray["tables"]["lang"] = $langTableName;
                    $returnArray["tables"]["useGlobalTable"] = ($returnArray["tables"]["data"] === $returnArray["tables"]["lang"]) ? true : false;
                    break 2;
                }
            }
        }

        return $returnArray;
    }
    private function searchForTranslation($dataArray, $forcedAppLang, $url){
        // SEARCH FAILED | Try look for data in global translation table by translated URL
        if( empty($dataArray) ){
            $translateData = $this->localizationClass->checkExistingTranslation($url, $forcedAppLang);
            return $translateData;
        }

        // SEARCH SUCCESS - Multiply | Select correct translated from multiple results by forced language
        if( isset($dataArray[0]) ){
            $translateData = $this->localizationClass->selectRightTranslate($dataArray, $forcedAppLang);
            return $translateData;
        }

        return [];
    }
    private function createQuery($table, $baseTable, $url, $prefix = null){
        $bindValues = [];

        if( $this->autoUrlFixEnabled !== true ){
            // Both parameters must be right (URL PREFIX and URL ADDRESS)
            if( $baseTable !== null ){
                $select = "SELECT  meta.*, data.* FROM `{$baseTable}` AS data INNER JOIN `{$table}` AS meta ON data.id = meta.associatedId WHERE meta.url = :url";
            }else{
                $select = "SELECT meta.* FROM `{$table}` AS meta WHERE meta.url = :url";
            }
            $bindValues["url"] = $url;

            if( $prefix !== null ){
                $select .= " AND data.urlPrefix = :urlPrefix";
                $bindValues["urlPrefix"] = $prefix;
            }

        }else{
            // Only PREFIX must be right, after successful control page redirect to right URL
            if( $baseTable !== null ){
                $select = "SELECT meta.*, data.* FROM `{$baseTable}` AS data INNER JOIN `{$table}` AS meta ON data.id = meta.associatedId WHERE ";
            }else{
                $select = "SELECT meta.* FROM `{$table}` AS meta WHERE ";
            }

            if( $prefix !== null && $baseTable !== null ){
                $select .= "data.urlPrefix = :urlPrefix";
                $bindValues["urlPrefix"] = $prefix;
            }else{
                $select .= "meta.url = :url";
                $bindValues["url"] = $url;
            }

        }

        return $returnQuery = array("sql" => $select, "bind" => $bindValues);
    }

    private function getContentFromOtherTableById($objectId, $table, $langShort = null){
        $returnData = ["template" => null, "data" => [], "translation" => false];
        $appLang = ($langShort !== null) ? $langShort : $this->forcedAppLang;

        $baseTable = ( strpos($table, "-lang") !== false ) ? str_replace("-lang", "", $table) : null;
        if( $baseTable !== null ){
            $selectSql = "SELECT data.*,  meta.* FROM `{$baseTable}` AS data INNER JOIN `{$table}` AS meta ON data.id = meta.associatedId WHERE data.id = :id";
        }else{
            $selectSql = "SELECT meta.* FROM `{$table}` AS meta WHERE meta.id = :id";
        }

        $returnData["data"] = $this->database->getQuery($selectSql, ["id" => $objectId], false);   // Check if result exist

        // Check translation
        if( $this->translationsEnabled === true ){
            $translateData = $this->searchForTranslation($returnData["data"], $appLang, $returnData["data"]["url"]);

            if( !empty($translateData) ){
                $returnData["data"] = $translateData;
                $returnData["translation"] = true;
            }
        }

        // If have existing result
        if( !empty($returnData["data"]) ){
            $returnData["template"] = ( isset($returnData["data"]["templateName"]) ) ? $returnData["data"]["templateName"] : $this->activeTemplate;
            $returnData["data"]["templateName"] = $returnData["template"];
            $returnData["data"]["langShort"] = ( isset($returnData["data"]["langShort"]) ) ? $returnData["data"]["langShort"] : $appLang;
            $returnData["contentTable"] = ( $baseTable !== null ) ? $baseTable : $table;
        }

        return $returnData;
    }
    private function getBreadcrumbsContentFromOtherTable($sqlObject, $templateData, $blogCategoryBread){
        $returnData = ["data" => [], "translation" => false];

        $table = $sqlObject["data"];
        $crossTable = $sqlObject["cross"];
        $identifierObject = $sqlObject["identifierObject"];
        $identifierParent = $sqlObject["identifierParent"];

        // Create two tables if -lang prefix exist (tableName and tableName-lang)
        $baseTable = ( strpos($table, "-lang") !== false ) ? str_replace("-lang", "", $table) : null;
        $selectSql = ( $baseTable !== null ) ? "SELECT data.*,  meta.* FROM `{$baseTable}` AS data INNER JOIN `{$table}` AS meta ON data.id = meta.associatedId " : "SELECT meta.* FROM `{$table}` AS meta ";
        $selectParent = ( $baseTable !== null ) ? "SELECT data.*,  meta.* FROM `{$baseTable}` AS data INNER JOIN `{$table}` AS meta ON data.id = meta.associatedId " : "SELECT data.* FROM `{$table}` AS data ";

        // Check if PREV PAGE was on this site and if its true, return last param from URL address (WITHOUT URL PREFIX)
        $parsedLink = isset($_SERVER["HTTP_REFERER"]) ? parse_url($_SERVER["HTTP_REFERER"]) : [];
        if(!empty($parsedLink)){
            if($parsedLink["host"] == $_SERVER["HTTP_HOST"]){
                $urlParts = array_reverse(array_filter(explode("/", $parsedLink["path"])));
                $sqlReferer = $selectSql."WHERE meta.url = :url;";
                $parentObject = $this->database->getQuery($sqlReferer, ["url" => $urlParts[0]], false);
                $parentObject = ($parentObject["visibility"] == 1) ? $parentObject : null;
            }
        }

        // Check if object have some parent groups or categories - by object type
        if( !isset($parentObject) ){
            $condition = null;
            if($blogCategoryBread !== null){ // BLOG POST-CATEGORY condition
                $condition = 'AND '.$identifierParent.' = '.$blogCategoryBread;
            }
            if($templateData["templateName"] == "productDetail"){ //  PRODUCT DETAIL-CATEGORY condition
                $condition = 'ORDER BY headCategory DESC, categoryId DESC';
            }

            $crossObjectSql = "SELECT * FROM `{$crossTable}` WHERE {$identifierObject} = :id {$condition} LIMIT 1";
            $crossObject = $this->database->getQuery($crossObjectSql, ["id" => $templateData["id"]], false);

            if(!empty($crossObject)){
                $sqlParent = $selectParent."WHERE data.id = :id AND data.visibility = 1;";
                $parentObject = $this->database->getQuery($sqlParent, ["id" => $crossObject[$identifierParent]], false);
            }
        }

        // If object HAVE parent - continue ELSE break method
        if(!empty($parentObject)){
            do{
                // Initialize loop
                $break = true;
                $templateClass = $this->templateClass;

                // Check translation
                if( $this->translationsEnabled === true ){
                    $translateData = $this->searchForTranslation($parentObject, $this->forcedAppLang, $parentObject["url"]);

                    if( !empty($translateData) ){
                        $parentObject = $translateData;
                        $returnData["translation"] = true;
                    }
                }

                // If exist templateName in parentObject (groups in contentEditor) OR in default templateName is blog create URL PATH
                if( ((isset($parentObject["templateName"]) && !empty($parentObject["templateName"]))) OR (isset($templateData["templateName"]))  ){
                    if( $templateData["templateName"] == "blog" ){
                        $templatePath = $templateClass->getTemplateInfoByName($templateData["templateName"], $this->forcedAppLang, false);
                        $parentObject["url"] = '/'.$templatePath["url"]."/".$parentObject["url"]; // BLOG CATEGORY - templateName + category name
                    }elseif( $templateData["templateName"] == "productDetail" ){
                        $templatePath = $templateClass->getTemplateInfoByName("productCategory", $this->forcedAppLang, false);
                        $parentObject["parentTemplateUrl"] = '/'.$templatePath["url"]; // PRODUCT DETAIL - templateName + urlPrefix + URL
                        $parentObject["template"] = 'productCategory';
                    }else{
                        $templatePath = $templateClass->getTemplateInfoByName($parentObject["templateName"], $this->forcedAppLang, false);
                        if($parentObject["allowIndex"] == 1){
                            $parentObject["url"] = '/'.$templatePath["url"]."/".$parentObject["url"]; // GROUP - only templateName
                        }else{
                            return false; // Hide GROUP if is not allowed
                        }
                    }
                }


                $returnData["data"][] = ["id" => $parentObject["id"], "title" => $parentObject["title"], "url" => $parentObject["url"], "urlPrefix" => $parentObject["urlPrefix"], "template" => $parentObject["template"], "parentTemplateUrl" => $parentObject["parentTemplateUrl"]];

                if($parentObject["parentId"] != 0){
                    $sqlParent = $selectParent."WHERE data.id = :id AND data.visibility = 1;";
                    $parentObject = $this->database->getQuery($sqlParent, ["id" => $parentObject["parentId"]], false);

                    // If exist another parent continue loop
                    if(!empty($parentObject)){$break = false;}

                    // If exist col templateName (groups in contentEditor) and is empty, not show in breadcrumbs - break loop
                    if( isset($parentObject["templateName"]) && empty($parentObject["templateName"]) ){
                        $break = true;
                    }
                }

            }while($break === false);
        }

        // If have existing result
        if( !empty($returnData["data"]) ){
            $returnData["data"] = array_reverse($returnData["data"]);
        }

        return $returnData;
    }
}