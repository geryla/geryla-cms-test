<?php
class AppRouter{
    private $router;
    private $database;
    private $modulesController;
    private $templateClass;
    private $localeEngine;
    private $localizationClass;

    private $parsedUrl;
    public $langFile;

    private $adminActiveModule = null;
    private $adminActivePage = null;
    private $adminLogged = false;
    private $systemPages = [
        "before" => ["log" => "login", /*"pass" => "forgotten-password"*/],
        "after" => ["notFound" => "notFound", "dash" => "dashboard", "logout" => "logout"],
        "modules" => ["maintenance" => "maintenance"]
    ];

    public function __construct(Router $router, AppController $appController) {
        $this->router = $router;
        $this->database = $router->getDatabase();
        $this->modulesController = $appController->returnModulesController();
        $this->localeEngine = $appController->returnLocaleEngine();
        $this->localizationClass = $appController->returnLocalizationClass();
        $this->templateClass = $appController->returnTemplatesClass();

        $this->parsedUrl = $this->router->getParsedUrl();
        $this->langFile = $appController->getLangFile();
    }

    public function build() {
        return $this->buildContent();
    }

    private function buildContent(){
    // PARSE URL - Get URL info
        $this->router->setHttp(200, null);

    // URL | Set url parts for content
        $this->adminActiveModule = ( isset($this->parsedUrl["urlParts"][2]) ) ? $this->parsedUrl["urlParts"][2] : null;
        $this->adminActivePage = ( isset($this->parsedUrl["urlParts"][3]) ) ? $this->parsedUrl["urlParts"][3] : null;

    // ADMIN CHECK | Check status and prevent/allow secured access
        // Check if admin is logged (or expired and do related stuff)
        //$adminEngine = new adminEngine($this->database, $this->localeEngine);
        $this->adminLogged = true;

        // Admin NOT LOGGED - Check page access
        if( $this->adminLogged === false){
            if( $this->adminActiveModule === null OR !in_array($this->adminActiveModule, $this->systemPages["before"]) ){
                // Prohibited access
                $this->router->setHttp(303, ADMIN."/".$this->systemPages["before"]["log"]);
            }else{
                // Allowed access
                $this->router->setHttp(200, null);
            }

            $this->router->initHttp();
            $adminInfo = [];
        }
        // Admin LOGGED - Check page access
        if( $this->adminLogged === true){
            $customDashboard = env('CUSTOM_PROJECT_DASHBOARD');
            $customDashboard = (!empty($customDashboard)) ? $customDashboard : $this->systemPages["after"]["dash"];

            if( $this->adminActiveModule === null OR in_array($this->adminActiveModule, $this->systemPages["before"]) ){
                // Prohibited access | Empty or not system page
                $this->router->setHttp(303, ADMIN."/".$customDashboard);
            }elseif( !in_array($this->adminActiveModule, $this->systemPages["after"]) && !in_array($this->adminActiveModule, $this->modulesController->returnModules("active")) ){
                // Prohibited access | Not active module or not admin page
                $this->router->setHttp(303, ADMIN."/".$customDashboard);
            }else{
                if( $this->adminActiveModule == $this->systemPages["after"]["notFound"] ){
                    // Allowed access | 404 result
                    $this->router->setHttp(404, null);
                }elseif( $this->adminActiveModule == $this->systemPages["after"]["logout"] ){
                    // Allowed access | Logout
                    $this->router->setHttp(200, null);
                }elseif( $this->adminActiveModule != $this->systemPages["after"]["dash"] && ($this->adminActivePage === null OR !file_exists(MODULE_DIR.$this->adminActiveModule."/".$this->adminActivePage.".php") ) ){
                    // Prohibited access | Module page not exists
                    $this->router->setHttp(404, ADMIN."/".$this->systemPages["after"]["notFound"]);
                }else{
                    // Allowed access
                    $this->router->setHttp(200, null);
                }
            }

            // Set HTTP Header or redirect
            $this->router->initHttp();

            // Get logged admin info
            //$loggedAdmin = new moduleAdmins($adminEngine, $this->localizationClass);
            $adminInfo = new stdClass();
            $adminInfo->permanentAccess = 1;

            // PERMISSIONS | Check admin access
            if($adminInfo->permanentAccess != 1){
                $activeUrl = str_replace('/'.ADMIN.'/', '', $this->parsedUrl["urlPath"]);
                $adminPermissions = [];

                if(!in_array($activeUrl, $adminPermissions) && ($this->adminActiveModule != $this->systemPages["after"]["dash"] && $this->adminActiveModule != $this->systemPages["after"]["notFound"] && $this->adminActiveModule != $this->systemPages["after"]["logout"])){
                    // Not allowed access
                    $this->router->setHttp(404, ADMIN."/".$this->systemPages["after"]["notFound"]);
                    $this->router->initHttp();
                }
            }
        }

    // LANGUAGE | Check and set main language
        $mainLanguage = $this->localizationClass->getMainLanguage();
        $mainLanguage = (!empty($mainLanguage)) ? $mainLanguage["langShort"] : DEFAULT_LANG;

    // MODULE | Get active module configuration
        $moduleConfig = $this->modulesController->getModuleConfig($this->adminActiveModule);

    // BREADCRUMBS | Get all breadcrumbs
        $breadCrumbs = new breadCrumbs();
        if($moduleConfig){
            $breadPage = $this->adminActivePage;
            $breadNum = 0;
            do{
                $actualPageId = array_search($breadPage, array_column($moduleConfig["modulePages"], 'urlName'));
                $actualPageInfo = $moduleConfig["modulePages"][$actualPageId];

                $actualBreadTitle = ( $actualPageInfo["parent"] == 0 && $actualPageInfo["headPage"] == 1 ) ? translate($moduleConfig["moduleLangName"]) : translate($actualPageInfo["urlName"]);
                $actualBreadTitle = ( $actualPageInfo["parent"] == 0 && $breadNum == 0 ) ?  translate($actualPageInfo["urlName"]) : $actualBreadTitle;

                $breadData = [ "id" => $actualPageInfo["pageId"], "url" => $this->adminActiveModule.'/'.$actualPageInfo["url"], "title" => $actualBreadTitle ];
                $parentId = array_search($actualPageInfo["parent"], array_column($moduleConfig["modulePages"], 'pageId'));
                $breadPage = $moduleConfig["modulePages"][$parentId]["urlName"];

                $breadCrumbs->add($breadData, $this->adminActiveModule);
                $breadNum++;
            }while($actualPageInfo["parent"] != 0);
        }else{
            $breadData = ["id" => 1, "url" => $this->adminActiveModule, "title" => translate($this->adminActiveModule)];
            $breadCrumbs->add($breadData, $this->adminActiveModule);
        }

    // TEMPLATE | Prepare and load template data
        $templateInfo = [
            "id" => 1,
            "templateName" => $this->adminActiveModule,
            "url" => ( $this->adminActivePage !== null && !empty($moduleConfig) ) ? $this->adminActiveModule."/".$this->adminActivePage : $this->adminActiveModule,
            "title" => ( $this->adminActivePage !== null && !empty($moduleConfig) ) ? translate($this->adminActivePage) : translate($this->adminActiveModule),
            "langShort" => $_SESSION["languages"]["adminLang"]
        ];
        $baseTemplate = new EntityTemplate($templateInfo);
        $templateData = $baseTemplate->get();

    // Reset TEMPLATE anchors to correct admin language
        $this->templateClass->allTemplates = $this->templateClass->getAllTemplateAnchors($_SESSION["languages"]["adminLang"]);

    // CHECK | Check if content is AUTH page
        $authorizationPage = (in_array($this->adminActiveModule, $this->systemPages["before"])) ? true : false;

    // PAGE PATH | Set correct page path for controller load
        $backEndType = "module";
        $pagePath = MODULE_DIR.$this->adminActiveModule.'/'.$this->adminActivePage.'.php';
        if( !$moduleConfig ) {
            $backEndType = "system";
            $pagePath = AUTH_DIR . $templateData["templateName"] . '.php';
        }

    // CONTENT rules | Set all necessary rules and information for controller
        $activeContent = ["module" => $this->adminActiveModule, "page" => $this->adminActivePage, "authorizationPage" => $authorizationPage, "pageType" => $backEndType, "pagePath" => $pagePath, "enableTranslate" => ($_SESSION["languages"]["adminLang"] == $mainLanguage) ? false : true];

    // BUILD DATA | Build object with data and rules for the view render
        $objectData = ["breadCrumbs" => $breadCrumbs->getReverse(), "activeModuleConfig" => (object) $moduleConfig, "activeContent" => (object) $activeContent,"templateData" => (object) $templateData, "loggedAdmin" => $adminInfo];

        return $objectData;
    }
}