<?php

class Router {
    private $database;
    private $appController;

    private $apiEnabled;
    private $projectDisabled;

    private $url;
    private $requestMethod;
    private $urlParams;
    private $urlPath;

    public $routerType;
    private $allowHttpRedirect = true;
    private $httpHeader = null;
    private $httpRedirect = null;

    private $templateParts;
    private $templatePartsCount;

    public function __construct(Database $database, AppController $appController, Bool $ignoreDisabledProjectRule = false) {
        $this->database = $database;
        $this->appController = $appController;

        $this->apiEnabled = useIfExists(env('API_ENABLED'), false);
        $this->projectDisabled = ($ignoreDisabledProjectRule === true) ? false : useIfExists(env('PROJECT_DISABLED'), false);
    }

    public function isApiEnabled() {
        return $this->apiEnabled;
    }
    public function getDatabase() {
        return $this->database;
    }

    public function define(String $requestUrl) {
        $this->parseUrl($requestUrl);
        $parsedUrl = $this->getParsedUrl();
        $this->routerType = "project";

        if($parsedUrl["urlPartsCount"] == 0){
            if($this->projectDisabled === true && !isNotEmpty($_POST)){
                $this->routerType = "app";
            }
            return;
        }else{
            $routeDefiner = mb_strtolower($parsedUrl["urlParts"][1]);
            if($routeDefiner === 'api'){
                $this->routerType = "api";
                return;
            }
            if($routeDefiner === mb_strtolower(ADMIN) || $this->projectDisabled === true){
                $this->routerType = "app";
                return;
            }
        }
    }
    public function start(Bool $allowHttpRedirect = true, Array $options = []){
        $this->allowHttpRedirect = $allowHttpRedirect;
        $localeEngine = $this->appController->returnLocaleEngine();

        switch($this->routerType){
            case "app":
                $this->setLocale($localeEngine->returnAppLang(), $localeEngine->returnLanguageFile('adminLang'));
                $appRouter = new AppRouter($this, $this->appController);
                return $this->load($appRouter->build());

                break;
            case "project":
                if($this->projectDisabled === true && !isExisting('requestName', $_POST)){
                    $this->setHttp(403, null);
                    $this->initHttp();
                    die;
                }

                $this->setLocale($this->appController->getLang(), $localeEngine->returnLanguageFile('appLang'));
                $projectRouter = new ProjectRouter($this, $this->appController);
                $projectRouter->init($options);
                return $this->load($projectRouter->get());
                break;
            case "api":
                if($this->isApiEnabled() === false){
                    $this->setHttp(403, null);
                    $this->initHttp();
                    die;
                }

                $this->setLocale($localeEngine->returnProjectLang(), $localeEngine->returnLanguageFile('appLang'));
                $apiRouter = new ApiRouter($this->database, $this->appController, $this->getParsedUrl());
                $apiRouter->validate();
                $apiRouter->output();
                die;
                break;
        }

        return null;
    }
    private function load(Array $routeData) {
        $metaData = (isset($routeData["metaData"])) ? $routeData["metaData"] : $this->createMetaData($routeData["templateData"]);
        $houseObject = ["metaData" => (object) $metaData, "routerType" => $this->routerType];

        return (object) array_merge($routeData, $houseObject);
    }
    private function setLocale(String $langShort, Array $langFile){
        $this->appController->setLang($langShort);
        $this->appController->setLangFile($langFile);
        $this->appController->initTemplateClass();
    }


// Parse actual page URl
    private function parseUrl($pageUrl) {
        $this->url = $pageUrl;

        $parsedLink = parse_url($this->url);
        $urlParts = array_filter(explode("/", $parsedLink["path"]), function($value) {
            return ($value !== null && $value !== false && $value !== '');
        });

        $templateParts = [];
        foreach ($urlParts as $key => $part) {
            $templateParts[$key] = str_replace(".php", "", $part);
        }

        $this->templateParts = $templateParts;
        $this->templatePartsCount = count($templateParts);
        $this->urlParams = (isset($parsedLink["query"])) ? $parsedLink["query"] : null;
        $this->urlPath = $parsedLink["path"];
        $this->requestMethod = $_SERVER['REQUEST_METHOD'];
    }
    public function getParsedUrl() {
        return ["requestUrl" => $this->url, "requestMethod" => $this->requestMethod, "urlParts" => $this->templateParts, "urlPartsCount" => $this->templatePartsCount, "urlParams" => $this->urlParams, "urlPath" => $this->urlPath];
    }
    public function breakUrlParts($part) {
        $urlParts = explode("-", $part);

        $urlPrefix = is_numeric($urlParts[0]) ? $urlParts[0] : null;
        if ($urlPrefix === null) {
            $url = $part;
        } else {
            unset($urlParts[0]);
            $url = implode("-", $urlParts);
        }

        return array("url" => $url, "urlPrefix" => $urlPrefix);
    }
    private function rebuildUrl($removePart, $url = null, $prefix = null) {
        $parts = $this->templateParts;
        unset($parts[$removePart]);
        $newUrl = "";
        if ($parts) {
            foreach ($parts as $part) {
                $newUrl .= $part . "/";
            }
        }

        $newUrl .= ($prefix !== null) ? $prefix . "-" : "";
        $newUrl .= ($newUrl !== null) ? $url : "";
        $newUrl .= (isset($parsedLink["query"])) ? $parsedLink["query"] : "";

        return $newUrl;
    }

    public function setHttp($httpHeader, $redirectPage) {
        $this->httpHeader = $httpHeader;
        $this->httpRedirect = $redirectPage;
    }
    public function initHttp() {
        if($this->allowHttpRedirect === true){

            $redirectPath = ($this->httpRedirect !== null && strpos($this->httpRedirect, '/') === 0) ? $this->httpRedirect : '/'.$this->httpRedirect;
            switch ($this->httpHeader) {
                case "301":
                    header(SERVER_HEADER." 301 Moved Permanently");
                    header('Location:  '.$redirectPath);
                    die;
                case "302":
                    header(SERVER_HEADER." 302 Found");
                    header('Location:  '.$redirectPath);
                    die;
                case "303":
                    header(SERVER_HEADER." 303 See Other");
                    header('Location:  '.$redirectPath);
                    die;
                case "403":
                    header(SERVER_HEADER." 403 Forbidden");
                    break;
                case "404":
                    header(SERVER_HEADER." 404 Not Found");
                    if ($this->httpRedirect !== null) {
                        header('Location:  /' . $this->httpRedirect);
                        die;
                    }
                    break;
                case "503":
                    header(SERVER_HEADER." 503 Service unavailable");
                    break;
                default:
                    header(SERVER_HEADER." 200 OK");
            }
            return true;
        }

        header(SERVER_HEADER." 200 OK");
        return true;
    }
    public function getHttp() {
        $redirectPath = (strpos($this->httpRedirect, '/') === 0) ? $this->httpRedirect : '/'.$this->httpRedirect;
        return ["code" => $this->httpHeader, "path" => $redirectPath];
    }

// Create META DATA
    private function createMetaData($templateData) {
        $templateData = (object) $templateData;
        $enabledIndex = (env('APP_IN_PROD') === true && ($templateData->allowIndex == true || !isset($templateData->allowIndex))) ? true : false;
        $metaDescription = (isset($templateData->metaDescription) && !empty($templateData->metaDescription)) ? $templateData->metaDescription : ((isset($templateData->shortDesc) && !empty($templateData->shortDesc)) ? mb_substr(strip_tags($templateData->shortDesc), 0, 250) : ((isset($templateData->description) && !empty($templateData->description)) ? mb_substr(strip_tags($templateData->description), 0, 250) : ""));

        $metaData = [
            "title" => (isset($templateData->metaTitle) && !empty($templateData->metaTitle)) ? $templateData->metaTitle : $templateData->title,
            "description" => preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", (html_entity_decode($metaDescription))),
            "keywords" => (isset($templateData->metaKeywords) && !empty($templateData->metaKeywords)) ? $templateData->metaKeywords : null,
            "allowIndex" => [
                "noindex" => !$enabledIndex,
                "nofollow" => !$enabledIndex,
                "meta" => ($enabledIndex === true) ? "index, follow, snippet, archive" : "noindex, nofollow"
            ],
            "pageUrl" => $this->url,
        ];

        if(isset($templateData->images["head"])){
            $mediaController = $this->appController->returnMediaController();
            if($mediaController->getMediaGroup($templateData->images["head"]["fileType"]) == "image" ){
                $mediaProcessor = $this->appController->returnMediaProcessor();
                $metaData["image"] = $mediaProcessor->buildAbsolutePath($mediaProcessor->getImagePath($templateData->images["head"]["basicUrl"], null));
            }
        }

        return $metaData;
    }
}