<?php

class ApiRouter {

    private $database;
    private $appController;
    private $apiCache;

    private $requestInfo;
    private $requestData;
    private $queryParams;
    private $apiUrl = ["moduleName" => null, "action" => null, "value" => null];

    private $responseObject = ["success" => false, "statusCode" => 200, "processName" => null, "processDate" => null, "processDuration" => 0, "processCached" => false, "results" => [], "responseTime" => 0, "message" => null, "errors" => []];
    private $responseCode = 200;
    private $forceStatusCode = null;
    private $executionTime = 0;
    private $validOrigin = null;

    private $allowCache;

    private $httpResultCodes = [
        200 => "Successful request",
        401 => "Unauthorized response",
        403 => "Forbidden",
        500 => "Failed request",
        404 => "Not Found",
    ];
    private $errorCodes = [
        100 => "The module was not found or is not active",
        101 => "The module's API is not provided",
        102 => "The module's API request action is empty",
        103 => "The API requests body is not a valid JSON",
        104 => "The API request origin is not valid",
    ];

    public function __construct(Database $database, AppController $appController, Array $parsedUrl) {
        $this->database = $database;
        $this->appController = $appController;
        $this->apiCache = new ApiCache();
        $this->allowCache = true;

        $this->requestInfo = $parsedUrl;
        $this->queryParams = $this->getQueryParams();
        $this->parseApi();
    }

    private function getQueryParams() {
        parse_str(useIfExists($this->requestInfo["urlParams"], ''), $query);
        return $query;
    }

    private function parseApi() {
        $this->apiUrl["moduleName"] = $this->requestInfo["urlParts"][2];
        $this->apiUrl["action"] = $this->requestInfo["urlParts"][3];
        $this->apiUrl["value"] = (isset($this->requestInfo["urlParts"][4])) ? $this->requestInfo["urlParts"][4] : null;
    }

    private function buildRequest() {
        $this->requestData["requestUrl"] = $this->requestInfo["requestUrl"];
        $this->requestData["method"] = $this->requestInfo["requestMethod"];
        $this->requestData["locale"] = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 5);

        $this->requestData["action"] = $this->apiUrl["action"];
        $this->requestData["value"] = $this->apiUrl["value"];
        $this->requestData["params"] = $this->queryParams;

        $this->requestData["body"] = json_decode(file_get_contents('php://input'), TRUE);
    }

    private function checkOrigin() {
        $allowedOrigins = explode(',', useIfExists(env('API_ORIGIN'), []));

        if (array_key_exists('HTTP_ORIGIN', $_SERVER)) {
            $origin = $_SERVER['HTTP_ORIGIN'];
        } else if (array_key_exists('HTTP_REFERER', $_SERVER)) {
            $origin = $_SERVER['HTTP_REFERER'];
        } else {
            $origin = $_SERVER['REMOTE_ADDR'];
        }

        if (in_array($origin, $allowedOrigins)) {
            $this->validOrigin = $origin;
            $this->requestData["origin"] = $origin;
            return true;
        }

        return false;
    }

    private function extendErrorCodes(Array $errorCodes) {
        $this->errorCodes = $this->errorCodes += $errorCodes;
    }

    public function validate() {
        $this->executionTime = microtime(true);
        $isAuth = false;

        if ($this->checkOrigin() === false) {
            $this->setError(403, 104);
            return false;
        }

        $this->buildRequest();

        if ($this->requestData["method"] === 'POST' && $this->requestData["body"] === null) {
            $this->setError(500, 103);
            return false;
        }

        if ($this->apiUrl["moduleName"] !== "auth") {
            if (!$this->appController->isActiveModule($this->apiUrl["moduleName"])) {
                $this->setError(500, 100);
                return false;
            }

            $moduleApiCrossroad = MODULE_DIR . $this->apiUrl["moduleName"] . '/api/ApiFactory.php';
            $apiFactory = "Api\\{$this->apiUrl['moduleName']}\\ApiFactory";
            if (!file_exists($moduleApiCrossroad)) {
                $this->setError(500, 101);
                return false;
            }
        } else {
            $moduleApiCrossroad = API_DIR . '/Auth/ApiFactory.php';
            $apiFactory = "Api\Auth\ApiFactory";
            $isAuth = true;
        }

        if (empty($this->apiUrl["action"])) {
            $this->setError(500, 102);
            return false;
        }

        $this->runApiFactory($moduleApiCrossroad, $apiFactory, $isAuth);
        return true;
    }

    public function output($returnBefore = false) {
        if ($returnBefore === true) {
            return $this->responseObject;
        }

        header("Access-Control-Allow-Origin: " . $this->validOrigin);
        header("Access-Control-Allow-Credentials: true");
        header("Access-Control-Allow-Headers: Authorization, Accept-Language, Content-Type, Access-Control-Allow-Headers, X-Requested-With, sentry-trace, baggage");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        header("Access-Control-Max-Age: 3600");
        header("Content-Type: application/json; charset=UTF-8");

        if (isset($this->requestData["method"]) && $this->requestData["method"] == 'OPTIONS') {
            header(SERVER_HEADER . ' 204 No Content');
        } else {
            $authError = ($this->responseObject["statusCode"] >= 312 && $this->responseObject["statusCode"] <= 315);
            if ($authError === true) {
                header(SERVER_HEADER . ' 401 ' . $this->httpResultCodes[401]);
            } else {
                if($this->forceStatusCode !== null){
                    $this->responseCode = $this->forceStatusCode;
                }
                header(SERVER_HEADER . ' ' . $this->responseCode . ' ' . $this->httpResultCodes[$this->responseCode]);
            }


            $this->responseObject["responseTime"] = round((microtime(true) - INIT), 3);

            $totalTime = round(((microtime(true) - INIT) * 1000), 2);
            Autoloader::trackPerformance('apiData', ((float)$totalTime - (float)Autoloader::getPerformance('warmUp')) . 'ms');
            Autoloader::trackPerformance('responseTime', $totalTime . 'ms');
            $apiPerformance = Autoloader::getPerformance();
            if (!empty($apiPerformance)) {
                $this->responseObject["performance"] = $apiPerformance;
            }

            echo json_encode($this->responseObject);
        }

        exit;
    }

    private function runApiFactory(String $moduleApiCrossroad, String $apiFactory, Bool $isAuth) {
        include_once($moduleApiCrossroad);
        $apiFactory = new $apiFactory($this->database, $this->appController, $this->requestData);
        $isValid = $apiFactory->validate();
        if ($isValid !== false) {
            if ($this->allowCache === true && $isAuth === false) {
                if ($apiFactory->isCacheAllowed() === true) {
                    $responseCache = $this->getCache($this->apiUrl["moduleName"], $this->requestData);
                    if ($responseCache !== null) {
                        $this->responseObject = $responseCache;
                        return;
                    }
                }
            }

            $apiFactory->proceedRequest();
        }

        $this->forceStatusCode = $apiFactory->getResponseForceCode();
        $this->extendErrorCodes($apiFactory->getErrorCodes());
        $errorMessage = $apiFactory->getResponseError();
        if ($errorMessage === null) {
            $errorMessage = $this->errorCodes[$apiFactory->getResponseCode()];
        }

        $this->setOutput($this->apiUrl["moduleName"], $apiFactory->getResponseCode(), $apiFactory->getResponseData(), $errorMessage);

        if ($this->allowCache === true && $isAuth === false) {
            if ($apiFactory->isCacheAllowed() === true) {
                if ($this->requestData && $this->responseObject) {
                    $this->setCache($this->apiUrl["moduleName"], $this->requestData, $this->responseObject);
                }
            }
        }
    }

    public function setError(Int $statusCode, Int $errorCode, String $errorMessage = null) {
        $this->responseCode = $statusCode;
        $this->setOutput($this->apiUrl["moduleName"], $errorCode, [], (($errorMessage !== null) ? $errorMessage : $this->errorCodes[$errorCode]));
    }

    private function setOutput(String $processName, Int $responseCode, Array $responseData, ?String $responseMessage = null) {
        $date = new \DateTime();
        $isSuccessCode = (substr($responseCode, 0, 1) == "2");

        $this->responseObject["success"] = ($responseMessage !== null && !$isSuccessCode) ? false : true;
        $this->responseObject["statusCode"] = $responseCode;

        $this->responseObject["processName"] = $processName;
        $this->responseObject["processDate"] = $date->format('Y-m-d H:i:s');
        $this->responseObject["processDuration"] = round((microtime(true) - $this->executionTime), 3);

        $this->responseObject["results"] = $this->preventOutput($responseData);
        $this->responseObject["message"] = $responseMessage;
        $this->responseObject["errors"] = (!empty($this->database->getErrors())) ? $this->database->getErrors() : [];
    }
    private function preventOutput(Array $responseData = []){
        if(!empty($responseData)){
            self::preventImagePath($responseData);
        }

        return $responseData;
    }
    private static function preventImagePath(&$array) {
        foreach ($array as $key => &$value) {
            if (is_array($value)) {
                self::preventImagePath($value);
            } elseif ($key === 'basicUrl') {
                if (strpos($value, 'http') !== 0) {
                    $array[$key] = SERVER_PROTOCOL."://".SERVER_NAME."/".STORAGE."/".$value;
                }
            }
        }
    }

    public function getCache(String $moduleName, Array $requestData) {
        $cacheData = $this->apiCache->get($moduleName, $requestData);
        if (isset($cacheData["processDuration"])) {
            $cacheData["processDuration"] = round((microtime(true) - $this->executionTime), 3);
        }

        return $cacheData;
    }
    public function setCache(String $moduleName, Array $requestData, Array $responseData) {
        $responseData["processCached"] = true;

        $this->apiCache->set($moduleName, $requestData, $responseData);
    }
}