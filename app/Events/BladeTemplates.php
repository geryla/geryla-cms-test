<?php
    $blade = new \Jenssegers\Blade\Blade([VIEWS_DIR], FILE_CACHE.'ProjectView');

/* BLADE methods */
    function render(String $view, Array $args = [], Bool $decode = true) {
        global $blade;
        $args = ($decode === true) ? json_decode(json_encode($args), true) : $args;
        echo $blade->render($view, $args);
        \Tracy\Debugger::barDump($args);
    }
    function renderReturn(String $view, Array $args = [], Bool $decode = true) {
        ob_start();
        render($view, $args, $decode);
        return ob_get_clean();
    }

/* CUSTOM SHORTCUTS functions */
    function output($variable, $simplyReturn = false){
        if($simplyReturn === true){
            return $variable;
        }else{
            echo $variable;
        }
    }
    function getTranslate($expression, $args = []){
        return translate($expression, $args);
    }
    function getAssetsPath($fileName, $fileType = null, $cached = false){
        if($fileType !== null){
            $fileName = $fileType.'/'.$fileName;
        }

        $assetRoute = "/assets/{$fileName}";
        $assetRoute .= ($cached === true) ? '?v='.getSettings('buildNum') : null;

        return $assetRoute;
    }
    function buildRoute($template, $parameters = []){
        global $presenter;

        $route = '/'.$presenter->templatesAnchors[$template];
        if(!empty($parameters)){
            $prefix = null;
            foreach($parameters as $param){
                $prefix = ($prefix === null && is_numeric($param)) ? true : $prefix;

                $route .= ($prefix === false) ? null : '/';
                $route .= $param;
                $route .= ($prefix === true) ? '-' : null;

                $prefix = ($prefix === true) ? false : null;
            }
            if($prefix === false){
                $route = rtrim($route, '-');
            }
        }

        return $route;
    }
    function returnMeta($metaTag){
        global $presenter;
        switch ($metaTag){
            case "title":
                return $presenter->metaData->title;
                break;
            case "description":
                return $presenter->metaData->description;
                break;
            case "keywords":
                return $presenter->metaData->keywords;
                break;
            case "robots":
                return $presenter->metaData->allowIndex["meta"];
                break;
            case "url":
                return $presenter->metaData->pageUrl;
                break;
            case "image":
                return SERVER_PROTOCOL."://".SERVER_NAME.$presenter->metaData->image;
                break;
            case "locale":
                return $presenter->templateData->langShort;
                break;
            default:
                return null;
        }
    }
    function getAllModulesHooks($hookName){
        global $database;
        global $appController;

        global $modulesController;
        global $localizationClass;

        global $lang;
        global $systemSettings;
        global $presenter;
        global $controller;

        ob_start();
        foreach($modulesController->getConfig('hooks') as $moduleViews){
            if(array_key_exists($hookName, $moduleViews)){
                include($moduleViews[$hookName]);
            }
        }

        return ob_get_clean();
    }
    function getSettings($settingsKey){
        global $systemSettings;
        return $systemSettings[$settingsKey];
    }
    function processCache($process, $fileName = "*"){
        if($process === "clear"){
            $files = glob(FILE_CACHE.'ProjectView/'.$fileName);
            if($files){
                foreach($files as $file){
                    if(is_file($file)) unlink($file);
                }
            }
        }
    }
    function getResizeImage($url, $size = "", $webp = false){
        global $mediaProcessor;

        if($url !== null){
            $url = $mediaProcessor->getImagePath($url, $size);
            if($webp === true){
                $url = $mediaProcessor->checkWebPSupport($url);
            }
        }

        return $url;
    }
    function getObjectImage($id, $template, $size = ""){
        global $mediaProcessor;
        global $presenter;

        return $mediaProcessor->getMainImage($id, $template, $presenter->templateData->langShort, $size)->url;
    }
    function buildPrice($priceValue, $langShort = null){
        global $currencyBuilder;
        $activeLangShort = ($langShort === null) ? $_SESSION["languages"]["appLang"] : $langShort;

        return $currencyBuilder->buildPriceString($activeLangShort, (float)$priceValue);
    }

/* CUSTOM BLADE directives */
    $blade->directive('lang', function ($expression) {
        return "<?php echo getTranslate({$expression}); ?>";
    });
    $blade->directive('assets', function ($expression) {
        return "<?php echo getAssetsPath({$expression}); ?>";
    });
    $blade->directive('route', function ($expression) {
        return "<?php echo buildRoute({$expression}); ?>";
    });
    $blade->directive('meta', function ($expression) {
        return "<?php echo returnMeta({$expression}); ?>";
    });
    $blade->directive('hook', function ($expression) {
        return "<?php echo getAllModulesHooks({$expression}); ?>";
    });
    $blade->directive('setting', function ($expression) {
        return "<?php echo getSettings({$expression}); ?>";
    });
    $blade->directive('cache', function ($expression) {
        return "<?php echo processCache({$expression}); ?>";
    });
    $blade->directive('getThumbnail', function ($expression) {
        return "<?php echo getResizeImage({$expression}); ?>";
    });
    $blade->directive('getImage', function ($expression) {
        return "<?php echo getObjectImage({$expression}); ?>";
    });
    $blade->directive('truncate', function ($expression) {
        return "<?php echo truncate({$expression}); ?>";
    });
    $blade->directive('priceString', function ($expression) {
        return "<?php echo buildPrice({$expression}); ?>";
    });