<?php
    if($router->getParsedUrl()['urlPartsCount'] === 0 || !in_array('watchdog', $router->getParsedUrl()['urlParts'])){
        return;
    }

    $jobsStartedTime = microtime(true);
    $scheduleToken = (isset($_GET["token"])) ? (string) $_GET["token"] : null;
    $isAuthorized = ($scheduleToken !== null && $scheduleToken === $systemSettings["scheduler-token"]) ? true : false;

    $jobsManager = new ScheduleController($database, $appController);
    $jobsCounter = $loopTime = 0;
    $jobsStatistics = ['count' => 0, 'finished' => 0, 'failed' => 0];

    if($isAuthorized === true){
        $maxExecutionTime = (ini_get('max_execution_time') * 0.85);
        $maxJobTime = $maxExecutionTime - (microtime(true) - $jobsStartedTime);
        $stopLoop = false;
        $dependentJobs = [];

        do{
            $pendingJobs = $jobsManager->getJobs('pending', 5, date('Y-m-d H:i:s'), $dependentJobs);
            if(!empty($pendingJobs)){
                foreach($pendingJobs as $jobInfo){
                    $jobStartTime = microtime(true);

                    $isFinished = $jobsManager->runJob($jobInfo["id"], $jobInfo);
                    if($isFinished === null){
                        $dependentJobs[] = $jobInfo["id"];
                    }else{
                        $jobsStatistics["count"]++;
                        $jobsStatistics[($isFinished ? 'finished' : 'failed')]++;
                    }

                    $jobsCounter++;
                    $loopTime += round(microtime(true) - $jobStartTime, 2);
                    $loopTimeAvg = $loopTime/$jobsCounter;
                    if($loopTime + ($loopTimeAvg * 2) >= $maxJobTime){
                        $stopLoop = true;
                        break;
                    }
                }
            }
        }while(!empty($pendingJobs) && $stopLoop === false);

        if($jobsStatistics["count"] > 0){
            if($jobsStatistics["count"] == $jobsStatistics["finished"]){
                $jobsManager->setStatus(200);
            }else{
                if($jobsStatistics["finished"] > 0){
                    $jobsManager->setStatus(301);
                }else{
                    $jobsManager->setStatus(302);
                }
            }
        }

        if($stopLoop === false){
            $jobsManager->checkAndClearJobs();
        }
    }

// EXECUTION info
    $executionTime = round((microtime(true) - $jobsStartedTime)*1000, 4);
    $executionStatus = $jobsManager->getStatus();

    echo "<strong>Watchdog response:</strong> {$executionStatus['code']} - {$executionStatus['message']} <br />";
    echo "<strong>Total execution time: </strong>{$executionTime}ms<br/>";

    echo "<strong>Finished jobs</strong>: {$jobsStatistics['finished']}/{$jobsStatistics['count']} <br/>";
    if($jobsStatistics['failed'] > 0){
        echo "<br/><strong>Failed jobs</strong>: {$jobsStatistics['failed']}<br/>";
    }

die;
