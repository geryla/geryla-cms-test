<?php
// CONNECT Database and init required app engines
    $database = new Database();
    $appController = new AppController($database);

    $localeEngine = $appController->returnLocaleEngine();
    $localizationClass = $appController->returnLocalizationClass();
    $modulesController = $appController->returnModulesController();
    $settingsController = $appController->returnSettingsController();
    $systemLogger = $appController->returnLogger();

    $currencyBuilder = $appController->returnCurrencyBuilder();
    $mediaController = $appController->returnMediaController();
    $mediaProcessor = $appController->returnMediaProcessor();

    $systemSettings = $appController->getSettings();

    $database->queryCounter("Init");
    Autoloader::trackPerformance('warmUp', (round(((microtime(true) - INIT)*1000), 2)).'ms');
    \Tracy\Debugger::getBar()
    ->addPanel(new \Debugger\SqlCache($database, $appController))
    ->addPanel(new \Debugger\SqlLogs($database, $appController));

// === Routing
    $router = new Router($database, $appController);
    $router->define($_SERVER["REQUEST_URI"]);

    require_once(EVENTS_DIR.'ModuleEvents.php');

    $presenter = $router->start(true);
    $templateClass = $appController->returnTemplatesClass();
    $database->queryCounter("Routing");

// === Lang file
    $lang = $appController->getLangFile();