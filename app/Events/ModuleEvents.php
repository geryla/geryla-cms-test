<?php
//WATCHDOG: System CRON event | Render stop on success
    require_once(EVENTS_DIR.'WatchDog.php');
//SCHEDULER: System CRON event | Render stop on success
    require_once(EVENTS_DIR.'Scheduler.php');

//WEBHOOKS: Module callback functions | Render stop on success
    require_once(EVENTS_DIR.'WebHooks.php');

//MODULE: Invoices - Documents view render from "secured" URL
    if($appController->isActiveModule("invoices") && $appController->isActiveModule("orders")){
        include(MODULE_DIR."invoices/Events/documentRender.php");
    }