<?php
    if($router->getParsedUrl()['urlPartsCount'] === 0 || !in_array('webhook', $router->getParsedUrl()['urlParts'])){
        return;
    }

//MODULE WEBHOOK: Orders - Payment gateway
    if($appController->isActiveModule("orders")){
        include(MODULE_DIR."orders/Events/updateOrderStatus.php");
    }
//MODULE WEBHOOK: Booking system - OTA's
    if($appController->isActiveModule("bookingSystem")){
        include(MODULE_DIR."bookingSystem/Events/webhook.php");
    }

    echo 'Webhook finished';
    die;