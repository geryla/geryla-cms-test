<?php
/* === APP CONTROLLER
 * ONLY global settings for system!!
 * Editable settings and methods for app edit are in resources/controllers, changes in this file can be overwritten by system update!
 * */

// DEFINE $Controller Array
$controller = [];

// Users -> Catch submitted user forms (login, register, etc.) or user actions (logout, activate account, etc.) and calls methods
    if($appController->isActiveModule("users")){
        $usersController = new UsersController($database, $appController);
        $userPermissions = $usersController->initUser($presenter->templateData);
        include(MODULE_DIR . "users/view/userActions.php");

        $presenter->userPermissions = $userPermissions["userPermissions"];
        $presenter->userInfo = $userPermissions["userInfo"];
    }
// Loyalty program -> check and keep updated users loyalty information
    if($appController->isActiveModule("loyaltyProgram")){
        include(MODULE_DIR . "loyaltyProgram/view/loyaltyProgram.php");

        (isset($presenter->loyaltyCartPoints)) ? $controller["loyaltyCartPoints"] = $presenter->loyaltyCartPoints : null;
    }
// Shopping cart & Discounts -> redirects, rules and counting
    if($appController->isActiveModule("shoppingCart")){
        $cartController = new CartController($database, $appController);
        $cartController->runProcess("calculation");
        $cartController->runProcess("deliveryFree");
        include(MODULE_DIR."shoppingCart/view/cartActions.php");

        if($appController->isActiveModule("discounts")){
            $cartController->runProcess("discountCheck");
        }

        include(MODULE_DIR . "shoppingCart/view/cartReservations.php");
    }
// Reservations -> create reservation
    if($appController->isActiveModule("reservations")){
        include(MODULE_DIR . "reservations/view/createReservation.php");
    }
// MRP accounting -> export orders/invoices
    if($appController->isActiveModule("mrpAccounting")){
        include(MODULE_DIR . "mrpAccounting/view/mrpActions.php");
    }

// Email controller - LAST MODULE extend for POST method! -> unset($_POST) included
// Is DEFAULT module = always active ... For success email send must form contains KEY (email template) and reCaptcha toke if module is enabled
    if($appController->isActiveModule("emailTemplates")){
        include(EMAIL_RUNNER);
    }

// Define default classes
    $cmsHook = new cmsHook($database, $appController);
    $contactBoxes = new contactBoxes($database, $appController);

// Controller data
    $controller["templateName"] = $presenter->templateData->templateName;
    $controller["topMenu"] = $cmsHook->getHookContentTree(1,0,0, $presenter->templateData, $router->getParsedUrl()["urlPath"]);

    $controller["appContact"] =  $contactBoxes->getAppContact($presenter->templateData->langShort);
    $controller["appContact"]["images"] = $mediaProcessor->getAllObjectImages((int)$controller["appContact"]["id"], "contactBox", $appController->getLang(), null);

    $appLogo = $mediaProcessor->getMainImage(1, "logo", $presenter->templateData->langShort, null);
    $controller["appLogo"] = ["url" => ($appLogo->url) ?? null, "alt" => ($appLogo->alt) ?? null];

    $controller["templatesAnchors"] = $presenter->templatesAnchors;