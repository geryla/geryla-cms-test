<?php
// POST (Requests) Requests Manager
    if(isExisting('requestName', $_POST)){
        $requestsController = new RequestsController($database, $appController);

        $requireAdminVerification = (isset($_POST["noVerify"]) && boolval($_POST["noVerify"]) === true) ? false : true;
        $isValidRequest = $requestsController->validate($_POST, $_FILES, $requireAdminVerification);

        if($isValidRequest === true){
            $request = $requestsController->getRequestInfo();
            $response = include($requestsController->getRequestPath());

            if($requestsController->getResponseType() === 'json'){
                echo json_encode($response);
            }else if($requestsController->getResponseType() === 'dom'){
                ob_start();
                $response;
                echo ob_get_clean();
            }
        }else{
            echo $requestsController->getError()["message"];
        }

        die;
    }