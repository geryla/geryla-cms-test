<?php
    if($router->getParsedUrl()['urlPartsCount'] === 0 || !in_array('scheduler', $router->getParsedUrl()['urlParts'])){
        return;
    }

    $schedulerMaster = new ScheduleController($database, $appController);
    $scheduleTimer = ['start' => microtime(true), 'date' => date('Y-m-d H:i:s')];
    $scheduleResponse = ['schedules' => ['count' => 0, 'succeeded' => 0, 'failed' => 0, 'failedList' => []]];

    $scheduleToken = (isset($_GET["token"])) ? (string) $_GET["token"] : null;
    $isAuthorized = ($scheduleToken !== null && $scheduleToken === $systemSettings["scheduler-token"]) ? true : false;

// CRON SCHEDULER JOBS
    if ($isAuthorized === true) {
        $cronList = $schedulerMaster->getAllTasks(true);
        if(!empty($cronList)){
            $actualDate = ['day' => strtolower(date('l')), 'hours' => date('H')];

            foreach ($cronList as $cron) {
            // PRE-CHECK - Reset jobRuns counter if last run was in previous days
                $date = new DateTime($cron['lastRun']);
                $now = new DateTime();
                $dayDiff = $now->diff($date);
                if($dayDiff->days > 0){
                    $cron['jobRuns'] = $schedulerMaster->resetTaskRun($cron["id"]);
                }

            // CHECK - If job is enabled in actual day and hour
                if($cron[$actualDate["day"]] == 1 && ($cron["start"] <= $actualDate["hours"] && $actualDate["hours"] <= $cron["end"])){
                    // CHECK - If cron hasn't reached the daily limit
                    if($cron['dailyFrequency'] === 0 || ($cron['dailyFrequency'] > 0 && $cron['jobRuns'] < $cron["dailyFrequency"])){
                        $cronPath = MODULE_DIR.$cron["moduleName"]."/cron/".$cron["cronJob"];
                        $scheduleResponse["schedules"]["count"]++;
                        $jobWork = false;
                        $cronExecutionTime = microtime(true);

                        if(file_exists($cronPath)){
                            $jobWork = include_once($cronPath);
                        }

                        if($jobWork === true){
                            $schedulerMaster->confirmTaskRun($cron["id"], $cron["jobRuns"]);
                            $scheduleResponse["schedules"]["succeeded"]++;
                        }else{
                            $scheduleResponse["schedules"]["failed"]++;
                            $scheduleResponse["schedules"]["failedList"][] = ["info" => $cron, "executionTime" => round((microtime(true) - $cronExecutionTime)*1000, 4), "date" => date('Y-m-d H:i:s')];
                        }
                    }
                }

                $logInfo = ["status" => ((isset($jobWork) && $jobWork === true) ? "true" : "skipped")];
                $systemLogger->createLog($_SESSION["languages"]["appLang"], "cron", $cron["cronJob"], 0, 0, json_encode($logInfo));
            }

            if($scheduleResponse["schedules"]["count"] > 0){
                if($scheduleResponse["schedules"]["count"] == $scheduleResponse["schedules"]["succeeded"]){
                    $schedulerMaster->setStatus(200);
                }else{
                    if($scheduleResponse["schedules"]["succeeded"] > 0){
                        $schedulerMaster->setStatus(301);
                    }else{
                        $schedulerMaster->setStatus(302);
                    }
                }
            }
        }
    }else{
        $schedulerMaster->setStatus(400);
    }

// EXECUTION info
    $executionTime = round((microtime(true) - $scheduleTimer['start'])*1000, 4);
    $executionStatus = $schedulerMaster->getStatus();

    echo "<strong>Scheduler response:</strong> {$executionStatus['code']} - {$executionStatus['message']} <br />";
    echo "<strong>Total execution time: </strong>{$executionTime}ms<br/>";

    echo "<strong>Schedules</strong>: {$scheduleResponse['schedules']['succeeded']}/{$scheduleResponse['schedules']['count']} <br/>";
    if($scheduleResponse['schedules']['failed'] > 0){
        echo "<br/><strong>Failed</strong>: <br/>";
        foreach($scheduleResponse['schedules']['failedList'] as $failedCron){
            echo "- <strong>{$failedCron['info']['cronJob']}</strong> ({$failedCron['info']['moduleName']}) | {$failedCron['executionTime']}ms | {$failedCron['date']} <br />";
        }
    }

die;