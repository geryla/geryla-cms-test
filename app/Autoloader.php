<?php
use Tracy\Debugger;
use Dotenv\Dotenv;

class Autoloader {
    private static $dotEnv;
    private static $performanceInfo = [];

    private static $envRules = [
        'APP_KEY' => ['notEmpty' => true],
        'DB_HOST' => ['notEmpty' => true],
        'DB_DATABASE' => ['notEmpty' => true],
        'DB_USERNAME' => ['notEmpty' => true],
        'DB_PASSWORD' => [],
    ];
    private static $CoreDefines = [
        "Directories" => [
            'App' => __DIR__,
            'Project' => ROOT.'/project',
        ],
        "Folders" => [
            'Admin' => 'jmAdmin',
            'Lang' => 'lang',
            'Requests' => 'requests',
            'Storage' => 'storage',
            'Migrations' => 'migrations',
        ],
        "Classes" => [
            'Http' => __DIR__.'/Http',
            'Services' => __DIR__.'/Services',
            'Database' => __DIR__.'/Services/Database',
            'Cache' => __DIR__.'/Services/Cache',
            'Controllers' => __DIR__.'/Services/Controllers',
            'Api' => __DIR__.'/Services/Api',
            'Models' => __DIR__.'/Services/Models',
            'Debugger' => __DIR__.'/Services/Debugger',
        ],
        "Includes" => [
            'Helpers' => __DIR__.'/Helpers',
        ]
    ];

    public function __construct() {
        self::load();
    }
    private static function load(){
    // Register Composer packages
        self::register(ROOT.'/vendor/autoload.php');
    // Extend PHP functions - App functions
        self::includeFiles();
    // Init Debugger and ENV package
        self::createInstance();
    // Define server config (limits)
        self::defineServer();
    // Define App core
        self::defineConstants("default_lang", "cs_CZ", false);
        self::defineConstants("develop_name", "ReSs Design", false);
    // Define system routes (Directories and Paths)
        self::defineRoutes();
    // Register System classes
        self::systemAutoload();
    // Define App paths
        self::definePaths();
    // Get Database tables
        self::getDatabaseTables();
    // Dump posts via session
        self::dumpConst();
    }

    protected static function register($filePath){
        if(file_exists($filePath) ){
            if(is_file($filePath)){
                require_once ($filePath);
            }
        }
    }
    protected static function defineConstants(String $alias, String $value, Bool $slash = true, String $prefix = null, String $subfix = null) {
        define($prefix.strtoupper($alias).$subfix, "{$value}".(($slash === true) ? '/' : null));
    }

    private static function defineTypes(String $type, Bool $slash = true, String $prefix = null, String $subfix = null) {
        foreach(self::$CoreDefines[$type] as $alias => $path){
            self::defineConstants($alias, $path, $slash, $prefix, $subfix);
        }
    }
    private static function loadClasses(String $className) {
        foreach(self::$CoreDefines["Classes"] as $alias => $path){
            self::register(str_replace("\\", "/", "{$path}/{$className}.php"));
        }
    }

    private static function includeFiles() {
        foreach(self::$CoreDefines["Includes"] as $alias => $path){
            $pathInfo = pathinfo($path);

            if(isset($pathInfo["extension"])){
                self::register($path);
            }else{
                $folder = scandir($path);
                if(!empty($folder)){
                    foreach($folder as $file){
                        self::register("{$path}/{$file}");
                    }
                }
            }
        }
    }
    private static function createInstance(){
        self::$dotEnv = Dotenv::createImmutable(ROOT);
        self::$dotEnv->load();

        $debugEnabled = useIfExists(env('APP_DEBUG'), false);
        $debuggerMode = !$debugEnabled;
        $whiteList = env('APP_DEBUG_WHITELIST');
        if($whiteList !== null && $whiteList !== ''){
            $whiteList = explode(',', trim($whiteList));
            $debuggerMode = $whiteList;
        }

        Debugger::enable($debuggerMode, ROOT.'/project/logs');
        Debugger::$maxDepth = useIfExists(env('DEBUG_DEPTH'), 15);
        Debugger::$maxLength = useIfExists(env('DEBUG_LENGTH'), 150);
        ($debugEnabled === true) ? error_reporting(E_ALL & ~E_NOTICE) : error_reporting(0);
        if(!empty(env('SENTRY_DSN'))){
            \Sentry\init(['dsn' => env('SENTRY_DSN'), 'traces_sample_rate' => 1.0, 'profiles_sample_rate' => 1.0, 'error_types' => E_ALL & ~E_NOTICE & ~E_WARNING]);
        }

        foreach(self::$envRules as $envName => $envRules){
            self::$dotEnv->required($envName);
            if(!empty($envRules)){
                if(in_array("notEmpty", $envRules)){
                    self::$dotEnv->required($envName)->notEmpty();
                }
            }
        }
    }
    private static function defineServer(){
        self::defineConstants("name", $_SERVER["SERVER_NAME"], false, "SERVER_");
        self::defineConstants("protocol", ((isset($_SERVER["HTTPS"]) && strtolower($_SERVER["HTTPS"]) == "on") ? "https" : "http"), false, "SERVER_");
        self::defineConstants("header", useIfExists(env('HTTP_PROTOCOL'), ((isset($_SERVER['SERVER_PROTOCOL'])) ? $_SERVER['SERVER_PROTOCOL'] : "HTTP/1.1")), false, "SERVER_");

        self::defineConstants("maxFileSize", ini_get('upload_max_filesize'), false, "MEDIA_");
        self::defineConstants("maxPostSize", ini_get('post_max_size'), false, "MEDIA_");
        self::defineConstants("MaxFilesLimit", ini_get('max_file_uploads'), false, "MEDIA_");
    }
    private static function defineRoutes(){
        self::defineTypes("Folders", false);
        self::defineTypes("Directories", true, null, "_DIR");
    }
    private static function systemAutoload(){
        spl_autoload_register([__CLASS__, 'loadClasses']);
    }
    private static function definePaths(){
        self::defineConstants("services", self::$CoreDefines["Classes"]["Services"], true, null, "_DIR");
        self::defineConstants("migrations", self::$CoreDefines["Classes"]["Database"].'/'.MIGRATIONS, true, null, "_DIR");
        self::defineConstants("events", APP_DIR."Events", true, null, "_DIR");//TODO: Move somewhere out of Autoloader
        self::defineConstants("admin", APP_DIR."Panel", true, null, "_DIR");
        self::defineConstants("module", APP_DIR."Modules", true, null, "_DIR");
        self::defineConstants("kernel", APP_DIR."Kernels", true, null, "_DIR");
        self::defineConstants("public", PROJECT_DIR."public", true, null, "_DIR");
        self::defineConstants("resources", PROJECT_DIR."resources", true, null, "_DIR");
        self::defineConstants("controllers", RESOURCES_DIR."controllers", true, null, "_DIR");
        self::defineConstants("api", self::$CoreDefines["Classes"]["Api"], true, null, "_DIR");
        self::defineConstants("views", RESOURCES_DIR."views", true, null, "_DIR");
        self::defineConstants("pages", VIEWS_DIR."pages", true, null, "_DIR");
        self::defineConstants("cache", SERVICES_DIR."Cache", true, null, "_DIR");
        self::defineConstants("file", PROJECT_DIR."cache", true, null, "_CACHE");
        self::defineConstants("assets", PUBLIC_DIR."assets", true, null, "_DIR");
        self::defineConstants("storage", ROOT.'/'.STORAGE, true, null, "_DIR");
        self::defineConstants("layout", ADMIN_DIR."layout", true, null, "_DIR");
        self::defineConstants("auth", LAYOUT_DIR."auth", true, null, "_DIR");
        self::defineConstants("config", PROJECT_DIR."config", true, null, "_DIR");
        self::defineConstants("protected", PROJECT_DIR."protected", true, null, "_DIR");
        //TODO: Email templates directories - check if still exists
        self::defineConstants("dirname", "value", false, "EMAIL_");
        self::defineConstants("runner", MODULE_DIR."emailTemplates/view/runner.php", false, "EMAIL_");
    }
    private static function dumpConst(){
        $_SESSION['DIR_ROOT'] = ROOT;
        if(useIfExists(env('APP_DEBUG'), false) === true){
            bdump(get_defined_constants(true)["user"]);
        }
    }

    public static function trackPerformance(String $name, $value){
        if(useIfExists(env('APP_DEBUG'), false) === true){
            self::$performanceInfo[$name] = $value;
        }
    }
    public static function getPerformance(?String $name = null){
        if(useIfExists(env('APP_DEBUG'), false) === true){
            if($name === null){
                return self::$performanceInfo;
            }
            return useIfExists(self::$performanceInfo, [], $name);
        }

        return [];
    }

    private static function getDatabaseTables(){
        $storedTables = FILE_CACHE."sqlTables.txt";
        if(!file_exists($storedTables)){
            $existingModules = scandir(MODULE_DIR);
            $databaseTables = [];
            foreach ($existingModules as $module) {
                if(file_exists(MODULE_DIR."{$module}/config.php")){
                    $moduleConfig = Autoloader::getConfig(MODULE_DIR."{$module}/config.php");
                    if(isset($moduleConfig["databaseTables"])){
                        $databaseTables[$module] = $moduleConfig["databaseTables"];
                    }
                }
            }

            file_put_contents($storedTables, json_encode($databaseTables));
        }

        self::defineConstants("database", $storedTables, false, null, "_TABLES");
    }
    public static function getConfig(String $path) {
        include($path);
        return $config;
    }
}