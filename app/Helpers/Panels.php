<?php
class Panels{
    private static $activeParameters = [];
    private static $panelSortableDates = ["today", "yesterday", "week", "month", "range"];

    public static function getParameters(){
        return self::$activeParameters;
    }
    public static function setParameters(Array $parametersArray = null){
        self::$activeParameters = $parametersArray;
    }
    public static function returnParameter(String $parameterName){
        return (isset($parameterName, self::$activeParameters)) ? self::$activeParameters[$parameterName] : false;
    }

    public static function getSortableDates(){
        return self::$panelSortableDates;
    }
    public static function getWeekDayInfo(){
        $date = new DateTime();
        $dayInWeek = $date->format('N');

        $daysRemain = 7 - $dayInWeek;
        $daysOver = $dayInWeek - 1;

        return ["currentDay" => $dayInWeek, "daysOver" => $daysOver, "daysRemain" => $daysRemain];
    }
    public static function getMonthDayInfo(){
        $date = new DateTime();
        $dayInMonth = $date->format('j');
        $monthDaysMax = $date->format('t');

        $daysRemain = $monthDaysMax - $dayInMonth;
        $daysOver = $dayInMonth - 1;

        return ["currentDay" => $dayInMonth, "daysOver" => $daysOver, "daysRemain" => $daysRemain];
    }


}