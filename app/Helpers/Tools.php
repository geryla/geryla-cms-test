<?php
class Tools{

/* ARRAY functions */
// Returns specific array columns | unique keys, reindex keys, rename keys
    public static function getArrayColumns(Array $array, Array $parameters, Bool $unique = false, Bool $reindex = false, Array $renameMap = []): Array{
        $arrayKeys = array_fill_keys($parameters, true);
        $filteredArray = array_map(function($a) use($arrayKeys){ return array_intersect_key($a, $arrayKeys); }, $array);

        if(!empty($renameMap)){
            $filteredArray = self::changeArrayKeys($filteredArray, $renameMap, true);
        }

        if($unique === true){
            $filteredArray = array_map("unserialize", array_unique(array_map("serialize", $filteredArray)));
        }

        if($reindex === true){
            $filteredArray = array_values($filteredArray);
        }

        return (!empty($filteredArray) && !empty($filteredArray[0])) ? $filteredArray : [];
    }
// Returns total of specific columns | add, subtract
    public static function getMultiArraySum(Array $multiArray, String $key, String $value, $subtract = false): Array{
        if(!empty($multiArray)){
            $sumArray = [];

            foreach($multiArray as $array){
                $keyName = (isset($array[$key])) ? $array[$key] : $key;
                if(!isset($sumArray[$keyName])){
                    $sumArray[$keyName] = 0;
                }
                ($subtract === false) ? $sumArray[$keyName] += $array[$value] : $sumArray[$keyName] -= $array[$value];
            }

            return $sumArray;
        }

        return [];
    }
// Returns array with values and renamed keys | recursive
    public static function getMappedArray(Array $array, Array $fillArray, Bool $recursive = false): Array{
        $renamedArray = [];

        foreach($array as $key => $value){
            if(is_array($value)){
                $inner = self::getMappedArray($value, $fillArray, $recursive);
                foreach($fillArray as $newKey => $oldKey){
                    $inner[$newKey] = $value[$oldKey];
                }
                $renamedArray[] = $inner;
            }
        }

        return $renamedArray;
    }
// Returns array with changed keys | recursive
    public static function changeArrayKeys(Array $array, Array $arrayMatches, Bool $recursive = false): Array{
        $renamedArray = [];

        foreach($array as $key => $value){
            $value = ($recursive === true && is_array($value)) ? self::changeArrayKeys($value, $arrayMatches, $recursive) : $value;

            if(isset($arrayMatches[$key])){
                $renamedArray[$arrayMatches[$key]] = $value;
            }else{
                $renamedArray[$key] = $value;
            }
        }

        return $renamedArray;
    }
// Returns array without duplicities by key | reindex keys
    public static function uniqueMultiArrayByKey(Array $array, String $key, Bool $reindex = false): Array{
        $temp = array_unique(array_column($array, $key));
        $uniqueArray = array_intersect_key($array, $temp);

        if($reindex === true){
            $uniqueArray = array_values($uniqueArray);
        }

        return $uniqueArray;
    }
// Returns array with changed values by key/value
    public static function changeArrayValues(Array $array, Array $mappingArray): Array{
        // $mappingArray -multiDimensional | [[keyForCheck -required, keyForChange -required, valueForChange -required, valueForCheck -optional]]

        $returnArray = [];
        foreach($array as $key => $value){
            $valueN = $value;
            if(is_array($value)){
                foreach($value as $key2 => $value2){
                    foreach($mappingArray as $mapping){
                        if($key2 == $mapping[0] && (isset($mapping[3]) ?  $value2 == $mapping[3] : true)){
                            $valueN[$mapping[1]] = $mapping[2];
                        }
                    }
                }
            }

            $returnArray[$key] = $valueN;
        }

        return $returnArray;
    }
//Get the index of an array element based on a substring match.
    public static function getArraySearchBySubstring(Array $array, String $subString){
        $index = null;
        foreach ($array as $key => $value) {
            if (substr($value, 0, strlen($subString)) === $subString) {
                $index = $key;
                break;
            }
        }

        return $index;
    }

}