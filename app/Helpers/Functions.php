<?php
/* URL and Paths helpers */
    function relativePath(String $absolutePath){
        $path = str_replace(ROOT, "", $absolutePath);
        $path = str_replace('\\', '/', $path);

        return $path;
    }
    function relativeFilePath(String $rootPath, String $filePath, String $moduleName = null){
        return relativePath($rootPath).(($moduleName !== null) ? $moduleName.'/' : null).$filePath;
    }
    function buildUrl(Array $parameters, ?String $path = null, Bool $clearParameters = false){
        $urlPath = (!empty($path)) ? $path : $_SERVER["REQUEST_URI"];
        if($clearParameters === true){
            $urlPath = explode("?", $urlPath)[0];
        }

        foreach($parameters as $name => $value){
            $urlPath .= ((strpos($urlPath, '?') !== false) ? '&' : '?')."{$name}={$value}";
        }

        return $urlPath;
    }

/* Condition helpers */
    function isNotEmpty($element, $trimWhitespace = false) {
        if ($trimWhitespace) {
            if (is_array($element)) {
                return !empty(array_filter($element));
            } elseif (!is_array($element) && !is_object($element)) {
                $element = trim($element);
            }
        }

        return isset($element) && !empty($element);
    }
    function isExisting(String $key, Array $array, Bool $notNull = true, Bool $notEmpty = true){
        if(!is_array($array) || !array_key_exists($key, $array)){
            return false;
        }
        if($notNull === true && !isset($array[$key])){
            return false;
        }
        if($notEmpty === true && empty($array[$key])){
            return false;
        }

        return true;
    }
    function useIfExists($element, $fallbackValue = null, $key = null){
        if ($element !== null) {
            $isBoolean = filter_var($element, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
            if ($isBoolean !== null) {
                return $isBoolean;
            }

            if($key !== null){
                if(isExisting($key, $element, false, false) === false){
                    return $fallbackValue;
                }

                $element = $element[$key];
            }

            if (is_array($element) && !empty($element)) {
                return $element;
            } elseif (is_string($element) && trim($element) !== '') {
                return $element;
            }
        }

        return $fallbackValue;
    }

/* Function helpers */
    function slug(String $string) {
        if (empty($string)) {
            return '';
        }

        $convertTable = [
            'á' => 'a', 'Á' => 'A', 'ä' => 'a', 'Ä' => 'A', 'č' => 'c',
            'Č' => 'C', 'ď' => 'd', 'Ď' => 'D', 'é' => 'e', 'É' => 'E',
            'ě' => 'e', 'Ě' => 'E', 'ë' => 'e', 'Ë' => 'E', 'í' => 'i',
            'Í' => 'I', 'ï' => 'i', 'Ï' => 'I', 'ľ' => 'l', 'Ľ' => 'L',
            'ĺ' => 'l', 'Ĺ' => 'L', 'ň' => 'n', 'Ň' => 'N', 'ń' => 'n',
            'Ń' => 'N', 'ó' => 'o', 'Ó' => 'O', 'ö' => 'o', 'Ö' => 'O',
            'ř' => 'r', 'Ř' => 'R', 'ŕ' => 'r', 'Ŕ' => 'R', 'š' => 's',
            'Š' => 'S', 'ś' => 's', 'Ś' => 'S', 'ť' => 't', 'Ť' => 'T',
            'ú' => 'u', 'Ú' => 'U', 'ů' => 'u', 'Ů' => 'U', 'ü' => 'u',
            'Ü' => 'U', 'ý' => 'y', 'Ý' => 'Y', 'ÿ' => 'y', 'Ÿ' => 'Y',
            'ž' => 'z', 'Ž' => 'Z', 'ź' => 'z', 'Ź' => 'Z',
        ];

        $string = strtolower(strtr($string, $convertTable));
        $string = preg_replace('/[^a-zA-Z0-9]+/u', '-', $string);
        $string = preg_replace('/-+/u', '-', $string);
        $string = trim($string, '-');

        return $string;
    }
    function truncate(?String $string, ?Int $length = 150, Bool $stripTags = true, Bool $addSuffix = false){
        if(!empty($string)){
            $string = ($stripTags === true) ? strip_tags($string) : $string;
            $truncated = mb_substr(iconv('utf-8', 'utf-8', $string), 0, $length, 'utf-8');
            $truncated = preg_replace('/&[^;]*?(\s|$)/u', '', $truncated).(($addSuffix === true) ? '...' : null);

            return $truncated;
        }

        return $string;
    }
    function jsonPrevent(?String $string){
        if(empty($string)){
            return "";
        }

        $string = htmlspecialchars_decode(html_entity_decode($string));
        if((strpos($string, '\\u') !== false)){
            $string = str_replace('\u', '*u', $string);
            $string = stripslashes($string);
            $string = str_replace('*u', '\u', $string);
        }

        $decodedString = json_decode($string);
        return ($decodedString === null) ? stripslashes($string) : $string;
    }
    function translate(String $message, Array $parameters = []) {
        global $appController;
        $langFile = $appController->getLangFile();
        $lang = $appController->getLang();

        if (isset($langFile[$message])) {
            $locale = explode('_', $lang)[0];

            $fmt = new MessageFormatter($locale, $langFile[$message]);
            return $fmt->format($parameters);
        }

        return '';
    }
    function getIp() {
        $localHostIP = '127.0.0.1';
        $ipAddress = $localHostIP;

        if (isset($_SERVER['HTTP_CLIENT_IP']) && $_SERVER['HTTP_CLIENT_IP'] != $localHostIP){
            $ipAddress = $_SERVER['HTTP_CLIENT_IP'];
        }else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'] != $localHostIP){
            $ipAddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else if (isset($_SERVER['HTTP_X_FORWARDED']) && $_SERVER['HTTP_X_FORWARDED'] != $localHostIP){
            $ipAddress = $_SERVER['HTTP_X_FORWARDED'];
        }else if (isset($_SERVER['HTTP_FORWARDED_FOR']) && $_SERVER['HTTP_FORWARDED_FOR'] != $localHostIP){
            $ipAddress = $_SERVER['HTTP_FORWARDED_FOR'];
        }else if (isset($_SERVER['HTTP_FORWARDED']) && $_SERVER['HTTP_FORWARDED'] != $localHostIP){
            $ipAddress = $_SERVER['HTTP_FORWARDED'];
        }else if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] != $localHostIP){
            $ipAddress = $_SERVER['REMOTE_ADDR'];
        }

        return $ipAddress;
    }

/* Array helpers */
    function arrayValue($array, $names, $index, $value){
        $key = $names[$index];
        $namesCountMax = count($names)-1;

        $arrayFilled = ($index == $namesCountMax) ? $value : arrayValue(useIfExists($array, [], $key), $names, $index+1, $value);
        (!empty($key)) ? $array[$key] = $arrayFilled : $array[] = $arrayFilled;

        return $array;
    }
    function arrayFill($value, $array = [], $name = null){
        $names = ($name === null) ? [] : explode(".", $name);
        $namesCount = count($names);

        if($name === null){
            $array[] = $value;
        }else{
            if($namesCount > 0){
                $array = arrayValue($array, $names, 0, $value);
            }
        }

        return $array;
    }
    function arraySearch($array, $name = null){
        if($name !== null){
            $names = explode(".", $name);
            $namesCount = count($names);
            $namesCounter = 0;

            do{
                if(isset($array[$names[$namesCounter]])){
                    $array = $array[$names[$namesCounter]];
                    $namesCounter++;
                }else{
                    $array = NULL;
                    $namesCounter = $namesCount;
                }
            }while($namesCounter < $namesCount);

        }

        return $array;
    }

/* Session functions */
    function session($name, $value = null, $remove = false){
        if($name === null || empty($name)){
            return null;
        }

        $sessionData = arraySearch($_SESSION, $name);

        if($value === null && $remove === false){
            return $sessionData;
        }

        if($remove === true){
            $value = null;
            if($sessionData === null){
                return false;
            }else{
                unset($sessionData);
            }
        }

        $_SESSION = arrayFill($value, $_SESSION, $name);
        return true;
    }
    function getSessionLifeTime() {
        $cookieParams = session_get_cookie_params();
        $cacheExpire = session_cache_expire() * 60; // Converted to seconds
        $gcMaxLifetime = ini_get('session.gc_maxlifetime');

        if ($cacheExpire === 0 || $cacheExpire < $gcMaxLifetime) {
            $sessionLifetime = $gcMaxLifetime;
        } else {
            if(isset($cookieParams['lifetime'])){
                $sessionLifetime = min($cookieParams['lifetime'], $gcMaxLifetime);
            }else{
                $sessionLifetime = $gcMaxLifetime;
            }
        }

        // Fallback value to 24 hours
        if((int) $sessionLifetime <= 0){
            $sessionLifetime = 86400;
        }

        return $sessionLifetime;
    }
    function getSessionExpirationTime(?String $dateFormat = null, Bool $fromInit = false) {
        $sessionInitTime = session(($fromInit === true) ? 'start_time' : 'last_action_time');
        $sessionLifeTime = getSessionLifeTime();

        $sessionExpiration = $sessionInitTime + $sessionLifeTime;
        if($dateFormat !== null){
            $sessionExpiration = date($dateFormat, $sessionExpiration);
        }

        return $sessionExpiration;
    }

/* ENV functions */
    function updateEnv(String $oldLine, String $newLine){
        $envFilePath = ROOT.'/.env';
        $lines = file($envFilePath, FILE_IGNORE_NEW_LINES);
        $found = false;

        foreach ($lines as &$line) {
            if (strpos($line, $oldLine) === 0) {
                $line = $newLine;
                $found = true;
                break;
            }
        }

        if($found === true){
            file_put_contents($envFilePath, implode("\n", $lines));
        }
    }

/* Class helpers */
    function initializeClass($className, $params, $forceNewInstance = false) {
        static $instances = [];

        try {
            if (class_exists($className)) {
                $refClass = new ReflectionClass($className);

                if ($refClass->isInstantiable()) {
                    if (!$forceNewInstance && isset($instances[$className])) {
                        return $instances[$className];
                    }

                    $instance = $refClass->newInstanceArgs($params);
                    $instances[$className] = $instance;
                    return $instance;
                } else {
                    throw new RuntimeException("The class $className is not instantiable");
                }
            } else {
                throw new RuntimeException("Class $className does not exist.");
            }
        } catch (ReflectionException $e) {
            throw new RuntimeException("Reflex operation error: " . $e->getMessage(), $e->getCode(), $e);
        }
    }