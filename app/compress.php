<?php
// === Minificator  CSS and JS
function codeMinify($code) {
  $code = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $code);
  $code = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $code);
  $code = str_replace('{ ', '{', $code);
  $code = str_replace(' }', '}', $code);
  $code = str_replace('; ', ';', $code);
  return $code;
}
// === HTML minificator
function sanitize_output($buffer){
  $search = array(
    '/\>[^\S ]+/s',  // strip whitespaces after tags, except space
    '/[^\S ]+\</s',  // strip whitespaces before tags, except space
    '/(\s)+/s'       // shorten multiple whitespace sequences
  );
  $replace = array(
    '>',
    '<',
    '\\1'
  );
  $buffer = preg_replace($search, $replace, $buffer);
  
  return $buffer;
}

$defaultCss = ASSETS_DIR.'css/default.css';
$appCss = ASSETS_DIR.'css/screen.css';
$appScript = ASSETS_DIR.'js/app.js';
require_once(RESOURCES_DIR . "cssParser.php");
require_once(RESOURCES_DIR . "jsParser.php");

if( $jsReturn === true OR $cssReturn === true){   
  $updateBuild = $settingsController->updateBuild();
  echo 'Completed build: '.$updateBuild;
  die;
}