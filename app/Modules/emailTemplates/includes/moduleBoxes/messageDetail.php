<?php
if(empty($oneInfo["settlementDate"])){
    echo '<form method="POST" action=""><button name="mark" value="true" class="btn success">'.translate("mod-emailTemplates-messages-settleBtn").'</button></form>';
}else{
    echo '<h4 style="text-align: right"><b>'.translate("mod-emailTemplates-messages-settlementDate").':</b> '.$oneInfo["settlementDate"].'</h4>';
}

$render->contentDiv(true, null, "editPart");
    $render->contentDiv(true, null, "inlineDiv full");
        echo '<h3>&nbsp;</h3>';
        $render->contentDiv(true, null, "rows");
            $render->contentRow(null, "rowBox inline two first", '<h3>'.translate("mod-emailTemplates-messages-sender").'</h3>', null, "<b>{$oneInfo['email']}</b>");
            $render->contentRow(null, "rowBox inline two last", '<h3>'.translate("mod-emailTemplates-messages-templateName").'</h3>', null, "<b>{$oneInfo['templateInfo']['title']}</b>");

            if(!empty($oneInfo["body"])){
                $render->contentRow(null, "rowBox", '<h3>'.translate("mod-emailTemplates-messages-body").'</h3>', null, null);

                $index = 1;
                foreach($oneInfo["body"] as $key => $value){
                    $num = ($index %2 !== 0) ? 'first' : 'last';
                    $render->contentRow(null, "rowBox inline two {$num}", '<h3 class="blue">['.$key.']</h3>', null, $value);
                    $index++;
                }

                if(!empty($oneInfo['files'])){
                    $render->contentRow(null, "rowBox", '<h3>'.translate("mod-emailTemplates-messages-attachments").'</h3>', null, null);
                    foreach($oneInfo["files"] as $file){
                        echo '<div><a href="'.relativePath($file["path"]).'" target="_blank">'.$file["name"].'</a></div>';
                    }
                }
            }

        $render->contentDiv(false);
    $render->contentDiv(false);
$render->contentDiv(false);