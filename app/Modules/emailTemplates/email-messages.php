<?php
/* =============================== CONTROLLER METHODS ======== */
$emailTemplates = new emailTemplates($database, $appController);

$listPage = $dataCompiler->modulePageUrl(4);
$pageTitle = translate("mod-emailTemplates-messages-list");
$prevButton = null;
$disableList = null;

// ACCESS CONTROL
$pageId = (int) ((isset($_GET["id"])) ? $_GET["id"] : 0);
$pageAction = ( isset($_GET["action"]) ) ? $_GET["action"] : null;
if( $pageAction !== null && ($pageAction == "show" && $pageId == 0)){
    $dataCompiler->setContinue(true);
    $dataCompiler->redirectPage(0, $listPage);
}

// CODE DETAIL load
if($pageAction !== null){
    $oneInfo = $emailTemplates->getMessage($pageId);
    $oneInfo["body"] = ($oneInfo["message"] !== null) ? json_decode(stripslashes($oneInfo["message"]), true) : [];
    $oneInfo["templateInfo"] = $emailTemplates->selectEmailTemplate($oneInfo["emailKey"], $presenter->templateData->langShort);

    $pageTitle = translate("mod-emailTemplates-messages-show");
    $prevButton = $listPage;
    $disableList = "disabled";

    if(isset($_POST["mark"]) && empty($oneInfo["settlementDate"])){
        $emailTemplates->settleMessage($pageId);
        $dataCompiler->refreshPage(true, "");
    }
}

$customTemplates = $emailTemplates->getAllCustomTemplates($presenter->templateData->langShort);
if(!empty($customTemplates)){
    $filterTemplates = [];
    foreach($customTemplates as $templateInfo){
        $filterTemplates[] = ["value" => $templateInfo["emailKey"], "title" => $templateInfo["title"]];
    }

    $filterToModule->setSortOptions([
        [
            'title' => translate("mod-emailTemplates-messages-filtration-sort-key"),
            'type' => 'select',
            'name' => 'emailKey',
            'values' => $filterTemplates
        ]
    ]);
}

$filterToModule->renderFiltration($_SERVER['REQUEST_URI'], $_GET, false, true);
$allData = $filterToModule->getAll_emailMessages($presenter->templateData->langShort, $filterToModule->activeFilters, $filterToModule->limit, $filterToModule->offset, $filterToModule->activeFilters["orderBy"], $filterToModule->activeFilters["orderDirection"]);

/* =============================== CONTENT ======== */
echo '<article class="module">';
    $render->header($pageTitle, $prevButton);
    echo '<section class="suppliers">';
        echo '<div class="moduleWrap">';
            if($pageAction !== null){
                $render->contentDiv(true, null, "contentBox");
                    include($render->getBoxPath("emailTemplates", "messageDetail"));
                $render->contentDiv(false);
            }

            echo '<table class="striped '.$disableList.'">';
                $render->tableHead([translate("mod-emailTemplates-messages-templateName"), translate("mod-emailTemplates-messages-sender"), translate("mod-emailTemplates-messages-createdDate"), translate("mod-emailTemplates-messages-settlementDate"), '']);
                foreach($allData as $message){
                    $templateInfo = $emailTemplates->selectEmailTemplate($message["emailKey"], $presenter->templateData->langShort);
                    $isSettled = (!empty($message["settlementDate"])) ? translate("mod-emailTemplates-messages-yes").' ('.$message["settlementDate"].')' : translate("mod-emailTemplates-messages-no");

                    echo '<tr>';
                        $render->tableColumn('<b>'.$templateInfo["title"].'</b>');
                        $render->tableColumn('<b>'.$message["email"].'</b>');
                        $render->tableColumn('<b>'.$message["createdDate"].'</b>');
                        $render->tableColumn('<b>'.$isSettled.'</b>');
                        $previewSwitch = $listPage.'?action=show&id='.$message["id"];
                        $deleteSwitch = 'class="deleteContent" name="emailMessage" data="'.$message["id"].'"';
                        $render->tableActionColumn(null, $deleteSwitch, $previewSwitch, 100, null, true);
                    echo '</tr>';
                }
            echo '</table>';
        echo '</div>';
    echo '</section>';
echo '</article>';