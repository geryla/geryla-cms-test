<?php
/* =============================== CONTROLLER METHODS ======== */
$langShort = $presenter->templateData->langShort;

$refreshPage = $presenter->activeContent->page;
$nextPage = $dataCompiler->modulePageUrl(3);

$emailTemplates = new emailTemplates($database, $appController);

/* =============================== LANGUAGE ADD-ON METHODS ======== */
$pageData = $emailTemplates->selectAllFrom($langShort);

/* =============================== CONTENT ======== */ 
echo '<article class="module">';
    $render->header(translate("mod-emailTemplates-all_posts"), null, ($presenter->activeContent->enableTranslate === true) ? null : $nextPage.'?action=add');
    echo '<section>';
        echo '<div class="moduleWrap">';
            echo '<table class="striped">';

            $tableHead = [translate("mod-emailTemplates-ad-table-title"), translate("mod-emailTemplates-ad-table-moduleOnly"), ''];
            $render->tableHead($tableHead);

            foreach($pageData as $emailTemplate){
                $moduleEmail = (empty($emailTemplate["moduleNames"])) ? false : true;
                $mustHaveModules = (!$moduleEmail) ? [] : explode(",", $emailTemplate["moduleNames"]);
                $emailType = (!$moduleEmail) ? translate("mod-emailTemplates-ad-table-moduleOnly-false") : translate("mod-emailTemplates-ad-table-moduleOnly-true");

                $enabledTemplate = true;

                if($moduleEmail){
                    foreach($mustHaveModules as $module){
                        $moduleName = trim($module);
                        if(!$appController->isActiveModule($moduleName)){
                            $enabledTemplate = false;
                            break;
                        }
                    }
                }

                $emailName = (!$moduleEmail) ? $emailTemplate["title"] : $emailTemplate["title"]." <i class='red noCursor'>(".$emailTemplate["moduleNames"].")</i>";

                $langTitle = ($presenter->activeContent->enabledMultilanguage > 1) ? "<i class='activeLang'>(".$emailTemplate["activeLang"].")</i> ".$emailName : $emailName;
                $langTranslate = ($presenter->activeContent->enabledMultilanguage > 1 && $emailTemplate["activeTranslation"] === true) ? true : false;

                if( $enabledTemplate === true ){
                    echo '<tr>';
                        $render->tableColumn($langTitle);
                        $render->tableColumn($emailType);

                        $editSwitch = $nextPage.'?action=edit&id='.$emailTemplate["id"];
                        $deleteSwitch = ($moduleEmail === false) ? 'class="deleteContent" name="'.$emailTemplates->translateType.'" data="'.$emailTemplate["id"].'"' : null;
                        $render->tableActionColumn($editSwitch, $deleteSwitch, null, 100, $langTranslate);
                    echo'</tr>';
                }
            }

            echo '</table>';
        echo '</div>';
    echo '</section>';
echo '</article>';