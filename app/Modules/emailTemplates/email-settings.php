<?php
$emailRunner = new emailRunner($database, $appController);
$SMTPConnection = $emailRunner->checkSMTPConnection();

$pageData = $settingsController->getModuleSettings("emailTemplates", true);

$SMTPState = translate("mod-emailTemplates-settings-SMTPConnection-not-active");
if($pageData["emailSettings-method"] === 'smtp'){
    $SMTPState = ($SMTPConnection['isConnected'] === true ) ? translate("mod-emailTemplates-settings-SMTPConnection-connected") : translate("mod-emailTemplates-settings-SMTPConnection-failed");
}


$mediaInfo["name"] = $settingsController->logoMediaName;
$pageId = 1;

if( isset($_POST['submit']) ){
  
  $submitData = [];
  foreach ($_POST as $key => $value) {
    if($key == 'submit'){
      continue;
    }else{
      $submitData[$key] = $value;
    }        
  }
  
  if( isset($submitData["emailSettings-smtp-password"]) && $submitData["emailSettings-smtp-password"] != $pageData["emailSettings-smtp-password"] ){
    $submitData["emailSettings-smtp-password"] = $emailRunner->hashPass("encrypt", $submitData["emailSettings-smtp-password"]);  
  }

  $editSettings = $settingsController->editSettings($submitData, "emailTemplates");

  if($editSettings === true){           
    $returnMessage = translate("admin-edit-true");
    $systemLogger->createLog($_SESSION["languages"]["adminLang"], $presenter->activeContent->module, "updateSettings", 0, $_SESSION["security"]["adminLogin"]["logIn"]);   
  }else{
    $returnMessage = translate("admin-create-false");
  }

  $dataCompiler->setMessage($returnMessage);
  $dataCompiler->setContinue(true);
}

$dataCompiler->showMessage();
$dataCompiler->refreshPage($dataCompiler->nextStep, "");


/* =============================== CONTENT ======== */ 
echo '<article class="module">';
  
  $render->header(translate("mod-emailTemplates-settings-sending"), null);
  
  echo '<section>';                                      
    echo '<form action="" method="POST" enctype="multipart/form-data">';  	
   
     echo $render->button("submit", "true", translate("save"), false, null, "btn icon save success");
      
      echo '<div class="moduleWrap">';             
       
        echo '<table class="striped">';                   
          $render->tableRow(
            translate("mod-emailTemplates-settings-sending-basicSettings"),
            "", true
          );                                    
          $render->tableRow(
            translate("mod-emailTemplates-settings-sending-emailName").'<br><i>'.translate("mod-emailTemplates-settings-sending-emailName-help").'</i>',
            $render->input("text", "emailSettings-emailName", $pageData["emailSettings-emailName"], null, false, null, null)
          );  
          $render->tableRow(
            translate("mod-emailTemplates-settings-sending-emailUser"),
            $render->input("email", "emailSettings-emailUser", $pageData["emailSettings-emailUser"], null, false, null, null)
          );  
          $render->tableRow(
            translate("mod-emailTemplates-settings-sending-encoding").'<br><i>'.translate("mod-emailTemplates-settings-sending-encoding-help").'</i>',
            $render->input("text", "emailSettings-encoding", $pageData["emailSettings-encoding"], null, false, null, null)
          ); 
          $selectValues = [ ["value" => "mail", "title" => translate("mod-emailTemplates-settings-sending-method-mail")], ["value" => "smtp", "title" => translate("mod-emailTemplates-settings-sending-method-smtp")] ];
          $render->tableRow(
            translate("mod-emailTemplates-settings-sending-method").'<br><i>'.translate("mod-emailTemplates-settings-sending-method-help").'</i>',
            $render->select("emailSettings-method", $selectValues, "value", "title", $pageData["emailSettings-method"], false)
          ); 

          
          $render->tableRow(
            translate("mod-emailTemplates-settings-sending-method-mail"),
            '<i>'.translate("mod-emailTemplates-settings-sending-method-mail-help").'</i>', true
          );                                              
          $render->tableRow(
            translate("mod-emailTemplates-settings-sending-method-mail-settings"),
            translate("mod-emailTemplates-settings-sending-method-mail-settings-none")
          );  
          
                   
          $render->tableRow(
            translate("mod-emailTemplates-settings-sending-method-smtp"),
            '<i>'.translate("mod-emailTemplates-settings-sending-method-smtp-help").'</i>', true
          );     
          $render->tableRow(
            translate("mod-emailTemplates-settings-sending-method-smtp-server"),
            $render->input("text", "emailSettings-smtp-server", $pageData["emailSettings-smtp-server"], null, false, null, null)
          );    
          $render->tableRow(
            translate("mod-emailTemplates-settings-sending-method-smtp-port"),
            $render->input("text", "emailSettings-smtp-port", $pageData["emailSettings-smtp-port"], null, false, null, null)
          ); 
          $selectValues = [ ["value" => "tls", "title" => translate("mod-emailTemplates-settings-sending-method-smtp-protocol-tls")], ["value" => "ssl", "title" => translate("mod-emailTemplates-settings-sending-method-smtp-protocol-ssl")] ];
          $render->tableRow(
            translate("mod-emailTemplates-settings-sending-method-smtp-protocol").'<br><i>'.translate("mod-emailTemplates-settings-sending-method-smtp-protocol-help").'</i>',
            $render->select("emailSettings-smtp-protocol", $selectValues, "value", "title", $pageData["emailSettings-smtp-protocol"], false)
          );  
          $render->tableRow(
            translate("mod-emailTemplates-settings-sending-method-smtp-email").'<br><i>'.translate("mod-emailTemplates-settings-sending-method-smtp-email-help").'</i>',
            $render->input("email", "emailSettings-smtp-email", $pageData["emailSettings-smtp-email"], null, false, null, null)
          );
          $render->tableRow(
            translate("mod-emailTemplates-settings-sending-method-smtp-password").'<br><i>'.translate("mod-emailTemplates-settings-sending-method-smtp-password-help").'</i>',
            $render->input("password", "emailSettings-smtp-password", $pageData["emailSettings-smtp-password"], null, false, null, null)
          );

        $render->tableRow(
            translate("mod-emailTemplates-settings-SMTPConnection-title"), '<b class="orange">'.$SMTPState.'</b>'
        );

        echo '</table>';

        if($pageData["emailSettings-method"] === 'smtp' && $SMTPConnection['isConnected'] === false){
            echo '<div>';
                echo ($SMTPConnection['debugger']);
            echo '</div>';
        }

                    
      echo '</div>'; 
    
     echo $render->button("submit", "true", translate("save"), false, null, "btn icon save success");
     
    echo '</form>'; 			 
  echo '</section>';     
echo '</article>';
