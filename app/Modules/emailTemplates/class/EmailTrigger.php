<?php
class EmailTrigger{
    private $database;
    private $appController;

    public $errorInfo = null;

    public function __construct(Database $database, AppController $appController){
        $this->database = $database;
        $this->appController = $appController;
    }

    public function triggerEmail(String $key, Array $data, Array $files = [], Array $options = []){
        if(!empty($key)){
            $allowSubmit = true;

            $reCaptchaResponse = null;
            if(isset($data["recaptcha_response"])){
                $reCaptchaResponse = $data["recaptcha_response"];
                unset($data["recaptcha_response"]);
            }

            // If enabled, run reCaptcha v3
            if($this->appController->isActiveModule("reCaptcha")){
                $allowSubmit = false;
                if($reCaptchaResponse !== null){
                    $systemSettings = $this->appController->getSettings();
                    $reCaptcha = json_decode(file_get_contents($systemSettings["reCaptcha-url"].'?secret='.$systemSettings["reCaptcha-secret_key"].'&response='.$reCaptchaResponse));
                    $allowSubmit = ($reCaptcha->success == true && $reCaptcha->score >= $systemSettings["reCaptcha-range"]) ? true : false;
                }
            }

            if ($allowSubmit === true) {
                $emailObject = new emailRunner($this->database, $this->appController);
                $emailObject->reParseEmailKey($key);

                $emailObject->createEmail($this->appController->getLang());
                $emailObject->address(null);
                $emailObject->attachment($files);
                $emailObject->content($data, null);
                if(isset($options["senderCopy"])){
                    $emailObject->senderCopy($options["senderCopy"]);
                }

                $success = $emailObject->send();
                $this->errorInfo = $emailObject->getError();
            }else{
                $success = false;
            }

            // Save email in case of failed history
            if($success !== false){
                $emailTemplates = new emailTemplates($this->database, $this->appController);
                $emailTemplates->saveMessage($key, $data, $files);
            }

            return $success;
        }

        return false;
    }

}