<?php
class emailSelector{
    private $database;
    private $appController;
    private $emailAssets;

    private $lang;

    public function __construct(Database $database, AppController $appController){
        $this->database = $database;
        $this->appController = $appController;
        $this->emailAssets = new emailAssets($database, $appController);

        $this->lang = $appController->getLangFile();
    }
  
    public function emailSwitch($parsedKey, $formData, $optionalData){
        if( !isset($parsedKey[1]) OR empty($parsedKey[1]) ){
            $returnData = $this->classicTemplate($formData);
        }else{
            $keyModule = strtolower($parsedKey[0]);
            $keyType = strtolower($parsedKey[1]);

            switch ($keyModule){
                case "users":
                    $returnData = $this->classicTemplate($formData);
                    break;
                case "order":
                    switch ($keyType){
                        case "doneorder":
                            $returnData = $this->orderSummary($formData, $optionalData); // (orderDetails, orderItems)
                            break;
                        case "doneorderadmin":
                            $returnData = $this->orderSummary($formData, $optionalData); // (orderDetails, orderItems)
                            break;
                        case "changestatus":
                            $returnData = $this->orderUpdate($formData, $optionalData["orderProducts"], $optionalData["orderStatus"]); // (orderDetails, orderItems, orderStatus)
                            break;
                        default:
                            $returnData = $this->classicTemplate($formData);
                    }
                    break;
                case "booking":
                    $bookingsProcessor = new BookingsProcessor($this->database, $this->appController);
                    switch ($keyType){
                        case "done":
                            $returnData = $bookingsProcessor->bookingEmailData($formData);
                            break;
                        case "doneadmin":
                            $returnData = $bookingsProcessor->bookingEmailData($formData);
                            break;
                        default:
                            $returnData = $this->classicTemplate($formData);
                    }
                    break;
                default:
                    $formData = (!empty($optionalData)) ? array_merge($formData, $optionalData) : $formData;
                    $returnData = $this->classicTemplate($formData);
            }
        }

        $returnData["path"] = SERVER_PROTOCOL."://".SERVER_NAME;
        $returnData["link"] = $this->appController->getProjectPath($formData["langShort"]);
        return $returnData;
    }

    private function classicTemplate($emailData){
        return $emailData;
    }
    private function orderSummary($orderInfo, $allProducts){
        $orderData["orderId"] = $orderInfo["orderId"];
        $orderData["previewToken"] = $orderInfo["previewToken"];
        $orderData["createdDate"] = $orderInfo["createdDate"];
        $orderData["orderDate"] = date('d.m.Y',  strtotime($orderInfo["createdDate"]));
        $orderData["orderTime"] = date('H:i:s',  strtotime($orderInfo["createdDate"]));

        $orderData["name"] = $orderInfo["deliveryName"];
        $orderData["surname"] = $orderInfo["deliverySurname"];
        $orderData["email"] = $orderInfo["invoiceEmail"];
        $orderData["phone"] = $orderInfo["deliveryPhone"];
        $orderData["address"] = $orderInfo["deliveryAddress"];
        $orderData["city"] = $orderInfo["deliveryCity"];
        $orderData["zip"] = $orderInfo["deliveryZip"];
        $orderData["country"] = $orderInfo["deliveryCountry"];

        $orderData["invoiceName"] = $orderInfo["invoiceName"];
        $orderData["invoiceSurname"] = $orderInfo["invoiceSurname"];
        $orderData["invoiceEmail"] = $orderInfo["invoiceEmail"];
        $orderData["invoicePhone"] = $orderInfo["invoicePhone"];
        $orderData["invoiceAddress"] = $orderInfo["invoiceAddress"];
        $orderData["invoiceCity"] = $orderInfo["invoiceCity"];
        $orderData["invoiceZip"] = $orderInfo["invoiceZip"];
        $orderData["invoiceCountry"] = $orderInfo["invoiceCountry"];

        $orderData["companyName"] = $orderInfo["companyName"];
        $orderData["companyIdentNum"] = $orderInfo["companyIdentNum"];
        $orderData["companyVat"] = $orderInfo["companyVat"];
        $orderData["companyTax"] = $orderInfo["companyTax"];

        $orderData["deliveryTitle"] = $orderInfo["deliveryTitle"];

        $deliveryExtendedInfo = (!empty($orderInfo["deliveryInfo"]) && $orderInfo["deliveryInfo"] !== null) ? json_decode($orderInfo["deliveryInfo"], true) : null;
        if(isset($deliveryExtendedInfo['name'])){
            $orderData["deliveryTitle"] .= " [{$deliveryExtendedInfo['name']}]";
        }

        $orderData["deliveryPrice"] = $this->emailAssets->buildPrice($orderInfo["deliveryPriceTax"], $orderInfo["langShort"]);
        $orderData["paymentTitle"] = $orderInfo["paymentTitle"];
        $orderData["paymentPrice"] = $this->emailAssets->buildPrice($orderInfo["paymentPriceTax"], $orderInfo["langShort"]);
        $orderData["deliveryNumber"] = $orderInfo["returnDeliveryId"];

        $orderData["loopItems"] = $this->emailAssets->optimizeItems($allProducts, $orderInfo["langShort"]);
        $orderData["discountValue"] = $this->emailAssets->buildDiscountString(["type" => $orderInfo["discount"], "value" => $orderInfo["discountValue"]], $orderInfo["langShort"]);
        if($orderInfo["discountValue"] && $orderInfo["discountValue"] > 0){
            $orderData["loopItems"][] = ["name" => $orderInfo["discountTitle"], "quantity" => 1, "price" => '-'.$orderData["discountValue"], "priceTotal" => '-'.$orderData["discountValue"]];
        }

        $orderData["totalPrice"] = $this->emailAssets->buildPrice($orderInfo["totalPriceTax"], $orderInfo["langShort"]);
        $orderData["totalPriceNoTax"] = $this->emailAssets->buildPrice($orderInfo["totalPrice"], $orderInfo["langShort"]);
        $orderData["message"] = $orderInfo["message"];

        // QR Payment module
        if(isset($orderInfo["qrPayment"])){
            $orderData["qrPayment"] = $orderInfo["qrPayment"];
        }

        $groupedData = [ // For system emails (hidden variables)
            "invoiceDetail" => $this->emailAssets->addUserInfo($orderInfo),
            "deliveryDetail" => $this->emailAssets->addDelivery($orderInfo).$this->emailAssets->addPayment($orderInfo),
            "orderSummary" => $this->emailAssets->addProducts($allProducts, $orderInfo["langShort"]).$this->emailAssets->addDiscountInfo($orderInfo),
        ];

        if(isset($orderInfo["extraData"]) && !empty($orderInfo["extraData"])){
            $extraData = json_decode(jsonPrevent($orderInfo["extraData"]), true);
            foreach($extraData as $name => $value){
                $orderData["extraData_".$name] = $value;
            }

            if($this->appController->isActiveModule('optysPartner')){
                if(isset($extraData["centerCode"]) && isset($extraData["partnerId"])){
                    $optysController = new OptysController($this->database, $this->appController);
                    $centerInfo = $optysController->getCenterByCode($extraData["centerCode"], $extraData["partnerId"]);
                    if(!empty($centerInfo)){
                        $orderData["extraData_centerTitle"] = $centerInfo["title"];
                    }
                }
            }
        }

        return array_merge($orderData, $groupedData);
    }
    private function orderUpdate($orderInfo, $allProducts, $orderStatus){
        $orderSummary = $this->orderSummary($orderInfo, $allProducts);

        $orderUpdate = ["orderStatus" => $orderStatus["title"], "orderStatusTitle" => $orderStatus["emailTemplateTitle"], "orderStatusText" => $orderStatus["emailTemplateText"]];
        $orderUpdate["orderStatusText"] = str_replace("<!DOCTYPE html><html><head></head><body>", "", $orderUpdate["orderStatusText"]);
        $orderUpdate["orderStatusText"] = str_replace("</body></html>", "", $orderUpdate["orderStatusText"]);

        if($orderInfo["paymentInfo"]){
            $orderUpdate["paymentLink"] = $orderInfo["paymentInfo"];
        }

        $emailData = array_merge($orderSummary, $orderUpdate);

        return $emailData;
    }
}