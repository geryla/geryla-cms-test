<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class emailRunner{
    protected $database;
    protected $appController;
    protected $emailTemplates;

    protected $lang;
    protected $langShort;

    protected $systemSettings;
    protected $mailerSettings;

    private $emailObject = null;
    private $activeEmailKey = null;
    private $parsedKey = null;

    private $defaultFromName = "E-mail";
    private $errorInfo = null;

    public function __construct(Database $database, AppController $appController){
        $this->database = $database;
        $this->appController = $appController;
        $this->emailTemplates = new emailTemplates($this->database, $this->appController);

        $this->loadSettings();
    }

    private function loadSettings(){
        $this->systemSettings = $this->appController->getSettings();

        $loadedSettings["systemEmailTO"] = $this->systemSettings["emailRecipients"];

        $loadedSettings["fromName"] = $this->systemSettings["emailSettings-emailName"];
        $loadedSettings["fromEmail"] = $this->systemSettings["emailSettings-emailUser"];

        $loadedSettings["encoding"] = $this->systemSettings["emailSettings-encoding"];
        $loadedSettings["activeMethod"] = $this->systemSettings["emailSettings-method"];

        $loadedSettings["smtp"]["server"] = $this->systemSettings["emailSettings-smtp-server"];
        $loadedSettings["smtp"]["port"] = $this->systemSettings["emailSettings-smtp-port"];
        $loadedSettings["smtp"]["protocol"] = $this->systemSettings["emailSettings-smtp-protocol"];
        $loadedSettings["smtp"]["email"] = $this->systemSettings["emailSettings-smtp-email"];
        $loadedSettings["smtp"]["password"] = $this->systemSettings["emailSettings-smtp-password"];

        $this->mailerSettings = (object) $loadedSettings;
    }
    public function getError(){
        return $this->errorInfo;
    }

    public function hashPass($action, $password){
        $output = false;
        $encryptMethod = "AES-256-CBC";
        $secureKey = hash('sha256', env('APP_KEY'));
        $secureIV = substr(hash('sha256', '12R45R7812R45R78'), 0, 16); // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning

        if($action == 'encrypt') {
            $output = openssl_encrypt($password, $encryptMethod, $secureKey, 0, $secureIV);
            $output = base64_encode($output);
        }elseif($action == 'decrypt') {
            $output = openssl_decrypt(base64_decode($password), $encryptMethod, $secureKey, 0, $secureIV);
        }

        return $output;
    }

    public function createEmail($langShort = null){
    // Define lang (empty replace with active frontend lang)
        $this->langShort = ($langShort !== null) ? $langShort : $_SESSION["languages"]["appLang"];

    // Initialize
        $this->emailObject = new PHPMailer();
        $this->emailObject->CharSet = $this->mailerSettings->encoding;
        $this->emailObject->isHTML(true);

    // Email method
        if($this->mailerSettings->activeMethod == "smtp"){
            $this->emailObject->isSMTP();
            $this->emailObject->Host = $this->mailerSettings->smtp["server"];
            $this->emailObject->SMTPAuth = true;
            $this->emailObject->Username = $this->mailerSettings->smtp["email"];
            $this->emailObject->SMTPSecure = $this->mailerSettings->smtp["protocol"];
            $this->emailObject->Port = $this->mailerSettings->smtp["port"];
        }

    // Sender info
        $this->emailObject->setFrom($this->mailerSettings->fromEmail, (empty($this->mailerSettings->fromName) ? $this->defaultFromName : $this->mailerSettings->fromName));

        return true;
    }
    public function address($emailRecipient = null){
    // Initialize | If value is empty, send email to system recipient
        $templateRecipient = (!empty($this->activeEmailKey)) ? $this->getEmailRecipient($this->activeEmailKey, $this->langShort) : null;
        $systemRecipient = ($templateRecipient !== null) ? $templateRecipient : $this->mailerSettings->systemEmailTO;

        $emailRecipient = (useIfExists(env('APP_DEBUG_EMAIL'), false) === true || $emailRecipient === null) ? $systemRecipient : $emailRecipient;
        foreach($this->parseEmails($emailRecipient) as $addEmail){
            $this->emailObject->addAddress($addEmail);
        }

        return true;
    }
    public function senderCopy($emailAddress){
        $this->emailObject->addCC($emailAddress);
        return true;
    }
    public function attachment($FILES = []){
        if( !empty($FILES) ){
            foreach($FILES as $file_to_send){
                $this->emailObject->addAttachment($file_to_send["tmp_name"], $file_to_send['name']);
            }
            return true;
        }

        return null;
    }
    public function content($formData = null, $optionalData = null){
        if($this->activeEmailKey == null OR ($formData == null AND $optionalData == null) ){
            return false;
        }

        $emailSelector = new emailSelector($this->database, $this->appController);

        //$customEmailTemplate = $emailSelector->checkForCustomTemplate($this->activeEmailKey, array_merge($formData, $optionalData));
        $emailData = $emailSelector->emailSwitch($this->parsedKey, $formData, $optionalData); // NEW MODULE templates must be wrote in selector class

        if( !empty($emailData) ){
            $emailBodyTemplate = $this->getEmailTemplate($emailData, $this->activeEmailKey, $this->langShort);
            $this->emailObject->Subject  = $this->getEmailSubject($emailData, $this->activeEmailKey, $this->langShort);
            $this->emailObject->Body = $emailBodyTemplate;
            $this->emailObject->AltBody = $this->getPlainTextBody($emailBodyTemplate);
            return true;
        }else{
            return false;
        }
    }
    public function customContent(String $subject, String $contentHtmlSupported){
        if(!empty($contentHtmlSupported)){
            $this->emailObject->Subject  = $subject;
            $this->emailObject->Body = $contentHtmlSupported;
            $this->emailObject->AltBody = $this->getPlainTextBody($contentHtmlSupported);
            return true;
        }else{
            return false;
        }
    }
    public function send(){
    // Decrypt password if SMTP enabled, right before sending
        if($this->mailerSettings->activeMethod == "smtp"){
            $this->emailObject->Password = $this->hashPass("decrypt", $this->mailerSettings->smtp["password"]);
        }

        try {
            $success = $this->emailObject->Send();
            if($success === true){
                $this->activeEmailKey = null;
                $this->parsedKey = null;
                return true;
            }else{
                $this->errorInfo = $this->emailObject->ErrorInfo;
            }
        } catch (Exception $e) {
            $this->errorInfo = $e->getMessage();
        }

        return false;
    }

    public function checkSMTPConnection(){
        $isConnected = false;
        $connectionDebug = null;

        if($this->mailerSettings->activeMethod == "smtp"){
            ob_start();
            try {
                $SMTPConnection = new PHPMailer();
                $SMTPConnection->isSMTP();
                $SMTPConnection->SMTPDebug = SMTP::DEBUG_SERVER;
                $SMTPConnection->Host = $this->mailerSettings->smtp["server"];
                $SMTPConnection->SMTPAuth = true;
                $SMTPConnection->Username = $this->mailerSettings->smtp["email"];
                $SMTPConnection->Password = $this->hashPass("decrypt", $this->mailerSettings->smtp["password"]);
                $SMTPConnection->SMTPSecure = $this->mailerSettings->smtp["protocol"];
                $SMTPConnection->Port = $this->mailerSettings->smtp["port"];

                $isConnected = $SMTPConnection->SmtpConnect();
                $SMTPConnection->smtpClose();
             } catch(Exception $error) {
                echo ("PHPMailer Failed to connect : " . $error);
            }
            $connectionDebug = ob_get_clean();
        }

        return ['isConnected' => $isConnected, 'debugger' => $connectionDebug];
    }
    public function reParseEmailKey($emailKey){
        $this->activeEmailKey = $emailKey;
        $this->parsedKey = explode("-", $emailKey);

        return true;
    }

    private function getEmailTemplate($variablesArr, $templateKey, $langShort){
        $template = $this->emailTemplates->selectEmailTemplate($templateKey, $langShort);
        $templateHTML = $this->decodeTemplate($template["emailContent"], $variablesArr);

        return $templateHTML;
    }
    private function getEmailSubject($variablesArr, $templateKey, $langShort){
        $template = $this->emailTemplates->selectEmailTemplate($templateKey, $langShort);
        $subjectHTML = $this->decodeTemplate($template["emailSubject"], $variablesArr);

        return $subjectHTML;
    }
    private function getEmailRecipient($templateKey, $langShort){
        $template = $this->emailTemplates->selectEmailTemplate($templateKey, $langShort);
        $recipient = (!empty($template["recipient"])) ? $template["recipient"] : null;

        return $recipient;
    }
    private function parseEmails($emailTo){
        $emails = (strpos($emailTo, ',') !== false) ? explode(',', $emailTo) : array($emailTo);
        return $emails;
    }
    private function decodeTemplate($HTML, $variablesArr, $clearAfterFinish = true){

        // Render variables in loop [product items]
        $loopHTML = $this->getStringsBetween($HTML, "{loopItems_start}", "{loopItems_end}");
        if($loopHTML && $variablesArr["loopItems"]){
            $replacedHtml = "";
            foreach($variablesArr["loopItems"] as $item){
                $replacedHtml .= $this->decodeTemplate($loopHTML, $item, false);
            }
            $HTML = str_replace($loopHTML, $replacedHtml, $HTML);
        }

        // Replace {variable} with real value if exists
        if(!empty($variablesArr)){
            foreach($variablesArr as $key => $value) {
                if(!is_array($value)){
                    if ($value !== null) {
                        $HTML = str_replace("{".$key."}", $value, $HTML);
                    }

                    $existsBetween = $this->getStringsBetween($HTML, "{ifExists}", "{endifExists}");
                    if($existsBetween){
                        $variableName = $this->getStringsBetween($existsBetween, "{", "}");
                        $HTML = str_replace("{ifExists}".$existsBetween."{endifExists}", $existsBetween, $HTML);

                        if(!$variablesArr[$variableName]){
                            $HTML = str_replace($existsBetween, "", $HTML);
                        }
                    }
                }
            }
        }

        // Remove {variable} if replace value is not exist
        if($clearAfterFinish === true){
            $HTML = preg_replace('/' . preg_quote('{') .'.*?'.preg_quote('}') . '/', '', $HTML);
        }

        return $HTML;
    }
    private function getStringsBetween($string, $start, $end){
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }
    private function getPlainTextBody($templateBody) {
        $fallbackBody = strip_tags($templateBody);
        $fallbackBody = str_replace("&nbsp;", "\n", $fallbackBody);
        $fallbackBody = explode("\n", $fallbackBody);
        $fallbackParts = [];

        $fallbackBodyCount = count($fallbackBody);

        for ($index = 0; $index < $fallbackBodyCount; $index++) {
            $actualString = trim($fallbackBody[$index]);

            if ($index + 1 < $fallbackBodyCount) {
                $nextString = trim($fallbackBody[$index + 1]);
            } else {
                $nextString = null; // To avoid the "trim() on null" error
            }

            if (!empty($actualString) || (!empty($nextString) && $nextString !== null)) {
                $fallbackParts[] = $fallbackBody[$index];
            }
        }

        return implode("\n", $fallbackParts);
    }

}