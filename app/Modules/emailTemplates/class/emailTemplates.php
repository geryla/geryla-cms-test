<?php
class emailTemplates {
    public $moduleName = "emailTemplates";
    private $database;
    private $appController;
    private $localizationClass;

    public $headTableName;
    public $messagesTable;
    public $lang = null;

    public $translateType;

    public $translateRows = ["title", "emailSubject", "emailContent"];
    public $dataRows = ["emailVariables", "createdDate", "updatedDate", "emailKey", "recipient"];

    public function __construct(Database $database, AppController $appController) {
        $this->database = $database;
        $this->appController = $appController;
        $this->localizationClass = $appController->returnLocalizationClass();
        $this->lang = $appController->getLangFile();

        $this->headTableName = databaseTables::getTable($this->moduleName, "templates");
        $this->messagesTable = databaseTables::getTable($this->moduleName, "messages");
        $this->translateType = $this->appController->getModuleConfig($this->moduleName,"translateType")["emailTemplates"];
    }

/* ================== MODULE FUNCTIONS === */

// Select one template by ID
    public function selectOneFrom($id, $langShort) {
        $sql = "SELECT * FROM `{$this->headTableName}` WHERE id = :id";
        $returnData = $this->localizationClass->translateResult($this->database->getQuery($sql, ["id" => $id], false), $this->translateType, $langShort);

        return $returnData;
    }
// Select email template by emailKey
    public function selectEmailTemplate($emailKey, $langShort) {
        $sql = "SELECT * FROM `{$this->headTableName}` WHERE emailKey = :emailKey";
        $returnData = $this->localizationClass->translateResult($this->database->getQuery($sql, ["emailKey" => $emailKey], false), $this->translateType, $langShort);

        return $returnData;
    }
// Select all templates
    public function selectAllFrom($langShort) {
        $sql = "SELECT * FROM `{$this->headTableName}`";
        $returnData = $this->localizationClass->translateResult($this->database->getQuery($sql, null, true), $this->translateType, $langShort);

        return $returnData;
    }
// Select all email keys for forms
    public function selectAllTemplates() {
        $sql = "SELECT emailKey FROM `{$this->headTableName}`";
        $returnData = $this->database->getQuery($sql, null, true);

        return $returnData;
    }
// Select all templates as select box options
    public function selectAllTemplatesAsSelect($selectedTemplate, $langShort, $modulesController, $all = false) {
        $allTemplates = $this->selectAllFrom($langShort);
        $returnData = null;

        if (!empty($allTemplates)) {
            foreach ($allTemplates as $template) {
                $okey = true;

                if ($all === false) {
                    $mustHaveModules = explode(",", $template["moduleNames"]);
                    if (!empty($mustHaveModules)) {
                        foreach ($mustHaveModules as $mustHave) {
                            if (!empty($mustHave) && !$this->appController->isActiveModule($mustHave)) {
                                $okey = false;
                                break;
                            }
                        }
                    }
                }

                if ($okey === true) {
                    $returnData .= '<option value="' . $template["id"] . '" ' . (($selectedTemplate == $template["id"]) ? "selected" : "") . '>' . $template["title"] . '</option>';
                }
            }
        }

        return $returnData;
    }
// Select all email keys for forms
    public function getAllCustomTemplates($langShort) {
        $sql = "SELECT title, emailKey FROM `{$this->headTableName}` WHERE (moduleNames IS NULL OR moduleNames = '')";
        $returnData = $this->localizationClass->translateResult($this->database->getQuery($sql, null, true), $this->translateType, $langShort);

        return $returnData;
    }

    public function saveMessage(String $emailKey, Array $formData, Array $filesData = []) {
        $messageData = ['emailKey' => $emailKey, 'email' => $formData["email"], 'message' => json_encode($formData), 'createdDate' => date("Y-m-d H:i:s")];
        $messageId = $this->database->insertQuery("INSERT INTO `{$this->messagesTable}`", $messageData);

        if($messageId !== false && !empty($filesData)){
            $messagesFolder = $this->getMessageFolder($messageId);
            foreach($filesData as $file){
                if (file_exists($file['tmp_name'])) {
                    if (!is_dir($messagesFolder)) {
                        mkdir($messagesFolder, 0755, true);
                    }

                    copy($file['tmp_name'], $messagesFolder.$file['name']);
                }
            }
        }

        return $messageId;
    }
    public function getMessage(Int $messageId) {
        $messageData = $this->database->getQuery("SELECT * FROM `{$this->messagesTable}` WHERE id = :id", ["id" => $messageId], false);
        if(!empty($messageData)){
            $messageFolder = $this->getMessageFolder($messageId);
            if (is_dir($messageFolder)) {
                $files = scandir($messageFolder);
                $files = array_diff($files, array('.', '..'));

                foreach ($files as $file) {
                    $messageData['files'][] = ['path' => $messageFolder.$file, 'name' => $file];
                }
            }
        }

        return $messageData;
    }
    private function getMessageFolder(Int $messageId){
        return STORAGE_DIR.'emailMessages/'.$messageId.'/';
    }
    public function deleteMessageAttachments(Int $messageId) {
        $messageFolder = $this->getMessageFolder($messageId);
        if (is_dir($messageFolder)) {
            $files = scandir($messageFolder);
            $files = array_diff($files, array('.', '..'));

            foreach ($files as $file) {
                unlink($messageFolder.$file);
            }
            rmdir($messageFolder);
        }
    }
    public function settleMessage(Int $messageId) {
        return $this->database->updateQuery("UPDATE `{$this->messagesTable}`", ["settlementDate" =>  date("Y-m-d H:i:s")], ["id" => $messageId], "WHERE id = :id");
    }


// Check if URL address exist
    public function checkKeyExist($emailKey) {
        $sql = "SELECT * FROM `{$this->headTableName}` WHERE emailKey = :emailKey;";
        $returnData = $this->database->getQuery($sql, array("emailKey" => $emailKey), false);

        return (empty($returnData)) ? false : $returnData;
    }
// Check if URL is not exits
    public function checkUrlDuplicity($id, $keyCheck) {
        $urlCheck = $keyCheck;
        $urlControl = $this->checkKeyExist($urlCheck);

        if ($urlControl !== false) {
            $urlExist = ($id == $urlControl["id"]) ? false : true;
        } else {
            $urlExist = $urlControl;
        }

        if ($urlExist === true) {
            $urlCount = 1;
            do {
                $urlCheck = $keyCheck . '-' . $urlCount;
                $urlExist = $this->checkKeyExist($urlCheck);
                $urlCount++;
            } while ($urlExist === true);
        }

        return $urlCheck;
    }
// Get variables for backend
    public function getVariablesToAdmin($emailId, $emailVariables, $noVariable) {

        $variables = '<b class="green">' . $noVariable . '</b><br /><br />';
        $variablesArray = [];

        if (!empty($emailVariables)) {
            $variablesArray = explode(",", $emailVariables);
        } else {
            $contactForms = new contactForm($this->database, $this->appController);
            $allFormsForEmail = $contactForms->selectCrossByEmail($emailId);
            if (!empty($allFormsForEmail)) {
                foreach ($allFormsForEmail as $assignedForm) {
                    $checkForm = $contactForms->selectOneVisibleFrom($assignedForm["formId"], $_SESSION["languages"]["adminLang"]);
                    if (!empty($checkForm)) {
                        $formVariables = $contactForms->checkForVariables($checkForm["description"], "[", "]");
                        if (!empty($formVariables)) {
                            $variablesArray = array_merge($variablesArray, $formVariables);
                        }
                    }
                }
            }
        }

        if (!empty($variablesArray)) {
            $variables = '<div class="groupedHelpers">';
            foreach ($variablesArray as $variable) {
                $variables .= '<label data="' . $variable . '">' . $variable . '</label>';
            }
            $variables .= '</div>';
        }

        return $variables;
    }
}