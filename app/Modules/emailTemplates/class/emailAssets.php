<?php
class emailAssets{
  private $database;
  private $mediaProcessor;
  private $currencyBuilder;

  private $lang;
  private $templatePath = EMAIL_DIRNAME."/templates/";

  private $defaultStyles = [
      "table" => "width: 100%;border-spacing: unset;",
      "cell" => "padding: 5px 15px;border: 1px solid #000;border-right: 0px;border-top: 0px;",
      "cellLast" => "padding: 5px 15px;border: 1px solid #000;border-top: 0px;",
      "cellTop" => "padding: 5px 15px;border: 1px solid #000;border-right: 0px;",
      "cellLastTop" => "padding: 5px 15px;border: 1px solid #000;",

      "width-100" => "width: 100%;",
      "width-75" => "width: 75%;",
      "width-50" => "width: 50%;",
      "width-45" => "width: 45%;",
      "width-25" => "width: 25%;",
      "width-20" => "width: 20%;",
      "width-10" => "width: 10%;",

      "fontWeight-regular" => "font-weight: 300;",
      "fontWeight-medium" => "font-weight: 500;",
      "fontWeight-bold" => "font-weight: 700;",
  ];
  
  public function __construct(Database $database, AppController $appController){
      $this->database = $database;
      $this->mediaProcessor = $appController->returnMediaProcessor();
      $this->currencyBuilder = $appController->returnCurrencyBuilder();

      $this->lang = $appController->getLangFile();
  }

   
/* ================== UTILITIES for email assets */
// Build price (currency Builder - language module)
    public function buildPrice($priceValue, $langShort){
        return $this->currencyBuilder->buildPriceString($langShort, (float)$priceValue);
    }    
    public function buildDiscountString($discountInfo, $langShort){
        return ($discountInfo["type"] == "percent") ? $discountInfo["value"]." %" : $this->currencyBuilder->buildPriceString($langShort, (float)$discountInfo["value"]);
    }


/* ================== EMAIL PARTS CREATOR === */
    
// ORDER - Items in order
    public function addProducts($templateData, $langShort){
        $lang = $this->lang;
        $emailTemplate = null;

        if( file_exists($this->templatePath."orderProducts.php") ){
            include($this->templatePath."orderProducts.php"); // INPUTS: $templateData, $lang, OUTPUT: $emailTemplate
        }else{
            $emailTemplate .= '<table style="'.$this->defaultStyles["table"].'">';
            foreach($templateData as $dataKey => $item){
                $itemAttributes = (!empty($item["selectedAttributes"])) ? explode(";", $item["selectedAttributes"]) : null;
                $attributesString = null;
                if($itemAttributes !== null){
                    foreach($itemAttributes as $key => $attribute){
                        $attributesString .= ($key != 0) ? ", " : "";
                        $attributesString .= str_replace("|", " : ", $attribute);
                    }
                }

                if($dataKey == 0){
                    $emailTemplate .= '<tr>';
                        $emailTemplate .= '<td style="'.$this->defaultStyles["cellTop"].$this->defaultStyles["width-45"].'">';
                            $emailTemplate .= '<span style="'.$this->defaultStyles["fontWeight-bold"].'">'.translate("mod-emailTemplates-emailTemplate-product-name").'</span>';
                        $emailTemplate .= '</td>';
                        $emailTemplate .= '<td style="'.$this->defaultStyles["cellTop"].$this->defaultStyles["width-10"].'">';
                            $emailTemplate .= '<span style="'.$this->defaultStyles["fontWeight-bold"].'">'.translate("mod-emailTemplates-emailTemplate-product-quantity").'</span>';
                        $emailTemplate .= '</td>';
                        $emailTemplate .= '<td style="'.$this->defaultStyles["cellTop"].$this->defaultStyles["width-20"].'">';
                            $emailTemplate .= '<span style="'.$this->defaultStyles["fontWeight-bold"].'">'.translate("mod-emailTemplates-emailTemplate-product-priceSingle").'</span>';
                        $emailTemplate .= '</td>';
                        $emailTemplate .= '<td style="'.$this->defaultStyles["cellLastTop"].$this->defaultStyles["width-25"].'">';
                            $emailTemplate .= '<span style="'.$this->defaultStyles["fontWeight-bold"].'">'.translate("mod-emailTemplates-emailTemplate-product-priceTotal").'</span>';
                        $emailTemplate .= '</td>';
                    $emailTemplate .= '</tr>';
                }

                $emailTemplate .= '<tr>';
                    $emailTemplate .= '<td style="'.$this->defaultStyles["cell"].$this->defaultStyles["width-45"].'">';
                        $emailTemplate .= '<span style="'.$this->defaultStyles["fontWeight-medium"].'">'.$item["name"].'</span>';
                        $emailTemplate .= (isset($attributesString)) ? '<div style="font-size: 80%;'.$this->defaultStyles["fontWeight-regular"].'">('.$attributesString.')</div>' : null;
                    $emailTemplate .= '</td>';
                    $emailTemplate .= '<td style="'.$this->defaultStyles["cell"].$this->defaultStyles["width-10"].'">';
                        $emailTemplate .= '<span style="'.$this->defaultStyles["fontWeight-regular"].'">'.$item["quantity"].' '.translate("mod-emailTemplates-emailTemplate-pcs").'</span>';
                    $emailTemplate .= '</td>';
                    $emailTemplate .= '<td style="'.$this->defaultStyles["cell"].$this->defaultStyles["width-20"].'">';
                        $emailTemplate .= '<span style="'.$this->defaultStyles["fontWeight-regular"].'">'.$this->buildPrice($item["priceTax"], $langShort).'</span>';
                    $emailTemplate .= '</td>';
                    $emailTemplate .= '<td style="'.$this->defaultStyles["cellLast"].$this->defaultStyles["width-25"].'">';
                        $emailTemplate .= '<span style="'.$this->defaultStyles["fontWeight-bold"].'">'.$this->buildPrice(($item["quantity"]*$item["priceTax"]), $langShort).'</span>';
                    $emailTemplate .= '</td>';
                $emailTemplate .= '</tr>';
            }
            $emailTemplate .= '</table>';
        }

        return $emailTemplate;
    }
// ORDER - Order discount
    public function addDiscountInfo($templateData){
        $lang = $this->lang;
        $emailTemplate = null;

        if(!empty($templateData["discountValue"])) {
            $discountValue = $this->buildDiscountString(["type" => $templateData["discount"], "value" => $templateData["discountValue"]], $templateData["langShort"]);

            if (file_exists($this->templatePath . "orderDiscount.php")) {
                include($this->templatePath . "orderDiscount.php"); // INPUTS: $templateData, $lang, $discountValue, OUTPUT: $emailTemplate
            }else{
            $emailTemplate .= '<table style="'.$this->defaultStyles["table"].'">';
                $emailTemplate .= '<tr>';
                    $emailTemplate .= '<td style="'.$this->defaultStyles["cell"].$this->defaultStyles["width-75"].'">';
                        $emailTemplate .= '<span style="'.$this->defaultStyles["fontWeight-medium"].'">'.translate("mod-emailTemplates-emailTemplate-discount").'</span>';
                    $emailTemplate .= '</td>';
                    $emailTemplate .= '<td style="'.$this->defaultStyles["cellLast"].$this->defaultStyles["width-25"].'">';
                        $emailTemplate .= '<span style="'.$this->defaultStyles["fontWeight-bold"].'">- '.$discountValue.'</span>';
                    $emailTemplate .= '</td>';
                $emailTemplate .= '</tr>';
            $emailTemplate .= '</table>';
            }
        }

        return $emailTemplate;
    }
// ORDER - Delivery method
    public function addDelivery($templateData){
      $lang = $this->lang;
      $emailTemplate = null;

      if (file_exists($this->templatePath . "orderDeliveryMethod.php")) {
          include($this->templatePath . "orderDeliveryMethod.php"); // INPUTS: $templateData, $lang, OUTPUT: $emailTemplate
      }else{
          $emailTemplate .= '<table style="'.$this->defaultStyles["table"].'">';
              $emailTemplate .= '<tr>';
                  $emailTemplate .= '<td style="'.$this->defaultStyles["cellTop"].$this->defaultStyles["width-75"].'">';
                    $emailTemplate .= '<span style="'.$this->defaultStyles["fontWeight-medium"].'">'.translate("mod-emailTemplates-emailTemplate-delivery").': '.$templateData["deliveryTitle"].'</span>';
                  $emailTemplate .= '</td>';
                  $emailTemplate .= '<td style="'.$this->defaultStyles["cellLastTop"].$this->defaultStyles["width-25"].'">';
                    $emailTemplate .= '<span style="'.$this->defaultStyles["fontWeight-bold"].'">'.$this->buildPrice($templateData["deliveryPriceTax"], $templateData["langShort"]).'</span>';
                  $emailTemplate .= '</td>';
              $emailTemplate .= '</tr>';
          $emailTemplate .= '</table>';
      }

      return $emailTemplate;
  }
// ORDER - Payment method
    public function addPayment($templateData){
        $lang = $this->lang;
        $emailTemplate = null;

        if (file_exists($this->templatePath . "orderPaymentMethod.php")) {
            include($this->templatePath . "orderPaymentMethod.php"); // INPUTS: $templateData, $lang, OUTPUT: $emailTemplate
        }else{
            $emailTemplate .= '<table style="'.$this->defaultStyles["table"].'">';
                $emailTemplate .= '<tr>';
                    $emailTemplate .= '<td style="'.$this->defaultStyles["cell"].$this->defaultStyles["width-75"].'">';
                        $emailTemplate .= '<span style="'.$this->defaultStyles["fontWeight-medium"].'">'.translate("mod-emailTemplates-emailTemplate-payment").': '.$templateData["paymentTitle"].'</span>';
                    $emailTemplate .= '</td>';
                    $emailTemplate .= '<td style="'.$this->defaultStyles["cellLast"].$this->defaultStyles["width-25"].'">';
                        $emailTemplate .= '<span style="'.$this->defaultStyles["fontWeight-bold"].'">'.$this->buildPrice($templateData["paymentPriceTax"], $templateData["langShort"]).'</span>';
                    $emailTemplate .= '</td>';
                $emailTemplate .= '</tr>';
            $emailTemplate .= '</table>';
        }

        return $emailTemplate;
    }
// ORDER - User delivery details
    public function addUserInfo($templateData){
        $lang = $this->lang;
        $emailTemplate = null;
        $companyOrder = (!empty($templateData["companyName"]) || !empty($templateData["companyIdentNum"])) ? true : false;

        if (file_exists($this->templatePath . "orderUserInfo.php")) {
            include($this->templatePath . "orderUserInfo.php"); // INPUTS: $templateData, $lang, $companyOrder, OUTPUT: $emailTemplate
        }else{
            $emailTemplate .= '<table style="'.$this->defaultStyles["table"].'">';
                $emailTemplate .= '<tr>';
                    $emailTemplate .= '<td style="'.(($companyOrder === true) ? $this->defaultStyles["cellTop"].$this->defaultStyles["width-50"] : $this->defaultStyles["cellLastTop"].$this->defaultStyles["width-100"]).'">';
                        $emailTemplate .= '<div style="'.$this->defaultStyles["fontWeight-bold"].'">'.translate("mod-emailTemplates-emailTemplate-userInfo").':</div>';
                        $emailTemplate .= '<div style="'.$this->defaultStyles["fontWeight-medium"].'">'.$templateData["deliveryName"].' '.$templateData["deliverySurname"].'</div>';
                        $emailTemplate .= '<div style="'.$this->defaultStyles["fontWeight-regular"].'">'.$templateData["deliveryAddress"].', '.$templateData["deliveryCity"].'</div>';
                        $emailTemplate .= '<div style="'.$this->defaultStyles["fontWeight-regular"].'">'.$templateData["deliveryZip"].'</div>';
                        $emailTemplate .= '<br /><div style="'.$this->defaultStyles["fontWeight-regular"].'">'.translate("mod-emailTemplates-emailTemplate-email").': '.$templateData["invoiceEmail"].'</div>';
                        $emailTemplate .= (!empty($templateData["deliveryPhone"])) ? '<div style="'.$this->defaultStyles["fontWeight-regular"].'">'.translate("mod-emailTemplates-emailTemplate-phone").': '.$templateData["deliveryPhone"].'</div>' : null;
                    $emailTemplate .= '</td>';
                    if($companyOrder === true){
                        $emailTemplate .= '<td style="'.$this->defaultStyles["cellLastTop"].$this->defaultStyles["width-50"].'">';
                            $emailTemplate .= '<div style="'.$this->defaultStyles["fontWeight-bold"].'">'.translate("mod-emailTemplates-emailTemplate-companyInfo").':</div>';
                            $emailTemplate .= '<div style="'.$this->defaultStyles["fontWeight-medium"].'">'.$templateData["companyName"].'</div>';
                            $emailTemplate .= (!empty($templateData["companyIdentNum"])) ? '<div style="'.$this->defaultStyles["fontWeight-regular"].'">'.translate("mod-emailTemplates-emailTemplate-identNum").': '.$templateData["companyIdentNum"].'</div>' : null;
                            $emailTemplate .= (!empty($templateData["companyVat"])) ? '<div style="'.$this->defaultStyles["fontWeight-regular"].'">'.translate("mod-emailTemplates-emailTemplate-vatNum").': '.$templateData["companyVat"].'</div>' : null;
                            $emailTemplate .= (!empty($templateData["companyTax"])) ? '<div style="'.$this->defaultStyles["fontWeight-regular"].'">'.translate("mod-emailTemplates-emailTemplate-taxNum").': '.$templateData["companyTax"].'</div>' : null;
                        $emailTemplate .= '</td>';
                    }
                $emailTemplate .= '</tr>';
            $emailTemplate .= '</table>';
        }

        return $emailTemplate;
  }

/* ================== EMAIL PARTS CREATOR === */
    public function optimizeItems($templateData, $langShort){
        $items = [];

        foreach($templateData as $dataKey => $item){
            $optItem = $item;

            $itemAttributes = (!empty($item["selectedAttributes"])) ? explode(";", $item["selectedAttributes"]) : null;
            $attributesString = null;
            if($itemAttributes !== null){
                foreach($itemAttributes as $key => $attribute){
                    $attributesString .= ($key != 0) ? ", " : "";
                    $attributesString .= str_replace("|", " : ", $attribute);
                }
                $optItem["selectedAttributes"] = $attributesString;
            }

            $optItem["price"] = $this->buildPrice($item["priceTax"], $langShort);
            $optItem["priceTotal"] = $this->buildPrice(($item["quantity"]*$item["priceTax"]), $langShort);
            $optItem["priceNoTax"] = $this->buildPrice($item["price"], $langShort);
            $optItem["priceTotalNoTax"] = $this->buildPrice(($item["quantity"]*$item["price"]), $langShort);

            if(isset($item["productId"])){
                $optItem["image"] = $this->mediaProcessor->getMainImage($item["productId"], 'product', $langShort, '60')->url;
            }
            $items[] = $optItem;
        }

        return $items;
    }
}