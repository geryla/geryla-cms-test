<?php
    $config = [
        "moduleName" => "Email templates",
        "moduleUrlName" => "emailTemplates",
        "moduleLangName" => "emailTemplates",
        "moduleIcon" => "fa-envelope-o",
        "moduleVersion" => "4.6.0",
        "requiredModules" => [
            ["moduleName" => "settings"],
        ],

        "mediaSettings" => [],
        "translateType" => [
            "emailTemplates" => "emailTemplates"
        ],
        "databaseTables" => [
            "templates" => [
                "name" => "emailtemplates",
            ],
            "messages" => [
                "name" => "email-messages",
            ],
        ],

        "modulePages" => [
            [
                "pageId" => 1,
                "urlName" => "email-settings",
                "url" => "email-settings",
                "parent" => 0,
                "inMenu" => 1,
                "headPage" => 1
            ],
            [
                "pageId" => 2,
                "urlName" => "email-templates",
                "url" => "email-templates",
                "parent" => 0,
                "inMenu" => 1,
                "headPage" => 0
            ],
            [
                "pageId" => 3,
                "urlName" => "email-detail",
                "url" => "email-detail",
                "parent" => 1,
                "inMenu" => 0,
                "headPage" => 0
            ],
            [
                "pageId" => 4,
                "urlName" => "email-messages",
                "url" => "email-messages",
                "parent" => 0,
                "inMenu" => 1,
                "headPage" => 0
            ]
        ]
    ];