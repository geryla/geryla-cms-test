<?php
/* =============================== CONTROLLER METHODS ======== */
$langShort = $presenter->templateData->langShort;

$refreshPage = $presenter->activeContent->page;
$prevPage = $dataCompiler->modulePageUrl(2);

$pageId = ( isset($_GET["id"]) ) ? $_GET["id"] : 0;
$pageAction = ( isset($_GET["action"]) ) ? $_GET["action"] : null;

$emailTemplates = new emailTemplates($database, $appController);

/* =============================== LANGUAGE ADD-ON METHODS ======== */
$emailData = ($pageId !== 0 ) ? $emailTemplates->selectOneFrom($pageId, $langShort) : null;

// REQUIRED MODULES CONTROL
if($emailData !== null){
    $mustHaveModules = explode(",", $emailData["moduleNames"]);
    if( !empty($mustHaveModules) ){
        foreach($mustHaveModules as $mustHave){
            $moduleName = trim($mustHave);
            if( !empty($moduleName) && !$appController->isActiveModule($moduleName)){
                $pageId = 0;
                break;
            }
        }
    }
}

// ACCESS CONTROL
if( $pageAction === null OR ($pageAction == "edit" && $pageId == 0) OR ($pageAction == "add" && $pageId != null) ){
  $dataCompiler->setContinue(true);
  $dataCompiler->redirectPage(0, $prevPage);
}

$moduleEmail = ( ($pageAction == "add") OR ($pageAction == "edit" && empty($emailData["moduleNames"])) ) ? false : true;
$pageTitle = ( $pageAction == "add" ) ? translate("mod-emailTemplates-add-post") : translate("mod-emailTemplates-edit-post").': <b>'.$emailData["title"].'</b>';
$variables = ($pageId !== 0 && ($appController->isActiveModule("contactForm") || !empty($emailData["moduleNames"]))) ? $emailTemplates->getVariablesToAdmin($pageId, $emailData["emailVariables"], translate("mod-emailTemplates-ad-variables-none")) : null;

if( isset($_POST['submit']) ){

// SetUp variables  
  $continue = true;
  $postData = $_POST;  
  unset($postData['submit']);

// Create additional params 
  $postData["emailKey"] = $emailTemplates->checkUrlDuplicity($emailData["id"], $prefixEngine->title2pagename($postData["emailKey"]));  
  $postData["createdDate"] = ($pageId === 0) ? date("Y-m-d H:i:s") : $emailData["createdDate"];  
  $postData["updatedDate"] = date("Y-m-d H:i:s"); 

// Module work (add/edit/translate)    
  $dataCompiler->setTables($emailTemplates->headTableName, null);
  $dataCompiler->setData($postData, $emailTemplates->translateRows, $emailTemplates->dataRows);
  $dataCompiler->setTranslate($presenter->activeContent->enableTranslate);
   
  if($pageId == 0){               
    $pageId = $dataCompiler->addQueryAndTranslate(); // Add and translate
  }else{  
    $dataCompiler->setDefaultData(null);
    $result = $dataCompiler->updateQueryAndTranslate($pageId, $emailTemplates->translateType); // Update and translate
  } 
  
  if($continue === true){
    $systemLogger->createLog($langShort, $presenter->activeContent->module, $pageAction."EmailTemplate", $pageId, $_SESSION["security"]["adminLogin"]["logIn"]);  
  }
  
  $refreshPage = ($continue === true) ? 1 : 0; 
}     

$dataCompiler->showMessage();
$dataCompiler->refreshPage($dataCompiler->nextStep, ($pageAction == "add" && $pageId != 0) ? $presenter->activeContent->page.'?action=edit&id='.$pageId : "");

/* =============================== CONTENT ======== */ 
echo '<article class="module">';
  
  $render->header($pageTitle, $prevPage);
                                            
  echo '<section>';                                      
    echo '<form action="" method="POST" enctype="multipart/form-data">'; 
      
      echo $render->button("submit", "true", translate("save"), false, null, "btn icon save success");
      
      echo '<div class="moduleWrap">';             
        
        $render->contentDiv(true, null, "contentBox full");  
         
          $editableInput = ($moduleEmail === false) ? $render->input("text", "title", $emailData["title"], null, true, null, null) : $render->disabled("text", $emailData["title"]);          
          $render->contentDiv(true, null, "rowBox"); 
            $render->contentRow(null, "full", '<h3>'.translate("mod-emailTemplates-ad-title").'</h3>', null, $editableInput);
          $render->contentDiv(false);

          if($moduleEmail === false){
            $render->contentDiv(true, null, "rowBox");
              $render->contentRow(null, "full", '<h3>'.translate("mod-emailTemplates-ad-key").'</h3>', null, $render->input("text", "emailKey", $emailData["emailKey"], null, true, null, null));
            $render->contentDiv(false);
          }else{
              echo $render->input("hidden", "emailKey", $emailData["emailKey"], null, true, null, null);
          }
                          
          $render->contentDiv(true, null, "rowBox"); 
            $render->contentRow(null, "full", '<h3>'.translate("mod-emailTemplates-ad-subject").'</h3>', null, $render->input("text", "emailSubject", $emailData["emailSubject"], null, true, null, null));
          $render->contentDiv(false);

            $render->contentDiv(true, null, "rowBox");
                $render->contentRow(null, "full notTranslatable", '<h3>'.translate("mod-emailTemplates-ad-recipient").'</h3>', '<span class="help">'.translate("mod-emailTemplates-ad-recipient-help").'</span>', $render->input("text", "recipient", $emailData["recipient"], null, false, null, null));
            $render->contentDiv(false);

          if($variables !== null){
              $render->contentDiv(true, null, "rowBox");
                $render->contentRow(null, "full notTranslatable", '<h3>'.translate("mod-emailTemplates-ad-variables").'</h3>', '<span class="help">'.translate("mod-emailTemplates-ad-variables-help").'</span>', $variables);
              $render->contentDiv(false);
          }

          $render->contentDiv(true, null, "rowBox"); 
            $render->contentRow(null, "full", '<h3>'.translate("mod-emailTemplates-ad-content").'</h3>', null, $render->textarea("emailContent", $emailData["emailContent"], 7, null, null, null, "textarea"));
          $render->contentDiv(false); 
                    
        $render->contentDiv(false);          

      echo '</div>'; 
      
      echo $render->button("submit", "true", translate("save"), false, null, "btn icon save success");
      
    echo '</form>';  			 
  echo '</section>'; 
      
echo '</article>';