<?php

    return [
        "sendEmail" => [
            "path" => "/emailTemplates/sendEmail",
            "method" => "POST",
            "authorized" => false,
            "cache" => false,
            "body" => [
                "key" => [
                    "required" => true,
                    "default" => Null,
                    "type" => 'String',
                    "description" => "Email key for specific the email template",
                ],
                "formData" => [
                    "required" => true,
                    "default" => [],
                    "type" => 'Array',
                    "description" => "Custom Array of form data",
                ],
                "formFiles" => [
                    "required" => false,
                    "default" => [],
                    "type" => 'Array',
                    "description" => "Array of uploaded files",
                    "values" => [
                        "item" => [
                            "required" => false,
                            "default" => [],
                            "type" => 'Array',
                            "description" => "Array of uploaded items",
                            "values" => [
                                "name" => [
                                    "required" => true,
                                    "default" => Null,
                                    "type" => 'String',
                                    "description" => "Filename",
                                ],
                                "data" => [
                                    "required" => true,
                                    "default" => Null,
                                    "type" => 'String',
                                    "description" => "Base 64 encoded data",
                                ],
                            ]
                        ],
                    ]
                ],
                "senderCopy" => [
                    "required" => false,
                    "default" => Null,
                    "type" => 'String',
                    "description" => "Email address where the copy of the message will be sent",
                ],
            ],
        ],
    ];