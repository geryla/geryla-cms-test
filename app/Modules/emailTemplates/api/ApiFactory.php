<?php
namespace Api\emailTemplates;

class ApiFactory extends \ApiController{

    private $requestMethods;
    private $errorCodes = [
        200 => 'The email was sent successfully',
        400 => 'Email key is missing',
        401 => 'Email data is missing',
        402 => 'Failed to send email',
    ];

    public function __construct(\Database $database, \AppController $appController, Array $requestData) {
        parent::__construct($database, $appController, $requestData);
        $this->requestMethods = include(__DIR__.'/requests.php');
    }

    public function getErrorCodes(){
        return $this->returnErrorCodes($this->errorCodes);
    }
    public function validate(){
        return $this->validateRequest($this->requestData, $this->requestMethods);
    }
    public function proceedRequest() {

        switch($this->requestData["action"]){
            case "sendEmail":
                $this->sendEmail();
                break;
            default:
                $this->setState(310);
                return false;
        }

        $this->closeAuthRequest();
        return true;
    }

    private function sendEmail(){
        if(!isExisting('key', $this->requestBodyValidated)){
            $this->setState(400);
            return false;
        }
        if(!isExisting('formData', $this->requestBodyValidated)){
            $this->setState(401);
            return false;
        }

        $uploadedFiles = [];
        if(!empty($this->requestBodyValidated["formFiles"])){
            foreach($this->requestBodyValidated["formFiles"] as $fileData){
                $extension = pathinfo($fileData['name'], PATHINFO_EXTENSION);
                $tmpName = sys_get_temp_dir() . '/' . uniqid('uploaded_file_') . '.' . $extension;

                list(, $fileData['data']) = explode(';', $fileData['data']); // Remove headers
                list(, $fileData['data']) = explode(',', $fileData['data']); // Remove headers
                $decodedData = base64_decode($fileData['data']);
                file_put_contents($tmpName, $decodedData);

                $uploadedFiles[] = [
                    'tmp_name' => $tmpName,
                    'name' => $fileData['name'],
                ];
            }
        }

        $options = [];
        if(isset($this->requestBodyValidated["senderCopy"]) && $this->requestBodyValidated["senderCopy"] !== null){
            $options["senderCopy"] = $this->requestBodyValidated["senderCopy"];
        }

        $emailTrigger = new \EmailTrigger($this->database, $this->appController);
        $success = $emailTrigger->triggerEmail($this->requestBodyValidated["key"], $this->requestBodyValidated["formData"], $uploadedFiles, $options);

        if($success === false){
            $this->setState(402, $this->errorCodes[402].': '.$emailTrigger->errorInfo);
            return false;
        }

        $this->setResponseData([]);
        return true;
    }
}