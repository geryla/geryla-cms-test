SET FOREIGN_KEY_CHECKS=0;

-- DELETE TABLES
DROP TABLE `emailtemplates`;

-- DELETE module SETTINGS
DELETE FROM `settings` WHERE moduleName = 'emailTemplates';

-- DELETE ALL TRANSLATION by translateType from module CONFIG
DELETE FROM `language-translations` WHERE objectType = 'emailTemplates';

SET FOREIGN_KEY_CHECKS=1;