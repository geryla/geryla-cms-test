-- CREATE TABLE for EMAIL TEMPLATES
CREATE TABLE IF NOT EXISTS `emailtemplates` (
  `id` int(10) NOT NULL AUTO_INCREMENT,

  `title` varchar(75) NOT NULL,
  `emailSubject` varchar(255) NOT NULL,
  `emailContent` text NOT NULL,
  `emailVariables` varchar(255) NOT NULL,
  `emailKey` varchar(75) NOT NULL,
  `moduleNames` varchar(255) NOT NULL,

  `createdDate` datetime NOT NULL,
  `updatedDate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- INSERT MODULE SETTINGS into TABLE 'settings'
INSERT INTO `settings` (`settName`, `settValue`, `moduleName`, `settGroup`) VALUES
  ('emailSettings-emailName', '', 'emailTemplates', ''),
  ('emailSettings-emailUser', '', 'emailTemplates', ''),
  ('emailSettings-encoding', 'UTF-8', 'emailTemplates', ''),
  ('emailSettings-method', 'mail', 'emailTemplates', ''),
  ('emailSettings-smtp-server', '', 'emailTemplates', 'smtp'),
  ('emailSettings-smtp-port', '', 'emailTemplates', 'smtp'),
  ('emailSettings-smtp-protocol', 'tls', 'emailTemplates', 'smtp'),
  ('emailSettings-smtp-password', '', 'emailTemplates', 'smtp');