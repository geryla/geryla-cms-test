<?php
$lang = array();
$lang["emailTemplates"] = 'E-mailing';

$lang["email-templates"] = 'Emailové šablony';
$lang["email-detail"] = 'Detail emailu';
$lang["email-settings"] = 'Nastavení emailů';
$lang["email-messages"] = 'Přijaté emaily';


$lang["mod-emailTemplates-all_posts"] = 'Seznam šablon';
$lang["mod-emailTemplates-add-post"] = 'Přidat šablonu';
$lang["mod-emailTemplates-edit-post"] = 'Upravit šablonu';

$lang["mod-emailTemplates-ad-table-title"] = "Název";
$lang["mod-emailTemplates-ad-table-moduleOnly"] = "Typ";
$lang["mod-emailTemplates-ad-table-moduleOnly-true"] = "Systémový email";
$lang["mod-emailTemplates-ad-table-moduleOnly-false"] = "Vlastní email";

$lang["mod-emailTemplates-ad-title"] = 'Název emailu <i>(pouze pro administraci)</i>';
$lang["mod-emailTemplates-ad-subject"] = 'Předmět emailu';
$lang["mod-emailTemplates-ad-content"] = 'Obsah emailu'; 
$lang["mod-emailTemplates-ad-key"] = 'Jedinečný identifikátor <i>(bez diakritiky a mezer)</i>';

$lang["mod-emailTemplates-ad-variables"] = 'Proměnné pro email';
$lang["mod-emailTemplates-ad-variables-help"] = 'Při odeslání emailu dojde automaticky k nahrazení proměnné reálnou hodnotou z formuláře nebo nastavení.';
$lang["mod-emailTemplates-ad-variables-none"] = 'Pro využívání je potřeba přiřadit email k některému z kontaktních formulářů.';

$lang["mod-emailTemplates-ad-recipient"] = 'Systémový příjemce <i>(volitelné)</i>';
$lang["mod-emailTemplates-ad-recipient-help"] = 'Neni-li vyplněno, použije se výchozí nastavení';

$lang["mod-emailTemplates-settings-sending"] = "Nastavení odesílání";

$lang["mod-emailTemplates-settings-sending-basicSettings"] = "Základní nastavení";
$lang["mod-emailTemplates-settings-sending-emailName"] = "Název odesílatele";
$lang["mod-emailTemplates-settings-sending-emailName-help"] = "Bude zobrazeno u odeslaného emailu";    
$lang["mod-emailTemplates-settings-sending-emailUser"] = "Platná e-mailová adresa";
$lang["mod-emailTemplates-settings-sending-encoding"] = "Typ kódování <i>(výchozí UTF-8)</i>";
$lang["mod-emailTemplates-settings-sending-encoding-help"] = "Pokud si nejste jistí, ponechte výchozí nastavení";
$lang["mod-emailTemplates-settings-sending-method"] = "Metoda";
$lang["mod-emailTemplates-settings-sending-method-help"] = "Dostupnost metody SMTP je závislá na nastavení serveru";

$lang["mod-emailTemplates-settings-sending-method-smtp"] = "SMTP";
$lang["mod-emailTemplates-settings-sending-method-smtp-help"] = "Odesílání skrze SMTP vyžaduje platnou emailovou adresu a heslo přes které bude probíhat zabezpečená komunikace.";   
$lang["mod-emailTemplates-settings-sending-method-smtp-server"] = "Server";
$lang["mod-emailTemplates-settings-sending-method-smtp-user"] = "E-mail";
$lang["mod-emailTemplates-settings-sending-method-smtp-user-inherit"] = "Bude použit email zvolený pro metodu MAIL";
$lang["mod-emailTemplates-settings-sending-method-smtp-password"] = "Heslo";
$lang["mod-emailTemplates-settings-sending-method-smtp-password-help"] = "Heslo k emailu bude šifrováno";
$lang["mod-emailTemplates-settings-sending-method-smtp-email"] = "E-mail";
$lang["mod-emailTemplates-settings-sending-method-smtp-email-help"] = "E-mailová adresa";
$lang["mod-emailTemplates-settings-sending-method-smtp-port"] = "Port";
$lang["mod-emailTemplates-settings-sending-method-smtp-protocol"] = "Protokol";
$lang["mod-emailTemplates-settings-sending-method-smtp-protocol-help"] = "Typ protokolu pro uvedený port";        
$lang["mod-emailTemplates-settings-sending-method-smtp-protocol-tls"] = "TLS";
$lang["mod-emailTemplates-settings-sending-method-smtp-protocol-ssl"] = "SSL";
$lang["mod-emailTemplates-settings-SMTPConnection-title"] = "Kontrola SMTP spojení <i>(je-li aktivní)</i>";
$lang["mod-emailTemplates-settings-SMTPConnection-connected"] = "Úspěšně spojeno";
$lang["mod-emailTemplates-settings-SMTPConnection-failed"] = "Chyba spojení";
$lang["mod-emailTemplates-settings-SMTPConnection-not-active"] = "Není aktivní";
$lang["mod-emailTemplates-settings-SMTPConnection-error"] = "";

$lang["mod-emailTemplates-settings-sending-method-mail"] = "MAIL";
$lang["mod-emailTemplates-settings-sending-method-mail-help"] = "Odesílání metodou MAIL() nevyžaduje zabezpečené připojení, je však náchylnější na SPAMy (nevyžádaná pošta).";   
$lang["mod-emailTemplates-settings-sending-method-mail-settings"] = "Nastavení";
$lang["mod-emailTemplates-settings-sending-method-mail-settings-none"] = "Není potřeba rozšířené nastavení";

// RETURN MESSAGES
$lang["mod-emailTemplates-returnMessage-true"] = "Váš email byl úspěšně odeslán. Děkujeme.";
$lang["mod-emailTemplates-returnMessage-false"] = "Nastala chyba při odesílání emailu. Omlouváme se.";

// EMAIL TEMPLATES PLACEHOLDERS
$lang["mod-emailTemplates-emailTemplate-userInfo"] = 'Dodací adresa';
$lang["mod-emailTemplates-emailTemplate-companyInfo"] = 'Firma';
$lang["mod-emailTemplates-emailTemplate-delivery"] = 'Dopravce';
$lang["mod-emailTemplates-emailTemplate-payment"] = 'Platební metoda';
$lang["mod-emailTemplates-emailTemplate-pcs"] = 'ks';
$lang["mod-emailTemplates-emailTemplate-discount"] = 'Sleva za zboží';
$lang["mod-emailTemplates-emailTemplate-phone"] = 'Telefon'; 
$lang["mod-emailTemplates-emailTemplate-email"] = 'Email';
$lang["mod-emailTemplates-emailTemplate-identNum"] = 'IČ';
$lang["mod-emailTemplates-emailTemplate-vatNum"] = 'DIČ';
$lang["mod-emailTemplates-emailTemplate-taxNum"] = 'IČ DPH';

$lang["mod-emailTemplates-emailTemplate-product-name"] = 'Název';
$lang["mod-emailTemplates-emailTemplate-product-quantity"] = 'Počet kusů';
$lang["mod-emailTemplates-emailTemplate-product-priceSingle"] = 'Jednotná cena';
$lang["mod-emailTemplates-emailTemplate-product-priceTotal"] = 'Celková cena';

$lang["mod-emailTemplates-messages-templateName"] = 'Emailová šablona';
$lang["mod-emailTemplates-messages-sender"] = 'Odesílatel';
$lang["mod-emailTemplates-messages-createdDate"] = 'Datum';
$lang["mod-emailTemplates-messages-settlementDate"] = 'Vyřízeno';
$lang["mod-emailTemplates-messages-settleBtn"] = 'Vyřídit';
$lang["mod-emailTemplates-messages-yes"] = 'Ano';
$lang["mod-emailTemplates-messages-no"] = 'Ne';
$lang["mod-emailTemplates-messages-body"] = 'Obsah zprávy';
$lang["mod-emailTemplates-messages-list"] = 'Seznam zpráv';
$lang["mod-emailTemplates-messages-show"] = 'Detail zprávy';
$lang["mod-emailTemplates-messages-attachments"] = 'Přílohy';
$lang["mod-emailTemplates-messages-filtration-sort-key"] = 'Emailová šablona';