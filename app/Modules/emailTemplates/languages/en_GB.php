<?php
$lang = array();
$lang["emailTemplates"] = 'Emailing';

$lang["email-templates"] = 'Email Templates';
$lang["email-detail"] = 'Email detail';
$lang["email-settings"] = 'Email Settings';
$lang["email-messages"] = 'Received emails';


$lang["mod-emailTemplates-all_posts"] = 'List of templates';
$lang["mod-emailTemplates-add-post"] = 'Add Template';
$lang["mod-emailTemplates-edit-post"] = 'Edit Template';

$lang["mod-emailTemplates-ad-table-title"] = "Title";
$lang["mod-emailTemplates-ad-table-moduleOnly"] = "Type";
$lang["mod-emailTemplates-ad-table-moduleOnly-true"] = "System email";
$lang["mod-emailTemplates-ad-table-moduleOnly-false"] = "Custom email";

$lang["mod-emailTemplates-ad-title"] = 'Email title <i>(admin only)</i>';
$lang["mod-emailTemplates-ad-subject"] = 'Email Subject';
$lang["mod-emailTemplates-ad-content"] = 'Email Content';
$lang["mod-emailTemplates-ad-key"] = 'Unique identifier <i>(without diacritics and spaces)</i>';

$lang["mod-emailTemplates-ad-variables"] = 'Variables for email';
$lang["mod-emailTemplates-ad-variables-help"] = 'When sending an email, the variable will be automatically replaced with a real value from the form or settings.';
$lang["mod-emailTemplates-ad-variables-none"] = 'For use, you need to assign an email to one of the contact forms.';

$lang["mod-emailTemplates-ad-recipient"] = 'System recipient <i>(optional)</i>';
$lang["mod-emailTemplates-ad-recipient-help"] = 'If not filled, default will be used';

$lang["mod-emailTemplates-settings-sending"] = "Sending Settings";

$lang["mod-emailTemplates-settings-sending-basicSettings"] = "Basic Settings";
$lang["mod-emailTemplates-settings-sending-emailName"] = "Sender Name";
$lang["mod-emailTemplates-settings-sending-emailName-help"] = "Will be displayed on sent email";
$lang["mod-emailTemplates-settings-sending-emailUser"] = "Valid email address";
$lang["mod-emailTemplates-settings-sending-encoding"] = "Encoding type <i>(default UTF-8)</i>";
$lang["mod-emailTemplates-settings-sending-encoding-help"] = "If unsure, leave default settings";
$lang["mod-emailTemplates-settings-sending-method"] = "Method";
$lang["mod-emailTemplates-settings-sending-method-help"] = "The availability of the SMTP method depends on the server settings";

$lang["mod-emailTemplates-settings-sending-method-smtp"] = "SMTP";
$lang["mod-emailTemplates-settings-sending-method-smtp-help"] = "Sending via SMTP requires a valid email address and password for secure communication.";
$lang["mod-emailTemplates-settings-sending-method-smtp-server"] = "Server";
$lang["mod-emailTemplates-settings-sending-method-smtp-user"] = "Email";
$lang["mod-emailTemplates-settings-sending-method-smtp-user-inherit"] = "The email selected for the MAIL method will be used";
$lang["mod-emailTemplates-settings-sending-method-smtp-password"] = "Password";
$lang["mod-emailTemplates-settings-sending-method-smtp-password-help"] = "Email password will be encrypted";
$lang["mod-emailTemplates-settings-sending-method-smtp-email"] = "Email";
$lang["mod-emailTemplates-settings-sending-method-smtp-email-help"] = "Email Address";
$lang["mod-emailTemplates-settings-sending-method-smtp-port"] = "Port";
$lang["mod-emailTemplates-settings-sending-method-smtp-protocol"] = "Protocol";
$lang["mod-emailTemplates-settings-sending-method-smtp-protocol-help"] = "Protocol type for the specified port";
$lang["mod-emailTemplates-settings-sending-method-smtp-protocol-tls"] = "TLS";
$lang["mod-emailTemplates-settings-sending-method-smtp-protocol-ssl"] = "SSL";
$lang["mod-emailTemplates-settings-SMTPConnection-title"] = "Check SMTP connection <i>(if active)</i>";
$lang["mod-emailTemplates-settings-SMTPConnection-connected"] = "Successfully connected";
$lang["mod-emailTemplates-settings-SMTPConnection-failed"] = "Connection failed";
$lang["mod-emailTemplates-settings-SMTPConnection-not-active"] = "Not active";
$lang["mod-emailTemplates-settings-SMTPConnection-error"] = "";

$lang["mod-emailTemplates-settings-sending-method-mail"] = "MAIL";
$lang["mod-emailTemplates-settings-sending-method-mail-help"] = "Sending with the MAIL() method does not require a secure connection, but is more prone to SPAM.";
$lang["mod-emailTemplates-settings-sending-method-mail-settings"] = "Settings";
$lang["mod-emailTemplates-settings-sending-method-mail-settings-none"] = "No advanced settings needed";

// RETURN MESSAGES
$lang["mod-emailTemplates-returnMessage-true"] = "Your email was sent successfully. Thank you.";
$lang["mod-emailTemplates-returnMessage-false"] = "There was an error sending the email. We are sorry.";

// EMAIL TEMPLATES PLACEHOLDERS
$lang["mod-emailTemplates-emailTemplate-userInfo"] = 'Shipping Address';
$lang["mod-emailTemplates-emailTemplate-companyInfo"] = 'Company';
$lang["mod-emailTemplates-emailTemplate-delivery"] = 'Shipper';
$lang["mod-emailTemplates-emailTemplate-payment"] = 'Payment method';
$lang["mod-emailTemplates-emailTemplate-pcs"] = 'pcs';
$lang["mod-emailTemplates-emailTemplate-discount"] = 'Item Discount';
$lang["mod-emailTemplates-emailTemplate-phone"] = 'Phone';
$lang["mod-emailTemplates-emailTemplate-email"] = 'Email';
$lang["mod-emailTemplates-emailTemplate-identNum"] = 'ID';
$lang["mod-emailTemplates-emailTemplate-vatNum"] = 'VAT';
$lang["mod-emailTemplates-emailTemplate-taxNum"] = 'VAT ID';

$lang["mod-emailTemplates-emailTemplate-product-name"] = 'Name';
$lang["mod-emailTemplates-emailTemplate-product-quantity"] = 'Number of pieces';
$lang["mod-emailTemplates-emailTemplate-product-priceSingle"] = 'Single Price';
$lang["mod-emailTemplates-emailTemplate-product-priceTotal"] = 'Total Price';

$lang["mod-emailTemplates-messages-templateName"] = 'Email Template';
$lang["mod-emailTemplates-messages-sender"] = 'Sender';
$lang["mod-emailTemplates-messages-createdDate"] = 'Date';
$lang["mod-emailTemplates-messages-settlementDate"] = 'Settled';
$lang["mod-emailTemplates-messages-settleBtn"] = 'Settle';
$lang["mod-emailTemplates-messages-yes"] = 'Yes';
$lang["mod-emailTemplates-messages-no"] = 'No';
$lang["mod-emailTemplates-messages-body"] = 'Message body';
$lang["mod-emailTemplates-messages-list"] = 'Message List';
$lang["mod-emailTemplates-messages-show"] = 'Message detail';
$lang["mod-emailTemplates-messages-attachments"] = 'Attachments';
$lang["mod-emailTemplates-messages-filtration-sort-key"] = 'Email Template';