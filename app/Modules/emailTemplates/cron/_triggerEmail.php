<?php
//INPUT: $jobInfo, $parameters

if(isset($parameters)){
    $emailObject = new emailRunner($database, $appController);
    $emailLanguage = ($parameters["emailData"]["langShort"]) ?? $appController->getLang();
    $emailRecipient = ($parameters["recipient"]) ?? null;
    $customContent = ($parameters["customContent"]) ?? null;

    $emailObject->createEmail($emailLanguage);

    if(!empty($customContent)){
        $emailObject->customContent((($parameters["subject"]) ?? 'Email message'), $customContent);
    }else{
        $emailObject->reParseEmailKey($parameters["emailKey"]);
        $emailObject->content($parameters["emailData"], $parameters["optionalData"]);
    }

    $emailObject->address($emailRecipient);
    if(isset($parameters["attachment"]) && !empty($parameters["attachment"])){
        $emailObject->attachment($parameters["attachment"]);
    }

    $isSend = $emailObject->send();
    return ($isSend === true) ? true : false;
}

return false;