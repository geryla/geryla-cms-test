<?php
// CONTROL - LEVEL 0 - CHECK if some POST request with submit is ON AIR
if( isset($_POST) && isset($_POST["submit"]) ){

    // INITIALIZE basic variables
    $postAjax = false;
    $success = false;

    // PARSE POST PARAMS - and if email was POST by AJAX, re-parse it too
    if( isset($_POST["ajax"]) && $_POST["ajax"] == "true" ){
        $postAjax = true;
        session_start();
        include($_SESSION["const"]["AJAX_DEFINE"]);
        parse_str($_POST['data'], $formData);
        $returnMessage = "error";
    }else{
        $formData = $_POST;
        $returnMessage = translate("mod-emailTemplates-returnMessage-false");
        $returnType = "error";
    }
    unset($_POST);
    unset($formData["submit"]);

    if(!empty($formData)){
        $emailTrigger = new EmailTrigger($database, $appController);
        $emailKey = $formData["key"]; unset($formData["key"]);

        $success = $emailTrigger->triggerEmail($emailKey, $formData, $_FILES);

        if($success === true){
            $returnMessage = translate("mod-emailTemplates-returnMessage-true");
            $returnType = "success";
        }

        $systemLogger->addNotification($returnMessage, $returnType);

        unset($formData, $emailKey, $success);

        if($postAjax === true){
            echo $returnMessage;
        }
    }

    unset($postAjax);
}