$(window).load(function() {

/* ===== ADD value from label to tinyMce editor to active row === */
  $(document).on('click', '.groupedHelpers label', function(e) {   
    var insertData = $(this).attr('data');
    tinymce.activeEditor.execCommand('mceInsertContent', false, insertData);
  });
  
});