<?php
    $config = [
        "moduleName" => "Filtration",
        "moduleUrlName" => "filtration",
        "moduleLangName" => "filtration",
        "moduleIcon" => "fa-filter",
        "moduleVersion" => "3.5.2",
        "requiredModules" => [
            ["moduleName" => "templates"]
        ],

        "mediaSettings" => [],
        "translateType" => [],

        "modulePages" => []
    ];