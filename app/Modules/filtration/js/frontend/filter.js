/* PARAMETERS in URL - query ADD */
    function addParameter(url, parameterName, parameterValue, atStart){
    replaceDuplicates = true;
    if(url.indexOf('#') > 0){
        var cl = url.indexOf('#');
        urlhash = url.substring(url.indexOf('#'),url.length);
    } else {
        urlhash = '';
        cl = url.length;
    }
    sourceUrl = url.substring(0,cl);

    var urlParts = sourceUrl.split("?");
    var newQueryString = "";

    if (urlParts.length > 1)
    {
        var parameters = urlParts[1].split("&");
        for (var i=0; (i < parameters.length); i++)
        {
            var parameterParts = parameters[i].split("=");
            if (!(replaceDuplicates && parameterParts[0] == parameterName))
            {
                if (newQueryString == "")
                    newQueryString = "?";
                else
                    newQueryString += "&";
                newQueryString += parameterParts[0] + "=" + (parameterParts[1]?parameterParts[1]:'');
            }
        }
    }
    if (newQueryString == "")
        newQueryString = "?";

    if(atStart){
        newQueryString = '?'+ parameterName + "=" + parameterValue + (newQueryString.length>1?'&'+newQueryString.substring(1):'');
    } else {
        if (newQueryString !== "" && newQueryString != '?')
            newQueryString += "&";
        newQueryString += parameterName + "=" + (parameterValue?parameterValue:'');
    }
    return urlParts[0] + newQueryString + urlhash;
    };
/* PARAMETERS in URL - query REMOVE */
    function removeURLParameter(url, parameter) {
      //prefer to use l.search if you have a location/link object
      var urlparts= url.split('?');
      if (urlparts.length>=2) {

          var prefix= encodeURIComponent(parameter)+'=';
          var pars= urlparts[1].split(/[&;]/g);

          //reverse iteration as may be destructive
          for (var i= pars.length; i-- > 0;) {
              //idiom for string.startsWith
              if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                  pars.splice(i, 1);
              }
          }

          url= urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : "");
          return url;
      } else {
          return url;
      }
    }

/* FILTRATION OVERLAY */
    function callLoader(time){
        time = (time === undefined) ? 3000 : time;
        let overlay = "position: fixed; top: 0; left: 0; width: 100%; height: 100%; background-color: #ffffff; opacity: 0.6; z-index: 9;";
        if(time === true){
            $('<div style="'+overlay+'" class="filter-overlay"><span></span></div>').appendTo('body');
        }else if(time === false){
            $(".filter-overlay").remove();
        }else{
            $('<div style="'+overlay+'" class="filter-overlay"></div>').appendTo('body');
            setTimeout(function(){ $(".filter-overlay").remove(); }, time);
        }
    }

/* PRICE SLIDER */
    function initPriceSlider(){

        if($("#slider-range.priceSlider").length > 0){
            let limitFrom = parseFloat ($("#slider-values").attr("data-limitMin")),
                limitTo = parseFloat ($("#slider-values").attr("data-limitMax")),
                currency = $("#slider-values").attr("data-currency"),
                min = parseFloat ($("#priceMin").val()),
                max = parseFloat ($("#priceMax").val());

            /* INITIALIZE */
            $("#slider-range.priceSlider").slider({
                range: true,
                min: limitFrom,
                max: limitTo,
                values: [min, max],
                slide: function( event, ui ) {
                    $("#priceMin").val(ui.values[0] +' '+currency);
                    $("#priceMax").val(ui.values[1] +' '+currency);
                },
                stop: function( event, ui ) {
                    callLoader(true);
                    var url = window.location.toString();
                    var min = parseFloat ($("#priceMin").val());
                    var min = (!min) ? "0" : min;
                    var max = parseFloat ($("#priceMax").val());
                    url = removeURLParameter(url, 'page');

                    var newUrl = addParameter(url, 'priceMin', min, false);
                    var newUrl = addParameter(newUrl, 'priceMax', max, false);

                    window.location.href = newUrl;
                }
            });

            if($("#slider-range .ui-slider-range").length > 1){
                $("#slider-range .ui-slider-range").not().first().remove();
            }
            $('#slider-range').removeClass('hide');
        }
    }
    initPriceSlider();

/* PARAMETRIC FILTRATION */
    $("#parametricFiltration").on("click", "label", function (e) {
        callLoader(true);
        let url = $(this).closest('label').find("a").attr("href");

        window.history.replaceState(null, null, url);
        document.location.reload(true);
        return false;
    });
/* AJAX PAGINATION */
    $(document).on("click","#loadMoreResults", function(e){
        let dataType = $(this).data('objects'),
            template = $(this).data('template'),
            activeCategory = $(this).data('category'),
            defaultSort = $(this).data('default'),
            requestUrl = $(this).data('path'),
            activePage = $(this).data('page'),
            paginationLimit = $(this).data('limit'),
            labelSort = $(this).data('labelsort'),
            holdAvailability = $(this).data('holdavailability');
        callLoader(true);

        $.ajax({
            url: '/',
            type: 'POST',
            data: {task: 'loadDataToPage', module: 'filtration', noVerify: true, response: 'json', data: {
                    dataType:dataType,
                    template:template,
                    activeCategory:activeCategory,
                    sorting:{default:defaultSort, byLabels:labelSort, byAvailability:holdAvailability},
                    url:atob(requestUrl),
                    activePage:activePage,
                    limit:paginationLimit,
                    returnTemplate:true
                }},

            success: function(data) {
                let callback = JSON.parse(data);
                $('#loadDataBox').append(callback["data"]);
                $('#pagination').addClass('old');
                var ajaxButton = $('#pagination').find('#loadMoreResults').html();
                $('#pagination.old').after(callback["pagination"]).remove();
                $('#pagination #loadMoreResults').html(ajaxButton);

                var newUrl = addParameter(window.location.toString(), 'page', activePage, false);
                window.history.replaceState(null, null, newUrl);
                callLoader(false);
            }
        });
    });