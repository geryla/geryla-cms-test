<?php
namespace Api\filtration;

class ApiFactory extends \ApiController{
    private $filtrationClass;

    private $requestMethods;
    private $errorCodes = [
        400 => 'Object/s not found',
    ];

    public function __construct(\Database $database, \AppController $appController, Array $requestData) {
        parent::__construct($database, $appController, $requestData);
        $this->filtrationClass = new \filtrationClass($this->database, $this->appController);

        $this->requestMethods = include(__DIR__.'/requests.php');
    }

    public function getErrorCodes(){
        return $this->returnErrorCodes($this->errorCodes);
    }
    public function validate(){
        return $this->validateRequest($this->requestData, $this->requestMethods);
    }
    public function proceedRequest() {

        switch($this->requestData["action"]){
            case "getData":
                $this->getData();
                break;
            case "getFilters":
                $this->getFilters();
                break;
            case "paginate":
                $this->paginate();
                break;
            default:
                $this->setState(310);
                return false;
        }

        $this->closeAuthRequest();
        return true;
    }

    private function getData(){
        $url = $this->requestBodyValidated["url"];
        $queryParams = [];
        parse_str(parse_url($url)['query'], $queryParams);
        $page = (isset($this->requestBodyValidated["page"])) ? $this->requestBodyValidated["page"] : ((isset($queryParams["page"])) ? $queryParams["page"] : 1);

        $filtrationRules = $this->filtrationClass->getRulesForFiltration($this->requestData["value"], $this->requestData["locale"], [
            "requestUrl" => $url,
            "queryParams" => $queryParams,
            "activePage" => $page,
            "sorting" => $this->requestBodyValidated["sorting"],

            "categoryId" => (int) $this->requestBodyValidated["activeCategory"],
            "paginationLimit" => $this->requestBodyValidated["limit"],
            "ajaxPagination" => $this->requestBodyValidated["onClickLoad"],

            "itemThumbnail" => $this->requestParametersValidated["thumbnail"],
            "optimizeParams" => $this->requestParametersValidated["with"],

            "apiRequestUrl" => $this->requestData["requestUrl"]
        ]);

        if(empty($filtrationRules["data"])){
            $this->setState(400);
        }

        if(!empty($this->requestParametersValidated["include"])){
            $filtrationRules["filtration"]["filterValues"] = $this->loadFilters($this->requestData["value"], $this->requestBodyValidated["url"], $this->requestParametersValidated["include"], $this->requestData["locale"], $this->requestBodyValidated["activeCategory"]);
        }

        $this->setResponseData($filtrationRules);
        return true;
    }
    private function getFilters(){
        $filtersGroups = $this->loadFilters($this->requestData["value"], $this->requestBodyValidated["url"], $this->requestBodyValidated["include"], $this->requestData["locale"], $this->requestBodyValidated["activeCategory"]);

        $this->setResponseData($filtersGroups);
        return true;
    }
    private function paginate(){
        $request["data"] = $this->requestBodyValidated;
        $url = $request["data"]["url"];

        parse_str(parse_url($url)['query'], $request["data"]["queryParams"]);
        $request["data"]["url"] = $url;
        $request["data"]["activePage"] = (isset($request["data"]["getParams"]["page"])) ? $request["data"]["getParams"]["page"] + 1 : 1;
        $request["data"]["dataType"] = $this->requestData["value"];
        $request["data"]["langShort"] = $this->requestData["locale"];
        $request["data"]["thumbnail"] = $this->requestParametersValidated["thumbnail"];
        $request["data"]["with"] = $this->requestParametersValidated["with"];

        $paginationResults = include(MODULE_DIR.'filtration/'.REQUESTS.'/loadDataToPage.php');

        $this->setResponseData($paginationResults);
        return true;
    }

    private function loadFilters(String $type, String $url, Array $filterGroups, String $langShort, Int $activeCategoryId){
        parse_str(parse_url($url)['query'], $getParams);
        $filterOptionsParameters = ["categoryId" => $activeCategoryId, "url" => $url, "getParams" => $getParams,];

        return $this->filtrationClass->getFilterOptions($type, $filterGroups, $langShort, $filterOptionsParameters);
    }
}