<?php

    return [
        "getData" => [
            "path" => "/filtration/getData/{value}",
            "method" => "POST",
            "authorized" => true,
            "cache" => false,
            "value" => [
                "required" => true,
                "type" => 'String',
                "description" => "Type of returned results",
                "values" => "products",
            ],
            "params" => [
                "include" => [
                    "required" => false,
                    "default" => [],
                    "type" => 'Array',
                    "description" => "Extend filtration object with an extra data",
                    "values" => "productParameters, productLabels, productManufacturers",
                ],
                "with" => [
                    "required" => false,
                    "default" => [],
                    "type" => 'Array',
                    "description" => "Returns optimized object with specific extensions based on requested type",
                    "values" => "all",
                ],
                "thumbnail" => [
                    "required" => false,
                    "default" => Null,
                    "type" => 'String',
                    "description" => "Returns the specific compressed and resized images"
                ],
            ],
            "body" => [
                "url" => [
                    "required" => true,
                    "default" => "/",
                    "type" => 'String',
                    "description" => "Active page URL, including all parameters for filtration (BASE64 encoded)",
                    "values" => "page, sort, priceMin, priceMax, availability, ..."
                ],
                "activeCategory" => [
                    "required" => false,
                    "default" => 0,
                    "type" => 'Integer',
                    "description" => "Active category ID"
                ],
                "page" => [
                    "required" => false,
                    "default" => 1,
                    "type" => 'Integer',
                    "description" => "Page ID for infinity load",
                ],
                "sorting" => [
                    "required" => false,
                    "default" => [],
                    "type" => 'Array',
                    "values" => [
                        "default" => [
                            "required" => false,
                            "default" => "default",
                            "type" => 'String',
                            "description" => "Default SORT BY method from category settings"
                        ],
                        "options" => [
                            "required" => false,
                            "default" => [],
                            "type" => 'Array',
                            "description" => "List of SORT BY values (custom order for project)",
                            "values" => "default, price-asc, price-desc, name-desc, name-asc"
                        ],

                        "byLabels" => [
                            "required" => false,
                            "default" => [],
                            "type" => 'Array',
                            "description" => "Sort by labels (in specific order)",
                        ],
                        "byAvailability" => [
                            "required" => false,
                            "default" => False,
                            "type" => 'Boolean',
                            "description" => "Sort by availability (isStock first, outOfStock last)"
                        ],
                        "byDiscount" => [
                            "required" => false,
                            "default" => False,
                            "type" => 'Boolean',
                            "description" => "Sort by discount higher then 0"
                        ],
                    ]
                ],
                "limit" => [
                    "required" => false,
                    "default" => Null,
                    "type" => 'Integer',
                    "description" => "Limit of returned items (default 12)"
                ],
                "onClickLoad" => [
                    "required" => false,
                    "default" => False,
                    "type" => 'Boolean',
                    "description" => "If object return on-click next page load data"
                ],
            ],
        ],
        "getFilters" => [
            "path" => "/filtration/getFilters/{value}",
            "method" => "POST",
            "authorized" => true,
            "cache" => false,
            "value" => [
                "required" => true,
                "type" => 'String',
                "description" => "Type of returned results",
                "values" => "products",
            ],
            "body" => [
                "url" => [
                    "required" => true,
                    "default" => "/",
                    "type" => 'String',
                    "description" => "Active page URL, including all parameters for filtration (BASE64 encoded)",
                    "values" => "page, sort, priceMin, priceMax, availability, ..."
                ],
                "include" => [
                    "required" => true,
                    "default" => [],
                    "type" => 'Array',
                    "description" => "Extend filtration object with an extra data",
                    "values" => "productParameters, productLabels, productManufacturers",
                ],
                "activeCategory" => [
                    "required" => false,
                    "default" => 0,
                    "type" => 'Integer',
                    "description" => "Active category ID"
                ],
            ],
        ],
        "paginate" => [
            "path" => "/filtration/paginate/{value}",
            "method" => "POST",
            "authorized" => true,
            "cache" => false,
            "value" => [
                "required" => true,
                "type" => 'String',
                "description" => "Type of returned results",
                "values" => "products",
            ],
            "params" => [
                "with" => [
                    "required" => false,
                    "default" => [],
                    "type" => 'Array',
                    "description" => "Returns optimized object with specific extensions based on requested type",
                    "values" => "all",
                ],
                "thumbnail" => [
                    "required" => false,
                    "default" => Null,
                    "type" => 'String',
                    "description" => "Returns the specific compressed and resized images"
                ],
            ],
            "body" => [
                "url" => [
                    "required" => true,
                    "default" => "/",
                    "type" => 'String',
                    "description" => "Active page URL, including all parameters for filtration (BASE64 encoded)",
                    "values" => "page, sort, priceMin, priceMax, availability, ..."
                ],
                "activeCategory" => [
                    "required" => false,
                    "default" => 0,
                    "type" => 'Integer',
                    "description" => "Active category ID"
                ],
                "limit" => [
                    "required" => false,
                    "default" => Null,
                    "type" => 'Integer',
                    "description" => "Limit of returned items (default 12)"
                ],
                "sorting" => [
                    "required" => false,
                    "default" => [],
                    "type" => 'Array',
                    "values" => [
                        "default" => [
                            "required" => false,
                            "default" => "default",
                            "type" => 'String',
                            "description" => "Default SORT BY method from category settings"
                        ],

                        "byLabels" => [
                            "required" => false,
                            "default" => [],
                            "type" => 'Array',
                            "description" => "Sort by labels (in specific order)",
                        ],
                        "byAvailability" => [
                            "required" => false,
                            "default" => False,
                            "type" => 'Boolean',
                            "description" => "Sort by availability (isStock first, outOfStock last)"
                        ],
                        "byDiscount" => [
                            "required" => false,
                            "default" => False,
                            "type" => 'Boolean',
                            "description" => "Sort by discount higher then 0"
                        ],
                    ]
                ],
            ],
        ],
    ];