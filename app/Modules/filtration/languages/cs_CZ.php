<?php
$lang = array();

$lang["filtration"] = 'Filtrace';

$lang["mod-filtration-frontFilter-prev"] = "Předchozí";
$lang["mod-filtration-frontFilter-next"] = "Další";
$lang["mod-filtration-buttons-filter"] = "Filtrovat";
$lang["mod-filtration-buttons-remove"] = "Zrušit filtr";
$lang["mod-filtration-select-plc"] = "-- Vybrat --";

$lang["mod-filtration-order"] = "Seřadit";
$lang["mod-filtration-order-orderNum-asc"] = "Vzestupně";
$lang["mod-filtration-order-orderNum-desc"] = "Sestupně";
$lang["mod-filtration-order-id-asc"] = "Nejstarší";
$lang["mod-filtration-order-id-desc"] = "Nejnovější";
$lang["mod-filtration-sort"] = "Třídit dle";
$lang["mod-filtration-search"] = "Vyhledat";
$lang["mod-filtration-showLimit"] = "Počet výsledků";
$lang["mod-filtration-showLimit-default"] = "Výchozí";
$lang["mod-filtration-showLimit-all"] = "Všechny";

$lang["mod-filtration-sortByPlc-default"] = "Výchozí";
$lang["mod-filtration-sortByPlc-price-asc"] = "Od nejlevnějšího";
$lang["mod-filtration-sortByPlc-price-desc"] = "Od nejdražšího";
$lang["mod-filtration-sortByPlc-name-asc"] = "Dle názvu A-Z";
$lang["mod-filtration-sortByPlc-name-desc"] = "Dle názvu Z-A";