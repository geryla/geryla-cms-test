<?php
$lang = array();

$lang["filtration"] = 'Filtration';

$lang["mod-filtration-frontFilter-prev"] = "Prev";
$lang["mod-filtration-frontFilter-next"] = "Next";
$lang["mod-filtration-buttons-filter"] = "Filter";
$lang["mod-filtration-buttons-remove"] = "Remove filter";
$lang["mod-filtration-select-plc"] = "-- Select --";

$lang["mod-filtration-order"] = "Sort";
$lang["mod-filtration-order-orderNum-asc"] = "Ascending";
$lang["mod-filtration-order-orderNum-desc"] = "Descending";
$lang["mod-filtration-order-id-asc"] = "Oldest";
$lang["mod-filtration-order-id-desc"] = "Latest";
$lang["mod-filtration-sort"] = "Sort by";
$lang["mod-filtration-search"] = "Search";
$lang["mod-filtration-showLimit"] = "Number of results";
$lang["mod-filtration-showLimit-default"] = "Default";
$lang["mod-filtration-showLimit-all"] = "All";

$lang["mod-filtration-sortByPlc-default"] = "Default";
$lang["mod-filtration-sortByPlc-price-asc"] = "From cheapest";
$lang["mod-filtration-sortByPlc-price-desc"] = "From Most Expensive";
$lang["mod-filtration-sortByPlc-name-asc"] = "By Name A-Z";
$lang["mod-filtration-sortByPlc-name-desc"] = "By Name Z-A";