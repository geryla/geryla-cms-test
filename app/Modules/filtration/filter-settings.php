<?php
/* ===== FUNCTIONS */
$pageStep = 0;
$pageTitle = translate("module_filtr-adminSettings-paramFilter");
$buttonPage = translate("module_filtr-adminSettings-buttons-choose");
$submitName = "submit";

if( isset($_POST) ){

  if( ($_POST["paramCategory"] && $_POST["paramCategory"]!=0) ){
    $_SESSION["filterOption"] = $_POST["paramCategory"];
  } 
  
  if( isset($_POST["back"]) ){
    unset($_SESSION["filterOption"]);
  } 
 
  if( isset($_SESSION["filterOption"]) ){    
    $categoryDetail = $CategoryClass->selectCategory($_SESSION["filterOption"]);
    $categoryOptions = $FilterClass->getFilterOptionsForCategory($_SESSION["filterOption"]);
    
    foreach($FilterClass->filterTypes as $filterName => $filterPage){
      $filterOptions[$filterName] = $categoryOptions[$filterName];
    }
    
    $paramFilterOptions = $FilterClass->getFilterParamsForCategory($categoryOptions["id"]);
    $paramFilterOptions = ($paramFilterOptions) ? $paramFilterOptions : array($FilterClass->paramStructure);
    
    $pageStep = 1;
    $pageTitle .= " ".translate("module_filtr-adminSettings-paramFilter-for").": ".$categoryDetail["titulek"];
    $buttonPage = translate("module_filtr-adminSettings-buttons-save");
    $submitName = "save";
  }
  
  if( isset($_POST["save"]) ){
    $categoryOptions = $FilterClass->getFilterOptionsForCategory($_SESSION["filterOption"]);
    $paramFilterOptions = $FilterClass->getFilterParamsForCategory($categoryOptions["id"]);

    foreach($_POST as $key => $value){
      if($key == "save"){
        continue;
      }elseif( $key == 'paramType' OR $key == 'paramName' OR $key == 'paramIcon' OR $key == 'paramHelp' OR $key == 'paramParent' OR $key == 'new' ){
        $filterParams[$key]= $value;
      }else{
        $filterSettings[$key] = $value;
      }
    }
    
    $filterSettings["categoryId"] = $_SESSION["filterOption"];
    
    if( empty($categoryOptions) ){
      $saving = $FunctionsClass->addToTableArray($FilterClass->headTableName, $filterSettings);
    }else{
      $saving = $FunctionsClass->editInTableArray($FilterClass->headTableName, $filterSettings, $categoryOptions["id"]);
    }
    
    foreach($filterParams["paramName"] as $paramId => $paramValue){  
      
      if(empty($paramValue)){continue;}
      
      $paramArray = array(
        "paramName" => $paramValue,
        "paramType" => $filterParams["paramType"][$paramId], 
        "paramIcon" => $filterParams["paramIcon"][$paramId], 
        "paramHelp" => $filterParams["paramHelp"][$paramId], 
        "paramParent" => $filterParams["paramParent"][$paramId],
        
        "filterId" => $categoryOptions["id"]
      );
          
      if( $filterParams["new"][$paramId] == 0 ){
        $saving = $FunctionsClass->addToTableArray($FilterClass->paramsTableName, $paramArray);
      }else{
        $saving = $FunctionsClass->editInTableArray($FilterClass->paramsTableName, $paramArray, $filterParams["new"][$paramId]);
      }
    } 
 
      
    if($saving === true){           
      $returnMessage = translate("ad_edit_true");
    }else{
      $returnMessage = translate("ad_create_false");
    } 
    
    $this->setContinue(true);
    $this->setMessage($returnMessage);   
  }    
}

$dataCompiler->showMessage();
$dataCompiler->refreshPage($dataCompiler->nextStep, "");

/* ===== PAGE CONTENT */

echo '<div class="row">';
  echo '<div class="col-md-12">';
  	echo '<article class="widget">';
    
  		echo '<header class="widget__header">';
  			echo '<div class="widget__title full">';
  				echo '<h2>'.$pageTitle.'</h2>';          
  			echo '</div>';
  		echo '</header>';
                                            
  		echo '<div class="widget__content formul">';
       echo '<form action="" method="POST">';  	
       
          echo '<div class="module_wrap filterWrap">';    
          
	          // STEP 1) - Choose category    
              if($pageStep == 0){
                require(__DIR__.'/parts/firstStep.php');              
              }
              
            // STEP 2) - Enable/Disable filter options fo category
              if($pageStep == 1){
                require(__DIR__.'/parts/secondStep.php');                 
              }            
                    
            echo '<br />';
            echo ($pageStep == 1) ? '<div><span class="add addParam">'.translate("module_filtr-adminSettings-buttons-addParam").'</span></div>' : '';
            echo '<br />';
          
          echo '</div>'; 
          
        
         echo '<input type="submit" name="'.$submitName.'" class="save" value="'.$buttonPage.'">';
         echo ($pageStep == 1) ? '<input type="submit" name="back" class="save" value="'.translate("module_filtr-adminSettings-buttons-back").'">' : '';
         
       echo '</form>'; 			
  		echo '</div>';   
      
  	echo '</article>';
  echo '</div>';
echo '</div>';

?>