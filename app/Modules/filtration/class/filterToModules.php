<?php
class filterToModules {

    private $database;
    private $appController;
    private $filtrationClass;
    private $localizationClass;

    public $activeFilters;
    public $activePage;
    public $limit;
    public $limitDefault;
    public $start;
    public $offset;
    public $dataCount = 0;
    public $dataCountNoLimit = 0;
    public $enabledPagination = false;

    private $ignoredParams = ["folder" => true, "parentId" => true, "page" => true, "action" => true, "id" => true, "setAdminLang" => true];
    private $enabledLimits = ["all" => 0];
    public $optionsArray = [];
    private $customOrder = [];
    private $indexKeywords = false;

    private $systemSettings = false;

    public function __construct(Database $database, AppController $appController) {
        $this->database = $database;
        $this->appController = $appController;
        $this->localizationClass = $appController->returnLocalizationClass();
        $this->filtrationClass = new filtrationClass($database, $appController);

        $this->systemSettings = $appController->getSettings();
    }
    public function __toString() {
        return get_class($this);
    }

/* ================== INIT & RENDER === */
    public function init($settingsController){
        $this->activePage = (int) $settingsController["activePage"];
        $limit = $settingsController["limit"];
        $moduleLimit = $settingsController["moduleLimit"];
        $limitNum = ($moduleLimit > 0) ? $moduleLimit : $limit;

        $this->limitDefault = $limitNum;
        $this->setLimit($limitNum);
    }
    public function setLimit($limit){
        $this->limit = $limit;
        $this->offset = ($this->limit !== null) ? ( ($this->activePage === 0) ? 0 : (($this->activePage - 1) * $this->limit) ) : null;
    }
    public function indexKeywords($bool = false){
        $this->indexKeywords = $bool;
    }
    public function setSortOptions($optionsArray = []){
        $this->optionsArray = $optionsArray;
    }
    public function addSortOptions($optionsArray){
        if(!empty($optionsArray)){
            $this->optionsArray[] = $optionsArray;
        }
    }
    public function setCustomOrder($optionsArray = []){
        $this->customOrder = $optionsArray;
    }
    public function setSortOrder($orderBy, $orderDirection){
        $this->activeFilters['orderBy'] = $orderBy;
        $this->activeFilters['orderDirection'] = $orderDirection;
    }
    public function countAndControl($dataForFilter, $dataNoLimited = []){
        $this->dataCount = count($dataForFilter);
        $this->dataCountNoLimit = count($dataNoLimited);

        $this->enabledPagination = ($this->limit !== null && $this->dataCount > $this->limit);

        if ($this->limit === null) {
            $this->enabledPagination = false;
        }
    }
    private function checkActiveFilter($getParams){
        $activeParams = $getParams;
        foreach($getParams as $paramKey => $paramValue){
            if(isset($this->ignoredParams[$paramKey]) && !empty($paramValue)){
                unset($activeParams[$paramKey]);
            }
        }

        return (!empty($activeParams)) ? true : false;
    }
    private function checkRequiredParams($getParams, $rules){
        $check = $rules;
        foreach($getParams as $paramKey => $paramValue){
            if(isset($rules[$paramKey]) && !empty($paramValue) && $paramValue == $rules[$paramKey]){
                unset($check[$paramKey]);
            }
        }

        return (empty($check)) ? true : false;
    }
    public function renderFiltration($actualLink, $getParams = [], $disableSearch = false, $simpleOrder = false, $skipResetParams = []){
        global $lang;

        $this->activeFilters = $this->filtrationClass->reParseGetParameters($getParams);
        $removeFilterLink = $this->filtrationClass->easyFilterLink($actualLink, null, null, true);
        if(!empty($skipResetParams)){
            foreach($skipResetParams as $skipParam => $skipValue){
                $removeFilterLink = $this->filtrationClass->easyFilterLink($removeFilterLink, $skipParam, $skipValue);
            }
        }

        if(isset($getParams["pageLimit"]) && $getParams["pageLimit"] != "default"){
            $pageLimitKey = $getParams["pageLimit"];
            if(isset($this->enabledLimits[$pageLimitKey])){
                $pageLimit = ($this->enabledLimits[$pageLimitKey] == 0) ? null : $this->enabledLimits[$pageLimitKey];
                $this->setLimit($pageLimit);
            }
        }

        $filtrationBox = '<form class="filtration" action="" method="GET">';
            $filtrationBox .='<div class="options defaultFilter">';
                $filtrationBox .='<div class="group">';
                    $filtrationBox .='<div class="opt">';
                        $filtrationBox .='<span>'.translate("mod-filtration-showLimit").':</span>';
                        $filtrationBox .='<select name="pageLimit">';
                            $filtrationBox .='<option value="default" '.((!isset($getParams["pageLimit"]) || isset($getParams["pageLimit"]) && $getParams["pageLimit"] == "default") ? "selected" : null).'>'.translate("mod-filtration-showLimit-default").' ('.$this->limitDefault.')</option>';
                            $filtrationBox .='<option value="all" '.((isset($getParams["pageLimit"]) && $getParams["pageLimit"] == "all") ? "selected" : null).'>'.translate("mod-filtration-showLimit-all").'</option>';
                        $filtrationBox .='</select>';
                    $filtrationBox .='</div>';
                    if(empty($this->customOrder)){
                        $filtrationBox .= '
                            <div class="opt">
                                <span>'.translate("mod-filtration-order").':</span>
                                <select name="order">
                                    '.(($simpleOrder === false) ? '<option value="orderNum_desc" '.((isset($this->activeFilters["parameters"]['order']) && $this->activeFilters["parameters"]['order'] === 'orderNum_desc') ? "selected" : null).'>'.translate("mod-filtration-order-orderNum-desc").'</option>' : null).'
                                    '.(($simpleOrder === false) ? '<option value="orderNum_asc" '.((isset($this->activeFilters["parameters"]['order']) && $this->activeFilters["parameters"]['order'] === 'orderNum_asc') ? "selected" : null).'>'.translate("mod-filtration-order-orderNum-asc").'</option>' : null).'
                                    <option value="id_desc" '.((isset($this->activeFilters["parameters"]['order']) && $this->activeFilters["parameters"]['order'] === 'id_desc') ? "selected" : null).'>'.translate("mod-filtration-order-id-desc").'</option>
                                    <option value="id_asc" '.((isset($this->activeFilters["parameters"]['order']) && $this->activeFilters["parameters"]['order'] === 'id_asc') ? "selected" : null).'>'.translate("mod-filtration-order-id-asc").'</option>
                                </select>
                            </div>
                        ';
                    }else{
                        $filtrationBox .='<div class="opt">';
                            $filtrationBox .='<span>'.$this->customOrder["title"].':</span>';
                            $filtrationBox .='<select name="'.$this->customOrder["name"].'">';
                                foreach($this->customOrder["values"] as $custom){
                                    $filtrationBox .='<option value="'.$custom["value"].'" '.((isset($this->activeFilters["parameters"][$this->customOrder["name"]]) && $this->activeFilters["parameters"][$this->customOrder["name"]] === $custom["value"]) ? "selected" : null).'>'.$custom["title"].'</option>';
                                }
                            $filtrationBox .='</select>';
                        $filtrationBox .='</div>';
                    }
                $filtrationBox .='</div>';
                    $filtrationBox .= '<div class="group action">';
                        $filtrationBox .= '<div class="opt">';
                            $filtrationBox .= '<button class="btn icon refresh success inverted">'.translate("mod-filtration-buttons-filter").'</button>';
                            $filtrationBox .= ($this->checkActiveFilter($getParams)) ? '<a href="'.$removeFilterLink.'" class="btn icon cancel error inverted">'.translate("mod-filtration-buttons-remove").'</a>': null;
                        $filtrationBox .= '</div>';
                    $filtrationBox .= '</div>';
                $filtrationBox .= '</div>';
                $filtrationBox .= '<div class="options advancedFiltration">';
                    $filtrationBox .='<div class="group">';
                        if(!empty($this->optionsArray)){
                            foreach($this->optionsArray as $sort){
                                if(!isset($sort["requiredParams"]) OR $this->checkRequiredParams($getParams, $sort["requiredParams"]) === true){
                                    if($sort['type'] !== 'hidden'){
                                        $filtrationBox .= '<div class="opt">';
                                        $filtrationBox .= '<span>'.$sort['title'].':</span>';
                                    }

                                    switch($sort['type']){
                                        case "select":
                                            $filtrationBox .= '<select name="'.$sort["name"].'">';
                                            $selectedValueDefault = (!isset($this->activeFilters["parameters"][$sort["name"]]) OR $this->activeFilters["parameters"][$sort["name"]] == "") ? "selected" : null;
                                            $filtrationBox .= '<option value="" '.$selectedValueDefault.'>'.translate("mod-filtration-select-plc").'</option>';
                                            if(!empty($sort["values"])){
                                                foreach($sort["values"] as $value){
                                                    if($value !== null){
                                                        $selectedValue = (isset($this->activeFilters["parameters"][$sort["name"]]) && $this->activeFilters["parameters"][$sort["name"]] == $value["value"]) ? "selected" : null;
                                                        $filtrationBox .= '<option value="'.$value["value"].'" '.$selectedValue.'>'.$value["title"].'</option>';
                                                    }
                                                }
                                            }
                                            $filtrationBox .= '</select>';
                                            break;
                                        case "date":
                                            $dateValue = (isset($this->activeFilters["parameters"][$sort["name"]]) && $this->activeFilters["parameters"][$sort["name"]] !== '0000-00-00 00:00') ? date('d.m.Y H:i', strtotime($this->activeFilters["parameters"][$sort["name"]])) : '0000-00-00 00:00';

                                            $filtrationBox .= '<input type="text" name="'.$sort["name"].'" value="'.$dateValue.'" class="column labelDate datepicker" />';
                                            break;

                                        case "dateSimple":
                                            $dateValue = (isset($this->activeFilters["parameters"][$sort["name"]]) && $this->activeFilters["parameters"][$sort["name"]] !== '0000-00-00') ? date('d.m.Y', strtotime($this->activeFilters["parameters"][$sort["name"]])) : '0000-00-00';

                                            $filtrationBox .= '<input type="text" name="'.$sort["name"].'" value="'.$dateValue.'" class="column labelDate datepickersimple" />';
                                            break;
                                        case "hidden":
                                            $filtrationBox .= '<input type="hidden" name="'.$sort["name"].'" value="'.$sort["value"].'" class="column" />';
                                            break;
                                    }

                                    if($sort['type'] !== 'hidden'){
                                        $filtrationBox .= '</div>';
                                    }
                                }
                            }
                        }
                        if($disableSearch === false){
                            $filtrationBox .= '
                                <div class="opt">
                                    <span>'.translate("mod-filtration-search").':</span>
                                    <input type="text" name="search" value="'.((isset($this->activeFilters["parameters"]['search'])) ? $this->activeFilters["parameters"]['search'] : null).'"/>
                                </div>
                            ';
                        }
                        if($getParams){
                            foreach($getParams as $name => $value) {
                                if(isset($this->ignoredParams[$name])){
                                    if($name != 'page'){
                                        $filtrationBox .= '<input type="hidden" name="'.htmlspecialchars($name).'" value="'.htmlspecialchars($value).'">';
                                        $removeFilterLink = $this->filtrationClass->easyFilterLink($removeFilterLink, htmlspecialchars($name), htmlspecialchars($value), false);
                                    }
                                }
                            }
                        }
                    $filtrationBox .= '</div>';
            $filtrationBox .= '</div>';
        $filtrationBox .= '</form>';

        if(isset($this->activeFilters["parameters"]['order'])){
            $orderBy = explode("_", $this->activeFilters["parameters"]['order']);
            unset($this->activeFilters["parameters"]['order']);
            $this->setSortOrder($orderBy[0], $orderBy[1]);
        }

        echo $filtrationBox;
    }

/* ================== PRODUCTS - Products === */
    public function getAll_Products_Where($langShort = null, $categoryId = 0, $activeFilters = [], $visibility = null, $limit = null, $offset = null, $orderBy = null, $orderDirection = null) {
        $dataTable = databaseTables::getTable("products", "productTable");
        $translateTable = databaseTables::getTable("products", "productTableLang");
        $whereCond = $whereBind = $langCheck = $langColumns = null;

        $sqlRows = "product.*";
        $sqlQuery = "{$langColumns} FROM `{$dataTable}` as product";

        if ($langShort !== null) {
            $sqlRows .= ", lang.title, lang.shortDesc, lang.description, lang.metaKeywords, lang.metaTitle, lang.metaDescription, lang.url, lang.langShort, lang.youtubeUrl";
            $sqlQuery .= " INNER JOIN `{$translateTable}` as lang ON product.id = lang.associatedId";
            $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
            $whereCond .= "lang.langShort = :langShort";
            $whereBind["langShort"] = $langShort;
        }
        if ($categoryId !== false && $categoryId != 0) {
            $categoryHookTable = databaseTables::getTable("products", "crossCategoryTable");
            $sqlQuery .= " INNER JOIN `{$categoryHookTable}` as categoryHook ON product.id = categoryHook.productId";

            $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
            $whereCond .= "categoryHook.categoryId = :categoryId";
            $whereBind["categoryId"] = $categoryId;
        }
        if (!empty($activeFilters)) {
            if (isset($activeFilters["parameters"]["label"])) {
                $labelTable = databaseTables::getTable("products", "labelTable");
                $labelHookTable = databaseTables::getTable("products", "crossLabelTable");
                $sqlQuery .= " INNER JOIN `{$labelHookTable}` as labelHook ON product.id = labelHook.productId INNER JOIN `{$labelTable}` as label ON labelHook.labelId = label.id";
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $paramWhere = null;

                foreach ($activeFilters["parameters"]["label"] as $key => $label) {
                    $bindKey = str_replace("-", "", $key);
                    $paramWhere .= ($paramWhere === null) ? "(" : " OR ";
                    $paramWhere .= "label.shortTag = :lab{$bindKey}";
                    $whereBind["lab{$bindKey}"] = $key;
                }
                $whereCond .= $paramWhere.")";
            }
            if (isset($activeFilters["parameters"]["parameter"])) {
                $crossParamTable = databaseTables::getTable("products", "crossParamTable");

                $paramSubCondition = [];
                $paramBindValues = [];
                $paramBindCount = 0;
                $paramColorColumn = ($this->systemSettings["groupParamsColors"] === "true") ? 'paramColorGroup' : 'paramColor';
                foreach ($activeFilters["parameters"]["parameter"] as $valueId => $paramId) {
                    $paramSubCondition[] = "(
                        paramHook.paramValue = (
                            SELECT paramInfo.paramValue
                            FROM `{$crossParamTable}` AS paramInfo
                            WHERE paramInfo.paramId = :{$paramBindCount}_paramId AND paramInfo.valueId = :{$paramBindCount}_valueId
                            LIMIT 1
                        )
                        OR paramHook.{$paramColorColumn} = :{$paramBindCount}_valueColor
                    )";

                    $paramBindValues["{$paramBindCount}_paramId"] = $paramId;
                    $paramBindValues["{$paramBindCount}_valueId"] = $valueId;
                    $paramBindValues["{$paramBindCount}_valueColor"] = "#{$valueId}";
                    $paramBindCount++;
                }

                $paramSubQuery = "SELECT GROUP_CONCAT(DISTINCT paramHook.productId ORDER BY paramHook.productId) AS productIds
                                FROM `{$crossParamTable}` AS paramHook
                                WHERE paramHook.paramId IN (".implode(', ', array_values($activeFilters["parameters"]["parameter"])).")
                                AND (".implode(' OR ', $paramSubCondition).")";

                $paramData = $this->database->getQuery($paramSubQuery, $paramBindValues, false, true);
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= " product.id IN (".( (!empty($paramData["productIds"]) && isset($paramData["productIds"])) ? $paramData["productIds"] : '0').")";
            }
            if (isset($activeFilters["parameters"]["type"])) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "(product.type = :type)";
                $whereBind["type"] = $activeFilters["parameters"]["type"];
            }
            if (isset($activeFilters["parameters"]["search"]) && $langShort) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "((";
                $whereCond .= "translate.title LIKE CONCAT('%', :word, '%')";
                if(isset($activeFilters["parameters"]["searchBy"])){
                    foreach($activeFilters["parameters"]["searchBy"] as $searchParam){
                        $whereCond .= " OR {$searchParam} LIKE CONCAT('%', :word, '%')";

                        if($searchParam === "manufacturer.title"){
                            $manufacturerTable = databaseTables::getTable("products", "manufacturerTable");
                            $sqlQuery .= " LEFT JOIN `{$manufacturerTable}` as manufacturer ON product.manufacturer = manufacturer.id";
                        }
                    }
                }
                $whereBind["word"] = $activeFilters["parameters"]["search"];
                $whereCond .= ")";
                if($langCheck !== true){
                    $sqlQuery .= " INNER JOIN `{$translateTable}` as translate ON product.id = translate.associatedId";
                    $whereCond .= " AND lang.langShort = :searchLang";
                    $whereBind["searchLang"] = $langShort;

                    $langCheck = true;
                }
                $whereCond .= ")";
            }
            if (isset($activeFilters["parameters"]["manufacturer"])) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $paramWhere = null;
                if(!is_array($activeFilters["parameters"]["manufacturer"])){
                    $manufacturer = $activeFilters["parameters"]["manufacturer"];
                    $activeFilters["parameters"]["manufacturer"] = [];
                    $activeFilters["parameters"]["manufacturer"][$manufacturer] = true;
                }

                foreach ($activeFilters["parameters"]["manufacturer"] as $key => $label) {
                    $paramWhere .= ($paramWhere === null) ? "(" : " OR ";
                    $paramWhere .= "product.manufacturer = :manufacturer{$key}";
                    $whereBind["manufacturer{$key}"] = $key;
                }
                $whereCond .= $paramWhere.")";
            }
            if (isset($activeFilters["parameters"]["availability"])) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "(product.availability = :availability)";
                $whereBind["availability"] = 1; // In STOCK
            }
            if (isset($activeFilters["parameters"]["saleOption"])) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "(product.saleOption = :saleOption)";
                $whereBind["saleOption"] = $activeFilters["parameters"]["saleOption"];
            }

            if (isset($activeFilters["parameters"]["optysSubjectId"])) {
                if($this->appController->isActiveModule('optysPartner')){
                    $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                    $whereCond .= "(product.optysSubjectId = :optysSubjectId)";
                    $whereBind["optysSubjectId"] = $activeFilters["parameters"]["optysSubjectId"];
                }
            }
        }
        if ($visibility !== null) {
            $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
            $whereCond .= ($visibility === false) ? "product.visibility = 0" : "product.visibility = 1";
        }

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;
        $order = ($orderBy === null) ? "product.orderNum DESC, product.id DESC" : $orderBy;
        $order .= ($orderDirection !== null && $orderBy !== null) ? " ".$orderDirection : null;

        $query = "SELECT {$sqlRows} {$sqlQuery} {$whereCond} GROUP BY product.id ORDER BY {$order}";
        $productData = $this->database->getQuery("{$query} {$limit} {$offset};", $whereBind, true);
        $productDataNoLimit = ($limit !== null) ? $this->database->getQuery("{$query};", $whereBind, true) : $productData;

        // 100% match in product code
        if (isset($activeFilters["parameters"]["searchBy"], $activeFilters["parameters"]["search"]) && in_array("product.code", $activeFilters["parameters"]["searchBy"]) && !empty($productDataNoLimit)) {
            $searchCode = $activeFilters["parameters"]["search"];
            $key = array_search($searchCode, array_column($productDataNoLimit, 'code'));
            
            if ($key !== false) {
                $productData = [$productDataNoLimit[$key]];
                $productDataNoLimit = $productData;
            }
        }

        $this->countAndControl($productData, $productDataNoLimit);

        return $productData;
    }
    public function getAll_Products_In($inValues, $langShort = null, $limit = null, $offset = null) {
        $dataTable = databaseTables::getTable("products", "productTable");
        $translateTable = databaseTables::getTable("products", "productTableLang");

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;

        $sqlQuery = "SELECT * FROM `{$dataTable}` WHERE id IN ({$inValues}) ORDER BY FIND_IN_SET(id, '{$inValues}') {$limit} {$offset};";
        $productData = $this->database->getQuery($sqlQuery, null, true);

        if ($langShort !== null) {
            $langColumns = "title, shortDesc, description, metaKeywords, metaTitle, metaDescription, url, langShort, youtubeUrl";
            $productData = $this->localizationClass->getTranslation($productData, $langColumns, $translateTable, $langShort);
        }

        return $productData;
    }
    public function getAll_Products($langShort = null, $categoryId = 0, $activeFilters = [], $visibility = null, $limit = null, $offset = null, $orderBy = null, $orderDirection = null) {
        $data = $this->getAll_Products_Where($langShort, $categoryId, $activeFilters, $visibility, null, null, $orderBy, $orderDirection);
        $dataInfo = $this->filtrationClass->countData($data, []);

        $returnData = ($dataInfo["count"] > 0) ? $this->getAll_Products_In($dataInfo["countValues"], $langShort, $limit, $offset) : [];
        return $returnData;
    }
/* ================== PRODUCTS - Categories === */
    public function getAll_Categories_Where($langShort = null, $categoryId = 0, $activeFilters = [], $visibility = null, $limit = null, $offset = null, $orderBy = null, $orderDirection = null) {
        $dataTable = databaseTables::getTable("products", "categoryTable");
        $translateTable = databaseTables::getTable("products", "categoryTableLang");
        $whereCond = $whereBind = $langCheck = null;
        $sqlQuery = "SELECT category.* FROM `{$dataTable}` as category";

        if ($categoryId !== false) {
            $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
            $whereCond .= "category.parentId = :categoryId";
            $whereBind["categoryId"] = $categoryId;
        }
        if (!empty($activeFilters)) {
            if (isset($activeFilters["parameters"]["search"]) && $langShort) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "(";
                $whereCond .= "translate.title LIKE CONCAT('%', :word, '%')";
                $whereCond .= " OR translate.metaTitle LIKE CONCAT('%', :word, '%')";
                $whereBind["word"] = $activeFilters["parameters"]["search"];

                if($langCheck !== true){
                    $sqlQuery .= " INNER JOIN `{$translateTable}` as translate ON category.id = translate.associatedId";
                    $whereCond .= " AND langShort = :searchLang";
                    $whereBind["searchLang"] = $langShort;

                    $langCheck = true;
                }
                $whereCond .= ")";
            }

            if (isset($activeFilters["parameters"]["optysSubjectId"])) {
                if($this->appController->isActiveModule('optysPartner')){
                    $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                    $whereCond .= "(category.optysSubjectId = :optysSubjectId)";
                    $whereBind["optysSubjectId"] = $activeFilters["parameters"]["optysSubjectId"];
                }
            }
        }
        if ($visibility !== null) {
            $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
            $whereCond .= ($visibility === false) ? "category.visibility = 0" : "category.visibility = 1";
        }

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;
        $order = ($orderBy === null) ? "orderNum DESC, id DESC" : $orderBy;
        $order .= ($orderDirection !== null && $orderBy !== null) ? " ".$orderDirection : null;

        $categoryData = $this->database->getQuery("{$sqlQuery} {$whereCond} ORDER BY {$order} {$limit} {$offset};", $whereBind, true);
        $categoryDataNoLimit = $this->database->getQuery("{$sqlQuery} {$whereCond} ORDER BY {$order};", $whereBind, true);
        $this->countAndControl($categoryData, $categoryDataNoLimit);

        if ($langShort !== null) {
            $langColumns = "title, shortDesc, description, metaKeywords, metaTitle, metaDescription, url, langShort";
            $categoryData = $this->localizationClass->getTranslation($categoryData, $langColumns, $translateTable, $langShort);
        }

        return $categoryData;
    }
    public function getAll_Categories_In($inValues, $langShort = null, $limit = null, $offset = null) {
        $dataTable = databaseTables::getTable("products", "categoryTable");
        $translateTable = databaseTables::getTable("products", "categoryTableLang");

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;

        $sqlQuery = "SELECT * FROM `{$dataTable}` WHERE id IN ({$inValues}) ORDER BY FIND_IN_SET(id, '{$inValues}') {$limit} {$offset};";
        $categoryData = $this->database->getQuery($sqlQuery, null, true);

        if ($langShort !== null) {
            $langColumns = "title, shortDesc, description, metaKeywords, metaTitle, metaDescription, url, langShort";
            $categoryData = $this->localizationClass->getTranslation($categoryData, $langColumns, $translateTable, $langShort);
        }

        return $categoryData;
    }
    public function getAll_Categories($langShort = null, $categoryId = 0, $activeFilters = [], $visibility = null, $limit = null, $offset = null, $orderBy = null, $orderDirection = null) {
        $data = $this->getAll_Categories_Where($langShort, $categoryId, $activeFilters, $visibility, null, null, $orderBy, $orderDirection);
        $dataInfo = $this->filtrationClass->countData($data, []);

        $returnData = ($dataInfo["count"] > 0) ? $this->getAll_Categories_In($dataInfo["countValues"], $langShort, $limit, $offset) : [];
        return $returnData;
    }
/* ================== PRODUCTS - Parameters === */
    public function getAll_Parameters($langShort = null, $activeFilters = [], $limit = null, $offset = null, $orderBy = null, $orderDirection = null) {
        $dataTable = databaseTables::getTable("products", "paramTable");

        $whereCond = $whereBind = $langCheck = null;
        $sqlQuery = "SELECT * FROM `{$dataTable}`";

        if (!empty($activeFilters)) {
            if (isset($activeFilters["parameters"]["search"]) && $langShort) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "title LIKE CONCAT('%', :word, '%')";
                $whereBind["word"] = $activeFilters["parameters"]["search"];
            }
            if (isset($activeFilters["parameters"]["type"])) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "type = :type";
                $whereBind["type"] = $activeFilters["parameters"]["type"];
            }
        }

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;
        $order = ($orderBy === null) ? "id DESC" : $orderBy;
        $order .= ($orderDirection !== null && $orderBy !== null) ? " ".$orderDirection : null;

        $sqlQuery = "{$sqlQuery} {$whereCond} ORDER BY {$order} {$limit} {$offset};";
        $returnData = $this->database->getQuery($sqlQuery, $whereBind, true);
        $this->countAndControl($returnData);

        if ($langShort !== null) {
            $productParams = new productParams($this->database, $this->appController);
            $returnData = $this->localizationClass->translateResult($returnData, $productParams->translateType, $langShort);
        }

        return $returnData;
    }
/* ================== PRODUCTS - Manufacturers === */
    public function getAll_Manufacturers($langShort = null, $activeFilters = [], $limit = null, $offset = null, $orderBy = null, $orderDirection = null) {
        $dataTable = databaseTables::getTable("products", "manufacturerTable");

        $whereCond = $whereBind = $langCheck = null;
        $sqlQuery = "SELECT * FROM `{$dataTable}`";

        if (!empty($activeFilters)) {
            if (isset($activeFilters["parameters"]["search"]) && $langShort) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "title LIKE CONCAT('%', :word, '%')";
                $whereBind["word"] = $activeFilters["parameters"]["search"];
            }
        }

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;
        $order = ($orderBy === null) ? "id DESC" : $orderBy;
        $order .= ($orderDirection !== null && $orderBy !== null) ? " ".$orderDirection : null;

        $returnData = $this->database->getQuery("{$sqlQuery} {$whereCond} ORDER BY {$order} {$limit} {$offset};", $whereBind, true);
        $returnDataNoLimit = $this->database->getQuery("{$sqlQuery} {$whereCond} ORDER BY {$order};", $whereBind, true);
        $this->countAndControl($returnData, $returnDataNoLimit);

        if ($langShort !== null) {
            $translateType = $this->appController->getModuleConfig("products","translateType")["productManufacturer"];
            $returnData = $this->localizationClass->translateResult($returnData, $translateType, $langShort);
        }

        return $returnData;
    }
/* ================== MEDIA MANAGER - Media files === */
    public function getAll_MediaFiles_Where($langShort = null, $folderId = 0, $activeFilters = [], $visibility = null, $limit = null, $offset = null, $orderBy = null, $orderDirection = null) {
        $dataTable = databaseTables::getTable("mediaManager", "files");
        $translateTable = databaseTables::getTable("mediaManager", "lang");
        $whereCond = $whereBind = $langCheck = null;
        $sqlQuery = "SELECT files.* FROM `{$dataTable}` as files";

        if ($folderId !== false) {
            $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
            $whereCond .= "files.folder = :folderId AND files.duplicated = 0";
            $whereBind["folderId"] = $folderId;
        }

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;
        $order = ($orderBy === null) ? "orderNum DESC, id DESC" : $orderBy;
        $order .= ($orderDirection !== null && $orderBy !== null) ? " ".$orderDirection : null;

        $sqlQuery = "{$sqlQuery} {$whereCond} ORDER BY {$order} {$limit} {$offset};";
        $mediaFiles = $this->database->getQuery($sqlQuery, $whereBind, true);
        $this->countAndControl($mediaFiles);

        if ($langShort !== null) {
            $langColumns = "title, alt, langShort";
            $mediaFiles = $this->localizationClass->getTranslation($mediaFiles, $langColumns, $translateTable, $langShort);
        }

        return $mediaFiles;
    }
    public function getAll_MediaFiles_In($inValues, $langShort = null, $limit = null, $offset = null) {
        $dataTable = databaseTables::getTable("mediaManager", "files");
        $translateTable = databaseTables::getTable("mediaManager", "lang");

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;

        $sqlQuery = "SELECT * FROM `{$dataTable}` WHERE id IN ({$inValues}) ORDER BY FIND_IN_SET(id, '{$inValues}') {$limit} {$offset};";
        $returnData = $this->database->getQuery($sqlQuery, null, true);

        if ($langShort !== null) {
            $langColumns = "title, alt, langShort";
            $returnData = $this->localizationClass->getTranslation($returnData, $langColumns, $translateTable, $langShort);
        }

        return $returnData;
    }
    public function getAll_MediaFiles($langShort = null, $folderId = 0, $activeFilters = [], $visibility = null, $limit = null, $offset = null, $orderBy = null, $orderDirection = null) {
        $data = $this->getAll_MediaFiles_Where($langShort, $folderId, $activeFilters, $visibility, null, null, $orderBy, $orderDirection);
        $dataInfo = $this->filtrationClass->countData($data, []);

        $returnData = ($dataInfo["count"] > 0) ? $this->getAll_MediaFiles_In($dataInfo["countValues"], $langShort, $limit, $offset) : [];
        return $returnData;
    }
/* ================== CONTENT EDITOR - Posts === */
    public function getAll_Posts_Where($langShort = null, $activeFilters = [], $visibility = null, $limit = null, $offset = null, $orderBy = null, $orderDirection = null) {
        $dataTable = databaseTables::getTable("contentEditor", "postTable");
        $translateTable = databaseTables::getTable("contentEditor", "postTableLang");
        $crossGroupTable = databaseTables::getTable("contentEditor", "crossGroupTable");
        $crossCategoryTable = databaseTables::getTable("contentEditor", "crossCategoryTable");

        $whereCond = $whereBind = $langCheck = null;
        $sqlQuery = "SELECT post.* FROM `{$dataTable}` as post";

        if (!empty($activeFilters)) {
            if (isset($activeFilters["parameters"]["type"])) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                if(strpos($activeFilters["parameters"]["type"], '!') !== false){
                    $activeFilters["parameters"]["type"] = str_replace('!', '', $activeFilters["parameters"]["type"]);
                    $whereCond .= "(post.postType != :type";
                }else{
                    $whereCond .= "(post.postType = :type";
                }

                $whereBind["type"] = $activeFilters["parameters"]["type"];

                if($activeFilters["parameters"]["type"] == 3){
                    $whereCond .= " AND (SELECT COUNT(*) FROM `{$crossGroupTable}` AS groups WHERE post.id = groups.postId) > 0)";
                    $whereBind["type"] = 1;
                }elseif($activeFilters["parameters"]["type"] == 1){
                    $whereCond .= " AND (SELECT COUNT(*) FROM `{$crossGroupTable}` AS groups WHERE post.id = groups.postId) = 0)";
                }else{
                    $whereCond .= ")";
                }
            }
            if (!empty($langShort)) {
                $sqlQuery .= " INNER JOIN `{$translateTable}` as translate ON post.id = translate.associatedId";
                $whereCond .= " AND translate.langShort = :searchLang";
                $whereBind["searchLang"] = $langShort;
                $langCheck = true;
            }
            if (isset($activeFilters["parameters"]["search"]) && !empty($langShort)) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "(";
                $whereCond .= "translate.title LIKE CONCAT('%', :word, '%')";
                $whereCond .= ($this->indexKeywords === true) ? " OR translate.metaKeywords LIKE CONCAT('%', :word, '%')" : null;
                $whereBind["word"] = $activeFilters["parameters"]["search"];
                $whereCond .= ")";
            }
            if (isset($activeFilters["parameters"]["group"])) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "(SELECT COUNT(*) FROM `{$crossGroupTable}` AS groups WHERE post.id = groups.postId AND groups.groupId = :groupId) > 0";
                $whereBind["groupId"] = $activeFilters["parameters"]["group"];
            }
            if (isset($activeFilters["parameters"]["category"])) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "(SELECT COUNT(*) FROM `{$crossCategoryTable}` AS categories WHERE post.id = categories.postId AND categories.categoryId = :categoryId) > 0";
                $whereBind["categoryId"] = $activeFilters["parameters"]["category"];
            }
            if (isset($activeFilters["parameters"]["label"])) {
                $labelTable = databaseTables::getTable("contentEditor", "labelsTable");
                $labelHookTable = databaseTables::getTable("contentEditor", "labelsHookTable");
                $sqlQuery .= " INNER JOIN `{$labelHookTable}` as labelHook ON post.id = labelHook.postId INNER JOIN `{$labelTable}` as label ON labelHook.labelId = label.id";
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $paramWhere = null;

                foreach ($activeFilters["parameters"]["label"] as $key => $label) {
                    $bindKey = str_replace("-", "", $key);
                    $paramWhere .= ($paramWhere === null) ? "(" : " OR ";
                    $paramWhere .= "label.shortTag = :lab{$bindKey}";
                    $whereBind["lab{$bindKey}"] = $key;
                }
                $whereCond .= $paramWhere.")";
            }
        }
        if ($visibility !== null) {
            $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
            $whereCond .= ($visibility === false) ? "post.visibility = 0" : "post.visibility = 1";
        }

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;
        $order = ($orderBy === null) ? "orderNum DESC, id DESC" : $orderBy;
        $order .= ($orderDirection !== null && $orderBy !== null) ? " ".$orderDirection : null;

        $postData = $this->database->getQuery("{$sqlQuery} {$whereCond} GROUP BY post.id ORDER BY {$order} {$limit} {$offset};", $whereBind, true);
        $postDataNoLimit = $this->database->getQuery("{$sqlQuery} {$whereCond} GROUP BY post.id ORDER BY {$order};", $whereBind, true);
        $this->countAndControl($postData, $postDataNoLimit);

        if ($langShort !== null) {
            $langColumns = "title, shortDesc, description, metaKeywords, metaTitle, metaDescription, url, langShort";
            $postData = $this->localizationClass->getTranslation($postData, $langColumns, $translateTable, $langShort);
        }

        return $postData;
    }
    public function getAll_Posts_In($inValues, $langShort = null, $limit = null, $offset = null) {
        $dataTable = databaseTables::getTable("contentEditor", "postTable");
        $translateTable = databaseTables::getTable("contentEditor", "postTableLang");

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;

        $sqlQuery = "SELECT * FROM `{$dataTable}` WHERE id IN ({$inValues}) ORDER BY FIND_IN_SET(id, '{$inValues}') {$limit} {$offset};";
        $postData = $this->database->getQuery($sqlQuery, null, true);

        if ($langShort !== null) {
            $langColumns = "title, shortDesc, description, metaKeywords, metaTitle, metaDescription, url, langShort";
            $postData = $this->localizationClass->getTranslation($postData, $langColumns, $translateTable, $langShort);
        }

        return $postData;
    }
    public function getAll_Posts($langShort = null, $activeFilters = [], $visibility = null, $limit = null, $offset = null, $orderBy = null, $orderDirection = null) {
        $data = $this->getAll_Posts_Where($langShort, $activeFilters, $visibility, null, null, $orderBy, $orderDirection);
        $dataInfo = $this->filtrationClass->countData($data, []);

        $returnData = ($dataInfo["count"] > 0) ? $this->getAll_Posts_In($dataInfo["countValues"], $langShort, $limit, $offset) : [];
        return $returnData;
    }
/* ================== CONTENT EDITOR - Categories === */
    public function getAll_PostCategories_Where($langShort = null, $activeFilters = [], $visibility = null, $limit = null, $offset = null, $orderBy = null, $orderDirection = null) {
        $dataTable = databaseTables::getTable("contentEditor", "categoryTable");

        $whereCond = $whereBind = $langCheck = null;
        $sqlQuery = "SELECT post.* FROM `{$dataTable}` as post";

        if (!empty($activeFilters)) {
            if (isset($activeFilters["parameters"]["search"]) && $langShort) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "(";
                $whereCond .= "translate.title LIKE CONCAT('%', :word, '%')";
                $whereCond .= ($this->indexKeywords === true) ? " OR translate.metaKeywords LIKE CONCAT('%', :word, '%')" : null;
                $whereBind["word"] = $activeFilters["parameters"]["search"];

                if($langCheck !== true){
                    $sqlQuery .= " INNER JOIN `{$translateTable}` as translate ON post.id = translate.associatedId";
                    $whereCond .= " AND langShort = :searchLang";
                    $whereBind["searchLang"] = $langShort;

                    $langCheck = true;
                }
                $whereCond .= ")";
            }
        }
        if ($visibility !== null) {
            $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
            $whereCond .= ($visibility === false) ? "post.visibility = 0" : "post.visibility = 1";
        }

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;
        $order = ($orderBy === null) ? "orderNum DESC, id DESC" : $orderBy;
        $order .= ($orderDirection !== null && $orderBy !== null) ? " ".$orderDirection : null;

        $returnData = $this->database->getQuery("{$sqlQuery} {$whereCond} ORDER BY {$order} {$limit} {$offset};", $whereBind, true);
        $returnDataNoLimit = $this->database->getQuery("{$sqlQuery} {$whereCond} ORDER BY {$order};", $whereBind, true);
        $this->countAndControl($returnData, $returnDataNoLimit);

        if ($langShort !== null) {
            $translateType = $this->appController->getModuleConfig('contentEditor', 'translateType')["postCategory"];
            $returnData = $this->localizationClass->translateResult($returnData, $translateType, $langShort);
        }

        return $returnData;
    }
    public function getAll_PostCategories_In($inValues, $langShort = null, $limit = null, $offset = null) {
        $dataTable = databaseTables::getTable("contentEditor", "categoryTable");

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;

        $sqlQuery = "SELECT * FROM `{$dataTable}` WHERE id IN ({$inValues}) ORDER BY FIND_IN_SET(id, '{$inValues}') {$limit} {$offset};";
        $postData = $this->database->getQuery($sqlQuery, null, true);

        if ($langShort !== null) {
            $translateType = $this->appController->getModuleConfig('contentEditor', 'translateType')["postCategory"];
            $postData = $this->localizationClass->translateResult($postData, $translateType, $langShort);
        }

        return $postData;
    }
    public function getAll_PostCategories($langShort = null, $activeFilters = [], $visibility = null, $limit = null, $offset = null, $orderBy = null, $orderDirection = null) {
        $data = $this->getAll_PostCategories_Where($langShort, $activeFilters, $visibility, null, null, $orderBy, $orderDirection);
        $dataInfo = $this->filtrationClass->countData($data, []);

        $returnData = ($dataInfo["count"] > 0) ? $this->getAll_PostCategories_In($dataInfo["countValues"], $langShort, $limit, $offset) : [];
        return $returnData;
    }
/* ================== TEMPLATES - Redirected URLs === */
    public function getAll_redirectedUrls_Where($activeFilters = [], $visibility = null, $limit = null, $offset = null, $orderBy = null, $orderDirection = null) {
        $dataTable = databaseTables::getTable("templates", "redirectsTable");
        $skipCache = (isset($activeFilters["noCache"]));

        $whereCond = $whereBind = $langCheck = null;
        $sqlQuery = "SELECT url.* FROM `{$dataTable}` as url";

        if (!empty($activeFilters)) {
            if (isset($activeFilters["parameters"]["status"])) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "(url.status = :status)";
                $whereBind["status"] = $activeFilters["parameters"]["status"];
            }
            if (isset($activeFilters["parameters"]["search"])) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "(url.alias LIKE CONCAT('%', :word, '%'))";
                $whereBind["word"] = $activeFilters["parameters"]["search"];
            }
        }
        if ($visibility !== null) {
            $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
            $whereCond .= ($visibility === false) ? "url.visibility = 0" : "url.visibility = 1";
        }

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;
        $order = ($orderBy === null) ? "id DESC" : $orderBy;
        $order .= ($orderDirection !== null && $orderBy !== null) ? " ".$orderDirection : null;

        $sqlQuery = "{$sqlQuery} {$whereCond} ORDER BY {$order} {$limit} {$offset};";
        $returnData = $this->database->getQuery($sqlQuery, $whereBind, true, $skipCache);
        $this->countAndControl($returnData);

        return $returnData;
    }
    public function getAll_redirectedUrls_In($inValues, $limit = null, $offset = null) {
        $dataTable = databaseTables::getTable("templates", "redirectsTable");

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;

        $sqlQuery = "SELECT * FROM `{$dataTable}` WHERE id IN ({$inValues}) ORDER BY FIND_IN_SET(id, '{$inValues}') {$limit} {$offset};";
        $returnData = $this->database->getQuery($sqlQuery, null, true);

        return $returnData;
    }
    public function getAll_redirectedUrls($activeFilters = [], $visibility = null, $limit = null, $offset = null, $orderBy = null, $orderDirection = null) {
        $data = $this->getAll_redirectedUrls_Where($activeFilters, $visibility, null, null, $orderBy, $orderDirection);
        $dataInfo = $this->filtrationClass->countData($data, []);

        $returnData = ($dataInfo["count"] > 0) ? $this->getAll_redirectedUrls_In($dataInfo["countValues"], $limit, $offset) : [];
        return $returnData;
    }
/* ================== TEMPLATES - Content templates === */
    public function getAll_Templates_Where($langShort = null, $activeFilters = [], $onlyWithView = false, $limit = null, $offset = null, $orderBy = null, $orderDirection = null) {
        $dataTable = databaseTables::getTable("templates", "templateTable");
        $translateTable = databaseTables::getTable("templates", "templateTableLang");

        $whereCond = $whereBind = $langCheck = null;
        $sqlQuery = "SELECT template.* FROM `{$dataTable}` as template";

        if (!empty($activeFilters)) {
            if (isset($activeFilters["parameters"]["search"]) && $langShort) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "(";
                $whereCond .= "translate.title LIKE CONCAT('%', :word, '%')";
                $whereCond .= ($this->indexKeywords === true) ? " OR translate.metaKeywords LIKE CONCAT('%', :word, '%')" : null;
                $whereBind["word"] = $activeFilters["parameters"]["search"];

                if($langCheck !== true){
                    $sqlQuery .= " INNER JOIN `{$translateTable}` as translate ON template.id = translate.associatedId";
                    $whereCond .= " AND langShort = :searchLang";
                    $whereBind["searchLang"] = $langShort;

                    $langCheck = true;
                }
                $whereCond .= ")";
            }
        }
        if ($onlyWithView === true) {
            $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
            $whereCond .= "template.pageType != 2";
        }

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;
        $order = ($orderBy === null) ? "id DESC" : $orderBy;
        $order .= ($orderDirection !== null && $orderBy !== null) ? " ".$orderDirection : null;

        $returnData = $this->database->getQuery("{$sqlQuery} {$whereCond} ORDER BY {$order} {$limit} {$offset};", $whereBind, true);
        $returnDataNoLimit = $this->database->getQuery("{$sqlQuery} {$whereCond} ORDER BY {$order};", $whereBind, true);
        $this->countAndControl($returnData, $returnDataNoLimit);

        if ($langShort !== null) {
            $langColumns = "title, shortDesc, description, metaKeywords, metaTitle, metaDescription, url, langShort";
            $returnData = $this->localizationClass->getTranslation($returnData, $langColumns, $translateTable, $langShort);
        }

        return $returnData;
    }
    public function getAll_Templates_In($inValues, $langShort = null, $limit = null, $offset = null) {
        $dataTable = databaseTables::getTable("templates", "templateTable");
        $translateTable = databaseTables::getTable("templates", "templateTableLang");

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;

        $sqlQuery = "SELECT * FROM `{$dataTable}` WHERE id IN ({$inValues}) ORDER BY FIND_IN_SET(id, '{$inValues}') {$limit} {$offset};";
        $returnData = $this->database->getQuery($sqlQuery, null, true);

        if ($langShort !== null) {
            $langColumns = "title, shortDesc, description, metaKeywords, metaTitle, metaDescription, url, langShort";
            $returnData = $this->localizationClass->getTranslation($returnData, $langColumns, $translateTable, $langShort);
        }

        return $returnData;
    }
    public function getAll_Templates($langShort = null, $activeFilters = [], $onlyWithView = false, $limit = null, $offset = null, $orderBy = null, $orderDirection = null) {
        $data = $this->getAll_Templates_Where($langShort, $activeFilters, $onlyWithView, null, null, $orderBy, $orderDirection);
        $dataInfo = $this->filtrationClass->countData($data, []);

        $returnData = ($dataInfo["count"] > 0) ? $this->getAll_Templates_In($dataInfo["countValues"], $langShort, $limit, $offset) : [];
        return $returnData;
    }
/* ================== GALLERY - Galleries === */
    public function getAll_galleries_Where($galleryModule, $langShort = null, $activeFilters = [], $visibility = null, $limit = null, $offset = null, $orderBy = null, $orderDirection = null) {
        $dataTable = databaseTables::getTable("gallery", "galleries");

        $whereCond = $whereBind = $langCheck = null;
        $sqlQuery = "SELECT gallery.* FROM `{$dataTable}` as gallery";

        if (!empty($activeFilters)) {
            if (isset($activeFilters["parameters"]["groupId"])) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "(gallery.groupId = :groupId)";
                $whereBind["groupId"] = $activeFilters["parameters"]["groupId"];
            }
            if (isset($activeFilters["parameters"]["search"])) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "(gallery.title LIKE CONCAT('%', :word, '%'))";
                $whereBind["word"] = $activeFilters["parameters"]["search"];
            }
        }
        if ($visibility !== null) {
            $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
            $whereCond .= ($visibility === false) ? "gallery.visibility = 0" : "gallery.visibility = 1";
        }

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;
        $order = ($orderBy === null) ? "orderNum DESC, id DESC" : $orderBy;
        $order .= ($orderDirection !== null && $orderBy !== null) ? " ".$orderDirection : null;
        $order = "groupId ASC, ".$order;

        $sqlQuery = "{$sqlQuery} {$whereCond} ORDER BY {$order} {$limit} {$offset};";
        $returnData = $this->database->getQuery($sqlQuery, $whereBind, true);
        $this->countAndControl($returnData);

        if ($langShort !== null) {
            $returnData = $this->localizationClass->translateResult($returnData, $galleryModule->translateType, $langShort);
        }

        return $returnData;
    }
    public function getAll_galleries_In($galleryModule, $inValues, $langShort = null, $limit = null, $offset = null) {
        $dataTable = databaseTables::getTable("gallery", "galleries");

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;

        $sqlQuery = "SELECT * FROM `{$dataTable}` WHERE id IN ({$inValues}) ORDER BY FIND_IN_SET(id, '{$inValues}') {$limit} {$offset};";
        $returnData = $this->database->getQuery($sqlQuery, null, true);

        if ($langShort !== null) {
            $returnData = $this->localizationClass->translateResult($returnData, $galleryModule->translateType, $langShort);
        }

        return $returnData;
    }
    public function getAll_galleries($langShort = null, $activeFilters = [], $visibility = null, $limit = null, $offset = null, $orderBy = null, $orderDirection = null) {
        $galleryModule = new moduleGallery($this->database, $this->appController);

        $data = $this->getAll_galleries_Where($galleryModule, $langShort, $activeFilters, $visibility, null, null, $orderBy, $orderDirection);
        $dataInfo = $this->filtrationClass->countData($data, []);

        $returnData = ($dataInfo["count"] > 0) ? $this->getAll_galleries_In($galleryModule, $dataInfo["countValues"], $langShort, $limit, $offset) : [];
        return $returnData;
    }
/* ================== USERS - Users list === */
    public function getAll_users_Where($activeFilters = [], $limit = null, $offset = null, $orderBy = null, $orderDirection = null) {
        $dataTable = databaseTables::getTable("users", "userTable");
        $statusesTable = databaseTables::getTable("users", "statusesTable");
        $controlTable = databaseTables::getTable("users", "controlTable");

        $whereCond = $whereBind = $langCheck = null;
        $sqlQuery = "SELECT user.* FROM `{$dataTable}` as user";

        if (!empty($activeFilters)) {
            if (isset($activeFilters["parameters"]["status"])) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "(userControl.userStatus = :statusId)";
                $whereBind["statusId"] = $activeFilters["parameters"]["status"];

                $sqlQuery .= " INNER JOIN `{$controlTable}` as userControl ON user.id = userControl.associatedId";
                $sqlQuery .= " INNER JOIN `{$statusesTable}` as userStatus ON userControl.userStatus = userStatus.id";
            }
            if (isset($activeFilters["parameters"]["search"])) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "(user.name LIKE CONCAT('%', :word, '%') OR user.surname LIKE CONCAT('%', :word, '%') OR user.email LIKE CONCAT('%', :word, '%'))";
                $whereBind["word"] = $activeFilters["parameters"]["search"];
            }
        }

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;
        $order = ($orderBy === null) ? "id DESC" : $orderBy;
        $order .= ($orderDirection !== null && $orderBy !== null) ? " ".$orderDirection : null;

        $sqlQuery = "{$sqlQuery} {$whereCond} ORDER BY {$order} {$limit} {$offset};";
        $returnData = $this->database->getQuery($sqlQuery, $whereBind, true);
        $this->countAndControl($returnData);

        return $returnData;
    }
    public function getAll_users_In($inValues, $limit = null, $offset = null) {
        $dataTable = databaseTables::getTable("users", "userTable");

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;

        $sqlQuery = "SELECT * FROM `{$dataTable}` WHERE id IN ({$inValues}) ORDER BY FIND_IN_SET(id, '{$inValues}') {$limit} {$offset};";
        $returnData = $this->database->getQuery($sqlQuery, null, true);

        return $returnData;
    }
    public function getAll_users($activeFilters = [], $limit = null, $offset = null, $orderBy = null, $orderDirection = null) {
        $data = $this->getAll_users_Where($activeFilters, null, null, $orderBy, $orderDirection);
        $dataInfo = $this->filtrationClass->countData($data, []);

        $returnData = ($dataInfo["count"] > 0) ? $this->getAll_users_In($dataInfo["countValues"], $limit, $offset) : [];
        return $returnData;
    }
/* ================== ORDERS - Orders list === */
    public function getAll_orders_Where($activeFilters = [], $limit = null, $offset = null, $orderDirection = null) {
        $dataTable = databaseTables::getTable("orders", "orderTable");

        $whereCond = $whereBind = $langCheck = null;
        $sqlQuery = "SELECT orders.* FROM `{$dataTable}` as orders";

        if (!empty($activeFilters)) {
            if (isset($activeFilters["parameters"]["status"])) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "(orders.orderStatus = :statusId)";
                $whereBind["statusId"] = $activeFilters["parameters"]["status"];
            }
            if (isset($activeFilters["parameters"]["dateFrom"]) OR isset($activeFilters["parameters"]["dateTo"])) {
                $dateFill = false;
                $dateCond = ($whereCond === null) ? " WHERE " : " AND ";
                $dateBind = [];

                $dateCond .= "(";
                if(isset($activeFilters["parameters"]["dateFrom"]) && $activeFilters["parameters"]["dateFrom"] !== '0000-00-00 00:00'){
                    $dateCond .= "orders.createdDate >= :dateFrom";
                    $dateBind["dateFrom"] = date('Y-m-d H:i:s', strtotime($activeFilters["parameters"]["dateFrom"]));
                    $dateFill = true;
                }
                if(isset($activeFilters["parameters"]["dateTo"]) && $activeFilters["parameters"]["dateTo"] !== '0000-00-00 00:00'){
                    $dateCond .= ($dateFill === true) ? ' AND ' : '';
                    $dateCond .= "orders.createdDate <= :dateTo";
                    $dateBind["dateTo"] = date('Y-m-d H:i:s', strtotime($activeFilters["parameters"]["dateTo"]));
                    $dateFill = true;
                }
                $dateCond .= ")";

                if($dateFill === true){
                    $whereCond .= $dateCond;
                    $whereBind = (!empty($whereBind)) ? array_merge($whereBind, $dateBind) : $dateBind;
                }
            }
            if (isset($activeFilters["parameters"]["search"])) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "(orders.orderId = :word OR orders.invoiceName LIKE CONCAT('%', :word, '%') OR orders.invoiceSurname LIKE CONCAT('%', :word, '%') OR orders.invoiceEmail LIKE CONCAT('%', :word, '%') OR orders.discountTitle LIKE CONCAT('%', :word, '%') OR orders.discountCode LIKE CONCAT('%', :word, '%') )";
                $whereBind["word"] = $activeFilters["parameters"]["search"];
            }
            if (isset($activeFilters["parameters"]["delivery"])) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "(orders.deliveryId = :deliveryId)";
                $whereBind["deliveryId"] = $activeFilters["parameters"]["delivery"];
            }
            if (isset($activeFilters["parameters"]["payment"])) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "(orders.paymentId = :paymentId)";
                $whereBind["paymentId"] = $activeFilters["parameters"]["payment"];
            }
            if (isset($activeFilters["parameters"]["currency"])) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "(orders.currency = :currency)";
                $whereBind["currency"] = $activeFilters["parameters"]["currency"];
            }
        }

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;
        $order = "createdDate";
        $order .= ($orderDirection !== null) ? " ".$orderDirection : " DESC";

        $sqlQuery = "{$sqlQuery} {$whereCond} ORDER BY {$order} {$limit} {$offset};";
        $returnData = $this->database->getQuery($sqlQuery, $whereBind, true);
        $this->countAndControl($returnData);

        return $returnData;
    }
    public function getAll_orders_In($inValues, $limit = null, $offset = null) {
        $dataTable = databaseTables::getTable("orders", "orderTable");

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;

        $sqlQuery = "SELECT * FROM `{$dataTable}` WHERE orderId IN ({$inValues}) ORDER BY FIND_IN_SET(orderId, '{$inValues}') {$limit} {$offset};";
        $returnData = $this->database->getQuery($sqlQuery, null, true);

        return $returnData;
    }
    public function getAll_orders($activeFilters = [], $limit = null, $offset = null, $orderDirection = null) {
        $data = $this->getAll_orders_Where($activeFilters, null, null, $orderDirection);
        $dataInfo = $this->filtrationClass->countData($data, [], 'orderId');

        $returnData = ($dataInfo["count"] > 0) ? $this->getAll_orders_In($dataInfo["countValues"], $limit, $offset) : [];
        return $returnData;
    }
/* ================== CONTACT BOXES - Contacts === */
    public function getAll_Contacts_Where($langShort = null, $activeFilters = [], $visibility = null, $limit = null, $offset = null, $orderBy = null, $orderDirection = null) {
        $dataTable = databaseTables::getTable("contactBoxes", "contacts");
        $contactBoxes = new contactBoxes($this->database, $this->appController);

        $whereCond = $whereBind = $langCheck = null;
        $sqlQuery = "SELECT contact.* FROM `{$dataTable}` as contact";

        if (!empty($activeFilters)) {
            if (isset($activeFilters["parameters"]["search"])) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "(contact.companyName LIKE CONCAT('%', :word, '%') OR contact.companyPerson LIKE CONCAT('%', :word, '%'))";
                $whereBind["word"] = $activeFilters["parameters"]["search"];
            }
        }
        if ($visibility !== null) {
            $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
            $whereCond .= ($visibility === false) ? "contact.visibility = 0" : "contact.visibility = 1";
        }

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;
        $order = ($orderBy === null) ? "orderNum DESC, id DESC" : $orderBy;
        $order .= ($orderDirection !== null && $orderBy !== null) ? " ".$orderDirection : null;

        $returnData = $this->database->getQuery("{$sqlQuery} {$whereCond} ORDER BY {$order} {$limit} {$offset};", $whereBind, true);
        $returnDataNoLimit = $this->database->getQuery("{$sqlQuery} {$whereCond} ORDER BY {$order};", $whereBind, true);
        $this->countAndControl($returnData, $returnDataNoLimit);

        if ($langShort !== null) {
            $returnData = $this->localizationClass->translateResult($returnData, $contactBoxes->translateType, $langShort);
        }

        return $returnData;
    }
    public function getAll_Contacts_In($inValues, $langShort = null, $limit = null, $offset = null) {
        $dataTable = databaseTables::getTable("contactBoxes", "contacts");
        $contactBoxes = new contactBoxes($this->database, $this->appController);

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;

        $sqlQuery = "SELECT * FROM `{$dataTable}` WHERE id IN ({$inValues}) ORDER BY FIND_IN_SET(id, '{$inValues}') {$limit} {$offset};";
        $returnData = $this->database->getQuery($sqlQuery, null, true);

        if ($langShort !== null) {
            $returnData = $this->localizationClass->translateResult($returnData, $contactBoxes->translateType, $langShort);
        }

        return $returnData;
    }
    public function getAll_Contacts($langShort = null, $activeFilters = [], $visibility = null, $limit = null, $offset = null, $orderBy = null, $orderDirection = null) {
        $data = $this->getAll_Contacts_Where($langShort, $activeFilters, $visibility, null, null, $orderBy, $orderDirection);
        $dataInfo = $this->filtrationClass->countData($data, []);

        $returnData = ($dataInfo["count"] > 0) ? $this->getAll_Contacts_In($dataInfo["countValues"], $langShort, $limit, $offset) : [];
        return $returnData;
    }
/* ================== RESERVATIONS  === */
    public function getAll_reservations_Where($activeFilters = [], $visibility = null, $limit = null, $offset = null, $orderBy = null, $orderDirection = null) {
        $dataTable = databaseTables::getTable("reservations", "reservations");

        $whereCond = $whereBind = $langCheck = null;
        $sqlQuery = "SELECT reservations.* FROM `{$dataTable}` as reservations";

        if (!empty($activeFilters)) {
            if (isset($activeFilters["parameters"]["status"])) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "(reservations.statusId = :statusId)";
                $whereBind["statusId"] = $activeFilters["parameters"]["status"];
            }
            if (isset($activeFilters["parameters"]["dateFrom"]) OR isset($activeFilters["parameters"]["dateTo"])) {
                $dateFill = false;
                $dateCond = ($whereCond === null) ? " WHERE " : " AND ";
                $dateBind = [];

                $dateCond .= "(";
                if(isset($activeFilters["parameters"]["dateFrom"]) && $activeFilters["parameters"]["dateFrom"] !== '0000-00-00 00:00'){
                    $dateCond .= "reservations.createdDate >= :dateFrom";
                    $dateBind["dateFrom"] = date('Y-m-d H:i:s', strtotime($activeFilters["parameters"]["dateFrom"]));
                    $dateFill = true;
                }
                if(isset($activeFilters["parameters"]["dateTo"]) && $activeFilters["parameters"]["dateTo"] !== '0000-00-00 00:00'){
                    $dateCond .= ($dateFill === true) ? ' AND ' : '';
                    $dateCond .= "reservations.createdDate <= :dateTo";
                    $dateBind["dateTo"] = date('Y-m-d', strtotime($activeFilters["parameters"]["dateTo"]));
                    $dateFill = true;
                }
                $dateCond .= ")";

                if($dateFill === true){
                    $whereCond .= $dateCond;
                    $whereBind = (!empty($whereBind)) ? array_merge($whereBind, $dateBind) : $dateBind;
                }
            }
            if (isset($activeFilters["parameters"]["reservationDate"])) {
                $rDateFill = false;
                $rDateCond = ($whereCond === null) ? " WHERE " : " AND ";
                $rDateBind = [];

                $rDateCond .= "(";
                if(isset($activeFilters["parameters"]["reservationDate"]) && $activeFilters["parameters"]["reservationDate"] !== '0000-00-00 00:00'){
                    $rDateCond .= "reservations.reservationDate = DATE(:reservationDate) AND reservations.timeStart >= :startTime";
                    $rDateBind["reservationDate"] = date('Y-m-d', strtotime($activeFilters["parameters"]["reservationDate"]));
                    $rDateBind["startTime"] = date('H:i', strtotime($activeFilters["parameters"]["reservationDate"]));
                    $rDateFill = true;
                }
                $rDateCond .= ")";

                if($rDateFill === true){
                    $whereCond .= $rDateCond;
                    $whereBind = (!empty($whereBind)) ? array_merge($whereBind, $rDateBind) : $rDateBind;
                }
            }
            if (isset($activeFilters["parameters"]["search"])) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "(reservations.reservationId = :word OR reservations.name LIKE CONCAT('%', :word, '%') OR reservations.surname LIKE CONCAT('%', :word, '%') OR reservations.email LIKE CONCAT('%', :word, '%'))";
                $whereBind["word"] = $activeFilters["parameters"]["search"];
            }
        }
        if ($visibility !== null) {
            $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
            $whereCond .= ($visibility === false) ? "reservations.visibility = 0" : "reservations.visibility = 1";
        }

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;
        $order = ($orderBy === null || $orderBy == "createdDate") ? "createdDate DESC" : "reservationDate DESC, timeStart ASC";

        $sqlQuery = "{$sqlQuery} {$whereCond} ORDER BY {$order} {$limit} {$offset};";
        $returnData = $this->database->getQuery($sqlQuery, $whereBind, true);
        $this->countAndControl($returnData);

        return $returnData;
    }
    public function getAll_reservations_In($inValues, $limit = null, $offset = null) {
        $dataTable = databaseTables::getTable("reservations", "reservations");

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;

        $sqlQuery = "SELECT * FROM `{$dataTable}` WHERE id IN ({$inValues}) ORDER BY FIND_IN_SET(id, '{$inValues}') {$limit} {$offset};";
        $returnData = $this->database->getQuery($sqlQuery, null, true);

        return $returnData;
    }
    public function getAll_reservations($activeFilters = [], $visibility = null, $limit = null, $offset = null, $orderBy = null, $orderDirection = null) {
        $data = $this->getAll_reservations_Where($activeFilters, $visibility, null, null, $orderBy, $orderDirection);
        $dataInfo = $this->filtrationClass->countData($data, []);

        $returnData = ($dataInfo["count"] > 0) ? $this->getAll_reservations_In($dataInfo["countValues"], $limit, $offset) : [];
        return $returnData;
    }
/* ================== RESERVATIONS - Groups === */
    public function getAll_reservationGroups_Where($reservationsClass, $langShort = null, $activeFilters = [], $visibility = null, $limit = null, $offset = null, $orderBy = null, $orderDirection = null) {
        $dataTable = databaseTables::getTable("reservations", "groups");

        $whereCond = $whereBind = $langCheck = null;
        $sqlQuery = "SELECT groups.* FROM `{$dataTable}` as groups";

        if (!empty($activeFilters)) {
            if (isset($activeFilters["parameters"]["search"])) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "(groups.title LIKE CONCAT('%', :word, '%'))";
                $whereBind["word"] = $activeFilters["parameters"]["search"];
            }
        }
        if ($visibility !== null) {
            $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
            $whereCond .= ($visibility === false) ? "groups.visibility = 0" : "groups.visibility = 1";
        }

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;
        $order = ($orderBy === null) ? "orderNum DESC, id DESC" : $orderBy;
        $order .= ($orderDirection !== null && $orderBy !== null) ? " ".$orderDirection : null;

        $sqlQuery = "{$sqlQuery} {$whereCond} ORDER BY {$order} {$limit} {$offset};";
        $returnData = $this->database->getQuery($sqlQuery, $whereBind, true);
        $this->countAndControl($returnData);

        if ($langShort !== null) {
            $returnData = $this->localizationClass->translateResult($returnData, $reservationsClass->translateType, $langShort);
        }

        return $returnData;
    }
    public function getAll_reservationGroups_In($reservationsClass, $inValues, $langShort = null, $limit = null, $offset = null) {
        $dataTable = databaseTables::getTable("reservations", "groups");

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;

        $sqlQuery = "SELECT * FROM `{$dataTable}` WHERE id IN ({$inValues}) ORDER BY FIND_IN_SET(id, '{$inValues}') {$limit} {$offset};";
        $returnData = $this->database->getQuery($sqlQuery, null, true);

        if ($langShort !== null) {
            $returnData = $this->localizationClass->translateResult($returnData, $reservationsClass->translateType, $langShort);
        }

        return $returnData;
    }
    public function getAll_reservationGroups($langShort = null, $activeFilters = [], $visibility = null, $limit = null, $offset = null, $orderBy = null, $orderDirection = null) {
        $reservationsClass = new reservationsClass($this->database);

        $data = $this->getAll_reservationGroups_Where($reservationsClass, $langShort, $activeFilters, $visibility, null, null, $orderBy, $orderDirection);
        $dataInfo = $this->filtrationClass->countData($data, []);

        $returnData = ($dataInfo["count"] > 0) ? $this->getAll_reservationGroups_In($reservationsClass, $dataInfo["countValues"], $langShort, $limit, $offset) : [];
        return $returnData;
    }
/* ================== INVOICES and documents === */
    public function getAll_documents_Where($activeFilters = [], $limit = null, $offset = null, $orderDirection = null) {
        $dataTable = databaseTables::getTable("orders", "orderTable");

        $whereCond = $whereBind = null;
        $sqlQuery = "SELECT * FROM `{$dataTable}` WHERE (invoice > '' OR proform > '' OR credit > '')";
        $orderBy = "createdDate";

        if (!empty($activeFilters)) {
            if (isset($activeFilters["parameters"]["dateFrom"]) OR isset($activeFilters["parameters"]["dateTo"])) {
                $dateFill = false;
                $dateCond = " AND ";
                $dateBind = [];

                $dateCond .= "(";
                if(isset($activeFilters["parameters"]["dateFrom"]) && $activeFilters["parameters"]["dateFrom"] !== '0000-00-00 00:00'){
                    $dateCond .= "orders.createdDate >= :dateFrom";
                    $dateBind["dateFrom"] = date('Y-m-d H:i:s', strtotime($activeFilters["parameters"]["dateFrom"]));
                    $dateFill = true;
                }
                if(isset($activeFilters["parameters"]["dateTo"]) && $activeFilters["parameters"]["dateTo"] !== '0000-00-00 00:00'){
                    $dateCond .= ($dateFill === true) ? ' AND ' : '';
                    $dateCond .= "orders.createdDate <= :dateTo";
                    $dateBind["dateTo"] = date('Y-m-d H:i:s', strtotime($activeFilters["parameters"]["dateTo"]));
                    $dateFill = true;
                }
                $dateCond .= ")";

                if($dateFill === true){
                    $whereCond .= $dateCond;
                    $whereBind = (!empty($whereBind)) ? array_merge($whereBind, $dateBind) : $dateBind;
                }
            }
            if (isset($activeFilters["parameters"]["search"])) {
                $whereCond .= " AND ";
                $whereCond .= "(orders.orderId = :word OR orders.invoiceName LIKE CONCAT('%', :word, '%') OR orders.invoiceSurname LIKE CONCAT('%', :word, '%') OR orders.invoice LIKE CONCAT('%', :word, '%') OR orders.credit LIKE CONCAT('%', :word, '%') OR orders.proform LIKE CONCAT('%', :word, '%') )";
                $whereBind["word"] = $activeFilters["parameters"]["search"];
            }
            if (isset($activeFilters["parameters"]["byDate"])) {
                $orderBy = $activeFilters["parameters"]["byDate"];
            }
        }

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;
        $order = $orderBy." ";
        $order .= ($orderDirection !== null) ? $orderDirection : "DESC";

        $sqlQuery = "{$sqlQuery} {$whereCond} ORDER BY {$order} {$limit} {$offset};";
        $returnData = $this->database->getQuery($sqlQuery, $whereBind, true);
        $this->countAndControl($returnData);

        return $returnData;
    }
    public function getAll_documents_In($inValues, $limit = null, $offset = null) {
        $dataTable = databaseTables::getTable("orders", "orderTable");

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;

        $sqlQuery = "SELECT * FROM `{$dataTable}` WHERE orderId IN ({$inValues}) ORDER BY FIND_IN_SET(orderId, '{$inValues}') {$limit} {$offset};";
        $returnData = $this->database->getQuery($sqlQuery, null, true);

        return $returnData;
    }
    public function getAll_documents($activeFilters = [], $limit = null, $offset = null, $orderDirection = null) {
        $data = $this->getAll_documents_Where($activeFilters, null, null, $orderDirection);
        $dataInfo = $this->filtrationClass->countData($data, [], "orderId");

        $returnData = ($dataInfo["count"] > 0) ? $this->getAll_documents_In($dataInfo["countValues"], $limit, $offset) : [];
        return $returnData;
    }
/* ================== ATTENDANCE - Log list === */
    public function getAll_attendanceLogs_Where($activeFilters = [], $limit = null, $offset = null, $orderDirection = null) {
        $dataTable = databaseTables::getTable("attendances", "list");

        $whereCond = $whereBind = $langCheck = null;
        $sqlQuery = "SELECT * FROM `{$dataTable}`";

        if (!empty($activeFilters)) {
            if (isset($activeFilters["parameters"]["userId"])) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "(userId = :userId)";
                $whereBind["userId"] = $activeFilters["parameters"]["userId"];
            }
            if (isset($activeFilters["parameters"]["logType"])) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "(logType = :logType)";
                $whereBind["logType"] = $activeFilters["parameters"]["logType"];
            }
            if (isset($activeFilters["parameters"]["dateFrom"]) OR isset($activeFilters["parameters"]["dateTo"])) {
                $dateFill = false;
                $dateCond = ($whereCond === null) ? " WHERE " : " AND ";
                $dateBind = [];

                $dateCond .= "(";
                if(isset($activeFilters["parameters"]["dateFrom"]) && $activeFilters["parameters"]["dateFrom"] !== '0000-00-00 00:00'){
                    $dateCond .= "logTime >= :dateFrom";
                    $dateBind["dateFrom"] = date('Y-m-d H:i:s', strtotime($activeFilters["parameters"]["dateFrom"]));
                    $dateFill = true;
                }
                if(isset($activeFilters["parameters"]["dateTo"]) && $activeFilters["parameters"]["dateTo"] !== '0000-00-00 00:00'){
                    $dateCond .= ($dateFill === true) ? ' AND ' : '';
                    $dateCond .= "logTime <= :dateTo";
                    $dateBind["dateTo"] = date('Y-m-d H:i:s', strtotime($activeFilters["parameters"]["dateTo"]));
                    $dateFill = true;
                }
                $dateCond .= ")";

                if($dateFill === true){
                    $whereCond .= $dateCond;
                    $whereBind = (!empty($whereBind)) ? array_merge($whereBind, $dateBind) : $dateBind;
                }
            }
        }

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;
        $order = "logTime";
        $order .= ($orderDirection !== null) ? " ".$orderDirection : " DESC";

        $sqlQuery = "{$sqlQuery} {$whereCond} ORDER BY {$order} {$limit} {$offset};";
        $returnData = $this->database->getQuery($sqlQuery, $whereBind, true);
        $this->countAndControl($returnData);

        return $returnData;
    }
    public function getAll_attendanceLogs_In($inValues, $limit = null, $offset = null) {
        $dataTable = databaseTables::getTable("attendances", "list");

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;

        $inValuesExploded = explode(",", $inValues);
        if(!empty($inValuesExploded)){
            $inValuesEscaped = null;
            foreach($inValuesExploded as $key => $logTime){
                $inValuesEscaped .= ($key == 0) ? null : ',';
                $inValuesEscaped .= '"'.$logTime.'"';
            }
        }

        $sqlQuery = "SELECT * FROM `{$dataTable}` WHERE logTime IN ({$inValuesEscaped}) ORDER BY FIND_IN_SET(logTime, '{$inValues}') {$limit} {$offset};";
        $returnData = $this->database->getQuery($sqlQuery, null, true);

        return $returnData;
    }
    public function getAll_attendanceLogs($activeFilters = [], $limit = null, $offset = null, $orderDirection = null) {
        $data = $this->getAll_attendanceLogs_Where($activeFilters, null, null, $orderDirection);
        $dataInfo = $this->filtrationClass->countData($data, [], 'logTime');

        $returnData = ($dataInfo["count"] > 0) ? $this->getAll_attendanceLogs_In($dataInfo["countValues"], $limit, $offset) : [];
        return $returnData;
    }
/* ================== ORDERS - Orders list === */
    public function getAll_ordersReviews_Where($activeFilters = [], $limit = null, $offset = null, $orderDirection = null) {
        $dataTable = databaseTables::getTable("ordersReviews", "reviews");

        $whereCond = $whereBind = null;
        $sqlQuery = "SELECT * FROM `{$dataTable}`";

        if (!empty($activeFilters)) {
            if (isset($activeFilters["parameters"]["search"])) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= " ( orderId = :word OR reviewerName LIKE CONCAT('%', :word, '%') )";
                $whereBind["word"] = $activeFilters["parameters"]["search"];
            }
            if (isset($activeFilters["parameters"]["approval"])) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "(approved = :approval)";
                $whereBind["approval"] = $activeFilters["parameters"]["approval"];
            }
            if (isset($activeFilters["parameters"]["dateFrom"]) OR isset($activeFilters["parameters"]["dateTo"])) {
                $dateFill = false;
                $dateCond = ($whereCond === null) ? " WHERE " : " AND ";
                $dateBind = [];

                $dateCond .= "(";
                if(isset($activeFilters["parameters"]["dateFrom"]) && $activeFilters["parameters"]["dateFrom"] !== '0000-00-00 00:00'){
                    $dateCond .= "createdDate >= :dateFrom";
                    $dateBind["dateFrom"] = date('Y-m-d H:i:s', strtotime($activeFilters["parameters"]["dateFrom"]));
                    $dateFill = true;
                }
                if(isset($activeFilters["parameters"]["dateTo"]) && $activeFilters["parameters"]["dateTo"] !== '0000-00-00 00:00'){
                    $dateCond .= ($dateFill === true) ? ' AND ' : '';
                    $dateCond .= "createdDate <= :dateTo";
                    $dateBind["dateTo"] = date('Y-m-d H:i:s', strtotime($activeFilters["parameters"]["dateTo"]));
                    $dateFill = true;
                }
                $dateCond .= ")";

                if($dateFill === true){
                    $whereCond .= $dateCond;
                    $whereBind = (!empty($whereBind)) ? array_merge($whereBind, $dateBind) : $dateBind;
                }
            }
        }

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;
        $order = "createdDate";
        $order .= ($orderDirection !== null) ? " ".$orderDirection : " DESC";

        $sqlQuery = "{$sqlQuery} {$whereCond} ORDER BY {$order} {$limit} {$offset};";
        $returnData = $this->database->getQuery($sqlQuery, $whereBind, true);
        $this->countAndControl($returnData);

        return $returnData;
    }
    public function getAll_ordersReviews_In($inValues, $limit = null, $offset = null) {
        $dataTable = databaseTables::getTable("ordersReviews", "reviews");

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;

        $sqlQuery = "SELECT * FROM `{$dataTable}` WHERE id IN ({$inValues}) ORDER BY FIND_IN_SET(id, '{$inValues}') {$limit} {$offset};";
        $returnData = $this->database->getQuery($sqlQuery, null, true);

        return $returnData;
    }
    public function getAll_ordersReviews($activeFilters = [], $limit = null, $offset = null, $orderDirection = null) {
        $data = $this->getAll_ordersReviews_Where($activeFilters, null, null, $orderDirection);
        $dataInfo = $this->filtrationClass->countData($data, []);

        $returnData = ($dataInfo["count"] > 0) ? $this->getAll_ordersReviews_In($dataInfo["countValues"], $limit, $offset) : [];
        return $returnData;
    }
/* ================== PRODUCTS - Units === */
    public function getAll_units($langShort = null, $activeFilters = [], $limit = null, $offset = null, $orderBy = null, $orderDirection = null) {
        $dataTable = databaseTables::getTable("products", "units");

        $whereCond = $whereBind = $langCheck = null;
        $sqlQuery = "SELECT * FROM `{$dataTable}`";

        if (!empty($activeFilters)) {
            if (isset($activeFilters["parameters"]["search"]) && $langShort) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "title LIKE CONCAT('%', :word, '%')";
                $whereBind["word"] = $activeFilters["parameters"]["search"];
            }
        }

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;
        $order = ($orderBy === null) ? "id DESC" : $orderBy;
        $order .= ($orderDirection !== null && $orderBy !== null) ? " ".$orderDirection : null;

        $sqlQuery = "{$sqlQuery} {$whereCond} ORDER BY {$order} {$limit} {$offset};";
        $returnData = $this->database->getQuery($sqlQuery, $whereBind, true);
        $this->countAndControl($returnData);

        if ($langShort !== null) {
            $productUnits = new ProductUnits($this->database, $this->appController);
            $returnData = $this->localizationClass->translateResult($returnData, $productUnits->translateType, $langShort);
        }

        return $returnData;
    }
/* ================== EMAIL TEMPLATES - Messages === */
    public function getAll_emailMessages($langShort = null, $activeFilters = [], $limit = null, $offset = null, $orderBy = null, $orderDirection = null) {
        $messagesTable = databaseTables::getTable("emailTemplates", "messages");

        $whereCond = $whereBind = $langCheck = null;
        $sqlQuery = "SELECT * FROM `{$messagesTable}`";

        if (!empty($activeFilters)) {
            if (isset($activeFilters["parameters"]["search"])) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "email LIKE CONCAT('%', :word, '%')";
                $whereBind["word"] = $activeFilters["parameters"]["search"];
            }
            if (isset($activeFilters["parameters"]["emailKey"])) {
                $whereCond .= ($whereCond === null) ? " WHERE " : " AND ";
                $whereCond .= "emailKey = :emailKey";
                $whereBind["emailKey"] = $activeFilters["parameters"]["emailKey"];
            }
        }

        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;
        $order = ($orderBy === null) ? "createdDate DESC" : $orderBy;
        $order .= ($orderDirection !== null && $orderBy !== null) ? " ".$orderDirection : null;

        $sqlQuery = "{$sqlQuery} {$whereCond} ORDER BY {$order} {$limit} {$offset};";
        $returnData = $this->database->getQuery($sqlQuery, $whereBind, true);
        $this->countAndControl($returnData);

        return $returnData;
    }
}