<?php
class filtrationClass {

    public $moduleName = "filtration";
    private $database;
    private $appController;
    private $currencyBuilder;
    private $localizationClass;
    private $langFile;

    public $pageLimit = 30;
    public $pageLimitFront = 12;
    public $systemSettings = 12;

    private $cachePricesList;

    private $sortOptions = ["default", "price-asc", "price-desc", "name-desc", "name-asc"];

    public function __construct(Database $database, AppController $appController) {
        $this->database = $database;
        $this->appController = $appController;
        $this->currencyBuilder = $appController->returnCurrencyBuilder();
        $this->localizationClass = $appController->returnLocalizationClass();

        $this->langFile = $appController->getLangFile();
        $this->systemSettings = $appController->getSettings();
    }

/* ================== PAGINATION METHODS === */

// FILTRATION - Set sort options for controller
    public function getSortOptions($customSort = null){
        if($customSort !== null){
            $this->setSortOptions($customSort);
        }

        return $this->sortOptions;
    }
    public function setSortOptions($setSort){
        if(isNotEmpty($setSort)){
            array_unshift($this->sortOptions, $setSort);
            $this->sortOptions = array_unique($this->sortOptions);
        }
    }
// PAGINATION - FrontEnd (without html structure)
    public function getPagination($dataType, $limit, $numberOfRecords, $activePage, $isAjax, $requestUrl, $apiRequestUrl = null) {

        $paginationReturn = [];

        // Get limit for results, for one reveal
        $filterPageLimit = (is_numeric($limit)) ? (($limit == 0) ? $this->pageLimit : $limit) : $this->pageLimitFront;

        if ($numberOfRecords > $filterPageLimit) {
            if ($numberOfRecords % $filterPageLimit == 0) {
                $paginationLimit = ceil($numberOfRecords / $filterPageLimit);
            } elseif ($numberOfRecords < $filterPageLimit) {
                $paginationLimit = 1;
            } else {
                $paginationLimit = ceil($numberOfRecords / $filterPageLimit);
            }

            if($activePage === 0){
                $activePage = 1;
            }

            $paginationReturn["dataType"] = $dataType;
            $paginationReturn["ajax"] = ($isAjax === true && $activePage + 1 < $paginationLimit) ? true : false;
            $paginationReturn["activePage"] = (int) $activePage;
            $paginationReturn["recordsNum"] = (int) $numberOfRecords;
            $paginationReturn["filterPageLimit"] = (int) $filterPageLimit;
            $paginationReturn["filterOffset"] = (int) ($activePage === 1) ? 0 : (($activePage - 1) * $limit);
            $paginationReturn["paginationLimit"] = (int) $paginationLimit;
            $paginationReturn["previousPath"] = ($activePage > 1) ? $this->easyFilterLink($requestUrl, 'page', $activePage - 1) : null;
            $paginationReturn["requestUrl"] = $requestUrl;

            for ($i = 1; $i <= $paginationLimit; $i++) {
                $paginationReturn["pagination"][] = ["number" => $i, "path" => $this->easyFilterLink($requestUrl, 'page', $i), "active" => (($activePage == $i) ? true : false)];
            }

            $paginationReturn["nextPath"] = ($activePage < $paginationLimit) ? $this->easyFilterLink($requestUrl, 'page', $activePage + 1) : null;

            if($apiRequestUrl !== null){
                if($paginationReturn["nextPath"] !== null){
                    $currentUrl = base64_encode($this->easyFilterLink($requestUrl, 'page', $activePage + 1));
                    $nextApiUrl = $this->easyFilterLink($apiRequestUrl, 'url', $currentUrl);
                    $paginationReturn["nextApiRequest"] = str_replace("/api", "", $nextApiUrl);
                }
            }
        }

        return $paginationReturn;
    }
// PAGINATION - BackEnd (with html structure)
    public function getAdminPagination($limit, $numberOfRecords, $activePage, $isAjax, $requestUrl) {
//TODO: Update admin pagination and filtration
        $paginationInfo = $this->getPagination(null, $limit, $numberOfRecords, $activePage, $isAjax, $requestUrl);

        echo '<span id="classicPagi" last=' . $paginationInfo["paginationLimit"] . '>';
        echo ($paginationInfo["previousPath"] !== null) ? '<a class="cont prev" href="' . $paginationInfo["previousPath"] . '"><span>&#171; ' . translate("mod-filtration-frontFilter-prev") . '</span></a>' : null;
        if (isset($paginationInfo["pagination"]) && !empty($paginationInfo["pagination"])) {
            foreach ($paginationInfo["pagination"] as $pageIndex => $pageValue) {
                $paginationTemplate = '<a class="cont ' . (($pageValue["active"] === true) ? "active" : null) . '" href="' . $pageValue["path"] . '"><span>' . $pageValue["number"] . '</span></a>';
                $lastNum = round($paginationInfo["paginationLimit"]);

                if ($paginationInfo["paginationLimit"] > 5) {
                    if ($pageIndex == 1) {
                        echo $paginationTemplate;
                        echo (!(($activePage - 2) <= 0)) ? "<span class='cont'>...</span>" : null;
                    } elseif ($pageIndex == $lastNum) {
                        echo (!(($activePage + 2) >= $lastNum)) ? "<span class='cont'>...</span>" : null;
                        echo $paginationTemplate;
                    } elseif ($pageIndex == ($activePage - 1) OR $pageIndex == $activePage OR $pageIndex == ($activePage + 1)) {
                        echo $paginationTemplate;
                    } else {
                        continue;
                    }
                } else {
                    echo $paginationTemplate;
                }
            }
        }
        echo ($paginationInfo["nextPath"] !== null) ? '<a class="cont next" href="' . $paginationInfo["nextPath"] . '"><span>' . translate("mod-filtration-frontFilter-next") . ' &#187;</span></a>' : null;
        echo '</span>';
    }

// LINKS - Generate new URL with parameters for filtration/pagination
    public function easyFilterLink($actual_link, $filter, $urlValue, $reset = false) {

        $parts = parse_url($actual_link);
        $skip = false;
        $queryParams = array();

        if (isset($parts['query'])) {
            parse_str($parts['query'], $queryParams);
        }

        // Unset active parameter
        unset($queryParams[$filter]);

        if ($filter !== 'page') {
            unset($queryParams['page']);
        }

        // Remove checkbox parameter
        if ($filter !== null && strpos($filter, '[') !== false) {
            $arrayedFilter = explode("[", $filter);
            $arrayedFilter = array("key" => $arrayedFilter[0], "name" => str_replace("]", "", $arrayedFilter[1]), "value" => $urlValue);
            if (isset($queryParams[$arrayedFilter["key"]]) AND isset($queryParams[$arrayedFilter["key"]][$arrayedFilter["name"]])) {
                if ($queryParams[$arrayedFilter["key"]][$arrayedFilter["name"]] == "true" || $queryParams[$arrayedFilter["key"]][$arrayedFilter["name"]] == $arrayedFilter["value"]) {
                    unset($queryParams[$arrayedFilter["key"]][$arrayedFilter["name"]]);
                    $skip = true;
                }
                if (empty($queryParams[$arrayedFilter["key"]])) {
                    unset($queryParams[$arrayedFilter["key"]]);
                }
            }
        }

        // Build new query
        $queryString = http_build_query($queryParams);

        // Generate URL link
        if($reset === false){
            if ($filter === 'page' && $urlValue === 1) {
                $skip = true;
            }
            if ($queryString) {
                $url = $parts['path'] . '?' . $queryString;
                if ($urlValue != "default" AND $skip === false) {
                    $url .= "&" . $filter . "=" . $urlValue;
                }
            } else {
                $url = $parts['path'];
                if ($urlValue != "default" AND $skip === false) {
                    $url .= "?" . $filter . "=" . $urlValue;
                }
            }
        }else{
            $url = $parts['path'];
        }

        return $url;
    }
// FILTRATION - Get SORTING values
    public function getFilterValue($sortTitle, $requestUrl, $filtrationParams, $paramType, $paramValue) {
        $paramShorted = substr($paramType, 0, strpos($paramType, "["));
        $paramLast = str_replace("[", "", str_replace("]", "", str_replace($paramShorted, "", $paramType)));
        
        $defaultUrl = $this->easyFilterLink($requestUrl, $paramType, $paramValue, true);

        $activeParam = ((
            (!empty($paramShorted) && isset($filtrationParams[$paramShorted][$paramLast]) && $filtrationParams[$paramShorted][$paramLast] == $paramValue)
            OR (isset($filtrationParams[$paramType]) && $filtrationParams[$paramType] == $paramValue))
            OR (!isset($filtrationParams[$paramType]) && $paramValue == "default")) ? true : false;

        return ["title" => $sortTitle, "path" => $this->easyFilterLink($requestUrl, $paramType, $paramValue), "singlePath" => $this->easyFilterLink($defaultUrl, $paramType, $paramValue),  "active" => $activeParam, "class" => (($activeParam === true) ? "active" : null)];
    }
// FILTRATION - Create FILTRATION rules && LOAD data && CREATE pagination
    public function getRulesForFiltration($dataType, $langShort, $rules = []){

    // SET Sorting settings
        $rules["sortBy"] = $this->getSortOptions($rules["sorting"]["options"]);
        if(!isset($rules["queryParams"]["sort"])){
            $rules["queryParams"]["sort"] = (!empty($rules["sorting"]["default"])) ? $rules["sorting"]["default"] : $rules["sortBy"][0];
        }

    // SET Price settings
        $rules["priceSliderMin"] = (float) (isset($rules["queryParams"]["priceMin"])) ?? 0;
        $rules["priceSliderMax"] = (float) (isset($rules["queryParams"]["priceMax"])) ?? 0;

    // SET Optimize parameters for Entity
        if(!isset($rules["optimizeParams"]) || empty($rules["optimizeParams"])){
            $rules["optimizeParams"] = ["all"];
        }

        $filtrationHelper = [
            "paginationLimit" => (isset($rules["paginationLimit"]) ? $rules["paginationLimit"] : $this->pageLimitFront),
            "activePage" => (isset($rules["activePage"]) ? $rules["activePage"] : 0),
            "ajaxPagination" => ((isset($rules["ajaxPagination"]) && $rules["ajaxPagination"] === true) ? true : false),
            "requestUrl" => (isset($rules["requestUrl"]) ? $rules["requestUrl"] : null),
        ];
        $filtrationInfo = [
            "count" => 0,
            "data" => [],
            "priceRules" => [
                "priceMin_value" => $rules["priceSliderMin"],
                "priceMax_value" => $rules["priceSliderMax"],
                "priceMin" => (string) $this->currencyBuilder->buildRoundedPrice($langShort, $rules["priceSliderMin"]),
                "priceMax" => (string)$this->currencyBuilder->buildRoundedPrice($langShort, $rules["priceSliderMax"]),
                "priceCurrency" => (string)$this->currencyBuilder->getCurrencySymbol($langShort),
                "priceLimitMin_value" => (float) 0,
                "priceLimitMax_value" => (float) 0,
                "priceLimitMin" => (string) 0,
                "priceLimitMax" => (string) 0,
            ],
            "activeCategory" => $rules["categoryId"],
            "defaultSort" => $rules["sortBy"][0],
            "activeFilters" => (isset($rules["queryParams"])) ? $this->reParseGetParameters($rules["queryParams"]) : null,
            "resetFilter" => $this->easyFilterLink($filtrationHelper["requestUrl"], null, null, true)
        ];
        $filtrationInfo = $this->filterByDataType($dataType, $langShort, $rules, $filtrationInfo, $filtrationHelper);
        $filtrationInfo["pagination"] = $this->getPagination($dataType, $filtrationHelper["paginationLimit"], $filtrationInfo["count"], $filtrationHelper["activePage"], $filtrationHelper["ajaxPagination"], $filtrationHelper["requestUrl"], $rules["apiRequestUrl"]);
        if(isset($rules["sortBy"])){
            foreach($rules["sortBy"] as $sort){
                $filtrationInfo["sortBy"][] = $this->getFilterValue(translate("mod-filtration-sortByPlc-".$sort), $rules["requestUrl"], $rules["queryParams"], "sort", $sort);
            }
        }

        $results = $this->restructureFilterResponse($filtrationInfo);
        return $results;
    }
    public function getAjaxLoadResults($dataType, $langShort, $rules = []){
        $activeFilters = (isset($rules["queryParams"])) ? $this->reParseGetParameters($rules["queryParams"]) : null;
        $filtrationHelper = [
            "paginationLimit" => (isset($rules["paginationLimit"]) ? $rules["paginationLimit"] : $this->pageLimitFront),
            "activePage" => (isset($rules["activePage"]) ? $rules["activePage"] : 0),
            "ajaxPagination" => ((isset($rules["ajaxPagination"]) && $rules["ajaxPagination"] === true) ? true : false),
            "requestUrl" => (isset($rules["requestUrl"]) ? $rules["requestUrl"] : null),
        ];

        $filtrationInfo = $this->filterByDataType($dataType, $langShort, $rules, ["activeFilters" => $activeFilters], $filtrationHelper);
        $filtrationInfo["pagination"] = $this->getPagination($dataType, $filtrationHelper["paginationLimit"], $filtrationInfo["count"], $filtrationHelper["activePage"], $filtrationHelper["ajaxPagination"], $filtrationHelper["requestUrl"]);

        return $this->restructureFilterResponse($filtrationInfo);
    }
    private function restructureFilterResponse(Array $filtrationInfo){
        $response = [];
        $response["data"] = $filtrationInfo["data"];
        $response["count"] = $filtrationInfo["count"];
        $response["pagination"] = $filtrationInfo["pagination"];
        $response["filtration"]["activeFilters"] = $filtrationInfo["activeFilters"];
        $response["filtration"]["activeFilters"]["priceRules"] = $filtrationInfo["priceRules"];

        if(isset($filtrationInfo["activeCategory"])){
            $response["filtration"]["activeCategory"] = $filtrationInfo["activeCategory"];
        }
        if(isset($filtrationInfo["resetFilter"])){
            $response["filtration"]["resetFilter"] = $filtrationInfo["resetFilter"];
        }
        if(isset($filtrationInfo["sortBy"])){
            $response["filtration"]["sortBy"] = $filtrationInfo["sortBy"];
        }
        if(isset($filtrationInfo["defaultSort"])){
            $response["filtration"]["defaultSort"] = $filtrationInfo["defaultSort"];
        }
        /*if(isset($filtrationInfo["countValues"])){
            $response["countValues"] = $filtrationInfo["countValues"];
        }*/

        return $response;
    }

// FILTRATION - Get filtered results
    public function reParseGetParameters($GET) {
        $sortBy = null;
        $parameters = [];

        foreach ($GET as $key => $value) {
            if ((empty($value) && $value != 0) OR $value == "") {
                continue;
            } elseif ($key == "sort") {
                $sortBy = $value;
            } else {
                $parameters[$key] = $value;
            }
        }

        $activeParameters = $parameters;
        if(isset($activeParameters["page"])){
            unset($activeParameters["page"]);
        }

        return ["sortBy" => $sortBy, "activeParameters" => ((!empty($activeParameters))), "parameters" => $parameters];
    }
    public function countData($allData, $filtrationInfo, $customColumn = 'id'){
        $filtrationInfo["countValues"] = implode(",", array_column($allData, $customColumn));
        $filtrationInfo["count"] = count($allData);

        return $filtrationInfo;
    }

    private function filterByDataType($dataType, $langShort, $rules, $filtrationInfo, $filtrationHelper) {
        $filterModules = new filterToModules($this->database, $this->appController);
        $filtrationHelper["filterOffset"] = ($filtrationHelper["activePage"] === 1) ? 0 : (($filtrationHelper["activePage"] - 1) * $filtrationHelper["paginationLimit"]);
        $filtrationInfo["data"] = [];

        if($dataType === "products"){
            $filtrationInfo["activeFilters"]["parameters"]["saleOption"] = 1;

            if($this->appController->isActiveModule('optysPartner')){
                $optysProcessor = new \OptysProcessor($this->database, $this->appController);
                $filtrationInfo["activeFilters"] = $optysProcessor->extendFiltrationParams($filtrationInfo["activeFilters"]);
            }

            $productClass = new productsClass($this->database, $this->appController);
            $return_productInfo = $productClass->getFiltratedProducts($langShort, (isset($rules["categoryId"]) && $rules["categoryId"] !== 0) ? $rules["categoryId"] : false, $filtrationInfo["activeFilters"], true, null, null);
            if($this->appController->isActiveModule('optysPartner')){
                $optysProcessor = new \OptysProcessor($this->database, $this->appController);
                $return_productInfo = $optysProcessor->sortSubjectProducts($return_productInfo);
            }

            if(!empty($return_productInfo)){
                function createParameterSQL($paramType, $alias, $type){
                    return '(SELECT SUM(easyParams.paramCharge) totalCharge
                FROM ( 
                    SELECT paramHook.productId, paramHook.paramId, paramHook.valueId, '.$type.'(CAST(paramHook.paramCharge AS DECIMAL(9,3))) AS paramCharge, paramInfo.type
                    
                    FROM `products-paramshook` AS paramHook
                    INNER JOIN `products-params` AS paramInfo
                    ON paramInfo.id = paramHook.paramId
                    
                    WHERE paramHook.productId = :productId AND paramInfo.`type` = \''.$paramType.'\' AND paramHook.paramCharge != ""
                    GROUP BY paramHook.paramId
                ) easyParams
            ) AS '.$alias;
                }
                $sql_allProductParametersMaxPrices = "SELECT ".createParameterSQL("easy", "requiredCharge", "MAX").", ".createParameterSQL("multiple", "additionalCharge", "MAX")."";
                $sql_allProductParametersMinPrices = "SELECT ".createParameterSQL("easy", "requiredCharge", "MIN")."";

                foreach($return_productInfo as &$product){
                    $translatedProductPrices = $this->localizationClass->updateExistingTranslationByTableName($langShort, $this->localizationClass->mainLanguage, $productClass->translateTable, $product);
                    if(!empty($translatedProductPrices)){
                        $product = array_merge($product, $translatedProductPrices);
                    }

                    $priceDiscount = $productClass->countPriceDiscount($product["discountType"], $product["discountValue"], $product["price"]);
                    switch($product["type"]){
                        case "parameters":
                            $return_paramPrices_Max = $this->database->getQuery($sql_allProductParametersMaxPrices, ["productId" => $product["id"]], false);
                            $return_paramPrices_Min = $this->database->getQuery($sql_allProductParametersMinPrices, ["productId" => $product["id"]], false);

                            $productPriceSum = ($product["price"] + $return_paramPrices_Max["requiredCharge"] + $return_paramPrices_Max["additionalCharge"]);
                            $productPriceMinimal = ($product["price"] + $return_paramPrices_Min["requiredCharge"]);

                            $productPrices = $this->getPrice($langShort, $product["tax"], $productPriceSum, $priceDiscount);
                            $productPricesMinimal = $this->getPrice($langShort, $product["tax"], $productPriceMinimal, $priceDiscount);
                            break;
                        case "combination":
                            $productCombiPrice_Max = $this->database->getQuery("SELECT price, (price * ((tax/100)+1)) AS priceTax, tax FROM `products-variants` WHERE productId = :productId ORDER BY priceTax DESC LIMIT 1", ["productId" => $product["id"]], false);
                            $productCombiPrice_Min = $this->database->getQuery("SELECT price, (price * ((tax/100)+1)) AS priceTax, tax FROM `products-variants` WHERE productId = :productId ORDER BY priceTax ASC LIMIT 1", ["productId" => $product["id"]], false);

                            $priceDiscount_max = $productClass->countPriceDiscount($product["discountType"], $product["discountValue"], $productCombiPrice_Max["price"]);
                            $productPrices = $this->getPrice($langShort, $productCombiPrice_Max["tax"], $productCombiPrice_Max["price"], $priceDiscount_max);

                            $priceDiscount_min = $productClass->countPriceDiscount($product["discountType"], $product["discountValue"], $productCombiPrice_Min["price"]);
                            $productPricesMinimal = $this->getPrice($langShort, $productCombiPrice_Min["tax"], $productCombiPrice_Min["price"], $priceDiscount_min);
                            break;
                        default:
                            $productPrices = $this->getPrice($langShort, $product["tax"], $product["price"], $priceDiscount);
                            $productPricesMinimal = 0;
                            break;
                    }
                    if($productPrices["calculatePrice_tax"] > $filtrationInfo["priceRules"]["priceLimitMax_value"]){
                        $filtrationInfo["priceRules"]["priceLimitMax"] = $productPrices["price_tax"];
                        $filtrationInfo["priceRules"]["priceLimitMax_value"] = $productPrices["calculatePrice_tax"];
                    }
                    $product["filterPrice"] = ($productPricesMinimal !== 0) ? $productPricesMinimal["calculatePrice_tax"] : $productPrices["calculatePrice_tax"];
                }

                $rules["limit"] = $filtrationHelper["paginationLimit"];
                $rules["offset"] = $filtrationHelper["filterOffset"];
                $filtrationInfo = $this->getFiltratedResults($return_productInfo, $filtrationInfo, $rules);
                $filtrationInfo["data"] = $productClass->optimize($filtrationInfo["data"], $langShort, true, ["itemThumbnail" => $rules["itemThumbnail"], "with" => $rules["optimizeParams"]]);

                if($filtrationInfo["priceRules"]["priceMax_value"] == 0){
                    $filtrationInfo["priceRules"]["priceMax"] = $filtrationInfo["priceRules"]["priceLimitMax"];
                    $filtrationInfo["priceRules"]["priceMax_value"] = $filtrationInfo["priceRules"]["priceLimitMax_value"];
                }
            }
        }
        elseif($dataType === "blogPosts"){
            $cmsPostBlog = new cmsPostBlog($this->database, $this->appController);
            $filtrationInfo["activeFilters"]["parameters"]["type"] = 2;
            if(isset($rules["categoryId"]) && $rules["categoryId"] !== 0){
                $filtrationInfo["activeFilters"]["parameters"]["category"] = $rules["categoryId"];
            }

            $return_dataInfo = $filterModules->getAll_Posts_Where(
                $langShort,
                $filtrationInfo["activeFilters"],
                true,
                null,
                null,
                'createdDate',
                'DESC'
            );

            if(!empty($return_dataInfo)){
                $rules["limit"] = $filtrationHelper["paginationLimit"];
                $rules["offset"] = $filtrationHelper["filterOffset"];
                $filtrationInfo = $this->getFiltratedResults($return_dataInfo, $filtrationInfo, $rules);
                $filtrationInfo["data"] = $cmsPostBlog->optimize($filtrationInfo["data"], $langShort, ["itemThumbnail" => $rules["itemThumbnail"], "with" => $rules["optimizeParams"]]);
            }
        }

        return $filtrationInfo;
    }
    private function getPrice($langShort, $tax, $price, $discount){
        $identify = (string) ("{$langShort}-{$tax}-{$price}-{$discount}");
        if(isset($this->cachePricesList[$identify])){
            return $this->cachePricesList[$identify];
        }else{
            $price = $this->currencyBuilder->buildPrice($langShort, $tax, $price, $discount, null);
            $this->cachePricesList[$identify] = $price;
            return $price;
        }
    }

    private function getFiltratedResults($items, $filtrationInfo, $rules = []) {
    // SORT
        if(!empty($items)){
            $activeFilters = $filtrationInfo["activeFilters"];
            if (isset($activeFilters["parameters"]["priceMin"]) && isset($activeFilters["parameters"]["priceMax"])) {
                $items = array_filter($items, function ($item) use ($activeFilters) {
                    return $item["filterPrice"] >= $activeFilters["parameters"]["priceMin"] && $item["filterPrice"] <= $activeFilters["parameters"]["priceMax"];
                });
            }
            if(!empty($items)){
                if(isset($activeFilters["sortBy"])){
                    switch($activeFilters["sortBy"]){
                        case "name-desc":
                            usort($items, function ($a, $b) { return $a['title'] <=> $b['title']; });
                            break;
                        case "name-asc":
                            usort($items, function ($a, $b) { return $b['title'] <=> $a['title']; });
                            break;
                        case "price-desc":
                            usort($items, function ($a, $b) { return strnatcmp($b["filterPrice"], $a["filterPrice"]); });
                            break;
                        case "price-asc":
                            usort($items, function ($a, $b) { return strnatcmp($a["filterPrice"], $b["filterPrice"]); });
                            break;
                    }
                }

                foreach($rules["sorting"] as $sortType => $sortValues){
                    $sortGroups = [];
                    if($sortType === "byAvailability" && $sortValues === true){
                        $sortGroups = ["inStock" => [], "others" => [], "outOfStock" => []];
                    }
                    if($sortType === "byLabels" && !empty($sortValues) && $activeFilters["sortBy"] == "default"){
                        $sortGroups = array_fill_keys(array_merge($rules["sorting"]["byLabels"], ["others"]), []);
                    }
                    if($sortType === "byDiscount" && $sortValues === true && $activeFilters["sortBy"] == "default"){
                        $sortGroups = ["hasDiscount" => [], "others" => []];
                    }

                    if(!empty($sortGroups)){
                        foreach($items as $item){
                            // AVAILABILITY
                            if($sortType === "byAvailability"){
                                $validAvailability = false;
                                if($item["availability"] == 1){
                                    $validAvailability = true;
                                    $sortGroups["inStock"][] = $item;
                                }elseif($item["availability"] == 2){
                                    $validAvailability = true;
                                    $sortGroups["outOfStock"][] = $item;
                                }else{
                                    if($this->appController->isActiveModule('esoOptys')){
                                        if($item["availability"] == 10){
                                            $validAvailability = true;
                                            $sortGroups["outOfStock"][] = $item;
                                        }
                                    }
                                }
                                if($validAvailability === false){
                                    $sortGroups["others"][] = $item;
                                }
                            }
                            // LABEL sorting
                            if($sortType === "byLabels"){
                                $validLabel = false;
                                if(isset($item["labelTags"]) && !empty($item["labelTags"])){
                                    $labelTags = explode(',', $item["labelTags"]);
                                    if(!empty($labelTags)){
                                        $matchedLabels = array_intersect($rules["sorting"]["byLabels"], $labelTags);
                                        if(!empty($matchedLabels)){
                                            $validLabel = true;
                                            $sortGroups[$matchedLabels[0]][] = $item;
                                        }
                                    }
                                }
                                if($validLabel === false){
                                    $sortGroups["others"][] = $item;
                                }
                            }
                            // DISCOUNT
                            if($sortType === "byDiscount"){
                                $validDiscount = false;
                                if($item["discountValue"] > 0){
                                    $validDiscount = true;
                                    $sortGroups["hasDiscount"][] = $item;
                                }
                                if($validDiscount === false){
                                    $sortGroups["others"][] = $item;
                                }
                            }
                        }

                        $items = [];
                        foreach($sortGroups as $sortResults){
                            $items = array_merge($items, $sortResults);
                        }
                    }
                }
            }

            $items = array_values($items);
        }
    // COUNT results
        $filtrationInfo = $this->countData($items, $filtrationInfo);
    // LIMIT
        if(isset($rules["limit"]) || isset($rules["offset"])) {
            $limit = (isset($rules["limit"])) ? (int) $rules["limit"] : null;
            $offset = (isset($rules["offset"])) ? (int) $rules["offset"] : null;

            $items = array_slice($items, $offset, $limit);
        }

        $filtrationInfo["data"] = $items;
        return $filtrationInfo;
    }
    public function getFilterOptions(String $dataType, Array $requestedOptions, String $langShort = null, Array $parameters = []){
        $enabledFilterOptions = ["enabled" => 0, "data" => []];

        $langShort = ($langShort !== null) ? $langShort : $this->appController->getLang();
        $categoryId = (isset($parameters["categoryId"])) ? $parameters["categoryId"] : 0;
        $requestUrl = (isset($parameters["url"])) ? $parameters["url"] : "/";
        $getParams = (isset($parameters["getParams"])) ? $parameters["getParams"] : [];

        if($dataType === 'products' && in_array('productManufacturers', $requestedOptions)){
            $manufacturerClass = new manufacturerClass($this->database, $this->appController);

            $enabledFilterOptions["data"]["productManufacturers"] = [];
            $allManufacturers = $manufacturerClass->getAllManufacturersForFilter($langShort, $categoryId);
            if (!empty($allManufacturers)) {
                $enabledFilterOptions["enabled"]++;

                foreach ($allManufacturers as $key => $manufacturer){
                    $enabledFilterOptions["data"]["productManufacturers"][$key] = $manufacturer;
                    $enabledFilterOptions["data"]["productManufacturers"][$key]["filterName"] = "manufacturer[{$manufacturer["id"]}]";
                    $enabledFilterOptions["data"]["productManufacturers"][$key]["anchor"] = $this->getFilterValue(null, $requestUrl, $getParams, $enabledFilterOptions["data"]["productManufacturers"][$key]["filterName"] , "true");
                }
            }
        }
        if($dataType === 'products' && in_array('productLabels', $requestedOptions)){
            $labelsClass = new productLabels($this->database, $this->appController);

            $enabledFilterOptions["data"]["productLabels"] = [];
            $allLabels = $labelsClass->getAllCategoryLabels($langShort, $categoryId);
            if (!empty($allLabels)) {
                $enabledFilterOptions["enabled"]++;

                foreach ($allLabels as $key => $label){
                    $enabledFilterOptions["data"]["productLabels"][$key] = $label;
                    $enabledFilterOptions["data"]["productLabels"][$key]["filterName"] = "label[{$label["shortTag"]}]";
                    $enabledFilterOptions["data"]["productLabels"][$key]["anchor"] = $this->getFilterValue(null, $requestUrl, $getParams, $enabledFilterOptions["data"]["productLabels"][$key]["filterName"], "true");
                }
            }
        }
        if($dataType === 'products' && in_array('productParameters', $requestedOptions)){
            $parametersClass = new productParams($this->database, $this->appController);

            $enabledFilterOptions["data"]["productParameters"] = [];
            $allParameters = $parametersClass->getAllCategoryParameters($langShort, $categoryId);
            if (!empty($allParameters)) {
                $prefixEngine = new prefixEngine($this->database);

                $enabledFilterOptions['enabled'] = 0;
                $paramIdentify = [];
                $paramValuesArray = [];
                $uniqColors = [];

                foreach ($allParameters as $parameter) {
                    $enabledFilterOptions['enabled']++;

                    $paramIdentify[$parameter['identificator']] ??= count($paramIdentify);
                    $paramGroupIndex = $paramIdentify[$parameter['identificator']];
                    $parameterInfo = $parameter;

                    $paramGroup = &$enabledFilterOptions['data']['productParameters'][$paramGroupIndex];
                    if (!isset($paramGroup)) {
                        $paramGroup = ['title' => $parameter['title'], 'identificator' => $parameter['identificator'], 'data' => []];
                    }
                    $paramUniq = $parameterInfo['paramId'] . '_' . $prefixEngine->title2pagename($parameterInfo['paramValue']);
                    $paramColorKey = ($this->systemSettings['groupParamsColors'] === 'true') ? 'paramColorGroup' : 'paramColor';
                    $paramColor = $parameterInfo[$paramColorKey];

                    if (!in_array($paramUniq, $paramValuesArray)) {
                        $addParam = true;

                        if ($parameter['isColor'] === 1) {
                            $addParam = !in_array($paramColor, $uniqColors);
                            if ($addParam) {
                                $parameterInfo['valueId'] = str_replace('#', '', $paramColor);
                            }
                        }

                        $parameterInfo['filterName'] = "parameter[{$parameterInfo['valueId']}]";
                        $parameterInfo['anchor'] = $this->getFilterValue(null, $requestUrl, $getParams, $parameterInfo['filterName'], $parameterInfo['paramId']);

                        if ($addParam) {
                            $paramGroup['data'][] = $parameterInfo;
                            $paramValuesArray[] = $paramUniq;
                            $uniqColors[] = $paramColor;

                            usort($paramGroup['data'], fn($a, $b) => strcmp($a[($parameter['isColor'] === 1) ? $paramColorKey : 'paramValue'], $b[($parameter['isColor'] === 1) ? $paramColorKey : 'paramValue']));
                        }
                    }
                }
                usort($enabledFilterOptions['data']['productParameters'], fn($a, $b) => strcmp($a['title'], $b['title']));
            }
        }
        if($dataType === 'blogPosts' && in_array('postLabels', $requestedOptions)){
            // Same logic is in cmsPostBlog - keep on both places
            $cmsPostBlog = new cmsPostBlog($this->database, $this->appController);

            $enabledFilterOptions["data"]["postLabels"] = [];
            $allLabels = $cmsPostBlog->getAllCategoryLabels($langShort, $categoryId);
            if (!empty($allLabels)) {
                $enabledFilterOptions["enabled"]++;
                $groupsData = $cmsPostBlog->getAllLabelsGroups($langShort);
                $groupTitles = array_column($groupsData, 'title', 'id');
                $groupTags = array_column($groupsData, 'shortTag', 'id');

                foreach ($allLabels as $key => $label){
                    $labelData = $label;
                    $labelData["filterName"] = "label[{$labelData["shortTag"]}]";
                    $labelData["anchor"] = $this->getFilterValue(null, $requestUrl, $getParams, $labelData["filterName"], "true");

                    $enabledFilterOptions["data"]["postLabels"][$label["groupId"]]["id"] = $label["groupId"];
                    $enabledFilterOptions["data"]["postLabels"][$label["groupId"]]["title"] = $groupTitles[$label["groupId"]];
                    $enabledFilterOptions["data"]["postLabels"][$label["groupId"]]["shortTag"] = $groupTags[$label["groupId"]];
                    $enabledFilterOptions["data"]["postLabels"][$label["groupId"]]["values"][] = $labelData;
                }

                $enabledFilterOptions["data"]["postLabels"] = array_values($enabledFilterOptions["data"]["postLabels"]);
            }
        }

        $enabledFilterOptions["enabled"] = ($enabledFilterOptions["enabled"] > 0) ? true : false;

        return $enabledFilterOptions;
    }
}  