<?php
    $callback["data"] = [];

    if(isset($request["data"]["dataType"]) && isset($request["data"]["url"])) {
        $database = (isset($this->database)) ? $this->database : $database;
        $appController = (isset($this->appController)) ? $this->appController : $appController;
        $filtration = new filtrationClass($this->database, $this->appController);

        $resultsLimit = (isset($request["data"]["limit"])) ? $request["data"]["limit"] : 6;
        $templateName = (isset($request["data"]['templateName'])) ? $request["data"]['templateName'] : null;
        $itemThumbnail = (isset($request["data"]['thumbnail'])) ? $request["data"]['thumbnail'] : null;
        $langShort = (isset($request["data"]["langShort"])) ? $request["data"]["langShort"] : $appController->getLang();

        $rules = [
            "requestUrl" => $request["data"]["url"],
            "queryParams" => (!empty($request["data"]["queryParams"])) ? $request["data"]["queryParams"] : null,
            "activePage" => (isset($request["data"]["activePage"])) ? $request["data"]["activePage"] : 1,
            "sorting" => $request["data"]["sorting"],

            "categoryId" => $request["data"]["activeCategory"],
            "paginationLimit" => $request["data"]["limit"],
            "ajaxPagination" => true,

            "itemThumbnail" => $request["data"]["thumbnail"],
            "optimizeParams" => $request["data"]["with"],

            "dataType" => $request["data"]["dataType"],
        ];
        (!isset($rules["getParams"]["sort"])) ? $rules["getParams"]["sort"] = $rules["defaultSort"] : null;

        $paginationResults = $filtration->getAjaxLoadResults($rules["dataType"], $langShort, $rules);
        $callback = $paginationResults;

        if($paginationResults["count"] > 0){
            if(isExisting('returnTemplate', $request["data"])){
                $pageContent = null;
                if(!empty($paginationResults["data"])){
                    foreach($paginationResults["data"] as $product){
                        $pageContent .= renderReturn('static.products_productWrap', ["product" => $product]);
                    }
                }

                return [
                    "data" => $pageContent,
                    "pagination" => renderReturn('static.filtration_pagination', ["pagination" => $paginationResults["pagination"], "activeCategory" => $rules["categoryId"], "defaultSort" => $rules["defaultSort"], "labelSort" => implode(",", $rules["labelSort"])])
                ];
            }
        }

    }

    return $callback;