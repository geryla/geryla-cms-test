SET FOREIGN_KEY_CHECKS=0;

-- DELETE TABLES
DROP TABLE `mediamanager`;
DROP TABLE `mediamanager-lang`;
DROP TABLE `mediamanager-folders`;

-- DELETE ALL TRANSLATION by translateType from module CONFIG
DELETE FROM `language-translations` WHERE objectType = 'mediaFolder';

SET FOREIGN_KEY_CHECKS=1;