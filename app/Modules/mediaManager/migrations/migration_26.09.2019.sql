-- CREATE TABLE for MEDIA
CREATE TABLE IF NOT EXISTS `mediamanager` (
  `id` int(10) NOT NULL AUTO_INCREMENT,

  `url` varchar(255) NOT NULL,
  `objectId` int(10) NULL DEFAULT '0',
  `template` varchar(40) NULL,
  `head` int(1) NULL DEFAULT '0',
  `grouped` int(1) NULL DEFAULT '0',
  `duplicated` int(1) NOT NULL DEFAULT '0',
  `fileType` varchar(255) NOT NULL,
  `fileSize` int(255) NULL DEFAULT '0',
  `folder` int(11) NOT NULL DEFAULT '0',

  `orderNum` int(10) NOT NULL DEFAULT '0',
  `createdDate` datetime NOT NULL,
  `updatedDate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- CREATE TABLE for MEDIA LANGUAGE
CREATE TABLE IF NOT EXISTS `mediamanager-lang` (
  `id` int(10) NOT NULL AUTO_INCREMENT,

  `associatedId` int(10) NOT NULL,
  `title` varchar(75) NULL,
  `alt` varchar(75) NULL,

  `langShort` varchar(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `associatedId` (`associatedId`),
  FOREIGN KEY (`associatedId`) REFERENCES `mediamanager` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- CREATE TABLE for MEDIA FOLDERS
CREATE TABLE IF NOT EXISTS `mediamanager-folders` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parentId` int(10) NOT NULL,
  `title` varchar(75) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;