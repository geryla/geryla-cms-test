<?php
    $config = [
        "moduleName" => "Media manager",
        "moduleUrlName" => "mediaManager",
        "moduleLangName" => "mediaManager",
        "moduleIcon" => "fa-file-image-o",
        "moduleVersion" => "5.0.0",
        "requiredModules" => [
            ["moduleName" => "localization"],
        ],

        "modulePaginationLimit" => "18",
        "mediaSettings" => [],
        "translateType" => [
            "mediaFolder" => "mediaFolder"
        ],
        "databaseTables" => [
            "files" => [
                "name" => "mediamanager",
            ],
            "lang" => [
                "name" => "mediamanager-lang",
            ],
            "folders" => [
                "name" => "mediamanager-folders",
            ],
        ],

        "modulePages" => [
            [
                "pageId" => 1,
                "urlName" => "media-list",
                "url" => "media-list",
                "parent" => 0,
                "inMenu" => 1,
                "headPage" => 1
            ],
            [
                "pageId" => 2,
                "urlName" => "upload-media",
                "url" => "upload-media",
                "parent" => 1,
                "inMenu" => 0,
                "headPage" => 0
            ]
        ]
    ];