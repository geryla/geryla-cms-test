<?php
if(isset($file) && isset($fileActions)){
    $fileInfo = $mediaProcessor->getFileInfo($file);

    echo '<div class="imageBox" id="'.$fileInfo["id"].'" data-template="'.$file["template"].'">';
        echo '<div class="image">';
            echo '<div class="wrap">';
                echo $mediaProcessor->generatePreview($file, "120");
            echo '</div>';
            echo '<div class="actions">';
                $deleteText = (isset($render)) ? $render->deleteText : null;
                $mediaProcessor->getFileActions($fileInfo, $fileInfo["url"], $fileActions["delete"], $deleteText, $fileActions["sort"], $fileActions["head"], $fileActions["group"], $fileActions["selector"]);
            echo '</div>';
        echo '</div>';
        echo '<div class="name">';
            echo '<span>'.$fileInfo["viewName"].'</span>';
        echo '</div>';
    echo '</div>';
}