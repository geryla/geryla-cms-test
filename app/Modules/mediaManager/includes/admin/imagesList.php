<?php
    $render->contentDiv(true, 'wrap_'.$pageId, "listOfFiles");
    if(!empty($allImages)) {
        echo '<div class="sortableWrap" data-name="mediaManager" data-axis="x,y">';
        foreach ($allImages as $file) {
            $fileActions = ["delete" => false, "sort" => true, "head" => (($file["head"] == 1) ? true : false), "group" => (($file["grouped"] == 1) ? true : false), "selector" => false];
            include(MODULE_DIR . "mediaManager/includes/admin/fileWrap.php");
        }
        echo '</div>';
    }
    $render->contentDiv(false);