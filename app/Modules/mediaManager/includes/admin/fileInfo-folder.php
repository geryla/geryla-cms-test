<?php
if($activeFolder != 0){
    $render->contentDiv(true, null, "infoBlock folderInfo");
        echo '<h2>'.translate("mod-mediaManager-files-folderInfo").':</h2>';
        $activeFolderTitle = ($activeFolder == 0 ) ? translate("mod-mediaManager-files-route-main") : $folderInfo["title"];
        $render->contentRow(null, "rowBox", '<h5>'.translate("mod-mediaManager-list-titulek").'</h5>', null, $render->input("text", "edit-title", $activeFolderTitle, null, false, null, null) );
        $render->contentDiv(true, null, "optionsAll");
            $render->contentDiv(true, null, "option");
                echo '<a class="saveFolderDetail btn success" data-id="'.$folderInfo["id"].'" data-type="folder"><h5>'.translate("mod-mediaManager-list-save").'</h5></a>';
            $render->contentDiv(false);
            if(empty($pageData)){
                $render->contentDiv(true, null, "option");
                    echo $render->deleteAnchorCustomClass('data-id="'.$folderInfo["id"].'" data-type="folder"', 'class="deleteFolder btn error"', '<h5>'.translate("mod-mediaManager-list-delete").'</h5>');
                $render->contentDiv(false);
            }
            $render->contentDiv(true, null, "option");
                echo '<a id="closeFolderEdit" class="btn warning"><h5>'.translate("mod-mediaManager-list-close").'</h5></a>';
            $render->contentDiv(false);
        $render->contentDiv(false);
    $render->contentDiv(false);
}