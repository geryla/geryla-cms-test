<?php
function createList($folders, $activeFolder, $repeat = 0){
    $foldersList = "";

    if(!empty($folders)){
        $foldersList .= '<ul class="subcategory" '.(($repeat === 0) ? 'style="display: block;"' : null).'>';
        foreach($folders as $folderName){
            $foldersList .= '<li id="'.$folderName["id"].'" class="'.(($activeFolder === $folderName["id"]) ? 'active' : null).' '.(($folderName["child"] && $repeat !== 0) ? 'hasChild' : null).'">';
                $foldersList .= ($folderName["child"]) ? '<i class="fa fa-plus-square-o fa-minus-square-o"></i>' : (($repeat === 0) ? '<i class="fa fa-empty"></i>' : null);
                $foldersList .= '<a href="?folder='.$folderName["id"].'" folder-id="'.$folderName["id"].'" class="folder">';
                    $foldersList .= ($folderName["child"]) ? '<i class="fa fa-folder"></i>' : '<i class="fa fa-folder-o"></i>';
                    $foldersList .= '<span>'.$folderName["title"].'</span>';
                $foldersList .= '</a>';
                $foldersList .= createList($folderName["child"], $activeFolder, $repeat+1);
            $foldersList .= '</li>';
        }
        $foldersList .= '</ul>';
    }

    return $foldersList;
}

echo '<div class="foldersList categoryCheck">';
    echo '<ul class="subcategory root">';
        echo '<li id="0" '.(($activeFolder === 0) ? 'class="active"' : null).'>';
            echo '<a href="'.(isset($refreshPage) ? $refreshPage : '#').'" class="folder">';
                echo (!empty($foldersData)) ? '<i class="fa fa-folder"></i>' : '<i class="fa fa-folder-o"></i>';
                echo '<span>'.translate("mod-mediaManager-files-route-main").'</span>';
            echo '</a>';
            echo createList($foldersData, $activeFolder, 0);
        echo '</li>';
    echo '</ul>';
echo '</div>';