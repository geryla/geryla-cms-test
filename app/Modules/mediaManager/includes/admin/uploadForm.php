<?php
echo '<section class="fileUpload">';
  echo '<div id="drag-and-drop-zone" class="dm-uploader" data-upload="'.translate("mod-mediaManager-status-upload").'" data-canceled="'.translate("mod-mediaManager-status-canceled").'" data-success="'.translate("mod-mediaManager-status-success").'" data-error="'.translate("mod-mediaManager-status-error").'">';
    echo '<h3>'.translate("mod-mediaManager-select_files").'</h3>';
    echo '<label class="btn inverted">';
      echo '<span>'.translate("mod-mediaManager-select_files_here").'</span>';
      echo '<input type="file" />';
      echo '<input type="hidden" name="uploadFolder" value="'.$activeFolder.'" />';
    echo '</label>';
  echo '</div>';

  echo '<ul class="list-unstyled p-2 d-flex flex-column col" id="files">';
  echo '</ul>';

  echo '<script type="text/html" id="files-template">';
    echo '<li class="media">';
      echo '<div class="media-image">';
        echo '<i class="fa fa-picture-o"></i>';
        echo '<b>'.translate("mod-mediaManager-noImage").'</b>';
      echo '</div>';
      echo '<div class="media-body">';
        echo '<p class="status">';
          echo '<strong>%%filename%%</strong> '.translate("mod-mediaManager-status").': <span>'.translate("mod-mediaManager-status-waiting").'</span>';
        echo '</p>';
        echo '<div class="progress">';
          echo '<div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>';
        echo '</div>';
      echo '</div>';
    echo '</li>';
  echo '</script>';
echo '</section>';