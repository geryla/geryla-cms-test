<?php
    echo '<div class="allFiles full">';
        echo '<div class="addFileWrap">';
            echo '<a class="btn icon folder-open warning js_addFile" data-objectid="'.$pageId.'" data-template="'.$mediaInfo["name"].'" data-limit="'.(isset($mediaInfo["limit"]) ? $mediaInfo["limit"] : null).'">';
                echo translate("mod-mediaManager-list-choice");
            echo '</a>';
            if(isExisting('limit', $mediaInfo)){
                echo '<span class="limit_files">'.translate("mod-mediaManager-list-limit_images").': '.$mediaInfo["limit"].'</span>';
            }
        echo '</div>';

        echo '<div class="filesList">';
            echo '<div class="filesWrap">';
                echo '<div class="detailsWrap">';
                    include(MODULE_DIR."mediaManager/includes/admin/fileInfo-file.php");
                echo '</div>';
                echo '<div id="loadedFilesWrap">';
                    require(MODULE_DIR . 'mediaManager/includes/admin/imagesList.php');
                echo '</div>';
            echo '</div>';
        echo '</div>';
    echo '</div>';