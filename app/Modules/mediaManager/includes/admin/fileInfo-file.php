<?php
$render->contentDiv(true, null, "infoBlock fileInfo");
    echo '<h2>'.translate("mod-mediaManager-files-fileInfo").': <span></span></h2>';
    $render->contentRow(null, "rowBox", '<h5>'.translate("mod-mediaManager-list-titulek").'</h5>', null, $render->input("text", "edit-title", "", null, false, null, null) );
    $render->contentRow(null, "rowBox last", '<h5>'.translate("mod-mediaManager-list-alt").'</h5>', null, $render->input("text", "edit-alt", "", null, false, null, null) );
    $render->contentDiv(true, null, "optionsAll");
        $render->contentDiv(true, null, "option");
            echo '<a class="saveFileDetail btn success" data-id="" data-type="file"><h5>'.translate("mod-mediaManager-list-save").'</h5></a>';
        $render->contentDiv(false);
        $render->contentDiv(true, null, "option");
            echo '<a class="hideInfoBlock btn warning"><h5>'.translate("mod-mediaManager-list-close").'</h5></a>';
        $render->contentDiv(false);
    $render->contentDiv(false);
$render->contentDiv(false);