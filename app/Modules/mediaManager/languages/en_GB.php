<?php
$lang = array();

$lang["mediaManager"] = 'File Manager';

$lang["media-list"] = 'List of files';
$lang["upload-media"] = 'Upload files';


$lang["mod-mediaManager-all_files"] = 'List of files';
$lang["mod-mediaManager-add_file"] = 'Upload files';
$lang["mod-mediaManager-edit_file"] = 'File Detail';

$lang["mod-mediaManager-select_files"] = 'Select files to upload';
$lang["mod-mediaManager-select_files_here"] = 'Select files or move them here';
$lang["mod-mediaManager-noImage"] = 'Preview not available';

$lang["mod-mediaManager-status"] = 'Status';
$lang["mod-mediaManager-status-waiting"] = 'Waiting in queue';
$lang["mod-mediaManager-status-upload"] = 'Uploading ...';
$lang["mod-mediaManager-status-cancelled"] = 'Cancelled';
$lang["mod-mediaManager-status-success"] = 'Uploaded successfully';
$lang["mod-mediaManager-status-error"] = 'An error occurred while uploading';

$lang["mod-mediaManager-form-title"] = 'Filename';
$lang["mod-mediaManager-form-alt"] = 'ALT description';
$lang["mod-mediaManager-form-main"] = 'Main Image';
$lang["mod-mediaManager-form-group"] = 'Image Group';

$lang["mod-mediaManager-return-true"] = 'Uploaded successfully!';
$lang["mod-mediaManager-return-false"] = 'An error occurred while uploading!';

$lang["mod-mediaManager-return-add-true"] = 'Files added successfully!';
$lang["mod-mediaManager-return-add-false"] = 'The action could not be performed.';
$lang["mod-mediaManager-return-add-false-id"] = 'Invalid modified action id!';
$lang["mod-mediaManager-return-add-false-template"] = 'Invalid template to assign!';
$lang["mod-mediaManager-return-add-false-file"] = 'Select files to assign!';
$lang["mod-mediaManager-return-add-false-adding"] = 'One or more files could not be added!';

$lang["mod-mediaManager-return-add-important-save_before"] = 'You must save the page before selecting files';

$lang["mod-mediaManager-limits"] = 'Maximum allowed size of one file is <b>%s</b>.<br />';
$lang["mod-mediaManager-limits-second"] = 'Due to the server settings, a maximum of <b>%s files</b> with a total size of <b>%s</b> can be uploaded at once.';

$lang["mod-mediaManager-alt-none"] = 'Image not found';

$lang["mod-mediaManager-list-list"] = 'Associated Product Images';
$lang["mod-mediaManager-list-title"] = 'Title';
$lang["mod-mediaManager-list-alt"] = 'Alt description';
$lang["mod-mediaManager-list-basicUrl"] = 'Short URL';
$lang["mod-mediaManager-list-fileName"] = 'File Name';

$lang["mod-mediaManager-list-title_popUp"] = 'File Manager';
$lang["mod-mediaManager-list-text_popUp"] = 'The selected files will be assigned to the content you are editing.';

$lang["mod-mediaManager-list-limit_images"] = "The limit of assigned files is";

$lang["mod-mediaManager-list-head"] = 'Head Image';
$lang["mod-mediaManager-list-group"] = 'Group';
$lang["mod-mediaManager-list-remove"] = 'Remove';
$lang["mod-mediaManager-list-choice"] = 'File Manager';
$lang["mod-mediaManager-list-add"] = 'Assign Files';
$lang["mod-mediaManager-list-close"] = 'Close';
$lang["mod-mediaManager-list-save"] = 'Save Data';
$lang["mod-mediaManager-list-delete"] = 'Delete';
$lang["mod-mediaManager-list-select"] = 'Select';
$lang["mod-mediaManager-list-deselect"] = 'Deselect';

$lang["mod-mediaManager-pagination-title"] = 'Pagination';
$lang["mod-mediaManager-pagination-prev"] = 'Prev';
$lang["mod-mediaManager-pagination-next"] = 'Next';
$lang["mod-mediaManager-pagination-activePage"] = 'Displaying <span id="paginationPage">{currentPage}</span> page from {totalPages}';

$lang["mod-mediaManager-files-route"] = 'You are in';
$lang["mod-mediaManager-files-route-main"] = 'Default Folder';
$lang["mod-mediaManager-files-createFolder"] = 'Create subfolder';
$lang["mod-mediaManager-files-uploadFiles"] = 'Upload Files';

$lang["mod-mediaManager-files-fileInfo"] = 'Modify file';
$lang["mod-mediaManager-files-folderInfo"] = 'Folder Details';
$lang["mod-mediaManager-files-editFolder"] = 'Edit Folder';

$lang["mod-mediaManager-ajax-list"] = 'List of files';
$lang["mod-mediaManager-ajax-upload"] = 'Upload files to folder';
$lang["mod-mediaManager-ajax-backToList"] = 'Back to file list';
$lang["mod-mediaManager-ajax-uploadPopUp"] = 'Select files to upload to your chosen folder or return to file list.';

$lang["mod-mediaManager-ajax-uploadPopUp-true"] = 'Files uploaded successfully!';
$lang["mod-mediaManager-ajax-uploadPopUp-false"] = 'An error occurred while uploading files!';

$lang["mod-mediaManager-admin-tabs-files"] = 'File Manager';