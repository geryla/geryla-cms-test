<?php
$lang = array();

$lang["mediaManager"] = 'Správce souborů';

$lang["media-list"] = 'Seznam souborů';
  $lang["upload-media"] = 'Nahrát soubory';


$lang["mod-mediaManager-all_files"] = 'Seznam souborů';
$lang["mod-mediaManager-add_file"] = 'Nahrát soubory';
$lang["mod-mediaManager-edit_file"] = 'Detail souboru';

$lang["mod-mediaManager-select_files"] = 'Zvolte soubory k nahrání';
$lang["mod-mediaManager-select_files_here"] = 'Vyberte soubory nebo je přesuňte zde';
$lang["mod-mediaManager-noImage"] = 'Náhled není k dispozici';

$lang["mod-mediaManager-status"] = 'Stav';
$lang["mod-mediaManager-status-waiting"] = 'Čeká ve frontě';
$lang["mod-mediaManager-status-upload"] = 'Nahrávání ...';
$lang["mod-mediaManager-status-canceled"] = 'Zrušeno';
$lang["mod-mediaManager-status-success"] = 'Úspěšně nahráno';
$lang["mod-mediaManager-status-error"] = 'Nastala chyba při nahrávání';

$lang["mod-mediaManager-form-titulek"] = 'Název souboru';
$lang["mod-mediaManager-form-alt"] = 'ALT popis';
$lang["mod-mediaManager-form-hlavni"] = 'Hlavní obrázek';
$lang["mod-mediaManager-form-skupina"] = 'Skupina obrázků';

$lang["mod-mediaManager-return-true"] = 'Úspěšně nahráno!';
$lang["mod-mediaManager-return-false"] = 'Nastala chyba při nahrávání!';

$lang["mod-mediaManager-return-add-true"] = 'Soubory byly úspěšně přiřazeny!';
$lang["mod-mediaManager-return-add-false"] = 'Akci nebylo možné uskutečnit.';
$lang["mod-mediaManager-return-add-false-id"] = 'Neplatné id upravované akce!';
$lang["mod-mediaManager-return-add-false-template"] = 'Neplatná šablona pro přiřazení!';
$lang["mod-mediaManager-return-add-false-file"] = 'Vyberte soubory k přiřazení!';
$lang["mod-mediaManager-return-add-false-adding"] = 'Jeden nebo více souborů se nepodařilo přiřadit!';

$lang["mod-mediaManager-return-add-important-save_before"] = 'Před zvolením souborů je nutné stránku uložit';

$lang["mod-mediaManager-limits"] = 'Maximální povolená velikost jednoho souboru je <b>%s</b>.<br />';
$lang["mod-mediaManager-limits-second"] = 'Vzhledem k nastavení serveru je možné nahrát najednou maximálně <b>%s souborů</b> o celkové <b>velikosti %s</b>.';

$lang["mod-mediaManager-alt-none"] = 'Obrázek nenalezen';

$lang["mod-mediaManager-list-list"] = 'Přiřazené obrázky produktu';
$lang["mod-mediaManager-list-titulek"] = 'Titulek';
$lang["mod-mediaManager-list-alt"] = 'Alt popis';
$lang["mod-mediaManager-list-basicUrl"] = 'Zkrácená URL';
$lang["mod-mediaManager-list-fileName"] = 'Název souboru';

$lang["mod-mediaManager-list-title_popUp"] = 'Správce souborů';
$lang["mod-mediaManager-list-text_popUp"] = 'Zvolené soubory budou přiřazeny Vámi upravovanému obsahu.';

$lang["mod-mediaManager-list-limit_images"] = "Limit přiřazených souborů je";

$lang["mod-mediaManager-list-head"] = 'Hlavní obrázek';
$lang["mod-mediaManager-list-group"] = 'Skupina';
$lang["mod-mediaManager-list-remove"] = 'Vyřadit';
$lang["mod-mediaManager-list-choice"] = 'Správce souborů';
$lang["mod-mediaManager-list-add"] = 'Přiřadit soubory';
$lang["mod-mediaManager-list-close"] = 'Zavřít';
$lang["mod-mediaManager-list-save"] = 'Uložit údaje';
$lang["mod-mediaManager-list-delete"] = 'Smazat';
$lang["mod-mediaManager-list-select"] = 'Vybrat';
$lang["mod-mediaManager-list-deselect"] = 'Zrušit výběr';

$lang["mod-mediaManager-pagination-title"] = 'Stránkování';
$lang["mod-mediaManager-pagination-prev"] = 'Předchozí';
$lang["mod-mediaManager-pagination-next"] = 'Další';
$lang["mod-mediaManager-pagination-activePage"] = 'Zobrazuje se <span id="paginationPage">{currentPage}</span> stránka ze {totalPages}';

$lang["mod-mediaManager-files-route"] = 'Nacházíte se';
$lang["mod-mediaManager-files-route-main"] = 'Výchozí složka';
$lang["mod-mediaManager-files-createFolder"] = 'Vytvořit podsložku';
$lang["mod-mediaManager-files-uploadFiles"] = 'Nahrát soubory';

$lang["mod-mediaManager-files-fileInfo"] = 'Upravit soubor';
$lang["mod-mediaManager-files-folderInfo"] = 'Podrobnosti složky';
$lang["mod-mediaManager-files-editFolder"] = 'Upravit složku';

$lang["mod-mediaManager-ajax-list"] = 'Seznam souborů';
$lang["mod-mediaManager-ajax-upload"] = 'Nahrát soubory do složky';
$lang["mod-mediaManager-ajax-backToList"] = 'Zpět na seznam souborů';
$lang["mod-mediaManager-ajax-uploadPopUp"] = 'Vyberte soubory k nahrání do Vámi zvolené složky nebo se vraťte na výpis souborů.';

$lang["mod-mediaManager-ajax-uploadPopUp-true"] = 'Soubory byly úspěšně nahrány!';
$lang["mod-mediaManager-ajax-uploadPopUp-false"] = 'Došlo k chybě při nahrávání souborů!';

$lang["mod-mediaManager-admin-tabs-files"] = 'Správa souborů';
?>