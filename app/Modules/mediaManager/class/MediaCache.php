<?php

class MediaCache extends Cache {

    private $database;
    private $appController;

    private $tableData;
    private $tableLang;
    private $globalName = 'GLOBAL';

    public $cache = [];
    private $mediaData;

    public function __construct(Database $database, AppController $appController) {
        parent::__construct();
        $this->database = $database;
        $this->appController = $appController;

        $this->tableData = databaseTables::getTable('mediaManager', "files");
        $this->tableLang = databaseTables::getTable('mediaManager', "lang");
    }

    public function start(String $templateName = null){
        if(empty($this->cache[$templateName])){
            $this->loadCache($templateName);
        }
    }

    public function getItem(String $templateName = null, Int $objectId = null, Int $id = null){
        if($id === null && $templateName === null && $objectId === null){
            return [];
        }

        $this->start($templateName);
        $cacheName = $this->getCacheName($templateName, $objectId, $id);
        $cacheData = arraySearch($this->cache, $cacheName);
        $responseData = is_array($cacheData) ? array_values($cacheData) : [];

        return $responseData;
    }

    private function setItem(Int $id, Array $data){
        $cacheName = $this->getCacheName($data["template"], $data["objectId"], $id);
        $this->cache = arrayFill($data, $this->cache, $cacheName);
    }

    private function getCacheName(String $templateName = null, Int $objectId = null, Int $id = null){
        $templateName = ($templateName !== null && !empty($templateName)) ? $templateName : null;
        $objectId = ($objectId !== null) ? $objectId : null;

        $cacheName = ($templateName !== null) ? $templateName : $this->globalName;
        if($objectId !== null){
            $cacheName .= '.'.$objectId;
        }
        if($id !== null){
            $cacheName .= '.'.$id;
        }

        return $cacheName;
    }

    private function getAllMedia(String $templateName = null) {
        $mediaBind = null;
        if($templateName !== null){
            $mediaTemplate = "WHERE template = :templateName";
            $mediaBind["templateName"] = $templateName;
        }
        $this->mediaData = $this->database->getQuery("SELECT * FROM `{$this->tableData}` {$mediaTemplate};", $mediaBind, true);

        if (!empty($this->mediaData)) {
            foreach ($this->mediaData as $media) {
                $this->setItem($media["id"], $media);
            }
        }
    }

    private function getCachePath(String $templateName = null){
        $cacheName = ($templateName !== null) ? $templateName : $this->globalName;
        $cachePath = $this->checkDirectory('Media');

        return "{$cachePath}/media_{$cacheName}.txt";
    }

    private function loadCache(String $templateName = null){
        $cachePath = $this->getCachePath($templateName);
        $cacheExist = false;

        if (file_exists($cachePath)) {
            $fileCreationTime = filectime($cachePath);
            $elapsedTime = time() - $fileCreationTime;
            $duration = 1440 * 2; // 48h

            if ($elapsedTime < ($duration * 60)) {
                $this->cache[$templateName] = json_decode(file_get_contents($cachePath), true);
                $cacheExist = true;
            } else {
                unlink($cachePath);
            }
        }

        if (!$cacheExist) {
            $this->getAllMedia($templateName);
            if (isset($this->cache[$templateName])) {
                file_put_contents($cachePath, json_encode($this->cache[$templateName]));
            }
        }
    }

    public function deleteCache(String $templateName = null, Bool $reload = false){
        $cachePath = $this->getCachePath($templateName);

        if (file_exists($cachePath)) {
            unlink($cachePath);
        }

        if ($reload) {
            $this->loadCache($templateName);
        }
    }

}