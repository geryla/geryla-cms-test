<?php
    use \Gumlet\ImageResize;

    class uploadClass {
        private $database;
        private $appController;
        private $mediaController;
        private $systemLogger;

        private const FILE_PERMISSIONS = 0777;
        private const FOLDER_PERMISSIONS = 0777;

        public function __construct(Database $database, AppController $appController) {
            $this->database = $database;
            $this->appController = $appController;

            $this->mediaController = $appController->returnMediaController();
            $this->systemLogger = $appController->returnLogger();
        }

        public function uploadImage(Array $fileData){
            $uploadedId = 0;

            try {
                if (!empty($fileData)) {
                    $uploadPathInfo = $this->buildUploadPath();
                    $imageParameters = $this->mediaController->parseMediaUrlToParts($fileData['name'], false);
                    $imageName = $this->mediaController->generateUniqueFileName($imageParameters["fileName"], $imageParameters["extension"], $imageParameters["rowFilename"], $uploadPathInfo["folder"]);

                    $uploadPath = $uploadPathInfo["path"] . "/" . $imageName;
                    $uploadPathShorted = $uploadPathInfo["folder"] . $imageName;

                    $image = new ImageResize($fileData["tmp_name"]);
                    $isUploaded = $image->save($uploadPath, null, null);

                    if ($isUploaded) {
                        $uploadedId = $this->setDatabaseInfo($fileData, $uploadPathShorted);
                        if ($uploadedId !== 0) {
                            $this->systemLogger->createLog($this->appController->getLang(), 'mediaManager', 'upload', $uploadedId, session('security.adminLogin.logIn'));
                        }else{
                            unlink($uploadPath);
                        }
                    }
                }
            } catch (\Gumlet\ImageResizeException $e) {
                $uploadedId = 0; // Processing error
            }

            return $uploadedId;
        }
        public function generateImage(String $fileUrl, ?String $thumbnailSize = null, Array $fileData = []){
            $uploadedId = 0;

            try {
                if ($thumbnailSize === null && empty($fileData)) {
                    return $uploadedId;
                }

                $uploadPathInfo = $this->buildUploadPath();
                $imageParameters = $this->mediaController->parseMediaUrlToParts($fileUrl, true);
                $imageName = ($thumbnailSize === null) ? $this->mediaController->generateUniqueFileName($imageParameters["fileName"], $imageParameters["extension"], $imageParameters["rowFilename"], $uploadPathInfo["folder"]) : $imageParameters["fileName"];

                $uploadPath = $uploadPathInfo["path"] . "/" . $imageName;
                $uploadPathShorted = $uploadPathInfo["folder"] . $imageName;
                $uploadQuality = null; // Default
                $uploadType = null; // Default

                $fileUrlDecoded = ($fileUrl !== false && $fileUrl !== STORAGE_DIR && file_exists($fileUrl)) ? file_get_contents($fileUrl) : null;
                if ($fileUrlDecoded !== null && $fileUrlDecoded !== false) {
                    $image = ImageResize::createFromString($fileUrlDecoded);

                    if ($thumbnailSize !== null) {
                        $thumbnailParameters = $this->mediaController->getThumbnailInfo($thumbnailSize);
                        if ($thumbnailParameters !== null) {
                            $compressOnly = (isset($thumbnailParameters["compressOnly"]) && $thumbnailParameters["compressOnly"] == true) ? true : false;
                            if ($compressOnly === true) {
                                $thumbnailParameters["width"] = $image->getSourceWidth();
                                $thumbnailParameters["height"] = $image->getSourceHeight();
                            }

                            if ($thumbnailParameters["crop"] !== null) {
                                switch ($thumbnailParameters["crop"]) {
                                    case "TOP":
                                        $cropPosition = ImageResize::CROPTOP;
                                        break;
                                    case "BOTTOM":
                                        $cropPosition = ImageResize::CROPBOTTOM;
                                        break;
                                    case "LEFT":
                                        $cropPosition = ImageResize::CROPLEFT;
                                        break;
                                    case "RIGHT":
                                        $cropPosition = ImageResize::CROPRIGHT;
                                        break;
                                    default:
                                        $cropPosition = ImageResize::CROPCENTER;
                                        break;
                                }
                                $image->crop($thumbnailParameters["width"], $thumbnailParameters["height"], true, $cropPosition);
                            } elseif ($thumbnailParameters["cropX"] !== null && $thumbnailParameters["cropY"] !== null) {
                                $image->freecrop($thumbnailParameters["width"], $thumbnailParameters["height"], $thumbnailParameters["cropX"], $thumbnailParameters["cropY"]);
                            } else {
                                if ($thumbnailParameters["aspectRatio"] === true) {
                                    $image->resizeToBestFit($thumbnailParameters["width"], $thumbnailParameters["height"]);
                                } else {
                                    $image->resize($thumbnailParameters["width"], $thumbnailParameters["height"]);
                                }
                            }

                            $uploadPath = $this->buildResizePath($fileUrl, $thumbnailSize, $imageName);
                            $uploadQuality = $thumbnailParameters["quality"];
                        }
                    }

                    $isUploaded = $image->save($uploadPath, $uploadType, $uploadQuality);
                    if ($isUploaded) {
                        $uploadedId = ($thumbnailSize === null) ? $this->setDatabaseInfo($fileData, $uploadPathShorted) : $this->getDatabaseId($fileUrl);
                        if ($uploadedId !== 0) {
                            $this->systemLogger->createLog($this->appController->getLang(), 'mediaManager', (($thumbnailSize === null) ? 'upload' : 'regenerate'), $uploadedId, session('security.adminLogin.logIn'));
                        }else{
                            unlink($uploadPath);
                        }
                    }
                }
            } catch (\Gumlet\ImageResizeException $e) {
                $uploadedId = 0; // Processing error
            }

            return $uploadedId;
        }
        public function uploadFile($file) {
            $uploadedId = 0;

            $uploadPath = $this->buildUploadPath();
            $fileInfo = $this->mediaController->parseMediaUrlToParts($file['name'], false);
            $fileName = $this->mediaController->generateUniqueFileName($fileInfo["fileName"], $fileInfo["extension"], $fileInfo["rowFilename"], $uploadPath["folder"]);
            $fileData = ["url" => $uploadPath["folder"].$fileName, "objectId" => 0, "fileType" => $file["type"], "fileSize" => $file["size"], "folder" => $file["folderId"], "createdDate" => date("Y-m-d H:i:s"), "updatedDate" => date("Y-m-d H:i:s")];

            $uploadFile = move_uploaded_file($file["tmp_name"], $uploadPath["path"]."/".$fileName);
            if($uploadFile !== false){
                $uploadedId = $this->mediaController->createNewMedia($fileData);
                if ($uploadedId !== 0) {
                    $this->systemLogger->createLog($this->appController->getLang(), 'mediaManager', 'uploadFile', $uploadedId, session('security.adminLogin.logIn'));
                }
            }

            return $uploadedId;
        }
        public function generateWebP($imageUrl) {
            if(useIfExists(env('WEBP_ENABLED'), true) === true){
                $webpUrl = $this->mediaController->replaceImageExtension($imageUrl, 'webp');
                if ($webpUrl !== false) {
                    try {
                        $webpPath = (strpos($webpUrl, STORAGE_DIR) !== false) ? $webpUrl : STORAGE_DIR.$webpUrl;
                        $imagePath = (strpos($imageUrl, STORAGE_DIR) !== false) ? $imageUrl : STORAGE_DIR.$imageUrl;

                        if(!file_exists($webpPath)) {
                            $imageUrlDecoded = ($imagePath !== false && $imagePath !== STORAGE_DIR && file_exists($imagePath)) ? file_get_contents($imagePath) : null;
                            if ($imageUrlDecoded !== null && $imageUrlDecoded !== false) {
                                $image = ImageResize::createFromString($imageUrlDecoded);
                                $isUploaded = $image->save($webpPath, IMAGETYPE_WEBP);
                                if ($isUploaded === false) {
                                    return false;
                                }
                            }
                        }
                    } catch (\Gumlet\ImageResizeException $e) {
                        return false;
                    }
                }
            }

            return true;
        }

        private function buildUploadPath(){
            $folder = date('Y/m/');
            $uploadPath = STORAGE_DIR.$folder;

            if (!is_dir($uploadPath)) {
                mkdir($uploadPath, self::FOLDER_PERMISSIONS, true);
            }

            return ["folder" => $folder, "path" => $uploadPath];
        }
        private function buildResizePath($url, $size, $imageName) {
            $rowFilename = substr(strrchr(stripslashes($url), "/"), 1);
            $filePath = str_replace($rowFilename, "", $url);
            $resizePath = $filePath.$size."/";

            if (!file_exists($resizePath)) {
                mkdir($resizePath, self::FILE_PERMISSIONS, true);
            }

            return $resizePath.$imageName;
        }
        private function setDatabaseInfo(Array $fileData, String $urlPath){
            $imageData = ["url" => $urlPath, "objectId" => 0, "fileType" => $fileData["type"], "fileSize" => $fileData["size"], "folder" => $fileData["folderId"], "createdDate" => date("Y-m-d H:i:s"), "updatedDate" => date("Y-m-d H:i:s")];
            return $this->mediaController->createNewMedia($imageData);
        }
        private function getDatabaseId(String $fileUrl){
            $mediaUrl = str_replace(STORAGE_DIR, "", $fileUrl);
            $loadedMedia = $this->mediaController->getOneMediaByUrl($mediaUrl, null);
            return (int) ((!empty($loadedMedia)) ? ((isset($loadedMedia["id"])) ? $loadedMedia["id"] : $loadedMedia[0]["id"]) : 0);
        }
    }