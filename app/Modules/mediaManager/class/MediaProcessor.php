<?php
    class MediaProcessor {
        private $database;
        private $mediaController;
        private $uploadClass;

        private $loadedImages = [];
        private $langFile;

        public function __construct(Database $database, AppController $appController, MediaController $mediaController) {
            $this->database = $database;
            $this->mediaController = $mediaController;
            $this->uploadClass = new uploadClass($database, $appController);

            $this->langFile = $appController->getLangFile();
        }

        public function getAllObjectMedia(Int $objectId, String $templateName, String $langShort, ?String $thumbnailSize = null) {
            $responseMedia = [];
            $mediaData = $this->mediaController->getAllObjectMedia($objectId, $templateName, $langShort);

            if (!empty($mediaData)) {
                usort($mediaData, function ($a, $b) { return $a['orderNum'] <=> $b['orderNum']; });
                foreach ($mediaData as $media) {
                    $builtMediaEntity = $this->buildMediaEntity($media, $thumbnailSize);
                    if ($builtMediaEntity !== null) {
                        $responseMedia[] = $builtMediaEntity;
                    }
                }
            }

            return (empty($responseMedia)) ? false : (object) $responseMedia;
        }
        public function getAllObjectMediaByType(Int $objectId, String $templateName, String $langShort, Array $fileTypes, ?String $thumbnailSize = null) {
            $responseMedia = [];
            $mediaData = $this->mediaController->getAllObjectMedia($objectId, $templateName, $langShort);
            if ($mediaData) {
                usort($mediaData, function ($a, $b) { return $a['orderNum'] <=> $b['orderNum']; });
                foreach ($mediaData as $media) {
                    $builtMediaEntity = $this->buildMediaEntity($media, $thumbnailSize, $fileTypes);
                    if ($builtMediaEntity !== null) {
                        $responseMedia[] = $builtMediaEntity;
                    }
                }
            }

            return (empty($responseMedia)) ? false : (object) $responseMedia;
        }
        public function getMainImage(Int $objectId, String $templateName, String $langShort, ?String $thumbnailSize = null) {
            $mainMedia = $this->mediaController->getObjectMainMedia($objectId, $templateName, $langShort);
            if (!empty($mainMedia)) {
                $mediaEntity = $this->buildMediaEntity($mainMedia, $thumbnailSize, ['image']);
                if($mediaEntity !== null){
                    return (object) $mediaEntity;
                }
            }

            return false;
        }
        public function getAllObjectImages(Int $objectId, String $templateName, String $langShort, ?String $thumbnailSize = null, Bool $serializeResponse = false) {
            $objectImages = $this->getAllObjectMediaByType($objectId, $templateName, $langShort, ['image'], $thumbnailSize);
            if($objectImages === false){
                $objectImages = [];
            }

            return ($serializeResponse === true) ? $this->serializeMedia($objectImages) : (array) $objectImages;
        }
        private function buildMediaEntity(Array $mediaData, ?String $thumbnailSize = null, Array $allowedTypes = []){
            $mediaType = ($this->mediaController->checkMediaType($mediaData["fileType"], false));
            if(!empty($allowedTypes) && !in_array($mediaType, $allowedTypes)){
                return null;
            }

            $mediaInfo = $this->mediaController->parseMediaUrlToParts($mediaData["url"], true);
            if($mediaData["fileType"] === 'image/svg+xml'){
                $thumbnailSize = null;
            }

            $mediaData["fileName"] = $mediaInfo["fileName"];
            $mediaData["basicUrl"] = $mediaData["url"];
            $relativeUrl = $this->getImagePath($mediaData["url"], $thumbnailSize);
            $mediaData["url"] = $this->buildAbsolutePath($relativeUrl);

            if ($mediaType === "image") {
                $mediaData["parameters"] = $this->getImageInfo(STORAGE_DIR.$mediaData["basicUrl"]);
                $mediaData["webP"] = $this->checkWebPSupport($relativeUrl);
                $entityObject = new EntityImage($mediaData);
            } else {
                $mediaData["fileType"] = $this->mediaController->getMediaType($mediaData["fileType"]);
                $entityObject = new EntityFile($mediaData);
            }

            return $entityObject->get();
        }
        private function getImageInfo(String $url){
            $imageInfo = [];

            $image = @file_get_contents($url);
            if ($image !== false) {
                $info = @getimagesizefromstring($image);
                if ($info !== false) {
                    $imageInfo['width'] = $info[0];
                    $imageInfo['height'] = $info[1];
                    $imageInfo['type'] = pathinfo(parse_url($url, PHP_URL_PATH), PATHINFO_EXTENSION);
                }
            }

            return $imageInfo;
        }
        public function checkWebPSupport(String $url){
            if(useIfExists(env('WEBP_ENABLED'), true) === true){
                $webpUrl = $this->mediaController->replaceImageExtension($url, 'webp');
                if($webpUrl !== false){
                    $pathInfo = $this->sanitizePath($webpUrl, null);
                    if(file_exists(STORAGE_DIR.$pathInfo["path"])){
                        return $this->buildAbsolutePath($pathInfo["storagePath"].$pathInfo["path"]);
                    }
                }
            }

            return '';
        }

        public function getImagePath(String $url, ?String $thumbnailSize = null) {
            if(!empty($thumbnailSize) && empty($this->mediaController->getThumbnailInfo($thumbnailSize))){
                $thumbnailSize = null;
            }

            $pathInfo = $this->sanitizePath($url, $thumbnailSize);

            if(!file_exists(STORAGE_DIR.$pathInfo["path"])) {
                if($thumbnailSize === null || $thumbnailSize === ''){
                    return false;
                }

                $regenerateFile = $this->uploadClass->generateImage(STORAGE_DIR.$pathInfo["url"], $thumbnailSize);
                if ($regenerateFile === 0) {
                    return false;
                }
            }

            $this->uploadClass->generateWebP($pathInfo["path"]);
            $this->loadedImages[] = $pathInfo["url"];

            $relativePath = $pathInfo["storagePath"].$pathInfo["path"];
            return $relativePath;
        }
        public function sanitizePath(String $url, ?String $thumbnailSize = null) {
            $storagePath = str_replace(ROOT, "", STORAGE_DIR);
            $url = (strpos($url, $storagePath) !== false) ? str_replace($storagePath, "", $url) : $url;
            $mediaPath = stripslashes($this->mediaController->getThumbnailUrl($url, $thumbnailSize));

            return ["storagePath" => $storagePath, "url" => $url, "path" => $mediaPath];
        }
        public function buildAbsolutePath(String $relativeUrl){
            $serverPath = SERVER_PROTOCOL."://".SERVER_NAME;
            return $serverPath.((strpos($relativeUrl, '/') === 0) ? "" : '/').$relativeUrl;
        }
        public function serializeMedia($mediaList) {
            $serializedList = [];

            if(!empty($mediaList)){
                foreach($mediaList as $media){
                    $mediaData = (array) $media;
                    $serializedList["all"][] = $mediaData;

                    if($mediaData["head"] == 1){
                        $serializedList["head"] = $mediaData;
                    }elseif($mediaData["grouped"] == 1){
                        $serializedList["grouped"][] = $mediaData;
                    }else{
                        $serializedList["other"][] = $mediaData;
                    }
                }

                if (!isset($serializedList["head"]) && isset($serializedList["other"][0])) {
                    $serializedList["head"] = $serializedList["other"][0];
                }
            }

            return $serializedList;
        }
        public function getLoadedImage(Int $index){
            if(!empty($this->loadedImages)){
                return ($this->loadedImages[$index]) ?? null;
            }

            return null;
        }

    // PANEL (Backend) methods
        public function generatePreview($file, ?String $size = null, ?String $id = null, ?String $class = null, ?String $attr = null) {
            $file = (array) $file;

            $fileType = $this->mediaController->checkMediaType($file["fileType"]);
            $class = ($fileType !== "image") ? $class ?? "svgIcon" : $class;

            $fileUrl = $this->getFileUrl($file, $size, $fileType);
            $fileAlt = $file["alt"] ?? $file["title"] ?? '';

            $idTag = ($id !== null) ? 'id="' . $id . '"' : '';
            $classTag = ($class !== null) ? 'class="' . $class . '"' : '';
            $attrTag = ($attr !== null) ? $attr : '';

            $imageTag = ($fileUrl !== false) ? "<img src='{$fileUrl}' alt='{$fileAlt}' {$idTag} {$classTag} {$attrTag}>" : null;

            return $imageTag;
        }
        public function getFileInfo($file) {
            $file = (array) $file;

            $fileSubName = (!empty($file["title"])) ? $file["title"] : $file["alt"];
            $fileSubName = (!empty($fileSubName)) ? $fileSubName : substr(strrchr(stripslashes($file["url"]), "/"), 1);

            $file["url"] = (isset($file["basicUrl"])) ? $file["basicUrl"] : $file["url"];

            $fileSize = (!empty($file["fileSize"])) ? $file["fileSize"] : filesize(STORAGE_DIR.$file["url"]);
            $checkSize = true;
            $sizeAttach = [0 => "B", 1 => "kB", 2 => "MB", 3 => "GB"];
            $attach = 0;
            do {
                $reducedSize = $fileSize / 1024;
                if ($reducedSize > 1024) {
                    $fileSize = $reducedSize;
                    $attach++;
                } else {
                    $checkSize = false;
                    $attach++;
                    $fileSize = round($reducedSize, 2) . " " . $sizeAttach[$attach];
                }
            } while ($checkSize === true);

            return ["viewName" => $fileSubName, "id" => $file["id"], "title" => $file["title"], "alt" => $file["alt"], "url" => $file["url"], "size" => $fileSize];
        }
        public function getFileActions($fileInfo, $viewUrl, $delete = false, $deleteText = null, $sort = false, $head = null, $group = null, $selector = false) {
            if($deleteText === null){
                $deleteText = "confirm-head='".translate("confirm-delete-head")."' confirm-text='".translate("confirm-delete-text")."' confirm-yes='".translate("confirm-delete-yes")."' confirm-no='".translate("confirm-delete-no")."'";
            }

            echo '<a class="action blue external" href="/'.STORAGE.'/'.$viewUrl.'" target="_blank"><i class="fa fa-external-link"></i></a>';
            echo ($sort === true) ? '<span class="action handle"><i class="fa fa-arrows"></i></span>' : null;
            echo ($head !== null) ? '<a class="action gray changeImageStatus head '.(($head === true) ? "active" : null).'" data-id="'.$fileInfo["id"].'" data-action="head"><i class="fa fa-graduation-cap"></i></a>' : null;
            echo ($group !== null) ? '<a class="action gray changeImageStatus group '.(($group === true) ? "active" : null).'" data-id="'.$fileInfo["id"].'" data-action="grouped"><i class="fa fa-object-group"></i></a>' : null;

            echo ($selector === false) ? '<a class="action gray editFile" data-id="'.$fileInfo["id"].'" data-title="'.$fileInfo["title"].'" data-alt="'.$fileInfo["alt"].'" data-name="'.$fileInfo["viewName"].'"><i class="fa fa-align-left"></i></a>' : null;
            echo ($selector === true) ? '<span class="btn selectImage" data-select="'.translate("mod-mediaManager-list-select").'" data-deselect="'.translate("mod-mediaManager-list-deselect").'">'.translate("mod-mediaManager-list-select").'</span>' : null;
            echo ($selector === true) ? '<input type="checkbox" name="selectedImages[]" value="'.$fileInfo["id"].'" />' : null;

            echo ($delete === true) ? '<a class="action red deleteFile" data-id="'.$fileInfo["id"].'" data-type="file" action="delete" '.$deleteText.'><i class="fa fa-ban"></i></a>' : null;
            echo ($delete === false) ? '<a class="action red changeImageStatus" data-id="'.$fileInfo["id"].'" data-action="remove"><i class="fa fa-ban"></i></a>' : null;
        }
        private function getFileUrl(Array $file, ?String $size, String $fileType){
            if (isset($file["basicUrl"])) {
                $file["url"] = $file["basicUrl"];
            }

            if ($fileType !== "image") {
                $assetsPath = ADMIN_DIR.'/assets/images/';
                $fileUrl = null;

                if ($this->mediaController->getMediaType($file["fileType"]) === 'svg') {
                    $fileXml = base64_encode(file_get_contents(STORAGE.'/'.$file["url"]));
                    $fileUrl = (!empty($fileXml)) ? 'data:image/svg+xml;base64,' . $fileXml : null;
                }

                if ($fileUrl === null) {
                    $iconType = (file_exists("{$assetsPath}mediaIcon-{$fileType}.svg")) ? $fileType : 'notFound';
                    $fileUrl = relativePath("{$assetsPath}mediaIcon-{$iconType}.svg");
                }
            } else {
                $fileUrl = $this->getImagePath($file["url"], $size);
            }

            return $fileUrl;
        }

    }