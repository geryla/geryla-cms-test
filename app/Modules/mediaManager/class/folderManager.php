<?php
class folderManager {

    public $moduleName = "mediaManager";
    private $database;
    private $mediaController;
    private $localizationClass;

    public $translateType;
    public $dataTable;

    public $translateRows = ["title"]; // Only editable fields
    public $dataRows = ["id", "parentId"]; // Only editable fields

    public function __construct(Database $database, AppController $appController) {
        $this->database = $database;
        $this->localizationClass = $appController->returnLocalizationClass();
        $this->mediaController = $appController->returnMediaController();

        $this->dataTable = databaseTables::getTable($this->moduleName, "folders");
        $this->translateType = $appController->getModuleConfig($this->moduleName, "translateType")["mediaFolder"];
    }

// Check if name exist  
    public function checkIfNameExist($filename, $extension, $rowFilename, $uploadFolder) {
        $nameExist = $this->mediaController->findNameInTable($uploadFolder . $filename);
        if ($nameExist === true) {
            $i = 1;
            do {
                $filename = $rowFilename . $i . "." . $extension;
                $nameExist = $this->mediaController->findNameInTable($uploadFolder . $filename);
                $i++;
            } while ($nameExist === true);
        }
        return $filename;
    }

/* ================== Functions for work with data === */

// Select all folders
    public function getAllFolders($langShort) {
        $sql = "SELECT * FROM `{$this->dataTable}`;";
        $returnData = $this->database->getQuery($sql, null, true);
        $returnData = $this->localizationClass->translateResult($returnData, $this->translateType, $langShort);

        return $returnData;
    }
// Select one folder
    public function getOneFolderById($id, $langShort) {
        $sql = "SELECT * FROM `{$this->dataTable}` WHERE id = :id;";
        $returnData = $this->database->getQuery($sql, array("id" => $id), false);
        $returnData = $this->localizationClass->translateResult($returnData, $this->translateType, $langShort);

        return $returnData;
    }
// Select all folders by parent id
    public function getAllFoldersByParent($langShort, $parentId) {
        $sql = "SELECT * FROM `{$this->dataTable}` WHERE parentId = :parentId;";
        $returnData = $this->database->getQuery($sql, array("parentId" => $parentId), true);
        $returnData = $this->localizationClass->translateResult($returnData, $this->translateType, $langShort);

        return $returnData;
    }
// Select all folders and bredcrumbs for adminstration
    public function getAllFoldersInfo($langShort, $parentId) {
        $folderRoute = [];

        $allFolders = $this->getAllFoldersByParent($langShort, $parentId);
        $folderInfo = $this->getOneFolderById($parentId, $langShort);

        if (!empty($folderInfo)) {
            $folderRoute[] = $folderInfo;
            $lastFolder = $folderInfo;
            do {
                $folderInfo = $this->getOneFolderById($folderInfo["parentId"], $langShort);
                if (!empty($folderInfo)) {
                    $folderRoute[] = $folderInfo;
                }
            } while ($folderInfo["parentId"] != 0 OR !empty($folderInfo));
        }

        $lastFolder = (isset($lastFolder)) ? $lastFolder : $folderInfo;

        return ["allFolders" => $allFolders, "folderRoute" => array_reverse($folderRoute), "activeFolder" => $lastFolder];
    }
// Get all folders with files (structured)
    public function getAllFoldersListed($parentId, $langShort, $repeat = 0, $attachFiles = true) {

        $allFolders = $this->getAllFoldersByParent($langShort, $parentId);
        $returnFolders = [];

        if ($allFolders) {
            foreach ($allFolders as $key => $folder) {
                $allFiles = null;
                if($attachFiles === true){
                    $allFiles = $this->mediaController->getAllMediaByFolderId($folder["id"], $langShort, 10000, 0);
                }

                $returnFolders[] = ["id" => $folder["id"], "title" => $folder["title"], "child" => $this->getAllFoldersListed($folder["id"], $langShort, $repeat + 1, $attachFiles), "files" => $allFiles];
            }
        }

        return $returnFolders;
    }
// Delete folder
    public function deleteFolder($folderId) {
        $sql = "DELETE FROM `{$this->dataTable}`";
        $whereSql = "WHERE id = :id";
        $removeFolder = $this->database->deleteQuery($sql, array("id" => $folderId), $whereSql);

        $sql = "DELETE FROM `{$this->localizationClass->translationsTable}`";
        $whereSql = "WHERE objectId = :id AND objectType = :type";
        $removeTranslate = $this->database->deleteQuery($sql, array("id" => $folderId, "type" => $this->translateType), $whereSql);

        return $removeFolder;
    }
// Edit file in database
    public function editFile($langValues, $whereValues = null) {
        $sql = "UPDATE `{$this->languagesTable}`";
        $whereSql = "WHERE id = :id";
        $returnData = $this->database->updateQuery($sql, $langValues, $whereValues, $whereSql);
        return $returnData;
    }
    public function getFilesCount($folderId = 0) {
        $mediaTable = databaseTables::getTable($this->moduleName, "files");
        $mediaData = $this->database->getQuery("SELECT COUNT(*) AS resultsCount FROM `{$mediaTable}` WHERE folder = :folder AND duplicated = 0;", ["folder" => $folderId], false);

        return $mediaData["resultsCount"];
    }

}