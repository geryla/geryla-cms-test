<?php
    class MediaController {
        public $moduleName = "mediaManager";

        private $database;
        private $appController;
        private $localizationClass;
        private $mediaCache;

        public $dataTable;
        public $translateTable;
        public $translateRows = ["title", "alt", "langShort"];
        public $dataRows = ["objectId", "url", "template", "head", "grouped", "fileType", "folder", "createdDate", "updatedDate", "duplicated"];

        private $imageSizesArray = [
            "60" => [
                "width" => 60,
                "height" => 60,
                "quality" => 90,
                "aspectRatio" => true
            ],
            "120" => [
                "width" => 120,
                "height" => 120,
                "quality" => 90,
                "aspectRatio" => true
            ],
            "250" => [
                "width" => 250,
                "height" => 250,
                "quality" => 70,
                "aspectRatio" => true
            ],
            "500" => [
                "width" => 500,
                "height" => 500,
                "quality" => 80,
                "aspectRatio" => true
            ],
            "800" => [
                "width" => 800,
                "height" => 800,
                "quality" => 80,
                "aspectRatio" => true
            ],
            "ogTag" => [
                "width" => 1200,
                "height" => 630,
                "quality" => 80,
                "aspectRatio" => true
            ]
        ];
        private $supportedFormats = [
            "image" => [
                "image/jpeg" => "jpg",
                "image/png" => "png",
                "image/gif" => "gif",
                "image/webp" => "webp"
            ],
            "file" => [
                "image/svg+xml" => "svg",

                "application/pdf" => "pdf",
                "application/msword" => "word",
                "application/vnd.openxmlformats-officedocument.wordprocessingml.document" => "word",
                "application/vnd.ms-excel" => "excel",
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" => "excel",
                "application/vnd.ms-powerpoint" => "powerpoint",
                "application/vnd.openxmlformats-officedocument.presentationml.presentation" => "powerpoint",
                "application/vnd.openxmlformats-officedocument.presentationml.slideshow" => "powerpoint",
                "application/xml" => "xml",
                "text/xml" => "xml",
                "text/plain" => "txt",
                "text/csv" => "csv",
                "application/csvm+json" => "csv",
                "application/json" => "json",
                "application/vnd.software602.filler.form+xml" => "fo",
                "application/vnd.software602.filler.form-xml-zip" => "zfo",
                "application/zip" => "zip",
                "application/x-zip-compressed" => "zip",
                "multipart/x-zip" => "zip",
                "application/x-rar" => "rar",
                "application/x-rar-compressed" => "rar",
            ],
            "video" => [
                "video/mp4" => "mp4",
                "video/webm" => "webm",
                "video/ogg" => "ogg",
                "video/quicktime" => "mov",
                "video/x-ms-wmv" => "x-ms-wmv"
            ],
            "audio" => [
                "audio/mpeg" => "mp3",
                "audio/mp3" => "mp3",
                "audio/wav" => "wav",
                "audio/ogg" => "ogg"
            ]
        ];

        public function __construct(Database $database, AppController $appController) {
            $this->database = $database;
            $this->appController = $appController;
            $this->localizationClass = $appController->returnLocalizationClass();

            $this->mediaCache = new MediaCache($database, $appController);

            if (file_exists(CONFIG_DIR.'/_Thumbnails.php')) {
                $thumbnails = include_once(CONFIG_DIR.'/_Thumbnails.php');
                $this->imageSizesArray = $this->imageSizesArray + $thumbnails;
            }

            $this->dataTable = databaseTables::getTable($this->moduleName, "files");
            $this->translateTable = databaseTables::getTable($this->moduleName, "lang");
        }

        public function getThumbnailInfo(String $thumbnail){
            return $this->imageSizesArray[$thumbnail];
        }
        public function getAllThumbnailsInfo(){
            return $this->imageSizesArray;
        }

        public function getAllMediaByFolderId(Int $folderId, ?String $langShort = null, ?Int $limit = null, ?Int $offset = null) {
            $limitValue = ($limit !== 0 && $limit !== null) ? "LIMIT {$limit}" : null;
            $offsetValue = ($offset !== 0 && $offset !== null) ? "OFFSET {$offset}" : null;

            $mediaData = $this->database->getQuery("SELECT * FROM `{$this->dataTable}` WHERE folder = :folder AND duplicated = 0 ORDER BY id DESC {$limitValue} {$offsetValue};", ["folder" => $folderId], true);
            return $this->translateMediaData($mediaData, $langShort);
        }
        public function getAllObjectMedia(Int $objectId, String $template, ?String $langShort = null) {
            //$mediaData = $this->database->getQuery("SELECT * FROM `{$this->dataTable}` WHERE objectId = :objectId AND template = :template;", ["objectId" => $objectId, "template" => $template], true);

            $mediaData = $this->mediaCache->getItem($template, $objectId, null);
            return $this->translateMediaData($mediaData, $langShort);
        }
        public function getOneMediaById(Int $id, ?String $langShort = null) {
            $mediaData = $this->database->getQuery("SELECT * FROM `{$this->dataTable}` WHERE id = :id;", ["id" => $id], false);
            return $this->translateMediaData($mediaData, $langShort);
        }
        public function getOneMediaByUrl(String $url, ?String $langShort = null) {
            $mediaData = $this->database->getQuery("SELECT * FROM `{$this->dataTable}` WHERE url = :url AND duplicated = 0;", ["url" => $url], false);
            return $this->translateMediaData($mediaData, $langShort);
        }
        public function getObjectMainMedia(Int $objectId, String $template, ?String $langShort = null) {
            $mediaData = $this->database->getQuery("SELECT * FROM `{$this->dataTable}` WHERE template = :template AND objectId = :objectId ORDER BY head DESC, orderNum ASC LIMIT 1;", ["objectId" => $objectId, "template" => $template], false);
            return $this->translateMediaData($mediaData, $langShort);
        }
        private function translateMediaData(Array $data, ?String $langShort){
            if(empty($data) || $langShort === null || trim($langShort) === ''){
                return $data;
            }

            return $this->localizationClass->getTranslation($data, "title, alt, langShort", $this->translateTable, $langShort);
        }

        public function checkMediaType(String $fileType, Bool $strict = true) {
            if($strict === false){
                if($fileType === "image/svg+xml"){
                    return "image";
                }
            }

            foreach ($this->supportedFormats as $category => $formats) {
                if (isset($formats[$fileType])) {
                    return $category;
                }
            }
            return false;
        }
        public function getMediaType(String $fileType) {
            return $this->supportedFormats["file"][$fileType] ?? $fileType;
        }
        public function getMediaGroup(String $fileType) {
            if (isset($this->supportedFormats["file"][$fileType])) {
                $returnType = "file";
            } elseif (isset($this->supportedFormats["audio"][$fileType])) {
                $returnType = "audio";
            } elseif (isset($this->supportedFormats["video"][$fileType])) {
                $returnType = "video";
            } else {
                $returnType = "image";
            }

            return $returnType;
        }
        public function parseMediaUrlToParts(String $name, Bool $isUrl = false) {
            $response = ["fileName" => "", "extension" => "", "rowFilename" => ""];

            if ($name !== '') {
                $filename = ($isUrl === false) ? stripslashes($name) : substr(strrchr(stripslashes($name), "/"), 1);

                $response["extension"] = pathinfo($name, PATHINFO_EXTENSION);
                $response["rowFilename"] = slug(pathinfo($filename, PATHINFO_FILENAME));
                $response["fileName"] = $response['rowFilename'].'.'.$response['extension'];
            }
            return $response;
        }
        public function getThumbnailUrl(String $url, ?String $thumbnailSize = null) {
            if ($thumbnailSize !== null && $thumbnailSize !== '') {
                $fileName = pathinfo(stripslashes($url), PATHINFO_BASENAME);
                $filePath = pathinfo(stripslashes($url), PATHINFO_DIRNAME);
                $fileExtension = pathinfo($url, PATHINFO_EXTENSION);

                $url = "{$filePath}/{$thumbnailSize}/{$fileName}";
            }

            return $url;
        }
        public function generateUniqueFileName(String $baseFilename, String $extension, String $rowFilename, String $uploadFolder) {
            $filename = $baseFilename;

            if ($this->isNameTaken($uploadFolder.$filename)) {
                $index = 1;

                do {
                    $filename = "{$rowFilename}-{$index}.{$extension}";
                    $index++;
                } while ($this->isNameTaken($uploadFolder.$filename));
            }

            return $filename;
        }
        private function isNameTaken(String $mediaPath) {
            $returnData = $this->database->getQuery("SELECT * FROM `$this->dataTable` WHERE url = :url;", ["url" => $mediaPath], true);
            return (empty($returnData)) ? false : true;
        }
        public function replaceImageExtension(String $url, String $extension) {
            $imageExtension = pathinfo($url, PATHINFO_EXTENSION);
            $imageBasename = pathinfo($url, PATHINFO_FILENAME);
            $imageDirectory = pathinfo($url, PATHINFO_DIRNAME);

            if (strtolower($imageExtension) !== $extension) {
                return $imageDirectory.'/'.$imageBasename.'.'.$extension;
            }

            return false;
        }

        public function createNewMedia(Array $mediaData) {
            $mediaId = $this->setMediaData($mediaData);
            if($mediaId !== 0){
                $mediaLangId = $this->setMediaLocale($mediaId);
                if($mediaLangId === 0){
                    $this->deleteMediaFromDatabase($mediaId, 'data');
                    return 0;
                }
            }

            return $mediaId;
        }
        private function setMediaData(Array $mediaData) {
            $mediaId = $this->database->insertQuery("INSERT INTO `{$this->dataTable}`", $mediaData);
            return ($mediaId === false || $mediaId === 0) ? 0 : $mediaId;
        }
        private function setMediaLocale(Int $mediaId) {
            if($mediaId === 0){
                return $mediaId;
            }

            $mediaLangId = $this->database->insertQuery("INSERT INTO `{$this->translateTable}`", ["associatedId" => $mediaId, "langShort" => $this->appController->getLang()]);
            return ($mediaLangId === false || $mediaLangId === 0) ? 0 : $mediaLangId;
        }

        public function deleteMediaFromDatabase(Int $mediaId, ?String $partialDeleteOption = null) {
            if($partialDeleteOption === null || $partialDeleteOption === 'locale'){
                $this->database->deleteQuery("DELETE FROM `{$this->translateTable}`", ["id" => $mediaId], "WHERE associatedId = :id");
            }
            if($partialDeleteOption === null || $partialDeleteOption === 'data'){
                $this->database->deleteQuery("DELETE FROM `{$this->dataTable}`", ["id" => $mediaId], "WHERE id = :id");
            }
        }
        public function removeMediaFromStorage(String $fileUrl) {
            if(file_exists(STORAGE_DIR.$fileUrl)) {
                $removed = unlink(STORAGE_DIR.$fileUrl);

                if(useIfExists(env('WEBP_ENABLED'), true) === true){
                    if($removed === true){
                        $webpPath = $this->replaceImageExtension($fileUrl, 'webp');
                        if($webpPath !== false){
                            unlink(STORAGE_DIR.$webpPath);
                        }
                    }
                }

                return $removed;
            }

            return false;
        }
        public function resetMediaToDefault(Int $mediaId){
            $values = ["template" => "", "head" => 0, "objectId" => 0, "grouped" => 0, "duplicated" => 0, "orderNum" => 0, "updatedDate" => date("Y-m-d H:i:s")];
            $this->database->updateQuery("UPDATE `{$this->dataTable}`", $values, ["id" => $mediaId], "WHERE id = :id");
        }
        public function clearObjectMediaTraces(Int $objectId, String $templateName) {
            $existingMedia = $this->database->getQuery("SELECT * FROM `{$this->dataTable}` WHERE objectId = :objectId AND template = :template;", ["objectId" => $objectId, "template" => $templateName], true);
            $errorCounter = 0;

            if (!empty($existingMedia)) {
                foreach($existingMedia as $media){
                    if($media["duplicated"] === 1){
                            $this->deleteMediaFromDatabase($media["id"]);
                    }else{
                        $this->resetMediaToDefault($media["id"]);
                    }
                }

                $this->mediaCache->deleteCache($templateName);
            }

            return ($errorCounter === 0) ? true : false;
        }

        public function deleteMedia(Int $mediaId){
            $errorCounter = 0;
            $mediaData = $this->getOneMediaById($mediaId);
            $mediaType = $this->checkMediaType($mediaData["fileType"]);

            $originalFileDeleted = $this->removeMediaFromStorage($mediaData["url"]);
            if($originalFileDeleted === true){
                if($mediaType === "image"){
                    foreach($this->imageSizesArray as $thumbnailName => $thumbnailData){
                        $thumbnailUrl = $this->getThumbnailUrl($mediaData["url"], $thumbnailName);
                        $this->removeMediaFromStorage($thumbnailUrl);
                    }
                }
            }else{
                $errorCounter++;
            }

            if($originalFileDeleted === true){
                $this->deleteMediaFromDatabase($mediaData["id"]);
            }

            return ($errorCounter === 0);
        }

        public function deleteThumbnails(String $thumbnail){
            if($this->imageSizesArray[$thumbnail]){
                    $storageDir = STORAGE_DIR;
                    
                    $years = scandir($storageDir);
                    foreach ($years as $year) {
                        if ($year == '.' || $year == '..') continue;

                        $months = scandir($storageDir.$year);
                        foreach ($months as $month) {
                            if ($month == '.' || $month == '..') continue;

                            $thumbnailFolder = $storageDir.$year.'/'.$month.'/'.$thumbnail;
                            if (!file_exists($thumbnailFolder)) continue;

                            $files = scandir($thumbnailFolder);
                            foreach ($files as $file) {
                                if ($file == '.' || $file == '..') continue;

                                $filePath = $thumbnailFolder.'/'.$file;
                                if (is_file($filePath)) {
                                    unlink($filePath);
                                }
                            }
                        }
                    }
                
                return true;
            }

            return false;
        }
    }