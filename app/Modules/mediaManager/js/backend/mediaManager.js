function upload_files(){
    $('#drag-and-drop-zone').dmUploader({
        url: requestsUrl,
        fieldName: "uploadMedia",
        extraData: {
            requestName: 'uploadImage', module: 'mediaManager', response: 'json', data: JSON.stringify({"uploadFolder": $('#drag-and-drop-zone').find('input[name="uploadFolder"]').val()})
        },
        onDragEnter: function(){
            this.addClass('active');
        },
        onDragLeave: function(){
            this.removeClass('active');
        },
        onNewFile: function(id, file){
            ui_multi_add_file(id, file);
            if (typeof FileReader !== "undefined"){
                var reader = new FileReader();
                var img = $('#uploaderFile' + id).find('.media-image');

                reader.onload = function (e) {
                    if (file.type.match('image.*')) {
                        img.html('<img class="preview-img" src="'+ e.target.result +'" />');
                    }
                };

                reader.readAsDataURL(file);
            }
        },
        onBeforeUpload: function(id){
            ui_multi_update_file_status(id, 'uploading', this.data('upload'));
            ui_multi_update_file_progress(id, 0, '', true);
        },
        onUploadCanceled: function(id) {
            ui_multi_update_file_status(id, 'warning', this.data('canceled'));
            ui_multi_update_file_progress(id, 0, 'warning', false);
        },
        onUploadProgress: function(id, percent){
            ui_multi_update_file_progress(id, percent);
        },
        onUploadSuccess: function(id, data){
            let uploadCallback = JSON.parse(data);
            ui_multi_update_file_status(id, uploadCallback["status"], this.data(uploadCallback["status"]));
            ui_multi_update_file_progress(id, 100, uploadCallback["status"], false);
        },
        onUploadError: function(id, xhr, status, message){
            ui_multi_update_file_status(id, 'error', message);
            ui_multi_update_file_progress(id, 0, 'error', false);
        }
    });
}
// Creates a new file and add it to our list
function ui_multi_add_file(id, file) {
    var template = $('#files-template').text();
    template = template.replace('%%filename%%', file.name);

    template = $(template);
    template.prop('id', 'uploaderFile' + id);
    template.data('file-id', id);

    $('#files').find('li.empty').fadeOut(); // remove the 'no files yet'
    $('#files').prepend(template);
}
// Changes the status messages on our list
function ui_multi_update_file_status(id, status, message) {
    $('#uploaderFile' + id).find('span').html(message).prop('class', 'status text-' + status);
}
// Updates a file progress, depending on the parameters it may animate it or change the color.
function ui_multi_update_file_progress(id, percent, color, active) {
    color = (typeof color === 'undefined' ? false : color);
    active = (typeof active === 'undefined' ? true : active);

    var bar = $('#uploaderFile' + id).find('div.progress-bar');

    bar.width(percent + '%').attr('aria-valuenow', percent);
    bar.toggleClass('progress-bar-striped progress-bar-animated', active);

    if (percent === 0){
        bar.html('');
    } else {
        bar.html(percent + '%');
    }

    if (color !== false){
        bar.removeClass('bg-success bg-info bg-warning bg-danger');
        bar.addClass('bg-' + color);
    }
}

/* ======== AJAX FILE functions + upload file script === */
    $(document).ajaxComplete(function () {
    /* FILE UPLOADER (with D&D - allowed for modal uploading) */
        upload_files();

    /* REFRESH METOD OF AJAX LIST OF FILES  */
        function refreshMethod(ajaxBox, folder, objectId, template, paginationNum = 0) {
            ajaxLoading();

            $.ajax({
                url: requestsUrl,
                type: 'POST',
                data: {requestName: 'chooseBox', module: 'mediaManager', response: 'dom', data: {activeFolder:folder, objectId:objectId, template:template, paginationNum: paginationNum}},
                success: function(data) {
                    let newData = $(data).html();
                    $(ajaxBox).empty().html(newData);
                    return true;
                }
            });
        }
        function selectCounter() {
            let selectedTotal = $(".allFiles.ajaxBox .filesList .imageBox.selected").length;
            (selectedTotal > 0) ? $("#sendImageToUpload").removeClass('disabled') : $("#sendImageToUpload").addClass('disabled');

            return (selectedTotal > 0) ? true : false;
        }
        function ajaxLoading(remove = false) {
            if(remove === false){
                let ajaxLoad = $("#contentLoader").clone().css({"display" : "flex"});
                $(".allFiles.ajaxBox").append(ajaxLoad);
            }else{
                $(".allFiles.ajaxBox #contentLoader").remove();
            }
        }
        selectCounter();

    /* REDIRECT FOLDERS */
        $(".allFiles.ajaxBox .foldersList").off().on('click', '.folder', function(e) {
            e.preventDefault();

            let ajaxBoxWrap = $(this).closest("#adminWrap");
            let ajaxBox = $(this).closest(".allFiles");
            let folder = $(this).attr("folder-id");
            let objectId = $(ajaxBox).find('input[name="objectId"]').val();
            let template = $(ajaxBox).find('input[name="template"]').val();

            refreshMethod(ajaxBoxWrap, folder, objectId, template);
        });
    /* SELECT FILES */
        $(".allFiles.ajaxBox .filesList").off().on('click', '.selectImage', function(e) {
            e.preventDefault();

            $(this).toggleClass("warning");
            let imageBox = $(this).closest(".imageBox");
            let checkbox = $(imageBox).find("input[type='checkbox']");
            $(imageBox).toggleClass("selected");
            ($(checkbox).is(":checked")) ? $(checkbox).prop("checked", false) && $(this).text($(this).attr("data-select")) : $(checkbox).prop("checked", true) && $(this).text($(this).attr("data-deselect"));

            $("#sendImageToUpload").find("b").text($(".allFiles.ajaxBox .filesList .imageBox.selected").length);
            selectCounter();
        });
    /* ADD FILES */
        $(".allFiles.ajaxBox").off().on('click', '#sendImageToUpload', function(e){
            let uploadFormData = $("#listOfFiles").serialize();
            let checkPerms = selectCounter();

            let activeImageWrap = $(".js_addFile.js-selected").closest('.allFiles');
            let refreshedListOfFiles = $(activeImageWrap).find("#loadedFilesWrap .listOfFiles").attr('id');

            if(checkPerms === true){
                ajaxLoading();
                $.ajax({
                    url: requestsUrl,
                    type: 'POST',
                    data: {requestName: 'addImage', module: 'mediaManager', response: 'dom', data: {uploadFormData:uploadFormData}},
                    success: function(data) {
                        $(activeImageWrap).find("#loadedFilesWrap").load(location.href + " #"+refreshedListOfFiles);
                        ajaxLoading(true);

                        $(".returnInfo").html(data);
                        $(".allFiles.ajaxBox .imageBox.selected .selectImage").trigger('click');
                    }
                });
            }
        });
    /* AJAX PAGINATION */
        $(".allFiles.ajaxBox #pagination").off().on('click', '.cont', function(e){
            let button = $(this),
                wrap = $(button).closest("#pagination");

            let type = (button).data("type"),
                step = (button).data("step");

            let folderId = (wrap).data("folder"),
                folderMax = (wrap).data("folder-max");

            let ajaxBoxWrap = $(wrap).closest("#adminWrap"),
                ajaxBox = $(wrap).closest(".allFiles");

            let objectId = $(ajaxBox).find('input[name="objectId"]').val();
            let template = $(ajaxBox).find('input[name="template"]').val();

            if( (type === "prev" && step <= -1) || (type === "next" && step >= folderMax) ){
                return false;
            }

            refreshMethod(ajaxBoxWrap, folderId, objectId, template, step);
        });
    /* AJAX UPLOAD buttons */
        $("#ajaxUploadFile").off().on('click', function(e) {
            let ajaxList = $(this).closest(".filesList");

            $(ajaxList).find("#listOfFiles").hide();
            $(ajaxList).find("#ajaxUpload").show();
        });
        $("#ajaxBack").off().on('click', function(e) {
            let ajaxBoxWrap = $(this).closest("#adminWrap");
            let ajaxBox = $(this).closest(".allFiles");
            let uploadedFiles = $(ajaxBoxWrap).find("#files");

            if($(uploadedFiles).find('li').length > 0){
                let folder = $(ajaxBox).find('input[name="activeFolder"]').val();
                let objectId = $(ajaxBox).find('input[name="objectId"]').val();
                let template = $(ajaxBox).find('input[name="template"]').val();

                refreshMethod(ajaxBoxWrap, folder, objectId, template);
            }else{
                let ajaxList = $(this).closest(".filesList");

                $(ajaxList).find("#listOfFiles").show();
                $(ajaxList).find("#ajaxUpload").hide();
            }
        });
    });
/* ======== CREATE AJAX SELECT BOX === */
    $(document).on('ready ajaxComplete', function () {
        $(".js_addFile").on('click', function(e) {
            let objectId = $(this).data('objectid');
            let template = $(this).data('template');
            let limit = $(this).data('limit');
            let allowSelect = true;

            if(limit){
                let itemsCount = $(this).closest('.allFiles').find('.listOfFiles').find('.imageBox').length;
                allowSelect = (itemsCount < limit);
            }

            if(allowSelect === true){
                $(this).addClass('js-selected');
                $.ajax({
                    url: requestsUrl,
                    type: 'POST',
                    data: {requestName: 'chooseBox', module: 'mediaManager', response: 'dom', data: {objectId: objectId, template:template}},
                    success: function(callback) {
                        $.colorbox({
                            width:"90%",
                            height:"90%",
                            fixed: true,
                            escKey: false,
                            html:callback,
                            onOpen: function(){
                                adminLoader(true);
                            },
                            onLoad: function(){
                                $("#cboxClose").addClass("btn error icon cancel").text("");
                            },
                            onComplete: function(){
                                adminLoader(false);
                                $("#cboxClose").show();
                            },
                            onClosed: function(){
                                $("#cboxClose").hide();
                                $(".js_addFile.js-selected").removeClass('js-selected');
                            }
                        });
                    }
                });
            }


        });
        upload_files();
    });
/* ======== CHANGE IMAGE STATUS (inline) === */
    $(document).on('ready ajaxComplete', function () {
        $('.imageBox').off('click').on('click', '.changeImageStatus', function() {
            let action = $(this).attr("data-action"),
                id = $(this).attr("data-id"),
                $btn = $(this);

            $.ajax({
                url: requestsUrl,
                type: 'POST',
                data: {requestName: 'actionImage', module: 'mediaManager', response: 'json', data: {id:id, action:action}},
                success: function(callback) {
                    if( action == "delete" ){
                        location.reload();
                    }else if( action == "remove" ){
                        $btn.closest(".imageBox").remove();
                    }else{
                        $btn.toggleClass("active");
                    }
                }
            });
        });
    });
/* ======== DELETE IMAGE === */
    $(document).on('click', '.deleteFile, .deleteFolder', function(e) {
        let action = $(this).attr("action"),
        objectId = $(this).attr("data-id"),
        fileType = $(this).attr("data-type");

        let confirmHead = $(this).attr("confirm-head"),
        confirmText = $(this).attr("confirm-text"),
        confirmYes = $(this).attr("confirm-yes"),
        confirmNo = $(this).attr("confirm-no");

        $('<div></div>').appendTo('body').html('<div><h6>'+confirmText+'</h6></div>')
        .dialog({
            modal: true,
            title: confirmHead,
            zIndex: 10000,
            autoOpen: true,
            width: 'auto',
            resizable: false,
            buttons:[
                {
                    text: confirmYes,
                        click: function() {
                        adminLoader(true);
                        $.ajax({
                            url: requestsUrl,
                            type: 'POST',
                            data: {requestName: 'actionImage', module: 'mediaManager', response: 'json', data: {action:action, id:objectId, fileType:fileType}},
                            success: function(callback) {
                                location.reload(true);
                            }
                        });
                    },
                    'class': 'agreeButton'
                },
                {
                    text: confirmNo,
                    click: function() {
                        $(this).dialog("close");
                    },
                    'class': 'closeButton'
                }
            ],
            close: function (event, ui) {
                $(this).remove();
            }
        });
    });
/* ======== SAVE FILE/FOLDER DETAILS === */
    $(document).on('click', '.saveFileDetail, .saveFolderDetail', function(e) {
        let objectId = $(this).attr("data-id");
        let fileType = $(this).attr("data-type");
        let title = $(this).closest(".infoBlock").find('input[name=edit-title]').val();
        let alt = $(this).closest(".infoBlock").find('input[name=edit-alt]').val();
        adminLoader(true);

        $.ajax({
            url: requestsUrl,
            type: 'POST',
            data: {requestName: 'actionImage', module: 'mediaManager', response: 'json', data: {action:"saveDetails", info:{title: title, alt: alt}, id:objectId, fileType:fileType}},
            success: function(objectName) {
                if(fileType === "folder"){
                    location.reload(true);
                }else{
                    $(".imageBox.selected").find(".name > span").text(objectName);
                    adminLoader(false);
                }
            }
        });
    });
/* ======== FOLDER INFO TOGGLE === */
    $(document).on('click', '#createFolder', function() {
        if( !$(".newFolder").hasClass("hide") ){
            $(".newFolder").addClass("hide");
            $(".newFolder form input").prop("disabled", true);
        }else{
            $(".newFolder").removeClass("hide");
            $(".newFolder form input").prop("disabled", false);
        }
    });
    $(document).on('click', '#editFolder, #closeFolderEdit', function() {
        let fileInfoOpen = ($(".filesWrap .infoBlock.fileInfo").hasClass("opened")) ? true : false;

        $(".filesWrap .infoBlock.folderInfo").toggleClass("opened");
        if(fileInfoOpen === true){
            $(".filesWrap .infoBlock.fileInfo").removeClass("opened");
        }
        $(".filesWrap .imageBox").removeClass("selected");
    });
/* ======== FILE INFO TOGGLE === */
    $(document).on('click', '.filesWrap .imageBox .editFile', function() {
        if( !$(this).hasClass("clickable") ){
            let wrapper = $(this).closest(".filesList .filesWrap");

            let isSelected = ($(this).closest(".imageBox").hasClass("selected")) ? true : false;
            let fileInfoOpen = ($(wrapper).find(".infoBlock.fileInfo").hasClass("opened")) ? true : false;
            let folderInfoOpen = ($(wrapper).find(".infoBlock.folderInfo").hasClass("opened")) ? true : false;

            $(wrapper).find(".imageBox").removeClass("selected");
            if(folderInfoOpen === true){
                $(wrapper).find(".infoBlock.folderInfo").removeClass("opened");
            }

            if(fileInfoOpen === false){
                $(wrapper).find(".infoBlock.fileInfo").addClass("opened");
            }else{
                if(isSelected === true){
                    $(wrapper).find(".infoBlock.fileInfo").removeClass("opened");
                }
            }

            if(isSelected === false){
                $(this).closest(".imageBox").addClass("selected");

                let fileId = $(this).attr("data-id"),
                fileName = $(this).attr("data-name"),
                fileTitle = $(this).attr("data-title"),
                fileAlt = $(this).attr("data-alt");

                $(".detailsWrap .fileInfo .saveFileDetail").attr("data-id", fileId);
                $(".detailsWrap .fileInfo input[name='edit-title']").val(fileTitle);
                $(".detailsWrap .fileInfo input[name='edit-alt']").val(fileAlt);
                $(".detailsWrap .fileInfo h2 span").html(fileName);
            }else{
                $(this).closest(".imageBox").removeClass("selected");
            }
        }
    });
    $(document).on('click', '.filesWrap .detailsWrap .hideInfoBlock', function() {
        $(this).closest(".infoBlock").removeClass("opened");
        $(".filesWrap .imageBox").removeClass("selected");
    });