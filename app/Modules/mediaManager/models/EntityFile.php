<?php
class EntityFile extends Entity{

    protected $extendedParams = [
        "objectId" => [
            "type" => "Integer",
        ],
        "template" => [
            "type" => "String",
        ],
        "head" => [
            "type" => "Boolean",
        ],
        "title" => [
            "type" => "String",
        ],
        "fileName" => [
            "type" => "String",
        ],
        "basicUrl" => [
            "type" => "String",
        ],
        "fileType" => [
            "type" => "String",
        ],
    ];

    public function __construct(Array $objectData) {
        $this->build($objectData, $this->extendedParams);
    }

}