<?php
class EntityImage extends Entity{

    protected $extendedParams = [
        "objectId" => [
            "type" => "Integer",
        ],
        "template" => [
            "type" => "String",
        ],
        "head" => [
            "type" => "Boolean",
        ],
        "grouped" => [
            "type" => "Boolean",
        ],
        "alt" => [
            "type" => "String",
        ],
        "title" => [
            "type" => "String",
        ],
        "fileName" => [
            "type" => "String",
        ],
        "url" => [
            "type" => "String",
        ],
        "basicUrl" => [
            "type" => "String",
        ],
        "fileType" => [
            "type" => "String",
        ],
        "parameters" => [
            "type" => "Array",
            "body" => [
                "width" => ["type" => "Integer"],
                "height" => ["type" => "Integer"],
                "type" => ["type" => "String"],
            ]
        ],
        "webP" => [
            "type" => "String",
        ],
    ];

    public function __construct(Array $objectData) {
        $this->build($objectData, $this->extendedParams);
    }

}