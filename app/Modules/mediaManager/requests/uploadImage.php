<?php
    if(isExisting('files', $request)){
        $uploadClass = new uploadClass($database, $appController);
        $allImages = $allFiles = $allVideos = $allSounds = $notSupported = [];

        $fileNameParts = $mediaController->parseMediaUrlToParts($request["files"]['uploadMedia']['name'], false);
        $fileType = ( !empty($request["files"]['uploadMedia']['type']) ) ? $request["files"]['uploadMedia']['type'] : $fileNameParts["extension"];
        $fileSize = ( !empty($request["files"]['uploadMedia']['size']) ) ? $request["files"]['uploadMedia']['size'] : filesize($request["files"]['uploadMedia']['tmp_name']);
        $supportedFile = $mediaController->checkMediaType($fileType);

        if($supportedFile === false){
            $notSupported[] = $fileNameParts["fileName"];
        }else{
            $fileInfo = [
                "name" => $request["files"]['uploadMedia']['name'],
                "type" => $fileType,
                "tmp_name" => $request["files"]['uploadMedia']['tmp_name'],
                "error" => $request["files"]['uploadMedia']['error'],
                "size" => $fileSize,
                "folderId" => $database->injectionProtect($request["data"]['uploadFolder'])
            ];
            if( !empty($fileInfo["name"]) && !empty($fileInfo["type"]) ){
                switch ($supportedFile) {
                case "file":
                    $uploadFile = $uploadClass->uploadFile($fileInfo);
                    break;
                case "video":
                    $uploadFile = $uploadClass->uploadFile($fileInfo);
                    break;
                case "audio":
                    $uploadFile = $uploadClass->uploadFile($fileInfo);
                    break;
                default:
                    $uploadFile = $uploadClass->uploadImage($fileInfo);
                }
            }
        }

        if($uploadFile === 0){
            return ['status' => 'error'];
        }else{
            // Cache
            $mediaCache = new MediaCache($database, $appController);

            $mediaCache->deleteCache(null);
            return ['status' => 'success'];
        }
    }