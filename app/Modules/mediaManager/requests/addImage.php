<?php
    $callback = ["status" => true, "message" => null];
    $adminLang = $localeEngine->returnAppLang();

    if(!isExisting('uploadFormData', $request["data"])){
        $callback = ["status" => false, "message" =>  translate("mod-mediaManager-return-add-false")];
    }elseif(!isExisting('objectId', $request["data"]["uploadFormData"])){
        $callback = ["status" => false, "message" =>  translate("mod-mediaManager-return-add-false-id")];
    }elseif(!isExisting('template', $request["data"]["uploadFormData"])){
        $callback = ["status" => false, "message" =>  translate("mod-mediaManager-return-add-false-template")];
    }elseif(!isExisting('selectedImages', $request["data"]["uploadFormData"])){
        $callback = ["status" => false, "message" =>  translate("mod-mediaManager-return-add-false-file")];
    }

    if($callback["status"] === true){
        foreach($request["data"]["uploadFormData"]["selectedImages"] as $image){
            $imageInfo = $mediaController->getOneMediaById($image, $adminLang);

            // IF image is not associated, create association
            if( empty($imageInfo["template"]) AND $imageInfo["objectId"] == 0 ){
                $objectInfo = ["objectId" => $request["data"]["uploadFormData"]["objectId"], "template" => $request["data"]["uploadFormData"]["template"], "updatedDate" => date("Y-m-d H:i:s")];

                $sortedData = $localizationClass->createMultiTableQuery($objectInfo, $mediaController->translateRows, $mediaController->dataRows);
                $successAction = $localizationClass->updateQuery($sortedData, $mediaController->dataTable, $mediaController->translateTable, $imageInfo["id"], $adminLang);
                if($successAction === true){
                    $renderAction = "addImageToObject";
                    $objectId = $imageInfo["id"];
                }
            }else{
                $objectInfo = $imageInfo;
                $objectInfo["objectId"] = $request["data"]["uploadFormData"]["objectId"];
                $objectInfo["template"] = $request["data"]["uploadFormData"]["template"];
                $objectInfo["updatedDate"] = date("Y-m-d H:i:s");
                $objectInfo["head"] = 0;
                $objectInfo["grouped"] = 0;
                $objectInfo["langShort"] = $adminLang;
                $objectInfo["duplicated"] = 1;
                unset($objectInfo["id"]);

                $sortedData = $localizationClass->createMultiTableQuery($objectInfo, $mediaController->translateRows, $mediaController->dataRows);
                $successAction = $localizationClass->addQuery($sortedData, $mediaController->dataTable, $mediaController->translateTable);

                if($successAction !== false){
                    $renderAction = "copyImageToObject";
                    $objectId = $successAction;
                    $successAction = true;
                }
            }

            // Log to database
            if($successAction === true && $objectId != 0){
                $systemLogger->createLog($adminLang, "mediaManager", $renderAction, $objectId, $_SESSION["security"]["adminLogin"]["logIn"]);
                $callback = ["status" => true, "message" =>  translate("mod-mediaManager-return-add-true")];
                $cache = new Cache();
                $cache->removeCache('Entity', "{$objectInfo["template"]}/{$objectInfo["objectId"]}/public");
            }else{
                $successAction = false;
                $callback = ["status" => false, "message" =>  translate("mod-mediaManager-return-add-false-adding")];
            }

            if($successAction !== true){
                break;
            }
        }

        // Cache
        $mediaCache = new MediaCache($database, $appController);
        $mediaCache->deleteCache($request["data"]["uploadFormData"]["template"], true);
    }

    echo '<p class="return '.(($callback["status"] === true) ? "true" : "false").'">'.$callback["message"].'</p>';