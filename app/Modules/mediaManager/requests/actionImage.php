<?php
    if(!isExisting('id', $request["data"])){
        return false;
    }else{
        $objectId = $request["data"]["id"];
        $actionName = $request["data"]['action'];
        $fileType = $request["data"]["fileType"];
    }
    
    $folderManager = new folderManager($database, $appController);
    $adminLang = $localeEngine->returnAppLang();

    if($objectId){
        $object = $mediaController->getOneMediaById($objectId, $adminLang);
        if($fileType === "file" && empty($object) ){
            return false;
        }

        // DELETE
        if($actionName == "delete"){
            if($fileType === "file"){
                $mediaController->deleteMedia($object["id"]);
                $renderAction = "deleteMedia";
            }else{
                $removeFolder = $folderManager->deleteFolder($objectId);
                $renderAction = "deleteFolder";
            }
        }

        // SAVE DETAILS
        if($actionName == "saveDetails" && isset($request["data"]['info'])){
            if($fileType == "file"){
                $sortedData = $localizationClass->createMultiTableQuery($request["data"]['info'], $mediaController->translateRows, $mediaController->dataRows);

                $translateSql = "SELECT id FROM `{$mediaController->translateTable}` WHERE associatedId = :associatedId AND langShort = :langShort;";
                $translateCheck = $database->getQuery($translateSql, ["associatedId" => $objectId, "langShort" => $adminLang], false);

                if(!empty($translateCheck)) {
                    $update = $localizationClass->updateQuery($sortedData, $mediaController->dataTable, $mediaController->translateTable, $objectId, $adminLang);
                }else{
                    $update = $localizationClass->addQuery($sortedData, $mediaController->dataTable, $mediaController->translateTable, $objectId);
                }
            }else{
                $enableFolderTranslate = ($adminLang == DEFAULT_LANG) ? false : true;
                $request["data"]['info']["id"] = $objectId;
                $sortedData = $localizationClass->createMultiTableQuery($request["data"]['info'], $folderManager->translateRows, $folderManager->dataRows);
                $update = $localizationClass->updateQueryAndTranslate($sortedData, $folderManager->dataTable, $objectId, $folderManager->translateType, $adminLang, null, $enableFolderTranslate);
            }

            if($update === true){
                $renderAction = "update'.$fileType.'Details";
                if($fileType == "file"){
                    $object["title"] = $request["data"]['info']["title"];
                    $object["alt"] = $request["data"]['info']["alt"];
                    echo $mediaProcessor->getFileInfo($object)["viewName"];
                }
            }
        }

        // Remove image from product settings
        if($actionName == "remove"){
            if($object["duplicated"] === 1){
                $mediaController->deleteMediaFromDatabase($objectId);
                $renderAction = "removeDuplicatedImage";
            }else{
                $mediaController->resetMediaToDefault($objectId);
                $renderAction = "removeImageFromObject";
            }
        }

        // Switch image parameter - Head image
        if($actionName == "head"){
            $dataValues = ["head" => ($object["head"] == 0) ? 1 : 0, "updatedDate" => date("Y-m-d H:i:s")];
            $sortedData = $localizationClass->createMultiTableQuery($dataValues, $mediaController->translateRows, $mediaController->dataRows);
            $update = $localizationClass->updateQuery($sortedData, $mediaController->dataTable, $mediaController->translateTable, $objectId, $adminLang);
            if($update === true){
                $renderAction = "setImageAsHead";
            }
        }

        // Switch image parameter - Grouped image
        if($actionName == "grouped"){
            $dataValues = ["grouped" => ($object["grouped"] == 0) ? 1 : 0, "updatedDate" => date("Y-m-d H:i:s")];
            $sortedData = $localizationClass->createMultiTableQuery($dataValues, $mediaController->translateRows, $mediaController->dataRows);
            $update = $localizationClass->updateQuery($sortedData, $mediaController->dataTable, $mediaController->translateTable, $objectId, $adminLang);
            if($update === true){
                $renderAction = "addImageToGroup";
            }
        }

        // Cache
        $mediaCache = new MediaCache($database, $appController);
        $mediaCache->deleteCache($object["template"], true);

        $cache = new Cache();
        $cache->removeCache('Entity', "{$object["template"]}/{$object["objectId"]}/public");

    // Log to database
        if( isset($renderAction) && !empty($objectId) ){
          $systemLogger->createLog($adminLang, "mediaManager", $renderAction, $objectId, $_SESSION["security"]["adminLogin"]["logIn"]);
        }

    }