<?php
    $activeFolder = ( isset($request["data"]["activeFolder"]) ) ? $request["data"]["activeFolder"] : 0;
    $set_objectId = ( isset($request["data"]["objectId"]) ) ? $request["data"]["objectId"] : $_GET["objectId"];
    $set_template = ( isset($request["data"]["template"]) ) ? $request["data"]["template"] : $_GET["template"];
    $paginationStep = ( isset($request["data"]["paginationNum"]) ) ? $request["data"]["paginationNum"] : 0;

    $folderManager = new folderManager($database, $appController);// TODO: Medie manager FOLDER nenačítá (není classa)
    $tableForScripts = $mediaController->dataTable;

    $paginationLimit = $appController->getModuleConfig("mediaManager", "modulePaginationLimit");
    $resultsStart = ($paginationStep === 0) ? 0 : $paginationStep * $paginationLimit;
    $resultsCount = $folderManager->getFilesCount($activeFolder);
    $foldersLimit = ceil($resultsCount/$paginationLimit);

    $pageData = $mediaController->getAllMediaByFolderId($activeFolder, $appController->getLang(), (int)$paginationLimit, (int)$resultsStart);
    $folderInfo = $folderManager->getOneFolderById($activeFolder, $appController->getLang());
    $foldersData = $folderManager->getAllFoldersListed(0, $appController->getLang(), 0, false);

    (!empty($folderInfo)) ? $pageTitle = $folderInfo["title"] : null;

/* =============================== CONTENT ======== */
echo '<div id="adminWrap">';
    echo '<div id="content">';
        echo '<div class="allFiles ajaxBox">';
            include(MODULE_DIR."mediaManager/includes/admin/foldersList.php");

            echo '<div class="filesList">';
                echo '<form action="" method="POST" id="listOfFiles">';
                    echo '<div class="mediaButtons">';
                        echo '<span id="sendImageToUpload" class="create btn icon save success">'.translate("mod-mediaManager-list-add").' (<b>0</b>)</span>';
                        echo '<span id="ajaxUploadFile" class="upload btn icon add inverted">'.translate("mod-mediaManager-ajax-upload").'</span>';
                        echo '<input type="hidden" name="objectId" value="'.$set_objectId.'" />';
                        echo '<input type="hidden" name="template" value="'.$set_template.'" />';
                        echo '<input type="hidden" name="activeFolder" value="'.$activeFolder.'" />';
                        echo '<div class="returnInfo"></div>';
                    echo '</div>';

                    echo '<div class="filesWrap">';
                        if(!empty($pageData)){
                            $fileActions = ["delete" => null, "sort" => null, "head" => null, "group" => null, "selector" => true];
                            foreach($pageData as $file){
                                include(MODULE_DIR."mediaManager/includes/admin/fileWrap.php");
                            }
                        }
                    echo '</div>';

                    if($resultsCount > $paginationLimit){
                        echo '<div id="pagination" data-folder="'.$activeFolder.'" data-folder-max="'.$foldersLimit.'">';
                            echo '<b>'.translate("mod-mediaManager-pagination-activePage", ["currentPage" => $paginationStep + 1, "totalPages" => $foldersLimit]).'</b>';
                            echo '<div class="wrap">';
                                echo '<span class="prevBox">';
                                    echo '<span class="cont prev btn warning '.(($paginationStep == 0) ? "inverted" : null).'" data-step="'.($paginationStep - 1).'" data-type="prev">&#171; '.translate("mod-mediaManager-pagination-prev").'</span>';
                                echo '</span>';
                                echo '<span class="nextBox">';
                                    echo '<span class="cont next btn warning '.((($paginationStep + 1) >= $foldersLimit) ? "inverted" : null).'" data-step="'.($paginationStep + 1).'" data-type="next" >'.translate("mod-mediaManager-pagination-next").' &#187;</span>';
                                echo '</span>';
                            echo '</div>';
                        echo '</div>';
                    }
                echo '</form>';
                echo '<div id="ajaxUpload" style="display: none;">';
                    echo '<div class="mediaButtons">';
                        echo '<span id="ajaxBack" class="upload btn icon back inverted">'.translate("mod-mediaManager-ajax-backToList").'</span>';
                    echo '</div>';
                    include(MODULE_DIR."mediaManager/includes/admin/uploadForm.php");
                echo '</article>';

            echo '</div>';

    echo '</div>';
echo '</div>';