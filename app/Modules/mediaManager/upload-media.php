<?php
/* =============================== FUNCTIONS ======== */ 
$prevPage = "media-list";
$activeFolder = ( isset($_GET["uploadFolder"]) ) ? $_GET["uploadFolder"] : 0;

if($activeFolder != 0){
  $prevPage = $prevPage."?folder=".$activeFolder;
}

/* =============================== CONTENT ======== */ 
echo '<article class="module">';
    $render->header(translate("mod-mediaManager-add_file"), $prevPage);
    include(MODULE_DIR."mediaManager/includes/admin/uploadForm.php");
echo '</article>';