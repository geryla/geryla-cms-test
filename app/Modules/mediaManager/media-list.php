<?php
/* =============================== FUNCTIONS ======== */
$folders = new folderManager($database, $appController);

$langShort = $presenter->templateData->langShort;
$tableForScripts = $mediaController->dataTable;

$activeFolder = ( isset($_GET["folder"]) ) ? $_GET["folder"] : 0;
$nextPage = "upload-media?uploadFolder=".$activeFolder;
$refreshPage = "media-list";
$pageTitle = translate("mod-mediaManager-files-route-main");

$folderInfo = $folders->getOneFolderById($activeFolder, $langShort);
if( $activeFolder != 0 && empty($folderInfo) ){
    $dataCompiler->setContinue(true);
    $dataCompiler->redirectPage(0, $refreshPage);
}


$filterToModule->renderFiltration($_SERVER['REQUEST_URI'], $_GET, true);
$foldersData = $folders->getAllFoldersListed(0, $langShort, 0, false);
$pageData = $filterToModule->getAll_MediaFiles($langShort, $activeFolder, $filterToModule->activeFilters, null, $filterToModule->limit, $filterToModule->offset, $filterToModule->activeFilters["orderBy"], $filterToModule->activeFilters["orderDirection"]);

(!empty($folderInfo)) ? $pageTitle = $folderInfo["title"] : null;

if( isset($_POST["submit"]) && (isset($_POST["newTitle"]) && !empty($_POST["newTitle"])) ){

// SetUp variables
  $continue = true;
  $postData = $_POST;

// Create additional params
  $postData['title'] = $postData['newTitle'];
  unset($postData['submit']);
  unset($postData['newTitle']);
  unset($postData['alt']);

// Module work (add/edit/translate)
  $dataCompiler->setTables($folders->dataTable, null);
  $dataCompiler->setData($postData, $folders->translateRows, $folders->dataRows);
  $dataCompiler->setTranslate($presenter->activeContent->enableTranslate);

  $pageId = $dataCompiler->addQueryAndTranslate(); // Add and translate

// Log
  if($continue === true){
    $systemLogger->createLog($langShort, $presenter->activeContent->module, "createFolder", $pageId, $_SESSION["security"]["adminLogin"]["logIn"]);
  }

  $refreshPage = ($continue === true) ? 1 : 0;
}

$dataCompiler->showMessage();
$dataCompiler->refreshPage($dataCompiler->nextStep, "");

/* =============================== CONTENT ======== */
echo '<article class="module">';
    $render->header("<b>".translate("mod-mediaManager-files-route").": </b>".$pageTitle, null, null);
    echo '<section>';
        echo '<div class="moduleWrap allFiles">';
            include(MODULE_DIR."mediaManager/includes/admin/foldersList.php");

            echo '<div class="filesList">';
                echo '<div class="mediaButtons">';
                    echo '<a href="'.$nextPage.'" class="upload btn icon add inverted">'.translate("mod-mediaManager-files-uploadFiles").'</a>';
                    echo '<span id="createFolder" class="create btn icon folder-open warning">'.translate("mod-mediaManager-files-createFolder").'</span>';
                    echo '<span class="newFolder hide">';
                        echo '<form action="" method="POST">';
                            echo $render->input("text", "newTitle", "", null, false, null, null);
                            echo $render->input("hidden", "parentId", $activeFolder, null, false, null, null);
                            echo $render->input("submit", "submit", translate("save"), null, false, null, null);
                        echo '</form>';
                    echo '</span>';
                    echo ($activeFolder != 0) ? '<span id="editFolder" class="add btn icon refresh">'.translate("mod-mediaManager-files-editFolder").'</span>' : null;
                echo '</div>';

                echo '<div class="filesWrap">';
                    echo '<div class="detailsWrap">';
                        include(MODULE_DIR."mediaManager/includes/admin/fileInfo-file.php");
                        include(MODULE_DIR."mediaManager/includes/admin/fileInfo-folder.php");
                    echo '</div>';

                    if(!empty($pageData)){
                        $fileActions = ["delete" => true, "sort" => null, "head" => null, "group" => null, "selector" => false];
                        foreach($pageData as $file){
                            include(MODULE_DIR."mediaManager/includes/admin/fileWrap.php");
                        }
                    }
                echo '</div>';
            echo '</div>';
        echo '</div>';
    echo '</section>';
echo '</article>';