<?php

namespace Api\templates;

class ApiFactory extends \ApiController {

    private $requestMethods;
    private $errorCodes = [
        400 => 'Access denied - private route requested',
        401 => 'Invalid route',
        402 => 'No API bundles found',
        403 => 'The requested API bundle is not defined',
        404 => 'The requested API bundle is empty',
        405 => 'The requested API has missing method',
    ];

    public function __construct(\Database $database, \AppController $appController, Array $requestData) {
        parent::__construct($database, $appController, $requestData);

        $this->requestMethods = include(__DIR__ . '/requests.php');
    }

    public function getErrorCodes() {
        return $this->returnErrorCodes($this->errorCodes);
    }

    public function validate(){
        return $this->validateRequest($this->requestData, $this->requestMethods);
    }
    public function proceedRequest() {

        switch ($this->requestData["action"]) {
            case "getRoute":
                $this->getRoute();
                break;
            case "getBundle":
                $this->getBundle();
                break;
            case "getRedirect":
                $this->getRedirect();
                break;
            default:
                $this->setState(310);
                return false;
        }

        $this->closeAuthRequest();
        return true;
    }

    private function getRoute() {
        $router = new \Router($this->database, $this->appController, true);
        $router->define($this->requestData["body"]["url"]);
        if ($router->routerType !== "project") {
            $this->setState(400);
            return false;
        }

        $routerData = $router->start(false, ["preLoadedLocale" => true, "thumbnail" => $this->requestParametersValidated["thumbnail"]]);
        if ($routerData === null) {
            $this->setState(401);
            return false;
        }

        $this->setResponseData($this->restructureResponse($routerData));
        return true;
    }
    private function getRedirect() {
        $router = new \Router($this->database, $this->appController, true);
        $router->define($this->requestData["body"]["url"]);
        if ($router->routerType !== "project") {
            $this->setState(400);
            return false;
        }

        $routerData = $router->start(false, ['redirectOnly' => true, "preLoadedLocale" => true]);
        if ($routerData === null) {
            $this->setState(401);
            return false;
        }

        $this->setResponseData($this->restructureResponse($routerData));
        return true;
    }
    private function getBundle() {
        if (!file_exists(CONFIG_DIR.'/_ApiBundles.php')) {
            $this->setState(402);
            return false;
        }

        $apiBundles = include_once(CONFIG_DIR.'/_ApiBundles.php');
        if (!isset($apiBundles[$this->requestData["value"]])) {
            $this->setState(403);
            return false;
        }

        $apiBundle = $apiBundles[$this->requestData["value"]];
        if (count($apiBundle) === 0) {
            $this->setState(404);
            return false;
        }

        $apiResponses = [];
        foreach ($apiBundle as $alias => $path) {
            $request = $inputData = [];
            $request["path"] = $path;

            if (isset($this->requestData["body"]["data"][$alias])) {
                $inputData = $this->requestData["body"]["data"][$alias];
            }

            if (isset($inputData["value"])) {
                $request["path"] = str_replace('{value}', $inputData["value"], $request["path"]);
            }
            if (isset($inputData["params"])) {
                $request["path"] .= '?' . http_build_query($inputData["params"]);
            }

            if (isset($inputData["body"])) {
                $request["body"] = $inputData["body"];
            }

            $reqRouter = new \Router($this->database, $this->appController);
            $reqRouter->define('/api' . $request["path"]);
            $reqUrl = $reqRouter->getParsedUrl();
            $reqUrl["requestMethod"] = (isset($inputData["method"])) ? $inputData["method"] : $reqUrl["requestMethod"];

            $apiRouters = new \ApiRouter($this->database, $this->appController, $reqUrl);

            if (!isset($inputData["method"])) {
                $apiRouters->setError(500, 405, $this->errorCodes[405]);
            }else{
                $apiRouters->validate();
            }

            $apiResponses[$alias] = $apiRouters->output(true);
        }


        $this->setResponseData($apiResponses);
        return true;
    }


    private function restructureResponse(Object $routerData) {
        $basicData = ["code", "redirect", "maintenance", "activeTemplate"];
        $responseData = [];

        foreach ($routerData as $key => $value) {
            if (in_array($key, $basicData) || ($this->requestParametersValidated["with"] !== NULL && in_array($key, $this->requestParametersValidated["with"]))) {
                $responseData[$key] = $value;
            }
        }

        return $responseData;
    }

}