<?php

return [
    "getRoute" => [
        "path" => "/templates/getRoute",
        "method" => "POST",
        "authorized" => true,
        "cache" => false,
        "body" => [
            "url" => [
                "required" => true,
                "default" => Null,
                "type" => 'String',
                "description" => "URL path",
            ],
        ],
        "params" => [
            "with" => [
                "required" => false,
                "default" => [],
                "type" => 'Array',
                "description" => "Returns optimized object with specific extensions",
                "values" => "templateData, breadCrumbs, metaData, activeLanguage, templateAnchors",
            ],
            "thumbnail" => [
                "required" => false,
                "default" => Null,
                "type" => 'String',
                "description" => "Returns the specific compressed and resized images"
            ],
        ]
    ],
    "getRedirect" => [
        "path" => "/templates/getRedirect",
        "method" => "POST",
        "authorized" => false,
        "cache" => true,
        "body" => [
            "url" => [
                "required" => true,
                "default" => Null,
                "type" => 'String',
                "description" => "URL path",
            ],
        ],
    ],
    "getBundle" => [
        "path" => "/templates/getBundle/{value}",
        "method" => "POST",
        "authorized" => true,
        "cache" => false,
        "value" => [
            "required" => true,
            "type" => 'String',
            "description" => "Bundle name from config"
        ],
        "body" => [
            "data" => [
                "required" => true,
                "default" => [],
                "type" => 'Array',
                "description" => "Multidimensional object with a specific data for each request",
            ],
        ]
    ],
];