<?php
    $config = [
        "moduleName" => "Templates",
        "moduleUrlName" => "templates",
        "moduleLangName" => "templates",
        "moduleIcon" => "fa-file",
        "moduleVersion" => "3.6.1",
        "requiredModules" => [
            ["moduleName" => "localization"],
            ["moduleName" => "mediaManager"],
        ],

        "mediaSettings" => [
            "template" => "template"
        ],
        "translateType" => [],
        "databaseTables" => [
            "templateTable" => [
                "name" => "templates",
                "entityBonds" => [
                    "template" => ["major" => true],
                ]
            ],
            "templateTableLang" => [
                "name" => "templates-lang",
                "entityBonds" => [
                    "template" => ["major" => true],
                ]
            ],
            "prefixTable" => [
                "name" => "prefixs",
            ],
            "redirectsTable" => [
                "name" => "redirects",
            ],
        ],

        "modulePages" => [
            [
                "pageId" => 1,
                "urlName" => "templates-list",
                "url" => "templates-list",
                "parent" => 0,
                "inMenu" => 1,
                "headPage" => 1
            ],
            [
                "pageId" => 2,
                "urlName" => "template-detail",
                "url" => "template-detail",
                "parent" => 1,
                "inMenu" => 0,
                "headPage" => 0
            ],
            [
                "pageId" => 3,
                "urlName" => "redirected-urls",
                "url" => "redirected-urls",
                "parent" => 0,
                "inMenu" => 1,
                "headPage" => 0,
                "groupId" => 3,
                "groupOrder" => 10
            ]
        ]
    ];