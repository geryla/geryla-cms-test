<?php
/* =============================== CONTROLLER METHODS ======== */
$redirectedUrls = new redirectedUrls($database);
$listPage = $dataCompiler->modulePageUrl(3);

$pageTitle = translate("mod-templates-admin-redirects-list");
$prevButton = null;
$nextButton = $listPage."?action=add";
$disableList = null;

// ACCESS CONTROL
$pageId = ( isset($_GET["id"]) ) ? $_GET["id"] : 0;
$pageAction = ( isset($_GET["action"]) ) ? $_GET["action"] : null;
if( $pageAction !== null && (($pageAction == "edit" && $pageId == 0) OR ($pageAction == "add" && $pageId != 0))){
    $dataCompiler->setContinue(true);
    $dataCompiler->redirectPage(0, $listPage);
}

$filterToModule->setSortOptions([
    [
        'title' => translate("mod-templates-admin-filtration-sort-status"),
        'type' => 'select',
        'name' => 'status',
        'values' => $redirectedUrls->getStatusesAsSelect()
    ]
]);
$filterToModule->renderFiltration($_SERVER['REQUEST_URI'], $_GET, false, true);
$filterToModule->activeFilters["noCache"] = true;
$allUrls = $filterToModule->getAll_redirectedUrls($filterToModule->activeFilters, null, $filterToModule->limit, $filterToModule->offset, useIfExists($filterToModule->activeFilters, null, 'orderBy'), useIfExists($filterToModule->activeFilters, null, 'orderDirection'));
$allEnabledLanguages = $localizationClass->getAllActiveLanguages();

// CODE DETAIL load
if($pageAction !== null){
    $oneInfo = $redirectedUrls->getById($pageId);

    $pageTitle = ($pageAction == "edit") ? translate("mod-templates-admin-redirects-edit").': <b>'.$oneInfo["alias"].'</b>' : translate("mod-templates-admin-redirects-add");
    $prevButton = $listPage;
    $nextButton = null;
    $disableList = "disabled";
}

if( isset($_POST['submit']) ){
    $postData = [];
    foreach ($_POST as $key => $value) {
        if($key == 'submit'){
            continue;
        }else{
            $postData[$key] = $value;
        }
    }

// Module work (add/edit/translate)
    $dataCompiler->setTables($redirectedUrls->redirectsTable, null);
    $dataCompiler->setData($postData, null, $redirectedUrls->dataRows);
    $dataCompiler->setTranslate(false);

    if($pageId == 0){
        $pageId = $dataCompiler->addQuery(); // Add
    }else{
        $dataCompiler->updateQuery($pageId); // Update
    }

// Log
    if($dataCompiler->nextStep === true){
        $systemLogger->createLog($presenter->templateData->langShort, $presenter->activeContent->module, $pageAction."Redirects", $pageId, $_SESSION["security"]["adminLogin"]["logIn"]);
    }
}

$dataCompiler->showMessage();
$dataCompiler->refreshPage($dataCompiler->nextStep, ($pageAction == "add" && $pageId != 0) ? $presenter->activeContent->page.'?action=edit&id='.$pageId : "");

/* =============================== CONTENT ======== */
echo '<article class="module">';
    $render->header($pageTitle, $prevButton, $nextButton);
    echo '<section class="availabilities">';
        echo '<div class="moduleWrap">';
            if($pageAction !== null){
                echo '<form action="" method="POST" enctype="multipart/form-data">';
                    $render->contentDiv(true, null, "contentBox");
                        echo $render->button("submit", "true", translate("save"), false, null, "btn icon save success");
                        include($render->getBoxPath("templates", "redirects"));
                        echo $render->button("submit", "true", translate("save"), false, null, "btn icon save success");
                    $render->contentDiv(false);
                echo '</form>';
            }

            echo '<table class="striped '.$disableList.'">';
            $render->tableHead([
                translate("mod-templates-admin-redirects-alias"),
                translate("mod-templates-admin-redirects-inputUrl"),
                translate("mod-templates-admin-redirects-outputUrl"),
                translate("mod-templates-admin-redirects-status"),
                translate("mod-templates-admin-redirects-langShort"),
                translate("mod-templates-admin-list-visibility"),
                '']);
            foreach($allUrls as $url){
                echo '<tr>';
                    $render->tableColumn('<b>'.$url["alias"].'</b>');
                    $render->tableColumn('<i class="blue small">'.$url["inputUrl"].'</i>');
                    $render->tableColumn('<i class="green small">'.$url["outputUrl"].'</i>');
                    $render->tableColumn('<i class="red small">'.$url["status"].'</i>');
                    $render->tableColumn('<b class="orange small">'.$url["langShort"].'</b>');
                    $render->tableColumn($render->visibilitySwitch($url["id"], "redirects", $url["id"], $url["visibility"]), 80, true);

                    $editSwitch = '?action=edit&id='.$url["id"];
                    $deleteSwitch = 'class="deleteContent" name="redirects" data="'.$url["id"].'"';
                    $render->tableActionColumn($editSwitch, $deleteSwitch, null, 100, false);
                echo '</tr>';
            }
            echo '</table>';
        echo '</div>';
    echo '</section>';
echo '</article>';