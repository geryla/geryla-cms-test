<?php
/* =============================== CONTROLLER METHODS ======== */
$templatesClass = new templateClass($database, $appController);

$templatesTable = $templatesClass->templatesTable;
$templatesTranslateTable = $templatesClass->templatesLangTable;
$prevPage = $dataCompiler->modulePageUrl(1);

$mediaInfo["name"] = $modulesController->getModuleMediaName($templatesClass->mediaInfoName);

// ACCESS CONTROL
$pageId = ( isset($_GET["id"]) ) ? $_GET["id"] : null;
if($pageId === null){
$dataCompiler->setContinue(true);
$dataCompiler->redirectPage(0, $prevPage);
}

/* =============================== LANGUAGE ADD-ON METHODS ======== */
$pageData = $templatesClass->getTemplateInfoById($pageId, $presenter->templateData->langShort);
$previewLink = ($pageData && ($pageData["pageType"] == 0 OR $pageData["pageType"] == 1 OR $pageData["pageType"] == 3)) ? $render->getPreviewPath($pageData["templateName"], $pageData["url"], null, true) : null;

if( isset($_POST['submit']) ){
// SetUp variables
    $continue = true;
    $postData = $_POST;
    unset($postData['submit']);

// Create additional params + check URL
    $postData["url"] = $prefixEngine->title2pagename( (empty($postData["url"])) ? $postData["title"] : $postData["url"] );
    $postData["updatedDate"] = date("Y-m-d H:i:s");

// Module work (add/edit/translate)
    $dataCompiler->setTables($templatesClass->templatesTable, $templatesClass->templatesLangTable);
    $dataCompiler->setData($postData, $templatesClass->translateRows, $templatesClass->dataRows);
    $dataCompiler->setTranslate($presenter->activeContent->enableTranslate);

    $dataCompiler->updateQuery($pageId); // Update template detail
    $dataCompiler->updateImages($mediaController->translateTable); // Edit images info

// Log to system
    if($dataCompiler->nextStep === true){
        $systemLogger->createLog($presenter->templateData->langShort, $presenter->activeContent->module, "templateDetail", $pageId, $_SESSION["security"]["adminLogin"]["logIn"]);
    }
}

$dataCompiler->showMessage();
$dataCompiler->refreshPage($dataCompiler->nextStep, "");


/* =============================== CONTENT ======== */ 
echo '<article class="module">';
    $render->header(translate("mod-templates-admin-edit").': <b>'.$pageData["title"].'</b>', $prevPage, null, $previewLink);
    echo '<section>';
        echo '<form action="" method="POST" enctype="multipart/form-data">';
            echo $render->button("submit", "true", translate("save"), false, null, "btn icon save success");

            echo '<div class="moduleWrap">';
                $render->contentDiv(true, null, "optionBox");
                    $render->contentDiv(true, null, "boxOption");
                        $render->boxTab("templates", "basicInfo", "mod-templates-admin-tabs-basic", "basic", "tab active", false);
                        $render->boxTab("mediaManager", "images", "mod-mediaManager-admin-tabs-files", "images", "tab", false);
                        $render->boxTab("templates", "customize", "mod-templates-admin-tabs-customize", "customize", "tab", false);
                        $render->boxTab("templates", "seo", "mod-templates-admin-tabs-seo", "seo", "tab", false);
                    $render->contentDiv(false);
                $render->contentDiv(false);

                $render->contentDiv(true, null, "contentBox");
                    foreach($render->moduleBoxes as $moduleBox => $boxPath){
                        include($boxPath);
                    }
                $render->contentDiv(false);
            echo '</div>';

            echo $render->button("submit", "true", translate("save"), false, null, "btn icon save success");
        echo '</form>';
    echo '</section>';
echo '</article>';