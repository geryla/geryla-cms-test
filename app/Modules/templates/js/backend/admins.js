$(document).on('ready', function() {
    let activeSubcategory = $("ul.permissionsCheck").find('input[type="checkbox"]:checked').closest('.subcategory');
    $(activeSubcategory).show();
    $(activeSubcategory).closest('li').find('label > i').toggleClass('fa-plus-square-o');
});