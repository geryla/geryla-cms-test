<?php
    $jobFinished = true;
    $enabledLocales = $localizationClass->getAllActiveLanguages();
    $existingLocales = array_column($enabledLocales, "langShort");
    $existingSitemaps = [];

    function buildSitemapUrlPath($path, $url = null, $prefix = null, $linkParts = []) {
        $link = $path;

        if (!empty($linkParts)) {
            foreach ($linkParts as $part) {
                $link .= $part."/";
            }
        }

        $link .= ($prefix !== null) ? $prefix . "-" : "";
        $link .= ($link !== null) ? $url : "";

        return $link;
    }

    foreach($existingLocales as $sitemapLocale){
        $sitemapLinks = [];
        $existingTemplates = [];

        $path = $appController->getProjectPath($sitemapLocale);

        $templatesTable = databaseTables::getTable('templates', "templateTable");
        $templatesLangTable = databaseTables::getTable('templates', "templateTableLang");
        $allTemplates = $database->getQuery("SELECT lang.url, data.templateName, data.pageType, data.updatedDate FROM `{$templatesTable}` AS data LEFT JOIN `{$templatesLangTable}` AS lang ON data.id = lang.associatedId WHERE lang.langShort = :langShort AND data.visibility = 1 AND data.allowIndex = 1 AND data.loggedOnly = 0", ["langShort" => $sitemapLocale], true);
        if(!empty($allTemplates)){
            foreach($allTemplates as $template){
                $existingTemplates[$template["templateName"]] = $template;
                if($template["pageType"] === 0 || $template["pageType"] === 1 || $template["pageType"] === 3){
                    $templateUrl = ($template["templateName"] !== 'home') ? $template["url"] : null;
                    $sitemapLinks[] = ['url' => buildSitemapUrlPath($path, $templateUrl), 'priority' => '0.9', 'freq' => 'weekly', 'updated' => $template["updatedDate"]];
                }
            }
        }

        if(!empty($existingTemplates)){
            if($appController->isActiveModule('products')){
                if(isset($existingTemplates["productDetail"])){
                    $productsTable = databaseTables::getTable('products', "productTable");
                    $productsLangTable = databaseTables::getTable('products', "productTableLang");
                    $allProducts = $database->getQuery("SELECT data.urlPrefix, lang.url, data.updatedDate FROM `{$productsTable}` AS data LEFT JOIN `{$productsLangTable}` AS lang ON data.id = lang.associatedId WHERE lang.langShort = :langShort AND data.visibility = 1", ["langShort" => $sitemapLocale], true);
                    if(!empty($allProducts)){
                        foreach($allProducts as $product){
                            $sitemapLinks[] = ['url' => buildSitemapUrlPath($path, $product["url"], $product["urlPrefix"], [$existingTemplates["productDetail"]["url"]]), 'priority' => '0.9', 'freq' => 'daily', 'updated' => $product["updatedDate"]];
                        }
                    }
                }
                if(isset($existingTemplates["productCategory"])){
                    $categoryTable = databaseTables::getTable('products', "categoryTable");
                    $categoryTableTable = databaseTables::getTable('products', "categoryTableLang");
                    $allCategories = $database->getQuery("SELECT data.urlPrefix, lang.url FROM `{$categoryTable}` AS data LEFT JOIN `{$categoryTableTable}` AS lang ON data.id = lang.associatedId WHERE lang.langShort = :langShort AND data.visibility = 1", ["langShort" => $sitemapLocale], true);
                    if(!empty($allCategories)){
                        foreach($allCategories as $category){
                            $sitemapLinks[] = ['url' => buildSitemapUrlPath($path, $category["url"], $category["urlPrefix"], [$existingTemplates["productCategory"]["url"]]), 'priority' => '0.9', 'freq' => 'daily', 'updated' => null];
                        }
                    }
                }
            }
            if($appController->isActiveModule('contentEditor')){
                if(isset($existingTemplates["blogDetail"])){
                    $postsTable = databaseTables::getTable('contentEditor', 'postTable');
                    $postsLangTable = databaseTables::getTable('contentEditor', 'postTableLang');
                    $allBlogPosts = $database->getQuery("SELECT data.urlPrefix, lang.url, data.updatedDate FROM `{$postsTable}` AS data LEFT JOIN `{$postsLangTable}` AS lang ON data.id = lang.associatedId WHERE data.postType = 2 AND lang.langShort = :langShort AND data.visibility = 1 AND data.allowIndex = 1", ["langShort" => $sitemapLocale], true);
                    if(!empty($allBlogPosts)){
                        foreach($allBlogPosts as $blogPost){
                            $sitemapLinks[] = ['url' => buildSitemapUrlPath($path, $blogPost["url"], $blogPost["urlPrefix"], [$existingTemplates["blogDetail"]["url"]]), 'priority' => '0.9', 'freq' => 'daily', 'updated' => $blogPost["updatedDate"]];
                        }
                    }
                }
            }
        }

        if(!empty($sitemapLinks)){
            $xml = "<?xml version='1.0' encoding='UTF-8'?" . ">\n";
            $xml .= "<urlset xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'>\n";
            foreach($sitemapLinks as $link){
                $xml .="<url>";
                    $xml .= "\t\t<loc>".$link["url"]."</loc>\n";
                    $xml .= "\t\t<priority>".$link["priority"]."</priority>\n";
                    //$xml .= "\t\t<lastmod>".$link["updated"]."</lastmod>\n";
                    $xml .= "\t\t<changefreq>".$link["freq"]."</changefreq>\n";
                $xml .="</url>";
            }
            $xml .= "</urlset>\n";

            $sitemapName = "sitemap-{$sitemapLocale}.xml";
            $saveFile = file_put_contents(PUBLIC_DIR."/{$sitemapName}", $xml);
            if($saveFile !== false){
                $existingSitemaps[] = SERVER_PROTOCOL."://".SERVER_NAME.relativePath(PUBLIC_DIR).$sitemapName;
            }
            $jobFinished = ($saveFile !== false);
        }
    }

    if(!empty($existingSitemaps)){
        $xml = "<?xml version='1.0' encoding='UTF-8'?" . ">\n";
        $xml .= "<sitemapindex xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'>\n";
        foreach($existingSitemaps as $link){
            $xml .="<sitemap>";
                $xml .= "\t\t<loc>".$link."</loc>\n";
                $xml .= "\t\t<lastmod>".date('c')."</lastmod>\n";
            $xml .="</sitemap>";
        }
        $xml .= "</sitemapindex>\n";
    }
    $saveFile = file_put_contents(ROOT.'/sitemap.xml', $xml);

    return $jobFinished;