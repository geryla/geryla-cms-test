<?php
class prefixEngine {
    public $database;
    public $moduleName = "templates";


    private $prefixMin = 1000000;
    private $prefixMax = 9999999;

    public $prefixesTable;

    public function __construct(Database $database) {
        $this->database = $database;
        $this->prefixesTable = databaseTables::getTable($this->moduleName, "prefixTable");
    }

/* ================== Application functions === */

// Create URL
    public function title2pagename($title) {
        static $convertTable = array(
            'á' => 'a', 'Á' => 'A', 'ä' => 'a', 'Ä' => 'A', 'č' => 'c',
            'Č' => 'C', 'ď' => 'd', 'Ď' => 'D', 'é' => 'e', 'É' => 'E',
            'ě' => 'e', 'Ě' => 'E', 'ë' => 'e', 'Ë' => 'E', 'í' => 'i',
            'Í' => 'I', 'ï' => 'i', 'Ï' => 'I', 'ľ' => 'l', 'Ľ' => 'L',
            'ĺ' => 'l', 'Ĺ' => 'L', 'ň' => 'n', 'Ň' => 'N', 'ń' => 'n',
            'Ń' => 'N', 'ó' => 'o', 'Ó' => 'O', 'ö' => 'o', 'Ö' => 'O',
            'ř' => 'r', 'Ř' => 'R', 'ŕ' => 'r', 'Ŕ' => 'R', 'š' => 's',
            'Š' => 'S', 'ś' => 's', 'Ś' => 'S', 'ť' => 't', 'Ť' => 'T',
            'ú' => 'u', 'Ú' => 'U', 'ů' => 'u', 'Ů' => 'U', 'ü' => 'u',
            'Ü' => 'U', 'ý' => 'y', 'Ý' => 'Y', 'ÿ' => 'y', 'Ÿ' => 'Y',
            'ž' => 'z', 'Ž' => 'Z', 'ź' => 'z', 'Ź' => 'Z',
        );
        $title = strtolower(strtr($title, $convertTable));
        $title = preg_replace('/[^a-zA-Z0-9]+/u', '-', $title);
        $title = str_replace('--', '-', $title);
        $title = trim($title, '-');
        return $title;
    }
// Return unique prefix
    public function createPrefix() {
        do {
            $prefixNum = $this->controlPrefix($this->generatePrefix());
        } while ($prefixNum === false);

        return $prefixNum;
    }
// Generate random prefix number
    public function generatePrefix() {
        $randNum = mt_rand($this->prefixMin, $this->prefixMax);
        return $randNum;
    }
// Check if prefix exist
    public function controlPrefix($prefixNum) {
        $sql = "SELECT * FROM `{$this->prefixesTable}` WHERE urlPrefix = :urlPrefix;";
        $returnData = $this->database->getQuery($sql, array("urlPrefix" => $prefixNum), true);
        return (empty($returnData)) ? $prefixNum : false;
    }
// Add new prefix into database
    public function addNewPrefix($objectTable) {
        $prefix = $this->createPrefix();
        $returnData = $this->database->insertQuery("INSERT INTO `{$this->prefixesTable}`", ["urlPrefix" => $prefix, "contentTable" => $objectTable]);
        return ($returnData === 0) ? false : $prefix;
    }

// Delete prefix
    public function clearPrefixes() {
        $sql = "SELECT * FROM `{$this->prefixesTable}`";
        $prefixes = $this->database->getQuery($sql, null, true);

        foreach ($prefixes as $prefix) {
            $table = $prefix["contentTable"];
            $sql = "SELECT * FROM `{$table}` WHERE urlPrefix = :urlPrefix;";
            $returnData = $this->database->getQuery($sql, array("urlPrefix" => $prefix["urlPrefix"]), true);

            if (empty($returnData)) {
                $sql = "DELETE FROM `{$this->prefixesTable}`";
                $whereSql = "WHERE urlPrefix = :urlPrefix";
                $this->database->deleteQuery($sql, array("urlPrefix" => $prefix["urlPrefix"]), $whereSql);
            }
        }

        return true;
    }


}