<?php
class templateClass {
    public $moduleName = "templates";

    private $database;
    private $appController;
    private $localizationClass;

    private $mediaProcessor;
    private $contentBlocks;

    private $activeLangShort;
    public $allTemplates;

    public $templatesTable;
    public $templatesLangTable;
    public $mediaInfoName;

    public $translateRows = ["title", "shortDesc", "description", "metaKeywords", "metaTitle", "metaDescription", "url", "youtubeUrl", "langShort"]; // Only editable fields
    public $dataRows = ["allowIndex", "className", "updatedDate", "visibility"]; // Only editable fields
    public $pageTypes = [0 => false, 1 => false, 2 => true, 3 => null]; // URL CONTENT (www.XXX.xx/template/URL-CONTENT) >  0-false = DISABLE checking; 1-false = MUST NOT; 2-true = MUST; 3-null = BOTH (blog)

    public function __construct(Database $database, AppController $appController) {
        $this->database = $database;
        $this->appController = $appController;
        $this->localizationClass = $appController->returnLocalizationClass();
        $this->activeLangShort = $appController->getLang();

        $this->templatesTable = databaseTables::getTable($this->moduleName, "templateTable");
        $this->templatesLangTable = databaseTables::getTable($this->moduleName, "templateTableLang");
        $this->mediaInfoName = $this->appController->getModuleConfig($this->moduleName,"mediaSettings")["template"];

        $this->allTemplates = $this->getAllTemplateAnchors($this->activeLangShort);
    }


/* ================== Application functions === */
    public function optimize(Array $data, ?String $langShort = null, Array $options = []) {
        $options["deepLevel"] = (isset($options["deepLevel"])) ? $options["deepLevel"] + 1 : 0;
        $options["with"] = (isset($options["with"])) ? $options["with"] : ['all'];
        $options["itemThumbnail"] = (isset($options["itemThumbnail"]) && trim($options["itemThumbnail"]) !== "") ? $options["itemThumbnail"] : null;

        $langShort = ($langShort === null) ? $this->appController->getLang() : $langShort;
        $isSingleItem = (isset($data["id"]));
        $dataArray = ($isSingleItem === true) ? [$data] : $data;

        $optimizedData = [];
        if(!empty($dataArray)){
            foreach($dataArray as $key => $item){
                $optimizedData[$key] = (!empty($item)) ? $this->cacheItem($item, $langShort, $options) : [];
            }
        }

        return ($isSingleItem === true) ? $optimizedData[0] : $optimizedData;
    }
    public function cacheItem(Array $item, ?String $langShort = null, Array $options = []) {
        $entityCache = new EntityCache();
        $entityName = 'template';
        $langShort = ($langShort === null) ? $this->appController->getLang() : $langShort;

        $itemCache = [];
        if(!empty($item)){
            $itemCache = $entityCache->get($entityName, $item["id"], $langShort);
            if($itemCache === null){
                $options["with"] = ["all"];
                $itemCache = $this->utilize($item, $langShort, $options);
                $entityCache->set($entityName, $item["id"], $langShort, $itemCache);
            }

            $imagesEntity = "images/{$entityName}/{$item['id']}";
            $imagesResponse = $entityCache->get($imagesEntity, $options["itemThumbnail"], $langShort, null, false);
            if($imagesResponse === null){
                $imagesResponse = $this->mediaProcessor->getAllObjectImages($item["id"], $this->mediaInfoName, $langShort, $options["itemThumbnail"], true);
                $entityCache->set($imagesEntity, $options["itemThumbnail"], $langShort, $imagesResponse, null, false);
            }
            $itemCache["images"] = $imagesResponse;
        }

        return $itemCache;
    }
    private function utilize(Array $item, ?String $langShort = null, Array $options = []) {
        ($this->mediaProcessor === null) ? $this->mediaProcessor = $this->appController->returnMediaProcessor() : null;

        $langShort = ($langShort === null) ? $this->appController->getLang() : $langShort;
        $requestAllParameters = (array_key_exists('with', $options) && in_array('all', $options["with"]));
        $entityBonds = []; //TODO: $entityBonds - create minor bonds and delete entity by them

        // Default
        $itemOptimized = $item;
        $itemOptimized["path"] = '/'.$item["url"];

        if ($requestAllParameters || in_array("contentBlocks", $options["with"])) {
            if($this->appController->isActiveModule('contentBlocks')){
                if(isExisting('modelName', $item)){
                    ($this->contentBlocks === null) ? $this->contentBlocks = new ContentBlocks($this->database, $this->appController) : null;
                    $itemOptimized["contentBlocks"] = $this->contentBlocks->getObjectBlocks($item["id"], $item["modelName"]);
                }
            }
        }

        return $itemOptimized;
    }

// Get all URL addresses from templates
    public function getAllTemplateAnchors($langShort, $skipMissingTranslations = false, $returnMissing = false) {
        $sqlQuery = "SELECT id, templateName FROM `{$this->templatesTable}`;";
        $langColumns = "url, langShort";

        $templatesUrls = $this->database->getQuery($sqlQuery, null, true);
        $translatedUrls = $this->localizationClass->getTranslation($templatesUrls, $langColumns, $this->templatesLangTable, $langShort);

        $allAnchors = $missingAnchors = [];
        if ($translatedUrls) {
            foreach ($translatedUrls as $template) {
                if($skipMissingTranslations === false || $skipMissingTranslations === true && $template["translationExists"] === true){
                    $allAnchors[$template["templateName"]] = $template["url"];
                }else{
                    $missingAnchors[$template["templateName"]] = $template["url"];
                }
            }
        }

        return ($returnMissing === true) ? $missingAnchors : $allAnchors;
    }

/* ================== Functions for work with data === */

// Get all templates
    public function getAllTemplates($langShort) {
        $sqlQuery = "SELECT * FROM `{$this->templatesTable}`;";
        $langColumns = "title, shortDesc, description, metaKeywords, metaTitle, metaDescription, url, youtubeUrl, langShort";

        $returnData = $this->localizationClass->getTranslation($this->database->getQuery($sqlQuery, null, true), $langColumns, $this->templatesLangTable, $langShort);
        return $returnData;
    }
// Get all visible templates
    public function getAllVisibleTemplates($langShort) {
        $sqlQuery = "SELECT * FROM `{$this->templatesTable}` WHERE visibility = 1;";
        $langColumns = "title, shortDesc, description, metaKeywords, metaTitle, metaDescription, url, youtubeUrl, langShort";

        $returnData = $this->localizationClass->getTranslation($this->database->getQuery($sqlQuery, null, true), $langColumns, $this->templatesLangTable, $langShort);
        return $returnData;
    }
// Get one template by ID
    public function getTemplateInfoById($templateId, $langShort, $checkVisibility = false) {
        $sqlQuery = "SELECT * FROM `{$this->templatesTable}` WHERE id = :id;";
        $langColumns = "title, shortDesc, description, metaKeywords, metaTitle, metaDescription, url, youtubeUrl, langShort";

        $returnData = $this->localizationClass->getTranslation($this->database->getQuery($sqlQuery, ["id" => $templateId], false), $langColumns, $this->templatesLangTable, $langShort);
        $returnObject = ($checkVisibility !== true) ? $returnData : (($returnData["visibility"] == 1) ? $returnData : []);
        return $returnObject;
    }
// Get one template by TEMPLATE NAME
    public function getTemplateInfoByName($templateName, $langShort, $checkVisibility = false) {
        $sqlQuery = "SELECT * FROM `{$this->templatesTable}` WHERE templateName = :templateName;";
        $langColumns = "title, shortDesc, description, metaKeywords, metaTitle, metaDescription, url, youtubeUrl, langShort";

        $returnData = $this->localizationClass->getTranslation($this->database->getQuery($sqlQuery, ["templateName" => $templateName], false), $langColumns, $this->templatesLangTable, $langShort);
        $returnObject = ($checkVisibility !== true) ? $returnData : (($returnData["visibility"] == 1) ? $returnData : []);
        return $returnObject;
    }
// Get one template by URL
    public function getTemplateInfoByUrl($templateUrl, $langShort, $checkVisibility = false) {
        $sqlQuery = "SELECT * FROM `{$this->templatesTable}` WHERE url = :url;";
        $langColumns = "title, shortDesc, description, metaKeywords, metaTitle, metaDescription, url, youtubeUrl, langShort";

        $returnData = $this->localizationClass->getTranslation($this->database->getQuery($sqlQuery, ["url" => $templateUrl], false), $langColumns, $this->templatesLangTable, $langShort);
        $returnObject = ($checkVisibility !== true) ? $returnData : (($returnData["visibility"] == 1) ? $returnData : []);
        return $returnObject;
    }

}