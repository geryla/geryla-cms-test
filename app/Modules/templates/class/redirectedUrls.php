<?php
class redirectedUrls {

    public $moduleName = "templates";
    private $database;

    public $redirectsTable = null;
    public $dataRows = ["alias", "inputUrl", "outputUrl", "status", "langShort", "visibility"];

    public $redirectStatuses = [
        ["status" => '200', "name" => "HTTP/1.1 200 OK"],
        ["status" => '301', "name" => "HTTP/1.1 301 Moved Permanently"],
        ["status" => '302', "name" => "HTTP/1.1 302 Found"],
        ["status" => '303', "name" => "HTTP/1.1 303 See Other"],
        ["status" => '404', "name" => "HTTP/1.1 404 Not Found"],
    ];

    public function __construct(Database $database) {
        $this->database = $database;
        $this->redirectsTable = databaseTables::getTable($this->moduleName, "redirectsTable");
    }

    public function getAllRedirects() {
        $sqlQuery = "SELECT * FROM `{$this->redirectsTable}`;";
        $returnData = $this->database->getQuery($sqlQuery, null, true);

        return $returnData;
    }
    public function getById($objectId, $isVisible = true) {
        $visibility = ($isVisible === true) ? "AND visibility = 1" : null;
        $returnData = $this->database->getQuery("SELECT * FROM `{$this->redirectsTable}` WHERE id = :id {$visibility};", ["id" => $objectId], false);

        return $returnData;
    }
    public function getByUrl($inputUrl, $isVisible = true, $langShort = null) {
        $inputUrl_noSlash = rtrim($inputUrl, "/");
        $inputUrl_addSlash = $inputUrl.'/';

        $bindSQL = ["inputUrl" => $inputUrl, "inputUrl_noSlash" => $inputUrl_noSlash, "inputUrl_addSlash" => $inputUrl_addSlash];
        $whereSQL = "WHERE (inputUrl = :inputUrl OR inputUrl = :inputUrl_noSlash OR inputUrl = :inputUrl_addSlash)";
        if($langShort !== null){
            $bindSQL["langShort"] = $langShort;
            $whereSQL .= " AND langShort = :langShort";
        }

        $visibility = ($isVisible === true) ? "AND visibility = 1" : null;
        $sqlQuery = "SELECT * FROM `{$this->redirectsTable}` {$whereSQL} {$visibility};";

        $returnData = $this->database->getQuery($sqlQuery, $bindSQL, false);
        return $returnData;
    }
    public function getStatusesAsSelect() {
        $returnArray = [];
        foreach($this->redirectStatuses as $statusName){
            $returnArray[] = ["value" => $statusName["status"], "title" => $statusName["name"]];
        }

        return $returnArray;
    }

}