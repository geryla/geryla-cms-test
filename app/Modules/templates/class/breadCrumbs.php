<?php
class breadCrumbs {

    private $breadCrumbs = [];

    private $lastTitle = null;
    private $lastUrl = null;
    private $lastPrefix = null;
    private $lastId = null;
    private $lastTemplate = null;


    public function add($pageData, $template = null) {
        if (empty($pageData["title"]) || empty($pageData["id"])) {
            return false;
        }

        $this->lastTitle = $pageData["title"];
        $this->lastUrl = (isset($pageData["url"])) ? $pageData["url"] : null;
        $this->lastPrefix = (isset($pageData["urlPrefix"])) ? $pageData["urlPrefix"] : null;
        $this->lastId = $pageData["id"];
        $this->lastTemplate = (isset($template)) ? $template : null;

        $urlCompleted = (isset($pageData["path"])) ? $pageData["path"] : (($this->lastPrefix !== null && $this->lastPrefix != "0") ? $this->lastPrefix . "-" . $this->lastUrl : $this->lastUrl);
        $urlCompleted = (substr($urlCompleted, 0, 1) !== '/') ? "/" . $urlCompleted : $urlCompleted; // FIX: Add slash before empty URL (for breadcrumbs)
        if(isset($pageData["parentTemplateUrl"])){
            $urlCompleted = $pageData["parentTemplateUrl"].$urlCompleted;
        }

        $this->breadCrumbs[] = [
            "id" => $this->lastId,
            "title" => $this->lastTitle,
            "urlPrefix" => $this->lastPrefix,
            "url" => $this->lastUrl,
            "urlComplete" => $urlCompleted,
            "template" => $this->lastTemplate
        ];

        return true;
    }
    public function get() {
        return $this->breadCrumbs;
    }
    public function getReverse() {
        return array_reverse($this->get());
    }
    public function removeLastBread() {
        $count = count($this->breadCrumbs);
        unset($this->breadCrumbs[$count - 1]);
    }
    public function getLast() {
        $count = count($this->breadCrumbs);
        return $this->breadCrumbs[$count - 1];
    }
    public function removeAll() {
        $this->breadCrumbs = [];
    }

}