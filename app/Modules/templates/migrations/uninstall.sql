SET FOREIGN_KEY_CHECKS=0;

-- DELETE TABLES
DROP TABLE `prefixs`;
DROP TABLE `templates`;
DROP TABLE `templates-lang`;

-- DELETE ALL MEDIA by mediaSettings from module CONFIG
DELETE media, lang FROM `mediamanager` AS media INNER JOIN `mediamanager-lang` AS lang WHERE media.template = 'template';

SET FOREIGN_KEY_CHECKS=1;