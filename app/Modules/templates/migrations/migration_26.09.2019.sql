-- CREATE TABLE for PREFIXES
CREATE TABLE IF NOT EXISTS `prefixs` (
  `urlPrefix` int(11) NOT NULL,
  `contentTable` varchar(255) NOT NULL,
  PRIMARY KEY (`urlPrefix`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- CREATE TABLE for MODULES
CREATE TABLE IF NOT EXISTS `templates` (
  `id` int(10) NOT NULL AUTO_INCREMENT,

  `urlPrefix` int(11) NOT NULL,
  `allowIndex` int(1) NOT NULL DEFAULT '1',
  `pageType` int(1) NOT NULL DEFAULT '1',
  `templateName` varchar(40) NOT NULL,
  `moduleName` varchar(255) NOT NULL,
  `className` varchar(55) DEFAULT NULL,
  `loggedOnly` int(9) DEFAULT '0',
  `pageSidebar` varchar(40) NOT NULL,
  `pageTopMenu` int(1) NOT NULL DEFAULT '1',

  `createdDate` datetime NOT NULL,
  `updatedDate` datetime NOT NULL,
  `visibility` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `urlPrefix` (`urlPrefix`),
  FOREIGN KEY (`urlPrefix`) REFERENCES `prefixs` (`urlPrefix`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- CREATE TABLE for MODULES
CREATE TABLE IF NOT EXISTS `templates-lang` (
  `id` int(10) NOT NULL AUTO_INCREMENT,

  `associatedId` int(10) NOT NULL,
  `title` varchar(75) NOT NULL,
  `shortDesc` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `metaKeywords` varchar(255) NOT NULL,
  `metaTitle` varchar(255) NOT NULL,
  `metaDescription` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `youtubeUrl` varchar(255) NOT NULL,

  `langShort` varchar(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `associatedId` (`associatedId`),
  FOREIGN KEY (`associatedId`) REFERENCES `templates` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- TRUNCATE DATA AND INSERT DEFAULT VALUES - Languages
DELETE FROM `prefixs`;
ALTER TABLE `prefixs` DISABLE KEYS;
INSERT INTO `prefixs` (`urlPrefix`, `contentTable`) VALUES (0, 'templates');
ALTER TABLE `prefixs` ENABLE KEYS;