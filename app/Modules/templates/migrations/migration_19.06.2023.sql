-- ADD Language support (in case of similar URLs for different languages)
ALTER TABLE `redirects` ADD `langShort` VARCHAR(5) NULL AFTER `status`;