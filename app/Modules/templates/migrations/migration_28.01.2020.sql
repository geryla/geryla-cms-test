-- CREATE TABLE for REDIRECTED URLS
CREATE TABLE IF NOT EXISTS `redirects` (
  `id` int(10) NOT NULL AUTO_INCREMENT,

  `alias` varchar(255) NOT NULL,
  `inputUrl` TEXT NOT NULL,
  `outputUrl` TEXT NOT NULL,
  `status` varchar(5) NOT NULL,

  `visibility` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;