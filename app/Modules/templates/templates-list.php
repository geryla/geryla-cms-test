<?php
/* =============================== CONTROLLER METHODS ======== */
$templatesClass = new templateClass($database, $appController);

$templatesTable = $templatesClass->templatesTable;
$templatesTranslateTable = $templatesClass->templatesLangTable;
$nextPage = $dataCompiler->modulePageUrl(2);

$pageData = $templatesClass->getAllTemplates($presenter->templateData->langShort);

/* =============================== CONTENT ======== */ 
echo '<article class="module">';
  
  $render->header(translate("mod-templates-admin-list"), null);
                                            
  echo '<section>';                                      
     
      echo '<div class="moduleWrap templateList">';
        echo '<table class="striped">';   
        
    	  	$tableHead = [
            translate("mod-templates-admin-list-title") ,
            translate("mod-templates-admin-list-pageType") ,
            translate("mod-templates-admin-list-visibility") ,
            ''
          ];
            $render->tableHead($tableHead);
                        
            foreach($pageData as $template){
                $langTitle = ($presenter->activeContent->enabledMultilanguage > 1) ? "<i class='activeLang'>(".$template["activeLang"].")</i> ".$template["title"] : $template["title"];
                $langTranslate = ($presenter->activeContent->enabledMultilanguage > 1 && $template["activeTranslation"] === true) ? true : false;
                echo '<tr>';
                    $render->tableColumn($langTitle." <i>(".$template["templateName"].")</i>");
                    $render->tableColumn(translate("mod-templates-admin-list-pageType-".$template["pageType"]));
                    $render->tableColumn($render->visibilitySwitch($template["id"], "templates", $template["id"], $template["visibility"]), 80, true);

                    $previewLink = ($template["pageType"] == 0 OR $template["pageType"] == 1 OR $template["pageType"] == 3) ? $render->getPreviewPath($template["templateName"], $template["url"], null, true) : null;
                    $render->tableActionColumn($nextPage.'?id='.$template["id"], null, $previewLink, 100, $langTranslate);
                echo '</tr>';
            }
           
        echo '</table>';            
      echo '</div>'; 
     			 
  echo '</section>';    
   
echo '</article>';
  