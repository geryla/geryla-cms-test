<?php
$lang = [];

$lang["templates"] = 'Šablony';

$lang["templates-list"] = 'Šablony obsahu';
  $lang["template-detail"] = 'Detail šablony'; 
$lang["redirected-urls"] = 'Přesměrování odkazů';


$lang["mod-templates-admin-list"] = "Seznam šablon";
$lang["mod-templates-admin-edit"] = "Upravit šablonu";
$lang["mod-templates-admin-redirects-list"] = "Seznam odkazů";
$lang["mod-templates-admin-redirects-add"] = "Přidat odkaz";
$lang["mod-templates-admin-redirects-edit"] = "Upravit odkaz";

$lang["mod-templates-admin-list-title"] = 'Název';
$lang["mod-templates-admin-list-visibility"] = 'Viditelnost';

$lang["mod-templates-admin-detail-title"] = 'Titulek';
$lang["mod-templates-admin-detail-shortDesc"] = 'Krátký popis';
$lang["mod-templates-admin-detail-desc"] = 'Popis'; 
$lang["mod-templates-admin-detail-seoTitle"] = 'SEO titulek';
$lang["mod-templates-admin-detail-seoUrl"] = 'SEO url adresa';
$lang["mod-templates-admin-detail-seoDesc"] = 'SEO popis';
$lang["mod-templates-admin-detail-keywords"] = 'Klíčová slova';
$lang["mod-templates-admin-detail-mainImage"] = 'Hlavní obrázek';
$lang["mod-templates-admin-detail-youtubeUrl"] = 'Video z YouTube';
$lang["mod-templates-admin-detail-allowIndex"] = 'Povolit indexaci';
$lang["mod-templates-admin-detail-allowIndex-yes"] = 'Ano';
$lang["mod-templates-admin-detail-allowIndex-no"] = 'Ne';

$lang["mod-templates-admin-detail-className"] = 'CSS třída';

$lang["mod-templates-admin-detail-youtubeUrl-help"] = 'Vložte pouze část za ?v=';
$lang["mod-templates-admin-detail-youtubeUrl-plc"] = 'např: KD5fLb-WgBU';

$lang["mod-templates-admin-detail-seoTitle-help"] = 'Vygenerovat z názvu titulku';
$lang["mod-templates-admin-detail-keywords-plc"] = 'Klíčová slova oddělujte čárkou';
  
$lang["mod-templates-admin-tabs-basic"] = 'Informace';
$lang["mod-templates-admin-tabs-customize"] = 'Přizpůsobení';
$lang["mod-templates-admin-tabs-seo"] = 'SEO';

$lang["mod-templates-admin-list-pageType"] = 'SEO informace';
$lang["mod-templates-admin-list-pageType-0"] = 'dle nastavení';
$lang["mod-templates-admin-list-pageType-1"] = 'dle nastavení';
$lang["mod-templates-admin-list-pageType-2"] = 'dědičné';
$lang["mod-templates-admin-list-pageType-3"] = 'dle nastavení/dědičné';

$lang["mod-templates-admin-redirects-inputUrl"] = 'Zdrojová adresa';
$lang["mod-templates-admin-redirects-outputUrl"] = 'Cílový odkaz';
$lang["mod-templates-admin-redirects-status"] = 'Stavový kód';
$lang["mod-templates-admin-redirects-alias"] = 'Název';
$lang["mod-templates-admin-redirects-inputUrl-help"] = '<i>/cast-adresy-za-prvnim-lomitkem</i>';
$lang["mod-templates-admin-redirects-alias-help"] = '<i>Pouze pro administraci</i>';
$lang["mod-templates-admin-redirects-status-help"] = '<i>Stavový kód</i>';
$lang["mod-templates-admin-redirects-langShort"] = 'Jazyková mutace';

$lang["mod-templates-admin-filtration-sort-status"] = 'Třídit dle stavu';