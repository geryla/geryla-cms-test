<?php
$lang = [];

$lang["templates"] = 'Templates';

$lang["templates-list"] = 'Content Templates';
$lang["template-detail"] = 'Template detail';
$lang["redirected-urls"] = 'Redirecting links';


$lang["mod-templates-admin-list"] = "List of templates";
$lang["mod-templates-admin-edit"] = "Edit Template";
$lang["mod-templates-admin-redirects-list"] = "List of Redirects";
$lang["mod-templates-admin-redirects-add"] = "Add Redirect";
$lang["mod-templates-admin-redirects-edit"] = "Edit Redirect";

$lang["mod-templates-admin-list-title"] = 'Title';
$lang["mod-templates-admin-list-visibility"] = 'Visibility';

$lang["mod-templates-admin-detail-title"] = 'Title';
$lang["mod-templates-admin-detail-shortDesc"] = 'Short Description';
$lang["mod-templates-admin-detail-desc"] = 'Description';
$lang["mod-templates-admin-detail-seoTitle"] = 'SEO Title';
$lang["mod-templates-admin-detail-seoUrl"] = 'SEO url address';
$lang["mod-templates-admin-detail-seoDesc"] = 'SEO Description';
$lang["mod-templates-admin-detail-keywords"] = 'Keywords';
$lang["mod-templates-admin-detail-mainImage"] = 'Main Image';
$lang["mod-templates-admin-detail-youtubeUrl"] = 'Video from YouTube';
$lang["mod-templates-admin-detail-allowIndex"] = 'Allow indexing';
$lang["mod-templates-admin-detail-allowIndex-yes"] = 'Yes';
$lang["mod-templates-admin-detail-allowIndex-no"] = 'No';

$lang["mod-templates-admin-detail-className"] = 'CSS Class';

$lang["mod-templates-admin-detail-youtubeUrl-help"] = 'Insert only the part after ?v=';
$lang["mod-templates-admin-detail-youtubeUrl-plc"] = 'eg: KD5fLb-WgBU';

$lang["mod-templates-admin-detail-seoTitle-help"] = 'Generate from title title';
$lang["mod-templates-admin-detail-keywords-plc"] = 'Separate keywords with a comma';

$lang["mod-templates-admin-tabs-basic"] = 'Information';
$lang["mod-templates-admin-tabs-customize"] = 'Customization';
$lang["mod-templates-admin-tabs-seo"] = 'SEO';

$lang["mod-templates-admin-list-pageType"] = 'SEO Information';
$lang["mod-templates-admin-list-pageType-0"] = 'according to settings';
$lang["mod-templates-admin-list-pageType-1"] = 'according to settings';
$lang["mod-templates-admin-list-pageType-2"] = 'inherited';
$lang["mod-templates-admin-list-pageType-3"] = 'by setting/inherit';

$lang["mod-templates-admin-redirects-inputUrl"] = 'Source URL';
$lang["mod-templates-admin-redirects-outputUrl"] = 'Destination Link';
$lang["mod-templates-admin-redirects-status"] = 'Status Code';
$lang["mod-templates-admin-redirects-alias"] = 'Name';
$lang["mod-templates-admin-redirects-inputUrl-help"] = '<i>/cast-addresses-after-the-first-slash</i>';
$lang["mod-templates-admin-redirects-alias-help"] = '<i>For administration only</i>';
$lang["mod-templates-admin-redirects-status-help"] = '<i>Status Code</i>';
$lang["mod-templates-admin-redirects-langShort"] = 'Language Mutation';

$lang["mod-templates-admin-filtration-sort-status"] = 'Sort by status';