<?php
  $render->contentDiv(true, "seo", "boxContent");  
    $selectValues = [ ["value" => "1", "title" => translate("mod-templates-admin-detail-allowIndex-yes")], ["value" => "0", "title" => translate("mod-templates-admin-detail-allowIndex-no")] ];
    $render->contentRow(null, "rowBox", '<h3>'.translate("mod-templates-admin-detail-allowIndex").'</h3>', null, $render->select('allowIndex', $selectValues, "value", "title", $pageData["allowIndex"], false) );
    $render->contentRow(null, "rowBox", '<h3>'.translate("mod-templates-admin-detail-seoTitle").'</h3>', null, $render->input("text", "metaTitle", $pageData["metaTitle"], null, false, null, null) );
    $render->contentRow(null, "rowBox", '<h3>'.translate("mod-templates-admin-detail-seoUrl").'</h3>', '<p class="help"><span id="generateUrl">'.translate("mod-templates-admin-detail-seoTitle-help").'</span></p>', $render->input("text", "url", $pageData["url"], null, false, "prettyUrl", null) );
    $render->contentRow(null, "rowBox", '<h3>'.translate("mod-templates-admin-detail-seoDesc").'</h3>', null, $render->textarea("metaDescription", $pageData["metaDescription"], 5, null, null, null, null) );
    $render->contentRow(null, "rowBox", '<h3>'.translate("mod-templates-admin-detail-keywords").'</h3>', null, $render->textarea("metaKeywords", $pageData["metaKeywords"], 5, translate("mod-templates-admin-detail-keywords-plc"), null, null, null) );
  $render->contentDiv(false);