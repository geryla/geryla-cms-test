<?php
  $render->contentDiv(true, "basic", "boxContent");  
    $render->contentRow(null, "rowBox", '<h3>'.translate("mod-templates-admin-detail-title").'</h3>', null, $render->input("text", "title", $pageData["title"], null, true, "prettyTitle", null) );
    $render->contentRow(null, "rowBox", '<h3>'.translate("mod-templates-admin-detail-shortDesc").'</h3>', null, $render->textarea("shortDesc", $pageData["shortDesc"], 7, null, null, null, null) );
    $render->contentRow(null, "rowBox", '<h3>'.translate("mod-templates-admin-detail-desc").'</h3>', null, $render->textarea("description", $pageData["description"], 7, null, null, null, "textarea") );
  $render->contentDiv(false);  
?>