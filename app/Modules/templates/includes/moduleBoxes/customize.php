<?php
  $render->contentDiv(true, "customize", "boxContent");  
    $render->contentRow(null, "rowBox", '<h3>'.translate("mod-templates-admin-detail-youtubeUrl").'</h3>', '<p class="help">'.translate("mod-templates-admin-detail-youtubeUrl-help").'</p>', $render->input("text", "youtubeUrl", $pageData["youtubeUrl"], translate("mod-templates-admin-detail-youtubeUrl-plc"), false, null, null) );
    $render->contentRow(null, "rowBox", '<h3>'.translate("mod-templates-admin-detail-className").'</h3>', null, $render->input("text", "className", $pageData["className"], null, false, null, null) );
  $render->contentDiv(false);  
?>