<?php

$render->contentDiv(true, null, "editPart");
    $render->contentDiv(true, null, "inlineDiv full");
        echo '<h3>&nbsp;</h3>';
        $render->contentDiv(true, null, "rows");
            $render->contentRow(null, "rowBox inline three first", '<h3>'.translate("mod-templates-admin-redirects-alias").'</h3>', translate("mod-templates-admin-redirects-alias-help"), $render->input("text", "alias", $oneInfo["alias"], null, true, null, null) );
            $render->contentRow(null, "rowBox inline three", '<h3>'.translate("mod-templates-admin-redirects-status").'</h3>', translate("mod-templates-admin-redirects-status-help"), $render->select("status", $redirectedUrls->redirectStatuses, "status", "name", $oneInfo["status"], true, null, null) );
            $render->contentRow(null, "rowBox inline three last", '<h3>'.translate("mod-templates-admin-redirects-langShort").'</h3>', null, $render->select("langShort", $allEnabledLanguages, "langShort", "title", $oneInfo["langShort"], true) );
            $render->contentRow(null, "rowBox inline two first", '<h3>'.translate("mod-templates-admin-redirects-inputUrl").'</h3>', translate("mod-templates-admin-redirects-inputUrl-help"), $render->input("text", "inputUrl", $oneInfo["inputUrl"], null, true, null, null) );
            $render->contentRow(null, "rowBox inline two last", '<h3>'.translate("mod-templates-admin-redirects-outputUrl").'</h3>', null, $render->input("text", "outputUrl", $oneInfo["outputUrl"], null, true, null, null) );
        $render->contentDiv(false);
    $render->contentDiv(false);
$render->contentDiv(false);