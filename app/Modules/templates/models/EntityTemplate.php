<?php
class EntityTemplate extends Entity{

    protected $extendedParams = [
        "title" => [
            "type" => "String",
        ],
        "path" => [
            "type" => "String",
        ],
        "shortDesc" => [
            "type" => "String",
        ],
        "description" => [
            "type" => "String",
        ],
        "pageType" => [
            "type" => "Integer",
        ],
        "loggedOnly" => [
            "type" => "Integer",
        ],
        "images" => [
            "type" => "Array",
        ],
    ];

    public function __construct(Array $objectData) {
        $this->extendEntity($objectData);
        $this->build($objectData, $this->extendedParams);
    }
    private function extendEntity(Array $objectData) {
        if(isExisting('contentBlocks', $objectData)){
            $entityBlock = new EntityBlock($objectData["contentBlocks"]);
            $this->extendedParams["contentBlocks"] = ["type" => "Array", "body" => $entityBlock->getParams(true)];
        }
    }

}