<?php
class EntityRoute extends Entity{

    protected $extendedParams = [
        "id" => [
            "type" => "Integer",
        ],
        "title" => [
            "type" => "String",
        ],
        "shortDesc" => [
            "type" => "String",
        ],
        "description" => [
            "type" => "String",
        ],
        "langShort" => [
            "type" => "String",
        ],
        "path" => [
            "type" => "String",
        ],
        "visibility" => [
            "type" => "Boolean",
        ],

        "metaKeywords" => [
            "type" => "String",
        ],
        "metaTitle" => [
            "type" => "String",
        ],
        "metaDescription" => [
            "type" => "String",
        ],
        "allowIndex" => [
            "type" => "Integer",
        ],
    ];

    public function __construct(Array $objectData) {
        $this->build($objectData, $this->extendedParams, true);
    }

}