<?php
namespace Api\localization;

class ApiFactory extends \ApiController{
    private $requestMethods;

    private $errorCodes = [
        400 => 'Object/s not found',
        401 => 'Missing filtration parameter LabelTag or CategoryId',
    ];

    public function __construct(\Database $database, \AppController $appController, Array $requestData) {
        parent::__construct($database, $appController, $requestData);

        $this->requestMethods = include(__DIR__.'/requests.php');
    }

    public function getErrorCodes(){
        return $this->returnErrorCodes($this->errorCodes);
    }
    public function validate(){
        return $this->validateRequest($this->requestData, $this->requestMethods);
    }
    public function proceedRequest() {

        switch($this->requestData["action"]){
            case "getLocalization":
                $this->getLocalization();
                break;
            default:
                $this->setState(310);
                return false;
        }

        $this->closeAuthRequest();
        return true;
    }

    private function getLocalization(){
        $localeEngine = $this->appController->returnLocaleEngine();
        $localeEngine->setLanguage('appLang', $this->requestData["locale"]);
        $langFile = $localeEngine->returnCustomLangFile('appLang');

        $this->setResponseData($langFile);
        return true;
    }
}