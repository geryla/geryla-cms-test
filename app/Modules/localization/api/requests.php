<?php

return [
    "getLocalization" => [
        "path" => "/localization/getLocalization",
        "method" => "GET",
        "authorized" => false,
        "cache" => false
    ],
];