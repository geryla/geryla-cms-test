<?php
class LanguageClass {
    public $database;

    protected $moduleName = "localization";
    protected $languagesTable;

    public function __construct(Database $database) {
        $this->database = $database;
        $this->languagesTable = databaseTables::getTable($this->moduleName, "languageTable");
    }

// Select all languages in database
    public function getAllLanguages() {
        $sql = "SELECT * FROM `{$this->languagesTable}` ORDER BY orderNum;";
        $returnData = $this->database->getQuery($sql, null, true);
        return $returnData;
    }
// Select all active languages in database
    public function getAllActiveLanguages() {
        $sql = "SELECT * FROM `{$this->languagesTable}` WHERE visibility = 1 ORDER BY orderNum;";
        $returnData = $this->database->getQuery($sql, null, true);
        return $returnData;
    }
// Check if language exist and is active
    public function visibleLanguage($langShort) {
        $sql = "SELECT visibility FROM `{$this->languagesTable}` WHERE langShort = :langShort;";
        $returnData = $this->database->getQuery($sql, array("langShort" => $langShort), false);
        return $returnData;
    }
// Add new language into database
    public function addLanguage($langValues) {
        $sql = "INSERT INTO `{$this->languagesTable}`";
        $returnData = $this->database->insertQuery($sql, $langValues);
        return $returnData;
    }
// Edit language in database  
    public function editLanguage($langValues, $whereValues = null) {
        $sql = "UPDATE `{$this->languagesTable}`";
        $whereSql = "WHERE id = :id";
        $returnData = $this->database->updateQuery($sql, $langValues, $whereValues, $whereSql);
        return $returnData;
    }
// Delete language from database  
    public function deleteLanguage($whereValues) {
        $sql = "DELETE FROM `{$this->languagesTable}`";
        $whereSql = "WHERE id = :id";
        $returnData = $this->database->deleteQuery($sql, $whereValues, $whereSql);
        return $returnData;
    }
// Get one language by currency
    public function getOneLangByCurrency($currency, $strict = true) {
        $strictLimit = ($strict === true) ? "LIMIT 1" : null;
        $returnData = $this->database->getQuery("SELECT * FROM `{$this->languagesTable}` WHERE currency = :currency {$strictLimit};", ["currency" => $currency], false);
        return $returnData;
    }
// Get one language by langShort
    public function getOneLangByLangShort($langShort) {
        $sql = "SELECT * FROM `{$this->languagesTable}` WHERE langShort = :langShort;";
        $returnData = $this->database->getQuery($sql, array("langShort" => $langShort), false);
        return $returnData;
    }
// Get main language
    public function getMainLanguage() {
        $sql = "SELECT * FROM `{$this->languagesTable}` WHERE main = 1;";
        $returnData = $this->database->getQuery($sql, null, false);
        return $returnData;
    }

}   