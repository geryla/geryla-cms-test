<?php

class currencyBuilder {

    private $database;
    private $appController;
    private $modulesController;
    public $languagesTable;

    private $loadedCurrency = [];
    private $activeCurrency = null;
    private $loggedUser = [];

    private $customerPriceLevelId = null;
    private $priceLevelPercentage = 100;
    private $priceLevelChanged = false;
    private $defaultRoundingValue = 3;

    private $dailyRatesCNB = false;

    public function __construct(Database $database, AppController $appController) {
        $this->database = $database;
        $this->appController = $appController;
        $this->modulesController = $appController->returnModulesController();
        $this->languagesTable = databaseTables::getTable("localization", "languageTable");

        $this->init();
    }

    public function init(){
        $this->checkUser();
        $this->checkUserPriceLevel();

        $langShort = $this->appController->getLang();
        $langShort = $this->extendOptysEsoSubject($langShort);

        $this->activeCurrency = $this->loadCurrency($langShort);
        return $langShort;
    }

// LOAD CURRENCY | Get info and store
    private function loadCurrency(String $langShort) {
        if (isset($this->loadedCurrency[$langShort])) {
            $activeLanguageInfo = $this->loadedCurrency[$langShort];
        } else {
            $sql = "SELECT *  FROM `{$this->languagesTable}` WHERE langShort = :langShort AND visibility = 1;";
            $activeLanguageInfo = $this->database->getQuery($sql, ["langShort" => $langShort], false);
            $this->loadedCurrency[$langShort] = $activeLanguageInfo;
        }

        return $activeLanguageInfo;
    }

// BUILD | Complete price info
    public function buildPrice($langShort, $taxValue, $basePrice, $discountPrice, $extraData = []) {
        if (!empty($langShort)) {
            $activeLanguageInfo = $this->loadCurrency($langShort);

            if (!empty($activeLanguageInfo)) {
                // DEFAULT CORRECTION - Round all values to keep same decimal range
                $basePrice = round($basePrice, $this->defaultRoundingValue);
                $discountPrice = round($discountPrice, $this->defaultRoundingValue);

                // MODULE: PRICE LEVEL -> Change prices
                if (($this->priceLevelChanged === true && $this->priceLevelPercentage !== 100) && !isset($extraData["skipRaw"])) {
                    // Store DEFAULT prices if is price level change enabled
                    $extraData["skipRaw"] = true;
                    $priceBuilder["rawPriceView"] = $this->getViewPrices($this->buildPrice($langShort, $taxValue, $basePrice, $discountPrice, $extraData), false);

                    // Load price levels of manufacturers
                    if (isset($extraData["productManufacturer"])) {
                        $this->checkManufacturerPriceLevel($extraData["productManufacturer"]);
                    }

                    // RE-CALCULATE price levels and CORRECT prices again
                    if (!isset($extraData["noRecalculate"])) {
                        $basePrice = round($this->calculateWithPercentage($basePrice), $this->defaultRoundingValue);
                        $discountPrice = round($this->calculateWithPercentage($discountPrice), $this->defaultRoundingValue);
                    }
                }

                // MULTI-LANGUAGE | Price conversion - check if enabled (or forced)
                $allowPriceConversion = ($activeLanguageInfo["useRate"] == 1) ? true : false;
                if (isset($extraData["forceConversion"])) {
                    $allowPriceConversion = true;
                }

                // SET Default prices type
                $defaultNoTax = $this->checkPricesDefaultNoTax($langShort, $activeLanguageInfo);

                // GET price object
                return $this->buildPriceObject($activeLanguageInfo, $basePrice, $taxValue, $discountPrice, $allowPriceConversion, $defaultNoTax);
            }
        }

        return false;
    }

// BUILD | Rounded price
    public function buildRoundedPrice($langShort, $priceValue, $customRounding = false) {
        if (!empty($langShort)) {
            $activeLanguageInfo = $this->loadCurrency($langShort);

            if (!empty($activeLanguageInfo)) {
                // Round and style prices -> If enabled rounding, round to custom, else disable rounding
                $roundDecimals = ($activeLanguageInfo["round"] == 1) ? $activeLanguageInfo["decimals"] : $this->defaultRoundingValue;
                $roundDecimals = ($customRounding !== false) ? $customRounding : $roundDecimals;
                return round($priceValue, $roundDecimals);
            }
        }

        return $priceValue;
    }

// BUILD | Price string
    public function buildPriceString(String $langShort, Float $priceValue, Bool $customRounding = false) {
        if (!empty($langShort)) {
            $activeLanguageInfo = $this->loadCurrency($langShort);

            if (!empty($priceValue)) {
                // Round and style prices -> If enabled rounding, round to custom, else disable rounding
                $roundDecimals = ($activeLanguageInfo["round"] == 1) ? $activeLanguageInfo["decimals"] : $this->defaultRoundingValue;
                $roundDecimals = ($customRounding !== false) ? $customRounding : $roundDecimals;
                $priceValue = number_format($priceValue, $roundDecimals, ',', ' ');
            }

            // Currency symbols -> Push symbol of currency to price
            $priceValue = ($activeLanguageInfo["symbolPosition"] == "right") ? $priceValue . " " . $activeLanguageInfo["symbol"] : $activeLanguageInfo["symbol"] . " " . $priceValue;
        }

        return $priceValue;
    }

// BUILD | Price string without symbol
    public function buildPriceStringWithoutSymbol($langShort, $priceValue, $customRounding = false) {
        if (!empty($langShort)) {
            $activeLanguageInfo = $this->loadCurrency($langShort);

            if (!empty($priceValue)) {
                // Round and style prices -> If enabled rounding, round to custom, else disable rounding
                $roundDecimals = ($activeLanguageInfo["round"] == 1) ? $activeLanguageInfo["decimals"] : $this->defaultRoundingValue;
                $roundDecimals = ($customRounding !== false) ? $customRounding : $roundDecimals;
                $priceValue = number_format($priceValue, $roundDecimals, ',', ' ');
            }
        }

        return $priceValue;
    }

// BUILD | Price string without rounding
    public function buildPriceStringWithoutRounding($langShort, $priceValue, $showDefaultDecimalFormat = false) {
        if (!empty($langShort)) {
            $activeLanguageInfo = $this->loadCurrency($langShort);
            if ($showDefaultDecimalFormat === true) {
                $priceValue = number_format($priceValue, $this->defaultRoundingValue, ',', ' ');
            }
            $priceValue = ($activeLanguageInfo["symbolPosition"] == "right") ? $priceValue . " " . $activeLanguageInfo["symbol"] : $activeLanguageInfo["symbol"] . " " . $priceValue;
        }

        return $priceValue;
    }

// BUILD | Generate price details
    private function buildPriceObject($activeLanguageInfo, $basePrice, $taxValue, $discountPrice, $exchangePrice, $defaultNoTax) {
        // Check language settings and prepare count variables
        $taxNum = ($activeLanguageInfo["useTax"] == 1) ? 1 + ($taxValue / 100) : 1;
        $roundDecimals = ($activeLanguageInfo["round"] == 1) ? $activeLanguageInfo["decimals"] : $this->defaultRoundingValue;

        // Exchange rate -> Convert price if rate exist and correct decimals
        if ($exchangePrice === true) {
            $basePrice = round($this->convertPrice($basePrice, $activeLanguageInfo["exchangeRate"]), $this->defaultRoundingValue);
            $discountPrice = round($this->convertPrice($discountPrice, $activeLanguageInfo["exchangeRate"]), $this->defaultRoundingValue);
        }

        // Build and store prices WITH and WITHOUT TAX (if tax is disabled WO TAX and WITH TAX prices are same)
        if ($defaultNoTax === true) {
            $priceBuilder["basePrice"] = $basePrice;
            $priceBuilder["discountPrice"] = $discountPrice;
            $priceBuilder["basePrice_tax"] = $basePrice * $taxNum;
            $priceBuilder["discountPrice_tax"] = $discountPrice * $taxNum;
        } else {
            $priceBuilder["basePrice"] = $basePrice / $taxNum;
            $priceBuilder["discountPrice"] = $discountPrice / $taxNum;
            $priceBuilder["basePrice_tax"] = $basePrice;
            $priceBuilder["discountPrice_tax"] = $discountPrice;
        }

        // Store TAX value
        $priceBuilder["tax"] = ($activeLanguageInfo["useTax"] == 1) ? (float) $taxValue : 0;

        // DEFAULT prices - WITH/OUT TAX - BEFORE/AFTER DISCOUNT
        $priceBuilder["original_price_value"] = $priceBuilder["basePrice"]; // Basic price - no Tax
        $priceBuilder["original_price_value_tax"] = $priceBuilder["basePrice_tax"]; // Basic price - with Tax
        $priceBuilder["price_value"] = ($priceBuilder["discountPrice"] > 0) ? $priceBuilder["discountPrice"] : $priceBuilder["basePrice"]; // Discount or Basic price - no Tax
        $priceBuilder["price_value_tax"] = ($priceBuilder["discountPrice_tax"] > 0) ? $priceBuilder["discountPrice_tax"] : $priceBuilder["basePrice_tax"]; // Discount or Basic price - with Tax

        // CALCULATION prices - Used for system counters (shopping cart, orders, discounts, invoices...)
        $priceBuilder["calculatePrice"] = round($priceBuilder["price_value"], (($priceBuilder["price_value"] < 0) ? $this->defaultRoundingValue : $roundDecimals));
        $priceBuilder["calculatePrice_tax"] = round($priceBuilder["price_value_tax"], $roundDecimals);

        // VIEW prices (1/2) -> Format price
        $priceBuilder["original_price"] = number_format($priceBuilder["original_price_value"], $roundDecimals, ',', ' ');
        $priceBuilder["original_price_tax"] = number_format($priceBuilder["original_price_value_tax"], $roundDecimals, ',', ' ');
        $priceBuilder["price"] = number_format($priceBuilder["price_value"], $roundDecimals, ',', ' ');
        $priceBuilder["price_tax"] = number_format($priceBuilder["price_value_tax"], $roundDecimals, ',', ' ');

        // VIEW prices (2/2) -> Currency symbol
        $priceBuilder["original_price"] = ($activeLanguageInfo["symbolPosition"] == "right") ? $priceBuilder["original_price"] . " " . $activeLanguageInfo["symbol"] : $activeLanguageInfo["symbol"] . " " . $priceBuilder["original_price"];
        $priceBuilder["original_price_tax"] = ($activeLanguageInfo["symbolPosition"] == "right") ? $priceBuilder["original_price_tax"] . " " . $activeLanguageInfo["symbol"] : $activeLanguageInfo["symbol"] . " " . $priceBuilder["original_price_tax"];
        $priceBuilder["price"] = ($activeLanguageInfo["symbolPosition"] == "right") ? $priceBuilder["price"] . " " . $activeLanguageInfo["symbol"] : $activeLanguageInfo["symbol"] . " " . $priceBuilder["price"];
        $priceBuilder["price_tax"] = ($activeLanguageInfo["symbolPosition"] == "right") ? $priceBuilder["price_tax"] . " " . $activeLanguageInfo["symbol"] : $activeLanguageInfo["symbol"] . " " . $priceBuilder["price_tax"];

        // DETAILS for currency and prices
        $priceBuilder["currency"] = $activeLanguageInfo["currency"];
        $priceBuilder["symbol"] = $activeLanguageInfo["symbol"];
        $priceBuilder["discount"] = (($priceBuilder["discountPrice"] > 0 && $priceBuilder["discountPrice"] != $priceBuilder["basePrice"])) ? true : false;
        $priceBuilder["freePrice"] = ($priceBuilder["calculatePrice_tax"] > 0) ? false : true;

        // MODULE: Loyalty program -> points from price | For view only
        $priceBuilder["loyaltyPoints"] = $this->getLoyaltyPointsFromPrice($priceBuilder["calculatePrice_tax"], $activeLanguageInfo["exchangeRate"]);

        return $priceBuilder;
    }

// CHECK | Tax enabled/disabled
    public function checkTaxEnabled($langShort) {
        if (!empty($langShort)) {
            $activeLanguageInfo = $this->loadCurrency($langShort);
            if (!empty($activeLanguageInfo)) {
                return ($activeLanguageInfo["useTax"] == 1) ? true : false;
            }
        }

        return false;
    }

// CHECK | Prices without tax set enabled/disabled
    public function checkPricesDefaultNoTax($langShort, $activeLanguageInfo = null) {
        if (!empty($langShort) || $activeLanguageInfo !== null) {
            $activeLanguageInfo = ($activeLanguageInfo === null) ? $this->loadCurrency($langShort) : $activeLanguageInfo;
            if (!empty($activeLanguageInfo)) {
                return ($activeLanguageInfo["defaultNoTax"] == 1) ? true : false;
            }
        }

        return true;
    }

// GET | Currency note
    public function getCurrencyNote($langShort) {
        $activeLanguageInfo = null;

        if (!empty($langShort)) {
            $activeLanguageInfo = $this->loadCurrency($langShort);
        }

        return $activeLanguageInfo["currency"];
    }

// GET | Currency symbol
    public function getCurrencySymbol($langShort) {
        $activeLanguageInfo = null;

        if (!empty($langShort)) {
            $activeLanguageInfo = $this->loadCurrency($langShort);
        }

        return $activeLanguageInfo["symbol"];
    }

// GET | Currency rounding value
    public function getCurrencyRoundingValue($langShort) {
        if (!empty($langShort)) {
            $activeLanguageInfo = $this->loadCurrency($langShort);
            if ($activeLanguageInfo["round"] == 1) {
                return $activeLanguageInfo["decimals"];
            }
        }

        return $this->defaultRoundingValue;
    }

// GET | View prices (shorted Array)
    public function getViewPrices($priceArray, $multiplePrices = false) {
        $viewPrices["basePrice"] = $priceArray["basePrice"];
        $viewPrices["basePrice_tax"] = $priceArray["basePrice_tax"];

        $viewPrices["original_price"] = $priceArray["original_price"];
        $viewPrices["original_price_tax"] = $priceArray["original_price_tax"];

        $viewPrices["calculatePrice"] = $priceArray["calculatePrice"];
        $viewPrices["calculatePrice_tax"] = $priceArray["calculatePrice_tax"];
        $viewPrices["price"] = $priceArray["price"];
        $viewPrices["price_tax"] = $priceArray["price_tax"];

        $viewPrices["multiplePrices"] = $multiplePrices;

        return $viewPrices;
    }

// GET | Converted price value
    public function getConvertedPrice($langShort, $priceValue, $formatted = false, $customRounding = false) {
        $activeLanguageInfo = $this->loadCurrency($langShort);
        $price = $priceValue;

        if (!empty($activeLanguageInfo)) {
            $price = ($activeLanguageInfo["main"] == 1) ? $priceValue : $this->convertPrice($priceValue, $activeLanguageInfo["exchangeRate"]);
            if ($formatted === true) {
                $price = $this->buildPriceString($langShort, (float)$price, $customRounding);
            }
        }

        return $price;
    }

// GET PRICE WITHOUT TAX based on system settings
    public function getPriceWithoutTax($langShort, $taxValue, $priceValue, $exchange = false) {
        if (!empty($langShort)) {
            $activeLanguageInfo = $this->loadCurrency($langShort);
            if (!empty($activeLanguageInfo)) {

                $priceValue = ($exchange === true) ? $this->convertPrice($priceValue, $activeLanguageInfo["exchangeRate"]) : $priceValue;

                $tax = ($taxValue > 0) ? 1 + ($taxValue / 100) : 1;
                $taxNum = ($activeLanguageInfo["useTax"] == 1) ? $tax : 0;

                $priceBuilder["tax"] = ($taxNum > 0) ? $taxValue : 0;
                $priceBuilder["value"] = round((($taxNum > 0) ? $priceValue / $taxNum : $priceValue), $this->defaultRoundingValue);
                $priceBuilder["value_tax"] = round($priceValue, $this->defaultRoundingValue);

                return $priceBuilder;
            }
        }

        return false;
    }

// CALC | Exchange price
    public function convertPrice($priceValue, $exchangeRate) {
        return ($exchangeRate > 0) ? $priceValue / $exchangeRate : $priceValue;
    }

    public function reConvertPrice($priceValue, $exchangeRate) {
        return ($exchangeRate > 0) ? $priceValue * $exchangeRate : $priceValue;
    }

// CALC | Price level percentage
    private function calculateWithPercentage($priceValue) {
        return ($priceValue / 100) * $this->priceLevelPercentage;
    }

// MODULES Extensions
    public function getUserInfo(){
        return $this->loggedUser;
    }


    public function setPriceRules(Bool $isActive, Int $percentage = 0, Array $rules = []){
        $this->priceLevelChanged = $isActive;
        $this->priceLevelPercentage = $percentage;

        $this->loggedUser["priceRules"] = ['isActive' => $this->priceLevelChanged, 'percentage' => $this->priceLevelPercentage];
        if(!empty($rules)){
            $this->loggedUser["priceRules"]["extra"] = $rules;
        }
    }
    private function checkUser() {
        if ($this->appController->isActiveModule("users")) {
            $userEngine = new userEngine($this->database, $this->appController);
            $isLogged = $userEngine->checkIsLogged();
            if($isLogged === true){
                $userInfo = $userEngine->getUser(session('security.appLogin.logIn'));
                if(!empty($userInfo)){
                    $this->loggedUser = ['id' => $userInfo["id"], 'priceLevel' => $userInfo["priceLevel"], 'loyaltyLevel' => $userInfo["loyaltyLevel"]];
                    return true;
                }
            }
        }

        $this->loggedUser = [];
        return false;
    }
    private function checkUserPriceLevel() {
        if (!empty($this->loggedUser) && $this->appController->isActiveModule("priceLevels")) {
            $priceLevels = new userLevels($this->database, $this->appController);
            $userLevel = $priceLevels->getLevelById($this->loggedUser["priceLevel"], session('languages.appLang'));

            $priceLevelPercentage = ($userLevel["id"] > 0) ? $priceLevels->calculatePercentagePrice($userLevel["value"]) : $this->priceLevelPercentage;
            $priceLevelChanged = ($userLevel["id"] > 0) ? true : false;
            $this->setPriceRules($priceLevelChanged, $priceLevelPercentage);
            $this->customerPriceLevelId = ($userLevel["id"] > 0) ? $userLevel["id"] : null;
        }
    }
    private function checkManufacturerPriceLevel($manufacturerId) {
        if ($this->appController->isActiveModule("priceLevels") && $this->customerPriceLevelId !== null) {
            $priceLevels = new userLevels($this->database, $this->appController);
            $manufacturerValue = $priceLevels->getOneManufacturerLevel($manufacturerId, $this->customerPriceLevelId);

            $priceLevelPercentage = (isset($manufacturerValue["value"])) ? $priceLevels->calculatePercentagePrice($manufacturerValue["value"]) : $this->priceLevelPercentage;
            $priceLevelChanged = (isset($manufacturerValue["value"])) ? true : false;
            $this->setPriceRules($priceLevelChanged, $priceLevelPercentage);
        }

    }
    private function getLoyaltyPointsFromPrice($value, $exchangeRate) {
        if ($this->appController->isActiveModule("users")) {
            if ($this->appController->isActiveModule("loyaltyProgram")) {
                $loyaltyProgram = new loyaltyProgram($this->database, $this->appController);
                return $loyaltyProgram->getPointsToEarn($this->reConvertPrice($value, $exchangeRate));
            }
        }

        return false;
    }
    private function extendOptysEsoSubject(String $langShort){
        if (!empty($this->loggedUser) && $this->appController->isActiveModule("esoOptys")){
            $subjectsTable = databaseTables::getTable('esoOptys', 'subjects');
            $subjectInfo = $this->database->getQuery("SELECT * FROM `{$subjectsTable}` WHERE userId = :id", ['id' => $this->loggedUser["id"]], false);
            if(!empty($subjectInfo)){
                $this->loggedUser["esoSubject"] = $subjectInfo;
                if($this->loggedUser["esoSubject"]["currency"] === 'EUR'){
                    $langShort = 'sk_SK';
                }
            }
        }

        return $langShort;
    }

// EXCHANGE RATES from ČNB (Czechia)
    public function getCNBDailyRates() {
        if (!$this->dailyRatesCNB) {
            if ($data = file_get_contents('http://www.cnb.cz/cs/financni_trhy/devizovy_trh/kurzy_devizoveho_trhu/denni_kurz.txt?date=' . date('d.m.Y'))) {
                $cnb_rates = [];
                foreach (array_slice(explode("\n", $data), 2) as $rate) {
                    $rate = explode('|', $rate);
                    if (!empty($rate[3])) {
                        $cnb_rates[$rate[3]] = (float)preg_replace(
                            '/[^0-9.]*/',
                            '',
                            str_replace(',', '.', $rate[4])
                        );
                    }
                }

                $this->dailyRatesCNB = $cnb_rates;
            }
        }

        return $this->dailyRatesCNB;
    }

}