<?php
class LocalizationClass extends LanguageClass {

    private $appController;
    private $localeEngine;

    public $languagesTable;
    public $translationsTable;

    public $showMismatchContent = false; // TODO: Check if after last update this make sense
    private $delimeters = ["element" => ";;", "part" => ":=", "htmlBefore" => "{/", "htmlAfter" => "/}"]; // ELEMENT - Between multiple elements, like TITLE, DESCRIPTION, ETC. | PART - Between key and value
    private $appLang = null;
    private $adminLang = null;

    public $allActiveLanguages = null;
    public $mainLanguage = null;
    public $translateEnabled = null;

    public function __construct(Database $database, AppController $appController) {
        parent::__construct($database);
        $this->appController = $appController;
        $this->localeEngine = $appController->returnLocaleEngine();

        $this->appLang = $this->localeEngine->returnProjectLang();
        $this->adminLang = $this->localeEngine->returnAppLang();

        $this->languagesTable = databaseTables::getTable($this->moduleName, "languageTable");
        $this->translationsTable = databaseTables::getTable($this->moduleName, "translateTable");

        //$this->showMismatchContent = ($this->database->systemSettings["showMismatchContent"] == "true") ? true : false; // TODO: System settings removed, check how it works now
        $this->mainLanguage = $this->setMainLanguage();

        $this->checkEnabledTranslation();
    }

    private function setMainLanguage() {
        $mainLanguage = $this->localeEngine->returnMainLanguage();
        if(!empty($mainLanguage)){
            return $mainLanguage["langShort"];
        }

        return DEFAULT_LANG;
    }
    public function checkEnabledTranslation() {
        if ($this->translateEnabled === null) {
            $sql = "SELECT * FROM `{$this->languagesTable}` WHERE visibility = 1";
            $this->allActiveLanguages = $this->database->getQuery($sql, null, true);
            if (!empty($this->allActiveLanguages)) {
                $this->translateEnabled = true;
            }
        }
    }

/* ================== TRANSLATE FILES  === */

// CHECK FOR file with data for translate
    public function checkTranslateFile($langPath) {
        if (file_exists($langPath)) {
            $lang = $this->loadTranslateFile($langPath);
        } else {
            $lang = [];
        }
        return $lang;
    }
// RETURN file with data for translate
    public function loadTranslateFile($langPath) {
        include($langPath);
        return $lang;
    }


/* ================== TRANSLATE DATA RESULTS  === */

// CHECK GLOBAL translate table by URL and langShort
    public function checkExistingTranslation($url, $langShort) {

        $sql = "SELECT * FROM `{$this->translationsTable}` WHERE url = :url AND langShort = :langShort";
        $translateResult = $this->database->getQuery($sql, ["url" => $url, "langShort" => $langShort], false);

        if (!empty($translateResult)) {
            return $this->createTranslate($translateResult, $langShort, array());
        } else {
            return false;
        }

    }
// CHECK GLOBAL translate table by URL and langShort
    public function checkExistingTranslationByDefaultUrl($urlDefault, $langShort) {

        $sql = "SELECT * FROM `{$this->translationsTable}` WHERE urlDefault = :urlDefault AND langShort = :langShort";
        $translateResult = $this->database->getQuery($sql, ["urlDefault" => $urlDefault, "langShort" => $langShort], false);

        if (!empty($translateResult)) {
            return $this->createTranslate($translateResult, $langShort, []);
        } else {
            return false;
        }

    }
// CHECK GLOBAL translate table by ID, objectType and langShort
    public function checkExistingTranslationObject($objectId, $objectType, $langShort, $defaultData) {
        if ($this->translateEnabled !== true OR (empty($objectId) OR empty($objectType) OR empty($langShort))) {
            return $defaultData;
        }

        $sql = "SELECT * FROM `{$this->translationsTable}` WHERE objectId = :id AND objectType = :objectType AND langShort = :langShort";
        $translateResult = $this->database->getQuery($sql, ["id" => $objectId, "objectType" => $objectType, "langShort" => $langShort], false);

        if (!empty($translateResult)) {
            return $this->createTranslate($translateResult, $langShort, $defaultData);
        } else {
            return false;
        }

    }
// CHECK GLOBAL translate table by ID, contentTable and langShort
    public function checkExistingTranslationByTable($objectId, $contentTable, $langShort, $defaultData) {
        if ($this->translateEnabled !== true OR (empty($objectId) OR empty($contentTable) OR empty($langShort))) {
            return $defaultData;
        }

        $sql = "SELECT * FROM `{$this->translationsTable}` WHERE objectId = :id AND contentTable = :contentTable AND langShort = :langShort LIMIT 1";
        $translateResult = $this->database->getQuery($sql, ["id" => $objectId, "contentTable" => $contentTable, "langShort" => $langShort], false);

        if (!empty($translateResult)) {
            return $this->createTranslate($translateResult, $langShort, $defaultData);
        } else {
            return false;
        }

    }
// CREATE TRANSLATED DATA ARRAY for further processing
    public function createTranslate($translateResult, $langShort, $defaultData) {
        $translatedArray = $translateBox = $returnArray = [];

        // 1) Load default (not translated) data if not coming to method
        if (empty($defaultData)) {
            $objectTable = $translateResult["contentTable"];

            $sql = "SELECT * FROM `{$objectTable}` WHERE id = :id";  // ALL tables which have translate engine enabled MUST HAVE primary key named as ID!
            $bind = array("id" => $translateResult["objectId"]);

            $defaultData = $this->database->getQuery($sql, $bind, false);
        }
        // 2) Parse translate string into array (by delimeters)
        if (isset($translateResult["translate"]) && !empty($translateResult["translate"])) {
            $translateBox = $this->splitTranslateString($translateResult["translate"]);

            foreach ($defaultData as $colName => $colValue) {
                $translatedArray[$colName] = (isset($translateBox[$colName]) && (!empty($translateBox[$colName]) || $translateBox[$colName] == "0")) ? $translateBox[$colName] : $colValue;
            }

            if(!empty($translateResult["url"])){
                $translatedArray["url"] = $translateResult["url"];
            }
        }
        // 3) Check system settings for REPLACEMENT and select replacement if translate not exist
        if ($this->showMismatchContent === false) {
            $returnArray = (empty($translatedArray)) ? false : $translatedArray; // NULL - If translation not exist, return FALSE
        } else {
            $returnArray = (empty($translatedArray)) ? $defaultData : $translatedArray; // TRUE - If translation not exist, show original content (TEXT data)
        }
        // 4) If translate object exist, add lang prefix (langShort) for template
        if (!empty($returnArray)) {
            $returnArray = array_merge($returnArray, ["langShort" => $langShort]);
        }

        $returnArray["contentTable"] = $translateResult["contentTable"];

        return $returnArray;
    }

    public function splitTranslateString(String $translatedString){
        $translateBox = [];
        $translateParts = explode($this->delimeters["element"], $translatedString);
        foreach ($translateParts as $part) {
            if (!empty($part)) {
                $languagePart = explode($this->delimeters["part"], $part);
                $languageValue = $languagePart[1];
                if ($languagePart[0] == "description") { // Prevent HTML tags in description
                    $languageValue = str_replace($this->delimeters["htmlAfter"], "", $languagePart[1]);
                    $languageValue = str_replace($this->delimeters["htmlBefore"], "", $languageValue);
                    $languageValue = html_entity_decode($languageValue);
                }
                $translateBox[$languagePart[0]] = $languageValue;
            }
        }

        return $translateBox;
    }

// SELECT TRANSLATE by language prefix (langShort) FROM (own language table) and from MULTIPLE TRANSLATIONS
    public function selectRightTranslate($multipleData, $activeLang) {
        // 1) Check if translate for selected language exists
        $translatedObject = array_search($activeLang, array_column($multipleData, 'langShort'));
        // 2) If not exist and system settings for REPLACEMENT is enabled, load DEFAULT language
        if (!isset($translatedObject) && $this->showMismatchContent === true) {
            $translatedObject = array_search($this->mainLanguage, array_column($multipleData["data"], 'langShort'));
        }

        return ($translatedObject === false) ? [] : $multipleData[$translatedObject];
    }
// CREATE TRANSLATE STRING for saving into database (by delimeters)
    public function createTranslateString($postData) {
        $allTranslates = null;
        if (!empty($postData)) {
            foreach ($postData as $name => $value) {
                $value = str_replace($this->delimeters["part"], "", str_replace($this->delimeters["element"], "", $value));

                $stringValue = ($name == "description") ? $this->delimeters["htmlBefore"] . $value . $this->delimeters["htmlAfter"] : $value; // Prevent HTML tags in description
                $allTranslates .= $name . $this->delimeters["part"] . $stringValue . $this->delimeters["element"];
            }
        }

        return $allTranslates;
    }
// CREATE TRANSLATE STRING and skip edit in default array (if default lang is not activated)
    public function saveValueAndSkipEditing($langShort, $translateType, $dataArray, $translateNames, $objectId, $contentTable = null) {
        if ($langShort != $this->mainLanguage) {
            $translateData["translate"] = [];

            // Prevent existing translation
            $existingTranslation = [];

            $translateType = (empty($translateType)) ? '' : $translateType;
            $bindWhere = "objectId = :id AND langShort = :langShort AND objectType = :objectType";
            $bind = ["id" => $objectId, "langShort" => $langShort, "objectType" => $translateType];

            $translateResult = $this->database->getQuery("SELECT * FROM `{$this->translationsTable}` WHERE {$bindWhere} LIMIT 1", $bind, false);
            if (!empty($translateResult)) {
                $existingTranslation = $this->splitTranslateString($translateResult["translate"]);
            }

            // Set values
            foreach($translateNames as $name){
                $translateData["translate"][$name] = (isset($dataArray[$name])) ? $dataArray[$name] : ((isset($existingTranslation[$name])) ? $existingTranslation[$name] : null);
                unset($dataArray[$name]);
            }

            $this->updateQueryAndTranslate($translateData, $contentTable, $objectId, $translateType, $langShort, null, true);
        }

        return $dataArray;
    }

/* ================== TRANSLATE DATA RESULTS - METHODS FOR MODULES LOADING DATA - loading two language arrays (with langShort and with defaultLang) === */

// GET TRANSLATIONS - GLOBAL TRANSLATIONS (global table)
    public function translateResult($defaultData, $translateType, $langShort, $customIdentify = "id") {
        $returnData = [];
        $isMultidimensional = (isset($defaultData[0]) && is_array($defaultData[0])) ? true : false;

        if (!empty($defaultData) && $langShort !== null) {
            $allData = ($isMultidimensional !== true) ? [$defaultData] : $defaultData;
            foreach ($allData as $data) {
                // Get TRANSLATED localization
                $defaultArray = array_merge($data, ["langShort" => $this->mainLanguage]);
                $translatedArray = (isset($data[$customIdentify])) ? $this->checkExistingTranslationObject($data[$customIdentify], $translateType, $langShort, $data) : false;
                $translatedData = ($translatedArray !== false) ? $translatedArray : $defaultArray;

                $translatedData["activeTranslation"] = true;
                if(isset($translatedData["langShort"])){
                    $translatedData["defaultLang"] = $defaultArray; //TODO: If only one language, not necessary push default lang (be sure, that it won't affect admin panel)
                    $translatedData["activeTranslation"] = ($langShort == $translatedData["defaultLang"]["langShort"] OR $langShort == $translatedData["langShort"]) ? false : true; //TODO: Why when lang == default?
                    $translatedData["activeLang"] = strtok($translatedData["langShort"], '_');
                }

                $returnData[] = array_merge($data, $translatedData);
            }
            $returnData = ($isMultidimensional !== true) ? $returnData[0] : $returnData;
        }

        return $returnData;
    }
// GET TRANSLATIONS - CUSTOM TRANSLATIONS (custom tables)
    public function getTranslation($allData, $langColumns, $translateTable, $langShort) {
        $returnData = [];
        $isMultidimensional = (isset($allData[0]) && is_array($allData[0])) ? true : false;

        if (!empty($allData) && $langColumns !== null) {
            $allData = ($isMultidimensional !== true) ? [$allData] : $allData;
            foreach ($allData as $data) {
                // Get DEFAULT language localization AND TRANSLATED localization
                $sql = "SELECT {$langColumns} FROM `{$translateTable}` WHERE associatedId = :associatedId AND (langShort = :langShort OR langShort = :defaultLang) ORDER BY id ASC;";
                $allTranslates = $this->database->getQuery($sql, ["associatedId" => $data["id"], "langShort" => $langShort, "defaultLang" => $this->mainLanguage], true);

                if (empty($allTranslates)) {
                    // FIX - If TRANSLATED texts are created first, so NO DEFAULT language exists, LOAD first created translation (main object language)
                    $sql = "SELECT {$langColumns} FROM `{$translateTable}` WHERE associatedId = :associatedId ORDER BY id ASC LIMIT 1;";
                    $translatedData = $this->database->getQuery($sql, ["associatedId" => $data["id"]], false);
                    $translatedData["defaultLang"] = $translatedData;
                    $translatedData["activeTranslation"] = true;

                } elseif ($langShort == $this->mainLanguage OR $allTranslates[0]["langShort"] == $langShort) {
                    $translatedData = $allTranslates[0];
                    $translatedData["defaultLang"] = $allTranslates[0];
                    $translatedData["activeTranslation"] = ($langShort == $translatedData["defaultLang"]["langShort"] OR $langShort == $translatedData["langShort"]) ? false : true;

                } else {
                    $translatedData = (isset($allTranslates[1])) ? $allTranslates[1] : $allTranslates[0];
                    $translatedData["defaultLang"] = $allTranslates[0];
                    $translatedData["activeTranslation"] = ($langShort == $translatedData["defaultLang"]["langShort"] OR $langShort == $translatedData["langShort"]) ? false : true;
                }

                $translatedData["activeLang"] = strtok($translatedData["langShort"], '_');
                $translatedData["translationExists"] = ($translatedData["langShort"] != $translatedData["defaultLang"]["langShort"]);

                $translatedArray = $this->updateExistingTranslationByTableName($langShort, $this->mainLanguage, $translateTable, $data);
                $translatedData = (!empty($translatedArray)) ? array_merge($translatedData, $translatedArray) : $translatedData;

                $returnData[] = array_merge($data, $translatedData);
            }
            $returnData = ($isMultidimensional !== true) ? $returnData[0] : $returnData;
        }

        return $returnData;
    }

/* ================== METHODS FOR DATABASE PROCESS  === */

// SORT DATA and prepare them for SQL methods | Method check data an translate rows from each module settings (not filled rows are skipped)
    public function createMultiTableQuery($postData, $translatableRows, $dataRows) {
        $data = $translate = $other = [];
        foreach ($postData as $key => $value) {
            if (isset($translatableRows[$key]) OR in_array($key, $translatableRows)) {
                $translate[$key] = $value;
            } elseif (isset($dataRows[$key]) OR in_array($key, $dataRows)) {
                $data[$key] = $value;
            } else {
                $other[$key] = $value;
            }
        }

        return ["data" => $data, "translate" => $translate, "other" => $other];
    }
// INSERT DATA with language prefix (own language table)
    public function addQuery($postData, $dataTable, $translateTable, $objectForSkip = null) {

        if (!empty($postData["data"]) && $dataTable !== null && $objectForSkip === null) {
            $sql = "INSERT INTO `{$dataTable}`";
            (int)$doneHead = $this->database->insertQuery($sql, $postData["data"]);
            if ($doneHead === 0) {
                return false;
            }
        }

        if (!empty($postData["translate"]) && $translateTable !== null) {
            if ($doneHead !== 0 AND $doneHead !== '0') {
                $postData["translate"]["associatedId"] = $doneHead;
            }
            if ($objectForSkip !== null) {
                $postData["translate"]["associatedId"] = $objectForSkip;
                $postData["translate"]["langShort"] = $_SESSION["languages"]["adminLang"];
            }
            $sql = "INSERT INTO `{$translateTable}`";
            (int)$doneTranslate = $this->database->insertQuery($sql, $postData["translate"]);
            if ($doneTranslate === 0) {
                return false;
            }
        }

        $done = ($doneHead !== 0) ? $doneHead : $doneTranslate;
        return ($done === 0) ? false : $done;
    }
// INSERT DATA without language prefix (global translate table)
    public function addQueryAndTranslate($postData, $dataTable) {
        if ((!empty($postData["data"]) OR !empty($postData["translate"])) && $dataTable !== null) {
            $sql = "INSERT INTO `{$dataTable}`";
            $baseData = array_merge($postData["data"], $postData["translate"]);
            (int)$doneHead = $this->database->insertQuery($sql, $baseData);
            if ($doneHead === 0) {
                return false;
            }
        }

        return (isset($doneHead)) ? $doneHead : false;
    }
// UPDATE DATA with language prefix (own language table)
    public function updateQuery($postData, $dataTable, $translateTable, $objectId, $objectLang) {

        if (!empty($postData["data"]) && $dataTable !== null) {
            $sqlQuery = "UPDATE `{$dataTable}`";
            $whereSql = "WHERE id = :id";
            $done = $this->database->updateQuery($sqlQuery, $postData["data"], array("id" => $objectId), $whereSql);
            if ($done !== true) {
                return false;
            }
        }

        if (!empty($postData["translate"]) && $translateTable !== null) {
            $sqlQuery = "UPDATE `{$translateTable}`";
            $whereSql = "WHERE associatedId = :associatedId AND langShort = :langShort";
            $done = $this->database->updateQuery($sqlQuery, $postData["translate"], array("associatedId" => $objectId, "langShort" => $objectLang), $whereSql);
            if ($done !== true) {
                return false;
            }
        }

        return (isset($done)) ? true : false;
    }
// UPDATE DATA without language prefix (global translate table)
    public function updateQueryAndTranslate($postData, $dataTable, $objectId, $objectType, $langShort, $defaultUrl, $enableTranslate = true) {

        if ($enableTranslate === false && !empty($postData["data"]) && $dataTable !== null) { // If DEFAULT LANG data was saved - ONLY UPDATE
            $sqlQuery = "UPDATE `{$dataTable}`";
            $whereSql = "WHERE id = :id";
            $baseData = array_merge($postData["data"], $postData["translate"]);
            $done = $this->database->updateQuery($sqlQuery, $baseData, array("id" => $objectId), $whereSql);
            if ($done !== true) {
                return false;
            }
        }else{
           if(!empty($dataTable)){
               $queryCache = new QueryCache();
               $queryCache->remove("UPDATE `{$dataTable}` WHERE id = {$objectId}", 'UPDATE');
           }
        }

        if (!empty($postData["translate"]) && $enableTranslate === true) { // If TRANSLATE LANG data was saved - TRANSLATE DATA
            $objectType = (empty($objectType)) ? '' : $objectType;
            $translateSql = "SELECT * FROM `{$this->translationsTable}` WHERE objectId = :objectId AND langShort = :langShort AND objectType = :objectType";
            $translateBind = array("objectId" => $objectId, "langShort" => $langShort, "objectType" => $objectType);
            $translateCheck = $this->database->getQuery($translateSql, $translateBind, false);

            $translate = [];
            $translate["url"] = $postData["translate"]["url"];
            unset($postData["translate"]["url"]);
            $translate["translate"] = $this->createTranslateString($postData["translate"]);
            $translate["urlDefault"] = $defaultUrl;

            if (!isset($translate["url"])) {
                unset($translate["url"]);
                unset($translate["urlDefault"]);
            }

            if (!empty($translateCheck)) {
                $sqlQuery = "UPDATE `{$this->translationsTable}`";
                $whereSql = "WHERE objectId = :objectId AND langShort = :langShort AND objectType = :objectType";
                $done = $this->database->updateQuery($sqlQuery, $translate, array("objectId" => $objectId, "langShort" => $langShort, "objectType" => $objectType), $whereSql);
            } else {
                $translate["objectId"] = $objectId;
                $translate["objectType"] = $objectType;
                $translate["contentTable"] = $dataTable;
                $translate["langShort"] = $langShort;

                $sql = "INSERT INTO `{$this->translationsTable}`";
                (int)$done = $this->database->insertQuery($sql, $translate);
                if ($done === 0) {
                    return false;
                }
            }

        }

        return (isset($done)) ? true : false;
    }
// TRANSLATE SETTINGS - CHECK if translation exist (global translate table)
    private function checkTranslatedSettings($objectIdentify, $enableTranslate, $moduleName, $langShort) {
        if ($enableTranslate === true) { // If TRANSLATE LANG data was saved - TRANSLATE DATA
            $translateSql = "SELECT * FROM `{$this->translationsTable}` WHERE objectId = :objectId AND langShort = :langShort AND objectType = :objectType";
            $translateCheck = $this->database->getQuery($translateSql, ["objectId" => $objectIdentify, "langShort" => $langShort, "objectType" => $moduleName], false);
            return (!empty($translateCheck)) ? $translateCheck : false;
        } else {
            return false;
        }
    }
// TRANSLATE SETTINGS - INSERT/UPDATE if translation exist (global translate table)
    public function saveAndTranslateSettings($postData, $objectIdentify, $enableTranslate, $moduleName, $langShort, $settGroup = null) {
        if ($enableTranslate === true) {
            $translateResults = $this->checkTranslatedSettings($objectIdentify, $enableTranslate, $moduleName, $langShort);
            $translatedString = [];
            $translatedString["translate"] = $this->createTranslateString($postData);

            if ($translateResults !== false) {
                $sqlQuery = "UPDATE `{$this->translationsTable}`";
                $whereSql = "WHERE objectId = :objectId AND langShort = :langShort AND objectType = :objectType";
                $returnNotify = $this->database->updateQuery($sqlQuery, $translatedString, ["objectId" => $objectIdentify, "langShort" => $langShort, "objectType" => $moduleName], $whereSql);
            } else {
                $translatedString["objectId"] = $objectIdentify;
                $translatedString["objectType"] = $moduleName;
                $translatedString["contentTable"] = databaseTables::getTable("settings", "settingTable");
                $translatedString["langShort"] = $langShort;

                $sql = "INSERT INTO `{$this->translationsTable}`";
                $returnNotify = (int)$this->database->insertQuery($sql, $translatedString);
                $returnNotify = ($returnNotify === 0) ? false : true;
            }
        } else {
            $settingsClass = new loaderSettings($this->database, $this->appController);
            $returnNotify = $settingsClass->editSettings($postData, $moduleName, $settGroup);
        }

        return $returnNotify;
    }
// TRANSLATE SETTINGS - SELECT translation - if exist - with default Lang data (global translate table)
    public function getTranslatedSettings($defaultData, $objectIdentify, $moduleName, $langShort) {
        $defaultData = (empty($defaultData)) ? [] : $defaultData;
        return $this->translateResult(array_merge(["id" => $objectIdentify], $defaultData), $moduleName, $langShort);
    }
// CHECK IF TRANSLATION with language prefix EXIST (own language table) | return TRUE or FALSE
    protected function selectExistingTranslateObject($objectId, $translateTable, $langShort) {
        $sql = "SELECT * FROM `{$translateTable}` WHERE associatedId = :associatedId AND langShort = :langShort";
        $bind = ["associatedId" => $objectId, "langShort" => $langShort];
        $translateResult = $this->database->getQuery($sql, $bind, false);

        return (!empty($translateResult)) ? true : false;
    }
// CHECK IF TRANSLATION without language prefix EXIST (global translate table) | return TRUE or FALSE
    protected function selectExistingTranslate($objectId, $objectType, $langShort) {
        $sql = "SELECT * FROM `{$this->translationsTable}` WHERE objectId = :id AND objectType = :objectType AND langShort = :langShort";
        $bind = ["id" => $objectId, "objectType" => $objectType, "langShort" => $langShort];
        $translateResult = $this->database->getQuery($sql, $bind, false);

        return (!empty($translateResult)) ? true : false;
    }
// RETURN INFORMATION if translate exist or not
    public function checkForTranslate($objectId, $objectType, $translateTable, $langShort) {
        if ($translateTable !== null) {
            $translateResult = $this->selectExistingTranslateObject($objectId, $translateTable, $langShort);
        } else {
            $translateResult = $this->selectExistingTranslate($objectId, $objectType, $langShort);
        }

        return $translateResult;
    }
// UPDATE TRANSLATION by TABLE NAME
    public function updateExistingTranslationByTableName($langShort, $appLangShort, $translateTable, $defaultData){
        $translatedArray = [];
        if($langShort != $appLangShort){
            if($this->appController->returnLocalizationClass()->getOneLangByLangShort($langShort)["useRate"] == "0"){
                $translatedArray = $this->checkExistingTranslationByTable($defaultData["id"], $translateTable, $langShort, $defaultData);
            }
        }

        return $translatedArray;
    }
// CHECK GLOBAL translate table by ID, contentTable and langShort
    public function getGroupedTranslationsByTableName($contentTable, $langShort, $defaultData) {
        if ($this->translateEnabled !== true OR (empty($contentTable) OR empty($langShort))) {
            return $defaultData;
        }

        $sql = "SELECT * FROM `{$this->translationsTable}` WHERE contentTable = :contentTable AND langShort = :langShort";
        $translateResult = $this->database->getQuery($sql, ["contentTable" => $contentTable, "langShort" => $langShort], true);


        if (!empty($translateResult)) {
            $translatedArray["translate"] = null;
            foreach($translateResult as $results){
                $translatedArray["translate"] .= $results["translate"];
            }

            return $this->createTranslate($translatedArray, $langShort, $defaultData);
        } else {
            return false;
        }

    }
}