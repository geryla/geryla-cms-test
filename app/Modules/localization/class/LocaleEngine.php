<?php
class LocaleEngine {
    private $database;
    private $languageClass;

    protected $mainLanguage; // Main App language | Array
    protected $appLang = null; // Active App language shortCode | String
    protected $adminLang = null; // Active Admin language shortCode | String
    protected $langFile; // Language file for both active languages | Multidimensional Array

    private $moduleLangFolder;
    private $activeModulesPaths;

    public function __construct(Database $database, ModulesController $modulesController, $appLang = null, $adminLang = null) {
        $this->database = $database;
        $this->languageClass = new LanguageClass($database);

        $this->appLang = $appLang;
        $this->adminLang = $adminLang;

        $this->moduleLangFolder = $modulesController->moduleFolders["lang"];
        $this->activeModulesPaths = $modulesController->getConfig('paths');

        $this->checkLanguage();
    }

    private function checkLanguage() {
        $this->mainLanguage = $this->languageClass->getMainLanguage();

        if ($this->appLang === null) {
            if (!isset($_SESSION["languages"]["appLang"]) OR empty($_SESSION["languages"]["appLang"])) {
                $_SESSION["languages"]["appLang"] = (!empty($this->mainLanguage)) ? $this->mainLanguage["langShort"] : DEFAULT_LANG;
            }
            $this->appLang = $_SESSION["languages"]["appLang"];
        } else {
            $this->setLanguage("appLang", $this->appLang);
        }

        if ($this->adminLang === null) {
            if (!isset($_SESSION["languages"]["adminLang"]) OR empty($_SESSION["languages"]["adminLang"])) {
                $_SESSION["languages"]["adminLang"] = (!empty($this->mainLanguage)) ? $this->mainLanguage["langShort"] : DEFAULT_LANG;
            }
            $this->adminLang = $_SESSION["languages"]["adminLang"];
        } else {
            $this->setLanguage("adminLang", $this->adminLang);
        }

        $this->createLangFile("appLang", $this->activeModulesPaths, $this->moduleLangFolder);
        $this->createLangFile("adminLang", $this->activeModulesPaths, $this->moduleLangFolder);
    }

    private function createLangFile($langType, $activeModulesPaths, $moduleLangFolder) {
        // SET BACKEND LANG to admin preset
        if($langType === "adminLang"){
            //$adminInfo = new adminEngine($this->database, $this);
            $adminLogged = (isset($_SESSION["security"]["adminLogin"]["logIn"])) ?? [];
            if(!empty($adminLogged)){
                $langBackUp = $_SESSION["languages"]["adminLang"];
                $this->setLanguage("adminLang", $adminLogged["adminLang"]);
            }
        }

        // LOAD FILES with language strings
        $langDefault = $this->getLanguageFile($_SESSION["languages"][$langType], false);
        $langModules = $this->getModuleLangFiles($langType, $activeModulesPaths, $moduleLangFolder);
        $langCustom = $this->getLanguageFile($_SESSION["languages"][$langType], true);

        // RETURN BACKEND LANG for editing
        if($langType === "adminLang"){
            if(isset($langBackUp)){
                $this->setLanguage("adminLang", $langBackUp);
            }
        }

        $this->langFile[$langType] = array_merge($langDefault, $langModules, $langCustom);
        return true;
    }
    private function getLanguageFile($langShort, $customLang = false) {
        $langPath = (($customLang === true) ? RESOURCES_DIR : ADMIN_DIR).LANG.'/';

        if (file_exists($langPath.$langShort.".php")) {
            include($langPath.$langShort.".php");
        } else {
            include($langPath.DEFAULT_LANG.".php");
        }

        return $lang;
    }
    private function getModuleLangFiles($langShort, $activeModulesPaths, $moduleLangFolder) {
        $activeLang = $_SESSION["languages"][$langShort];
        $langFile = $this->loadModuleLangFiles($activeModulesPaths, $moduleLangFolder, $activeLang);

        return $langFile;
    }
    private function loadModuleLangFiles($activeModulesPaths, $moduleLangFolder, $activeLang){
        $langFile = [];
        if ($activeModulesPaths) {
            foreach ($activeModulesPaths as $modules) {

                if (isset($modules[$moduleLangFolder][$activeLang]) && $activeLang != DEFAULT_LANG) {
                    $lang = $this->includeFile($modules, $moduleLangFolder, DEFAULT_LANG);
                    $originalLang = $lang;

                    $lang = $this->includeFile($modules, $moduleLangFolder, $activeLang);
                    $lang = array_filter($lang);

                    $lang = array_merge($originalLang, $lang);
                } else {
                    $lang = $this->includeFile($modules, $moduleLangFolder, DEFAULT_LANG);
                }

                $langFile = array_merge($langFile, $lang);
            }
        }

        return $langFile;
    }
    private function includeFile($modules, $moduleLangFolder, $langShort){
        if(is_array($modules) && array_key_exists($moduleLangFolder, $modules) && is_array($modules[$moduleLangFolder])){
            if ($modules[$moduleLangFolder][$langShort] !== null && file_exists($modules[$moduleLangFolder][$langShort])) {
                include($modules[$moduleLangFolder][$langShort]);
                return $lang;
            }
        }

        return [];
    }

    public function setLanguage($langType, $langShort) {
        $language = $this->languageClass->visibleLanguage($langShort);
        $_SESSION["languages"][$langType] = (!empty($language) && $language["visibility"] == 1) ? $langShort : DEFAULT_LANG;
        $this->$langType = $_SESSION["languages"][$langType];
    }
    public function getCustomLanguageTexts($customLangShort) {
        $langFile_default = $this->getLanguageFile($customLangShort, false);
        $langFile_modules = $this->loadModuleLangFiles($this->activeModulesPaths, $this->moduleLangFolder, $customLangShort);
        $langFile_custom = $this->getLanguageFile($customLangShort, true);

        return array_merge($langFile_default, $langFile_modules, $langFile_custom);
    }

    public function returnMainLanguage() {
        return $this->mainLanguage;
    }
    public function returnProjectLang() {
        return $this->appLang;
    }
    public function returnAppLang() {
        return $this->adminLang;
    }
    public function returnLanguageFile(String $fileType = "appLang") {
        return (isset($this->langFile[$fileType])) ? $this->langFile[$fileType] : [];
    }
    public function returnCustomLangFile(String $fileType = "appLang") {
        return $this->getLanguageFile($_SESSION["languages"][$fileType], true);
    }
}