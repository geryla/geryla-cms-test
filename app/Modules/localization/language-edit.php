<?php
/* =============================== CONTROLLER METHODS ======== */
$langShort = ( isset($_GET["short"]) && !empty($_GET["short"]) ) ? $_GET["short"] : null;
$selectedModule = ( isset($_GET["module"]) && !empty($_GET["module"]) ) ? $_GET["module"] : null;

$prevPage = $dataCompiler->modulePageUrl(2);
$languageControl = $localizationClass->visibleLanguage($langShort);
if( empty($languageControl) OR (!empty($selectedModule) && !in_array($selectedModule,$modulesController->returnModules('active'))) ){
  $dataCompiler->setContinue(true);
  $dataCompiler->redirectPage(0, $prevPage);
}
$prevPage = $prevPage.'?short='.$langShort;

if( empty($selectedModule) ){
  $filePath = RESOURCES_DIR.LANG.'/'.$langShort.".php";
  $langFile = $localizationClass->checkTranslateFile($filePath);
  $defaultLangFile = $localizationClass->loadTranslateFile(RESOURCES_DIR.LANG."/".DEFAULT_LANG.".php");
}else{
  $modulesPaths = $modulesController->getConfig('paths');
  $defaultPath = $modulesPaths[$selectedModule]["languages"][DEFAULT_LANG];
  $filePath = ( isset($modulesPaths[$selectedModule]["languages"][$langShort]) ) ? $modulesPaths[$selectedModule]["languages"][$langShort] : null;
  $langFile = ( $filePath !== null ) ? $localizationClass->loadTranslateFile($filePath) : [];
  $filePath = ( $filePath !== null ) ? $filePath : str_replace(DEFAULT_LANG, $langShort, $defaultPath);
  $defaultLangFile = $localizationClass->loadTranslateFile($defaultPath);
}

if( isset($_POST["submit"]) ){  
  unset($_POST["submit"]);
  $returnMessage = translate("admin-create-false");
  if( !empty($_POST) ){
    $langData = $_POST;  
    $fileUpdate = file_put_contents($filePath, '<?php $lang = ' . var_export($langData, true) . ';');
    if($fileUpdate !== false){
        $isWritable = is_writable($filePath);
        $returnMessage = ($isWritable) ? translate("mod-localization-lang-translate-true") : translate("admin-create-false");
        $systemLogger->createLog($presenter->templateData->langShort, $presenter->activeContent->module, $selectedModule, $langShort, $_SESSION["security"]["adminLogin"]["logIn"]);
    }else{
        $returnMessage = translate("mod-localization-lang-translate-false");
    }
  } 

  $dataCompiler->setMessage($returnMessage);
  $dataCompiler->setContinue(true);
}

$dataCompiler->showMessage();
$dataCompiler->refreshPage($dataCompiler->nextStep, "");


/* =============================== CONTENT ======== */ 
echo '<article class="module">';
  
  $render->header(translate("mod-localization-lang-edit"), $prevPage);
                                            
  echo '<section>';                                      
    echo '<form action="" method="POST" enctype="multipart/form-data">';                 
      
      echo $render->button("submit", "true", translate("save"), false, null, "btn icon save success");
          
      echo '<div class="moduleWrap">';               
        echo '<p class="langHelpText">'.translate("mod-localization-lang-helpText").'</p>';
             
        foreach($defaultLangFile as $langName => $langValue){
          if( !empty($langValue) ){
            $translate = ( isset($langFile[$langName]) ) ? $langFile[$langName] : "";   
            echo '<div class="languageDiv">';
              echo '<div class="leftPart">'.htmlspecialchars($langValue).'</div>';
              echo '<div class="rightPart">';
                echo '<textarea name="'.$langName.'">'.$translate.'</textarea>';
              echo '</div>';
            echo '</div>';              
          }else{
            echo '<textarea name="'.$langName.'" style="display: none;"></textarea>';
          }
        }  
                
      echo '</div>'; 
      
      echo $render->button("submit", "true", translate("save"), false, null, "btn icon save success");
      
    echo '</form>'; 			 
  echo '</section>';     
echo '</article>';