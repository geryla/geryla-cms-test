<?php
$lang = array();

$lang["localization"] = 'Localization'; // Module name

$lang["language-settings"] = 'Language and translations'; // Page name
$lang["language-assets"] = 'Edit Language'; // Page name
$lang["language-edit"] = 'Translation'; // Page name


$lang["mod-localization-lang-list"] = 'List of languages';
$lang["mod-localization-lang-assets"] = 'Items to translate';
$lang["mod-localization-lang-edit"] = 'Edit translation';

$lang["mod-localization-lang-dropDown"] = 'Language selection';

$lang["mod-localization-lang-table-title"] = 'Abbreviation and title';
$lang["mod-localization-lang-table-currency"] = 'Currency and symbol';
$lang["mod-localization-lang-table-active"] = 'Active';
$lang["mod-localization-lang-table-files"] = 'Translation files';
$lang["mod-localization-lang-table-rate"] = 'Exchange rate';
$lang["mod-localization-lang-table-tax"] = 'VAT Mode';
$lang["mod-localization-lang-table-tax-true"] = 'On';
$lang["mod-localization-lang-table-tax-false"] = 'Off';
$lang["mod-localization-lang-table-round"] = 'Rounding';
$lang["mod-localization-lang-table-round-true"] = 'Yes';
$lang["mod-localization-lang-table-round-false"] = 'No';
$lang["mod-localization-lang-table-decimals"] = 'Decimals';
$lang["mod-localization-lang-table-useRate"] = 'Price Transfers';
$lang["mod-localization-lang-table-useRate-all"] = 'All Rates';
$lang["mod-localization-lang-table-useRate-system"] = 'System calculations';
$lang["mod-localization-lang-table-defaultPrices"] = 'Entering default prices';
$lang["mod-localization-lang-table-defaultPrices-tax"] = 'with VAT';
$lang["mod-localization-lang-table-defaultPrices-noTax"] = 'no VAT';

$lang["mod-localization-lang-table-moduleName"] = 'Module Name';
$lang["mod-localization-lang-table-editFiles"] = 'Edit Translation';

$lang["mod-localization-lang-helpText"] = 'Please always respect all punctuation when translating. If the string contains %s, %d or HTML tags, leave these variables unchanged.';
$lang["mod-localization-lang-table-default-title"] = 'System Texts';

$lang["mod-localization-lang-translate-true"] = 'The translation has been saved!';
$lang["mod-localization-lang-translate-notAllowed"] = 'Translation only allowed while editing!';
$lang["mod-localization-lang-translate-false"] = 'The translation file could not be saved!';

$lang["mod-localization-lang-table-mismatchContent-title"] = 'Missing translations';
$lang["mod-localization-lang-table-mismatchContent-subtitle"] = 'Enable substitution';
$lang["mod-localization-lang-table-mismatchContent-help"] = '<i>If enabled, missing translations will be replaced by the default language. Otherwise, you will be redirected to the page NOT FOUND.</i>';

$lang["mod-localization-dashboard-languageSettings"] = 'Language Settings';
$lang["mod-localization-dashboard-warning"] = 'Warning';
$lang["mod-localization-dashboard-missingTranslations"] = 'A translation was not found for the templates below, which may cause a problem displaying the content.';
$lang["mod-localization-dashboard-defaultLanguage"] = 'Default Language';
$lang["mod-localization-dashboard-otherLanguages"] = 'Allowed mutations';