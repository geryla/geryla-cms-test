<?php
$lang = array();

$lang["localization"] = 'Lokalizace'; // Module name

$lang["language-settings"] = 'Jazyk a překlady'; // Page name
  $lang["language-assets"] = 'Úprava jazyka'; // Page name
    $lang["language-edit"] = 'Překlad'; // Page name


$lang["mod-localization-lang-list"] = 'Seznam jazyků';
$lang["mod-localization-lang-assets"] = 'Položky k překladu';
$lang["mod-localization-lang-edit"] = 'Úprava překladu';

$lang["mod-localization-lang-dropDown"] = 'Volba jazyka';

$lang["mod-localization-lang-table-title"] = 'Zkratka a název';
$lang["mod-localization-lang-table-currency"] = 'Měna a symbol';
$lang["mod-localization-lang-table-active"] = 'Aktivní';
$lang["mod-localization-lang-table-files"] = 'Překladové soubory';
$lang["mod-localization-lang-table-rate"] = 'Směnný kurz';
$lang["mod-localization-lang-table-tax"] = 'Režim DPH';
$lang["mod-localization-lang-table-tax-true"] = 'Zapnuto';
$lang["mod-localization-lang-table-tax-false"] = 'Vypnuto';
$lang["mod-localization-lang-table-round"] = 'Zaokrouhlování';
$lang["mod-localization-lang-table-round-true"] = 'Ano';
$lang["mod-localization-lang-table-round-false"] = 'Ne';
$lang["mod-localization-lang-table-decimals"] = 'Desetinná místa';
$lang["mod-localization-lang-table-useRate"] = 'Převody cen';
$lang["mod-localization-lang-table-useRate-all"] = 'Všechny ceny';
$lang["mod-localization-lang-table-useRate-system"] = 'Systémové výpočty';
$lang["mod-localization-lang-table-defaultPrices"] = 'Zadávání základních cen';
$lang["mod-localization-lang-table-defaultPrices-tax"] = 's DPH';
$lang["mod-localization-lang-table-defaultPrices-noTax"] = 'bez DPH';

$lang["mod-localization-lang-table-moduleName"] = 'Název modulu';
$lang["mod-localization-lang-table-editFiles"] = 'Upravit překlad';

$lang["mod-localization-lang-helpText"] = 'Při překladu prosím vždy dodržujte veškerou interpunkci. V případě, že řetězec obsahuje proměnné %s, %d nebo HTML tagy, tyto proměnné nechte beze změny.';
$lang["mod-localization-lang-table-default-title"] = 'Systémové texty';

$lang["mod-localization-lang-translate-true"] = 'Překlad byl uložen!';
$lang["mod-localization-lang-translate-notAllowed"] = 'Překlad povolen pouze při editaci!';
$lang["mod-localization-lang-translate-false"] = 'Překladový soubor nebylo možné uložit!';

$lang["mod-localization-lang-table-mismatchContent-title"] = 'Chybějící překlady';
$lang["mod-localization-lang-table-mismatchContent-subtitle"] = 'Povolit nahrazování';
$lang["mod-localization-lang-table-mismatchContent-help"] = '<i>Pokud je povoleno, budou chybějící překlady nahrazeny výchozím jazykem. V opačném případě dojde k přesměrování na stránku NENALEZENO.</i>';

$lang["mod-localization-dashboard-languageSettings"] = 'Jazyková nastavení ';
$lang["mod-localization-dashboard-warning"] = 'Upozornění';
$lang["mod-localization-dashboard-missingTranslations"] = 'U níže uvedených šablon nebyl nalezen překlad, což může způsobit problém se zobrazováním obsahu.';
$lang["mod-localization-dashboard-defaultLanguage"] = 'Výchozí jazyk';
$lang["mod-localization-dashboard-otherLanguages"] = 'Povolené mutace';