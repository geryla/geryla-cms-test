<?php
/* =============================== CONTROLLER METHODS ======== */
$prevPage = $dataCompiler->modulePageUrl(1);
$nextPage = $dataCompiler->modulePageUrl(3);
$langShort = (isset($_GET["short"])) ? $_GET["short"] : null;

$languageControl = $localizationClass->visibleLanguage($langShort);
if( empty($languageControl) ){
  $dataCompiler->setContinue(true);
  $dataCompiler->redirectPage(0, $prevPage);
}

/* =============================== CONTENT ======== */ 
echo '<article class="module">';
  
  $render->header(translate("mod-localization-lang-assets"), $prevPage);
                                            
  echo '<section>';                                      
    echo '<form action="" method="POST" enctype="multipart/form-data">';                 
          
      echo '<div class="moduleWrap">';             
        echo '<table class="striped">'; 
          	
          $tableHead = [
            '<b>'.translate("mod-localization-lang-table-moduleName").'</b>',
            '<b>'.translate("mod-localization-lang-table-files").'</b>'
          ];
          $render->tableHead($tableHead);
        
          foreach($modulesController->returnModules('active') as $activeModule){
          	echo '<tr>';
              $render->tableColumn(translate($activeModule));
              $render->tableColumn('<a href="'.$nextPage.'?short='.$langShort.'&module='.$activeModule.'" class="blue anchor">'.translate("mod-localization-lang-table-editFiles").'</a>');
          	echo '</tr>';                
          } 
        	echo '<tr>';
            $render->tableColumn(translate("mod-localization-lang-table-default-title"));
            $render->tableColumn('<a href="'.$nextPage.'?short='.$langShort.'" class="blue anchor">'.translate("mod-localization-lang-table-editFiles").'</a>');
        	echo '</tr>';                
                                  
        echo '</table>';            
      echo '</div>'; 
     
    echo '</form>'; 			 
  echo '</section>';     
echo '</article>';