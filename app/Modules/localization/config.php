<?php
    $config = [
        "moduleName" => "Localization",
        "moduleUrlName" => "localization",
        "moduleLangName" => "localization",
        "moduleIcon" => "fa-language",
        "moduleVersion" => "3.0.0",
        "requiredModules" => [
            ["moduleName" => "settings"]
        ],

        "mediaSettings" => [],
        "translateType" => [],
        "databaseTables" => [
            "languageTable" => [
                "name" => "language",
            ],
            "translateTable" => [
                "name" => "language-translations",
            ],
        ],

        "modulePages" => [
            [
                "pageId" => 1,
                "urlName" => "language-settings",
                "url" => "language-settings",
                "parent" => 0,
                "inMenu" => 1,
                "headPage" => 1
            ],
            [
                "pageId" => 2,
                "urlName" => "language-assets",
                "url" => "language-assets",
                "parent" => 1,
                "inMenu" => 0,
                "headPage" => 0
            ],
            [
                "pageId" => 3,
                "urlName" => "language-edit",
                "url" => "language-edit",
                "parent" => 2,
                "inMenu" => 0,
                "headPage" => 0
            ]
        ]
    ];