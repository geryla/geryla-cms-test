$(window).load(function() {
    hideNoTranslatableItems();
});
$(document).ajaxComplete(function() {
    hideNoTranslatableItems();
});

function hideNoTranslatableItems(){
    var isMainLang = $('#toolbar .toolsList .langSwitcher .dropdown-label').attr('data-default');
    if (isMainLang === 'true'){
        $('.notTranslatable').removeClass('notTranslatable');
    }

    $('.notTranslatable').prop('disabled', true);
    $('div.notTranslatable, span.notTranslatable, label.notTranslatable').css({'position' : 'relative'}).append('<div style="position:absolute;left: 0px;top: 0px;height: 100%; width: 100%; z-index: 2;cursor: not-allowed;"></div>')
}