<?php
/* =============================== CONTROLLER METHODS ======== */
$tableForScripts = $localizationClass->languagesTable;
$nextPage = $dataCompiler->modulePageUrl(2);
$pageData = $localizationClass->getAllLanguages();
$settingsController = $settingsController->getModuleSettings("localization", true);

if (isset($_POST['submit'])) {

    $submitData = [];
    $defaultNoTax = $_POST["defaultNoTax"];
    $postValues = $_POST;
    unset($postValues["submit"], $postValues["defaultNoTax"]);

    foreach ($postValues as $key => $value) {
        $value["defaultNoTax"] = $defaultNoTax;
        $langEdit = $localizationClass->editLanguage($value, ["id" => $key]);
        if ($langEdit === false) {
            break;
        }
    }

    if ($langEdit === true) {
        $returnMessage = translate("admin-edit-true");
        $systemLogger->createLog($presenter->templateData->langShort, $presenter->activeContent->module, "langSettings", 0, $_SESSION["security"]["adminLogin"]["logIn"]);
    } else {
        $returnMessage = translate("admin-create-false");
    }

    $dataCompiler->setMessage($returnMessage);
    $dataCompiler->setContinue(true);
}

$dataCompiler->showMessage();
$dataCompiler->refreshPage($dataCompiler->nextStep, "");

/* =============================== CONTENT ======== */
echo '<article class="module">';

$render->header(translate("mod-localization-lang-list"), null);

echo '<section>';
echo '<form action="" method="POST" enctype="multipart/form-data">';

echo $render->button("submit", "true", translate("save"), false, null, "btn icon save success");

echo '<div class="moduleWrap">';
echo '<table class="striped">';
$selectValues = [["value" => "0", "title" => translate("mod-localization-lang-table-defaultPrices-tax")], ["value" => "1", "title" => translate("mod-localization-lang-table-defaultPrices-noTax")]];
$render->tableRow('<b>' . translate("mod-localization-lang-table-defaultPrices") . '</b>', $render->select("defaultNoTax", $selectValues, "value", "title", $pageData[0]["defaultNoTax"], true));
echo '</table>';

echo '<table class="striped">';

$tableHead = [
    translate("mod-localization-lang-table-title"),
    translate("mod-localization-lang-table-currency"),
    translate("mod-localization-lang-table-active"),
    translate("mod-localization-lang-table-files"),
    translate("mod-localization-lang-table-tax"),
    translate("mod-localization-lang-table-round"),
    translate("mod-localization-lang-table-decimals"),
    translate("mod-localization-lang-table-rate"),
    translate("mod-localization-lang-table-useRate")
];
$render->tableHead($tableHead);

foreach ($pageData as $language) {
    echo '<tr class="langRow">';
    $render->tableColumn('<i>' . $language["langShort"] . '</i> | ' . $language["title"], 200);
    $render->tableColumn($language["currency"] . ' <i>(' . $language["symbol"] . ')</i> ', 200);

    $render->tableColumn((($language["main"] != 1) ? $render->visibilitySwitch($language["id"], $tableForScripts, $language["id"], $language["visibility"]) : null));

    $render->tableColumn('<a href="' . $nextPage . '?short=' . $language["langShort"] . '" class="blue anchor">' . translate("mod-localization-lang-table-editFiles") . '</a>');

    $selectValues = [["value" => "1", "title" => translate("mod-localization-lang-table-tax-true")], ["value" => "0", "title" => translate("mod-localization-lang-table-tax-false")]];
    $render->tableColumn($render->select($language["id"] . '[useTax]', $selectValues, "value", "title", $language["useTax"], false), 130);

    $selectValues = [["value" => "1", "title" => translate("mod-localization-lang-table-round-true")], ["value" => "0", "title" => translate("mod-localization-lang-table-round-false")]];
    $render->tableColumn($render->select($language["id"] . '[round]', $selectValues, "value", "title", $language["round"], false), 150);

    $render->tableColumn($render->input("text", $language["id"] . "[decimals]", $language["decimals"], null, false), 150);

    $render->tableColumn((($language["main"] != 1) ? $render->input("text", $language["id"] . "[exchangeRate]", $language["exchangeRate"], null, false) : null), 150);

    $selectValues = [["value" => "1", "title" => translate("mod-localization-lang-table-useRate-all")], ["value" => "0", "title" => translate("mod-localization-lang-table-useRate-system")]];
    $render->tableColumn($render->select($language["id"] . '[useRate]', $selectValues, "value", "title", $language["useRate"], false), 200);

    echo '</tr>';
}

echo '</table>';
echo '</div>';

echo $render->button("submit", "true", translate("save"), false, null, "btn icon save success");

echo '</form>';
echo '</section>';
echo '</article>';