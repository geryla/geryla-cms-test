-- INSERT MODULE SETTINGS into TABLE 'settings'
INSERT INTO `settings` (`settName`, `settValue`, `moduleName`, `settGroup`) VALUES ('showMismatchContent', 'false', 'localization', '');

-- CREATE TABLE for languages
CREATE TABLE IF NOT EXISTS `language` (
  `id` int(10) NOT NULL AUTO_INCREMENT,

  `title` varchar(30) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `currency` varchar(5) NOT NULL,
  `symbol` varchar(5) NOT NULL,
  `symbolPosition` varchar(5) NOT NULL DEFAULT 'right',
  `isoAlpha2` varchar(2) NOT NULL,
  `isoAlpha3` varchar(3) NOT NULL,
  `langShort` varchar(5) NOT NULL,
  `exchangeRate` decimal(9,3) NOT NULL DEFAULT '0.000',
  `useTax` int(1) NOT NULL DEFAULT '0',
  `round` int(1) DEFAULT '0',
  `main` int(1) DEFAULT '0',

  `visibility` int(1) NOT NULL DEFAULT '0',
  `orderNum` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- CREATE TABLE for translations
CREATE TABLE IF NOT EXISTS `language-translations` (
  `id` int(10) NOT NULL AUTO_INCREMENT,

  `objectId` varchar(15) NOT NULL,
  `objectType` varchar(25) NULL,
  `urlDefault` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `translate` text CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `contentTable` varchar(75) NOT NULL,

  `langShort` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- TRUNCATE DATA AND INSERT DEFAULT VALUES - Languages
ALTER TABLE `language` DISABLE KEYS;
DELETE FROM `language`;
INSERT INTO `language` (`id`, `title`, `currency`, `symbol`, `symbolPosition`, `isoAlpha2`, `isoAlpha3`, `langShort`, `exchangeRate`, `useTax`, `round`, `main`, `visibility`, `orderNum`)
VALUES (1, 'Čeština', 'CZK', 'Kč', 'right', 'CZ', 'CZE', 'cs_CZ', 0.000, 1, 1, 1, 1, 0);
ALTER TABLE `language` ENABLE KEYS;

-- TRUNCATE DATA - Translations
ALTER TABLE `language-translations` DISABLE KEYS;
DELETE FROM `language-translations`;
ALTER TABLE `language-translations` ENABLE KEYS;