SET FOREIGN_KEY_CHECKS=0;

-- DELETE TABLES
DROP TABLE `language`;
DROP TABLE `language-translations`;

-- DELETE module SETTINGS
DELETE FROM `settings` WHERE moduleName = 'localization';

SET FOREIGN_KEY_CHECKS=1;