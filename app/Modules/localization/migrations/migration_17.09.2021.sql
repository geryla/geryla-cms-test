-- ADD decimals option for rounding
ALTER TABLE `language` ADD `useRate` INT(1) DEFAULT '1' AFTER `decimals`;