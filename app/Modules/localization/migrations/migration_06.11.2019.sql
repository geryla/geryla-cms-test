-- ADD decimals option for rounding
ALTER TABLE `language` ADD `decimals` int(2) DEFAULT 0 AFTER `round`;