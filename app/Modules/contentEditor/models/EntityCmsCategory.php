<?php
class EntityCmsCategory extends Entity{

    protected $extendedParams = [
        "title" => [
            "type" => "String",
        ],
        "shortDesc" => [
            "type" => "String",
        ],
        "description" => [
            "type" => "String",
        ],
        "parentId" => [
            "type" => "Integer",
        ],
        "path" => [
            "type" => "String",
        ],
        "className" => [
            "type" => "String",
        ],
        "images" => [
            "type" => "Array",
        ],
    ];

    public function __construct(Array $objectData) {
        $this->extendEntity($objectData);
        $this->build($objectData, $this->extendedParams);
    }
    private function extendEntity(Array $objectData) {
        if(isExisting('images', $objectData)){
            $entityImage = new EntityImage($objectData["images"]);

            $this->extendedParams["images"]["body"]["head"] = ["type" => "Array", "body" => $entityImage->getParams(true)];
            $this->extendedParams["images"]["body"]["other"] = ["type" => "Array", "body" => $entityImage->getParams(true)];
            $this->extendedParams["images"]["body"]["all"] = ["type" => "Array", "body" => $entityImage->getParams(true)];
        }
    }

}