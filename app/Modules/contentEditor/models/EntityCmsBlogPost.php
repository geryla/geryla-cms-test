<?php
class EntityCmsBlogPost extends Entity{

    protected $extendedParams = [
        "title" => [
            "type" => "String",
        ],
        "shortDesc" => [
            "type" => "String",
        ],
        "description" => [
            "type" => "String",
        ],
        "perex" => [
            "type" => "String",
        ],
        "path" => [
            "type" => "String",
        ],
        "date" => [
            "type" => "String",
        ],
        "time" => [
            "type" => "String",
        ],
        "lengthTime" => [
            "type" => "String",
        ],
        "postType" => [
            "type" => "Integer",
        ],
        "className" => [
            "type" => "String",
        ],
        "categories" => [
            "type" => "Array",
        ],
        "images" => [
            "type" => "Array",
        ],
        "files" => [
            "type" => "Array",
        ],
        "authorId" => [
            "type" => "Integer",
        ],
        "revisionDate" => [
            "type" => "String",
        ],
        //"autoEnable" => [],
        //"pageSidebar" => [],
    ];

    public function __construct(Array $objectData) {
        $this->extendEntity($objectData);
        $this->build($objectData, $this->extendedParams);
    }
    private function extendEntity(Array $objectData) {
        if(isExisting('categories', $objectData)){
            $entityCmsCategory = new EntityCmsCategory($objectData["categories"]);
            $this->extendedParams["categories"]["body"] = $entityCmsCategory->getParams();
        }
        if(isExisting('images', $objectData)){
            $entityImage = new EntityImage($objectData["images"]);

            $this->extendedParams["images"]["body"]["head"] = ["type" => "Array", "body" => $entityImage->getParams(true)];
            $this->extendedParams["images"]["body"]["other"] = ["type" => "Array", "body" => $entityImage->getParams(true)];
            $this->extendedParams["images"]["body"]["all"] = ["type" => "Array", "body" => $entityImage->getParams(true)];
        }
        if(isExisting('files', $objectData)){
            $entityFile = new EntityFile($objectData["files"]);
            $this->extendedParams["files"]["body"] = $entityFile->getParams();
        }
        if(isExisting('contentBlocks', $objectData)){
            $entityBlock = new EntityBlock($objectData["contentBlocks"]);
            $this->extendedParams["contentBlocks"] = ["type" => "Array", "body" => $entityBlock->getParams(true)];
        }
        if(isExisting('author', $objectData)){
            $entityAdmin = new EntityAuthor($objectData["author"]);
            $this->extendedParams["author"] = ["type" => "Array", "body" => $entityAdmin->getParams(true)];
        }
        if(isExisting('labels', $objectData)){
            $this->extendedParams["labels"]["type"] = "Array";
            $this->extendedParams["labels"]["body"] = [
                "id" => ["type" => "Integer"],
                "title" => ["type" => "String"],
                "shortTag" => ["type" => "String"],
                "values" => ["type" => "Array",
                    "body" => [
                        "id" => ["type" => "Integer"],
                        "title" => ["type" => "String"],
                        "shortTag" => ["type" => "String"],
                        "color" => ["type" => "String"],
                        "anchor" => ["type" => "Array | String"],
                    ]
                ],
            ];
        }
    }

}