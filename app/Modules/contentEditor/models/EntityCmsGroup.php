<?php
class EntityCmsGroup extends Entity{

    protected $extendedParams = [
        "title" => [
            "type" => "String",
        ],
        "shortDesc" => [
            "type" => "String",
        ],
        "description" => [
            "type" => "String",
        ],
        "parentId" => [
            "type" => "Integer",
        ],
        "path" => [
            "type" => "String",
        ],
        "templatePath" => [
            "type" => "String",
        ],
        "className" => [
            "type" => "String",
        ],
        "shortTag" => [
            "type" => "String",
        ],
        "images" => [
            "type" => "Array",
        ],
        "groups" => [
            "type" => "Array",
        ],
    ];

    public function __construct(Array $objectData) {
        $this->extendEntity($objectData);
        $this->build($objectData, $this->extendedParams);
    }
    private function extendEntity(Array $objectData) {
        if(isExisting('groups', $objectData)){
            $entityCmsGroup = new EntityCmsGroup($objectData["groups"]);
            $this->extendedParams["groups"]["body"] = $entityCmsGroup->getParams();
        }
        if(isExisting('images', $objectData)){
            $entityImage = new EntityImage($objectData["images"]);

            $this->extendedParams["images"]["body"]["head"] = ["type" => "Array", "body" => $entityImage->getParams(true)];
            $this->extendedParams["images"]["body"]["other"] = ["type" => "Array", "body" => $entityImage->getParams(true)];
            $this->extendedParams["images"]["body"]["all"] = ["type" => "Array", "body" => $entityImage->getParams(true)];
        }
        if(isExisting('contentBlocks', $objectData)){
            $entityBlock = new EntityBlock($objectData["contentBlocks"]);
            $this->extendedParams["contentBlocks"] = ["type" => "Array", "body" => $entityBlock->getParams(true)];
        }
    }

}