<?php
/* =============================== CONTROLLER METHODS ======== */
$moduleCMS = new CMS($database, $appController);
$cmsGroups = new cmsGroups($database, $appController);

$langShort = $presenter->templateData->langShort;
$categoryTable = $cmsGroups->groupsTable;
$prevPage = $dataCompiler->modulePageUrl(10);

$mediaInfo["name"] = $modulesController->getModuleMediaName($cmsGroups->mediaNameGroup);
$modelName = 'groups';

// ACCESS CONTROL
$pageId = ( isset($_GET["id"]) ) ? $_GET["id"] : 0;
$pageAction = ( isset($_GET["action"]) ) ? $_GET["action"] : null;
if( $pageAction === null OR ($pageAction == "edit" && $pageId == 0) OR ($pageAction == "add" && $pageId != 0) OR ($pageAction == "add" && $presenter->activeContent->enableTranslate === true) ){
  $dataCompiler->setContinue(true);
  $dataCompiler->redirectPage(0, $prevPage);
}

$parentId = ( isset($_GET["parent"]) ) ? $_GET["parent"] : 0;
if( $parentId != 0 ){
  $prevPage = $prevPage."?parent=".$parentId;
}

/* =============================== LANGUAGE ADD-ON METHODS ======== */
$pageData =  $cmsGroups->getOneGroupById($pageId, $langShort);

$pageTitle = ( $pageAction == "add" ) ? translate("mod-contentEditor-groups-detail-add") : translate("mod-contentEditor-groups-detail-edit").': <b>'.$pageData["title"].'</b>';
$previewLink = ($pageData && $pageData["templateName"]) ? $render->getPreviewPath($pageData["templateName"], $pageData["url"]) : null;

if( isset($_POST['submit']) ){
    $moduleCompiler = new \Compiler\contentEditor\DataFactory($database, $appController, $dataCompiler);
    $moduleCompiler->buildGroup($_POST, $pageId, $cmsGroups, $pageData);
    $dataCompiler = $moduleCompiler->compileGroup($cmsGroups, $langShort, $presenter->activeContent->enableTranslate, $pageData);
    $pageId = $moduleCompiler->getObjectId();
}

$dataCompiler->showMessage();
$dataCompiler->refreshPage($dataCompiler->nextStep, ($pageAction == "add" && $pageId != 0) ? $presenter->activeContent->page.'?action=edit&id='.$pageId.'&parent='.$parentId : "");

/* =============================== CONTENT ======== */ 
echo '<article class="module">';
    $render->header($pageTitle, $prevPage, null, $previewLink);
    echo '<section>';
        echo '<form action="" method="POST" enctype="multipart/form-data">';
            echo $render->button("submit", "true", translate("save"), false, null, "btn icon save success");

            echo '<div class="moduleWrap">';
                $render->contentDiv(true, null, "optionBox");
                    $render->contentDiv(true, null, "boxOption");
                        $render->boxTab("templates", "basicInfo", "mod-templates-admin-tabs-basic", "basic", "tab active", false);
                        $render->boxTab("contentEditor", "groups", "mod-contentEditor-posts-tabs-groups", "postGroupInfo", "tab", false);
                        $render->boxTab("mediaManager", "images", "mod-mediaManager-admin-tabs-files", "images", "tab", false);
                        $render->boxTab("templates", "seo", "mod-templates-admin-tabs-seo", "seo", "tab", false);
                        $render->boxTab("contentBlocks", "pageBuilder", "mod-contentBlocks-admin-tabs-pageBuilder", "pageBuilder", "tab", false);
                    $render->contentDiv(false);
                $render->contentDiv(false);

                $render->contentDiv(true, null, "contentBox");
                    foreach($render->moduleBoxes as $moduleBox => $boxPath){
                        include($boxPath);
                    }
                $render->contentDiv(false);
            echo '</div>';

            echo $render->button("submit", "true", translate("save"), false, null, "btn icon save success");
        echo '</form>';
    echo '</section>';
echo '</article>';