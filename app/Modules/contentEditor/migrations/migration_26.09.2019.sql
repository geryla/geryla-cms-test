-- CREATE TABLE for CONTENT HOOKS
CREATE TABLE IF NOT EXISTS `contenteditor-hooks` (
  `id` int(10) NOT NULL AUTO_INCREMENT,

  `title` varchar(70) NOT NULL,
  `contentType` int(1) NOT NULL DEFAULT '1',
  `limit` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- CREATE TABLE for BONDS for content
CREATE TABLE IF NOT EXISTS `contenteditor-bonds` (
  `id` int(10) NOT NULL AUTO_INCREMENT,

  `hookId` int(10) NOT NULL,
  `parentId` int(10) NOT NULL,
  `objectId` int(10) NOT NULL,
  `contentType` int(11) NOT NULL DEFAULT '1',
  `dataTable` varchar(255) NOT NULL DEFAULT '0',

  `orderNum` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `hookId` (`hookId`),
  FOREIGN KEY (`hookId`) REFERENCES `contenteditor-hooks` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- CREATE TABLE for POSTS
CREATE TABLE IF NOT EXISTS `contenteditor-posts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,

  `urlPrefix` int(11) NOT NULL,
  `postType` int(10) NOT NULL DEFAULT '1',
  `autorId` int(10) NOT NULL DEFAULT '0',
  `autoEnable` datetime NOT NULL,
  `allowIndex` int(1) NOT NULL DEFAULT '1',
  `pageSidebar` varchar(40) NOT NULL,
  `className` varchar(50) NOT NULL,

  `createdDate` datetime NOT NULL,
  `updatedDate` datetime NOT NULL,
  `orderNum` int(10) NOT NULL DEFAULT '0',
  `visibility` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `urlPrefix` (`urlPrefix`),
  FOREIGN KEY (`urlPrefix`) REFERENCES `prefixs` (`urlPrefix`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- CREATE TABLE for POSTS LANGUAGE
CREATE TABLE IF NOT EXISTS `contenteditor-posts-lang` (
  `id` int(10) NOT NULL AUTO_INCREMENT,

  `associatedId` int(10) NOT NULL,
  `title` varchar(75) NOT NULL,
  `shortDesc` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `metaKeywords` varchar(255) NOT NULL,
  `metaTitle` varchar(255) NOT NULL,
  `metaDescription` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,

  `langShort` varchar(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `associatedId` (`associatedId`),
  FOREIGN KEY (`associatedId`) REFERENCES `contenteditor-posts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- CREATE TABLE for GROUPS
CREATE TABLE IF NOT EXISTS `contenteditor-groups` (
  `id` int(10) NOT NULL AUTO_INCREMENT,

  `parentId` int(10) NOT NULL,
  `title` varchar(75) NOT NULL,
  `shortDesc` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `metaKeywords` varchar(255) NOT NULL,
  `metaTitle` varchar(255) NOT NULL,
  `metaDescription` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `allowIndex` int(1) NOT NULL DEFAULT '1',
  `shortTag` varchar(50) NOT NULL,
  `className` varchar(150) NOT NULL,
  `templateName` varchar(40) NOT NULL,

  `createdDate` datetime NOT NULL,
  `updatedDate` datetime NOT NULL,
  `orderNum` int(10) NOT NULL DEFAULT '0',
  `visibility` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- CREATE TABLE for GROUPS connects
CREATE TABLE IF NOT EXISTS `contenteditor-postgroups` (
  `id` int(10) NOT NULL AUTO_INCREMENT,

  `postId` int(10) NOT NULL,
  `groupId` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `postId` (`postId`),
  KEY `groupId` (`groupId`),
  FOREIGN KEY (`postId`) REFERENCES `contenteditor-posts` (`id`),
  FOREIGN KEY (`groupId`) REFERENCES `contenteditor-groups` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
-- CREATE TABLE for CATEGORIES
CREATE TABLE IF NOT EXISTS `contenteditor-categories` (
  `id` int(10) NOT NULL AUTO_INCREMENT,

  `parentId` int(10) NOT NULL,
  `title` varchar(75) NOT NULL,
  `shortDesc` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `metaKeywords` varchar(255) NOT NULL,
  `metaTitle` varchar(255) NOT NULL,
  `metaDescription` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `allowIndex` int(1) NOT NULL DEFAULT '1',
  `className` varchar(150) NOT NULL,

  `createdDate` datetime NOT NULL,
  `updatedDate` datetime NOT NULL,
  `orderNum` int(10) NOT NULL DEFAULT '0',
  `visibility` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- CREATE TABLE for CATEGORIES connects
CREATE TABLE IF NOT EXISTS `contenteditor-postcategories` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `postId` int(10) NOT NULL,
  `categoryId` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `postId` (`postId`),
  KEY `categoryId` (`categoryId`),
  FOREIGN KEY (`postId`) REFERENCES `contenteditor-posts` (`id`),
  FOREIGN KEY (`categoryId`) REFERENCES `contenteditor-categories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- INSERT MODULE SETTINGS into TABLE 'settings'
INSERT INTO `settings` (`settName`, `settValue`, `moduleName`, `settGroup`) VALUES ('blog-enabled', 'false', 'contenteditor', '');