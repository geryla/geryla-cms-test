SET FOREIGN_KEY_CHECKS=0;

-- DELETE TABLES
DROP TABLE `contenteditor-hooks`;
DROP TABLE `contenteditor-bonds`;
DROP TABLE `contenteditor-posts`;
DROP TABLE `contenteditor-posts-lang`;
DROP TABLE `contenteditor-groups`;
DROP TABLE `contenteditor-postgroups`;
DROP TABLE `contenteditor-categories`;
DROP TABLE `contenteditor-postcategories`;

-- DELETE module SETTINGS
DELETE FROM `settings` WHERE moduleName = 'contenteditor';

-- DELETE OBJECT PREFIXES
DELETE FROM `prefixs` WHERE contentTable = 'contenteditor-posts';

-- DELETE ALL MEDIA by mediaSettings from module CONFIG
DELETE media, lang FROM `mediamanager` AS media INNER JOIN `mediamanager-lang` AS lang WHERE media.template = 'post';
DELETE media, lang FROM `mediamanager` AS media INNER JOIN `mediamanager-lang` AS lang WHERE media.template = 'postCategory';
DELETE media, lang FROM `mediamanager` AS media INNER JOIN `mediamanager-lang` AS lang WHERE media.template = 'postGroup';

-- DELETE ALL TRANSLATION by translateType from module CONFIG
DELETE FROM `language-translations` WHERE objectType = 'postCategory';
DELETE FROM `language-translations` WHERE objectType = 'postGroup';

SET FOREIGN_KEY_CHECKS=1;