CREATE TABLE IF NOT EXISTS `contenteditor-labelsgroups` (
  `id` INT(10) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(40) NOT NULL,
  `shortTag` VARCHAR(40) NOT NULL,

  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `contenteditor-labels` (
  `id` INT(10) NOT NULL AUTO_INCREMENT,

  `groupId` INT(10) NOT NULL,
  `title` VARCHAR(40) NOT NULL,
  `shortTag` VARCHAR(40) NOT NULL,
  `color` VARCHAR(20) NULL,

  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `contenteditor-labelshook` (
  `labelId` INT(10) NOT NULL,
  `postId` INT(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `contenteditor-posts` ADD lengthTime VARCHAR(30) NULL;