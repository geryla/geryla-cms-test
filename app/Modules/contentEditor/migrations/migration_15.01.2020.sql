-- CREATE ANCHORS table for hooks
CREATE TABLE IF NOT EXISTS `contenteditor-anchors` (
  `id` int(10) NOT NULL AUTO_INCREMENT,

  `title` varchar(90) NOT NULL,
  `url` varchar(255) NOT NULL,

  `langShort` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;