<?php
/* =============================== CONTROLLER METHODS ======== */
$moduleCMS = new CMS($database, $appController);
$cmsHook = new cmsHook($database, $appController);

$nextPage = $dataCompiler->modulePageUrl(2);

/* =============================== LANGUAGE ADD-ON METHODS ======== */
$pageData = $cmsHook->getAllHooks($presenter->templateData->langShort);

/* =============================== CONTENT ======== */
echo '<article class="module">';
  
  $render->header(translate("mod-contentEditor-hooks-list"), null);
                                            
  echo '<section>';                                      
    
    echo '<div class="moduleWrap">';             
      echo '<table class="striped">'; 
  	  	$tableHead = [
          translate("mod-contentEditor-hooks-list-title"),
          ""
        ];
        $render->tableHead($tableHead);
        
        foreach($pageData as $hook){
          $langTitle = ($presenter->activeContent->enabledMultilanguage > 1) ? "<i class='activeLang'>(".$hook["activeLang"].")</i> ".$hook["title"] : $hook["title"];
          $langTranslate = ($presenter->activeContent->enabledMultilanguage > 1 && $hook["activeTranslation"] === true) ? true : false;

          echo '<tr>';                  
            $render->tableColumn($langTitle);
            $render->tableActionColumn($nextPage.'?id='.$hook["id"], null, null, 100, $langTranslate);
          echo '</tr>';
        }
              
      echo '</table>';
    echo '</div>'; 
		 
  echo '</section>'; 
      
echo '</article>';
?>