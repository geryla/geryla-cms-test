<?php
/* =============================== FUNCTIONS ======== */
$moduleCMS = new CMS($database, $appController);
$cmsHook = new cmsHook($database, $appController);

$pageData = $settingsController->getModuleSettings($presenter->activeContent->module, true);

if( isset($_POST['submit']) ){
  
  $submitData = [];
  foreach ($_POST as $key => $value) {
    if($key == 'submit'){
      continue;
    }else{
      $submitData[$key] = $value;
    }        
  }

  $editSettings = $settingsController->editSettings($submitData, $presenter->activeContent->module);
     
  if($editSettings === true){           
    $returnMessage = translate("admin-edit-true");
    $systemLogger->createLog($_SESSION["languages"]["adminLang"], $presenter->activeContent->module, "updateSettings", 0, $_SESSION["security"]["adminLogin"]["logIn"]);   
  }else{
    $returnMessage = translate("admin-create-false");
  }

  $this->setContinue(true);
  $this->setMessage($returnMessage);  
}

$dataCompiler->showMessage();
$dataCompiler->refreshPage($dataCompiler->nextStep, "");

/* =============================== CONTENT ======== */ 
echo '<article class="module">';
  
  $render->header(translate("mod-contentEditor-settings-edit-title"), null);
  
  echo '<section>';                                      
    echo '<form action="" method="POST" enctype="multipart/form-data">';  	
   
     echo $render->input("submit", "submit", translate("save"), null, false);
      
      echo '<div class="moduleWrap">';             
        echo '<table class="striped">';           
                                   
          if($appController->isActiveModule("products")){
            $selectValues = [ ["value" => "true", "title" => translate("mod-contentEditor-settings-yes")], ["value" => "false", "title" => translate("mod-contentEditor-settings-no")] ];
            $render->tableRow(translate("mod-contentEditor-settings-active"), $render->select("categoryMenu-enabled", $selectValues, "value", "title", $pageData["categoryMenu-enabled"], true), true);
                     
            $render->tableRow(translate("mod-contentEditor-settings-position"), $render->select("categoryMenu-hookId", $cmsHook->getAllHooks($presenter->templateData->langShort), "id", "title", $pageData["categoryMenu-hookId"], false));
            
            $render->tableRow(translate("mod-contentEditor-settings-order"), $render->input("text", "categoryMenu-order", $pageData["categoryMenu-order"], null, false, null, null));
            
            $render->tableRow(translate("mod-contentEditor-settings-title"), $render->input("text", "categoryMenu-title", $pageData["categoryMenu-title"], null, false, null, null));
            
            $render->tableRow(translate("mod-contentEditor-settings-href").' ('.translate("mod-contentEditor-settings-help").')', $render->input("text", "categoryMenu-href", $pageData["categoryMenu-href"], null, false, null, null));
          }else{
            $render->tableRow(translate("mod-contentEditor-settings-disableEshop-title"), translate("mod-contentEditor-settings-disableEshop"), true);
          }  

          if($pageData["blog-enabled"] == "true" ){                   
            $selectValues = [ ["value" => "true", "title" => translate("mod-contentEditor-settings-yes")], ["value" => "false", "title" => translate("mod-contentEditor-settings-no")] ];
            $render->tableRow(translate("mod-contentEditor-settings-enableBlog"), $render->select("blogMenu-enabled", $selectValues, "value", "title", $pageData["blogMenu-enabled"], true), true);
                     
            $render->tableRow(translate("mod-contentEditor-settings-position"), $render->select("blogMenu-hookId", $cmsHook->getAllHooks($presenter->templateData->langShort), "id", "title", $pageData["blogMenu-hookId"], false));
            
            $render->tableRow(translate("mod-contentEditor-settings-order"), $render->input("text", "blogMenu-order", $pageData["blogMenu-order"], null, false, null, null));
            
            $render->tableRow(translate("mod-contentEditor-settings-title"), $render->input("text", "blogMenu-title", $pageData["blogMenu-title"], null, false, null, null));
          }else{
            $render->tableRow(translate("mod-contentEditor-settings-disableBlog-title"), translate("mod-contentEditor-settings-disableBlog"), true);
          }

                     
        echo '</table>';            
      echo '</div>'; 
    
     echo $render->input("submit", "submit", translate("save"), null, false);
     
    echo '</form>'; 			 
  echo '</section>';     
echo '</article>';