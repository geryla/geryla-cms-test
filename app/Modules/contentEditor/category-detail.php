<?php
/* =============================== CONTROLLER METHODS ======== */
$moduleCMS = new CMS($database, $appController);
$cmsCategories = new cmsCategories($database, $appController);

$langShort = $presenter->templateData->langShort;
$categoryTable = $cmsCategories->categoriesTable;
$prevPage = $dataCompiler->modulePageUrl(7);

$mediaInfo["name"] = $modulesController->getModuleMediaName($cmsCategories->mediaNameCategory);

// ACCESS CONTROL
$pageId = (int) ((isset($_GET["id"])) ? $_GET["id"] : 0);
$pageAction = ( isset($_GET["action"]) ) ? $_GET["action"] : null;
if( $pageAction === null OR ($pageAction == "edit" && $pageId == 0) OR ($pageAction == "add" && $pageId != 0) OR ($pageAction == "add" && $presenter->activeContent->enableTranslate === true) ){
  $dataCompiler->setContinue(true);
  $dataCompiler->redirectPage(0, $prevPage);
}

$parentId = ( isset($_GET["parent"]) ) ? $_GET["parent"] : 0;
if( $parentId != 0 ){
  $prevPage = $prevPage."?parent=".$parentId;
}

$pageData =  $cmsCategories->getOneCategoryById($pageId, $langShort);
if(!empty($pageData)){
    $optimized = $cmsCategories->optimize($pageData, $langShort);
}

$pageTitle = ( $pageAction == "add" ) ? translate("mod-contentEditor-categories-detail-add") : translate("mod-contentEditor-categories-detail-edit").': <b>'.$pageData["title"].'</b>';
$previewLink = ($pageData) ? $render->getPreviewPath("blog", $pageData["url"]) : null;

if( isset($_POST['submit']) ){

// SetUp variables  
  $continue = true;
  $postData = $_POST;  
  unset($postData['submit']);
  
// Create additional params + check URL
  $postData["url"] = $prefixEngine->title2pagename( (empty($postData["url"])) ? $postData["title"] : $postData["url"] ); 
  $postData["createdDate"] = ($pageId === 0) ? date("Y-m-d H:i:s") : $pageData["createdDate"];  
  $postData["updatedDate"] = date("Y-m-d H:i:s");     
  
  if($pageId != 0){
    $postData["url"] = ($postData["url"] == $pageData["defaultLang"]["url"]) ? $prefixEngine->title2pagename($postData["title"]) : $postData["url"];
  }  
  
// Check Url for possible duplicity
  $postData["url"] = $cmsCategories->checkUrlDuplicity($pageData["id"], $postData["url"]);
  
// Module work (add/edit/translate)    
  $dataCompiler->setTables($cmsCategories->categoriesTable, null);
  $dataCompiler->setData($postData, $cmsCategories->translateRows, $cmsCategories->dataRows);
  $dataCompiler->setTranslate($presenter->activeContent->enableTranslate);
   
  if($pageId == 0){               
    $pageId = $dataCompiler->addQueryAndTranslate(); // Add and translate
  }else{  
    $dataCompiler->setDefaultData($pageData["defaultLang"]["url"]);
    $result = $dataCompiler->updateQueryAndTranslate($pageId, $cmsCategories->translateType); // Update and translate
  }  
  
  $dataCompiler->updateImages($mediaController->translateTable); // Edit images info
   
// Log
  if($continue === true){
    $systemLogger->createLog($langShort, $presenter->activeContent->module, $pageAction."PostCategory", $pageId, $_SESSION["security"]["adminLogin"]["logIn"]);    
  }
  
  $refreshPage = ($continue === true) ? 1 : 0;
}

$dataCompiler->showMessage();
$dataCompiler->refreshPage($dataCompiler->nextStep, ($pageAction == "add" && $pageId != 0) ? $presenter->activeContent->page.'?action=edit&id='.$pageId.'&parent='.$parentId : "");

/* =============================== CONTENT ======== */ 
echo '<article class="module">';
    $render->header($pageTitle, $prevPage, null, $previewLink);
    echo '<section>';
        echo '<form action="" method="POST" enctype="multipart/form-data">';
            echo $render->button("submit", "true", translate("save"), false, null, "btn icon save success");

            echo '<div class="moduleWrap">';
                $render->contentDiv(true, null, "optionBox");
                    $render->contentDiv(true, null, "boxOption");
                        $render->boxTab("templates", "basicInfo", "mod-templates-admin-tabs-basic", "basic", "tab active", false);
                        $render->boxTab("contentEditor", "categories", "mod-contentEditor-posts-tabs-categories", "postCategoryInfo", "tab", false);
                        $render->boxTab("mediaManager", "images", "mod-mediaManager-admin-tabs-files", "images", "tab", false);
                        $render->boxTab("templates", "seo", "mod-templates-admin-tabs-seo", "seo", "tab", false);
                    $render->contentDiv(false);
                $render->contentDiv(false);

                $render->contentDiv(true, null, "contentBox");
                    foreach($render->moduleBoxes as $moduleBox => $boxPath){
                        include($boxPath);
                    }
                $render->contentDiv(false);
            echo '</div>';

            echo $render->button("submit", "true", translate("save"), false, null, "btn icon save success");
        echo '</form>';
    echo '</section>';
echo '</article>';