<?php
/* =============================== CONTROLLER METHODS ======== */
$moduleCMS = new CMS($database, $appController);
$cmsHook = new cmsHook($database, $appController);

$prevPage = $dataCompiler->modulePageUrl(1);

// ACCESS CONTROL
$pageId = ( isset($_GET["id"]) ) ? $_GET["id"] : null;
if($pageId === null){
  $dataCompiler->setContinue(true);
  $dataCompiler->redirectPage(0, $prevPage);
}


$cmsSettings = $settingsController->getModuleSettings($presenter->activeContent->module, true);

/* =============================== LANGUAGE ADD-ON METHODS ======== */
$pageData = $cmsHook->getOneHook($pageId, $presenter->templateData->langShort);

if( isset($_POST['submit']) && !empty($_POST['title']) ){

    $dataCompiler->setTables($cmsHook->hooksTable, null);
    $dataCompiler->setData(["title" => $_POST['title'], "limit" => 0, "id" => $pageId], ["title"], ["id"]);
    $dataCompiler->setTranslate($presenter->activeContent->enableTranslate);

    $result = $dataCompiler->updateQueryAndTranslate($pageId, $cmsHook->translateType);

  if($result === true){                                                 
    $returnMessage = translate("admin-edit-true");
    $systemLogger->createLog($presenter->templateData->langShort, $presenter->activeContent->module, "editHookTitle", $pageId, $_SESSION["security"]["adminLogin"]["logIn"]);   
  }else{
    $returnMessage = translate("admin-create-false");
  }

  $dataCompiler->setContinue(true);
  $dataCompiler->setMessage($returnMessage);
}

$dataCompiler->showMessage();
$dataCompiler->refreshPage($dataCompiler->nextStep, "");

/* =============================== CONTENT ======== */
echo '<article class="module">';
  
  $render->header(translate("mod-contentEditor-hooks-edit"), $prevPage);
                                            
  echo '<section>';                                      
    echo '<form action="" id="hookForm" method="POST" enctype="multipart/form-data">'; 

      echo $render->button("submit", "true", translate("save"), false, null, "btn icon save success");

      echo '<div class="moduleWrap">';             
        $render->contentDiv(true, null, "contentBox");   
        
          $render->contentRow(null, "rowBox", '<h3>'.translate("mod-contentEditor-hooks-list-title").'</h3>', null, $render->input("text", "title", $pageData["title"], null, true, null, null) );
          echo '<br>';  
          
          echo '<h3>'.translate("mod-contentEditor-hooks-edit-content").'</h3>';
          $render->contentDiv(true, null, "menuSelector");

            // TEMPLATES table
            $selectValueTable = databaseTables::getTable("templates", "templateTableLang");
            echo '<h4>'.translate("mod-contentEditor-hooks-edit-content-template").'</h4>';
            echo '<span class="selDiv">';
              echo '<select name="'.$selectValueTable.'">';
                echo '<option value="0">'.translate("mod-contentEditor-select-none").'</option>';
                $cmsHook->getOptionsForMenu($selectValueTable, $presenter->templateData->langShort);
              echo '</select>';
            echo '</span>';                             
            
            // GROUPS table
            $selectValueTable = databaseTables::getTable("contentEditor", "groupTable");
            echo '<h4>'.translate("mod-contentEditor-hooks-edit-content-group").'</h4>';
            echo '<span class="selDiv">';
              echo '<select name="'.$selectValueTable.'">';
                echo '<option value="0">'.translate("mod-contentEditor-select-none").'</option>';
                $cmsHook->getOptionsForMenu($selectValueTable, $presenter->templateData->langShort);
              echo '</select>';
            echo '</span>';  
            
            // POSTS table
            $selectValueTable = databaseTables::getTable("contentEditor", "postTableLang");
            echo '<h4>'.translate("mod-contentEditor-hooks-edit-content-post").'</h4>';
            echo '<span class="selDiv">';
              echo '<select name="'.$selectValueTable.'">';
                echo '<option value="0">'.translate("mod-contentEditor-select-none").'</option>';
                $cmsHook->getOptionsForMenu($selectValueTable, $presenter->templateData->langShort);
              echo '</select>';
            echo '</span>';  
            
            // BLOG CATEGORIES table
            if($cmsSettings["blog-enabled"] == "true"){
              $selectValueTable = databaseTables::getTable("contentEditor", "categoryTable");
              echo '<h4>'.translate("mod-contentEditor-hooks-edit-content-blogCategory").'</h4>';
              echo '<span class="selDiv">';
                echo '<select name="'.$selectValueTable.'">';
                  echo '<option value="0">'.translate("mod-contentEditor-select-none").'</option>';
                  $cmsHook->getOptionsForMenu($selectValueTable, $presenter->templateData->langShort);
                echo '</select>';
              echo '</span>'; 
            }

            // MARKETING table
            $selectValueTable = databaseTables::getTable("marketing", "pages");
            echo '<h4>'.translate("mod-contentEditor-hooks-edit-content-marketingPage").'</h4>';
            echo '<span class="selDiv">';
                echo '<select name="'.$selectValueTable.'">';
                    echo '<option value="0">'.translate("mod-contentEditor-select-none").'</option>';
                    $cmsHook->getOptionsForMenu($selectValueTable, $presenter->templateData->langShort);
                echo '</select>';
            echo '</span>';


            // PRODUCTS CATEGORIES table
            if($appController->isActiveModule("products")){
                $selectValueTable = databaseTables::getTable("products", "categoryTableLang");
                echo '<h4>'.translate("mod-contentEditor-hooks-edit-content-productCategory").'</h4>';
                echo '<span class="selDiv">';
                    echo '<select name="'.$selectValueTable.'">';
                        echo '<option value="0">'.translate("mod-contentEditor-select-none").'</option>';
                        $cmsHook->getOptionsForMenu($selectValueTable, $presenter->templateData->langShort);
                    echo '</select>';
                echo '</span>';
            }

            // PRODUCTS CATEGORIES table
            $selectValueTable = databaseTables::getTable("contentEditor", "anchorsTable");
            echo '<h4>'.translate("mod-contentEditor-hooks-edit-content-customAnchor").'</h4>';
            echo '<span class="selDiv">';
                echo '<input type="text" name="'.$selectValueTable.'[title]" placeholder="'.translate("mod-contentEditor-hooks-plc-title").'" class="customAnchor" />';
                echo '<input type="text" name="'.$selectValueTable.'[url]" placeholder="'.translate("mod-contentEditor-hooks-plc-url").'" />';
            echo '</span>';



echo '<br>';
            
            echo '<span class="selDiv">';
              echo '<input type="hidden" name="hookId" value="'.$pageId.'">';
              echo '<span id="addMenu" class="btn inverted" >'.translate("add").'</span>';
            echo '</span>';
          $render->contentDiv(false);
          
      		$render->contentDiv(true, null, "menuListBox");           
            echo '<div class="dd menu_listed menuListAll" id="nestable2">';
              echo '<ol class="dd-list">';
                echo $cmsHook->getHookContentSortable($pageId, 0, 0);
              echo '</ol>';
            echo '</div>';         
    	    $render->contentDiv(false);       
  
        $render->contentDiv(false);
      echo '</div>'; 

      echo $render->button("submit", "true", translate("save"), false, null, "btn icon save success");
      
    echo '</form>';  			 
  echo '</section>'; 
      
echo '</article>';