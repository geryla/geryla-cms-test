$(window).load(function() {

/* ===== CHANGE SELECTED CONTENT IN HOOK === */
  $(document).on('change', '.menuSelector select', function(e) {   
    var selectedOption = $(this).find('option:selected');
    $('.menuSelector select').find('option').prop("selected", false);
    $('.menuSelector').find('input[type=text]').val('');
    $(selectedOption).prop("selected", true);
  });

/* ===== ADD MENU TO HOOK === */
$('#addMenu').click( function() {
    $.ajax({
        url: requestsUrl,
        type: 'POST',
        data: {requestName: 'addMenu', module: 'contentEditor', response: 'dom', data: {items: $('#hookForm').serialize()}},
        success: function(data) {
            $('.menuListAll').html(data);
            $('.menuSelector').find('input[type=text]').val('');
            $('.menuSelector select').find('option').prop("selected", false);
        }
    });
}); 

/* ===== SERIALIZE HOOK MENU === */
var updateOutput = function(e){
  var out = $('#nestable2').nestable('serialize');       
  var array = jQuery('#nestable2 li').map(function(){
      return this.id.match(/(\d+)$/)[1]
  }).get();

    $.ajax({
        url: requestsUrl,
        type: 'POST',
        data: {requestName: 'sortMenu', module: 'contentEditor', response: 'dom', data: {sorted:out, sortedList:array}},
        success: function(data) {
            $(".listed").html(data);
        }
    });
};
$('#nestable2').nestable({group: 5}).on('change', updateOutput);

});