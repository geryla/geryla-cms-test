<?php
    $config = [
        "moduleName" => "contentEditor",
        "moduleUrlName" => "contentEditor",
        "moduleLangName" => "contentEditor",
        "moduleIcon" => "fa-files-o",
        "moduleVersion" => "4.3.0",
        "requiredModules" => [
            ["moduleName" => "templates"],
            ["moduleName" => "localization"],
            ["moduleName" => "settings"],
            ["moduleName" => "mediaManager"],
        ],

        "mediaSettings" => [
            "post" => "post",
            "postCategory" => "postCategory",
            "postGroup" => "postGroup"
        ],
        "translateType" => [
            "postCategory" => "postCategory",
            "postGroup" => "postGroup",
            "postHook" => "postHook",
            "postLabel" => "postLabel",
            "postLabelGroup" => "postLabelGroup",
        ],
        "databaseTables" => [
            "postTable" => [
                "name" => "contenteditor-posts",
                "entityBonds" => [
                    "post" => ["major" => true],
                ]
            ],
            "postTableLang" => [
                "name" => "contenteditor-posts-lang",
                "entityBonds" => [
                    "post" => ["major" => true],
                ]
            ],
            "categoryTable" => [
                "name" => "contenteditor-categories",
            ],
            "crossCategoryTable" => [
                "name" => "contenteditor-postcategories",
            ],
            "groupTable" => [
                "name" => "contenteditor-groups",
            ],
            "crossGroupTable" => [
                "name" => "contenteditor-postgroups",
            ],
            "hookTable" => [
                "name" => "contenteditor-hooks",
            ],
            "bondTable" => [
                "name" => "contenteditor-bonds",
            ],
            "anchorsTable" => [
                "name" => "contenteditor-anchors",
            ],
            "labelsGroupsTable" => [
                "name" => "contenteditor-labelsgroups",
            ],
            "labelsTable" => [
                "name" => "contenteditor-labels",
            ],
            "labelsHookTable" => [
                "name" => "contenteditor-labelshook",
            ],
        ],

        "modulePages" => [
            [
                "pageId" => 1,
                "urlName" => "content-hooks",
                "url" => "content-hooks",
                "parent" => 0,
                "inMenu" => 1,
                "headPage" => 1
            ],
            [
                "pageId" => 2,
                "urlName" => "edit-hook",
                "url" => "edit-hook",
                "parent" => 1,
                "inMenu" => 0,
                "headPage" => 0
            ],
            [
                "pageId" => 3,
                "urlName" => "posts",
                "url" => "posts",
                "parent" => 1,
                "inMenu" => 1,
                "headPage" => 0
            ],
            [
                "pageId" => 4,
                "urlName" => "post-detail",
                "url" => "post-detail",
                "parent" => 3,
                "inMenu" => 0,
                "headPage" => 0
            ],
            [
                "pageId" => 5,
                "urlName" => "blogPosts",
                "url" => "blogPosts",
                "parent" => 1,
                "inMenu" => 1,
                "headPage" => 0
            ],
            [
                "pageId" => 6,
                "urlName" => "blogPost-detail",
                "url" => "blogPost-detail",
                "parent" => 5,
                "inMenu" => 0,
                "headPage" => 0
            ],
            [
                "pageId" => 7,
                "urlName" => "categories",
                "url" => "categories",
                "parent" => 1,
                "inMenu" => 1,
                "headPage" => 0
            ],
            [
                "pageId" => 8,
                "urlName" => "category-detail",
                "url" => "category-detail",
                "parent" => 7,
                "inMenu" => 0,
                "headPage" => 0
            ],
            [
                "pageId" => 9,
                "urlName" => "blog-label",
                "url" => "blog-label",
                "parent" => 1,
                "inMenu" => 1,
                "headPage" => 0
            ],
            [
                "pageId" => 10,
                "urlName" => "groups",
                "url" => "groups",
                "parent" => 1,
                "inMenu" => 1,
                "headPage" => 0
            ],
            [
                "pageId" => 11,
                "urlName" => "group-detail",
                "url" => "group-detail",
                "parent" => 10,
                "inMenu" => 0,
                "headPage" => 0
            ],
        ],
    ];