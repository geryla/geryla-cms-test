<?php
/* =============================== FUNCTIONS ======== */
$moduleCMS = new CMS($database, $appController);
$cmsPost = new cmsPost($database, $appController);
$cmsGroups = new cmsGroups($database, $appController);

$langShort = $presenter->templateData->langShort;

$postTable = $cmsPost->postsTable;
$templatesPostsTable = $cmsPost->postsTranslateTable;
$prevPage = $dataCompiler->modulePageUrl(3);

$mediaInfo["name"] = $modulesController->getModuleMediaName($cmsPost->mediaNamePost);

// ACCESS CONTROL
$pageId = ( isset($_GET["id"]) ) ? $_GET["id"] : 0;
$pageAction = ( isset($_GET["action"]) ) ? $_GET["action"] : null;
if( $pageAction === null OR ($pageAction == "edit" && $pageId == 0) OR ($pageAction == "add" && $pageId != 0) ){
  $dataCompiler->setContinue(true);
  $dataCompiler->redirectPage(0, $prevPage);
}

/* =============================== LANGUAGE ADD-ON METHODS ======== */
$pageData = $cmsPost->getOnePost($pageId, $langShort);
$pageTitle = ( $pageAction == "add" ) ? translate("mod-contentEditor-posts-detail-add") : translate("mod-contentEditor-posts-detail-edit").': <b>'.$pageData["title"].'</b>';
if($pageData){
    $previewLink = $render->getPreviewPath("page", $pageData["url"], $pageData["urlPrefix"]);
}
if(!empty($pageData)){
    $optimized = $cmsPost->optimize($pageData, $langShort);
}

if( isset($_POST['submit']) ){
    $moduleCompiler = new \Compiler\contentEditor\DataFactory($database, $appController, $dataCompiler);
    $moduleCompiler->buildPost($_POST, $pageId, $langShort, $cmsPost, $pageData);
    $dataCompiler = $moduleCompiler->compilePost($cmsPost, $langShort, $presenter->activeContent->enableTranslate);
    $pageId = $moduleCompiler->getObjectId();
}

$dataCompiler->showMessage();
$dataCompiler->refreshPage($dataCompiler->nextStep, ($pageAction == "add" && $pageId != 0) ? $presenter->activeContent->page.'?action=edit&id='.$pageId : "");

/* =============================== CONTENT ======== */
echo '<article class="module">';
    $render->header($pageTitle, $prevPage, null, $previewLink);
    echo '<section>';
        echo '<form action="" method="POST" enctype="multipart/form-data">';
            echo $render->button("submit", "true", translate("save"), false, null, "btn icon save success");

            echo '<div class="moduleWrap">';
                $render->contentDiv(true, null, "optionBox");
                    $render->contentDiv(true, null, "boxOption");
                        $render->boxTab("templates", "basicInfo", "mod-templates-admin-tabs-basic", "basic", "tab active", false);
                        $render->boxTab("contentEditor", "postInfo", "mod-contentEditor-posts-tabs-postInfo", "postInfo", "tab", false);
                        $render->boxTab("mediaManager", "images", "mod-mediaManager-admin-tabs-files", "images", "tab", false);
                        $render->boxTab("templates", "seo", "mod-templates-admin-tabs-seo", "seo", "tab", false);
                    $render->contentDiv(false);
                $render->contentDiv(false);

                $render->contentDiv(true, null, "contentBox");
                    foreach($render->moduleBoxes as $moduleBox => $boxPath){
                        include($boxPath);
                    }
                $render->contentDiv(false);
            echo '</div>';

            echo $render->button("submit", "true", translate("save"), false, null, "btn icon save success");
        echo '</form>';
    echo '</section>';
echo '</article>';