<?php
/* =============================== FUNCTIONS ======== */
$moduleCMS = new CMS($database, $appController);
$cmsPostBlog = new cmsPostBlog($database, $appController);
$cmsCategories = new cmsCategories($database, $appController);

$langShort = $presenter->templateData->langShort;

$postTable = $cmsPostBlog->postsTable;
$templatesPostsTable = $cmsPostBlog->postsTranslateTable;
$prevPage = $dataCompiler->modulePageUrl(5);

$mediaInfo["name"] = $modulesController->getModuleMediaName($cmsPostBlog->mediaNamePost);
$modelName = 'posts';

// ACCESS CONTROL
$pageId = ( isset($_GET["id"]) ) ? $_GET["id"] : 0;
$pageAction = ( isset($_GET["action"]) ) ? $_GET["action"] : null;
if( $pageAction === null OR ($pageAction == "edit" && $pageId == 0) OR ($pageAction == "add" && $pageId != 0) ){
  $dataCompiler->setContinue(true);
  $dataCompiler->redirectPage(0, $prevPage);
}

/* =============================== LANGUAGE ADD-ON METHODS ======== */
$pageData = $cmsPostBlog->getOnePost($pageId, $langShort);
$pageTitle = ( $pageAction == "add" ) ? translate("mod-contentEditor-posts-detail-add") : translate("mod-contentEditor-posts-detail-edit").': <b>'.$pageData["title"].'</b>';
if($pageData){
    $previewLink = $render->getPreviewPath("blog", $pageData["url"], $pageData["urlPrefix"]);
}
if(!empty($pageData)){
    $optimized = $cmsPostBlog->optimize($pageData, $langShort);
}

if( isset($_POST['submit']) ){
    $moduleCompiler = new \Compiler\contentEditor\DataFactory($database, $appController, $dataCompiler);
    $moduleCompiler->buildBlogPost($_POST, $pageId, $langShort, $cmsPostBlog, $pageData);
    $dataCompiler = $moduleCompiler->compileBlogPost($cmsPostBlog, $langShort, $presenter->activeContent->enableTranslate);
    $pageId = $moduleCompiler->getObjectId();
}

$dataCompiler->showMessage();
$dataCompiler->refreshPage($dataCompiler->nextStep, ($pageAction == "add" && $pageId != 0) ? $presenter->activeContent->page.'?action=edit&id='.$pageId : "");

/* =============================== CONTENT ======== */
echo '<article class="module">';
    $render->header($pageTitle, $prevPage, null, $previewLink);
    echo '<section>';
        echo '<form action="" method="POST" enctype="multipart/form-data">';
            echo $render->button("submit", "true", translate("save"), false, null, "btn icon save success");

            echo '<div class="moduleWrap">';
                $render->contentDiv(true, null, "optionBox");
                    $render->contentDiv(true, null, "boxOption");
                        $render->boxTab("templates", "basicInfo", "mod-templates-admin-tabs-basic", "basic", "tab active", false);
                        $render->boxTab("contentEditor", "blogPostInfo", "mod-contentEditor-posts-tabs-postInfo", "blogPostInfo", "tab", false);
                        $render->boxTab("mediaManager", "images", "mod-mediaManager-admin-tabs-files", "images", "tab", false);
                        $render->boxTab("templates", "seo", "mod-templates-admin-tabs-seo", "seo", "tab", false);
                        $render->boxTab("contentBlocks", "pageBuilder", "mod-contentBlocks-admin-tabs-pageBuilder", "pageBuilder", "tab", false);
                    $render->contentDiv(false);
                $render->contentDiv(false);

                $render->contentDiv(true, null, "contentBox");
                    foreach($render->moduleBoxes as $moduleBox => $boxPath){
                        include($boxPath);
                    }
                $render->contentDiv(false);
            echo '</div>';

            echo $render->button("submit", "true", translate("save"), false, null, "btn icon save success");
        echo '</form>';
    echo '</section>';
echo '</article>';