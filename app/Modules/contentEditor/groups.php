<?php
/* =============================== CONTROLLER METHODS ======== */
$moduleCMS = new CMS($database, $appController);
$cmsGroups = new cmsGroups($database, $appController);

$langShort = $presenter->templateData->langShort;

$groupTable = $cmsGroups->groupsTable;

$nextPage = $dataCompiler->modulePageUrl(11);
$nextPageAdd = $nextPage.'?action=add';
$prevPage = null;

$parentId = ( isset($_GET["parent"]) ) ? $_GET["parent"] : 0;
$pageTitle = translate("mod-contentEditor-groups-list-title");

$pageData = $cmsGroups->getAllChildGroups($parentId, $langShort, null, 'ORDER BY templateName DESC, orderNum ASC');
$parentInfo = $cmsGroups->getOneGroupById($parentId, $langShort);

if( $parentId != 0 && !empty($parentInfo) ){
  $prevPage = $dataCompiler->modulePageUrl(10)."?parent=".$parentInfo["parentId"];
  $nextPageAdd = $nextPageAdd.'&parent='.$parentId;
  $pageTitle = "<b>".$parentInfo["title"]."</b>: ".$pageTitle;
}
/* =============================== CONTENT ======== */ 
echo '<article class="module">';
    $render->header($pageTitle, $prevPage, ($presenter->activeContent->enableTranslate === true) ? null : $nextPageAdd); // ADD only for default lang
    echo '<section>';
        echo '<div class="moduleWrap">';
            echo '<table class="striped">';

                $tableHead = [
                translate("mod-contentEditor-groups-categoryName"),
                translate("mod-contentEditor-groups-categoryParent"),
                translate("mod-contentEditor-groups-categoryChild"),
                translate("mod-contentEditor-groups-visibility"),
                '',
                ''
                ];
                $render->tableHeadSortable($tableHead);

                echo '<tbody class="sortableWrap" data-name="postsGroup">';
                    $functionalGroups = false;
                    foreach($pageData as $index => $group){
                        $parent = $cmsGroups->getOneGroupById($group["parentId"], $langShort);
                        $parentGroup = ( !empty($parent) ) ? $parent["title"] : translate("mod-contentEditor-groups-categoryParent-none");
                        $subGroup = $cmsGroups->getAllChildGroups($group["id"], $langShort);

                        $langTitle = ($presenter->activeContent->enabledMultilanguage > 1) ? "<i class='activeLang'>(".$group["activeLang"].")</i> ".$group["title"] : $group["title"];
                        $langTitle = ($group["templateName"]) ? $langTitle." <i>(".$group["templateName"].")</i>" : $langTitle;
                        $langTranslate = ($presenter->activeContent->enabledMultilanguage > 1 && $group["activeTranslation"] === true) ? true : false;

                        if($index == 0 && $group["templateName"] || (!$group["templateName"] && $functionalGroups === false)){
                            $functionalGroups = ($index == 0 && $group["templateName"]) ? false : true;
                            $groupsAlias = ($index == 0 && $group["templateName"]) ? translate('mod-contentEditor-groups-contentGroups') : translate('mod-contentEditor-groups-functionalGroups') ;

                            echo '<tr class="groupName blue">';
                                echo '<td colspan="7">'.$groupsAlias.'</td>';
                            echo'</tr>';
                        }


                        echo '<tr id="'.$group["id"].'">';
                            $render->tableColumnSortable();
                            $render->tableColumn('<strong>'.$langTitle.'</strong>');
                            $render->tableColumn($parentGroup);
                            $render->tableColumn(count($subGroup));
                            $render->tableColumn($render->visibilitySwitch($group["id"], $cmsGroups->translateType, $group["id"], $group["visibility"]), 80, true);
                            $render->tableColumn('<a href="?parent='.$group["id"].'" class="anchor blue"><i class="fa fa-search" title="'.translate("mod-contentEditor-groups-showChild").'"></i> '.translate("mod-contentEditor-groups-showChild").'</a>', null, null, "actions");

                            $editSwitch = $nextPage.'?action=edit&id='.$group["id"];
                            $editSwitch = ($parentId == 0) ? $editSwitch : $editSwitch."&parent=".$parentId;
                            $deleteSwitch = 'class="deleteContent" name="'.$cmsGroups->translateType.'" data="'.$group["id"].'"';
                            $previewLink = ($group["templateName"]) ? $render->getPreviewPath($group["templateName"], $group["url"]) : null;
                            $render->tableActionColumn($editSwitch, $deleteSwitch, $previewLink, 100, $langTranslate);
                        echo '</tr>';
                    }
                echo '</tbody>';

            echo '</table>';
        echo '</div>';
    echo '</section>';
echo '</article>';
