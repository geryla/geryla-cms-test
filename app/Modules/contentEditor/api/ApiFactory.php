<?php
namespace Api\contentEditor;

class ApiFactory extends \ApiController{
    private $requestMethods;

    private $errorCodes = [
        400 => 'Object/s not found',
        401 => 'Empty data',
    ];

    public function __construct(\Database $database, \AppController $appController, Array $requestData) {
        parent::__construct($database, $appController, $requestData);

        $this->requestMethods = include(__DIR__.'/requests.php');
    }

    public function getErrorCodes(){
        return $this->returnErrorCodes($this->errorCodes);
    }
    public function validate(){
        return $this->validateRequest($this->requestData, $this->requestMethods);
    }
    public function proceedRequest() {

        switch($this->requestData["action"]){
            case "getOnePost":
                $this->getOnePost();
                break;
            case "getAllPosts":
                $this->getAllPosts();
                break;
            case "getOneBlogPost":
                $this->getOneBlogPost();
                break;
            case "getAllBlogPosts":
                $this->getAllBlogPosts();
                break;
            case "getBlogCategories":
                $this->getBlogCategories();
                break;
            case "getOneCategory":
                $this->getOneCategory();
                break;
            case "getBlogPostsFrom":
                $this->getBlogPostsFrom();
                break;
            case "getOneGroup":
                $this->getOneGroup();
                break;
            case "getAllGroups":
                $this->getAllGroups();
                break;
            case "getAllGroupsByPost":
                $this->getAllGroupsByPost();
                break;
            case "getAllChildGroups":
                $this->getAllChildGroups();
                break;
            case "getHookContent":
                $this->getHookContent();
                break;
            default:
                $this->setState(310);
                return false;
        }

        $this->closeAuthRequest();
        return true;
    }

    private function getOnePost(){
        $cmsPosts = new \cmsPost($this->database, $this->appController);

        $responseData = $cmsPosts->getOnePost($this->requestData["value"], $this->appController->getLang());
        if(empty($responseData)){
            $this->setState(400);
            return false;
        }

        $responseData = $cmsPosts->optimize($responseData, $this->appController->getLang(), ["itemThumbnail" => $this->requestParametersValidated["thumbnail"], "with" => $this->requestParametersValidated["with"]]);
        $this->setResponseData($responseData);
        return true;
    }
    private function getAllPosts(){
        $cmsPost = new \cmsPost($this->database, $this->appController);

        if(isExisting('byGroup', $this->requestParametersValidated)){
            $responseData = $cmsPost->getAllPostsByGroup($this->requestParametersValidated["groupId"], $this->appController->getLang(), $this->requestParametersValidated["visibility"], $this->requestParametersValidated["limit"], $this->requestParametersValidated["offset"], 'orderNum', 'DESC');
        }else{
            $responseData = $cmsPost->getAllPosts($this->appController->getLang(), $this->requestParametersValidated["visibility"], $this->requestParametersValidated["limit"], $this->requestParametersValidated["offset"]);
        }

        if(empty($responseData)){
            $this->setState(400);
            return false;
        }

        $responseData = $cmsPost->optimize($responseData, $this->appController->getLang(), ["itemThumbnail" => $this->requestParametersValidated["thumbnail"], "with" => $this->requestParametersValidated["with"]]);
        $this->setResponseData($responseData);
        return true;
    }
    private function getOneBlogPost(){
        $cmsPostsBlog = new \cmsPostBlog($this->database, $this->appController);

        $responseData = $cmsPostsBlog->getOnePost($this->requestData["value"], $this->appController->getLang());
        if(empty($responseData)){
            $this->setState(400);
            return false;
        }

        $responseData = $cmsPostsBlog->optimize($responseData, $this->appController->getLang(), ["itemThumbnail" => $this->requestParametersValidated["thumbnail"], "with" => $this->requestParametersValidated["with"]]);
        $this->setResponseData($responseData);
        return true;
    }
    private function getAllBlogPosts(){
        $cmsPostBlog = new \cmsPostBlog($this->database, $this->appController);

        if(isExisting('byCategory', $this->requestParametersValidated)){
            $responseData = $cmsPostBlog->getAllPostsByCategory($this->requestParametersValidated["byCategory"], $this->appController->getLang(), $this->requestParametersValidated["visibility"], $this->requestParametersValidated["limit"], $this->requestParametersValidated["offset"], 'orderNum', 'DESC', $this->requestParametersValidated["exceptOne"]);
        }else{
            $responseData = $cmsPostBlog->getAllPostsByLang($this->appController->getLang(), $this->requestParametersValidated["visibility"], true, $this->requestParametersValidated["limit"], $this->requestParametersValidated["offset"], $this->requestParametersValidated["exceptOne"]);
        }

        if(empty($responseData)){
            $this->setState(400);
            return false;
        }

        $responseData = $cmsPostBlog->optimize($responseData, $this->appController->getLang(), ["itemThumbnail" => $this->requestParametersValidated["thumbnail"], "with" => $this->requestParametersValidated["with"]]);
        $this->setResponseData($responseData);
        return true;
    }
    private function getAllGroups(){
        $cmsGroups = new \cmsGroups($this->database, $this->appController);

        $responseData = $cmsGroups->getAllGroups($this->appController->getLang(), $this->requestParametersValidated["visibility"]);
        if(empty($responseData)){
            $this->setState(400);
            return false;
        }


        $responseData = $cmsGroups->optimize($responseData, $this->requestParametersValidated["thumbnail"], $this->requestParametersValidated["with"]);
        $this->setResponseData($responseData);
        return true;
    }
    private function getOneGroup(){
        $cmsGroups = new \cmsGroups($this->database, $this->appController);

        if($this->requestData["value"] === 'id'){
            $responseData = $cmsGroups->getOneGroupById($this->requestParametersValidated["value"], $this->appController->getLang(), $this->requestParametersValidated["visibility"]);
        }
        if($this->requestData["value"] === 'shortTag'){
            $responseData = $cmsGroups->getOneGroupByShortTag($this->requestParametersValidated["value"], $this->appController->getLang(), $this->requestParametersValidated["visibility"]);
        }
        if($this->requestData["value"] === 'className'){
            $responseData = $cmsGroups->getOneGroupByClassName($this->requestParametersValidated["value"], $this->appController->getLang(), $this->requestParametersValidated["visibility"]);
        }
        if($this->requestData["value"] === 'url'){
            $responseData = $cmsGroups->getOneGroupByUrl($this->requestParametersValidated["value"], $this->appController->getLang(), $this->requestParametersValidated["visibility"]);
        }

        if(empty($responseData)){
            $this->setState(400);
            return false;
        }

        $responseData = $cmsGroups->optimize($responseData, $this->requestParametersValidated["thumbnail"], $this->requestParametersValidated["with"]);
        $this->setResponseData($responseData);
        return true;
    }
    private function getBlogCategories(){
        $cmsCategories = new \cmsCategories($this->database, $this->appController);

        $responseData = $cmsCategories->getAllCategories($this->appController->getLang(), $this->requestParametersValidated["visibility"], $this->requestParametersValidated["orderBy"]);
        if(empty($responseData)){
            $this->setState(400);
            return false;
        }


        $responseData = $cmsCategories->optimize($responseData, $this->requestParametersValidated["thumbnail"], $this->requestParametersValidated["with"]);
        $this->setResponseData($responseData);
        return true;
    }
    private function getOneCategory(){
        $cmsCategories = new \cmsCategories($this->database, $this->appController);

        $responseData = $cmsCategories->getOneCategoryById($this->requestData["value"], $this->appController->getLang(), $this->requestParametersValidated["visibility"]);
        if(empty($responseData)){
            $this->setState(400);
            return false;
        }

        $responseData = $cmsCategories->optimize($responseData, $this->requestParametersValidated["thumbnail"], $this->requestParametersValidated["with"]);
        $this->setResponseData($responseData);
        return true;
    }
    private function getAllGroupsByPost(){
        $cmsGroups = new \cmsGroups($this->database, $this->appController);

        $responseData = $cmsGroups->getAllGroupsCross($this->requestData["value"], $this->requestParametersValidated["simplify"], $this->appController->getLang(), $this->requestParametersValidated["visibility"]);
        if(empty($responseData)){
            $this->setState(400);
            return false;
        }

        if($this->requestParametersValidated["simplify"] !== true){
            $responseData = $cmsGroups->optimize($responseData, $this->requestParametersValidated["thumbnail"], $this->requestParametersValidated["with"]);
        }

        $this->setResponseData($responseData);
        return true;
    }
    private function getAllChildGroups(){
        $cmsGroups = new \cmsGroups($this->database, $this->appController);

        $responseData = $cmsGroups->getAllChildGroups($this->requestData["value"], $this->appController->getLang(), $this->requestParametersValidated["visibility"]);
        if(empty($responseData)){
            $this->setState(400);
            return false;
        }

        $responseData = $cmsGroups->optimize($responseData, $this->requestParametersValidated["thumbnail"], $this->requestParametersValidated["with"]);

        $this->setResponseData($responseData);
        return true;
    }
    private function getHookContent(){
        $cmsHook = new \cmsHook($this->database, $this->appController);

        $templateData = [];
        if(isset($this->requestParametersValidated["templateUrl"])){
            $templateData["templateUrl"] = $this->requestParametersValidated["templateUrl"];
        }
        if(isset($this->requestParametersValidated["templateId"])){
            $templateData["templateId"] = $this->requestParametersValidated["templateId"];
        }
        if(isset($this->requestParametersValidated["templateName"])){
            $templateData["templateName"] = $this->requestParametersValidated["templateName"];
        }

        $urlPath = null;
        if(isset($this->requestParametersValidated["urlPath"])){
            $urlPath = $this->requestParametersValidated["urlPath"];
        }

        $responseData = $cmsHook->getHookContentTree($this->requestData["value"],0,0, $templateData, $urlPath, $this->requestParametersValidated["thumbnail"]);
        if(empty($responseData)){
            $this->setState(401);
            return false;
        }

        $this->setResponseData($responseData);
        return true;
    }
    private function getBlogPostsFrom(){
        $cmsCategories = new \cmsCategories($this->database, $this->appController);
        $cmsPostBlog = new \cmsPostBlog($this->database, $this->appController);

        $responseData = [];
        foreach($this->requestParametersValidated["categories"] as $categoryId){
            $categoryInfo = $cmsCategories->getOneCategoryById($categoryId, $this->appController->getLang(), true);
            if(!empty($categoryInfo)){
                $categoryInfo = $cmsCategories->optimize($categoryInfo, $this->requestParametersValidated["categoryThumbnail"], $this->requestParametersValidated["categoryWith"]);
                if(in_array('posts', $this->requestParametersValidated["categoryWith"])){
                    $categoryPosts = $cmsPostBlog->getAllPostsByCategory($categoryId,  $this->appController->getLang(), true, $this->requestParametersValidated["postLimit"], null, 'createdDate', 'DESC', null);
                    $categoryPosts = $cmsPostBlog->optimize($categoryPosts, $this->appController->getLang(), ["itemThumbnail" => $this->requestParametersValidated["postThumbnail"], "with" => $this->requestParametersValidated["postWith"]]);
                    $categoryInfo["posts"] = $categoryPosts;
                }
                $responseData[] = $categoryInfo;
            }
        }

        if(empty($responseData)){
            $this->setState(401);
            return false;
        }

        $this->setResponseData($responseData);
        return true;
    }
}