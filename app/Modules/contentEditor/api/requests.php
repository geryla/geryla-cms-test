<?php

return [
    "getOnePost" => [
        "path" => "/contentEditor/getOnePost/{value}",
        "method" => "GET",
        "authorized" => true,
        "cache" => true,
        "value" => [
            "required" => true,
            "type" => 'Integer',
            "description" => "Returns specific item by id"
        ],
        "params" => [
            "with" => [
                "required" => false,
                "default" => [],
                "type" => 'Array',
                "description" => "Returns optimized object with specific extensions",
                "values" => "all, images, categories, files",
            ],
            "thumbnail" => [
                "required" => false,
                "default" => Null,
                "type" => 'String',
                "description" => "Returns the specific compressed and resized images"
            ],
        ]
    ],
    "getAllPosts" => [
        "path" => "/contentEditor/getAllPosts",
        "method" => "GET",
        "authorized" => true,
        "cache" => true,
        "params" => [
            "byGroup" => [
                "required" => false,
                "default" => Null,
                "type" => 'Integer',
                "description" => "Returns only items with a specific group"
            ],
            "visibility" => [
                "required" => false,
                "default" => Null,
                "type" => 'Boolean',
                "description" => "Returns VISIBLE or HIDDEN items"
            ],
            "limit" => [
                "required" => false,
                "default" => Null,
                "type" => 'Integer',
                "description" => "Returns specific count of rows"
            ],
            "offset" => [
                "required" => false,
                "default" => Null,
                "type" => 'Integer',
                "description" => "Skip specific count of rows"
            ],
            "with" => [
                "required" => false,
                "default" => [],
                "type" => 'Array',
                "description" => "Returns optimized object with specific extensions",
                "values" => "all, images, categories, files",
            ],
            "thumbnail" => [
                "required" => false,
                "default" => Null,
                "type" => 'String',
                "description" => "Returns the specific compressed and resized images"
            ],
        ],
    ],

    "getOneCategory" => [
        "path" => "/contentEditor/getOneCategory/{value}",
        "method" => "GET",
        "authorized" => true,
        "cache" => true,
        "value" => [
            "required" => true,
            "type" => 'String',
            "description" => "Returns specific item by id"
        ],
        "params" => [
            "visibility" => [
                "required" => false,
                "default" => Null,
                "type" => 'Boolean',
                "description" => "Returns VISIBLE or HIDDEN items"
            ],
            "with" => [
                "required" => false,
                "default" => [],
                "type" => 'Array',
                "description" => "Returns optimized object with specific extensions",
                "values" => "all, images",
            ],
            "thumbnail" => [
                "required" => false,
                "default" => Null,
                "type" => 'String',
                "description" => "Returns the specific compressed and resized images"
            ],
        ]
    ],
    "getBlogCategories" => [
        "path" => "/contentEditor/getBlogCategories",
        "method" => "GET",
        "authorized" => true,
        "cache" => true,
        "params" => [
            "visibility" => [
                "required" => false,
                "default" => Null,
                "type" => 'Boolean',
                "description" => "Returns VISIBLE or HIDDEN items"
            ],
            "orderBy" => [
                "required" => false,
                "default" => Null,
                "type" => 'String',
                "description" => "Returns sorted results",
                "values" => "id, orderNum, createdDate, updatedDate - ASC/DESC"
            ],
            "with" => [
                "required" => false,
                "default" => [],
                "type" => 'Array',
                "description" => "Returns optimized object with specific extensions",
                "values" => "all, images",
            ],
            "thumbnail" => [
                "required" => false,
                "default" => Null,
                "type" => 'String',
                "description" => "Returns the specific compressed and resized images"
            ],
        ],
    ],
    "getOneBlogPost" => [
        "path" => "/contentEditor/getOneBlogPost/{value}",
        "method" => "GET",
        "authorized" => true,
        "cache" => false,
        "value" => [
            "required" => true,
            "type" => 'Integer',
            "description" => "Returns specific item by id"
        ],
        "params" => [
            "with" => [
                "required" => false,
                "default" => [],
                "type" => 'Array',
                "description" => "Returns optimized object with specific extensions",
                "values" => "all, images, groups, files",
            ],
            "thumbnail" => [
                "required" => false,
                "default" => Null,
                "type" => 'String',
                "description" => "Returns the specific compressed and resized images"
            ],
        ]
    ],
    "getAllBlogPosts" => [
        "path" => "/contentEditor/getAllBlogPosts",
        "method" => "GET",
        "authorized" => true,
        "cache" => false,
        "params" => [
            "byCategory" => [
                "required" => false,
                "default" => Null,
                "type" => 'Integer',
                "description" => "Returns only items with a specific category"
            ],
            "byLang" => [
                "required" => false,
                "default" => Null,
                "type" => 'String',
                "description" => "Returns only items with a specific language shortcode"
            ],
            "exceptOne" => [
                "required" => false,
                "default" => Null,
                "type" => 'Integer',
                "description" => "Returns items except for item with a specific ID"
            ],
            "visibility" => [
                "required" => false,
                "default" => Null,
                "type" => 'Boolean',
                "description" => "Returns VISIBLE or HIDDEN items"
            ],
            "limit" => [
                "required" => false,
                "default" => Null,
                "type" => 'Integer',
                "description" => "Returns specific count of rows"
            ],
            "offset" => [
                "required" => false,
                "default" => Null,
                "type" => 'Integer',
                "description" => "Skip specific count of rows"
            ],
            "with" => [
                "required" => false,
                "default" => [],
                "type" => 'Array',
                "description" => "Returns optimized object with specific extensions",
                "values" => "all, images, categories, files",
            ],
            "thumbnail" => [
                "required" => false,
                "default" => Null,
                "type" => 'String',
                "description" => "Returns the specific compressed and resized images"
            ],
        ],
    ],
    "getBlogPostsFrom" => [
        "path" => "/contentEditor/getBlogPostsFrom",
        "method" => "GET",
        "authorized" => true,
        "cache" => false,
        "params" => [
            "categories" => [
                "required" => true,
                "default" => [],
                "type" => 'Array',
                "description" => "Array of category ids",
            ],
            "categoryWith" => [
                "required" => false,
                "default" => [],
                "type" => 'Array',
                "description" => "Returns optimized object with specific extensions",
                "values" => "all, images, posts",
            ],
            "categoryThumbnail" => [
                "required" => false,
                "default" => Null,
                "type" => 'String',
                "description" => "Returns the specific compressed and resized images"
            ],
            "postWith" => [
                "required" => false,
                "default" => [],
                "type" => 'Array',
                "description" => "In case of posts required, returns optimized object with specific extensions",
                "values" => "all, categories, author, images, files, labels, contentBlocks",
            ],
            "postThumbnail" => [
                "required" => false,
                "default" => Null,
                "type" => 'String',
                "description" => "In case of posts required, returns the specific compressed and resized images"
            ],
            "postLimit" => [
                "required" => false,
                "default" => Null,
                "type" => 'Integer',
                "description" => "In case of posts required, limits posts count"
            ],
        ],
    ],

    "getOneGroup" => [
        "path" => "/contentEditor/getOneGroup/{value}",
        "method" => "GET",
        "authorized" => true,
        "cache" => true,
        "value" => [
            "required" => true,
            "type" => 'String',
            "description" => "Returns specific item by type"
        ],
        "params" => [
            "value" => [
                "required" => true,
                "default" => Null,
                "type" => 'String',
                "description" => "Returns specific item by type value",
                "values" => "id, shortTag, className, url",
            ],
            "visibility" => [
                "required" => false,
                "default" => Null,
                "type" => 'Boolean',
                "description" => "Returns VISIBLE or HIDDEN items"
            ],
            "with" => [
                "required" => false,
                "default" => [],
                "type" => 'Array',
                "description" => "Returns optimized object with specific extensions",
                "values" => "all, images, groups",
            ],
            "thumbnail" => [
                "required" => false,
                "default" => Null,
                "type" => 'String',
                "description" => "Returns the specific compressed and resized images"
            ],
        ]
    ],
    "getAllGroups" => [
        "path" => "/contentEditor/getAllGroups",
        "method" => "GET",
        "authorized" => true,
        "cache" => true,
        "params" => [
            "visibility" => [
                "required" => false,
                "default" => Null,
                "type" => 'Boolean',
                "description" => "Returns VISIBLE or HIDDEN items"
            ],
            "with" => [
                "required" => false,
                "default" => [],
                "type" => 'Array',
                "description" => "Returns optimized object with specific extensions",
                "values" => "all, images, groups",
            ],
            "thumbnail" => [
                "required" => false,
                "default" => Null,
                "type" => 'String',
                "description" => "Returns the specific compressed and resized images"
            ],
        ]
    ],
    "getAllGroupsByPost" => [
        "path" => "/contentEditor/getAllGroupsByPost/{value}",
        "method" => "GET",
        "authorized" => true,
        "cache" => true,
        "value" => [
            "required" => true,
            "type" => 'Integer',
            "description" => "Returns all groups by post id"
        ],
        "params" => [
            "simplify" => [
                "required" => false,
                "default" => True,
                "type" => 'Boolean',
                "description" => "Returns group ids or full object",
            ],
            "visibility" => [
                "required" => false,
                "default" => Null,
                "type" => 'Boolean',
                "description" => "Returns VISIBLE or HIDDEN items"
            ],
            "with" => [
                "required" => false,
                "default" => [],
                "type" => 'Array',
                "description" => "Returns optimized object with specific extensions",
                "values" => "all, images, groups",
            ],
            "thumbnail" => [
                "required" => false,
                "default" => Null,
                "type" => 'String',
                "description" => "Returns the specific compressed and resized images"
            ],
        ]
    ],
    "getAllChildGroups" => [
        "path" => "/contentEditor/getAllChildGroups/{value}",
        "method" => "GET",
        "authorized" => true,
        "cache" => true,
        "value" => [
            "required" => true,
            "type" => 'Integer',
            "description" => "Returns all child groups by parent group id"
        ],
        "params" => [
            "visibility" => [
                "required" => false,
                "default" => Null,
                "type" => 'Boolean',
                "description" => "Returns VISIBLE or HIDDEN items"
            ],
            "with" => [
                "required" => false,
                "default" => [],
                "type" => 'Array',
                "description" => "Returns optimized object with specific extensions",
                "values" => "all, images, groups",
            ],
            "thumbnail" => [
                "required" => false,
                "default" => Null,
                "type" => 'String',
                "description" => "Returns the specific compressed and resized images"
            ],
        ]
    ],

    "getHookContent" => [
        "path" => "/contentEditor/getHookContent/{value}",
        "method" => "GET",
        "authorized" => true,
        "cache" => true,
        "value" => [
            "required" => true,
            "type" => 'Integer',
            "description" => "Returns specific content by hook id"
        ],
        "params" => [
            "templateUrl" => [
                "required" => false,
                "default" => Null,
                "type" => 'String',
                "description" => "Active template URL for active item selector"
            ],
            "templateId" => [
                "required" => false,
                "default" => Null,
                "type" => 'Integer',
                "description" => "Active template ID for active item selector"
            ],
            "templateName" => [
                "required" => false,
                "default" => Null,
                "type" => 'String',
                "description" => "Active template NAME for active item selector"
            ],
            "urlPath" => [
                "required" => false,
                "default" => Null,
                "type" => 'String',
                "description" => "URL path for active item selector"
            ],
            "thumbnail" => [
                "required" => false,
                "default" => Null,
                "type" => 'String',
                "description" => "Returns the specific compressed and resized images"
            ],
        ]
    ]
];