<?php
/* =============================== CONTROLLER METHODS ======== */
$moduleCMS = new CMS($database, $appController);
$cmsPost = new cmsPost($database, $appController);
$cmsGroups = new cmsGroups($database, $appController);

$nextPage = $dataCompiler->modulePageUrl(4);
$langShort = $presenter->templateData->langShort;

$postTable = $cmsPost->postsTable;
$templatesPostsTable = $cmsPost->postsTranslateTable;

$filterToModule->setSortOptions([
    [
        'title' => translate("mod-contentEditor-admin-filtration-sort-group"),
        'type' => 'select',
        'name' => 'group',
        'values' => $cmsGroups->getAllGroupsAsNestedArray(0, $langShort),
    ]
]);
$filterToModule->renderFiltration($_SERVER['REQUEST_URI'], $_GET);
$filterToModule->activeFilters["parameters"]["type"] = '!2'; // Not blog posts
$pageData = $filterToModule->getAll_Posts($langShort, $filterToModule->activeFilters, null, $filterToModule->limit, $filterToModule->offset, $filterToModule->activeFilters["orderBy"], $filterToModule->activeFilters["orderDirection"]);
$pageData = $cmsPost->optimize($pageData, $langShort);

/* =============================== CONTENT ======== */ 
echo '<article class="module">';
    $render->header(translate("mod-contentEditor-posts-list"), null, $nextPage.'?action=add');
    echo '<section>';
        echo '<div class="moduleWrap">';
            echo '<table class="striped">';
                $tableHead = [translate("mod-contentEditor-posts-list-title"), translate("mod-contentEditor-posts-list-type"), translate("mod-contentEditor-posts-list-visibility"), ''];
                $render->tableHeadSortable($tableHead);

                echo '<tbody class="sortableWrap" data-name="postsPost">';
                    foreach($pageData as $post){
                        echo '<tr id="'.$post["id"].'">';
                            $postType = ( !empty($post["groups"]) ) ? "postGroup" : $cmsPost->postTypes[$post["postType"]];
                            $postGroupMore = ( !empty($post["groups"]) ) ? "<i class='groups'>(".((count($post["groups"]) > 1) ? $post["groups"][0]["title"].", ..." : $post["groups"][0]["title"]).")</i>" : "";

                            $langTitle = ($presenter->activeContent->enabledMultilanguage > 1) ? "<i class='activeLang'>(".$post["activeLang"].")</i> ".$post["title"] : $post["title"];
                            $langTranslate = ($presenter->activeContent->enabledMultilanguage > 1 && $post["activeTranslation"] === true) ? true : false;

                            $render->tableColumnSortable();
                            $render->tableColumn($langTitle);
                            $render->tableColumn(translate("mod-contentEditor-posts-list-type-".$postType).$postGroupMore);
                            $render->tableColumn($render->visibilitySwitch($post["id"], "posts", $post["id"], $post["visibility"]), 80, true);

                            $editSwitch = $nextPage.'?action=edit&id='.$post["id"];
                            $deleteSwitch = 'class="deleteContent" name="posts" data="'.$post["id"].'"';
                            $previewPath = $render->getPreviewPath("page", $post["url"], $post["urlPrefix"]);

                            $render->tableActionColumn($editSwitch, $deleteSwitch, $previewPath, 100, $langTranslate);
                        echo '</tr>';
                    }
                echo '</tbody>';
            echo '</table>';
        echo '</div>';
    echo '</section>';
echo '</article>';
