<?php
/* =============================== CONTROLLER METHODS ======== */
$cmsPostBlog = new cmsPostBlog($database, $appController);

$langShort = $presenter->templateData->langShort;
$listPage = $dataCompiler->modulePageUrl(9);

// ACCESS CONTROL
$pageAction = (isset($_GET["action"])) ? $_GET["action"] : null;
$objectType = (isset($_GET["type"])) ? $_GET["type"] : 'group';
$groupId = (isset($_GET["groupId"])) ? (int) $_GET["groupId"] : 0;
$labelId = (isset($_GET["labelId"])) ? (int) $_GET["labelId"] : 0;
$pageId = ($groupId !== 0) ? $groupId : $labelId;

$dataType = null;
if($objectType === 'group'){
    if($pageAction === 'add' || $groupId !== 0){
        $dataType = 'group-one';
    }else{
        $dataType = 'group-all';
    }
}else{
    if($groupId !== 0){
        if($pageAction === 'add' || $labelId !== 0){
            $dataType = 'label-one';
        }else{
            $dataType = 'label-all';
        }
    }
}
if($dataType === null || ($pageAction === 'edit' && $pageId === 0)){
    $dataCompiler->setContinue(true);
    $dataCompiler->redirectPage(0, $listPage);
}

$prevButton = ($pageAction !== null) ? "{$listPage}?type={$objectType}" : null;
$nextButton = "{$listPage}?type={$objectType}&action=add";
$pageTitle = translate("mod-contentEditor-admin-blog-labels-{$objectType}".(($pageAction !== null) ? "-{$pageAction}" : null));

switch($dataType){
    case "group-one":
        $pageData = $cmsPostBlog->getOneLabelGroup($groupId, $langShort);
        $nextButton = null;
        break;
    case "group-all":
        $pageData = $cmsPostBlog->getAllLabelsGroups($langShort);
        break;
    case "label-one":
        $pageData = $cmsPostBlog->getOneLabel($labelId, $langShort);
        $parentData = $cmsPostBlog->getOneLabelGroup($groupId, $langShort);
        $prevButton .= "&groupId={$groupId}";
        $nextButton = null;
        break;
    case "label-all":
        $pageData = $cmsPostBlog->getAllLabels($groupId, $langShort);
        $parentData = $cmsPostBlog->getOneLabelGroup($groupId, $langShort);
        $nextButton .= "&groupId={$groupId}";
        $prevButton .= "{$listPage}";
        break;
}

if( isset($_POST['submit']) ){
    $moduleCompiler = new \Compiler\contentEditor\DataFactory($database, $appController, $dataCompiler);

    if($objectType === 'group'){
        $moduleCompiler->buildLabelGroup($_POST, $groupId);
        $dataCompiler = $moduleCompiler->compileLabelGroup($cmsPostBlog, $langShort, $presenter->activeContent->enableTranslate);
    }else{
        $moduleCompiler->buildLabel($_POST, $labelId, $groupId);
        $dataCompiler = $moduleCompiler->compileLabel($cmsPostBlog, $langShort, $presenter->activeContent->enableTranslate);
    }

    $pageId = $moduleCompiler->getObjectId();
    $redirectId = "{$objectType}Id={$pageId}";
    if($objectType === 'label' ){
        $cmsPostBlog->clearLabelRelatedCache($pageId);
        if($groupId !== 0){
            $redirectId .= "&groupId={$groupId}";
        }
    }
}

$dataCompiler->showMessage();
$dataCompiler->refreshPage($dataCompiler->nextStep, ($pageAction == "add" && $pageId != 0) ? $presenter->activeContent->page.'?type='.$objectType.'&action=edit&'.$redirectId : "");

/* =============================== CONTENT ======== */
echo '<article class="module">';
    $render->header($pageTitle, $prevButton, $nextButton);
    echo '<section class="suppliers">';
        echo '<div class="moduleWrap">';

            if($dataType === "group-all"){
                echo '<table class="striped">';
                    $render->tableHead([translate("mod-contentEditor-admin-blog-labels-plc-title"), translate("mod-contentEditor-admin-blog-labels-plc-identify"), translate("mod-contentEditor-admin-blog-labels-plc-groupLabels"), '']);
                    foreach($pageData as $group){
                        $langTitle = ($presenter->activeContent->enabledMultilanguage > 1) ? "<i class='activeLang'>(".$group["activeLang"].")</i> ".$group["title"] : $group["title"];
                        $langTranslate = ($presenter->activeContent->enabledMultilanguage > 1 && $group["activeTranslation"] === true) ? true : false;

                        $editSwitch = "{$nextPage}?groupId={$group['id']}&type={$objectType}&action=edit";
                        $previewSwitch = "{$listPage}?groupId={$group['id']}&type=label";
                        $deleteSwitch = ($group["defaultLabel"] == 1) ? null : 'class="deleteContent" name="postLabelGroup" data="'.$group["id"].'"';

                        echo '<tr>';
                            $render->tableColumn('<b>'.$langTitle.'</b>');
                            $render->tableColumn('<i class="blue">'.$group["shortTag"].'</i>');
                            $render->tableColumn('<a href="'.$previewSwitch.'" class="anchor blue"><i class="blue fa fa-search" title="'.translate("mod-contentEditor-admin-blog-labels-plc-groupLabels-btn").'"></i> '.translate("mod-contentEditor-admin-blog-labels-plc-groupLabels-btn").'</a>');
                            $render->tableActionColumn($editSwitch, $deleteSwitch, null, 100, $langTranslate);
                        echo '</tr>';
                    }
                echo '</table>';
            }
            if($dataType === "label-all"){
                echo '<table class="striped">';
                $render->tableHead([translate("mod-contentEditor-admin-blog-labels-plc-title"), translate("mod-contentEditor-admin-blog-labels-plc-group"), translate("mod-contentEditor-admin-blog-labels-plc-identify"), translate("mod-contentEditor-admin-blog-labels-plc-color"), '']);
                    foreach($pageData as $label){
                        $langTitle = ($presenter->activeContent->enabledMultilanguage > 1) ? "<i class='activeLang'>(".$label["activeLang"].")</i> ".$label["title"] : $label["title"];
                        $langTranslate = ($presenter->activeContent->enabledMultilanguage > 1 && $label["activeTranslation"] === true) ? true : false;

                        echo '<tr>';
                            $render->tableColumn('<b>'.$langTitle.'</b>');
                            $render->tableColumn('<i class="blue">'.$parentData["title"].'</i>', 150);
                            $render->tableColumn('<i class="blue">'.$label["shortTag"].'</i>', 150);
                            $render->tableColumn('<span class="colorQ" style="background-color:'.$label["color"].';"></span>', 30);

                            $editSwitch = "{$nextPage}?labelId={$label['id']}&groupId={$parentData['id']}&type={$objectType}&action=edit";
                            $deleteSwitch = ($label["defaultLabel"] == 1) ? null : 'class="deleteContent" name="postLabel" data="'.$label["id"].'"';
                            $render->tableActionColumn($editSwitch, $deleteSwitch, null, 100, $langTranslate);
                        echo '</tr>';
                    }
                echo '</table>';
            }
            if($dataType === "group-one"){
                echo '<form action="" method="POST" enctype="multipart/form-data">';
                    $render->contentDiv(true, null, "contentBox");
                        echo $render->button("submit", "true", translate("save"), false, null, "btn icon save success");
                            $render->contentDiv(true, null, "editPart");
                                $render->contentDiv(true, null, "inlineDiv full");
                                    echo '<h3>&nbsp;</h3>';
                                    $render->contentDiv(true, null, "rows");
                                        $render->contentRow(null, "rowBox inline two first", '<h3>'.translate("mod-contentEditor-admin-blog-labels-plc-title").'</h3>', null, $render->input("text", "title", $pageData["title"], null, true, null, null) );
                                        $render->contentRow(null, "rowBox inline two last", '<h3>'.translate("mod-contentEditor-admin-blog-labels-plc-identify").'</h3>', null, $render->input("text", "shortTag", $pageData["shortTag"], null, true, null, null) );
                                    $render->contentDiv(false);
                                $render->contentDiv(false);
                            $render->contentDiv(false);
                        echo $render->button("submit", "true", translate("save"), false, null, "btn icon save success");
                    $render->contentDiv(false);
                echo '</form>';
            }
            if($dataType === "label-one"){
                echo '<form action="" method="POST" enctype="multipart/form-data">';
                    $render->contentDiv(true, null, "contentBox");
                        echo $render->button("submit", "true", translate("save"), false, null, "btn icon save success");
                            $render->contentDiv(true, null, "editPart");
                                $render->contentDiv(true, null, "inlineDiv full");
                                    echo '<h3>&nbsp;</h3>';
                                    $render->contentDiv(true, null, "rows");
                                        $render->contentRow(null, "rowBox inline two first", '<h3>'.translate("mod-contentEditor-admin-blog-labels-plc-title").'</h3>', null, $render->input("text", "title", $pageData["title"], null, true, null, null) );
                                        $render->contentRow(null, "rowBox inline two last ", '<h3>'.translate("mod-contentEditor-admin-blog-labels-plc-identify").'</h3>', null, $render->input("text", "shortTag", $pageData["shortTag"], null, false, null, null) );
                                        $render->contentRow(null, "rowBox inline two first colorPick ", '<h3>'.translate("mod-contentEditor-admin-blog-labels-plc-color").'</h3>', null, $render->input("text", "color", $pageData["color"], null, false, null, null) );
                                        $render->contentRow(null, "rowBox inline two last ", '<h3>'.translate("mod-contentEditor-admin-blog-labels-plc-group").'</h3>', null, $render->input("text", "groupName", $parentData["title"], null, false, null, null, null, true) );
                                    $render->contentDiv(false);
                                $render->contentDiv(false);
                            $render->contentDiv(false);
                        echo $render->button("submit", "true", translate("save"), false, null, "btn icon save success");
                    $render->contentDiv(false);
                echo '</form>';
            }

        echo '</div>';
    echo '</section>';
echo '</article>';