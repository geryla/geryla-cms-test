<?php
  $render->contentDiv(true, "postGroupInfo", "boxContent"); 
    
    $parentSelect = '<select name="parentId" class="notTranslatable">';
    $parentSelect .= '<option value="0">'.translate("mod-contentEditor-groups-detail-categoryParent-none").'</option>';
    $parentSelect .= $cmsGroups->selectAllGroupsAsSelectBoxTree(0, $pageId, 0, $presenter->templateData->langShort);
    $parentSelect .= '</select>'; 
    $render->contentRow(null, "rowBox", '<h3>'.translate("mod-contentEditor-groups-detail-categoryParent").'</h3>', null, $parentSelect);
    
    $templatesSelect = '<select name="templateName" class="notTranslatable">';
    $templatesSelect .= '<option value="">'.translate("mod-contentEditor-groups-detail-template-none").'</option>';
    $templatesSelect .= $cmsGroups->getTemplateForGroup("templates-lang", $presenter->templateData->langShort, $pageData["templateName"]);
    $templatesSelect .= '</select>'; 
    $render->contentRow(null, "rowBox", '<h3>'.translate("mod-contentEditor-groups-detail-template").'</h3>', null, $templatesSelect);
    
    $render->contentRow(null, "rowBox", '<h3>'.translate("mod-contentEditor-groups-shortTag").'</h3>', null, $render->input("text", "shortTag", $pageData["shortTag"], null, false, null, null) );
    
    $render->contentRow(null, "rowBox", '<h3>'.translate("mod-templates-admin-detail-className").'</h3>', null, $render->input("text", "className", $pageData["className"], null, false, null, null) );
    
  $render->contentDiv(false);  
?>