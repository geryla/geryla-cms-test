<?php
    //$adminEngine = new adminEngine($database, $localeEngine);
    //$moduleAdmins = new moduleAdmins($adminEngine, $localizationClass);
    //$allAdmins = $moduleAdmins->getAllAdmins();

    $authorSelect = '<select name="authorId">';
    $authorSelect .= $cmsPost->getPostAuthorToSelectBox($pageData["authorId"], []);
    $authorSelect .= '</select>';

    $postGroups = $cmsGroups->selectAllGroupsAsCheckboxTree(0, $pageId, 0, $presenter->templateData->langShort);

    $render->contentDiv(true, "postInfo", "boxContent");
        $render->contentRow(null, "rowBox inline two first", '<h3>'.translate("mod-contentEditor-posts-detail-author").'</h3>', null, $authorSelect);
        $render->contentRow(null, "rowBox inline two last", '<h3>'.translate("mod-contentEditor-info-createdDate").'</h3>', null, $render->input("text", "createdDate", $pageData["createdDate"], null, false, null, "datepicker") );
        $render->contentRow(null, "rowBox categoryCheck", '<h3>'.translate("mod-contentEditor-groups-detail-groups").'</h3>', null, $postGroups);
        $render->contentRow(null, "rowBox", '<h3>'.translate("mod-templates-admin-detail-className").'</h3>', null, $render->input("text", "className", $pageData["className"], null, false, null, null) );
        echo $render->input("hidden", "postType", 1, null, true, null, null);
    $render->contentDiv(false);