<?php
  $render->contentDiv(true, "postCategoryInfo", "boxContent"); 
    
    $parentSelect = '<select name="parentId" class="notTranslatable">';
    $parentSelect .= '<option value="0">'.translate("mod-contentEditor-categories-detail-categoryParent-none").'</option>';
    $parentSelect .= $cmsCategories->selectAllCategoriesAsSelectBoxTree(0, $pageId, 0, $presenter->templateData->langShort);
    $parentSelect .= '</select>'; 
    $render->contentRow(null, "rowBox", '<h3>'.translate("mod-contentEditor-categories-detail-categoryParent").'</h3>', null, $parentSelect);
    
    $render->contentRow(null, "rowBox", '<h3>'.translate("mod-templates-admin-detail-className").'</h3>', null, $render->input("text", "className", $pageData["className"], null, false, null, null) );
    
  $render->contentDiv(false);  
?>