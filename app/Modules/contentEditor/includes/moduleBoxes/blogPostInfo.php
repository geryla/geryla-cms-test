<?php
    //$adminEngine = new adminEngine($database, $localeEngine);
    //$moduleAdmins = new moduleAdmins($adminEngine, $localizationClass);
    //$allAdmins = $moduleAdmins->getAllAdmins();

    $authorSelect = '<select name="authorId">';
    $authorSelect .= $cmsPostBlog->getPostAuthorToSelectBox($pageData["authorId"], []);
    $authorSelect .= '</select>';

    $blogCategories = $cmsCategories->selectAllCategoriesAsCheckboxTree(0, $pageId, 0, $presenter->templateData->langShort);
    $blogLabels = $cmsPostBlog->renderLabelsCheckboxes($pageId, $presenter->templateData->langShort);

    $render->contentDiv(true, "blogPostInfo", "boxContent");
        $render->contentRow(null, "rowBox inline three first", '<h3>'.translate("mod-contentEditor-posts-detail-author").'</h3>', null, $authorSelect);
        $render->contentRow(null, "rowBox inline three", '<h3>'.translate("mod-contentEditor-info-createdDate").'</h3>', null, $render->input("text", "createdDate", $pageData["createdDate"], null, false, null, "datepicker") );
        $render->contentRow(null, "rowBox inline three last", '<h3>'.translate("mod-contentEditor-info-revisionDate").'</h3>', null, $render->input("text", "revisionDate", $pageData["revisionDate"], null, false, null, "datepicker") );
        $render->contentRow(null, "rowBox inline two first", '<h3>'.translate("mod-contentEditor-info-className").'</h3>', null, $render->input("text", "className", $pageData["className"], null, false, null, null) );
        $render->contentRow(null, "rowBox inline two last", '<h3>'.translate("mod-contentEditor-info-lengthTime").'</h3>', null, $render->input("text", "lengthTime", $pageData["lengthTime"], null, false, null, null) );
        $render->contentRow(null, "rowBox inline two first vertical-top categoryCheck", '<h3>'.translate("mod-contentEditor-posts-detail-categories").'</h3>', null, $blogCategories);
        $render->contentRow(null, "rowBox inline two last vertical-top categoryCheck", '<h3>'.translate("mod-contentEditor-admin-blog-labels-plc-hookedLabels").'</h3>', null, $blogLabels);
        echo $render->input("hidden", "postType", 2, null, true, null, null);
    $render->contentDiv(false);