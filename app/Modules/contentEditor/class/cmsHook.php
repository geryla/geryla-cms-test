<?php

class cmsHook {
    private $database;
    private $appController;
    private $localizationClass;

    private $templateClass;
    private $CMS;

    private $langShort;
    private $langFile;

    public $translateType;
    public $hooksTable;
    public $bondsTable;
    public $anchorsTable;

    public function __construct(Database $database, AppController $appController) {
        $this->database = $database;
        $this->appController = $appController;
        $this->localizationClass = $appController->returnLocalizationClass();

        $this->langShort = $appController->getLang();
        $this->langFile = $appController->getLangFile();

        $this->translateType = $this->appController->getModuleConfig('contentEditor', 'translateType')["postHook"];
        $this->hooksTable = databaseTables::getTable('contentEditor', "hookTable");
        $this->bondsTable = databaseTables::getTable('contentEditor', "bondTable");
        $this->anchorsTable = databaseTables::getTable('contentEditor', "anchorsTable");
    }


    public function getAllHooks($langShort) {
        $sql = "SELECT * FROM `{$this->hooksTable}` ORDER BY id;";
        return $this->localizationClass->translateResult($this->database->getQuery($sql, null, true), $this->translateType, $langShort);
    }

    public function getOneHook($hookId, $langShort) {
        $sql = "SELECT * FROM `{$this->hooksTable}` WHERE id = :id;";
        return $this->localizationClass->translateResult($this->database->getQuery($sql, ["id" => $hookId], false), $this->translateType, $langShort);
    }

    public function getAllHookContent($hookId, $parentId = 0) {
        $sql = "SELECT * FROM `{$this->bondsTable}` WHERE hookId = :hookId AND parentId = :parentId ORDER BY orderNum;";
        return $this->database->getQuery($sql, array("hookId" => $hookId, "parentId" => $parentId), true);
    }

    public function getAllHookContentChild($parentId) {
        $sql = "SELECT * FROM `{$this->bondsTable}` WHERE parentId = :parentId ORDER BY orderNum;";
        return $this->database->getQuery($sql, array("parentId" => $parentId), true);
    }

    public function addContentToHook($data) {
        return $this->database->insertQuery("INSERT INTO `{$this->bondsTable}`", $data);
    }

    public function updateHookMenu($id, $parentId, $orderNum) {
        return $this->database->updateQuery("UPDATE `{$this->bondsTable}`", ["parentId" => $parentId, "orderNum" => $orderNum], ["id" => $id], "WHERE id = :id");
    }

    public function updateHook($title, $hookId) {
        return $this->database->updateQuery("UPDATE `{$this->hooksTable}`", ["title" => $title], ["id" => $hookId], "WHERE id = :id");
    }

    public function updateChild($parentId) {
        $allChild = $this->getAllHookContentChild($parentId);
        if (!empty($allChild)) {
            foreach ($allChild as $child) {
                $this->database->updateQuery("UPDATE `{$this->bondsTable}`", ["parentId" => 0, "orderNum" => 0], ["id" => $child["id"]], "WHERE id = :id");
            }
        }
    }

    public function getOptionsForMenu($langTableName, $lang) {
        ($this->CMS === null) ? $this->CMS = new CMS($this->database, $this->appController) : null;
        $allContent = $this->CMS->getAllContentFromOtherTable($this->database->injectionProtect($langTableName), $lang);

        if (!empty($allContent)) {
            foreach ($allContent as $content) {
                if (isset($content["pageType"]) && $content["pageType"] == 2) {
                    continue;
                } // Skip non-content - TEMPLATES
                if (isset($content["templateName"]) && empty($content["templateName"])) {
                    continue;
                } // Skip non-content - POST GROUPS
                if ($langTableName == databaseTables::getTable("contentEditor", "categoryTable") && empty($content["url"])) {
                    continue;
                } // Skip non-content (without URL) - POST CATEGORIES
                if (empty($content["title"])) {
                    continue;
                } // SKIP not translated if TRANSLATE IS ENABLED
                if (isset($content["visibility"]) && $content["visibility"] == 0) {
                    continue;
                } // SKIP non visible content
                echo '<option value="' . $content["id"] . '">' . $content["title"] . '</option>';
            }
        }
    }

    public function getHookContentSortable($hookId, $parentId, $repeat = 0) {
        $lang = $this->langFile;
        $deleteText = "confirm-head='".translate("confirm-delete-head")."' confirm-text='".translate("confirm-delete-text")."' confirm-yes='".translate("confirm-delete-yes")."' confirm-no='".translate("confirm-delete-no")."'";
        $content = $this->getAllHookContent($hookId, $parentId);
        $hook = null;

        if (!empty($content)) {
            ($this->CMS === null) ? $this->CMS = new CMS($this->database, $this->appController) : null;
            foreach ($content as $track) {
                $oneContent = $this->CMS->getContentFromOtherTableById($track["objectId"], $track["dataTable"], $_SESSION["languages"]["adminLang"]);
                $hookInfo = $this->createHookInfo($oneContent, $track["dataTable"], []);

                $visibilityInfo = (isset($oneContent["visibility"]) && $oneContent["visibility"] == 0) ? "<span>" . translate("mod-contentEditor-hooks-nonVisibleElement") . "</span> " : null;
                $deleteType = ($track["dataTable"] == databaseTables::getTable("contentEditor", "anchorsTable")) ? 'bondsCustom' : 'bonds';
                $hookText = '
                    <a href="' . $hookInfo["url"] . '" target="_blank">
                        <span>' . $hookInfo["title"] . '</span>
                        ' . (($deleteType === "bondsCustom") ? "<em>[" . $hookInfo["url"] . "]</em>" : null) . '
                        <i>' . $visibilityInfo . '('.translate("mod-contentEditor-hooks-edit-content-".$hookInfo["type"]).')</i>
                    </a>
                ';

                $hook .= '<li class="dd-item" id="list_' . $track["id"] . '" data-id="' . $track["id"] . '">';
                $hook .= '<div class="dd-handle dd3-handle"><i class="fa fa-arrows"></i></div>';
                $hook .= '<div class="dd3-content">';
                $hook .= '<span class="text">' . $hookText . '</span>';
                $hook .= '<span class="delete_menu">';
                $hook .= '<span class="deleteContent" data="' . $track["id"] . '" name="' . $deleteType . '" ' . $deleteText . '>';
                $hook .= '<i class="fa fa-trash-o red" title="' . translate("delete") . '"></i> ' . translate("delete");
                $hook .= '</span>';
                $hook .= '</span>';
                $hook .= '</div>';

                $hookContent = $this->getHookContentSortable($hookId, $track["id"], $repeat + 1);
                $hook .= ($hookContent !== null) ? '<ol class="dd-list">' . $hookContent . '</ol>' : null;
                $hook .= '</li>';
            }
        }

        return $hook;
    }

    public function hookTree($id, $values, $repeat, $orderedList) {
        $space = "";
        for ($i = 0; $i < $repeat; $i++) {
            $space .= " ";
        }

        foreach ($values as $value) {
            if (isset($value["children"])) {
                $repeat++;
                $this->hookTree($value["id"], $value["children"], $repeat, $orderedList);
            }
            $this->updateHookMenu($value["id"], $id, $orderedList[$value["id"]]);
        }
    }

    public function getHookContentTree($hookId, $parentId, $repeat = 0, $activeTemplate = [], $urlPath = null, $itemThumbnail = null) {
        $contentTree = [];
        $activeTemplate = (array)$activeTemplate;
        $content = $this->getAllHookContent($hookId, $parentId);

        if (!empty($content)) {
            ($this->CMS === null) ? $this->CMS = new CMS($this->database, $this->appController) : null;
            foreach ($content as $track) {
                $isAuto = false;
                $oneContent = $this->CMS->getContentFromOtherTableById($track["objectId"], $track["dataTable"], $this->appController->getLang());
                if (isset($oneContent["visibility"]) && $oneContent["visibility"] == 0) {
                    continue;
                }

                $hookInfo = $this->createHookInfo($oneContent, $track["dataTable"], $urlPath, $activeTemplate, $this->appController->getLang(), $itemThumbnail);
                $childInfo = $this->getHookContentTree($hookId, $track["id"], $repeat + 1, $activeTemplate, $urlPath, $itemThumbnail);

                if (empty($childInfo) && $track["dataTable"] == databaseTables::getTable("products", "categoryTableLang")) {
                    $childInfo = $this->createHookProductCategoryInfo($oneContent, $activeTemplate, $itemThumbnail);
                    $isAuto = true;
                }

                $hookInfo["className"] = (!empty($childInfo)) ? $hookInfo["className"] . ' ' . 'hasChild' : $hookInfo["className"];
                $contentTree[] = ["data" => $hookInfo, "child" => $childInfo, "isAuto" => $isAuto];
            }
        }

        return $contentTree;
    }

    private function createHookInfo($objectInfo, $objectTable, $urlPath = null, $activeTemplate = [], $langShort = null, $imageThumbnail = null) {
        ($this->templateClass === null) ? $this->templateClass = $this->appController->returnTemplatesClass() : null;

        $templateAnchors = ($langShort === null) ? $this->templateClass->getAllTemplateAnchors($this->appController->getLang()) : $this->templateClass->allTemplates;
        $activeClass = $externalUrl = false;
        $type = $newUrl = null;

        // TEMPLATE
        if ($objectTable == databaseTables::getTable("templates", "templateTableLang")) {
            $newUrl = ($objectInfo["templateName"] == "home") ? "/" : "/" . $objectInfo["url"];
            $type = "template";
        }
        // CMS GROUP
        if ($objectTable == databaseTables::getTable("contentEditor", "groupTable")) {
            if (empty($objectInfo["templateName"]) OR $objectInfo["allowIndex"] != 1) {
                $newUrl = "#"; // If templateName is empty or index group is not allowed - disable URL and page content
            } else {
                $newUrl = "/" . $templateAnchors[$objectInfo["templateName"]] . "/" . $objectInfo["url"];
            }
            $type = "group";
        }
        // CMS POST (+ blog posts)
        if ($objectTable == databaseTables::getTable("contentEditor", "postTableLang")) {
            if ($objectInfo["postType"] == 1) {
                $newUrl = "/" . $templateAnchors["page"] . "/" . $objectInfo["urlPrefix"] . "-" . $objectInfo["url"];
                $type = "post";
            } else {
                $newUrl = "/" . $templateAnchors["blog"] . "/" . $objectInfo["urlPrefix"] . "-" . $objectInfo["url"];
                $type = "blogDetail";
            }
        }
        // CMS CATEGORY
        if ($objectTable == databaseTables::getTable("contentEditor", "categoryTable")) {
            $newUrl = "/" . $templateAnchors["blog"] . "/" . $objectInfo["url"];
            $objectInfo["templateName"] = "blog";
            $type = "blogCategory";
        }
        // MARKETING PAGES
        if ($objectTable == databaseTables::getTable("marketing", "pages")) {
            $newUrl = /*"/".$templateAnchors["page"].*/
                "/" . $objectInfo["url"];
            $type = "marketingPage";
        }
        // PRODUCT CATEGORY
        if ($objectTable == databaseTables::getTable("products", "categoryTableLang")) {
            $newUrl = "/" . $templateAnchors["productCategory"] . "/" . $objectInfo["urlPrefix"] . "-" . $objectInfo["url"];
            $type = "productCategory";
        }
        // CUSTOM ANCHOR
        if ($objectTable == databaseTables::getTable("contentEditor", "anchorsTable")) {
            $newUrl = $objectInfo["url"];
            $type = "customAnchor";
            $activeClass = ($urlPath !== null) ? (($urlPath == $objectInfo["url"]) ? true : false) : false;
            $externalUrl = (strpos($newUrl, "http") !== false OR strpos($newUrl, "www") !== false) ? true : false;
        }

        if ($activeClass === false) {
            $activeClass = ((isset($activeTemplate["id"]) && $activeTemplate["id"] == $objectInfo["id"]) && $activeTemplate["url"] == $objectInfo["url"]) ? true : false;
            $activeClass = ($activeClass === true && (isset($objectInfo["templateName"]) && $activeTemplate["templateName"] == $objectInfo["templateName"])) ? true : $activeClass;
            if ($activeClass === true && ($urlPath !== null && $urlPath != $newUrl)) {
                $activeClass = false;
            }
        }

        $className = ($objectInfo["className"]) ? $objectInfo["className"] : "";
        $className .= ($activeClass === true) ? " active" : "";
        $className .= ($newUrl == "#") ? "noClick" : "";
        $className .= ($externalUrl === true) ? " externalUrl" : "";

        $hookData = [
            "id" => $objectInfo["id"],
            "title" => $objectInfo["title"],
            "shortTag" => (isset($objectInfo["shortTag"])) ? $objectInfo["shortTag"] : "",
            "type" => $type,
            "className" => $className,
            "isActive" => $activeClass,
            "url" => $newUrl,
            "externalUrl" => $externalUrl,
        ];

        if ($imageThumbnail !== null) {
            $mediaProcessor = $this->appController->returnMediaProcessor();
            if(!empty($hookData["id"])){
                $objectImage = $mediaProcessor->getMainImage($hookData["id"], $type, $langShort, $imageThumbnail);
                $hookData["image"] = $objectImage;
            }
        }

        return $hookData;
    }

    private function createHookProductCategoryInfo($objectInfo, $activeTemplate, $imageThumbnail = null) {
        $categoryClass = new categoryClass($this->database, $this->appController);
        $subChild = $categoryClass->getAllChildCategories($objectInfo["id"], $this->appController->getLang(), true);
        $returnTree = [];

        if (!empty($subChild)) {
            foreach ($subChild as $category) {
                $subHook = $this->createHookInfo($category, databaseTables::getTable("products", "categoryTableLang"), null, $activeTemplate, $this->appController->getLang(), $imageThumbnail);
                $subChildHelp = $this->createHookProductCategoryInfo($category, $activeTemplate, $imageThumbnail);

                $subHook["className"] = (!empty($subChildHelp)) ? $subHook["className"] . ' ' . 'hasChild' : $subHook["className"];
                $returnTree[] = (!empty($subChildHelp)) ? ["data" => $subHook, "child" => $subChildHelp] : ["data" => $subHook];
            }
        }

        return $returnTree;
    }

    public function addHookAnchor($data) {
        $returnData = $this->database->insertQuery("INSERT INTO `{$this->anchorsTable}`", $data);
        return $returnData;
    }

    public function deleteHookAnchor($id) {
        $returnData = $this->database->deleteQuery("DELETE FROM `{$this->anchorsTable}`", ["id" => $id], "WHERE id = :id");
        return $returnData;
    }

    public function getAnchorFromHook($bondId) {
        $sql = "SELECT * FROM `{$this->bondsTable}` WHERE id = :bondId;";
        $returnData = $this->database->getQuery($sql, array("bondId" => $bondId), false);
        return $returnData;
    }
}