<?php

class cmsPost {
    protected $database;
    protected $appController;
    protected $localizationClass;

    protected $templateClass;
    protected $cmsGroup;
    protected $mediaProcessor;
    protected $adminEngine;
    protected $moduleAdmins;

    protected $langShort;
    protected $langFile;

    public $mediaNameGroup;
    public $mediaNamePost;
    public $mediaNameCategory;
    public $postsTable;
    public $postsTranslateTable;
    public $crossTable;
    public $crossGroupTable;
    public $labelsTable;
    public $labelsHookTable;
    public $labelsGroupsTable;

    public $postTypes = [1 => "post", 2 => "blog"];
    public $translateRows = ["title", "shortDesc", "description", "metaKeywords", "metaTitle", "metaDescription", "url", "langShort"]; // Editable fields
    public $dataRows = ["urlPrefix", "postType", "authorId", "autoEnable", "allowIndex", "className", "createdDate", "updatedDate", "revisionDate", "orderNum", "visibility", "lengthTime"]; // Editable ields

    public function __construct(Database $database, AppController $appController) {
        $this->database = $database;
        $this->appController = $appController;
        $this->localizationClass = $appController->returnLocalizationClass();
        $this->mediaProcessor = $appController->returnMediaProcessor();

        $this->langShort = $appController->getLang();
        $this->langFile = $appController->getLangFile();

        $this->mediaNameGroup = $this->appController->getModuleConfig('contentEditor', 'mediaSettings')["postGroup"];
        $this->mediaNamePost = $this->appController->getModuleConfig('contentEditor', 'mediaSettings')["post"];
        $this->mediaNameCategory = $this->appController->getModuleConfig('contentEditor', "mediaSettings")["postCategory"];
        $this->postsTable = databaseTables::getTable('contentEditor', 'postTable');
        $this->postsTranslateTable = databaseTables::getTable('contentEditor', 'postTableLang');
        $this->crossTable  = databaseTables::getTable('contentEditor', 'crossCategoryTable');
        $this->crossGroupTable = databaseTables::getTable('contentEditor', 'crossGroupTable');
        $this->labelsTable = databaseTables::getTable('contentEditor', 'labelsTable');
        $this->labelsHookTable = databaseTables::getTable('contentEditor', 'labelsHookTable');
        $this->labelsGroupsTable = databaseTables::getTable('contentEditor', 'labelsGroupsTable');
    }


// Add URL PATH and CATEGORIES to post
    public function optimize(Array $data, ?String $langShort = null, Array $options = [], Bool $cache = true) {
        $options["deepLevel"] = (isset($options["deepLevel"])) ? $options["deepLevel"] + 1 : 0;
        $options["with"] = (isset($options["with"])) ? $options["with"] : ['all'];
        $options["itemThumbnail"] = (isset($options["itemThumbnail"]) && trim($options["itemThumbnail"]) !== "") ? $options["itemThumbnail"] : null;

        $langShort = ($langShort === null) ? $this->appController->getLang() : $langShort;
        $isSingleItem = (isset($data["id"]));
        $dataArray = ($isSingleItem === true) ? [$data] : $data;

        $optimizedData = [];
        if(!empty($dataArray)){
            foreach($dataArray as $key => $item){
                $optimizedData[$key] = (!empty($item)) ? $this->cacheItem($item, $langShort, $options, $cache) : [];
            }
        }

        return ($isSingleItem === true) ? $optimizedData[0] : $optimizedData;
    }
    public function cacheItem(Array $item, ?String $langShort = null, Array $options = [], Bool $cache = true) {
        $entityCache = new EntityCache();
        $entityName = 'post';
        $langShort = ($langShort === null) ? $this->appController->getLang() : $langShort;

        $responseData = ($cache === true) ? $entityCache->get($entityName, $item["id"], $langShort) : null;
        if($responseData === null){
            $options["with"] = ["all"];
            $responseData = $this->utilize($item, $langShort, $options);
            if($cache === true){
                $entityCache->set($entityName, $item["id"], $langShort, $responseData);
            }
        }

        $imagesEntity = "images/{$entityName}/{$item['id']}";
        $imagesResponse = ($cache === true) ? $entityCache->get($imagesEntity, $options["itemThumbnail"], $langShort, null, false) : null;
        if($imagesResponse === null){
            $imagesResponse = $this->mediaProcessor->getAllObjectImages($item["id"], $this->mediaNamePost, $langShort, $options["itemThumbnail"], true);
            $entityCache->set($imagesEntity, $options["itemThumbnail"], $langShort, $imagesResponse, null, false);
        }
        $responseData["images"] = $imagesResponse;

        $entity = new EntityCmsPost($responseData);
        $postCache = $entity->get();

        return $postCache;
    }
    private function utilize(Array $item, ?String $langShort = null, Array $options = []) {
        ($this->templateClass === null) ? $this->templateClass = $this->appController->returnTemplatesClass() : null;

        $allAnchors = $this->templateClass->allTemplates;
        $langShort = ($langShort === null) ? $this->appController->getLang() : $langShort;
        $requestAllParameters = (array_key_exists('with', $options) && in_array('all', $options["with"]));

        // Default
        $itemOptimized = $item;
        $itemOptimized["path"] = "/{$allAnchors['page']}/{$item['urlPrefix']}-{$item['url']}";
        $itemOptimized["date"] = date("d. m. Y", strtotime($item["createdDate"]));
        $itemOptimized["time"] = date("H:i:s", strtotime($item["createdDate"]));

        $itemOptimized["perex"] = mb_substr(preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", strip_tags(html_entity_decode($item["description"]))), 0, 255, 'utf-8');

        if ($requestAllParameters || in_array("groups", $options["with"])) {
            ($this->cmsGroup === null) ? $this->cmsGroup = new cmsGroups($this->database, $this->appController) : null;

            $allPostCategories = [];
            $allCategories = $this->cmsGroup->getAllGroupsCross($item["id"]);
            if ($allCategories) {
                foreach ($allCategories as $categoryKey => $category) {
                    $allPostCategories[$categoryKey] = $this->cmsGroup->getOneGroupById($category["groupId"], $langShort);
                    $allPostCategories[$categoryKey]["templatePath"] = (!empty($allPostCategories[$categoryKey]["templateName"])) ? '/' . $allAnchors[$allPostCategories[$categoryKey]["templateName"]] : '';
                    $allPostCategories[$categoryKey]["path"] = $allPostCategories[$categoryKey]["templatePath"] . '/' . $allPostCategories[$categoryKey]["url"];
                    $allPostCategories[$categoryKey]["images"] = $this->mediaProcessor->getAllObjectImages($category["id"], $this->mediaNameGroup, $langShort, null, true);
                }
            }

            $itemOptimized["groups"] = $allPostCategories;
        }
        /*if ($requestAllParameters || in_array("author", $options["with"])) {
            ($this->adminEngine === null) ? $this->adminEngine = new adminEngine($this->database, $this->appController->returnLocaleEngine()) : null;
            ($this->moduleAdmins === null) ? $this->moduleAdmins = new moduleAdmins($this->adminEngine, $this->appController->returnLocalizationClass()) : null;

            $authorInfo = (array) $this->moduleAdmins->getOneAdmin($item["authorId"]);
            if(!empty($authorInfo)){
                $itemOptimized["author"] = $authorInfo;
                $itemOptimized["author"]["image"] = (array) $this->mediaProcessor->getMainImage($authorInfo["id"], $this->moduleAdmins->adminMedia, $langShort, '');
            }
        }*/
        if ($requestAllParameters || in_array("files", $options["with"])) {
            $optimizedInfo["files"] =  (array) $this->mediaProcessor->getAllObjectMediaByType($item["id"], $this->mediaNamePost, $langShort, ['audio', 'video', 'file']);
        }

        return $itemOptimized;
    }

    public function getOnePost($postId, $langShort, $visibility = null) {
        $visibility = ($visibility === null) ? null : (($visibility === false) ? "AND visibility = 0" : "AND visibility = 1");
        $sqlQuery = "SELECT * FROM `{$this->postsTable}` WHERE id = :id AND postType = 1 {$visibility};";
        $langColumns = "title, shortDesc, description, metaKeywords, metaTitle, metaDescription, url, langShort";

        $returnData = $this->localizationClass->getTranslation($this->database->getQuery($sqlQuery, ["id" => $postId], false), $langColumns, $this->postsTranslateTable, $langShort);
        return $returnData;
    }
    public function getAllPosts($langShort, $visibility = null, $limit = null, $offset = null) {
        $visibility = ($visibility === null) ? null : (($visibility === false) ? "AND visibility = 0" : "AND visibility = 1");
        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;

        $sqlQuery = "SELECT * FROM `{$this->postsTable}` WHERE postType = 1 {$visibility} ORDER BY orderNum DESC, id DESC {$limit} {$offset};";
        $langColumns = "title, shortDesc, description, metaKeywords, metaTitle, metaDescription, url, langShort";

        $returnData = $this->localizationClass->getTranslation($this->database->getQuery($sqlQuery, null, true), $langColumns, $this->postsTranslateTable, $langShort);
        return $returnData;
    }
    public function getAllPostsByGroup($groupId, $langShort, $visibility = null, $limit = null, $offset = null, $orderBy = 'orderNum', $orderDirection = 'DESC') {
        $visibility = ($visibility === null) ? null : (($visibility === false) ? "AND visibility = 0" : "AND visibility = 1");
        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;
        $orderDirection = ($orderDirection !== 'DESC') ? "DESC" : "ASC";

        $sqlQuery = "SELECT data.* FROM `{$this->postsTable}` AS data INNER JOIN `{$this->crossGroupTable}` AS postGroups ON data.id = postGroups.postId WHERE postGroups.groupId = :groupId AND data.postType = 1  {$visibility} ORDER BY {$orderBy} {$orderDirection} {$limit} {$offset};";
        $langColumns = "title, shortDesc, description, metaKeywords, metaTitle, metaDescription, url, langShort";

        $returnData = $this->localizationClass->getTranslation($this->database->getQuery($sqlQuery, ["groupId" => $groupId], true), $langColumns, $this->postsTranslateTable, $langShort);
        return $returnData;
    }

    public function getPostAuthorToSelectBox($authorId, $allAuthors) {
        $option = "";
        foreach ($allAuthors as $admin) {
            $selected = ($authorId == $admin->id) ? "selected" : "";
            $option .= '<option value="' . $admin->id . '" ' . $selected . '>' . $admin->name . ' ' . $admin->surname . '</option>';
        }

        return $option;
    }
}