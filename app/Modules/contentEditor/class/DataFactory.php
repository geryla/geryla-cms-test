<?php
    namespace Compiler\contentEditor;
    use Compiler\DataCompiler;

    class DataFactory {
        private $database;
        private $appController;
        private $dataCompiler = null;
        private $lang;
        private $langShort;

        private $objectId = 0;
        private $objectData = [];

        public function __construct(\Database $database, \AppController $appController, DataCompiler $dataCompiler){
            $this->database = $database;
            $this->appController = $appController;
            $this->dataCompiler = $dataCompiler;

            $this->lang = $appController->getLangFile();
            $this->langShort = $appController->getLang();
        }

        public function getObjectId(){
            return $this->objectId;
        }

    // Post
        public function buildPost(Array $objectData, Int $objectId, String $langShort, \cmsPost $cmsPost, Array $defaultData = []){
            unset($objectData['submit']); // Prevent POST

            $prefixEngine = new \prefixEngine($this->database);

            $objectData["url"] = $prefixEngine->title2pagename( (empty($objectData["url"])) ? $objectData["title"] : $objectData["url"] );
            $objectData["urlPrefix"] = (!empty($defaultData["urlPrefix"])) ? $defaultData["urlPrefix"] : $prefixEngine->addNewPrefix($cmsPost->postsTable); // Create prefix + save table
            $objectData["createdDate"] = (!isset($objectData["createdDate"]) OR empty($objectData["createdDate"])) ? date("Y-m-d H:i:s") : date('Y-m-d H:i:s', strtotime($objectData["createdDate"]));
            $objectData["updatedDate"] = date("Y-m-d H:i:s");
            $objectData["langShort"] = ($objectId === 0) ? $langShort : $defaultData["langShort"];

            $this->objectData = $objectData;
            $this->objectId = $objectId;
        }
        public function compilePost(\cmsPost $cmsPost, String $langShort, Bool $enableTranslate){
            if(!empty($this->objectData)){
                $this->dataCompiler->setTables($cmsPost->postsTable, $cmsPost->postsTranslateTable);
                $this->dataCompiler->setData($this->objectData, $cmsPost->translateRows, $cmsPost->dataRows);
                $this->dataCompiler->setTranslate($enableTranslate);

                if($this->objectId === 0){
                    $this->objectId = $this->dataCompiler->addQuery();
                }else{
                    $this->dataCompiler->updateQuery($this->objectId);
                }

                $mediaController = $this->appController->returnMediaController();
                $this->dataCompiler->updateImages($mediaController->translateTable);

                $this->dataCompiler->processContentBlocks($this->objectData);
                $this->updatePostGroups($this->objectId);

                $this->dataCompiler->setLog($this->objectId, 'contentEditor', 'Post', $langShort);
            }

            return $this->dataCompiler;
        }

    // Blog post
        public function buildBlogPost(Array $objectData, Int $objectId, String $langShort, \cmsPostBlog $cmsPostBlog, Array $defaultData = []){
            unset($objectData['submit']); // Prevent POST

            $prefixEngine = new \prefixEngine($this->database);

            $objectData["langShort"] = ($objectId === 0) ? $langShort : $defaultData["langShort"];
            $objectData["url"] = $prefixEngine->title2pagename( (empty($objectData["url"])) ? $objectData["title"] : $objectData["url"] );
            $objectData["urlPrefix"] = (!empty($defaultData["urlPrefix"])) ? $defaultData["urlPrefix"] : $prefixEngine->addNewPrefix($cmsPostBlog->postsTable); // Create prefix + save table
            $objectData["createdDate"] = (!isset($objectData["createdDate"]) OR empty($objectData["createdDate"])) ? date("Y-m-d H:i:s") : date('Y-m-d H:i:s', strtotime($objectData["createdDate"]));
            $objectData["updatedDate"] = date("Y-m-d H:i:s");
            $objectData["revisionDate"] = (!isset($objectData["revisionDate"]) OR empty($objectData["revisionDate"])) ? null : date('Y-m-d H:i:s', strtotime($objectData["revisionDate"]));

            $this->objectData = $objectData;
            $this->objectId = $objectId;
        }
        public function compileBlogPost(\cmsPostBlog $cmsPostBlog, String $langShort, Bool $enableTranslate){
            if(!empty($this->objectData)){
                $this->dataCompiler->setTables($cmsPostBlog->postsTable, $cmsPostBlog->postsTranslateTable);
                $this->dataCompiler->setData($this->objectData, $cmsPostBlog->translateRows, $cmsPostBlog->dataRows);
                $this->dataCompiler->setTranslate($enableTranslate);

                if($this->objectId === 0){
                    $this->objectId = $this->dataCompiler->addQuery();
                }else{
                    $this->dataCompiler->updateQuery($this->objectId);
                }

                $mediaController = $this->appController->returnMediaController();
                $this->dataCompiler->updateImages($mediaController->translateTable);

                $this->dataCompiler->processContentBlocks($this->objectData);
                $this->updatePostCategories($this->objectId, $langShort);
                $this->updatePostLabels($cmsPostBlog, $this->objectId);

                $this->dataCompiler->setLog($this->objectId, 'contentEditor', 'BlogPost', $langShort);
            }

            return $this->dataCompiler;
        }

    // Group
        public function buildGroup(Array $objectData, Int $objectId, \cmsGroups $cmsGroups, Array $defaultData = []){
            unset($objectData['submit']); // Prevent POST

            $prefixEngine = new \prefixEngine($this->database);

            $objectData["url"] = $prefixEngine->title2pagename( (empty($objectData["url"])) ? $objectData["title"] : $objectData["url"] );
            $objectData["createdDate"] = ($objectId === 0) ? date("Y-m-d H:i:s") : $defaultData["createdDate"];
            $objectData["updatedDate"] = date("Y-m-d H:i:s");

            if($objectId !== 0){
                $objectData["url"] = ($objectData["url"] == $defaultData["defaultLang"]["url"]) ? $prefixEngine->title2pagename($objectData["title"]) : $objectData["url"];
            }
            $objectData["url"] = $cmsGroups->checkUrlDuplicity($defaultData["id"], $objectData["url"]);

            $this->objectData = $objectData;
            $this->objectId = $objectId;
        }
        public function compileGroup(\cmsGroups $cmsGroups, String $langShort, Bool $enableTranslate, Array $defaultData = []){
            if(!empty($this->objectData)){
                $this->dataCompiler->setTables($cmsGroups->groupsTable, null);
                $this->dataCompiler->setData($this->objectData, $cmsGroups->translateRows, $cmsGroups->dataRows);
                $this->dataCompiler->setTranslate($enableTranslate);

                if($this->objectId === 0){
                    $this->objectId = $this->dataCompiler->addQueryAndTranslate();
                }else{
                    $this->dataCompiler->setDefaultData($defaultData["defaultLang"]["url"]);
                    $this->dataCompiler->updateQueryAndTranslate($this->objectId, $cmsGroups->translateType);
                }

                $mediaController = $this->appController->returnMediaController();
                $this->dataCompiler->updateImages($mediaController->translateTable);
                $this->dataCompiler->processContentBlocks($this->objectData);;
                $this->dataCompiler->setLog($this->objectId, 'contentEditor', 'PostGroups', $langShort);
            }

            return $this->dataCompiler;
        }

    // Blog post labels group
        public function buildLabelGroup(Array $objectData, Int $objectId){
            unset($objectData['submit']); // Prevent POST

            $prefixEngine = new \prefixEngine($this->database);

            $this->objectData["title"] = $objectData["title"];
            $this->objectData["shortTag"] = $prefixEngine->title2pagename((empty($objectData["shortTag"])) ? $objectData["title"] : $objectData["shortTag"]);

            if($objectId !== 0){
                $this->objectData["id"] = $objectId;
            }

            $this->objectId = $objectId;
        }
        public function compileLabelGroup(\cmsPostBlog $cmsPostBlog, String $langShort, Bool $enableTranslate){
            if(!empty($this->objectData)){
                $this->dataCompiler->setTables($cmsPostBlog->labelsGroupsTable, null);
                $this->dataCompiler->setData($this->objectData, ['title'], ['id', 'shortTag']);
                $this->dataCompiler->setTranslate($enableTranslate);

                if($this->objectId === 0){
                    $this->objectId = $this->dataCompiler->addQueryAndTranslate();
                }else{
                    $translateType = $this->appController->getModuleConfig('contentEditor', 'translateType')["postLabelGroup"];
                    $this->dataCompiler->updateQueryAndTranslate($this->objectId, $translateType);
                }

                $this->dataCompiler->setLog($this->objectId, 'contentEditor', 'PostLabelGroup', $langShort);
            }

            return $this->dataCompiler;
        }

    // Blog post label
        public function buildLabel(Array $objectData, Int $objectId, Int $groupId){
            unset($objectData['submit']); // Prevent POST

            $prefixEngine = new \prefixEngine($this->database);
            $this->objectData = $objectData;
            $this->objectData["groupId"] = $groupId;
            $this->objectData["shortTag"] = $prefixEngine->title2pagename((empty($objectData["shortTag"])) ? $objectData["title"] : $objectData["shortTag"]);

            $this->objectId = $objectId;
        }
        public function compileLabel(\cmsPostBlog $cmsPostBlog, String $langShort, Bool $enableTranslate){
            if(!empty($this->objectData)){
                $this->dataCompiler->setTables($cmsPostBlog->labelsTable, null);
                $this->dataCompiler->setData($this->objectData, ['title'], ['groupId', 'shortTag', 'color']);
                $this->dataCompiler->setTranslate($enableTranslate);

                if($this->objectId === 0){
                    $this->objectId = $this->dataCompiler->addQueryAndTranslate();
                }else{
                    $translateType = $this->appController->getModuleConfig('contentEditor', 'translateType')["postLabel"];
                    $this->dataCompiler->updateQueryAndTranslate($this->objectId, $translateType);
                }

                $this->dataCompiler->setLog($this->objectId, 'contentEditor', 'PostLabel', $langShort);
            }

            return $this->dataCompiler;
        }

    // HELPERS
        private function updatePostCategories($objectId, $langShort){
            if($this->dataCompiler->nextStep === true){
                $cmsCategories = new \cmsCategories($this->database, $this->appController);
                $pageAction = (($this->objectId === 0) ? "add" : "edit");

                if(isset($this->dataCompiler->data["other"]["postCategories"])){
                    if($this->dataCompiler->data["data"]["postType"] == 2){
                        if($pageAction == "add"){
                            $addCategories = $cmsCategories->addSelectedCategories($objectId, $this->dataCompiler->data["other"]["postCategories"]);
                            if($addCategories === false){
                                $this->dataCompiler->setMessage(translate("mod-contentEditor-posts-error-cantCategory"));
                            }
                            $this->dataCompiler->setContinue(($addCategories === false) ? false : true);
                        }else{
                            $postCateg1 = $cmsCategories->getSelectedCategories($objectId, $langShort);
                            $subCont = true;
                            if($postCateg1 != $this->dataCompiler->data["other"]["postCategories"]){
                                $cmsCategories->deleteSelectedCategories($objectId);
                                $addCategories = $cmsCategories->addSelectedCategories($objectId, $this->dataCompiler->data["other"]["postCategories"]);
                                if($addCategories === false){
                                    $this->dataCompiler->setMessage(translate("mod-contentEditor-posts-error-cantCategory"));
                                }
                                $subCont = ($addCategories === false) ? false : true;
                            }
                            $this->dataCompiler->setContinue(($subCont === false) ? false : true);
                        }
                    }else{
                        $cmsCategories->deleteSelectedCategories($objectId); // If postType IS NOT BLOG POST, DELETE all categories (for sure)
                    }
                }else{
                    $cmsCategories->deleteSelectedCategories($objectId);
                }
            }
        }
        private function updatePostGroups($objectId){
            if($this->dataCompiler->nextStep === true){
                $cmsGroups = new \cmsGroups($this->database, $this->appController);
                $pageAction = (($this->objectId === 0) ? "add" : "edit");

                if(isset($this->dataCompiler->data["other"]["postGroups"])){
                    if($this->dataCompiler->data["data"]["postType"] != 2){ // NOT BLOG POST
                        if($pageAction == "add"){
                            $addGroups = $cmsGroups->addSelectedGroups($objectId, $this->dataCompiler->data["other"]["postGroups"]);
                            if($addGroups === false){
                                $this->dataCompiler->setMessage(translate("mod-contentEditor-posts-error-cantGroups"));
                            }
                            $this->dataCompiler->setContinue(($addGroups === false) ? false : true);
                        }else{
                            $postGroup1 = $cmsGroups->getSelectedGroups($objectId);
                            $subCont = true;
                            if($postGroup1 != $this->dataCompiler->data["other"]["postGroups"]){
                                $cmsGroups->deleteSelectedGroups($objectId);
                                $addGroups = $cmsGroups->addSelectedGroups($objectId, $this->dataCompiler->data["other"]["postGroups"]);
                                if($addGroups === false){
                                    $this->dataCompiler->setMessage(translate("mod-contentEditor-posts-error-cantGroups"));
                                }
                                $subCont = ($addGroups === false) ? false : true;
                            }
                            $this->dataCompiler->setContinue(($subCont === false) ? false : true);
                        }
                    }else{
                        $cmsGroups->deleteSelectedGroups($objectId); // If postType IS BLOG POST, DELETE all groups (for sure)
                    }
                }else{
                    $cmsGroups->deleteSelectedGroups($objectId);
                }
            }
        }
        private function updatePostLabels(\cmsPostBlog $cmsPostBlog, $objectId){
            if($this->dataCompiler->nextStep === true){
                $cmsPostBlog->setLabels($objectId, $this->dataCompiler->data["other"]["postLabels"]);
            }
        }
    }