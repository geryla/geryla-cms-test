<?php
class cmsGroups {
    private $database;
    private $appController;
    private $localizationClass;

    private $templateClass;
    private $mediaProcessor;
    private $CMS;
    private $contentBlocks;

    private $langShort;
    private $langFile;

    public $translateType;
    public $mediaNameGroup;
    public $groupsTable;
    public $crossGroupTable;

    public $translateRows = ["title", "shortDesc", "description", "metaKeywords", "metaTitle", "metaDescription", "url"]; // Only editable fields
    public $dataRows = ["parentId", "allowIndex", "className", "templateName", "shortTag", "createdDate", "updatedDate", "orderNum", "visibility"]; // Only editable fields

    public function __construct(Database $database, AppController $appController) {
        $this->database = $database;
        $this->appController = $appController;
        $this->localizationClass = $appController->returnLocalizationClass();
        
        $this->langShort = $appController->getLang();
        $this->langFile = $appController->getLangFile();

        $this->groupsTable = databaseTables::getTable('contentEditor', "groupTable");
        $this->crossGroupTable = databaseTables::getTable('contentEditor', "crossGroupTable");
        $this->translateType = $this->appController->getModuleConfig('contentEditor', 'translateType')["postGroup"];
        $this->mediaNameGroup = $this->appController->getModuleConfig('contentEditor', 'mediaSettings')["postGroup"];
    }

/* ================== Application functions === */
    public function optimize($data, $imageSize = null, $parameters = []) {
        $optimizedData = [];
        $requestAllParameters = (in_array("all", $parameters));

        if(!empty($data)){
            if(isset($data["id"])){
                $optimizedData = $this->utilize($data, $imageSize, $parameters, $requestAllParameters);
            }else{
                foreach($data as $key => $value){
                    $optimizedData[$key] = (!empty($value)) ? $this->utilize($value, $imageSize, $parameters, $requestAllParameters) : $value;
                }
            }
        }

        return $optimizedData;
    }
    private function utilize($groupInfo, $imageSize = null, $optimizeValues = [], $requestAllParameters = false) {
        ($this->templateClass === null) ? $this->templateClass = $this->appController->returnTemplatesClass() : null;
        ($this->mediaProcessor === null) ? $this->mediaProcessor = $this->appController->returnMediaProcessor() : null;

        $templatesUrls = $this->templateClass->allTemplates;

        $optimizedInfo = $groupInfo;
        if(!empty($groupInfo["templateName"])){
            $optimizedInfo["path"] = '/' . $templatesUrls[$groupInfo["templateName"]] . '/' . $groupInfo["url"];
        }

        if ($requestAllParameters || in_array("images", $optimizeValues)) {
            $optimizedInfo["images"] = $this->mediaProcessor->getAllObjectImages($groupInfo["id"], $this->mediaNameGroup, $this->langShort, $imageSize, true);
        }
        if ($requestAllParameters || in_array("groups", $optimizeValues)) {
            $childGroups = $this->getAllChildGroups($groupInfo["id"], $this->langShort);
            $optimizedInfo["groups"] = (!empty($childGroups)) ? $this->optimize($childGroups, $imageSize, $optimizeValues) : [];
        }
        if ($requestAllParameters || in_array("contentBlocks", $optimizeValues)) {
            if($this->appController->isActiveModule('contentBlocks')){
                ($this->contentBlocks === null) ? $this->contentBlocks = new ContentBlocks($this->database, $this->appController) : null;
                $optimizedInfo["contentBlocks"] = $this->contentBlocks->getObjectBlocks($optimizedInfo["id"], 'groups');
            }
        }


        $entity = new EntityCmsGroup($optimizedInfo);
        return $entity->get();
    }


    public function getOneGroupById($id, $langShort, $visibility = null) {
        $visibility = ($visibility === null) ? null : (($visibility === false) ? "AND visibility = 0" : "AND visibility = 1");

        $sql = "SELECT * FROM `{$this->groupsTable}` WHERE id = :id {$visibility};";
        $returnData = $this->localizationClass->translateResult($this->database->getQuery($sql, ["id" => $id], false), $this->translateType, $langShort);

        return $returnData;
    }
    public function getOneGroupByShortTag($shortTag, $langShort, $visibility = null) {
        $visibility = ($visibility === null) ? null : (($visibility === false) ? "AND visibility = 0" : "AND visibility = 1");

        $sql = "SELECT * FROM `{$this->groupsTable}` WHERE shortTag = :shortTag {$visibility};";
        $returnData = $this->localizationClass->translateResult($this->database->getQuery($sql, ["shortTag" => $shortTag], false), $this->translateType, $langShort);

        return $returnData;
    }
    public function getOneGroupByClassName($className, $langShort, $visibility = null) {
        $visibility = ($visibility === null) ? null : (($visibility === false) ? "AND visibility = 0" : "AND visibility = 1");

        $sql = "SELECT * FROM `{$this->groupsTable}` WHERE className = :className {$visibility};";
        $returnData = $this->localizationClass->translateResult($this->database->getQuery($sql, ["className" => $className], false), $this->translateType, $langShort);

        return $returnData;
    }
    public function getOneGroupByUrl($url, $langShort, $visibility = null) {
        $visibility = ($visibility === null) ? null : (($visibility === false) ? "AND visibility = 0" : "AND visibility = 1");

        $sql = "SELECT * FROM `{$this->groupsTable}` WHERE url = :url {$visibility} LIMIT 1;";
        $returnData = $this->localizationClass->translateResult($this->database->getQuery($sql, ["url" => $url], false), $this->translateType, $langShort);

        return $returnData;
    }

    public function getAllGroupsCross($postId, $simplify = true, $langShort = null, $visibility = null) {
        $visibility = ($visibility === null) ? null : (($visibility === false) ? "AND visibility = 0" : "AND visibility = 1");

        $sql = "SELECT * FROM `{$this->crossGroupTable}` WHERE postId = :postId {$visibility} ORDER BY groupId;";
        $returnData = $this->database->getQuery($sql, ["postId" => $postId], true);

        if($simplify === false && $langShort !== null){
            if(!empty($returnData)){
                $optimizedData = [];
                foreach($returnData as $group){
                    $groupInfo = $this->getOneGroupById($group["groupId"], $langShort);
                    $optimizedData[] = $this->localizationClass->translateResult($groupInfo, $this->translateType, $langShort);
                }
                $returnData = $optimizedData;
            }
        }

        return $returnData;
    }

    public function getAllGroups($langShort, $visibility = null) {
        $visibility = ($visibility === null) ? null : (($visibility === false) ? "WHERE visibility = 0" : "WHERE visibility = 1");

        $sql = "SELECT * FROM `{$this->groupsTable}` {$visibility};";
        $returnData = $this->localizationClass->translateResult($this->database->getQuery($sql, null, true), $this->translateType, $langShort);

        return $returnData;
    }
    public function getAllChildGroups($id, $langShort, $visibility = null, $customOrder = null) {
        $visibility = ($visibility === null) ? null : (($visibility === false) ? "AND visibility = 0" : "AND visibility = 1");
        $orderBy = ($customOrder === null) ? 'ORDER BY orderNum' : $customOrder;

        $sql = "SELECT * FROM `{$this->groupsTable}` WHERE parentId = :id {$visibility} {$orderBy};";

        $returnData = $this->localizationClass->translateResult($this->database->getQuery($sql, ["id" => $id], true), $this->translateType, $langShort);
        return $returnData;
    }


// Add all post groups
    public function addSelectedGroups($postId, $crossArray) {
        foreach ($crossArray as $key => $value) {
            $sql = "INSERT INTO `{$this->crossGroupTable}`";
            $insert = (int)$this->database->insertQuery($sql, array("postId" => $postId, "groupId" => $value));
            if ($insert == 0) {
                return false;
            }
        }
        return true;
    }
// Check if URL address exist
    public function checkUrlExist($url) {
        $sql = "SELECT * FROM `{$this->groupsTable}` WHERE url = :url;";
        $returnData = $this->database->getQuery($sql, array("url" => $url), false);

        return (empty($returnData)) ? false : $returnData;
    }
// Check if URL is not exits
    public function checkUrlDuplicity($id, $urlForCheck) {
        $urlCheck = $urlForCheck;
        $urlControl = $this->checkUrlExist($urlCheck);

        if ($urlControl !== false) {
            $urlExist = ($id == $urlControl["id"]) ? false : true;
        } else {
            $urlExist = $urlControl;
        }

        if ($urlExist === true) {
            $urlCount = 1;
            do {
                $urlCheck = $urlForCheck . '-' . $urlCount;
                $urlExist = $this->checkUrlExist($urlCheck);
                $urlCount++;
            } while ($urlExist !== false);
        }

        return $urlCheck;
    }
// Select all post groups  
    public function getSelectedGroups($postId) {
        $allPostCategories = $this->getAllGroupsCross($postId);
        $allCategories = [];

        foreach ($allPostCategories as $postCategory) {
            $allCategories[] = $postCategory["groupId"];
        }

        return $allCategories;
    }
// Delete all post groups
    public function deleteSelectedGroups($postId) {
        $sql = "DELETE FROM `{$this->crossGroupTable}`";
        $whereSql = "WHERE postId = :id";
        $this->database->deleteQuery($sql, array("id" => $postId), $whereSql);

        return true;
    }
// Select all groups as checkboxes
    public function selectAllGroupsAsCheckboxTree($groupId, $postId, $repeat, $langShort) {

        $allGroups = $this->getAllChildGroups($groupId, $langShort);

        if ($allGroups) {
            $groupsIds = $this->getSelectedGroups($postId);

            $i = count($allGroups);
            $count = $i;
            $checkBoxTree = null;

            foreach ($allGroups as $key => $group) {
                $groupId = $group["id"];
                $subGroups = $this->getAllChildGroups($groupId, $langShort);
                $selected = (in_array($groupId, $groupsIds)) ? "checked" : "";

                if ($repeat != 0 AND $i == $count)
                    $checkBoxTree .= '<ul class="subcategory">';
                $checkBoxTree .= '<li id="' . $groupId . '">';
                    $checkBoxTree .= (!empty($subGroups)) ? '<i class="fa fa-plus-square-o fa-minus-square-o"></i>' : '<i class="fa fa-circle-thin"></i>';
                        $checkBoxTree .= '<div class="customInput inline checkbox">';
                            $checkBoxTree .= '<input type="checkbox" name="postGroups[]" class="checkboxStyled" id="gro' . $groupId . '" value="' . $groupId . '" ' . $selected . '>';
                            $checkBoxTree .= '<label for="gro' . $groupId . '"><span>' . $group["title"] . '</span></label>';
                        $checkBoxTree .= '</div>';
                    $checkBoxTree .= (!empty($subGroups)) ? $this->selectAllGroupsAsCheckboxTree($groupId, $postId, $repeat + 1, $langShort) : "";
                $checkBoxTree .= '</li>';
                $i--;
                if ($repeat != 0 AND $i == 0)
                    $checkBoxTree .= '</ul>';
            }
        } else {
            $checkBoxTree = translate("mod-contentEditor-groups-detail-groups-no");
        }

        return $checkBoxTree;
    }
// Select all groups as select box
    public function selectAllGroupsAsSelectBoxTree($groupPosition, $activeGroupId, $repeat, $langShort) {

        $allGroups = $this->getAllChildGroups($groupPosition, $langShort);
        $selectBoxTree = null;

        if ($allGroups) {
            $oneGroup = $this->getOneGroupById($activeGroupId, $langShort);

            foreach ($allGroups as $group) {
                if($activeGroupId == $group["id"]){ continue; }

                $selected = ($oneGroup["parentId"] == $group["id"]) ? "selected" : "";
                $selectBoxTree .= '<option value="' . $group["id"] . '" ' . $selected . '>';
                for ($i = 0; $i < $repeat; $i++) {
                    $selectBoxTree .= '-&nbsp;';
                }
                $selectBoxTree .= $group["title"];
                $selectBoxTree .= '</option>';
                $selectBoxTree .= $this->selectAllGroupsAsSelectBoxTree($group["id"], $activeGroupId, $repeat + 1, $langShort);
            }
        }
        return $selectBoxTree;
    }
// Delete post groups
    public function deletePostGroupsCross($objectId) {
        $sql = "DELETE FROM `{$this->groupsTable}`";
        $whereSql = "WHERE groupId = :id";
        $returnData = $this->database->deleteQuery($sql, array("id" => $objectId), $whereSql);
        return $returnData;
    }
// Replace child groups by parentId = 0
    public function replaceChildGroups($objectId) {
        $sqlQuery = "UPDATE `{$this->groupsTable}`";
        $whereSql = "WHERE parentId = :id";
        $done = $this->database->updateQuery($sqlQuery, array("parentId" => 0), array("id" => $objectId), $whereSql);

        return ($done !== true) ? false : true;
    }
// GET options for selectbox in HOOK MENU
    public function getTemplateForGroup($langTableName, $lang, $selectedName) {
        ($this->CMS === null) ? $this->CMS = new CMS($this->database, $this->appController) : null;

        $allContent = $this->CMS->getAllContentFromOtherTable($this->database->injectionProtect($langTableName), $lang);
        $return = null;

        if (!empty($allContent)) {
            foreach ($allContent as $content) {
                $selected = ($selectedName == $content["templateName"]) ? "selected" : "";
                $return .= '<option value="' . $content["templateName"] . '" ' . $selected . '>' . $content["title"] . '</option>';
            }
        }

        return $return;
    }
// Get all URL addresses from groups with template
    public function getAllGroupsAnchors($langShort, $onlyVisible = false) {
        $allGroups = $this->getAllGroups($langShort);
        $allAnchors = [];

        if (!empty($allGroups)) {
            foreach ($allGroups as $group) {
                if (!empty($group["templateName"])) {
                    $groupInfo = $this->optimize($group);
                    $groupName = (!empty($groupInfo["shortTag"])) ? $groupInfo["shortTag"] : $groupInfo["className"];
                    $groupName = (empty($groupName)) ? $groupInfo["url"] : $groupName;

                    if ($onlyVisible === false OR ($onlyVisible === true && $groupInfo["visibility"] == 1)) {
                        $allAnchors["group_" . $groupName] = mb_substr($groupInfo["path"], 1, null, 'utf-8'); // Remove first "/" from path
                    }

                }

            }
        }

        return $allAnchors;
    }
// Return array of all groups nested by child/parent
    public function getAllGroupsAsNestedArray($groupId, $langShort, $level = 0, $flag = true) {
        $allGroups = $this->getAllChildGroups($groupId, $langShort);
        $returnArray = [];

        if ($allGroups) {
            foreach ($allGroups as $key => $group) {
                $groupTitle = ($flag === true && $level > 0) ? str_repeat('-', $level).' '.$group["title"] : $group["title"];
                $returnArray[] = ["value" => $group["id"], "title" => $groupTitle];
                $returnArray = array_merge($returnArray, $this->getAllGroupsAsNestedArray($group["id"], $langShort, ($level+1), $flag));
            }
        }

        return $returnArray;
    }

// Select all visible child groups
    public function getAllGroupsByTemplate($langShort, $assignedTemplate, $customOrder = null) {
        $orderBy = ($customOrder === null) ? 'ORDER BY orderNum' : $customOrder;
        $sql = "SELECT * FROM `{$this->groupsTable}` WHERE visibility = 1 AND templateName = :template {$orderBy};";

        $returnData = $this->localizationClass->translateResult($this->database->getQuery($sql, ["template" => $assignedTemplate], true), $this->translateType, $langShort);
        return $returnData;
    }
}