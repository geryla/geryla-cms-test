<?php
class cmsPostBlog extends cmsPost {

    private $cmsCategory;
    private $contentBlocks;

    public $activeCategory = null;
    public $allAnchors = null;
    public $languageType = null;

    public function optimize(Array $data, ?String $langShort = null, Array $options = [], Bool $cache = true) {
        $options["deepLevel"] = (isset($options["deepLevel"])) ? $options["deepLevel"] + 1 : 0;
        $options["with"] = (isset($options["with"])) ? $options["with"] : ['all'];
        $options["itemThumbnail"] = (isset($options["itemThumbnail"]) && trim($options["itemThumbnail"]) !== "") ? $options["itemThumbnail"] : null;

        $langShort = ($langShort === null) ? $this->appController->getLang() : $langShort;
        $isSingleItem = (isset($data["id"]));
        $dataArray = ($isSingleItem === true) ? [$data] : $data;

        $optimizedData = [];
        if(!empty($dataArray)){
            foreach($dataArray as $key => $item){
                $optimizedData[$key] = (!empty($item)) ? $this->cacheItem($item, $langShort, $options, $cache) : [];
            }
        }

        return ($isSingleItem === true) ? $optimizedData[0] : $optimizedData;
    }
    public function cacheItem(Array $item, ?String $langShort = null, Array $options = [], Bool $cache = true) {
        $entityCache = new EntityCache();
        $entityName = 'post';

        $responseData = ($cache === true) ? $entityCache->get($entityName, $item["id"], $langShort) : null;
        if($responseData === null){
            $options["with"] = ["all"];
            $responseData = $this->utilizeBlog($item, $langShort, $options);
            if($cache === true){
                $entityCache->set($entityName, $item["id"], $langShort, $responseData);
            }
        }

        $imagesEntity = "images/{$entityName}/{$item['id']}";
        $imagesResponse = ($cache === true) ? $entityCache->get($imagesEntity, $options["itemThumbnail"], $langShort, null, false) : null;
        if($imagesResponse === null){
            $imagesResponse = $this->mediaProcessor->getAllObjectImages($item["id"], $this->mediaNamePost, $langShort, $options["itemThumbnail"], true);
            $entityCache->set($imagesEntity, $options["itemThumbnail"], $langShort, $imagesResponse, null, false);
        }
        $responseData["images"] = $imagesResponse;

        $entity = new EntityCmsBlogPost($responseData);
        $postCache = $entity->get();

        return $postCache;
    }

    private function utilizeBlog(Array $item, ?String $langShort = null, Array $options = []) {
        ($this->templateClass === null) ? $this->templateClass = $this->appController->returnTemplatesClass() : null;

        $allAnchors = $this->templateClass->allTemplates;
        $langShort = ($langShort === null) ? $this->appController->getLang() : $langShort;
        $requestAllParameters = (array_key_exists('with', $options) && in_array('all', $options["with"]));
        $entityBonds = []; //TODO: $entityBonds - create minor bonds and delete entity by them

        $blogDetailUrl = (isset($options["uniteUrls"])) ? $allAnchors["blog"] : $allAnchors["blogDetail"];
        $blogCategoryUrl = (isset($options["uniteUrls"])) ? $allAnchors["blog"] :  $allAnchors["blogCategory"];

        // Default
        $itemOptimized = $item;
        $itemOptimized["path"] = "/{$blogDetailUrl}/{$item['urlPrefix']}-{$item['url']}";
        $itemOptimized["date"] = date("d. m. Y", strtotime($item["createdDate"]));
        $itemOptimized["time"] = date("H:i:s", strtotime($item["createdDate"]));

        $itemOptimized["perex"] = mb_substr(preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", strip_tags(html_entity_decode($item["description"]))), 0, 255, 'utf-8');
        
        if ($requestAllParameters || in_array("categories", $options["with"])) {
            ($this->cmsCategory === null) ? $this->cmsCategory = new cmsCategories($this->database, $this->appController) : null;
            $allPostCategories = [];
            $allCategories = $this->cmsCategory->getAllCategoriesCross($item["id"], $langShort);
            if ($allCategories) {
                foreach ($allCategories as $categoryKey => $category) {
                    $allPostCategories[$categoryKey] = $this->cmsCategory->getOneCategoryById($category["categoryId"], $langShort);
                    $allPostCategories[$categoryKey]["path"] = '/'.$blogCategoryUrl.'/'.$allPostCategories[$categoryKey]["url"];
                    $allPostCategories[$categoryKey]["images"] = $this->mediaProcessor->getAllObjectImages($category["id"], $this->mediaNameCategory, $langShort, null, true);
                }
            }

            $itemOptimized["categories"] = $allPostCategories;
        }
        /*if ($requestAllParameters || in_array("author", $options["with"])) {
            ($this->adminEngine === null) ? $this->adminEngine = new adminEngine($this->database, $this->appController->returnLocaleEngine()) : null;
            ($this->moduleAdmins === null) ? $this->moduleAdmins = new moduleAdmins($this->adminEngine, $this->appController->returnLocalizationClass()) : null;

            $authorInfo = (array) $this->moduleAdmins->getOneAdmin($item["authorId"]);
            if(!empty($authorInfo)){
                $itemOptimized["author"] = $authorInfo;
                $authorThumbnail = (array_key_exists('authorThumbnail', $options)) ? $options["authorThumbnail"] : 'authorThumbnail';
                $itemOptimized["author"]["image"] = (array) $this->mediaProcessor->getMainImage((int) $authorInfo["id"], $this->moduleAdmins->adminMedia, $langShort, $authorThumbnail);
            }
        }*/
        if ($requestAllParameters || in_array("files", $options["with"])) {
            $itemOptimized["files"] =  (array) $this->mediaProcessor->getAllObjectMediaByType($item["id"], $this->mediaNamePost, $this->langShort, ['audio', 'video', 'file']);
        }
        if ($requestAllParameters || in_array("labels", $options["with"])) {
            $allLabels = $this->getAllPostLabels($item["id"], true, $langShort);
            $itemOptimized["labels"] = $this->groupPostLabels($allLabels, $langShort);
        }

        if ($requestAllParameters || in_array("contentBlocks", $options["with"])) {
            if($this->appController->isActiveModule('contentBlocks')){
                ($this->contentBlocks === null) ? $this->contentBlocks = new ContentBlocks($this->database, $this->appController) : null;
                $itemOptimized["contentBlocks"] = $this->contentBlocks->getObjectBlocks($item["id"], 'posts');
            }
        }

        return $itemOptimized;
    }

    public function setCategory($breadCrumbs) {
        $breadCrumbs = array_reverse((array)$breadCrumbs);
        if ($breadCrumbs[0]->template == "blogCategory") {
            $this->activeCategory = (array)$breadCrumbs[0];
        }
    }
    public function setAnchors($allAnchors) {
        $this->allAnchors = $allAnchors;
    }

    public function getOnePost($postId, $langShort, $visibility = null) {
        $visibility = ($visibility === null) ? null : (($visibility === false) ? "AND visibility = 0" : "AND visibility = 1");
        $sqlQuery = "SELECT * FROM `{$this->postsTable}` WHERE postType = 2 AND id = :id {$visibility};";
        $langColumns = "title, shortDesc, description, metaKeywords, metaTitle, metaDescription, url, langShort";

        $returnData = $this->localizationClass->getTranslation($this->database->getQuery($sqlQuery, ["id" => $postId], false), $langColumns, $this->postsTranslateTable, $langShort);
        return $this->optimize($returnData, null);
    }
    public function getAllPosts($langShort, $visibility = null, $orderByDate = false, $limit = null, $offset = null, $exceptOneId = null) {
        $visibility = ($visibility === null) ? null : (($visibility === false) ? "AND visibility = 0" : "AND visibility = 1");
        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;
        $orderBy = ($orderByDate === false) ? "orderNum ASC" : "createdDate DESC";
        $exceptOne = ($exceptOneId !== null) ? "AND data.id != $exceptOneId" : null;

        $sqlQuery = "SELECT * FROM `{$this->postsTable}` WHERE postType = 2 {$visibility} {$exceptOne} ORDER BY {$orderBy} {$limit} {$offset};";
        $langColumns = "title, shortDesc, description, metaKeywords, metaTitle, metaDescription, url, langShort";

        $returnData = $this->localizationClass->getTranslation($this->database->getQuery($sqlQuery, null, true), $langColumns, $this->postsTranslateTable, $langShort);
        return $returnData;
    }
    public function getAllPostsByLang($langShort, $visibility = null, $orderByDate = false, $limit = null, $offset = null, $exceptOneId = null) {
        $visibility = ($visibility === null) ? null : (($visibility === false) ? "AND data.visibility = 0" : "AND data.visibility = 1");
        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;
        $orderBy = ($orderByDate === false) ? "orderNum ASC" : "createdDate DESC";
        $exceptOne = ($exceptOneId !== null) ? "AND data.id != $exceptOneId" : null;

        $sqlQuery = "SELECT language.*, data.* FROM `{$this->postsTable}` as data INNER JOIN `{$this->postsTranslateTable}` as language ON data.id = language.associatedId WHERE language.langShort = :langShort AND data.postType = 2 {$visibility} {$exceptOne} ORDER BY {$orderBy} {$limit} {$offset};";
        $returnData = $this->database->getQuery($sqlQuery, ["langShort" => $langShort], true);
        return $returnData;
    }
    public function getAllPostsByCategory($categoryId, $langShort, $visibility = null, $limit = null, $offset = null, $orderBy = 'orderNum', $orderDirection = 'DESC', $exceptOneId = null) {
        $visibility = ($visibility === null) ? null : (($visibility === false) ? "AND visibility = 0" : "AND visibility = 1");
        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;
        $exceptOne = ($exceptOneId !== null) ? "AND data.id != $exceptOneId" : null;

        $sqlQuery = "SELECT data.* FROM `{$this->postsTable}` AS data INNER JOIN `{$this->crossTable}` AS postCategories ON data.id = postCategories.postId WHERE postCategories.categoryId = :categoryId AND data.postType = 2 {$visibility} {$exceptOne} ORDER BY {$orderBy} {$orderDirection} {$limit} {$offset};";
        $langColumns = "title, shortDesc, description, metaKeywords, metaTitle, metaDescription, url, langShort";

        $returnData = $this->localizationClass->getTranslation($this->database->getQuery($sqlQuery, ["categoryId" => $categoryId], true), $langColumns, $this->postsTranslateTable, $langShort);
        return $returnData;
    }
    public function getAllPostsByCategoryByLang($categoryId, $langShort, $visibility = null, $limit = null, $offset = null, $orderBy = 'orderNum', $orderDirection = 'DESC', $exceptOneId = null) {
        $visibility = ($visibility === null) ? null : (($visibility === false) ? "AND data.visibility = 0" : "AND data.visibility = 1");
        $limit = ($limit === null) ? null : "LIMIT " . $limit;
        $offset = ($offset === null) ? null : "OFFSET " . $offset;
        $exceptOne = ($exceptOneId !== null) ? "AND data.id != $exceptOneId" : null;

        $sqlQuery = "SELECT language.*, data.* FROM `{$this->postsTable}` as data INNER JOIN `{$this->postsTranslateTable}` as language ON data.id = language.associatedId INNER JOIN `{$this->crossTable}` AS postCategories ON data.id = postCategories.postId WHERE language.langShort = :langShort AND postCategories.categoryId = :categoryId AND data.postType = 2 {$visibility} {$exceptOne} ORDER BY {$orderBy} {$orderDirection} {$limit} {$offset};";
        $returnData = $this->database->getQuery($sqlQuery, ["categoryId" => $categoryId, "langShort" => $langShort], true);
        return $returnData;
    }

    public function getOneLabelGroup(Int $groupId, ?String $langShort = null){
        $returnData = $this->database->getQuery("SELECT * FROM `{$this->labelsGroupsTable}` WHERE id = :id;", ["id" => $groupId], false);
        if($langShort !== null && !empty($returnData)){
            $translateType = $this->appController->getModuleConfig('contentEditor', 'translateType')["postLabelGroup"];
            $returnData = $this->localizationClass->translateResult($returnData, $translateType, $langShort);
        }

        return $returnData;
    }
    public function getAllLabelsGroups(?String $langShort = null){
        $returnData = $this->database->getQuery("SELECT * FROM `{$this->labelsGroupsTable}`;", null, true);
        if($langShort !== null && !empty($returnData)){
            $translateType = $this->appController->getModuleConfig('contentEditor', 'translateType')["postLabelGroup"];
            $returnData = $this->localizationClass->translateResult($returnData, $translateType, $langShort);
        }

        return $returnData;
    }
    public function getAllLabels(?Int $groupId = null, ?String $langShort = null){
        $whereBind = [];
        $whereSql = null;
        if($groupId !== null){
            $whereSql = "WHERE groupId = :groupId";
            $whereBind["groupId"] = $groupId;
        }

        $returnData = $this->database->getQuery("SELECT * FROM `{$this->labelsTable}` {$whereSql};", $whereBind, true);
        if($langShort !== null && !empty($returnData)){
            $translateType = $this->appController->getModuleConfig('contentEditor', 'translateType')["postLabel"];
            $returnData = $this->localizationClass->translateResult($returnData, $translateType, $langShort);
        }

        return $returnData;
    }
    public function getOneLabel(Int $labelId, ?String $langShort = null){
        $returnData = $this->database->getQuery("SELECT * FROM `{$this->labelsTable}` WHERE id = :labelId;", ["labelId" => $labelId], false);
        if($langShort !== null && !empty($returnData)){
            $translateType = $this->appController->getModuleConfig('contentEditor', 'translateType')["postLabel"];
            $returnData = $this->localizationClass->translateResult($returnData, $translateType, $langShort);
        }

        return $returnData;
    }

    public function getAllPostLabels(Int $postId, Bool $extendHooks = false, ?String $langShort = null) {
        if($extendHooks === false){
            $sqlQuery = "SELECT * FROM `{$this->labelsHookTable}` WHERE postId = :postId;";
        }else{
            $sqlQuery = "SELECT label.* FROM `{$this->labelsHookTable}` AS hook INNER JOIN `{$this->labelsTable}` as label ON hook.labelId = label.id WHERE hook.postId = :postId;";
        }

        $returnData = $this->database->getQuery($sqlQuery, ["postId" => $postId], true);
        if(!empty($returnData) && $extendHooks === true && $langShort !== null){
            $translateType = $this->appController->getModuleConfig('contentEditor', 'translateType')["postLabel"];
            $returnData = $this->localizationClass->translateResult($returnData, $translateType, $langShort);
        }

        return $returnData;
    }
    public function getAllCategoryLabels($langShort, $categoryId, $visibleOnly = true){
        $whereCategoryCond = ($categoryId !== 0) ? "WHERE hook.categoryId = :categoryId" : null;
        $whereBind = ($categoryId !== 0) ? ["categoryId" => $categoryId] : null;
        if($visibleOnly === true){
            $visibilityInner = "INNER JOIN `{$this->postsTable}` AS post ON labelHook.postId = post.id";
            $whereCategoryCond .= ($whereCategoryCond === null) ? "WHERE " : " AND ";
            $whereCategoryCond .= "post.visibility = 1 AND post.postType = 2";
        }

        $sqlQuery = "
          SELECT labels.* FROM `{$this->labelsTable}` AS labels 
          INNER JOIN `{$this->labelsHookTable}` AS labelHook ON labels.id = labelHook.labelId
          INNER JOIN `{$this->crossTable}` AS hook ON labelHook.postId = hook.postId 
          {$visibilityInner} {$whereCategoryCond} GROUP BY labels.shortTag;";

        $translateType = $this->appController->getModuleConfig('contentEditor', 'translateType')["postLabel"];
        $returnData = $this->localizationClass->translateResult($this->database->getQuery($sqlQuery, $whereBind, true), $translateType, $langShort);
        return $returnData;
    }

// CMS + Widgets methods
    public function renderLabelsCheckboxes($postId, $langShort) {
        $checkBoxTree = null;

        $labelGroups = $this->getAllLabelsGroups($langShort);
        if (!empty($labelGroups)) {
            $hookedLabels = $this->getAllPostLabels($postId, false, null);
            foreach ($labelGroups as $group) {
                $labels = $this->getAllLabels($group["id"], $langShort);
                if(!empty($labels)){
                    $checkBoxTree .= '<h5>'.$group["title"].'</h5>';
                    $checkBoxTree .= '<ul class="subcategory">';
                        foreach($labels as $label){
                            $isSelected = (in_array($label["id"], array_column($hookedLabels, 'labelId'))) ? "checked" : "";

                            $checkBoxTree .= '<li id="'.$label["id"].'">';
                                $checkBoxTree .= '<div class="customInput inline checkbox">';
                                    $checkBoxTree .= '<input type="checkbox" name="postLabels[]" class="checkboxStyled" id="label_'.$label["id"].'" value="'.$label["id"].'" '.$isSelected.'>';
                                    $checkBoxTree .= '<label for="label_'.$label["id"].'"><span>'.$label["title"].'</span></label>';
                                $checkBoxTree .= '</div>';
                            $checkBoxTree .= '</li>';
                        }
                    $checkBoxTree .= '</ul>';
                }
            }
        } else {
            $checkBoxTree = translate("mod-contentEditor-admin-blog-labels-plc-noHookedLabels");
        }

        return $checkBoxTree;
    }
    public function setLabels(Int $postId, ?Array $postLabels = []) {
        $existingLabels = $this->getAllPostLabels($postId);
        $existingKeys = array_column($existingLabels, 'labelId', 'labelId');

        if(!empty($postLabels)){
            foreach($postLabels as $label){
                $isExisting = isset($existingKeys[$label]);

                if($isExisting === true){
                    unset($existingKeys[$label]);
                }else{
                    $this->database->insertQuery( "INSERT INTO `{$this->labelsHookTable}`", ["postId" => $postId, "labelId" => $label]);
                }
            }
        }

        if(!empty($existingKeys)){
            foreach($existingKeys as $label){
                $this->database->deleteQuery("DELETE FROM `{$this->labelsHookTable}`", ["postId" => $postId, "labelId" => $label], "WHERE postId = :postId AND labelId = :labelId");
            }
        }

        return true;
    }
    public function deleteLabel(Int $labelId, Bool $withHooks = false){
        $this->database->deleteQuery("DELETE FROM `{$this->labelsTable}`", ["labelId" => $labelId], "WHERE id = :labelId");
        if($withHooks === true){
            $this->database->deleteQuery("DELETE FROM `{$this->labelsHookTable}`", ["labelId" => $labelId], "WHERE labelId = :labelId");
        }
    }
    public function deleteLabelGroup(Int $groupId, Bool $withHooks = false){
        $this->database->deleteQuery("DELETE FROM `{$this->labelsGroupsTable}`", ["id" => $groupId], "WHERE id = :id");

        $existingLabels = $this->getAllLabels($groupId);
        if(!empty($existingLabels)){
            foreach($existingLabels as $label){
                $this->deleteLabel($label["id"], $withHooks);
            }
        }
    }
    public function groupPostLabels(Array $labels, String $langShort){
        $labelsGrouped = [];
        if (!empty($labels)) {
            $groupsData = $this->getAllLabelsGroups($langShort);
            $groupTitles = array_column($groupsData, 'title', 'id');
            $groupTags = array_column($groupsData, 'shortTag', 'id');

            $filtrationClass = new filtrationClass($this->database, $this->appController);
            foreach ($labels as $label){
                $labelData = $label;
                //$labelData["anchor"] = "?label[{$labelData['shortTag']}]=true";
                $labelName = "label[{$labelData['shortTag']}]";
                $labelData["anchor"] = $filtrationClass->getFilterValue(null, '/', null, $labelName, 'true');

                $labelsGrouped[$label["groupId"]]["id"] = $label["groupId"];
                $labelsGrouped[$label["groupId"]]["title"] = $groupTitles[$label["groupId"]];
                $labelsGrouped[$label["groupId"]]["shortTag"] = $groupTags[$label["groupId"]];
                $labelsGrouped[$label["groupId"]]["values"][] = $labelData;
            }
            $labelsGrouped = array_values($labelsGrouped);
        }

        return $labelsGrouped;
    }

    public function clearLabelRelatedCache(Int $labelId) {
        $existingArticles = $this->database->getQuery("SELECT * FROM `{$this->labelsHookTable}` WHERE labelId = :labelId;", ["labelId" => $labelId], true);
        if(!empty($existingArticles)){
            $cache = new Cache();
            $cache->removeUnsortedCache('Entity', 'post');
            foreach($existingArticles as $article){
                $cache->removeCache('Entity', "post/{$article["postId"]}");
            }
        }
    }

}