<?php
class cmsCategories {
    private $database;
    private $appController;
    private $localizationClass;
    
    private $templateClass;
    private $mediaProcessor;

    private $langShort;
    private $langFile;

    public $mediaNameCategory;
    public $categoriesTable;
    public $crossTable;
    public $translateType;

    public $translateRows = ["title", "shortDesc", "description", "metaKeywords", "metaTitle", "metaDescription", "url"]; // Editable fields
    public $dataRows = ["parentId", "allowIndex", "className", "createdDate", "updatedDate", "orderNum", "visibility"]; // Editable fields

    public function __construct(Database $database, AppController $appController) {
        $this->database = $database;
        $this->appController = $appController;
        $this->localizationClass = $appController->returnLocalizationClass();
        
        $this->langShort = $appController->getLang();
        $this->langFile = $appController->getLangFile();

        $this->categoriesTable = databaseTables::getTable('contentEditor', "categoryTable");
        $this->crossTable = databaseTables::getTable('contentEditor', "crossCategoryTable");
        $this->translateType = $this->appController->getModuleConfig('contentEditor', 'translateType')["postCategory"];
        $this->mediaNameCategory = $this->appController->getModuleConfig('contentEditor', 'mediaSettings')["postCategory"];
    }

/* ================== Application functions === */
    public function optimize($data, $imageSize = null, $parameters = []) {
        $optimizedData = [];
        $requestAllParameters = (in_array("all", $parameters));

        if(!empty($data)){
            if(isset($data["id"])){
                $optimizedData = $this->utilize($data, $imageSize, $parameters, $requestAllParameters);
            }else{
                foreach($data as $key => $value){
                    $optimizedData[$key] = (!empty($value)) ? $this->utilize($value, $imageSize, $parameters, $requestAllParameters) : $value;
                }
            }
        }

        return $optimizedData;
    }
    private function utilize($categoryInfo, $imageSize = null, $optimizeValues = [], $requestAllParameters = false) {
        ($this->templateClass === null) ? $this->templateClass = $this->appController->returnTemplatesClass() : null;
        ($this->mediaProcessor === null) ? $this->mediaProcessor = $this->appController->returnMediaProcessor() : null;

        $templatesUrls = $this->templateClass->allTemplates;

        $optimizedInfo = $categoryInfo;
        $optimizedInfo["path"] = '/' . $templatesUrls["blog"] . '/' . $categoryInfo["url"];

        if ($requestAllParameters || in_array("images", $optimizeValues)) {
            $optimizedInfo["images"] = $this->mediaProcessor->getAllObjectImages($categoryInfo["id"], $this->mediaNameCategory, $this->langShort, $imageSize, true);
        }

        $entity = new EntityCmsCategory($optimizedInfo);
        return $entity->get();
    }

/* ================== HOOK functions === */

// Select all categories
    public function getAllCategories(String $langShort, ?Bool $visibility = null, ?String $orderBy = null) {
        $visibility = ($visibility === null) ? null : (($visibility === false) ? "WHERE visibility = 0" : "WHERE visibility = 1");
        $orderBy = ($orderBy === null) ? null : "ORDER BY {$orderBy}";

        $sql = "SELECT * FROM `{$this->categoriesTable}` {$visibility} {$orderBy};";
        $returnData = $this->localizationClass->translateResult($this->database->getQuery($sql, null, true), $this->translateType, $langShort);

        return $returnData;
    }
// Select all post categories
    public function getAllCategoriesCross($postId, $langShort) {
        $sql = "SELECT * FROM `{$this->crossTable}` WHERE postId = :postId ORDER BY categoryId;";
        $returnData = $this->localizationClass->translateResult($this->database->getQuery($sql, ["postId" => $postId], true), $this->translateType, $langShort);

        return $returnData;
    }
// Select all child categories
    public function getAllChildCategories($id, $langShort) {
        $sql = "SELECT * FROM `{$this->categoriesTable}` WHERE parentId = :id ORDER BY orderNum;";
        $returnData = $this->localizationClass->translateResult($this->database->getQuery($sql, ["id" => $id], true), $this->translateType, $langShort);

        return $returnData;
    }
// Select all visible child categories
    public function getAllVisibleChildCategories($id, $langShort) {
        $sql = "SELECT * FROM `{$this->categoriesTable}` WHERE visibility = 1 AND parentId = :id ORDER BY orderNum;";
        $returnData = $this->localizationClass->translateResult($this->database->getQuery($sql, ["id" => $id], true), $this->translateType, $langShort);

        return $returnData;
    }

// Select one category
    public function getOneCategoryById(Int $id, String $langShort, ?Bool $visibility = null) {
        $visibility = ($visibility === null) ? null : (($visibility === false) ? "AND visibility = 0" : "AND visibility = 1");

        $sql = "SELECT * FROM `{$this->categoriesTable}` WHERE id = :id {$visibility};";
        $returnData = $this->localizationClass->translateResult($this->database->getQuery($sql, ["id" => $id], false), $this->translateType, $langShort);

        return $returnData;
    }
// Add all post categories
    public function addSelectedCategories($postId, $crossArray) {
        foreach ($crossArray as $key => $value) {
            $sql = "INSERT INTO `{$this->crossTable}`";
            (int)$insert = $this->database->insertQuery($sql, array("postId" => $postId, "categoryId" => $value));
            if ($insert == 0) {
                return false;
            }
        }
        return true;
    }
// Check if URL address exist
    public function checkUrlExist($url) {
        $sql = "SELECT * FROM `{$this->categoriesTable}` WHERE url = :url;";
        $returnData = $this->database->getQuery($sql, array("url" => $url), false);

        return (empty($returnData)) ? false : $returnData;
    }
// Check if URL is not exits
    public function checkUrlDuplicity($id, $urlForCheck) {
        $urlCheck = $urlForCheck;
        $urlControl = $this->checkUrlExist($urlCheck);

        if ($urlControl !== false) {
            $urlExist = ($id == $urlControl["id"]) ? false : true;
        } else {
            $urlExist = $urlControl;
        }

        if ($urlExist === true) {
            $urlCount = 1;
            do {
                $urlCheck = $urlForCheck . '-' . $urlCount;
                $urlExist = $this->checkUrlExist($urlCheck);
                $urlCount++;
            } while ($urlExist !== false);
        }

        return $urlCheck;
    }
// Select all post categories  
    public function getSelectedCategories($postId, $langShort) {
        $allPostCategories = $this->getAllCategoriesCross($postId, $langShort);
        $allCategories = [];

        foreach ($allPostCategories as $postCategory) {
            $allCategories[] = $postCategory["categoryId"];
        }

        return $allCategories;
    }
// Delete all post categories
    public function deleteSelectedCategories($postId) {
        $sql = "DELETE FROM `{$this->crossTable}`";
        $whereSql = "WHERE postId = :id";
        $returnData = $this->database->deleteQuery($sql, array("id" => $postId), $whereSql);

        return true;
    }
// Select all categories as checkboxes
    public function selectAllCategoriesAsCheckboxTree($categoryId, $postId, $repeat, $langShort) {

        $allCategories = $this->getAllChildCategories($categoryId, $langShort);

        if ($allCategories) {
            $categoriesIds = $this->getSelectedCategories($postId, $langShort);

            $i = count($allCategories);
            $count = $i;
            $checkBoxTree = null;

            foreach ($allCategories as $key => $category) {
                $categoryId = $category["id"];
                $subCategories = $this->getAllChildCategories($categoryId, $langShort);
                $selected = (in_array($categoryId, $categoriesIds)) ? "checked" : "";

                if ($repeat != 0 AND $i == $count)
                    $checkBoxTree .= '<ul class="subcategory">';
                $checkBoxTree .= '<li id="' . $categoryId . '">';
                    $checkBoxTree .= (!empty($subCategories)) ? '<i class="fa fa-plus-square-o fa-minus-square-o"></i>' : '<i class="fa fa-circle-thin"></i>';
                        $checkBoxTree .= '<div class="customInput inline checkbox">';
                            $checkBoxTree .= '<input type="checkbox" name="postCategories[]" class="checkboxStyled" id="cat' . $categoryId . '" value="' . $categoryId . '" ' . $selected . '>';
                        $checkBoxTree .= '<label for="cat' . $categoryId . '"><span>' . $category["title"] . '</span></label>';
                        $checkBoxTree .= '</div>';
                    $checkBoxTree .= (!empty($subCategories)) ? $this->selectAllCategoriesAsCheckboxTree($categoryId, $postId, $repeat + 1, $langShort) : "";
                $checkBoxTree .= '</li>';
                $i--;
                if ($repeat != 0 AND $i == 0)
                    $checkBoxTree .= '</ul>';
            }
        } else {
            $checkBoxTree = translate("mod-contentEditor-posts-detail-categories-no");
        }

        return $checkBoxTree;
    }
// Select all categories as select box
    public function selectAllCategoriesAsSelectBoxTree($categoryPosition, $activeCategoryId, $repeat, $langShort) {

        $allCategories = $this->getAllChildCategories($categoryPosition, $langShort);
        $selectBoxTree = null;

        if ($allCategories) {
            $oneCategory = $this->getOneCategoryById($activeCategoryId, $langShort);

            foreach ($allCategories as $category) {
                $selected = ($oneCategory["parentId"] == $category["id"]) ? "selected" : "";
                $selectBoxTree .= '<option value="' . $category["id"] . '" ' . $selected . '>';
                    for ($i = 0; $i < $repeat; $i++) {
                        $selectBoxTree .= '-&nbsp;';
                    }
                    $selectBoxTree .= " " . $category["title"];
                $selectBoxTree .= '</option>';
                $selectBoxTree .= $this->selectAllCategoriesAsSelectBoxTree($category["id"], $activeCategoryId, $repeat + 1, $langShort);
            }
        }
        return $selectBoxTree;
    }
// Delete post categories
    public function deletePostCategoriesCross($objectId) {
        $sql = "DELETE FROM `{$this->categoriesTable}`";
        $whereSql = "WHERE categoryId = :id";
        $returnData = $this->database->deleteQuery($sql, array("id" => $objectId), $whereSql);
        return $returnData;
    }
// Replace child categories by parentId = 0
    public function replaceChildCategories($objectId) {
        $sqlQuery = "UPDATE `{$this->categoriesTable}`";
        $whereSql = "WHERE parentId = :id";
        $done = $this->database->updateQuery($sqlQuery, array("parentId" => 0), array("id" => $objectId), $whereSql);

        return ($done !== true) ? false : true;
    }
// Return array of all groups nested by child/parent
    public function getAllCategoriesAsNestedArray($categoryId, $langShort, $level = 0, $flag = true) {
        $allCategories = $this->getAllChildCategories($categoryId, $langShort);
        $returnArray = [];

        if ($allCategories) {
            foreach ($allCategories as $key => $category) {
                $categoryTitle = ($flag === true && $level > 0) ? str_repeat('-', $level).' '.$category["title"] : $category["title"];
                $returnArray[] = ["value" => $category["id"], "title" => $categoryTitle];
                $returnArray = array_merge($returnArray, $this->getAllCategoriesAsNestedArray($category["id"], $langShort, ($level+1), $flag));
            }
        }

        return $returnArray;
    }
}