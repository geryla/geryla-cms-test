<?php
class CMS {
    public $moduleName = "contentEditor";
    
    private $database;
    private $appController;
    private $mediaController;
    private $mediaProcessor;
    private $localizationClass;

    public function __construct(Database $database, AppController $appController) {
        $this->database = $database;
        $this->appController = $appController;
        $this->mediaController = $appController->returnMediaController();
        $this->mediaProcessor = $appController->returnMediaProcessor();
        $this->localizationClass = $appController->returnLocalizationClass();
    }

// Queries - custom tables | Multi locale support
    public function getContentFromOtherTableById($objectId, $langTableName, $lang) {
        $dataTableName = (strpos($langTableName, "-lang") !== false) ? str_replace("-lang", "", $langTableName) : null;  // Create two tables if -lang prefix exist (tableName and tableName-lang)

        if ($dataTableName !== null) {
            $sql = "SELECT * FROM `{$dataTableName}` WHERE id = :id;";
            $contentData = $this->database->getQuery($sql, array("id" => $objectId), false);

            $sql = "SELECT * FROM `{$langTableName}` WHERE associatedId = :id AND langShort = :langShort;";
            $langData = $this->database->getQuery($sql, array("id" => $objectId, "langShort" => $lang), false);
            unset($langData["id"]);

            $returnData = array_merge($contentData, $langData);
        } else {
            $sql = "SELECT * FROM `{$langTableName}` WHERE id = :id;";
            $returnData = $this->database->getQuery($sql, array("id" => $objectId), false);

            $sql = "SELECT * FROM `{$this->localizationClass->translationsTable}` WHERE objectId = :id AND contentTable = :contentTable AND langShort = :langShort";
            $translateResult = $this->database->getQuery($sql, ["id" => $objectId, "contentTable" => $langTableName, "langShort" => $lang], false);

            $returnData = (!empty($translateResult)) ? $this->localizationClass->createTranslate($translateResult, $lang, $returnData) : $returnData;
        }

        return $returnData;
    }
    public function getAllContentFromOtherTable($langTableName, $lang) {
        $dataTableName = (strpos($langTableName, "-lang") !== false) ? str_replace("-lang", "", $langTableName) : null;  // Create two tables if -lang prefix exist (tableName and tableName-lang)

        if ($dataTableName !== null) {
            $returnData = [];
            $sql = "SELECT * FROM `{$dataTableName}`;";
            $contentData = $this->database->getQuery($sql, null, true);

            if ($contentData) {
                foreach ($contentData as $key => $content) {
                    $sql = "SELECT * FROM `{$langTableName}` WHERE associatedId = :id AND langShort = :langShort;";
                    $langData = $this->database->getQuery($sql, array("id" => $content["id"], "langShort" => $lang), false);
                    unset($langData["id"]);
                    $returnData[$key] = array_merge($content, $langData);
                }
            }
        } else {
            $sql = "SELECT * FROM `{$langTableName}`;";
            $returnData = $this->database->getQuery($sql, null, true);
        }

        return $returnData;
    }
}