<?php
class shortCodes {

    private $database;
    private $appController;
    private $lang;

    private $hayStack_start = "[shortcode";
    private $hayStack_end = "[/shortcode]";

    public function __construct(Database $database, AppController $appController) {
        $this->database = $database;
        $this->appController = $appController;

        $this->lang = $appController->getLangFile();
    }

    private function checkForShortCodes($haystack, $needle) {
        $offset = 0;
        $positions = [];

        while (($pos = strpos($haystack, $needle, $offset)) !== FALSE) {
            $offset   = $pos + 1;
            $positions[] = $pos;
        }
        return $positions;
    }
    private function buildShortCodePath($shortCodeInfo) {
        if (!isset($shortCodeInfo["type"]) OR !isset($shortCodeInfo["name"])) {
            return false;
        }

        $path = MODULE_DIR.$shortCodeInfo["type"]."/shortcodes/".$shortCodeInfo["name"].".php";
        $query = null;

        if (!file_exists($path)) {
            return false;
        }

        unset($shortCodeInfo["type"]);
        unset($shortCodeInfo["name"]);
        if (!empty($shortCodeInfo)) {
            $query = http_build_query($shortCodeInfo);
        }

        return ($query !== null) ? $path."?".$query : $path;
    }
    public function getShortCodes($content) {
        $shortCodes_starts = $this->checkForShortCodes($content, $this->hayStack_start);
        $allShortCodes = [];

        if(!empty($shortCodes_starts)){
            $endLenght = strlen($this->hayStack_end);

            foreach($shortCodes_starts as $start){
                $closing = strpos($content, $this->hayStack_end, $start);
                $length = ($closing - $start + $endLenght);
                $shortCodeInfo = ["shortCode" => substr($content, $start, $length), "start" => $start, "end" => ($start + $length), "length" => $length];

                $shortCodeParameters = str_replace($this->hayStack_start." ", "", $shortCodeInfo["shortCode"]);
                $shortCodeParameters = str_replace("]".$this->hayStack_end, "", $shortCodeParameters);
                $shortCodeParameters = explode(" ", $shortCodeParameters);

                $lastParam = null;
                foreach($shortCodeParameters as $parameter){
                    $trimmedParameter = str_replace('"', '', $parameter);
                    $trimmedParameter = explode("=", $trimmedParameter);

                    if(isset($trimmedParameter[1]) && $trimmedParameter[1] !== null){
                        $shortCodeInfo["parameters"][$trimmedParameter[0]] = $trimmedParameter[1];
                        $lastParam = $trimmedParameter[0];
                    }else{
                        $shortCodeInfo["parameters"][$lastParam] .= " ".$trimmedParameter[0];
                    }

                }

                if(isset($shortCodeInfo["parameters"]) && !empty($shortCodeInfo["parameters"])){
                    $shortCodeInfo["scriptPath"] = $this->buildShortCodePath($shortCodeInfo["parameters"]);
                    $allShortCodes[] = $shortCodeInfo;
                }
            }
        }

        return ["content" => $content, "shortCodes" => $allShortCodes];
    }
    public function buildContent($buildData){
        if(empty($buildData["shortCodes"])){
            $newContent = $buildData["content"];
        }else{
            $database = $this->database;
            $appController = $this->appController;
            $lang = $this->lang;
            $systemSettings = $this->appController->getSettings();

            $newContent = $buildData["content"];
            foreach($buildData["shortCodes"] as $shortCode){
                if(!empty($shortCode["scriptPath"]) ){
                    $parsedLink = parse_url($shortCode["scriptPath"]);

                    parse_str($parsedLink["query"], $shortcodeParams); // Input: $shortcodeParams
                    $shortCodeReturn = include($parsedLink["path"]);
                    $newContent = str_replace($shortCode["shortCode"], $shortCodeReturn, $newContent);
                }
            }
        }

        ob_start();
        echo $newContent;
        echo ob_get_clean();
    }

// LOAD short codes for backend (wysiwyg editor)
    public function loadShortCodesList($enabledShortCodeModules){
        if( !isset($_SESSION["cache"]["admin"]["shortcodes"]) ){
            $shortcodeList = [];

            foreach($enabledShortCodeModules as $moduleName){
                $handle = opendir(MODULE_DIR.$moduleName.'/shortcodes');
                while($file = readdir($handle)){
                    if( !is_dir($file) && ($file != '.' && $file != '..') ){
                        if( !is_dir($file) ){
                            list($fileName, $fileExtension) = explode(".",$file);
                            $shortcodeList[$moduleName][] = $fileName;
                        }
                    }
                }
            }

            if( !empty($shortcodeList) ){
                $_SESSION["cache"]["admin"]["shortcodes"] = $shortcodeList;
            }
        }
        if( isset($_SESSION["cache"]["admin"]["shortcodes"]) ){
            $shortcodePlugin = null;

            $shortcodePlugin .= 'setup: function(editor) {';
            $shortcodePlugin .= 'var shortcodesList = [';
            foreach($_SESSION["cache"]["admin"]["shortcodes"] as $shortcodeName => $shortcodes){
                foreach($shortcodes as $shortcode){

                    $shortcodePlugin .= '{text: "'.translate("shortcodeName-".$shortcodeName."-".$shortcode).'", onclick: function() {editor.insertContent("[shortcode type=\"'.$shortcodeName.'\" name=\"'.$shortcode.'\"][/shortcode]");}},';
                }
            }
            $shortcodePlugin .= '];';
            $shortcodePlugin .= 'editor.addButton("mypluginname", {
                type: "menubutton",
                text: "Shortcode",
                icon: "template",
                menu: shortcodesList
              });   
            },';

            echo $shortcodePlugin;
        }
    }
}