<?php
/* =============================== CONTROLLER METHODS ======== */
$moduleCMS = new CMS($database, $appController);
$cmsCategories = new cmsCategories($database, $appController);

$langShort = $presenter->templateData->langShort;

$categoryTable = $cmsCategories->categoriesTable;

$nextPage = $dataCompiler->modulePageUrl(8);
$nextPageAdd = $nextPage.'?action=add';
$prevPage = null;

$parentId = ( isset($_GET["parent"]) ) ? $_GET["parent"] : 0;
$pageTitle = translate("mod-contentEditor-categories-list-title");

$pageData = $cmsCategories->getAllChildCategories($parentId, $langShort);
$parentInfo = $cmsCategories->getOneCategoryById($parentId, $langShort);

if( $parentId != 0 && !empty($parentInfo) ){
  $prevPage = $dataCompiler->modulePageUrl(7)."?parent=".$parentInfo["parentId"];
  $nextPageAdd = $nextPageAdd.'&parent='.$parentId;
  $pageTitle = "<b>".$parentInfo["title"]."</b>: ".$pageTitle;
}

/* =============================== CONTENT ======== */ 
echo '<article class="module">';
    $render->header($pageTitle, $prevPage, ($presenter->activeContent->enableTranslate === true) ? null : $nextPageAdd); // ADD ony for default lang
    echo '<section>';
        echo '<div class="moduleWrap">';
            echo '<table class="striped">';
                $tableHead = [translate("mod-contentEditor-categories-categoryName"), translate("mod-contentEditor-categories-categoryParent"), translate("mod-contentEditor-categories-categoryChild"), translate("mod-contentEditor-categories-visibility"), '', ''];
                $render->tableHeadSortable($tableHead);
                echo '<tbody class="sortableWrap" data-name="postsCategory">';
                    foreach($pageData as $category){
                        $parent = $cmsCategories->getOneCategoryById($category["parentId"], $langShort);
                        $parentCategory = ( !empty($parent) ) ? $parent["title"] : translate("mod-contentEditor-categories-categoryParent-none");
                        $subCategory = $cmsCategories->getAllChildCategories($category["id"], $langShort);

                        $langTitle = ($presenter->activeContent->enabledMultilanguage > 1) ? "<i class='activeLang'>(".$category["activeLang"].")</i> ".$category["title"] : $category["title"];
                        $langTranslate = ($presenter->activeContent->enabledMultilanguage > 1 && $category["activeTranslation"] === true) ? true : false;

                        echo '<tr id="'.$category["id"].'">';
                            $render->tableColumnSortable();
                            $render->tableColumn('<strong>'.$langTitle.'</strong>');
                            $render->tableColumn($parentCategory);
                            $render->tableColumn(count($subCategory));
                            $render->tableColumn($render->visibilitySwitch($category["id"], $cmsCategories->translateType, $category["id"], $category["visibility"]), 80, true);
                            $render->tableColumn('<a href="?parent='.$category["id"].'" class="anchor blue"><i class="fa fa-search" title="'.translate("mod-contentEditor-categories-showChild").'"></i> '.translate("mod-contentEditor-categories-showChild").'</a>', null, null, "actions");

                            $editSwitch = $nextPage.'?action=edit&id='.$category["id"];
                            $editSwitch = ($parentId == 0) ? $editSwitch : $editSwitch."&parent=".$parentId;
                            $deleteSwitch = 'class="deleteContent" name="'.$cmsCategories->translateType.'" data="'.$category["id"].'"';
                            $previewLink = $render->getPreviewPath("blog", $category["url"]);
                            $render->tableActionColumn($editSwitch, $deleteSwitch, $previewLink, 100, $langTranslate);
                        echo '</tr>';
                    }
                echo '</tbody>';
            echo '</table>';
        echo '</div>';
    echo '</section>';
echo '</article>';
