<?php
/* =============================== CONTROLLER METHODS ======== */
$moduleCMS = new CMS($database, $appController);
$cmsPostBlog = new cmsPostBlog($database, $appController);
$cmsCategory = new cmsCategories($database, $appController);

$nextPage = $dataCompiler->modulePageUrl(6);
$langShort = $presenter->templateData->langShort;

$postTable = $cmsPostBlog->postsTable;
$templatesPostsTable = $cmsPostBlog->postsTranslateTable;

$filterToModule->setSortOptions([
    [
        'title' => translate("mod-contentEditor-admin-filtration-sort-category"),
        'type' => 'select',
        'name' => 'category',
        'values' => $cmsCategory->getAllCategoriesAsNestedArray(0, $langShort)
    ]
]);
$filterToModule->renderFiltration($_SERVER['REQUEST_URI'], $_GET);
$filterToModule->activeFilters["parameters"]["type"] = 2; // Blog posts

$pageData = $filterToModule->getAll_Posts($langShort, $filterToModule->activeFilters, null, $filterToModule->limit, $filterToModule->offset, $filterToModule->activeFilters["orderBy"], $filterToModule->activeFilters["orderDirection"]);
$pageData = $cmsPostBlog->optimize($pageData, $langShort);

/* =============================== CONTENT ======== */ 
echo '<article class="module">';
    $render->header(translate("mod-contentEditor-posts-list"), null, $nextPage.'?action=add');
    echo '<section>';
        echo '<div class="moduleWrap">';
            echo '<table class="striped">';
                $tableHead = [translate("mod-contentEditor-posts-list-title"), translate("mod-contentEditor-posts-list-visibility"), ''];
                $render->tableHeadSortable($tableHead);

                echo '<tbody class="sortableWrap" data-name="postsPost">';
                    foreach($pageData as $post){
                        echo '<tr id="'.$post["id"].'">';
                            $langTitle = ($presenter->activeContent->enabledMultilanguage > 1) ? "<i class='activeLang'>(".$post["activeLang"].")</i> ".$post["title"] : $post["title"];
                            $langTranslate = ($presenter->activeContent->enabledMultilanguage > 1 && $post["activeTranslation"] === true) ? true : false;

                            $render->tableColumnSortable();
                            $render->tableColumn($langTitle);
                            $render->tableColumn($render->visibilitySwitch($post["id"], "posts", $post["id"], $post["visibility"]), 80, true);

                            $editSwitch = $nextPage.'?action=edit&id='.$post["id"];
                            $deleteSwitch = 'class="deleteContent" name="posts" data="'.$post["id"].'"';
                            $previewPath = $render->getPreviewPath("blog", $post["url"], $post["urlPrefix"]);

                            $render->tableActionColumn($editSwitch, $deleteSwitch, $previewPath, 100, $langTranslate);
                        echo '</tr>';
                    }
                echo '</tbody>';
            echo '</table>';
        echo '</div>';
    echo '</section>';
echo '</article>';
