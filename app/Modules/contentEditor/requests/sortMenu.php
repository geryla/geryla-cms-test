<?php
    $requestFinished = false;
    if(isset($request["data"]["sorted"]) && isset($request["data"]["sortedList"]) ){
        $moduleCMS = new CMS($database, $appController);
        $cmsHook = new cmsHook($database, $appController);

        $orderNums = $request["data"]["sortedList"];
        $orderedList = array();

        $i=0;
        foreach($orderNums as $num){
            $orderedList[$request["data"]["sortedList"][$i]] = $i+1;
            $i++;
        }

        foreach($request["data"]["sorted"] as $value) {
            if( isset($value["children"]) ){
                $cmsHook->hookTree($value["id"], $value["children"], 1, $orderedList);
            }
            $cmsHook->updateHookMenu($value["id"], 0, $orderedList[$value["id"]]);
        }
        $requestFinished = true;
        $logId = $value["id"]; // Log hook id
    }

// LOG
    if($requestFinished === true ){
        $systemLogger = new systemLogger($database);
        $systemLogger->createLog($_SESSION["languages"]["adminLang"], "contentEditor", "sortingHook", $logId, $_SESSION["security"]["adminLogin"]["logIn"]);
    }