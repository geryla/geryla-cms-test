<?php
    $requestFinished = false;
    if(isExisting('items', $request["data"])){

        $moduleCMS = new CMS($database, $appController);
        $cmsHook = new cmsHook($database, $appController);

        $hookID = $request["data"]["items"]["hookId"];
        unset($request["data"]["items"]["title"]);
        unset($request["data"]["items"]["hookId"]);

        foreach($request["data"]["items"] as $table => $objectId){
            if($objectId != 0){
                if(is_array($objectId)){
                    if(!empty($objectId["title"]) && !empty($objectId["url"])){
                        $newAnchor = $cmsHook->addHookAnchor(["title" => $objectId["title"], "url" => $objectId["url"], "langShort" => $localeEngine->returnAppLang()]);
                        if($newAnchor != 0){
                            $objectId = $newAnchor;
                        }else{
                            continue;
                        }
                    }else{
                        continue;
                    }
                }

                $tableName = $database->injectionProtect($table);
                $dataValues = ["objectId" => $objectId, "dataTable" => $tableName, "hookId" => $hookID];
                $cmsHook->addContentToHook($dataValues);
            }
        }

        echo '<ol class="dd-list">';
            echo $cmsHook->getHookContentSortable($hookID, 0, 0);
        echo '</ol>';

        $requestFinished = true;
    }

// LOG
    if($requestFinished === true){
        $systemLogger = new systemLogger($database);
        $systemLogger->createLog($_SESSION["languages"]["adminLang"], "contentEditor", "addContentToHook", $hookID, $_SESSION["security"]["adminLogin"]["logIn"]);
    }