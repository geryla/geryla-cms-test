<?php
    $config = [
        "moduleName" => "Settings",
        "moduleUrlName" => "settings",
        "moduleLangName" => "settings",
        "moduleIcon" => "fa-cogs",
        "moduleVersion" => "4.5.0",

        "mediaSettings" => [
            "logo" => "logo",
            "favicon" => "favicon",
        ],
        "translateType" => [],
        "databaseTables" => [
            "settingTable" => [
                "name" => "settings",
            ],
            "logTable" => [
                "name" => "logs",
            ],
            "moduleTables" => [
                "name" => "modules",
            ],
            "accessLog" => [
                "name" => "access",
            ],
            "migrations" => [
                "name" => "migrations",
            ],
            "scheduler" => [
                "name" => "schedules",
            ],
            "jobs" => [
                "name" => "scheduledJobs",
            ],
            "apiSessions" => [
                "name" => "apiSessions",
            ],
        ],

        "modulePages" => [
            [
                "pageId" => 1,
                "urlName" => "settings-edit",
                "url" => "settings-edit",
                "parent" => 0,
                "inMenu" => 1,
                "headPage" => 1
            ],
            [
                "pageId" => 2,
                "urlName" => "settings-modules",
                "url" => "settings-modules",
                "parent" => 0,
                "inMenu" => 1,
                "headPage" => 0,
                'order' => 1,
                "onlyAdmin" => true
            ],
            [
                "pageId" => 3,
                "urlName" => "settings-config",
                "url" => "settings-config",
                "parent" => 0,
                "inMenu" => 1,
                "headPage" => 0
            ],
            [
                "pageId" => 4,
                "urlName" => "jobs-scheduler",
                "url" => "jobs-scheduler",
                "parent" => 0,
                "inMenu" => 1,
                "headPage" => 0
            ]
        ]
    ];