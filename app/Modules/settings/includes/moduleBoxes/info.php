<?php
for($i=0; $i<=24; $i++){
    if($i > 0){
        $dailyFrequency[] = ["value" => $i];
    }

    if($i < 24){
        $dayHours[] = ["value" => $i];
    }
}

$render->contentDiv(true, null, "editPart");
    $render->contentDiv(true, null, "inlineDiv full");
        echo '<h3>&nbsp;</h3>';
        $render->contentDiv(true, null, "rows");

            if(!$jobInfo["moduleName"]){
                $render->contentRow(null, "rowBox", '<h3>'.translate("mod-settings-scheduler-jobInfo-job").'</h3>', null, $render->input("text", "cronJob\"", $jobInfo["cronJob"], null, true, null, null) );
            }

            $render->contentRow(null, "rowBox inline three first", '<h3>'.translate("mod-settings-scheduler-jobInfo-dailyFrequency").'</h3>', null, $render->select("dailyFrequency", $dailyFrequency, "value", "value", $jobInfo["dailyFrequency"], true) );
            $render->contentRow(null, "rowBox inline three", '<h3>'.translate("mod-settings-scheduler-jobInfo-start").'</h3>', null, $render->select("start", $dayHours, "value", "value", $jobInfo["start"], true) );
            $render->contentRow(null, "rowBox inline three last", '<h3>'.translate("mod-settings-scheduler-jobInfo-end").'</h3>', null, $render->select("end", $dayHours, "value", "value", $jobInfo["end"], true) );

            $render->contentDiv(true, null, "rowBox");
                $render->contentDiv(true, null, null);
                    echo '<h3>'.translate("mod-settings-scheduler-jobInfo-days").'</h3>';
                        echo '<ul>';
                            echo '<li style="display: inline-block;margin-right: 15px;">';
                                echo '<div class="customInput inline checkbox">';
                                    echo '<input type="checkbox" name="monday" id="monday" class="checkboxStyled notTranslatable" '.(($jobInfo["monday"] == 1) ? "checked" : "").'>';
                                    echo '<label for="monday"><span>'.translate("mod-settings-scheduler-jobInfo-monday").'</span></label>';
                                echo '</div>';
                            echo '</li>';
                            echo '<li style="display: inline-block;margin-right: 15px;">';
                                echo '<div class="customInput inline checkbox">';
                                    echo '<input type="checkbox" name="tuesday" id="tuesday" class="checkboxStyled notTranslatable" '.(($jobInfo["tuesday"] == 1) ? "checked" : "").'>';
                                    echo '<label for="tuesday"><span>'.translate("mod-settings-scheduler-jobInfo-tuesday").'</span></label>';
                                echo '</div>';
                            echo '</li>';
                            echo '<li style="display: inline-block;margin-right: 15px;">';
                                echo '<div class="customInput inline checkbox">';
                                    echo '<input type="checkbox" name="wednesday" id="wednesday" class="checkboxStyled notTranslatable" '.(($jobInfo["wednesday"] == 1) ? "checked" : "").'>';
                                    echo '<label for="wednesday"><span>'.translate("mod-settings-scheduler-jobInfo-wednesday").'</span></label>';
                                echo '</div>';
                            echo '</li>';
                            echo '<li style="display: inline-block;margin-right: 15px;">';
                                echo '<div class="customInput inline checkbox">';
                                    echo '<input type="checkbox" name="thursday" id="thursday" class="checkboxStyled notTranslatable" '.(($jobInfo["thursday"] == 1) ? "checked" : "").'>';
                                    echo '<label for="thursday"><span>'.translate("mod-settings-scheduler-jobInfo-thursday").'</span></label>';
                                echo '</div>';
                            echo '</li>';
                            echo '<li style="display: inline-block;margin-right: 15px;">';
                                echo '<div class="customInput inline checkbox">';
                                    echo '<input type="checkbox" name="friday" id="friday" class="checkboxStyled notTranslatable" '.(($jobInfo["friday"] == 1) ? "checked" : "").'>';
                                    echo '<label for="friday"><span>'.translate("mod-settings-scheduler-jobInfo-friday").'</span></label>';
                                echo '</div>';
                            echo '</li>';
                            echo '<li style="display: inline-block;margin-right: 15px;">';
                                echo '<div class="customInput inline checkbox">';
                                    echo '<input type="checkbox" name="saturday" id="saturday" class="checkboxStyled notTranslatable" '.(($jobInfo["saturday"] == 1) ? "checked" : "").'>';
                                    echo '<label for="saturday"><span>'.translate("mod-settings-scheduler-jobInfo-saturday").'</span></label>';
                                echo '</div>';
                            echo '</li>';
                            echo '<li style="display: inline-block">';
                                echo '<div class="customInput inline checkbox">';
                                    echo '<input type="checkbox" name="sunday" id="sunday" class="checkboxStyled notTranslatable" '.(($jobInfo["sunday"] == 1) ? "checked" : "").'>';
                                    echo '<label for="sunday"><span>'.translate("mod-settings-scheduler-jobInfo-sunday").'</span></label>';
                                echo '</div>';
                            echo '</li>';
                        echo '</ul>';
                    echo '<br /><br />';
                $render->contentDiv(false);
            $render->contentDiv(false);

        $render->contentDiv(false);
    $render->contentDiv(false);
$render->contentDiv(false);