<?php
/* =============================== FUNCTIONS ======== */
$schedulerMaster = new ScheduleController($database, $appController);
$listPage = $dataCompiler->modulePageUrl(4);

// ACCESS CONTROL
$pageId = ( isset($_GET["id"]) ) ? $_GET["id"] : 0;
$pageAction = ( isset($_GET["action"]) ) ? $_GET["action"] : null;
if( $pageAction !== null && (($pageAction == "edit" && $pageId == 0) OR ($pageAction == "add" && $pageId != 0))){
    $dataCompiler->setContinue(true);
    $dataCompiler->redirectPage(0, $listPage);
}

$allJobs = $schedulerMaster->getAllTasks();
$pageTitle = translate("mod-settings-scheduler-list");
$prevButton = null;
//$nextButton = $listPage."?action=add";
$disableList = null;

// CODE DETAIL load
if($pageAction !== null){
    $jobInfo = $schedulerMaster->getOneTask($pageId);

    $pageTitle = ($pageAction == "edit") ? translate("mod-settings-scheduler-edit").': <b>'.$jobInfo["cronJob"].'</b>' : translate("mod-settings-scheduler-add");
    $prevButton = $listPage;
    $nextButton = null;
    $disableList = "disabled";
}

if( isset($_POST['submit']) ){

    $postData = [];
    $daysInfo = ["monday" => 0, "tuesday" => 0, "wednesday" => 0, "thursday" => 0, "friday" => 0, "saturday" => 0, "sunday" => 0];
    foreach ($_POST as $key => $value) {
        if($key == 'submit'){
            continue;
        }elseif(isset($daysInfo[$key])){
            $daysInfo[$key] = 1;
        }else{
            $postData[$key] = $value;
        }
    }
    $postData = array_merge($postData, $daysInfo);
    $postData["updatedDate"] = date("Y-m-d H:i:s");

// Module work (add/edit/translate)
    $dataCompiler->setTables($schedulerMaster->schedulesTable, null);
    $dataCompiler->setData($postData, null, $schedulerMaster->dataRows);
    $dataCompiler->setTranslate(false);

    $actionInfo = $dataCompiler->updateQuery($pageId);
     
  if($actionInfo === true){
    $returnMessage = translate("admin-edit-true");
    $systemLogger->createLog($_SESSION["languages"]["adminLang"], $presenter->activeContent->module, "updateCronJob", $pageId, $_SESSION["security"]["adminLogin"]["logIn"]);
  }else{
    $returnMessage = translate("admin-create-false");
  }

  $dataCompiler->setMessage($returnMessage);
  $dataCompiler->setContinue(true);
}

$dataCompiler->showMessage();
$dataCompiler->refreshPage($dataCompiler->nextStep, "");

/* =============================== CONTENT ======== */
echo '<article class="module">';
    $render->header($pageTitle, $prevButton, $nextButton);
    echo '<section class="discountCodes">';
        echo '<div class="moduleWrap">';

            if($pageAction !== null){
                echo '<form action="" method="POST" enctype="multipart/form-data">';
                    $render->contentDiv(true, null, "contentBox");
                        echo $render->button("submit", "true", translate("save"), false, null, "btn icon save success");
                        include($render->getBoxPath("settings", "info"));
                        echo $render->button("submit", "true", translate("save"), false, null, "btn icon save success");
                    $render->contentDiv(false);
                echo '</form>';
            }

            echo '<table class="striped '.$disableList.'">';
                $render->tableHeadSortable([
                    translate("mod-settings-scheduler-method"),
                    translate("mod-settings-scheduler-lastRun"),
                    translate("mod-settings-scheduler-visibility"),
                    '']);

                echo '<tbody class="sortableWrap" data-name="schedule">';
                    foreach($allJobs as $job){
                        $moduleCron = (!empty($job["moduleName"])) ? true : false;
                        $jobName = ($moduleCron) ? $job["cronJob"].' <i>('.translate($job["moduleName"]).')</i>' : $job["cronJob"];

                        echo '<tr id="'.$job["id"].'">';
                            $render->tableColumnSortable();
                            $render->tableColumn($jobName);
                            $render->tableColumn($job["lastRun"]);
                            $render->tableColumn($render->visibilitySwitch($job["id"], "schedule", $job["id"], $job["visibility"]), 80, true);

                            $editSwitch = $nextPage.'?action=edit&id='.$job["id"];
                            $deleteSwitch = ($moduleCron) ? null : 'class="deleteContent" name="schedule" data="'.$job["id"].'"';
                            $render->tableActionColumn($editSwitch, $deleteSwitch, null, 100);
                        echo '</tr>';
                    }
                echo '</tbody>';
            echo '</table>';

        echo '</div>';
    echo '</section>';
echo '</article>';