<?php
// CONTROLLER
    $pageData = $settingsController->getGroupedSettings("configuration", true);

    if(isset($_POST['submit'])){
        $submitData = [];
        foreach ($_POST as $key => $value) {
            if($key !== "submit"){
                $submitData[$key] = $value;
            }
        }

        $returnMessage = translate("admin-create-false");
        $editSettings = $settingsController->editSettings($submitData, null);

        if($editSettings === true){
            $returnMessage = translate("admin-edit-true");
            $systemLogger->createLog($_SESSION["languages"]["adminLang"], $presenter->activeContent->module, "updateConfiguration", 0, $_SESSION["security"]["adminLogin"]["logIn"]);
        }

        $dataCompiler->setMessage($returnMessage);
        $dataCompiler->setContinue(true);
    }

    if(isset($_GET["delete"])){
        if($_GET["delete"] === "cache" && isset($_GET["cacheType"])){
            $cache = new Cache();
            $cache->removeCache($_GET["cacheType"]);

            $dataCompiler->setMessage(translate("mod-settings-configuration-cache-deletedAction"));
            $dataCompiler->setContinue(true);
        }
        if($_GET["delete"] === "thumbnail" && isset($_GET["thumbnailType"])){
            $mediaController->deleteThumbnails($_GET["thumbnailType"]);

            $dataCompiler->setMessage(translate("mod-settings-media-thumbnail-deletedAction"));
            $dataCompiler->setContinue(true);
        }
    }

    $dataCompiler->showMessage();
    $dataCompiler->refreshPage($dataCompiler->nextStep, $presenter->activeContent->page);

// VIEW
    echo '<article class="module">';
        $render->header(translate("mod-settings-configuration"), null);
        echo '<section>';
            echo '<form action="" method="POST" enctype="multipart/form-data">';
                echo $render->button("submit", "true", translate("save"), false, null, "btn icon save success");
                echo '<div class="moduleWrap">';
                    echo '<table class="striped">';
                        $selectValues = [ ["value" => "true", "title" => translate("mod-settings-yes")], ["value" => "false", "title" => translate("mod-settings-no")] ];
                        $render->tableRow(translate("mod-settings-configuration-title"), "", true);
                        $render->tableRow(translate("mod-settings-configuration-enableCache"), $render->select("enableCache", $selectValues, "value", "title", $pageData["enableCache"], false));

                        if($appController->isActiveModule("users")){
                            $selectValues = [ ["value" => "true", "title" => translate("mod-users-admin-settings-enableAttempts-true")], ["value" => "false", "title" => translate("mod-users-admin-settings-enableAttempts-false")] ];

                            $render->tableRow(translate("mod-settings-configuration-bruteForceLimit"), translate("mod-settings-configuration-bruteForceLimit-help"), true);
                            $render->tableRow(translate("mod-settings-configuration-bruteForceLimit-title"), $render->input("text", "bruteForceLimit", $pageData["bruteForceLimit"], null, false, null, null));

                            $render->tableRow(translate("mod-users-admin-settings-loginAttempts"), translate("mod-users-admin-settings-loginAttempts-help"), true);
                            $render->tableRow(translate("mod-users-admin-settings-enableAttempts"), $render->select("enableLoginAttempts", $selectValues, "value", "title", $pageData["enableLoginAttempts"], false));
                        }
                    echo '</table>';
                echo '</div>';
                echo $render->button("submit", "true", translate("save"), false, null, "btn icon save success");
            echo '</form>';
            echo '<form action="'.$presenter->activeContent->page.'" method="GET" enctype="multipart/form-data">';
                echo '<div class="moduleWrap">';
                    echo '<table class="striped">';
                        $cacheValue = [
                            ["value" => "Api", "title" => translate("mod-settings-configuration-cache-deleting-Api")],
                            ["value" => "Entity", "title" => translate("mod-settings-configuration-cache-deleting-Entity")],
                            ["value" => "Media", "title" => translate("mod-settings-configuration-cache-deleting-Media")],
                            ["value" => "Query", "title" => translate("mod-settings-configuration-cache-deleting-Query")],
                            ["value" => "System", "title" => translate("mod-settings-configuration-cache-deleting-System")],
                        ];
                        $render->tableRow(translate("mod-settings-configuration-cache-title"), "", true);
                        $render->tableRow(
                            $render->select("cacheType", $cacheValue, "value", "title", null, false, null, null, null),
                            $render->button("delete", "cache", translate("mod-settings-configuration-cache-delete-button"), false, null, 'btn error')
                        );
                    echo '</table>';
                echo '</div>';
            echo '</form>';
            echo '<form action="'.$presenter->activeContent->page.'" method="GET" enctype="multipart/form-data">';
                echo '<div class="moduleWrap">';
                    echo '<table class="striped">';
                        $existingThumbnails = array_keys($mediaController->getAllThumbnailsInfo());
                        $thumbnailValues = array_combine($existingThumbnails, $existingThumbnails);
                        $render->tableRow(translate("mod-settings-media-thumbnail-title"), "", true);
                        $render->tableRow(
                            $render->select("thumbnailType", $thumbnailValues, null, null, null, false, null, null, null),
                            $render->button("delete", "thumbnail", translate("mod-settings-media-thumbnail-delete-button"), false, null, 'btn error')
                        );
                    echo '</table>';
                echo '</div>';
            echo '</form>';
        echo '</section>';
    echo '</article>';