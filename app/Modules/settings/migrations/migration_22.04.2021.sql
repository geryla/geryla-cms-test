ALTER TABLE `logs` ADD `logInfo` TEXT NULL AFTER `logAction`;

-- Move status from name of action to info column
UPDATE `logs` SET `logAction` = 'updateStatus', `logInfo` = '{"status":"true"}' WHERE `logAction`= 'updateStatusTrue';
UPDATE `logs` SET `logAction` = 'updateStatus', `logInfo` = '{"status":"false"}' WHERE `logAction`= 'updateStatusFalse';