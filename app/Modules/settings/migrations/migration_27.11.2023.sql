CREATE TABLE IF NOT EXISTS `scheduledJobs` (
  `id` INT(10) NOT NULL AUTO_INCREMENT,

  `name` VARCHAR(120) NOT NULL,
  `moduleName` VARCHAR(120) NULL,
  `parameters` LONGTEXT NULL,
  `dependency` INT(10) NULL DEFAULT '0',
  `status` VARCHAR(30) NOT NULL,
  `execution` FLOAT(6) NULL,
  `message` TEXT NULL,

  `updatedDate` datetime NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;