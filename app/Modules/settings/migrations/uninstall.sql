SET FOREIGN_KEY_CHECKS=0;

-- DELETE TABLES
DROP TABLE `settings`;
DROP TABLE `logs`;
DROP TABLE `access`;
DROP TABLE `scheduledJobs`;

-- DELETE ALL MEDIA by mediaSettings from module CONFIG
DELETE media, lang FROM `mediamanager` AS media INNER JOIN `mediamanager-lang` AS lang WHERE media.template = 'logo';
DELETE media, lang FROM `mediamanager` AS media INNER JOIN `mediamanager-lang` AS lang WHERE media.template = 'favicon';

SET FOREIGN_KEY_CHECKS=1;