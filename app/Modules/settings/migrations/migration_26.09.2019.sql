-- CREATE TABLE for SETTINGS
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) NOT NULL AUTO_INCREMENT,

  `settName` varchar(150) NOT NULL,
  `settValue` varchar(255) DEFAULT NULL,
  `moduleName` varchar(25) NULL,
  `settGroup` varchar(40) NULL,

  `updatedDate` datetime NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- CREATE TABLE for LOGS
CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(10) NOT NULL AUTO_INCREMENT,

  `langShort` varchar(5) NOT NULL,
  `logType` varchar(30) NOT NULL,
  `logAction` varchar(35) NOT NULL,
  `objectId` varchar(15) NOT NULL,
  `userId` int(10) NULL,
  `logTime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- CREATE TABLE for ACCESS (login)
CREATE TABLE IF NOT EXISTS `access` (
  `id` int(10) NOT NULL AUTO_INCREMENT,

  `ipAddress` varchar(255) NOT NULL,
  `isAdminAccess` int(1) NOT NULL DEFAULT '0',
  `newEntry` int(1) NOT NULL DEFAULT '1',
  `blockTime` varchar(80) NOT NULL,

  `createdDate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- TRUNCATE DATA AND INSERT DEFAULT VALUES - Settings
DELETE FROM `settings`;
ALTER TABLE `settings` DISABLE KEYS;
INSERT INTO `settings` (`settName`, `settValue`, `moduleName`, `settGroup`) VALUES
  ('buildNum', '1', 'settings', ''),
  ('emailRecipients', '', 'settings', ''),
  ('enableCache', 'false', 'settings', 'configuration'),
  ('bruteForceLimit', '3', 'settings', 'configuration');

-- INSERT MODULE SETTINGS into TABLE 'settings'
INSERT INTO `settings` (`settName`, `settValue`, `moduleName`, `settGroup`) VALUES ('showMismatchContent', 'false', 'localization', '');

ALTER TABLE `settings` ENABLE KEYS;