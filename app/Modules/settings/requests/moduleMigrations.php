<?php
    if( isset($request["data"]["action"]) && isset($request["data"]["name"]) ) {
        $moduleName = $request["data"]["name"];
        if($moduleName === 'checkForSystemUpdate'){
            $moduleName = '';
        }

        $callBack = $modulesController->triggerModuleAction($request["data"]["action"], $moduleName);
    }

    return $callBack;