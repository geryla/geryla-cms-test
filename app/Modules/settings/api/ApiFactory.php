<?php
namespace Api\settings;

class ApiFactory extends \ApiController{
    private $requestMethods;

    private $errorCodes = [
        400 => 'Object/s not found',
        401 => 'Missing filtration parameter LabelTag or CategoryId',
    ];

    public function __construct(\Database $database, \AppController $appController, Array $requestData) {
        parent::__construct($database, $appController, $requestData);

        $this->requestMethods = include(__DIR__.'/requests.php');
    }

    public function getErrorCodes(){
        return $this->returnErrorCodes($this->errorCodes);
    }
    public function validate(){
        return $this->validateRequest($this->requestData, $this->requestMethods);
    }
    public function proceedRequest() {

        switch($this->requestData["action"]){
            case "getModuleSettings":
                $this->getModuleSettings();
                break;
            default:
                $this->setState(310);
                return false;
        }

        $this->closeAuthRequest();
        return true;
    }

    private function getModuleSettings(){
        $settingsController = $this->appController->returnSettingsController();
        $moduleSettings = $settingsController->getModuleSettings($this->requestData["value"], true, true, true);

        $this->setResponseData($moduleSettings);
        return true;
    }
}