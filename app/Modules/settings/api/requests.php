<?php

return [
    "getModuleSettings" => [
        "path" => "/settings/getModuleSettings/{value}",
        "method" => "GET",
        "authorized" => true,
        "cache" => false,
        "value" => [
            "required" => true,
            "type" => 'String',
            "description" => "Returns specific module settings"
        ],
    ],
];