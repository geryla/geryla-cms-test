<?php
/* =============================== FUNCTIONS ======== */
$pageData = $settingsController->getModuleSettings("settings", true);

$mediaInfo["name"] = $settingsController->logoMediaName;
$pageId = 1;

if( isset($_POST['submit']) ){
  
  $submitData = [];
  foreach ($_POST as $key => $value) {
    if($key == 'submit' ){
      continue;
    }elseif($key == 'images'){
      $postData[$key] = $value;
    }else{
      $submitData[$key] = $value;
    }        
  }

  $editSettings = $settingsController->editSettings($submitData, "settings");

  if($editSettings === true){           
    $returnMessage = translate("admin-edit-true");
    $systemLogger->createLog($_SESSION["languages"]["adminLang"], $presenter->activeContent->module, "updateSettings", 0, $_SESSION["security"]["adminLogin"]["logIn"]);   
    
    if( isset($postData) ){
      $dataCompiler->setContinue(true);
      $dataCompiler->setData($postData, [], []);
      $dataCompiler->updateImages($mediaController->translateTable); // Edit images info
    }
  }else{
    $returnMessage = translate("admin-create-false");
  }

  $dataCompiler->setMessage($returnMessage);
  $dataCompiler->setContinue(true);
}

$dataCompiler->showMessage();
$dataCompiler->refreshPage($dataCompiler->nextStep, "");

/* =============================== CONTENT ======== */ 
echo '<article class="module">';
  
  $render->header(translate("mod-settings-edit"), null);
  
  echo '<section>';                                      
    echo '<form action="" method="POST" enctype="multipart/form-data">';

      echo $render->button("submit", "true", translate("save"), false, null, "btn icon save success");
      
      echo '<div class="moduleWrap">';             
       
        echo '<table class="striped">';                     
          $render->tableRow(translate("mod-settings-logo"), "", true);
        echo '</table>';
        include($render->getBoxPath("mediaManager", "images"));
        
        echo '<br /><br />';
        echo '<table class="striped">';                   
          $render->tableRow(translate("mod-settings-basic"), "", true);
          $render->tableRow(translate("mod-settings-basic-emailRecipients"), $render->input("text", "emailRecipients", $pageData["emailRecipients"], null, false, null, null));
        echo '</table>';
                    
      echo '</div>';

      echo $render->button("submit", "true", translate("save"), false, null, "btn icon save success");
     
    echo '</form>'; 			 
  echo '</section>';     
echo '</article>';
