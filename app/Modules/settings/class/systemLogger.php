<?php
class systemLogger {
    public $moduleName = "settings";
    private $database;

    public $logsTable;
    public $lastLog;

    protected $notifyMessage = null;
    protected $notifyRewrite = true;
    protected $lastNotify = null;

    public function __construct(Database $database) {
        $this->database = $database;
        $this->logsTable = databaseTables::getTable($this->moduleName, "logTable");
    }

    public function showNotification() {
        return $this->notifyMessage;
    }
    public function addNotification($message, $type = "success", $rewritable = true) {
        if ($message === null) {
            return false;
        }

        if ($this->notifyRewrite === true) {
            $this->notifyMessage = $message;
        }
        if ($rewritable === false) {
            $this->notifyRewrite = false;
        }

        $_SESSION["notifications"]["app"] = ["message" => $this->notifyMessage, "type" => $type, "displayed" => "false", "rewritable" => $this->notifyRewrite];
        return true;
    }
    public function setNotificationAction($action) {
        if(isset($_SESSION["notifications"]["app"])){
            $_SESSION["notifications"]["app"]["action"] = $action;
        }
        return true;
    }
    public function removeNotification() {
        $this->lastNotify = $_SESSION["notifications"]["app"];
        unset($_SESSION["notifications"]["app"]);
    }

    private function getLastLog() {
        $sql = "SELECT * FROM `{$this->logsTable}` ORDER BY logTime DESC LIMIT 1;";
        $returnData = $this->database->getQuery($sql, null, true);
        return $returnData;
    }
    public function createLog($langShort, $logType, $logAction, $objectId, $userId, $logInfo = null) {
        $logValues = [
            "langShort" => $langShort,
            "logType" => $logType,
            "logAction" => $logAction,
            "logInfo" => $logInfo,
            "objectId" => $objectId,
            "userId" => $userId,
            "ipAddress" => $this->getIp(),
            "logTime" => date("Y-m-d H:i:s")
        ];

        $returnData = $this->database->insertQuery("INSERT INTO `{$this->logsTable}`", $logValues);

        if ($returnData > 0) {
            $this->lastLog = $returnData;
            return true;
        } else {
            return false;
        }
    }
    private function getIp(){
        $localHostIP = '127.0.0.1';
        $ipAddress = $localHostIP;

        if (isset($_SERVER['HTTP_CLIENT_IP']) && $_SERVER['HTTP_CLIENT_IP'] != $localHostIP){
            $ipAddress = $_SERVER['HTTP_CLIENT_IP'];
        }else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'] != $localHostIP){
            $ipAddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else if (isset($_SERVER['HTTP_X_FORWARDED']) && $_SERVER['HTTP_X_FORWARDED'] != $localHostIP){
            $ipAddress = $_SERVER['HTTP_X_FORWARDED'];
        }else if (isset($_SERVER['HTTP_FORWARDED_FOR']) && $_SERVER['HTTP_FORWARDED_FOR'] != $localHostIP){
            $ipAddress = $_SERVER['HTTP_FORWARDED_FOR'];
        }else if (isset($_SERVER['HTTP_FORWARDED']) && $_SERVER['HTTP_FORWARDED'] != $localHostIP){
            $ipAddress = $_SERVER['HTTP_FORWARDED'];
        }else if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] != $localHostIP){
            $ipAddress = $_SERVER['REMOTE_ADDR'];
        }

        return $ipAddress;
    }

    public function getAllLogs($limit = 1) {
        $sql = "SELECT * FROM `{$this->logsTable}` ORDER BY logTime DESC LIMIT {$limit};";
        $returnData = $this->database->getQuery($sql, null, true);

        return $returnData;
    }
    public function getAllLogsByAction($logAction, $limit = 1) {
        $sql = "SELECT * FROM `{$this->logsTable}` WHERE logAction = :logAction ORDER BY logTime DESC LIMIT {$limit};";
        $returnData = $this->database->getQuery($sql, ["logAction" => $logAction], true);

        return $returnData;
    }
}