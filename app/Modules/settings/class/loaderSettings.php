<?php
class loaderSettings {

    public $moduleName = "settings";
    private $database;
    private $appController;
    private $localeEngine;
    private $localizationClass;

    public $settingsTable;

    public $logoMediaName;
    public $faviconMediaName;

    public $basicSettings;
    public $buildNum = null;

    public function __construct(Database $database, AppController $appController) {
        $this->database = $database;
        $this->appController = $appController;
        $this->localeEngine = $appController->returnLocaleEngine();
        $this->localizationClass = $appController->returnLocalizationClass();

        $this->settingsTable = databaseTables::getTable($this->moduleName, "settingTable");
        $this->logoMediaName = $this->getModuleConfig("mediaSettings")["logo"];
        $this->faviconMediaName = $this->getModuleConfig("mediaSettings")["favicon"];
    }
    public function getModuleConfig($configPart = null) {
        include(MODULE_DIR . $this->moduleName . "/config.php");

        if ($configPart !== null) {
            return (isset($config[$configPart])) ? $config[$configPart] : null;
        } else {
            return $config;
        }
    }

    private function reGroupSettings($settingsData, $translate = true) {
        $systemSettings = [];
        if ($settingsData) {

            foreach ($settingsData as $row) {
                $systemSettings[$row["settName"]] = $row["settValue"];
            }

            if($translate === true){
                $langShort = $this->appController->getLang();
                $mainLanguage = $this->localeEngine->returnMainLanguage();

                if($mainLanguage["langShort"] !== $langShort){
                    $systemSettings = $this->localizationClass->getGroupedTranslationsByTableName($this->settingsTable, $langShort, $systemSettings);
                }
            }
        }

        return $systemSettings;
    }
    private function getBuild() {
        return $this->basicSettings["buildNum"];
    }
    public function updateBuild() {
        $updateBuild = $this->editSettings(["buildNum" => $this->buildNum + 1], "settings");
        return $updateBuild;
    }
    public function loadSettings(Bool $return = false) {
        $this->basicSettings = $this->getSettings();
        $this->buildNum = $this->getBuild();
        return ($return !== true) ? true : $this->basicSettings;
    }

    public function getSettings($reGroup = true, $translate = true) {
        $defaultData = $this->database->getQuery("SELECT * FROM `{$this->settingsTable}` ORDER BY moduleName;", null, true);
        $systemSettings = ($reGroup === true) ? $this->reGroupSettings($defaultData, $translate) : $defaultData;

        return $systemSettings;
    }
    public function getModuleSettings($moduleName, $reGroup = true, $translate = true, Bool $publicOnly = false) {
        $isPublic = ($publicOnly === true) ? "AND isPrivate = 0" : null;
        $sql = "SELECT * FROM `{$this->settingsTable}` WHERE moduleName = :moduleName {$isPublic};";
        $returnData = $this->database->getQuery($sql, ["moduleName" => $moduleName], true);

        $systemSettings = ($reGroup === true) ? $this->reGroupSettings($returnData, $translate) : $returnData;

        return $systemSettings;
    }
    public function getGroupedSettings($settGroup, $reGroup = true, $translate = true) {
        $sql = "SELECT * FROM `{$this->settingsTable}` WHERE settGroup = :settGroup;";
        $returnData = $this->database->getQuery($sql, ["settGroup" => $settGroup], true);

        $systemSettings = ($reGroup === true) ? $this->reGroupSettings($returnData, $translate) : $returnData;

        return $systemSettings;
    }
    public function getGroupedSettingsByModule($moduleName, $settGroup, $reGroup = true, $translate = true) {
        $sql = "SELECT * FROM `{$this->settingsTable}` WHERE moduleName = :moduleName AND settGroup = :settGroup;";
        $returnData = $this->database->getQuery($sql, ["moduleName" => $moduleName, "settGroup" => $settGroup], true);

        $systemSettings = ($reGroup === true) ? $this->reGroupSettings($returnData, $translate) : $returnData;

        return $systemSettings;
    }

    public function addSettings(Array $settingsData) {
        $newSettings = $this->database->insertQuery("INSERT INTO `{$this->settingsTable}`", $settingsData);
        return ($newSettings !== 0) ? true : false;
    }
    public function editSettings($settingsData, $moduleName = null, $settGroup = null) {
        $sql = "UPDATE `{$this->settingsTable}`";
        $whereSql = "WHERE settName = :settName";
        $bindValues = [];

        if ($moduleName !== null) {
            $whereSql .= " AND moduleName = :moduleName";
            $bindValues["moduleName"] = $moduleName;
        }
        if ($settGroup !== null) {
            $whereSql .= " AND settGroup = :settGroup";
            $bindValues["settGroup"] = $settGroup;
        }

        foreach ($settingsData as $settName => $settValue) {
            $bindValues["settName"] = $settName;
            $returnData = $this->database->updateQuery($sql, array("settValue" => $settValue, "updatedDate" => date("Y-m-d H:i:s")), $bindValues, $whereSql);
            if ($returnData !== true) {
                return false;
            }
        }

        return true;
    }

    public function addCustomSettings($settingsName, $settingsData, $moduleName = null, $groupName = null, $serialize = false) {
        $buildResult["settName"] = $settingsName;
        $buildResult["settValue"] = ($serialize === true) ? base64_encode(serialize($settingsData)): $settingsData;
        $buildResult["updatedDate"] = date("Y-m-d H:i:s");
        ($moduleName !== null) ? $buildResult["moduleName"] = $moduleName : null;
        ($groupName !== null) ? $buildResult["settGroup"] = $groupName : null;

        $newSettings = $this->database->insertQuery("INSERT INTO `{$this->settingsTable}`", $buildResult);
        $success = ($newSettings != 0) ? true : false;
        return $success;
    }
    public function editCustomSettings($settingsName, $settingsData, $moduleName = null, $groupName = null, $serialize = false) {
        $buildResult["settValue"] = ($serialize === true) ? base64_encode(serialize($settingsData)): $settingsData;
        $buildResult["updatedDate"] = date("Y-m-d H:i:s");

        $whereCond = "WHERE settName = :settName";
        $whereBind["settName"] = $settingsName;

        if($groupName !== null){
            $whereCond .=  " AND settGroup = :settGroup";
            $whereBind["settGroup"] = $groupName;
        }
        if($moduleName !== null){
            $whereCond .=  " AND moduleName = :moduleName";
            $whereBind["moduleName"] = $moduleName;
        }

        $returnData = $this->database->updateQuery("UPDATE `{$this->settingsTable}`", $buildResult, $whereBind, $whereCond);
        return ($returnData !== true) ? false : true;
    }
    public function getCustomSettings($settingsName = null, $groupName = null, $unserialize = false) {
        if($settingsName === null && $groupName === null){
            return null;
        }else{
            $whereCond = null;
            $whereBind = [];
            if($settingsName !== null){
                $whereCond .=  "settName = :settName";
                $whereBind["settName"] = $settingsName;
            }
            if($groupName !== null){
                $whereCond .= ($whereCond !== null) ? " AND " : null;
                $whereCond .=  "settGroup = :settGroup";
                $whereBind["settGroup"] = $groupName;
            }
            $whereCond = ($whereCond !== null) ? "WHERE ".$whereCond : null;
        }

        $sql = "SELECT * FROM `{$this->settingsTable}` {$whereCond};";
        $returnData = $this->database->getQuery($sql, $whereBind, true);

        if(!empty($returnData) && $unserialize === true){
            foreach($returnData as &$data){
                $data["settValue"] = unserialize(base64_decode($data["settValue"]));
            }
        }

        return (count($returnData) > 1) ? $returnData : $returnData[0];
    }

}