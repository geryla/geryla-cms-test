<?php
class loggerAccess {
    public $moduleName = "settings";
    private $database;
    private $appController;

    public $accessTable;
    private $moduleSettings = null;

    private $blockTimeIp = 3600; // In seconds
    private $blockTimeAttempt = 1800; // In seconds


    public function __construct(Database $database, AppController $appController) {
        $this->database = $database;
        $this->appController = $appController;
        $this->accessTable = databaseTables::getTable($this->moduleName, "accessLog");

        $settingsController =  new loaderSettings($this->database, $appController);
        $this->moduleSettings = $settingsController->getModuleSettings($this->moduleName);
    }

/* ================== BRUTE FORCE prevent and check === */

// Prevent before IP ATTACK (block to 1 hour)
    public function bruteForceIP($ipAddress, $isAdminAccess) {
        $sessionType = ($isAdminAccess == 0) ? "appLogin" : "adminLogin";
        $bruteForce = false;

        if ($this->existInTable($ipAddress, $isAdminAccess) !== true) {
            // First try for ATTACK -> TRUE
            if (!isset($_SESSION["security"][$sessionType]["bruteForce"]["attempt"]) OR empty($_SESSION["security"][$sessionType]["bruteForce"]["attempt"])) {
                $_SESSION["security"][$sessionType]["bruteForce"]["attempt"] = 1;
                // First try for ATTACK -> FALSE -> But user was blocked and block time has expired
            } elseif (isset($_SESSION["security"][$sessionType]["bruteForce"]["userBlocked"]) AND $_SESSION["security"][$sessionType]["bruteForce"]["userBlocked"] <= time()) {
                $_SESSION["security"][$sessionType]["bruteForce"]["attempt"] = 1;
                unset($_SESSION["security"][$sessionType]["bruteForce"]["userBlocked"]);
                // First try for ATTACK -> FALSE -> And user can be blocked -> increase attempts
            } else {
                $_SESSION["security"][$sessionType]["bruteForce"]["attempt"] = $_SESSION["security"][$sessionType]["bruteForce"]["attempt"] + 1;
            }

            // Notify, that IP is blocked
            if ($_SESSION["security"][$sessionType]["bruteForce"]["attempt"] > $this->moduleSettings["bruteForceLimit"]) {
                $this->addIp($_SERVER['REMOTE_ADDR'], 0);
                unset($_SESSION["security"][$sessionType]["bruteForce"]);
                $bruteForce = true;
            }
        } else {
            $bruteForce = true;
        }

        return $bruteForce;
    }
// Prevent before USER MULTIPLE ATTEMPTS (block to 30 min)
    public function bruteForceAttempt($userAttempt) {
        $isBlocked = false;

        if ($userAttempt > 1000) { // TIME = user is blocked
            if ($userAttempt <= time()) { // Time to block is runaway
                $userAttempt = 0;
            } else {
                $isBlocked = true;
            }
        }

        // If user is not blocked, continue in method
        if ($isBlocked === false) {
            $increasedBF = $userAttempt + 1;
            if ($increasedBF < $this->moduleSettings["bruteForceLimit"]) {
                $newAttempt = $increasedBF;
            } else {
                $newAttempt = time() + $this->blockTimeAttempt; // 30 minutes
                $isBlocked = true;
            }
        } else {
            $newAttempt = $userAttempt;
        }

        return ["attempt" => $newAttempt, "isBlocked" => $isBlocked];
    }
// Add user block info to session
    public function createBruteSession($blockedUser, $isAdminAccess, $blockedTime) {
        if ($blockedUser === true) { // TIME = user is blocked
            $sessionType = ($isAdminAccess == 0) ? "appLogin" : "adminLogin";
            if (!isset($_SESSION["security"][$sessionType]["bruteForce"]["userBlocked"])) {
                $_SESSION["security"][$sessionType]["bruteForce"]["userBlocked"] = $blockedTime;
            }
        }
    }

/* ================== Functions for work with data === */

// Check if IP is in table for back/front end and if is new (active)
    public function existInTable($ipAddress, $isAdminAccess) {
        $sql = "SELECT * FROM `{$this->accessTable}` WHERE ipAddress = :ipAddress AND isAdminAccess =:isAdmin AND newEntry = 1;";
        $returnData = $this->database->getQuery($sql, ["ipAddress" => $ipAddress, "isAdmin" => $isAdminAccess], false);

        if (empty($returnData)) {
            return false;
        } else {
            if ($returnData["blockTime"] <= time()) {
                $this->clearIPs($ipAddress, $isAdminAccess);
                return false;
            } else {
                return true;
            }
        }
    }
// Add IP into table
    public function addIp($ipAddress, $isAdminAccess) {
        $ipValues = ["ipAddress" => $ipAddress, "isAdminAccess" => $isAdminAccess, "newEntry" => 1, "blockTime" => time() + $this->blockTimeIp, "createdDate" => date("Y-m-d H:i:s")];
        $returnData = $this->database->insertQuery("INSERT INTO `{$this->accessTable}`", $ipValues);

        return ($returnData > 0) ? true : false;
    }
// Clear all IP by front/back end type
    public function clearIPs($ipAddress, $isAdminAccess) {
        $this->database->updateQuery("UPDATE `{$this->accessTable}`", ["newEntry" => 0], ["ipAddress" => $ipAddress, "isAdminAccess" => $isAdminAccess], "WHERE ipAddress = :ipAddress AND isAdminAccess = :isAdminAccess");

        $sessionType = ($isAdminAccess == 0) ? "appLogin" : "adminLogin";
        session("security.{$sessionType}.bruteForce", null, true);
        return true;
    }

}