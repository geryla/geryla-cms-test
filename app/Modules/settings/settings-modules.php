<?php
/* =============================== FUNCTIONS ======== */
$allModules = $modulesController->loadAllModules();
usort($allModules, function($a, $b) { return $a['title'] <=> $b['title']; });

$dataCompiler->showMessage();
$dataCompiler->refreshPage($dataCompiler->nextStep, "");

/* =============================== CONTENT ======== */ 
echo '<article class="module">';
  
  $render->header(translate("mod-settings-modules-page"), null);
                                            
  echo '<section>';                                      
    echo '<form action="" method="POST" enctype="multipart/form-data">';
        echo '<div class="moduleWrap modSettings">';
            echo '<table class="striped">';

                echo '<tr>';
                    echo '<td><b>'.translate("mod-settings-modules-systemVersion").':</b></td>';
                    echo '<td class="version" colspan="3">v'.SYSTEM_VERSION.'</td>';

                    echo '<td class="right">';
                    echo '<a data-name="checkForSystemUpdate" data-action="update" class="anchor blue"><b><i class="fa fa-refresh"></i>&nbsp;&nbsp;'.translate("mod-settings-modules-checkForUpdate").'</b></a>';
                    echo '</td>';
                echo '</tr>';
                echo '<tr class="spacer"><td>&nbsp;</td></tr>';

                echo '<tr>';
                    echo '<th>'.translate("mod-settings-modules-name").'</th>';
                    echo '<th>'.translate("mod-settings-modules-version").'</th>';
                    echo '<th>'.translate("mod-settings-modules-default").'</th>';
                    echo '<th>'.translate("mod-settings-modules-active").'</th>';
                    echo '<th></th>';
                echo '</tr>';

                foreach($allModules as $module){
                    $moduleInfo = $modulesController->getModuleConfig($module["moduleName"]);

                    $moduleActivated = ($module["activeModule"] == 1) ? true : false;
                    $moduleIsCreated = (file_exists(MODULE_DIR.$module["moduleName"]) && is_dir(MODULE_DIR.$module["moduleName"])) ? true : false;
                    $requiredModules = null;

                    $modulePreparedClass = ($moduleActivated === true && ($moduleInfo["moduleVersion"] != $module["moduleVersion"])) ? "prepared" : null;

                    $moduleError = translate("mod-settings-modules-folderMissing");
                    $moduleErrorClass = "error";

                    if(isset($moduleInfo["requiredModules"])){
                        foreach($moduleInfo["requiredModules"] as $required){
                            if(!$appController->isActiveModule($required["moduleName"])){
                                $requiredModules .= ($requiredModules === null) ? $required["moduleName"] : ','.$required["moduleName"];
                            }
                        }
                    }
                    if($requiredModules !== null){
                        $moduleIsCreated = false;
                        $moduleError = translate("mod-settings-modules-requiredMissing").'<br /><i>'.$requiredModules.'</i>';
                        $moduleErrorClass = "alert";
                    }

                    $moduleMigrate = ($modulePreparedClass !== null) ? '<a data-name="'.$module["moduleName"].'" data-action="update" class="anchor blue"><i class="fa fa-upload"></i> '.translate("mod-settings-modules-migrate").'</a>' : null;
                    if($moduleActivated == 1){
                        $moduleToggle = '<a data-name="'.$module["moduleName"].'" data-action="deactivate"  class="anchor green"><i class="fa fa-low-vision"></i> '.translate("mod-settings-modules-deactivate").'</a>';
                    }else{
                        $moduleToggle = '<a data-name="'.$module["moduleName"].'" data-action="activate"  class="anchor green"><i class="fa fa-low-vision"></i> '.translate("mod-settings-modules-activate").'</a>';
                    }
                    $moduleUninstall = '<a data-name="'.$module["moduleName"].'" data-action="remove" class="anchor red"><i class="fa fa-trash"></i> '.translate("mod-settings-modules-delete").'</a>';

                    echo '<tr class="'.(($modulePreparedClass !== null) ? $modulePreparedClass : null).' '.(($moduleIsCreated === false) ? $moduleErrorClass : null).'">';
                        echo '<td><b>'.$module["title"].'</b><br /><i>'.$module["moduleInfo"].'</i></td>';
                        echo '<td class="version">'.(($moduleInfo !== false) ? 'v'.$moduleInfo["moduleVersion"] : null).'</td>';
                        echo '<td>'.(($module["defaultModule"] == 1) ? "<b class='green'>".translate("mod-settings-yes")."</b>" : "<span class='blue'>".translate("mod-settings-no")."</span>").'</td>';
                        echo '<td>'.(($module["activeModule"] == 1) ? "<b class='green'>".translate("mod-settings-yes")."</b>" : "<span class='red'>".translate("mod-settings-no")."</span>").'</td>';

                        echo '<td class="right">';
                            echo ($moduleIsCreated === true) ? (($moduleActivated === true) ? $moduleMigrate : null) : $moduleError;
                            if($moduleIsCreated === true){
                                echo ($module["defaultModule"] != 1) ? $moduleToggle : null;
                                echo ($moduleActivated === true && $module["defaultModule"] != 1) ? $moduleUninstall : null;
                            }
                        echo '</td>';
                    echo '</tr>';
                }
            echo '</table>';
        echo '</div>';
    echo '</form>'; 			 
  echo '</section>';     
echo '</article>';