$(window).load(function() {

    $(".moduleWrap.modSettings").on('click', '.anchor', function() {
        let name = $(this).data('name');
        let action = $(this).data('action');
        let modulePanel = $(this).closest("tr");

        if(name && action){
            adminLoader(true);
            $.ajax({
                url: requestsUrl,
                type: 'POST',
                data: {requestName: 'moduleMigrations', module: 'settings', response: 'json', data: {action: action, name:name}},
                success: function(returnJson) {
                    let callback = JSON.parse(returnJson);
                    if(callback["status"] === true){
                        location.reload(true);
                    }else{
                        adminLoader(false);
                        $(modulePanel).addClass("red");
                        console.log(returnJson);
                    }
                }
            });
        }
    });

});