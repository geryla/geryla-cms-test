<?php
    $jobFinished = true;

    if(useIfExists(env('API_ENABLED'), false) === true){
        $authController = new \Api\Auth\AuthController($database, $appController);
        $authController->clearExpiredAccesses();
    }

    return $jobFinished;