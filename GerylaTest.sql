-- --------------------------------------------------------
-- Hostitel:                     127.0.0.1
-- Verze serveru:                10.4.27-MariaDB - mariadb.org binary distribution
-- OS serveru:                   Win64
-- HeidiSQL Verze:               12.7.0.6850
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Exportování struktury pro tabulka gerylatest.apisessions
CREATE TABLE IF NOT EXISTS `apisessions` (
  `accessKey` varchar(120) NOT NULL,
  `sessionId` varchar(120) NOT NULL,
  `expire` datetime NOT NULL,
  UNIQUE KEY `accessKeyUnique` (`accessKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Exportování dat pro tabulku gerylatest.apisessions: ~0 rows (přibližně)

-- Exportování struktury pro tabulka gerylatest.contenteditor-anchors
CREATE TABLE IF NOT EXISTS `contenteditor-anchors` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(90) NOT NULL,
  `url` varchar(255) NOT NULL,
  `langShort` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Exportování dat pro tabulku gerylatest.contenteditor-anchors: ~0 rows (přibližně)

-- Exportování struktury pro tabulka gerylatest.contenteditor-bonds
CREATE TABLE IF NOT EXISTS `contenteditor-bonds` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `hookId` int(10) NOT NULL,
  `parentId` int(10) NOT NULL,
  `objectId` int(10) NOT NULL,
  `contentType` int(11) NOT NULL DEFAULT 1,
  `dataTable` varchar(255) NOT NULL DEFAULT '0',
  `orderNum` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `hookId` (`hookId`),
  CONSTRAINT `contenteditor-bonds_ibfk_1` FOREIGN KEY (`hookId`) REFERENCES `contenteditor-hooks` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Exportování dat pro tabulku gerylatest.contenteditor-bonds: ~0 rows (přibližně)

-- Exportování struktury pro tabulka gerylatest.contenteditor-categories
CREATE TABLE IF NOT EXISTS `contenteditor-categories` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parentId` int(10) NOT NULL,
  `title` varchar(75) NOT NULL,
  `shortDesc` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `metaKeywords` varchar(255) NOT NULL,
  `metaTitle` varchar(255) NOT NULL,
  `metaDescription` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `allowIndex` int(1) NOT NULL DEFAULT 1,
  `className` varchar(150) NOT NULL,
  `createdDate` datetime NOT NULL,
  `updatedDate` datetime NOT NULL,
  `orderNum` int(10) NOT NULL DEFAULT 0,
  `visibility` int(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Exportování dat pro tabulku gerylatest.contenteditor-categories: ~2 rows (přibližně)
INSERT INTO `contenteditor-categories` (`id`, `parentId`, `title`, `shortDesc`, `description`, `metaKeywords`, `metaTitle`, `metaDescription`, `url`, `allowIndex`, `className`, `createdDate`, `updatedDate`, `orderNum`, `visibility`) VALUES
	(1, 0, 'Rady a tipy', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed ac dolor sit amet purus malesuada congue. Etiam bibendum elit eget erat. Integer vulputate sem a nibh rutrum consequat. Nulla turpis magna, cursus sit amet, suscipit a, interdum id, felis.', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed ac dolor sit amet purus malesuada congue. Etiam bibendum elit eget erat. Integer vulputate sem a nibh rutrum consequat. Nulla turpis magna, cursus sit amet, suscipit a, interdum id, felis. Duis pulvinar. Nulla pulvinar eleifend sem. Nullam lectus justo, vulputate eget mollis sed, tempor sed magna. Nam quis nulla. Cras elementum. Suspendisse nisl. Fusce tellus odio, dapibus id fermentum quis, suscipit id erat.</p>\r\n<p>Donec quis nibh at felis congue commodo. Etiam bibendum elit eget erat. Sed vel lectus. Donec odio tempus molestie, porttitor ut, iaculis quis, sem. Nunc tincidunt ante vitae massa. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Maecenas fermentum, sem in pharetra pellentesque, velit turpis volutpat ante, in pharetra metus odio a lectus. Donec vitae arcu. Fusce nibh. Aenean placerat. Mauris tincidunt sem sed arcu. Nullam faucibus mi quis velit. In convallis. Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? Mauris metus. Nullam sapien sem, ornare ac, nonummy non, lobortis a enim. Curabitur ligula sapien, pulvinar a vestibulum quis, facilisis vel sapien.</p>\r\n<p>Nullam sapien sem, ornare ac, nonummy non, lobortis a enim. Nullam lectus justo, vulputate eget mollis sed, tempor sed magna. Cras elementum. Curabitur ligula sapien, pulvinar a vestibulum quis, facilisis vel sapien. Morbi scelerisque luctus velit. Morbi leo mi, nonummy eget tristique non, rhoncus non leo. Curabitur bibendum justo non orci. Nulla pulvinar eleifend sem. Aenean fermentum risus id tortor. Integer vulputate sem a nibh rutrum consequat. Integer lacinia. Integer malesuada. Fusce nibh. Integer tempor. Phasellus enim erat, vestibulum vel, aliquam a, posuere eu, velit. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Maecenas ipsum velit, consectetuer eu lobortis ut, dictum at dui.</p>\r\n</body>\r\n</html>', '', '', '', 'rady-a-tipy', 1, '', '2024-06-05 14:44:58', '2024-06-05 14:44:58', 0, 1),
	(2, 0, 'Kouzla a čáry', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed ac dolor sit amet purus malesuada congue. Etiam bibendum elit eget erat. Integer vulputate sem a nibh rutrum consequat. Nulla turpis magna, cursus sit amet, suscipit a, interdum id, felis. Duis', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed ac dolor sit amet purus malesuada congue. Etiam bibendum elit eget erat. Integer vulputate sem a nibh rutrum consequat. Nulla turpis magna, cursus sit amet, suscipit a, interdum id, felis. Duis pulvinar. Nulla pulvinar eleifend sem. Nullam lectus justo, vulputate eget mollis sed, tempor sed magna. Nam quis nulla. Cras elementum. Suspendisse nisl. Fusce tellus odio, dapibus id fermentum quis, suscipit id erat.</p>\r\n<p>Donec quis nibh at felis congue commodo. Etiam bibendum elit eget erat. Sed vel lectus. Donec odio tempus molestie, porttitor ut, iaculis quis, sem. Nunc tincidunt ante vitae massa. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Maecenas fermentum, sem in pharetra pellentesque, velit turpis volutpat ante, in pharetra metus odio a lectus. Donec vitae arcu. Fusce nibh. Aenean placerat. Mauris tincidunt sem sed arcu. Nullam faucibus mi quis velit. In convallis. Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? Mauris metus. Nullam sapien sem, ornare ac, nonummy non, lobortis a enim. Curabitur ligula sapien, pulvinar a vestibulum quis, facilisis vel sapien.</p>\r\n<p>Nullam sapien sem, ornare ac, nonummy non, lobortis a enim. Nullam lectus justo, vulputate eget mollis sed, tempor sed magna. Cras elementum. Curabitur ligula sapien, pulvinar a vestibulum quis, facilisis vel sapien. Morbi scelerisque luctus velit. Morbi leo mi, nonummy eget tristique non, rhoncus non leo. Curabitur bibendum justo non orci. Nulla pulvinar eleifend sem. Aenean fermentum risus id tortor. Integer vulputate sem a nibh rutrum consequat. Integer lacinia. Integer malesuada. Fusce nibh. Integer tempor. Phasellus enim erat, vestibulum vel, aliquam a, posuere eu, velit. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Maecenas ipsum velit, consectetuer eu lobortis ut, dictum at dui.</p>\r\n</body>\r\n</html>', '', '', '', 'kouzla-a-cary', 1, '', '2024-06-05 14:45:16', '2024-06-05 14:45:16', 0, 1);

-- Exportování struktury pro tabulka gerylatest.contenteditor-groups
CREATE TABLE IF NOT EXISTS `contenteditor-groups` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parentId` int(10) NOT NULL,
  `title` varchar(75) NOT NULL,
  `shortDesc` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `metaKeywords` varchar(255) NOT NULL,
  `metaTitle` varchar(255) NOT NULL,
  `metaDescription` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `allowIndex` int(1) NOT NULL DEFAULT 1,
  `shortTag` varchar(50) NOT NULL,
  `className` varchar(150) NOT NULL,
  `templateName` varchar(40) NOT NULL,
  `createdDate` datetime NOT NULL,
  `updatedDate` datetime NOT NULL,
  `orderNum` int(10) NOT NULL DEFAULT 0,
  `visibility` int(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Exportování dat pro tabulku gerylatest.contenteditor-groups: ~0 rows (přibližně)

-- Exportování struktury pro tabulka gerylatest.contenteditor-hooks
CREATE TABLE IF NOT EXISTS `contenteditor-hooks` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(70) NOT NULL,
  `contentType` int(1) NOT NULL DEFAULT 1,
  `limit` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Exportování dat pro tabulku gerylatest.contenteditor-hooks: ~0 rows (přibližně)

-- Exportování struktury pro tabulka gerylatest.contenteditor-labels
CREATE TABLE IF NOT EXISTS `contenteditor-labels` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `groupId` int(10) NOT NULL,
  `title` varchar(40) NOT NULL,
  `shortTag` varchar(40) NOT NULL,
  `color` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Exportování dat pro tabulku gerylatest.contenteditor-labels: ~16 rows (přibližně)
INSERT INTO `contenteditor-labels` (`id`, `groupId`, `title`, `shortTag`, `color`) VALUES
	(1, 1, 'Jaro', 'jaro', '#84bd11'),
	(2, 1, 'Léto', 'leto', '#f2ff00'),
	(3, 1, 'Podzim', 'podzim', '#654d78'),
	(4, 1, 'Zima', 'zima', '#f7e1f7'),
	(5, 2, 'Leden', 'leden', ''),
	(6, 2, 'Únor', 'unor', ''),
	(7, 2, 'Březen', 'brezen', ''),
	(8, 2, 'Duben', 'duben', ''),
	(9, 2, 'Květen', 'kveten', ''),
	(10, 2, 'Červen', 'cerven', ''),
	(11, 2, 'Červenec', 'cervenec', ''),
	(12, 2, 'Srpen', 'srpen', ''),
	(13, 2, 'Září', 'zari', ''),
	(14, 2, 'Říjen', 'rijen', ''),
	(15, 2, 'Listopad', 'listopad', ''),
	(16, 2, 'Prosinec', 'prosinec', '');

-- Exportování struktury pro tabulka gerylatest.contenteditor-labelsgroups
CREATE TABLE IF NOT EXISTS `contenteditor-labelsgroups` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(40) NOT NULL,
  `shortTag` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Exportování dat pro tabulku gerylatest.contenteditor-labelsgroups: ~2 rows (přibližně)
INSERT INTO `contenteditor-labelsgroups` (`id`, `title`, `shortTag`) VALUES
	(1, 'Roční období', 'rocni-obdobi'),
	(2, 'Měsíce v roce', 'mesice-v-roce');

-- Exportování struktury pro tabulka gerylatest.contenteditor-labelshook
CREATE TABLE IF NOT EXISTS `contenteditor-labelshook` (
  `labelId` int(10) NOT NULL,
  `postId` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Exportování dat pro tabulku gerylatest.contenteditor-labelshook: ~16 rows (přibližně)
INSERT INTO `contenteditor-labelshook` (`labelId`, `postId`) VALUES
	(1, 3),
	(3, 3),
	(10, 3),
	(13, 3),
	(5, 4),
	(15, 4),
	(16, 4),
	(2, 5),
	(3, 5),
	(5, 5),
	(7, 5),
	(9, 5),
	(5, 6),
	(10, 6),
	(11, 6),
	(16, 6);

-- Exportování struktury pro tabulka gerylatest.contenteditor-postcategories
CREATE TABLE IF NOT EXISTS `contenteditor-postcategories` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `postId` int(10) NOT NULL,
  `categoryId` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `postId` (`postId`),
  KEY `categoryId` (`categoryId`),
  CONSTRAINT `contenteditor-postcategories_ibfk_1` FOREIGN KEY (`postId`) REFERENCES `contenteditor-posts` (`id`),
  CONSTRAINT `contenteditor-postcategories_ibfk_2` FOREIGN KEY (`categoryId`) REFERENCES `contenteditor-categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Exportování dat pro tabulku gerylatest.contenteditor-postcategories: ~4 rows (přibližně)
INSERT INTO `contenteditor-postcategories` (`id`, `postId`, `categoryId`) VALUES
	(1, 3, 1),
	(2, 4, 2),
	(3, 5, 1),
	(4, 6, 1);

-- Exportování struktury pro tabulka gerylatest.contenteditor-postgroups
CREATE TABLE IF NOT EXISTS `contenteditor-postgroups` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `postId` int(10) NOT NULL,
  `groupId` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `postId` (`postId`),
  KEY `groupId` (`groupId`),
  CONSTRAINT `contenteditor-postgroups_ibfk_1` FOREIGN KEY (`postId`) REFERENCES `contenteditor-posts` (`id`),
  CONSTRAINT `contenteditor-postgroups_ibfk_2` FOREIGN KEY (`groupId`) REFERENCES `contenteditor-groups` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Exportování dat pro tabulku gerylatest.contenteditor-postgroups: ~0 rows (přibližně)

-- Exportování struktury pro tabulka gerylatest.contenteditor-posts
CREATE TABLE IF NOT EXISTS `contenteditor-posts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `urlPrefix` int(11) NOT NULL,
  `postType` int(10) NOT NULL DEFAULT 1,
  `authorId` int(10) DEFAULT 0,
  `autoEnable` datetime NOT NULL,
  `allowIndex` int(1) NOT NULL DEFAULT 1,
  `pageSidebar` varchar(40) NOT NULL,
  `className` varchar(50) NOT NULL,
  `createdDate` datetime NOT NULL,
  `updatedDate` datetime NOT NULL,
  `revisionDate` datetime DEFAULT NULL,
  `orderNum` int(10) NOT NULL DEFAULT 0,
  `visibility` int(1) NOT NULL DEFAULT 1,
  `lengthTime` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `urlPrefix` (`urlPrefix`),
  CONSTRAINT `contenteditor-posts_ibfk_1` FOREIGN KEY (`urlPrefix`) REFERENCES `prefixs` (`urlPrefix`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Exportování dat pro tabulku gerylatest.contenteditor-posts: ~4 rows (přibližně)
INSERT INTO `contenteditor-posts` (`id`, `urlPrefix`, `postType`, `authorId`, `autoEnable`, `allowIndex`, `pageSidebar`, `className`, `createdDate`, `updatedDate`, `revisionDate`, `orderNum`, `visibility`, `lengthTime`) VALUES
	(3, 9027681, 2, 0, '0000-00-00 00:00:00', 1, '', '', '2024-06-05 16:45:00', '2024-06-05 14:45:58', NULL, 0, 1, ''),
	(4, 1365902, 2, 0, '0000-00-00 00:00:00', 1, '', '', '2024-06-05 14:46:26', '2024-06-05 14:46:26', NULL, 0, 1, ''),
	(5, 7518350, 2, 0, '0000-00-00 00:00:00', 1, '', '', '2024-06-05 14:46:53', '2024-06-05 14:46:53', NULL, 0, 1, ''),
	(6, 5909795, 2, 0, '0000-00-00 00:00:00', 1, '', '', '2024-06-05 14:47:12', '2024-06-05 14:47:12', NULL, 0, 1, '');

-- Exportování struktury pro tabulka gerylatest.contenteditor-posts-lang
CREATE TABLE IF NOT EXISTS `contenteditor-posts-lang` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `associatedId` int(10) NOT NULL,
  `title` varchar(75) NOT NULL,
  `shortDesc` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `metaKeywords` varchar(255) NOT NULL,
  `metaTitle` varchar(255) NOT NULL,
  `metaDescription` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `langShort` varchar(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `associatedId` (`associatedId`),
  CONSTRAINT `contenteditor-posts-lang_ibfk_1` FOREIGN KEY (`associatedId`) REFERENCES `contenteditor-posts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Exportování dat pro tabulku gerylatest.contenteditor-posts-lang: ~4 rows (přibližně)
INSERT INTO `contenteditor-posts-lang` (`id`, `associatedId`, `title`, `shortDesc`, `description`, `metaKeywords`, `metaTitle`, `metaDescription`, `url`, `langShort`) VALUES
	(3, 3, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 'onec quis nibh at felis congue commodo. Etiam bibendum elit eget erat. Sed vel lectus. Donec odio tempus molestie, porttitor ut, iaculis quis, sem. ', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Nullam sapien sem, ornare ac, nonummy non, lobortis a enim. Nullam lectus justo, vulputate eget mollis sed, tempor sed magna. Cras elementum. Curabitur ligula sapien, pulvinar a vestibulum quis, facilisis vel sapien. Morbi scelerisque luctus velit. Morbi leo mi, nonummy eget tristique non, rhoncus non leo. Curabitur bibendum justo non orci. Nulla pulvinar eleifend sem. Aenean fermentum risus id tortor. Integer vulputate sem a nibh rutrum consequat. Integer lacinia. Integer malesuada. Fusce nibh. Integer tempor. Phasellus enim erat, vestibulum vel, aliquam a, posuere eu, velit. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Maecenas ipsum velit, consectetuer eu lobortis ut, dictum at dui.</p>\r\n<p>Praesent dapibus. Etiam commodo dui eget wisi. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Maecenas ipsum velit, consectetuer eu lobortis ut, dictum at dui. Etiam commodo dui eget wisi. Praesent dapibus. Quisque porta. Nullam justo enim, consectetuer nec, ullamcorper ac, vestibulum in, elit. Aliquam erat volutpat. Etiam posuere lacus quis dolor. Curabitur vitae diam non enim vestibulum interdum. Integer in sapien. Pellentesque sapien. Pellentesque arcu.</p>\r\n<p>Fusce aliquam vestibulum ipsum. Aenean vel massa quis mauris vehicula lacinia. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet sapien wisi sed libero. Aliquam in lorem sit amet leo accumsan lacinia. Aenean placerat. Etiam ligula pede, sagittis quis, interdum ultricies, scelerisque eu. Quisque porta. Proin mattis lacinia justo. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Etiam quis quam. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>\r\n</body>\r\n</html>', '', '', '', 'lorem-ipsum-dolor-sit-amet-consectetuer-adipiscing-elit', 'cs_CZ'),
	(4, 4, 'Praesent dapibus. Etiam commodo dui eget wisi. ', 'Curabitur ligula sapien, pulvinar a vestibulum quis, facilisis vel sapien. Morbi scelerisque luctus velit. Morbi leo mi, nonummy eget tristique non, rhoncus non leo. Curabitur bibendum justo non orci. Nulla pulvinar eleifend sem. Aenean fermentum risus id', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Praesent dapibus. Etiam commodo dui eget wisi. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Maecenas ipsum velit, consectetuer eu lobortis ut, dictum at dui. Etiam commodo dui eget wisi. Praesent dapibus. Quisque porta. Nullam justo enim, consectetuer nec, ullamcorper ac, vestibulum in, elit. Aliquam erat volutpat. Etiam posuere lacus quis dolor. Curabitur vitae diam non enim vestibulum interdum. Integer in sapien. Pellentesque sapien. Pellentesque arcu.</p>\r\n<p>Fusce aliquam vestibulum ipsum. Aenean vel massa quis mauris vehicula lacinia. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet sapien wisi sed libero. Aliquam in lorem sit amet leo accumsan lacinia. Aenean placerat. Etiam ligula pede, sagittis quis, interdum ultricies, scelerisque eu. Quisque porta. Proin mattis lacinia justo. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Etiam quis quam. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>\r\n</body>\r\n</html>', '', '', '', 'praesent-dapibus-etiam-commodo-dui-eget-wisi', 'cs_CZ'),
	(5, 5, 'Fusce aliquam vestibulum ipsum. Aenean vel massa quis mauris vehicula lacin', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed ac dolor sit amet purus malesuada congue. Etiam bibendum elit eget erat. Integer vulputate sem a nibh rutrum consequat. Nulla turpis magna, cursus sit amet, suscipit a, interdum id, felis. Duis', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Donec quis nibh at felis congue commodo. Etiam bibendum elit eget erat. Sed vel lectus. Donec odio tempus molestie, porttitor ut, iaculis quis, sem. Nunc tincidunt ante vitae massa. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Maecenas fermentum, sem in pharetra pellentesque, velit turpis volutpat ante, in pharetra metus odio a lectus. Donec vitae arcu. Fusce nibh. Aenean placerat. Mauris tincidunt sem sed arcu. Nullam faucibus mi quis velit. In convallis. Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? Mauris metus. Nullam sapien sem, ornare ac, nonummy non, lobortis a enim. Curabitur ligula sapien, pulvinar a vestibulum quis, facilisis vel sapien.</p>\r\n<p>Nullam sapien sem, ornare ac, nonummy non, lobortis a enim. Nullam lectus justo, vulputate eget mollis sed, tempor sed magna. Cras elementum. Curabitur ligula sapien, pulvinar a vestibulum quis, facilisis vel sapien. Morbi scelerisque luctus velit. Morbi leo mi, nonummy eget tristique non, rhoncus non leo. Curabitur bibendum justo non orci. Nulla pulvinar eleifend sem. Aenean fermentum risus id tortor. Integer vulputate sem a nibh rutrum consequat. Integer lacinia. Integer malesuada. Fusce nibh. Integer tempor. Phasellus enim erat, vestibulum vel, aliquam a, posuere eu, velit. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Maecenas ipsum velit, consectetuer eu lobortis ut, dictum at dui.</p>\r\n</body>\r\n</html>', '', '', '', 'fusce-aliquam-vestibulum-ipsum-aenean-vel-massa-quis-mauris-vehicula-lacinia', 'cs_CZ'),
	(6, 6, 'Fusce aliquam vestibulum ipsum. Aenean vel massa quis mauris vehicula lacin', 'Quisque porta. Proin mattis lacinia justo. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Etiam quis quam. Neque porro q', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Nullam sapien sem, ornare ac, nonummy non, lobortis a enim. Nullam lectus justo, vulputate eget mollis sed, tempor sed magna. Cras elementum. Curabitur ligula sapien, pulvinar a vestibulum quis, facilisis vel sapien. Morbi scelerisque luctus velit. Morbi leo mi, nonummy eget tristique non, rhoncus non leo. Curabitur bibendum justo non orci. Nulla pulvinar eleifend sem. Aenean fermentum risus id tortor. Integer vulputate sem a nibh rutrum consequat. Integer lacinia. Integer malesuada. Fusce nibh. Integer tempor. Phasellus enim erat, vestibulum vel, aliquam a, posuere eu, velit. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Maecenas ipsum velit, consectetuer eu lobortis ut, dictum at dui.</p>\r\n<p>Praesent dapibus. Etiam commodo dui eget wisi. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Maecenas ipsum velit, consectetuer eu lobortis ut, dictum at dui. Etiam commodo dui eget wisi. Praesent dapibus. Quisque porta. Nullam justo enim, consectetuer nec, ullamcorper ac, vestibulum in, elit. Aliquam erat volutpat. Etiam posuere lacus quis dolor. Curabitur vitae diam non enim vestibulum interdum. Integer in sapien. Pellentesque sapien. Pellentesque arcu.</p>\r\n<p>Fusce aliquam vestibulum ipsum. Aenean vel massa quis mauris vehicula lacinia. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet sapien wisi sed libero. Aliquam in lorem sit amet leo accumsan lacinia. Aenean placerat. Etiam ligula pede, sagittis quis, interdum ultricies, scelerisque eu. Quisque porta. Proin mattis lacinia justo. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Etiam quis quam. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>\r\n</body>\r\n</html>', '', '', '', 'fusce-aliquam-vestibulum-ipsum-aenean-vel-massa-quis-mauris-vehicula-lacinia', 'cs_CZ');

-- Exportování struktury pro tabulka gerylatest.email-messages
CREATE TABLE IF NOT EXISTS `email-messages` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `emailKey` varchar(40) NOT NULL,
  `email` varchar(40) DEFAULT NULL,
  `message` longtext NOT NULL,
  `createdDate` datetime NOT NULL,
  `settlementDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Exportování dat pro tabulku gerylatest.email-messages: ~0 rows (přibližně)

-- Exportování struktury pro tabulka gerylatest.emailtemplates
CREATE TABLE IF NOT EXISTS `emailtemplates` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(75) NOT NULL,
  `emailSubject` varchar(255) NOT NULL,
  `emailContent` longtext DEFAULT NULL,
  `emailVariables` text DEFAULT NULL,
  `emailKey` varchar(75) NOT NULL,
  `moduleNames` varchar(255) NOT NULL,
  `createdDate` datetime NOT NULL,
  `updatedDate` datetime NOT NULL,
  `recipient` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Exportování dat pro tabulku gerylatest.emailtemplates: ~0 rows (přibližně)

-- Exportování struktury pro tabulka gerylatest.language
CREATE TABLE IF NOT EXISTS `language` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `currency` varchar(5) NOT NULL,
  `symbol` varchar(5) NOT NULL,
  `symbolPosition` varchar(5) NOT NULL DEFAULT 'right',
  `isoAlpha2` varchar(2) NOT NULL,
  `isoAlpha3` varchar(3) NOT NULL,
  `langShort` varchar(5) NOT NULL,
  `exchangeRate` decimal(9,3) NOT NULL DEFAULT 0.000,
  `useTax` int(1) NOT NULL DEFAULT 0,
  `round` int(1) DEFAULT 0,
  `decimals` int(2) DEFAULT 0,
  `useRate` int(1) DEFAULT 1,
  `defaultNoTax` int(1) DEFAULT 1,
  `main` int(1) DEFAULT 0,
  `visibility` int(1) NOT NULL DEFAULT 0,
  `orderNum` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Exportování dat pro tabulku gerylatest.language: ~2 rows (přibližně)
INSERT INTO `language` (`id`, `title`, `currency`, `symbol`, `symbolPosition`, `isoAlpha2`, `isoAlpha3`, `langShort`, `exchangeRate`, `useTax`, `round`, `decimals`, `useRate`, `defaultNoTax`, `main`, `visibility`, `orderNum`) VALUES
	(1, 'Čeština', 'CZK', 'Kč', 'right', 'CZ', 'CZE', 'cs_CZ', 0.000, 1, 1, 0, 1, 0, 1, 1, 0),
	(2, 'Angličtina', 'EUR', '€', 'right', 'EN', 'GBR', 'en_GB', 25.000, 1, 1, 2, 1, 0, 0, 1, 0);

-- Exportování struktury pro tabulka gerylatest.language-translations
CREATE TABLE IF NOT EXISTS `language-translations` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `objectId` varchar(15) NOT NULL,
  `objectType` varchar(25) DEFAULT NULL,
  `urlDefault` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `translate` longtext DEFAULT NULL,
  `contentTable` varchar(75) NOT NULL,
  `langShort` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Exportování dat pro tabulku gerylatest.language-translations: ~0 rows (přibližně)

-- Exportování struktury pro tabulka gerylatest.logs
CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `langShort` varchar(5) NOT NULL,
  `logType` varchar(30) NOT NULL,
  `logAction` varchar(35) NOT NULL,
  `logInfo` text DEFAULT NULL,
  `objectId` varchar(15) NOT NULL,
  `userId` int(10) DEFAULT NULL,
  `logTime` datetime NOT NULL,
  `ipAddress` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Exportování dat pro tabulku gerylatest.logs: ~30 rows (přibližně)
INSERT INTO `logs` (`id`, `langShort`, `logType`, `logAction`, `logInfo`, `objectId`, `userId`, `logTime`, `ipAddress`) VALUES
	(1, 'cs_CZ', 'mediaManager', 'upload', NULL, '1', NULL, '2024-06-04 14:51:40', '127.0.0.1'),
	(2, 'cs_CZ', 'mediaManager', 'regenerate', NULL, '1', NULL, '2024-06-04 14:51:41', '127.0.0.1'),
	(3, 'cs_CZ', 'mediaManager', 'deleteMedia', NULL, '1', NULL, '2024-06-04 14:51:43', '127.0.0.1'),
	(4, 'cs_CZ', 'contentEditor', 'Post', NULL, '1', NULL, '2024-06-04 15:11:09', '127.0.0.1'),
	(5, 'cs_CZ', 'contentEditor', 'Post', NULL, '2', NULL, '2024-06-04 15:12:46', '127.0.0.1'),
	(6, 'cs_CZ', 'posts', 'deleting', NULL, '2', NULL, '2024-06-04 15:12:51', '127.0.0.1'),
	(7, 'cs_CZ', 'contentEditor', 'PostLabelGroup', NULL, '1', NULL, '2024-06-05 14:41:47', '127.0.0.1'),
	(8, 'cs_CZ', 'contentEditor', 'PostLabelGroup', NULL, '2', NULL, '2024-06-05 14:42:09', '127.0.0.1'),
	(9, 'cs_CZ', 'contentEditor', 'PostLabel', NULL, '1', NULL, '2024-06-05 14:42:35', '127.0.0.1'),
	(10, 'cs_CZ', 'contentEditor', 'PostLabel', NULL, '2', NULL, '2024-06-05 14:42:46', '127.0.0.1'),
	(11, 'cs_CZ', 'contentEditor', 'PostLabel', NULL, '3', NULL, '2024-06-05 14:43:03', '127.0.0.1'),
	(12, 'cs_CZ', 'contentEditor', 'PostLabel', NULL, '4', NULL, '2024-06-05 14:43:19', '127.0.0.1'),
	(13, 'cs_CZ', 'contentEditor', 'PostLabel', NULL, '5', NULL, '2024-06-05 14:43:28', '127.0.0.1'),
	(14, 'cs_CZ', 'contentEditor', 'PostLabel', NULL, '6', NULL, '2024-06-05 14:43:33', '127.0.0.1'),
	(15, 'cs_CZ', 'contentEditor', 'PostLabel', NULL, '7', NULL, '2024-06-05 14:43:39', '127.0.0.1'),
	(16, 'cs_CZ', 'contentEditor', 'PostLabel', NULL, '8', NULL, '2024-06-05 14:43:46', '127.0.0.1'),
	(17, 'cs_CZ', 'contentEditor', 'PostLabel', NULL, '9', NULL, '2024-06-05 14:43:50', '127.0.0.1'),
	(18, 'cs_CZ', 'contentEditor', 'PostLabel', NULL, '10', NULL, '2024-06-05 14:43:55', '127.0.0.1'),
	(19, 'cs_CZ', 'contentEditor', 'PostLabel', NULL, '11', NULL, '2024-06-05 14:44:00', '127.0.0.1'),
	(20, 'cs_CZ', 'contentEditor', 'PostLabel', NULL, '12', NULL, '2024-06-05 14:44:04', '127.0.0.1'),
	(21, 'cs_CZ', 'contentEditor', 'PostLabel', NULL, '13', NULL, '2024-06-05 14:44:09', '127.0.0.1'),
	(22, 'cs_CZ', 'contentEditor', 'PostLabel', NULL, '14', NULL, '2024-06-05 14:44:13', '127.0.0.1'),
	(23, 'cs_CZ', 'contentEditor', 'PostLabel', NULL, '15', NULL, '2024-06-05 14:44:17', '127.0.0.1'),
	(24, 'cs_CZ', 'contentEditor', 'PostLabel', NULL, '16', NULL, '2024-06-05 14:44:24', '127.0.0.1'),
	(25, 'cs_CZ', 'contentEditor', 'addPostCategory', NULL, '1', NULL, '2024-06-05 14:44:59', '127.0.0.1'),
	(26, 'cs_CZ', 'contentEditor', 'addPostCategory', NULL, '2', NULL, '2024-06-05 14:45:16', '127.0.0.1'),
	(27, 'cs_CZ', 'contentEditor', 'BlogPost', NULL, '3', NULL, '2024-06-05 14:45:58', '127.0.0.1'),
	(28, 'cs_CZ', 'contentEditor', 'BlogPost', NULL, '4', NULL, '2024-06-05 14:46:27', '127.0.0.1'),
	(29, 'cs_CZ', 'contentEditor', 'BlogPost', NULL, '5', NULL, '2024-06-05 14:46:53', '127.0.0.1'),
	(30, 'cs_CZ', 'contentEditor', 'BlogPost', NULL, '6', NULL, '2024-06-05 14:47:13', '127.0.0.1');

-- Exportování struktury pro tabulka gerylatest.mediamanager
CREATE TABLE IF NOT EXISTS `mediamanager` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `objectId` int(10) DEFAULT 0,
  `template` varchar(40) DEFAULT NULL,
  `head` int(1) DEFAULT 0,
  `grouped` int(1) DEFAULT 0,
  `duplicated` int(1) NOT NULL DEFAULT 0,
  `fileType` varchar(255) NOT NULL,
  `fileSize` int(255) DEFAULT 0,
  `folder` int(11) NOT NULL DEFAULT 0,
  `orderNum` int(10) NOT NULL DEFAULT 0,
  `createdDate` datetime NOT NULL,
  `updatedDate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Exportování dat pro tabulku gerylatest.mediamanager: ~0 rows (přibližně)

-- Exportování struktury pro tabulka gerylatest.mediamanager-folders
CREATE TABLE IF NOT EXISTS `mediamanager-folders` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parentId` int(10) NOT NULL,
  `title` varchar(75) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Exportování dat pro tabulku gerylatest.mediamanager-folders: ~0 rows (přibližně)

-- Exportování struktury pro tabulka gerylatest.mediamanager-lang
CREATE TABLE IF NOT EXISTS `mediamanager-lang` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `associatedId` int(10) NOT NULL,
  `title` varchar(75) DEFAULT NULL,
  `alt` varchar(75) DEFAULT NULL,
  `langShort` varchar(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `associatedId` (`associatedId`),
  CONSTRAINT `mediamanager-lang_ibfk_1` FOREIGN KEY (`associatedId`) REFERENCES `mediamanager` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Exportování dat pro tabulku gerylatest.mediamanager-lang: ~0 rows (přibližně)

-- Exportování struktury pro tabulka gerylatest.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `migration` varchar(75) NOT NULL,
  `moduleName` varchar(50) NOT NULL,
  `createdDate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Exportování dat pro tabulku gerylatest.migrations: ~71 rows (přibližně)
INSERT INTO `migrations` (`id`, `migration`, `moduleName`, `createdDate`) VALUES
	(1, 'migration_26.09.2019.sql', '', '2024-02-22 09:25:29'),
	(2, 'migration_17.02.2020.sql', '', '2024-02-22 09:25:29'),
	(3, 'migration_03.03.2020.sql', '', '2024-02-22 09:25:29'),
	(4, 'migration_08.05.2020.sql', '', '2024-02-22 09:25:29'),
	(5, 'migration_22.05.2020.sql', '', '2024-02-22 09:25:30'),
	(6, 'migration_30.06.2020.sql', '', '2024-02-22 09:25:30'),
	(7, 'migration_02.07.2020.sql', '', '2024-02-22 09:25:30'),
	(8, 'migration_19.01.2021.sql', '', '2024-02-22 09:25:30'),
	(9, 'migration_26.01.2021.sql', '', '2024-02-22 09:25:30'),
	(10, 'migration_19.02.2021.sql', '', '2024-02-22 09:25:30'),
	(11, 'migration_10.03.2021.sql', '', '2024-02-22 09:25:30'),
	(12, 'migration_12.03.2021.sql', '', '2024-02-22 09:25:30'),
	(13, 'migration_26.03.2021.sql', '', '2024-02-22 09:25:30'),
	(14, 'migration_01.04.2021.sql', '', '2024-02-22 09:25:30'),
	(15, 'migration_20.04.2021.sql', '', '2024-02-22 09:25:30'),
	(16, 'migration_03.06.2021.sql', '', '2024-02-22 09:25:31'),
	(17, 'migration_11.06.2021.sql', '', '2024-02-22 09:25:31'),
	(18, 'migration_15.07.2021.sql', '', '2024-02-22 09:25:31'),
	(19, 'migration_17.02.2022.sql', '', '2024-02-22 09:25:31'),
	(20, 'migration_02.03.2022.sql', '', '2024-02-22 09:25:31'),
	(21, 'migration_22.03.2022.sql', '', '2024-02-22 09:25:31'),
	(22, 'migration_26.03.2022.sql', '', '2024-02-22 09:25:31'),
	(23, 'migration_14.04.2022.sql', '', '2024-02-22 09:25:31'),
	(24, 'migration_27.04.2022.sql', '', '2024-02-22 09:25:31'),
	(25, 'migration_10.05.2022.sql', '', '2024-02-22 09:25:31'),
	(26, 'migration_28.11.2022.sql', '', '2024-02-22 09:25:31'),
	(27, 'migration_15.02.2023.sql', '', '2024-02-22 09:25:32'),
	(28, 'migration_06.04.2023.sql', '', '2024-02-22 09:25:32'),
	(29, 'migration_24.04.2023.sql', '', '2024-02-22 09:25:32'),
	(30, 'migration_02.06.2023.sql', '', '2024-02-22 09:25:32'),
	(31, 'migration_23.08.2023.sql', '', '2024-02-22 09:25:32'),
	(32, 'migration_07.09.2023.sql', '', '2024-02-22 09:25:32'),
	(33, 'migration_26.09.2023.sql', '', '2024-02-22 09:25:32'),
	(34, 'migration_18.10.2023.sql', '', '2024-02-22 09:25:32'),
	(35, 'migration_07.11.2023.sql', '', '2024-02-22 09:25:32'),
	(36, 'migration_10.01.2024.sql', '', '2024-02-22 09:25:32'),
	(37, 'migration_26.09.2019.sql', 'templates', '2024-02-22 09:25:33'),
	(38, 'migration_28.01.2020.sql', 'templates', '2024-02-22 09:25:34'),
	(39, 'migration_06.01.2024.sql', 'templates', '2024-02-22 09:25:34'),
	(40, 'migration_26.09.2019.sql', 'settings', '2024-02-22 09:25:34'),
	(41, 'migration_17.02.2020.sql', 'settings', '2024-02-22 09:25:34'),
	(42, 'migration_12.03.2021.sql', 'settings', '2024-02-22 09:25:34'),
	(43, 'migration_22.04.2021.sql', 'settings', '2024-02-22 09:25:35'),
	(44, 'migration_09.11.2023.sql', 'settings', '2024-02-22 09:25:35'),
	(45, 'migration_27.11.2023.sql', 'settings', '2024-02-22 09:25:35'),
	(46, 'migration_04.01.2024.sql', 'settings', '2024-02-22 09:25:35'),
	(47, 'migration_19.01.2024.sql', 'settings', '2024-02-22 09:25:35'),
	(48, 'migration_26.09.2019.sql', 'localization', '2024-02-22 09:25:35'),
	(49, 'migration_06.11.2019.sql', 'localization', '2024-02-22 09:25:35'),
	(50, 'migration_15.09.2021.sql', 'localization', '2024-02-22 09:25:35'),
	(51, 'migration_17.09.2021.sql', 'localization', '2024-02-22 09:25:36'),
	(52, 'migration_07.01.2022.sql', 'localization', '2024-02-22 09:25:36'),
	(56, 'migration_26.09.2019.sql', 'mediaManager', '2024-02-22 09:25:37'),
	(57, 'migration_26.09.2019.sql', 'contentEditor', '2024-02-22 09:25:37'),
	(58, 'migration_12.12.2019.sql', 'contentEditor', '2024-02-22 09:25:39'),
	(59, 'migration_15.01.2020.sql', 'contentEditor', '2024-02-22 09:25:39'),
	(60, 'migration_20.06.2023.sql', 'contentEditor', '2024-02-22 09:25:39'),
	(61, 'migration_25.09.2023.sql', 'contentEditor', '2024-02-22 09:25:39'),
	(62, 'migration_26.09.2019.sql', 'emailTemplates', '2024-02-22 09:25:40'),
	(63, 'migration_08.02.2021.sql', 'emailTemplates', '2024-02-22 09:25:40'),
	(64, 'migration_01.12.2022.sql', 'emailTemplates', '2024-02-22 09:25:41'),
	(65, 'migration_06.11.2023.sql', 'emailTemplates', '2024-02-22 09:25:41'),
	(66, 'migration_07.11.2023.sql', 'emailTemplates', '2024-02-22 09:25:41'),
	(75, 'migration_22.02.2024.sql', '', '2024-02-22 12:14:59'),
	(125, 'migration_10.04.2024.sql', '', '2024-04-11 08:03:16'),
	(127, 'migration_09.04.2024.sql', 'settings', '2024-04-11 08:03:22'),
	(128, 'migration_25.04.2024.sql', '', '2024-05-06 09:41:48'),
	(129, 'migration_03.05.2024.sql', 'contentEditor', '2024-05-06 09:41:51'),
	(130, 'migration_19.06.2023.sql', 'templates', '2024-05-06 09:41:55');

-- Exportování struktury pro tabulka gerylatest.modules
CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `title` varchar(35) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `moduleName` varchar(25) NOT NULL,
  `moduleInfo` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `moduleVersion` varchar(10) DEFAULT NULL,
  `contentTables` varchar(255) NOT NULL,
  `replaceContentTables` varchar(255) NOT NULL,
  `moduleGroup` int(15) NOT NULL DEFAULT 0,
  `defaultModule` int(1) NOT NULL DEFAULT 0,
  `defaultPriority` int(1) DEFAULT 0,
  `activeModule` int(1) NOT NULL DEFAULT 0,
  `orderNum` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Exportování dat pro tabulku gerylatest.modules: ~7 rows (přibližně)
INSERT INTO `modules` (`id`, `title`, `moduleName`, `moduleInfo`, `moduleVersion`, `contentTables`, `replaceContentTables`, `moduleGroup`, `defaultModule`, `defaultPriority`, `activeModule`, `orderNum`) VALUES
	(1, 'Templates', 'templates', 'All templates which are active for website and prefixes for pages', '3.6.1', '', '', 6, 1, 1, 1, 1),
	(2, 'Languages', 'localization', 'All languages functions (content, translate, multicurrency eshop, etc.)', '3.0.0', '', '', 3, 1, 3, 1, 1),
	(3, 'Settings', 'settings', 'Basic settings for website and log file of actions', '4.5.0', '', '', 0, 1, 2, 1, 13),
	(4, 'Filtration', 'filtration', 'Filtration (frontend & backend - products, posts, etc.)', '3.5.2', '', '', 0, 1, 5, 1, 7),
	(5, 'Media manager', 'mediaManager', 'Upload and editing files like images, pdf, etc.', '5.0.0', '', '', 0, 1, 6, 1, 6),
	(6, 'Content editor', 'contentEditor', 'Create posts, blog posts, etc.', '4.3.0', 'contenteditor-categories, contenteditor-groups, contenteditor-posts-lang', '', 0, 1, 7, 1, 3),
	(7, 'Email templates', 'emailTemplates', 'Dynamic email templates sended by other modules actions', '4.6.0', '', '', 0, 1, 8, 1, 11);

-- Exportování struktury pro tabulka gerylatest.prefixs
CREATE TABLE IF NOT EXISTS `prefixs` (
  `urlPrefix` int(11) NOT NULL,
  `contentTable` varchar(255) NOT NULL,
  PRIMARY KEY (`urlPrefix`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Exportování dat pro tabulku gerylatest.prefixs: ~7 rows (přibližně)
INSERT INTO `prefixs` (`urlPrefix`, `contentTable`) VALUES
	(0, 'templates'),
	(1365902, 'contenteditor-posts'),
	(4301894, 'contenteditor-posts'),
	(5909795, 'contenteditor-posts'),
	(7518350, 'contenteditor-posts'),
	(7776889, 'contenteditor-posts'),
	(9027681, 'contenteditor-posts');

-- Exportování struktury pro tabulka gerylatest.redirects
CREATE TABLE IF NOT EXISTS `redirects` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `alias` varchar(255) NOT NULL,
  `inputUrl` text NOT NULL,
  `outputUrl` text NOT NULL,
  `status` varchar(5) NOT NULL,
  `langShort` varchar(5) DEFAULT NULL,
  `visibility` int(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Exportování dat pro tabulku gerylatest.redirects: ~0 rows (přibližně)

-- Exportování struktury pro tabulka gerylatest.scheduledjobs
CREATE TABLE IF NOT EXISTS `scheduledjobs` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) NOT NULL,
  `moduleName` varchar(120) DEFAULT NULL,
  `parameters` longtext DEFAULT NULL,
  `dependency` int(10) DEFAULT 0,
  `status` varchar(30) NOT NULL,
  `execution` float DEFAULT NULL,
  `message` text DEFAULT NULL,
  `startDate` datetime DEFAULT NULL,
  `updatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Exportování dat pro tabulku gerylatest.scheduledjobs: ~0 rows (přibližně)

-- Exportování struktury pro tabulka gerylatest.schedules
CREATE TABLE IF NOT EXISTS `schedules` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `cronJob` text NOT NULL,
  `moduleName` varchar(50) DEFAULT NULL,
  `dailyFrequency` int(10) NOT NULL DEFAULT 1,
  `jobRuns` int(10) NOT NULL DEFAULT 0,
  `start` int(2) NOT NULL DEFAULT 0,
  `end` int(2) NOT NULL DEFAULT 23,
  `monday` int(1) DEFAULT 0,
  `tuesday` int(1) DEFAULT 0,
  `wednesday` int(1) DEFAULT 0,
  `thursday` int(1) DEFAULT 0,
  `friday` int(1) DEFAULT 0,
  `saturday` int(1) DEFAULT 0,
  `sunday` int(1) DEFAULT 0,
  `orderNum` int(10) NOT NULL DEFAULT 0,
  `visibility` int(1) NOT NULL DEFAULT 1,
  `lastRun` datetime DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Exportování dat pro tabulku gerylatest.schedules: ~3 rows (přibližně)
INSERT INTO `schedules` (`id`, `cronJob`, `moduleName`, `dailyFrequency`, `jobRuns`, `start`, `end`, `monday`, `tuesday`, `wednesday`, `thursday`, `friday`, `saturday`, `sunday`, `orderNum`, `visibility`, `lastRun`, `createdDate`, `updatedDate`) VALUES
	(1, 'sitemap.php', 'templates', 1, 0, 2, 3, 1, 1, 1, 1, 1, 1, 1, 0, 1, NULL, NULL, NULL),
	(2, 'clearAccessKeys.php', 'settings', 1, 0, 3, 4, 1, 1, 1, 1, 1, 1, 1, 0, 1, NULL, NULL, NULL);

-- Exportování struktury pro tabulka gerylatest.settings
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `settName` varchar(150) NOT NULL,
  `settValue` text DEFAULT NULL,
  `moduleName` varchar(25) DEFAULT NULL,
  `settGroup` varchar(40) DEFAULT NULL,
  `isPrivate` int(1) DEFAULT 1,
  `updatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Exportování dat pro tabulku gerylatest.settings: ~16 rows (přibližně)
INSERT INTO `settings` (`id`, `settName`, `settValue`, `moduleName`, `settGroup`, `isPrivate`, `updatedDate`) VALUES
	(1, 'buildNum', '22', 'settings', '', 1, '2024-02-22 11:12:05'),
	(2, 'emailRecipients', '', 'settings', '', 1, NULL),
	(3, 'enableCache', 'false', 'settings', 'configuration', 1, NULL),
	(4, 'bruteForceLimit', '3', 'settings', 'configuration', 1, NULL),
	(5, 'showMismatchContent', 'false', 'localization', '', 1, NULL),
	(6, 'scheduler-token', '23e4a28824a6c6c4aa38', 'settings', 'scheduler', 1, NULL),
	(7, 'showMismatchContent', 'false', 'localization', '', 1, NULL),
	(9, 'emailSettings-emailName', '', 'emailTemplates', '', 1, NULL),
	(10, 'emailSettings-emailUser', '', 'emailTemplates', '', 1, NULL),
	(11, 'emailSettings-encoding', 'UTF-8', 'emailTemplates', '', 1, NULL),
	(12, 'emailSettings-method', 'mail', 'emailTemplates', '', 1, NULL),
	(13, 'emailSettings-smtp-server', '', 'emailTemplates', 'smtp', 1, NULL),
	(14, 'emailSettings-smtp-port', '', 'emailTemplates', 'smtp', 1, NULL),
	(15, 'emailSettings-smtp-protocol', 'tls', 'emailTemplates', 'smtp', 1, NULL),
	(16, 'emailSettings-smtp-password', '', 'emailTemplates', 'smtp', 1, NULL),
	(17, 'emailSettings-smtp-email', '', 'emailTemplates', 'smtp', 1, NULL);

-- Exportování struktury pro tabulka gerylatest.templates
CREATE TABLE IF NOT EXISTS `templates` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `urlPrefix` int(11) NOT NULL,
  `allowIndex` int(1) NOT NULL DEFAULT 1,
  `pageType` int(1) NOT NULL DEFAULT 1,
  `templateName` varchar(40) NOT NULL,
  `moduleName` varchar(255) NOT NULL,
  `className` varchar(55) DEFAULT NULL,
  `loggedOnly` int(9) DEFAULT 0,
  `pageSidebar` varchar(40) NOT NULL,
  `pageTopMenu` int(1) NOT NULL DEFAULT 1,
  `createdDate` datetime NOT NULL,
  `updatedDate` datetime NOT NULL,
  `visibility` int(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `urlPrefix` (`urlPrefix`),
  CONSTRAINT `templates_ibfk_1` FOREIGN KEY (`urlPrefix`) REFERENCES `prefixs` (`urlPrefix`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Exportování dat pro tabulku gerylatest.templates: ~0 rows (přibližně)

-- Exportování struktury pro tabulka gerylatest.templates-lang
CREATE TABLE IF NOT EXISTS `templates-lang` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `associatedId` int(10) NOT NULL,
  `title` varchar(75) NOT NULL,
  `shortDesc` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `metaKeywords` varchar(255) NOT NULL,
  `metaTitle` varchar(255) NOT NULL,
  `metaDescription` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `youtubeUrl` varchar(255) NOT NULL,
  `langShort` varchar(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `associatedId` (`associatedId`),
  CONSTRAINT `templates-lang_ibfk_1` FOREIGN KEY (`associatedId`) REFERENCES `templates` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Exportování dat pro tabulku gerylatest.templates-lang: ~0 rows (přibližně)

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
