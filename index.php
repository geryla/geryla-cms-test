<?php
/**
 * Athena CMS
 * @package Athena
 * @author Marcel Miliček <milicek.marcel@seznam.cz>
 * @copyright ReSs Design & Geryla s.r.o.
 */

(session_status() == PHP_SESSION_NONE) ? session_start() : null;
$_SESSION["start_time"] = ($_SESSION["start_time"]) ?? time();
$_SESSION["last_action_time"] = time();

define('INIT', microtime(true));
define('ROOT', __DIR__);
define("SYSTEM_VERSION", "3.2.0"); //TODO: Connect with system migrations

/* Autoload register */
require_once(ROOT.'/app/Autoloader.php');
new Autoloader();

require_once(EVENTS_DIR.'Render.php');
require_once(EVENTS_DIR.'RequestsManager.php');

// VIEW render
require_once(KERNEL_DIR.'PanelKernel.php');