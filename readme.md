# Návod na zprovoznění CMS

- Nainstalovat si vendory přes composer
- Vytvořit databázi a nahrát prázdnou DB z rootu
- do .env souboru vložit hodnoty pro zprovoznění lokální DB

# Zadání pro tvorbu
## Vytvoření nového endpointu pro blogové příspěvky

V /app/Modules/contentEditor/api/requests.php vytvořit blueprint nového endpointu (authorized = false) a v ApiFactory.php vytvořit metodu pro tento blueprint.

Api je poté dostupné na URL/api/path-z-blueprintu.
API_ENABLED v env musí být true.

V CMS se blogové příspěvky a štítky tvoří v sekci obsah. Nejdříve je nutné vytvořit štítek a poté ho přiřadit blogovému článku.

### Výstupem bude:
Ednpoint který vrátí pole štítků blogu a k nim všechny blogové příspěvky, které daný šíttek mají. Příspěvky pouze viditelné, seřazené od nejnovějšího. A zároveň možnost aby endpoint přijmul limit a offset a dal se tak paginovat (paginace není nutná).

#### Pokročilejší (pokud bude čas - není nutné zpracovat)
V endpointu mít možnost určit pouze jeden štítek, dle kterého se poté blogové příspěvky vrátí. Tzn vyberu např. Zdraví a vrátí se mi pole šítků, kde bude 1 štítek zdraví a k němu XY blogových článků.

















