<?php
/* CUSTOM language file - ALLOWED TO EDIT
 * THIS file is LAST in load order -> If you need to edit texts at FRONTEND or BACKEND of application, place variables here
 * System will overwrite array variables from loaded files behind them
 * If you need create a new texts for frontend, place there a new string and use it
 * Be careful to CREATE UNIQUE variable name to avoid overwriting original texts unless they are intended
*/
$lang = [];

$lang["mod-contentBlocks-block-type-wysiwyg_tip"] = 'Tip';
$lang["mod-contentBlocks-block-type-wysiwyg_wrap"] = 'Osnova';
$lang["mod-contentBlocks-block-type-wysiwyg_table"] = 'Tabulka';
$lang["mod-contentBlocks-block-type-divider"] = 'Oddělovač';
$lang["mod-contentEditor-info-lengthTime"] = 'Délka tvoření';
$lang["mod-contentEditor-info-lengthTime"] = 'Délka tvoření';
$lang["mod-contentBlocks-block-type-button"] = 'Tlačítko';
$lang["mod-contentBlocks-block-type-button_primary"] = 'Primární';
$lang["mod-contentBlocks-block-type-button_secondary"] = 'Sekundární';
$lang["cb-title"] = 'Text';
$lang["cb-url"] = 'URL adresa';
$lang["cb-target"] = 'Cíl';
$lang["cb-target-self"] = 'Žádné';
$lang["cb-target-blank"] = 'Nové okno';
$lang["cb-align"] = 'Zarovnání';
$lang["cb-align-left"] = 'Vlevo';
$lang["cb-align-center"] = 'Na střed';
$lang["cb-align-right"] = 'Vpravo';
$lang["mod-contentBlocks-block-type-form"] = 'Formulář';
$lang["cb-emailKey-contact_form"] = 'Kontaktní formulář';
$lang["cb-emailKey-complaint_form"] = 'Reklamační formulář';
$lang["mod-contentBlocks-block-type-widget"] = 'Widgety';
$lang["mod-contentBlocks-block-type-widget_contacts"] = 'Kontakty';
$lang["mod-contentBlocks-block-type-widget_newsletter"] = 'Přihlášení k newsletteru';
$lang["mod-contentBlocks-block-type-widget_socials"] = 'Sociální sítě';
$lang["cb-widget-title"] = 'Titulek';