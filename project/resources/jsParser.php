<?php
// === Build minify js 
$jsReturn = false;
$compiler_parts = RESOURCES_DIR.'js/parts/';
$compiler_libraries = RESOURCES_DIR.'js/library/';
$compiler_media = RESOURCES_DIR.'js/responsive.css';

// === Minify
if(isset($_GET["afterDeploy"]) ) {
  $jsBuild = null;
                 
// MAIN APP JS
  $jsBuild .= file_get_contents($compiler_parts."main.js");

// App parts JS
    $handle = opendir($compiler_parts);
    if($handle !== false){
        while($file = readdir($handle)){
            if(!is_dir($file) && (strpos($file, 'main.js') === false)){
                $jsBuild .= file_get_contents($compiler_parts.$file);
            }
        }
    }

// Modules JS
    $modulesPaths = $modulesController->getConfig('paths');
    if( isset($modulesPaths) ){
        foreach( $modulesPaths as $modulePaths ){
            if( isset($modulePaths["js"]["frontend"]) ){
                foreach($modulePaths["js"]["frontend"] as $jsFile){
                    $jsBuild .= file_get_contents(ROOT.$jsFile);
                }
            }
        }
    }

    if($jsBuild !== null){
        (!is_dir(ASSETS_DIR.'js')) ? mkdir(ASSETS_DIR.'js') : null;
        file_put_contents($appScript, $jsBuild);

        // Third side JS (move libraries to assets folder)
        $handle = opendir($compiler_libraries);
        if($handle !== false){
            while($file = readdir($handle)){
                if(!is_dir($file)){
                    copy($compiler_libraries.$file, ASSETS_DIR.'js/'.$file);
                }
            }
        }

        $jsReturn = true;
    }
}