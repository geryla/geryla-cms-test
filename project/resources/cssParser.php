<?php
// === Build minify css 
$cssReturn = false;
$compiler_parts = RESOURCES_DIR.'css/parts/';
$compiler_libraries = RESOURCES_DIR.'css/library/';
$compiler_media = RESOURCES_DIR.'css/responsive.css';

// === Minify
if(isset($_GET["afterDeploy"])) {
    $cssBuild = null;

// App parts CSS
    $handle = opendir($compiler_parts);
    if($handle !== false){
        while($file = readdir($handle)){
            if(!is_dir($file) && (strpos($file, '.css') !== false) && (strpos($file, '.css.map') === false)){
                $cssBuild .= file_get_contents($compiler_parts.$file);
            }
        }
    }

// Modules CSS
    $modulesPaths = $modulesController->getConfig('paths');
    if( isset($modulesPaths) ){
        foreach($modulesPaths as $modulePaths){
            if( isset($modulePaths["css"]["frontend"]) ){
                foreach($modulePaths["css"]["frontend"] as $cssFile){
                    $cssBuild .= file_get_contents(ROOT.$cssFile);
                }
            }
        }
    }

// Third side CSS
    $handle = opendir($compiler_libraries);
    if($handle !== false){
        while($file = readdir($handle)){
            if(!is_dir($file)){
                $cssBuild .= file_get_contents($compiler_libraries.$file);
            }
        }
    }

// Responsive CSS
    if( file_exists($compiler_media) ){
        $cssBuild .= file_get_contents($compiler_media);
    }

    if($cssBuild !== null){
        (!is_dir(ASSETS_DIR.'css')) ? mkdir(ASSETS_DIR.'css') : null;
        file_put_contents($appCss, codeMinify($cssBuild));

        if(file_exists(RESOURCES_DIR.'css/default.css')){
            file_put_contents($defaultCss, codeMinify(file_get_contents(RESOURCES_DIR.'css/default.css')));
        }

        $cssReturn = true;
    }
}