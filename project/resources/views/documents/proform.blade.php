<!DOCTYPE html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>{{$data["documentTitle"]}}: {{$data["documentNumber"]}}</title>
        <style>
            * {
                margin: 0;
                padding: 0;
                font-family: Arial;
                font-size: 10pt;
                color: #000;
            }
            body {
                width: 100%;
                font-family: Arial;
                font-size: 10pt;
                margin: 0;
                padding: 0;
            }
            p {
                margin: 0;
                padding: 0;
            }
            #wrapper {
                width: 190mm;
                margin: 0 10mm;
            }
            table {
                border-left: 1px solid #ccc;
                border-top: 1px solid #ccc;
                border-spacing: 0;
                border-collapse: collapse;
            }
            table td {
                border-right: 1px solid #ccc;
                border-bottom: 1px solid #ccc;
                padding: 2mm;
            }
            table.noBorderBottom td {
                border-bottom: 0px;
            }
            table.heading {
                height: 50mm;
            }
            h1.heading {
                font-size: 14pt;
                color: #000;
                font-weight: normal;
            }
            h2.heading {
                font-size: 9pt;
                color: #000;
                font-weight: normal;
            }
            hr {
                color: #ccc;
                background: #ccc;
            }
            #invoice_body {
                height: 145mm;
            }
            #invoice_body ,
            #invoice_total {
                width: 100%;
            }
            #invoice_body table ,
            #invoice_total table {
                width: 100%;
                border-left: 1px solid #ccc;
                border-top: 1px solid #ccc;
                border-spacing: 0;
                border-collapse: collapse;
                margin-top: 5mm;
            }
            #invoice_body table td{
                text-align: center;
                font-size: 9pt;
                border-right: 1px solid #ccc;
                border-bottom: 1px solid #ccc;
                padding: 1mm 0;
            }
            tr.invoice_total td {
                text-align: center;
                font-size: 9pt;
                border-right: 1px solid #ccc;
                border-bottom: 1px solid #ccc;
                padding: 1mm 0;
            }
            tr.empty td {
                border-left: 1px solid white;
                border-right: 0px;
                padding-bottom: 4mm;
            }
            #invoice_body table td.right  ,
            #invoice_total table td.right {
                text-align: right;
                padding-right: 3mm;
                font-size: 9pt;
            }
            #footer {
                width: 180mm;
                margin: 0 15mm;
                padding-bottom: 3mm;
                text-align: center;
            }
        </style>
    </head>
    <body>
        <div id="wrapper">
            <p style="text-align:center; font-weight:bold; padding-top:5mm;">{{$data["documentTitle"]}}: {{$data["documentNumber"]}}</p><br />
            <table style="width:100%;" class="noBorderBottom">
                <tr>
                    <td valign="top" align="right" style="padding:2mm;">
                        <table>
                            <tr><td>@lang("module-invoice-render-header-orderNum"): </td><td>{{$data["orderDetails"]["orderId"]}}</td></tr>
                            <tr><td>@lang("module-invoice-render-header-variableSymbol"): </td><td>{{$data["variableSymbol"]}}</td></tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table style="width:100%;">
                <tr>
                    <td style="width:50%;" valign="top">
                        <b>@lang("module-invoice-render-header-supplier")</b>:<br /><br />
                        {{$data["invoiceDetails"]["companyName"]}}<br />
                        {{$data["invoiceDetails"]["companyStreet"]}} {{$data["invoiceDetails"]["companyHouseNum"]}}<br />
                        {{$data["invoiceDetails"]["companyZip"]}} {{$data["invoiceDetails"]["companyCity"]}}<br />

                        @if(!empty($data["invoiceDetails"]["companyCountry"])) {{$data["invoiceDetails"]["companyCountry"]}} <br /> @endif <br />
                        @if(!empty($data["invoiceDetails"]["identNum"])) @lang("module-invoice-render-header-identNum"): {{$data["invoiceDetails"]["identNum"]}} <br /> @endif
                        @if(!empty($data["invoiceDetails"]["vatNum"])) @lang("module-invoice-render-header-vatNum"): {{$data["invoiceDetails"]["vatNum"]}} <br /> @endif
                        @if(!empty($data["invoiceDetails"]["taxNum"])) @lang("module-invoice-render-header-taxNum"): {{$data["invoiceDetails"]["taxNum"]}} <br /> @endif
                        @if(!$taxUsed) @lang('module-invoice-render-invoice-noTaxPayer') @endif
                    </td>
                    <td valign="top">
                        <b>@lang("module-invoice-render-header-subscriber")</b>:<br /><br />
                        @if(!empty($data["orderDetails"]["companyName"])) {{$data["orderDetails"]["companyName"]}} @else {{$data["orderDetails"]["invoiceName"]}} {{$data["orderDetails"]["invoiceSurname"]}} @endif <br />
                        {{$data["orderDetails"]["invoiceStreet"]}} <br />
                        {{$data["orderDetails"]["invoiceZip"]}} {{$data["orderDetails"]["invoiceCity"]}} <br />

                        @if(!empty($data["orderDetails"]["companyIdentNum"])) @lang("module-invoice-render-header-identNum"): {{$data["orderDetails"]["companyIdentNum"]}} <br /> @endif
                        @if(!empty($data["orderDetails"]["companyVat"])) @lang("module-invoice-render-header-vatNum"): {{$data["orderDetails"]["companyVat"]}} <br /> @endif
                        @if(!empty($data["orderDetails"]["companyTax"])) @lang("module-invoice-render-header-taxNum"): {{$data["orderDetails"]["companyTax"]}} <br /> @endif
                        <br />
                        {{$data["orderDetails"]["invoiceEmail"]}} <br />
                        @if(!empty($data["orderDetails"]["invoicePhone"])) {{$data["orderDetails"]["invoicePhone"]}} @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;">
                        @lang("module-invoice-render-header-bankAcc"):<br />
                        {{$data["invoiceDetails"]["paymentName"]}}: {{$data["invoiceDetails"]["paymentNum"]}}<br />
                        @if(!empty($data["invoiceDetails"]["paymentIban"])) @lang("module-invoice-render-header-iban"): {{$data["invoiceDetails"]["paymentIban"]}}<br /> @endif
                        @if(!empty($data["invoiceDetails"]["paymentSwift"]))  @lang("module-invoice-render-header-swift"): {{$data["invoiceDetails"]["paymentSwift"]}}<br /> @endif
                        @if(!empty($data["invoiceDetails"]["registerText"])) <br /> {{$data["invoiceDetails"]["registerText"]}} @endif
                    </td>
                    <td>
                        <table valign="top" align="right" style="padding:2mm;">
                            <tr><td>@lang("module-invoice-render-header-dateOfIssue"): </td><td>{{$data["createdDate"]}}</td></tr>
                            <tr><td>@lang("module-invoice-render-header-dateDue"): </td><td>{{$data["payDate"]}}</td></tr>
                            @if($taxUsed) <tr><td>@lang("module-invoice-render-header-dateOfSupply"): </td><td>{{$data["payDate"]}}</td></tr> @endif
                        </table>
                    </td>
                </tr>
            </table>

            <div id="content">
                <div id="invoice_body">
                    @if($taxUsed)
                        <table>
                            <tr style="background:#eee;">
                                <td style="width:32%;text-align:left;padding-left:10px;"><b>@lang("module-invoice-render-content-name")</b></td>
                                <td style="width:8%;"><b>@lang("module-invoice-render-content-quantity")</b></td>
                                <td style="width:15%;text-align:right;padding-right:10px;"><b>@lang("module-invoice-render-content-priceSingle")</b></td>
                                <td style="width:12%;"><b>@lang("module-invoice-render-content-tax")</b></td>
                                <td style="width:13%;"><b>@lang("module-invoice-render-content-taxValue")</b></td>
                                <td style="width:20%;text-align:right;padding-right:10px;"><b>@lang("module-invoice-render-content-totalTax")</b></td>
                            </tr>
                        </table>
                        <table>
                            @foreach($data["items"] as $item)
                            <tr>
                                <td style="width:32%;text-align:left; padding-left:10px;">{{$item["name"]}} @if($item["selectedAttributes"]) <br /> {{$item["selectedAttributes"]}} @endif</td>
                                <td class="right" style="width:8%;">{{$item["quantity"]}}</td>
                                <td class="right" style="width:15%;">{{$item["price"]}}</td>
                                <td class="right" style="width:12%">{{$item["tax"]}}</td>
                                <td class="right" style="width:13%">{{$item["taxValue"]}}</td>
                                <td class="right" style="width:20%">{{$item["priceTotal"]}}</td>
                            </tr>
                            @endforeach
                        </table>
                    @else
                        <table>
                            <tr style="background:#eee;">
                                <td style="width:45%;text-align:left;padding-left:10px;"><b>@lang("module-invoice-render-content-name")</b></td>
                                <td style="width:10%;"><b>@lang("module-invoice-render-content-quantity")</b></td>
                                <td style="width:25%;text-align:right;padding-right:10px;"><b>@lang("module-invoice-render-content-priceSingle")</b></td>
                                <td style="width:20%;text-align:right;padding-right:10px;"><b>@lang("module-invoice-render-content-totalTax")</b></td>
                            </tr>
                        </table>
                        <table>
                            @foreach($data["items"] as $item)
                                <tr>
                                    <td style="width:45%;text-align:left; padding-left:10px;">{{$item["name"]}} @if($item["selectedAttributes"]) <br /> {{$item["selectedAttributes"]}} @endif</td>
                                    <td class="right" style="width:10%;">{{$item["quantity"]}}</td>
                                    <td class="right" style="width:25%;">{{$item["price"]}}</td>
                                    <td class="right" style="width:20%">{{$item["priceTotal"]}}</td>
                                </tr>
                            @endforeach
                        </table>
                    @endif

                    <table>
                        @if($taxUsed)
                            <tr class="invoice_total">
                                <td colspan="5" style="width:25%;text-align:left;padding-left:10px;">@lang("module-invoice-render-summary-withoutTax")</td>
                                <td style="width:20%;text-align:right;padding-right:10px;">{{$data["totalPrice_noTax"]}}</td>
                            </tr>

                            @if(!empty($data["taxes"]))
                                @foreach($data["taxes"] as $taxIndex => $taxValue){
                                    <tr class="invoice_total">
                                        <td colspan="5" style="width:25%;text-align:left;padding-left:10px;">@lang("module-invoice-render-summary-taxValue") {{$taxIndex}}%</td>
                                        <td style="width:20%;text-align:right;padding-right:10px;">{{$taxValue}}</td>
                                    </tr>
                                @endforeach
                            @endif
                        @endif

                        <tr class="invoice_total">
                            <td colspan="@if($taxUsed) 5 @else 3 @endif" style="width:25%;text-align:left;padding-left:10px;">
                                @if($taxUsed) <b>@lang('module-invoice-render-summary-totalWithTax')</b> @else <b>@lang('module-invoice-render-summary-total')</b> @endif
                            </td>
                            <td style="width:20%;text-align:right;padding-right:10px;">
                                <b>{{$data["totalPrice"]}}</b>
                            </td>
                        </tr>
                        <tr class="invoice_total">
                            <td colspan="@if($taxUsed) 5 @else 3 @endif" style="background:#eee;font-size: 12pt;width:25%;text-align:left;padding-left:10px;">
                                @if($taxUsed) <b>@lang('module-invoice-render-summary-totalWithTaxRounded')</b> @else <b>@lang('module-invoice-render-summary-totalRounded')</b> @endif
                            </td>
                            <td style="background:#eee;font-size: 12pt;width:20%;text-align:right;padding-right:10px;">
                                <b>{{$data["totalPrice_rounded"]}}</b>
                            </td>
                        </tr>
                    </table>

                </div>
            </div>
        </div>

        <htmlpagefooter name="footer"><div id="footer">&copy; {{date('Y')}} - {{$data["invoiceDetails"]["companyName"]}}</div></htmlpagefooter>
        <sethtmlpagefooter name="footer" value="on" />
    </body>
</html>