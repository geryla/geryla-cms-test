<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="description" content="@meta('description')">
        <meta name="keywords" content="@meta('keywords')">
        <meta name="author" content="@lang('developer_copyright-meta')">
        
        <meta property="og:url" content="@meta('url')">
        <meta property="og:title" content="@meta('title')">
        <meta property="og:description" content="@meta('description')">
        <meta property="og:site_name" content="@lang('page_title')">
        <meta property="og:locale" content="@meta('locale')">
        <meta property="og:image" content="@meta('image')">
        <meta property="og:image:width" content="1200">
        <meta property="og:image:height" content="630">

        <meta name="robots" content="@meta('robots')" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@lang('page_title') - @meta('title')</title>

        <link rel="shortcut icon" type="image/x-icon" href="/resources/imgs/favicon.ico">
        <link rel="stylesheet" type="text/css" href="@asset('screen.css', 'css', false, true)">
        <script src="@asset('jquery.min.js', 'js')"></script>
        @yield('head_scripts')
        @hook('head')
    </head>
    <body id="top">
        @yield('header')
        @yield('content')
        @yield('footer')

        @yield('footer_scripts')
        <script src="@asset('modernizr-2.6.2.min.js', 'js')"></script>
        <script src="@asset('owl.carousel.min.js', 'js')"></script>
        <script src="@asset('jquery-ui.js', 'js')"></script>
        <script src="@asset('app.js', 'js', false, true)"></script>
        @hook('footer')

    </body>
</html>